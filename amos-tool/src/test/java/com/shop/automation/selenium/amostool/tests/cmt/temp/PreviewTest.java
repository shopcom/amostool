package com.shop.automation.selenium.amostool.tests.cmt.temp;

import com.shop.automation.amostool.seleninum.pages.contentmangement.CMTHeaderPage;
import com.shop.automation.amostool.seleninum.pages.contentmangement.PreviewPage;
import com.shop.automation.amostool.seleninum.testdata.ImageModule;
import com.shop.automation.amostool.utils.datamanagement.dataprovider.AmosExcelDataDrivenProvider;
import com.shop.automation.selenium.amostool.tests.base.CMTBaseTest;
import org.testng.annotations.Test;

import java.util.List;


public class PreviewTest extends CMTBaseTest {

    @Test(dataProviderClass = AmosExcelDataDrivenProvider.class, dataProvider = "imageModules")
    public void addImageModule(ImageModule imageModule, List<String> testCampaignNames) throws InterruptedException {

        CMTHeaderPage headerpage = new CMTHeaderPage(driver, translator, elementDefinitionManager);
        headerpage.searchHomePageToolTestSite();

        Thread.sleep(10 * 1000);
        headerpage.getPreviewButton().click();
        Thread.sleep(5 * 1000);

        PreviewPage previewTest = new PreviewPage(driver, translator, elementDefinitionManager);

        previewTest.switchWindow();
        Thread.sleep(5 * 1000);

        System.out.println("driver.getCurrentUrl(=)" + driver.getCurrentUrl());


        previewTest.verifyImageModule(imageModule);


    }


}