package com.shop.automation.selenium.amostool.tests.base;


import com.shop.automation.amostool.seleninum.pages.common.ToolSelectionPage;
import com.shop.automation.amostool.seleninum.pages.contentmangement.CMTToolPage;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;

/**
 * Content Manage Tool
 * Created by bruce.zhang on 8/27/15.
 */
public class CMTBaseTest extends AmosBaseTests {
     protected static CMTToolPage cmtToolPage;

    /**
     * navigate to CMT Tool & do some validation
     *
     * @param context context
     * @param method  method
     * @param result  result
     */
    @BeforeMethod(alwaysRun = true)
    public void setupMethod(final ITestContext context, final Method method,
                            final ITestResult result) {
        super.setupMethod(context, method, result);
        //Navigate to Content MangementTool
        new ToolSelectionPage(driver, translator, elementDefinitionManager).goToContentManagementTool();
        cmtToolPage = new CMTToolPage(driver, translator, elementDefinitionManager);
    }

    /**
     * CMT Tools choose Page (Home Page) ---> Site(Full Site) --->(SHOP.COM) --->Country (Test)
     * this is common use for CMT HomePage tool
     */
    public void searchHomePageTestFullSite() {
        cmtToolPage.searchHeader.searchHomePageToolTestSite();
        cmtToolPage.containerPage.verifyAfterSearch();

    }

    /**
     * Refresh the Page HomePage
     */
    public void refreshHomePageTestCountry() {
        logger.info("##Refresh The HomePage SHOPCOM Test Site");
        cmtToolPage.searchHeader.refreshHomePageTestCountry();
        cmtToolPage.containerPage.verifyAfterSearch();

    }

    public void previewHomePageTestSite() {
        cmtToolPage.searchHeader.preview();
    }

    /**
     * CMT Tools choose Page (Department Page) --->Country (Test) --->Department (Beauty)
     * this is common use for CMT HomePage tool
     */
    public void searchDepartmentPageBeauty() {
        searchDepartmentPageTestCountry();
        //TODO choose Beauty Department
    }

    /**
     * CMT Tools choose Page (Department Page) --->Country (Test)
     * this is common use for CMT Department tool
     */
    private void searchDepartmentPageTestCountry() {
        //TODO
    }
}
