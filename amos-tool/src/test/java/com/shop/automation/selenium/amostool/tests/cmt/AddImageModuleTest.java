package com.shop.automation.selenium.amostool.tests.cmt;

import com.marketamerica.automation.annotations.TestDoc;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.shop.automation.amostool.seleninum.pages.contentmangement.CMTToolPage;
import com.shop.automation.amostool.seleninum.pages.contentmangement.CampaignPage;
import com.shop.automation.amostool.seleninum.pages.contentmangement.ImageModulePage;
import com.shop.automation.amostool.seleninum.pages.contentmangement.PreviewPage;
import com.shop.automation.amostool.seleninum.testdata.Campaign;
import com.shop.automation.amostool.seleninum.testdata.ImageModule;
import com.shop.automation.amostool.seleninum.testdata.ModuleType;
import com.shop.automation.amostool.utils.datamanagement.dataprovider.AmosExcelDataDrivenProvider;
import com.shop.automation.selenium.amostool.tests.base.CMTBaseTest;

import org.testng.annotations.Test;

/**
 * Created by bruce.zhang on 8/24/15.
 */
public class AddImageModuleTest extends CMTBaseTest {

	@Test(dataProviderClass = AmosExcelDataDrivenProvider.class, dataProvider = "imageModules")
	@TestDoc(description = "This test script will test Image Module", supportedCountries = { Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN, Country.MEXICO,
			Country.AUSTRALIA, Country.CANADA, Country.TAIWAN, Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = { Environment.STAGING, Environment.LIVE,
			Environment.LTV })
	public void addImageModule(final String id, final String description, ImageModule imageModule, Campaign campaign) {
		// search Homepage Full site , Test Country
		searchHomePageTestFullSite();

		CMTToolPage cmtToolPage = new CMTToolPage(driver, translator, elementDefinitionManager);

		CampaignPage campaginPage = new CampaignPage(driver, translator, elementDefinitionManager);
		// delete test data
		cmtToolPage.containerPage.deleteTestCompagins(campaign);

		if (!cmtToolPage.containerPage.verifyAfterDeleteCampaign(campaign)) {
			refreshHomePageTestCountry();
		}

		// click add a Campaign to pop up add Campaign Box pop up Add Campaign
		// Window fullFill Form with prepared data from Excel & XML
		cmtToolPage.containerPage.addCampaignContainer();

		// Fill up Campaign data and save Campaign
		campaginPage.createNewCampaign(campaign);

		if (cmtToolPage.containerPage.isStillLoading()) {
			refreshHomePageTestCountry();
		}

		cmtToolPage.containerPage.verifyAfterAddCampain(campaign);

		// Add Module
		cmtToolPage.containerPage.chooseModule(campaign.getCampaignName(), ModuleType.IMAGE.getCode());

		ImageModulePage imageModulePage = new ImageModulePage(driver, translator, elementDefinitionManager);

		imageModulePage.createImageModule(cmtToolPage.containerPage.getModuleWindow(), imageModule);

		if (cmtToolPage.containerPage.needRefreshPage(campaign)) {
			refreshHomePageTestCountry();
		}

		cmtToolPage.containerPage.verifyAfterAddImageModule(campaign, imageModule);

		previewHomePageTestSite();

		PreviewPage previewPage = new PreviewPage(driver, translator, elementDefinitionManager);

		previewPage.verifyImageModule(imageModule);

		StepInfo.addMessage(description + ",  Add Image module successfully and verify successfully");

		// use data fill form
	}

	protected String getBaseURL() {
		return null;
	}
}
