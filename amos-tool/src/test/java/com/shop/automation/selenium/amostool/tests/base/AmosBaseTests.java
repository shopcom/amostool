package com.shop.automation.selenium.amostool.tests.base;

import static org.testng.AssertJUnit.assertTrue;

import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.testmanagement.TestCases;
import com.shop.automation.amostool.seleninum.pages.common.LoginPage;
import com.shop.automation.amostool.seleninum.pages.common.ToolSelectionPage;
import com.shop.automation.amostool.seleninum.pages.common.WelcomePage;

import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;

/**
 * All the automation test for AmosTool will inherit this class It will Login
 * first and go the the Tool selection page
 * <p/>
 * Created by bruce.zhang on 8/24/15.
 */
public abstract class AmosBaseTests extends TestCases {
	private static final String LOGINPAGE_URL_STAGING = "https://staging-amos.shop.com/cc.class/cc?main=tools_signin&act=1&ccsyn=249";
	private static final String LOGINPAGE_URL_DEVLOPMENT = "https://dev-amos.shop.com/cc.class/cc?main=tools_signin&act=1&ccsyn=250";

	protected static void pause() {
		long end = System.currentTimeMillis() + 6000;
		while (System.currentTimeMillis() < end) {
		}
	}

	/**
	 * Go to Amos Login page , Sign in and Navigate to Tool selection Page
	 *
	 * @param context
	 *            context
	 * @param method
	 *            method
	 * @param result
	 *            result
	 */
	@BeforeMethod(alwaysRun = true)
	public void setupMethod(final ITestContext context, final Method method,
			final ITestResult result) {
		super.setupMethod(context, method, result);

		// Navigate to Login Page		
		driver.get(getLoginURL());

		LoginPage loginPage = new LoginPage(driver, translator,
				elementDefinitionManager);

		// navigate to Login page , fill up all username & password then click
		// sign in button to Login go to welcome page
		WelcomePage welcomePage = new WelcomePage(driver, translator,
				elementDefinitionManager);

		// navigate to Login page , fill up all username & password then click
		// sign in button to Login go to welcome page		
		loginPage.login();

		// click SHOP.COM href to navigate to Tool Selection List Page
		StepInfo.addMessage("Go to Tool Selection List Page ");
//		welcomePage.goToToolSelectionPage();

		// Verify whether login successfully
		ToolSelectionPage toolSelectionPage = new ToolSelectionPage(driver,
				translator, elementDefinitionManager);
		toolSelectionPage.verifyBodyExisted();

	}

	/**
	 * get Login page URL according to environment
	 * 
	 * @return loginURL
	 */
	protected String getLoginURL() {
		switch (environment) {
		case STAGING:
			return LOGINPAGE_URL_STAGING;
		case DEVELOPMENT:
			return LOGINPAGE_URL_DEVLOPMENT;
		}
		return "";
	}

	@Override
	protected String getBaseURL() {
		return null;
	}
}
