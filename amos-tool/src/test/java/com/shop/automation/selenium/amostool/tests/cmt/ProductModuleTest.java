package com.shop.automation.selenium.amostool.tests.cmt;

import com.marketamerica.automation.annotations.TestDoc;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.shop.automation.amostool.seleninum.pages.contentmangement.*;
import com.shop.automation.amostool.seleninum.testdata.Campaign;
import com.shop.automation.amostool.seleninum.testdata.ModuleType;
import com.shop.automation.amostool.seleninum.testdata.ProductModule;
import com.shop.automation.amostool.utils.datamanagement.dataprovider.AmosExcelDataDrivenProvider;
import com.shop.automation.selenium.amostool.tests.base.CMTBaseTest;

import org.testng.annotations.Test;

import java.util.List;

/**
 * Created by bruce.zhang on 8/24/15.
 */
public class ProductModuleTest extends CMTBaseTest {

	@Test(dataProviderClass = AmosExcelDataDrivenProvider.class, dataProvider = "productModules")
	@TestDoc(description = "This test script will test product module", supportedCountries = { Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN, Country.MEXICO,
			Country.AUSTRALIA, Country.CANADA, Country.TAIWAN, Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = { Environment.STAGING, Environment.LIVE,
			Environment.LTV })
	public void productModules(final String id, final String description, ProductModule productModule, Campaign campaign) {
		// Choose Home Page Full site -->Test Country
		searchHomePageTestFullSite();

		CampaignPage campaginPage = new CampaignPage(driver, translator, elementDefinitionManager);

		// Delete test data
		cmtToolPage.containerPage.deleteTestCompagins(campaign);

		if (!cmtToolPage.containerPage.verifyAfterDeleteCampaign(campaign)) {
			refreshHomePageTestCountry();
		}

		// click add a Campaign to pop up add Campaign Box pop up Add Campaign
		// Window fullFill Form with prepared data from Excel & XML
		cmtToolPage.containerPage.addCampaignContainer();

		// Fill up Campaign data and save Campaign
		campaginPage.createNewCampaign(campaign);

		if (cmtToolPage.containerPage.isStillLoading()) {
			refreshHomePageTestCountry();
		}

		cmtToolPage.containerPage.verifyAfterAddCampain(campaign);

		// Add Module
		cmtToolPage.containerPage.chooseModule(campaign.getCampaignName(), ModuleType.PRODUCT.getCode());

		ProductModuleSelectPage productModuleSelectPage = new ProductModuleSelectPage(driver, translator, elementDefinitionManager);

		/*
		 * Choose Product Module according to the ProductModuleType 80x80
		 * Multi-Row - 80x80 Multi-Row with 300x250
		 */
		productModuleSelectPage.chooseProductModuleByType(productModule);

		ProductModulePage productModulePage = new ProductModulePage(driver, translator, elementDefinitionManager);

		productModulePage.saveModule(cmtToolPage.containerPage.getModuleWindow(), productModule);

		if (cmtToolPage.containerPage.needRefreshPage(campaign)) {
			refreshHomePageTestCountry();
		}
		cmtToolPage.containerPage.verifyAfterAddProductModuleWithModuleTitle(productModule, campaign);
		cmtToolPage.containerPage.verifyAfterAddProductModuleWithProductURL(productModule, campaign);

		List<String> list = cmtToolPage.containerPage.getProductModuleID(campaign);

		previewHomePageTestSite();

		PreviewPage previewPage = new PreviewPage(driver, translator, elementDefinitionManager);

		previewPage.verifyProductModule(productModule, list, campaign);

        StepInfo.addMessage(description + ", Add product module successfully and verify successfully");

		// use data fill form
	}

	protected String getBaseURL() {
		return null;
	}
}
