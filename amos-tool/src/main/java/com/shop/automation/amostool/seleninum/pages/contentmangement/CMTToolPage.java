package com.shop.automation.amostool.seleninum.pages.contentmangement;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.shop.automation.amostool.seleninum.pages.base.AmosPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * This page is Whole CMT TOOL Page
 * it contains search page / contents page
 * Created by bruce.zhang on 8/30/15.
 */
public class CMTToolPage extends AmosPage {
    public final CMTHeaderPage searchHeader;
    public final ContainerPage containerPage;

    public CMTToolPage(WebDriver driver, Translator translator, ElementDefinitionManager elementDefinitionManager) {
        super(driver, translator, elementDefinitionManager);
        searchHeader = new CMTHeaderPage(driver,translator,elementDefinitionManager);
        containerPage = new ContainerPage(driver,translator,elementDefinitionManager);
    }

    @Override
    protected By getBody() throws UnableToVerifyPagesExistenceException {
        return null;
    }

    @Override
    public boolean atPage() throws UnableToVerifyPagesExistenceException {
        return false;
    }
}
