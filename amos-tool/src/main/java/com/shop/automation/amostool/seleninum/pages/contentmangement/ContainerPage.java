package com.shop.automation.amostool.seleninum.pages.contentmangement;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.shop.automation.amostool.seleninum.pages.base.AmosPage;
import com.shop.automation.amostool.seleninum.testdata.Campaign;
import com.shop.automation.amostool.seleninum.testdata.ImageModule;
import com.shop.automation.amostool.seleninum.testdata.ImageUploadTypeEnum;
import com.shop.automation.amostool.seleninum.testdata.Product;
import com.shop.automation.amostool.seleninum.testdata.ProductModule;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.io.File;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import static org.testng.AssertJUnit.assertTrue;

/**
 * Is class equals container of campaign
 *
 * @author Dawn.Fu
 */
public class ContainerPage extends AmosPage {

	private List<WebElement> givenCampaigns;
	private CampaignPage campaignPage;

	private static final String CAMPAIGN_FIELDSET_LEGEND = ".//fieldset/legend[text()=\"%s\"]/..";

	// ImageURL of image module
	private static final String IMAGE_MODULE_URL = ".//fieldset/legend[text()=\"%s\"]/..//img[@src=\"%s\"]";
	private static final String IMAGE_MODULE_LOCALURL = ".//fieldset/legend[text()=\"%s\"]/..//div[2]//img";
	// Alt Tag of image module
	private static final String IMAGE_MODULE_ALT = ".//fieldset/legend[text()=\"%s\"]/..//img[@alt=\"%s\"]";
	// product module title
	private static final String PRODUCT_MODULE_TITLE = ".//fieldset/legend[text()=\"%s\"]/..//div[@class=\"module-title\"]/div";
	// productID in product module
	private static final String PRODUCT_MODULE_ID = ".//fieldset/legend[text()=\"%s\"]/..//tr[@class=\"ui-widget-content jqgrow ui-row-ltr\"]/td[2]";
	private static final By MODULE_POPUP_DIV = By.id("modulePopupDiv");

	/* Campaign container */
	// Button: Add a campaign Container
	private static final By ADD_CONTAINER_BTN = By.id("containerBtn");
	// Button: Edit Campaign
	private static final By BTN_EDIT_CAMPAIGN = By.name("editCampaign");

	/* Module */
	// Button: Add Module
	private static final By BTN_CHOOSE_MODULE = By.name("chooseModule");
	// Button: Edit
	private static final By BTN_EDIT_MODULE_CONTENT = By
			.name("editModuleContent");

	/* Add a module window */
	// Window DIV id
	private static final By MODULE_DIV = By.id("moduleDiv");
	// Select element id
	private static final By MODULE_CHOOSE = By.id("moduleChoose");
	// Button: Next
	private static final By MODULE_ADD = By.id("moduleAdd");

	/**
	 * Super constructor
	 *
	 * @param driver
	 * @param translator
	 * @param elementDefinitionManager
	 */
	public ContainerPage(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
		campaignPage = new CampaignPage(driver, translator,
				elementDefinitionManager);
	}

	/**
	 * Click Add a campaign Container button
	 */
	public void addCampaignContainer() {
		this.waitForElementToBeClickable(ADD_CONTAINER_BTN);
		getContainerBtn().click();
		StepInfo.addMessage("Open Add Campaign Container Page");
	}

	/**
	 * is still loading after operation
	 * 
	 * @return loading
	 */
	public boolean isStillLoading() {
		boolean elementExisting = isElementExisting(LOADING_IMG, ZERO_SECOND);
		// find new Added Campaign Container after Loading Image Disappear.
		return elementExisting;
	}

	/**
	 * Click Add Module button
	 *
	 * @param campaignName
	 *            -- use to locate the container
	 * @param moduleTypeValue
	 *            -- module type number value
	 */
	public void chooseModule(String campaignName, final int moduleTypeValue) {
		StepInfo.addMessage("Start to create a module");
		givenCampaigns = getGivenCampaignContainers(campaignName);

		Assert.assertNotNull(givenCampaigns);

		WebElement btnChooseModule = givenCampaigns.get(0).findElement(
				BTN_CHOOSE_MODULE);
		btnChooseModule.click();
		WebElement moduleDiv = findElement(MODULE_DIV);
		Select moduleChoose = new Select(moduleDiv.findElement(MODULE_CHOOSE));
		moduleChoose.selectByValue(String.valueOf(moduleTypeValue));
		moduleDiv.findElement(MODULE_ADD).click();
	}

	/**
	 * Delete Campaign according to Campaign Title
	 *
	 * @param campaign
	 *            -- one case data
	 */
	public void deleteTestCompagins(Campaign campaign) {
		StepInfo.addMessage("Clear the old Test Data of campaign");
		if (isGiveTitleContainerExist(campaign.getCampaignName())) {
			List<WebElement> containers = getGivenCampaignContainers(campaign
					.getCampaignName());
			for (WebElement container : containers) {
				WebElement editCampaign = container
						.findElement(BTN_EDIT_CAMPAIGN);
				editCampaign.click();
				campaignPage.deleteCampaign();
				alertAccept();
				alertAccept();
				this.waitForjQueryAjax("Waiting for deleted element reload");
				waitForLoadingFinish();
			}
		} else {
			logger.warn("No Test data Exist!");
		}
	}

	/**
	 * According to title to obtain a given campaign container
	 *
	 * @param campaignName
	 * @return container element
	 */
	public List<WebElement> getGivenCampaignContainers(String campaignName) {
		if (isGiveTitleContainerExist(campaignName)) {
			givenCampaigns = getGivenCampaigns(campaignName);
		}
		return givenCampaigns;
	}

	/**
	 * Check test data Campaign container is exist or not
	 *
	 * @param campaignName
	 *            test data title
	 * @return exist or not
	 */
	public boolean isGiveTitleContainerExist(final String campaignName) {
		return isElementExistingAndDisplayed(
				getSpecialCampaignContainerBy(campaignName), 0);
	}

	/**
	 * Find containerBtn button element of current campaign container
	 *
	 * @return found containerBtn button element
	 */
	public WebElement getContainerBtn() {
		return findElement(ADD_CONTAINER_BTN);
	}

	/**
	 * Find campaign container element
	 *
	 * @param campaignName
	 *            use to locate the container
	 * @return found container element
	 */
	public List<WebElement> getGivenCampaigns(String campaignName) {
		return findElements(getSpecialCampaignContainerBy(campaignName));
	}

	/**
	 * Get the xpath of specified container
	 *
	 * @param campaignName
	 *            use to locate the container
	 * @return container xpath
	 */
	public By getSpecialCampaignContainerBy(String campaignName) {
		return messageFormat(CAMPAIGN_FIELDSET_LEGEND, campaignName);
	}

	/**
	 * Get the ImageModule with the ImageURL
	 *
	 * @param imageModule
	 * @return ImagrModule Xpath
	 */
	private By getImageModuleWithImageURL(Campaign campaign,
			ImageModule imageModule) {
		String campaignName = campaign.getCampaignName();
		String imageURL = imageModule.getImageUrl();
		return messageFormat(IMAGE_MODULE_URL, campaignName, imageURL);
	}

	/**
	 * Get the ImageModule with the Image AltTag
	 *
	 * @param imageModule
	 * @return ImagrModule Xpath
	 */
	private By getImageModuleWithImageAlt(Campaign campaign,
			ImageModule imageModule) {
		String campaignName = campaign.getCampaignName();
		String alt = imageModule.getAlt();
		return messageFormat(IMAGE_MODULE_ALT, campaignName, alt);
	}

	/**
	 * Find editModuleContent button of current campaign container
	 *
	 * @return found editModuleContent button element
	 */
	public WebElement getEditModuleContent() {
		return findElement(BTN_EDIT_MODULE_CONTENT);
	}

	/**
	 * Verify "Add a campaign Container" button and "hero&subhero container"
	 * module should display
	 */
	public void verifyAfterSearch() {
		this.waitForjQueryAjax("Wait before do verifyAfterSearch");
		assertTrue(isElementExisting(getContainerBtn()));
		StepInfo.addMessage("Verify the Add a campaign Container Button existing");
		assertTrue(isElementExisting(getSpecialCampaignContainerBy("Hero & Subhero Container")));
		waitForLoadingFinish();
		StepInfo.addMessage("Verify the Hero & Subhero Container existing and displaying");
	}

	/**
	 * Get the "Edit Campaign" button for the specific campaign
	 */
	public WebElement getEditCampainButton(Campaign campaign) {
		return findAbsoluteElement(campaign.getCampaignName(),
				BTN_EDIT_CAMPAIGN);

	}

	/**
	 * Get the "Add Module" button for the specific campaign
	 */
	public WebElement getAddModuleButton(Campaign campaign) {
		return findAbsoluteElement(campaign.getCampaignName(),
				BTN_CHOOSE_MODULE);
	}

	/**
	 * @param campaignName
	 * @param findingElement
	 * @return
	 */
	private WebElement findAbsoluteElement(String campaignName,
			By findingElement) {
		WebElement element = findElement(getSpecialCampaignContainerBy(campaignName));
		return element.findElement(findingElement);
	}

	/**
	 * Check whether can find Product Module with the local Image
	 * 
	 * @return
	 */

	private void verifyProductModuleWithLocalImage(ProductModule product,
			Campaign campaign) {
		String localimage = product.getUploadImagePath();
		localimage = localimage.substring(localimage.lastIndexOf("/"));

		boolean status = false;
		String uploadimageurl = getUploadImageURL(campaign);
		if (StringUtils.isNotBlank(uploadimageurl)
				&& uploadimageurl.contains(localimage)) {
			status = true;
		}

		assertTrue(status);

		System.out.println("Productmodule's imageURL from test data is "
				+ product.getImageUrl());
		StepInfo.addMessage("Verify Product module with the AltTag successfully on Preview Page");
	}

	/**
	 * Get Uploaded ImageURL from product module
	 * 
	 * @param campaign
	 * @return
	 */
	public String getUploadImageURL(Campaign campaign) {
		// Get Uploaded ImageURL from product module
		List<WebElement> campaigns = getGivenCampaigns(campaign
				.getCampaignName());
		campaigns.get(campaigns.size() - 1)
				.findElement(By.name("editModuleContent")).click();
		return getModuleWindow().findElement(By.id("url")).getText();

	}

	/**
	 * Verify Whether the campaign display, and whether has "Edit Campaign" and
	 * "Add Module" button after add campaign
	 */
	public void verifyAfterAddCampain(Campaign campaign) {
		String campaignName = campaign.getCampaignName();
		assertTrue(isElementExisting(getSpecialCampaignContainerBy(campaignName)));
		StepInfo.addMessage("Verify the Campaign Name adding successfully, the Campaign name: "
				+ campaignName);
		assertTrue(isElementExisting(getEditCampainButton(campaign)));
		StepInfo.addMessage("Verify the Edit button existing and displaying for the added Campaign");
		assertTrue(isElementExisting(getAddModuleButton(campaign)));
		StepInfo.addMessage("Verify the Add Module button existing and displaying for the added Campaign");
		StepInfo.addMessage("Verify the Campaign adding successfully");
	}

	public void verifyImageModuleWithImageURL(Campaign campaign,
			ImageModule imageModule) {
		assertTrue(isElementExisting(getImageModuleWithImageURL(campaign,
				imageModule)));
		StepInfo.addMessage("Verify the Image Module with Image URL successfully on container page, the Image URL: "
				+ imageModule.getImageUrl());
	}

	public void verifyImageModuleWithLocalImage(Campaign campaign,
			ImageModule imageModule) {

		String localimagepath = imageModule.getUploadImagePath();
		int extIndex = localimagepath.lastIndexOf(".");
		String localimage = localimagepath.substring(
				localimagepath.lastIndexOf(File.separator) + 1, extIndex);

		boolean status = false;
		System.out.println(messageFormat(IMAGE_MODULE_LOCALURL,
				campaign.getCampaignName()));
		String containerlocalimage = findElement(
				messageFormat(IMAGE_MODULE_LOCALURL, campaign.getCampaignName()))
				.getAttribute("src");
		System.out.println(containerlocalimage);
		if (StringUtils.isNotBlank(containerlocalimage)
				&& containerlocalimage.contains(localimage)) {
			status = true;
		}
		assertTrue(status);

		StepInfo.addMessage("Verify image module with the local uploaded image successfully on Container Page, the Image : "
				+ containerlocalimage);
	}

	public boolean verifyAfterDeleteCampaign(Campaign campaign) {
		logger.debug("Do verify After Delete ImageModule");
		String campaignName = campaign.getCampaignName();
		boolean result = !isElementExisting(
				getSpecialCampaignContainerBy(campaignName), 2);
		StepInfo.addMessage("Verify the old Test Data of Campaign cleared successfully");
		return result;

	}

	/**
	 * get the Module Window
	 *
	 * @return
	 */

	public WebElement getModuleWindow() {

		return findElement(MODULE_POPUP_DIV);

	}

	/**
	 * Get the xpath of product module title
	 *
	 * @param campaignName
	 *            use to locate the container
	 * @return product module title xpath
	 */
	public By getXpathOfProductModuleTitle(String campaignName) {
		return messageFormat(PRODUCT_MODULE_TITLE, campaignName);
	}

	/**
	 * Get the product module title
	 *
	 * @param campaignName
	 *            use to locate the container
	 * @return product module title
	 */
	public String getProductModuleTitle(String campaignName) {
		return findElement(getXpathOfProductModuleTitle(campaignName))
				.getText();
	}

	/**
	 * Get the xpath of productID in product module
	 *
	 * @param campaignName
	 *            use to locate the container
	 * @return productID xpath in product module
	 */
	public By getXpathOfProductURL(String campaignName) {
		return messageFormat(PRODUCT_MODULE_ID, campaignName);
	}

	/**
	 * Get the product module title
	 *
	 * @param campaign
	 *            use to locate the container
	 * @return product module title
	 */
	public List<String> getProductModuleID(Campaign campaign) {
		String campaignName = campaign.getCampaignName();

		List<String> proURLs = new ArrayList<>();
		List<WebElement> productURLs = findElements(getXpathOfProductURL(campaignName));
		for (WebElement productURL : productURLs) {
			String proURL = productURL.getText();
			proURLs.add(proURL);
		}
		return proURLs;

	}

	/**
	 * if Added Module is not show up after add this Module , refresh this page
	 * 
	 * @param productModule
	 *            productModule
	 * @param campaign
	 * @return
	 */
	public boolean needRefreshPage(Campaign campaign) {
		return !isElementExistingAndDisplayed(
				getXpathOfProductModuleTitle(campaign.getCampaignName()),
				THIRTY_SECONDS);
	}

	/**
	 * Verify the product module title after add product module
	 */
	public void verifyAfterAddProductModuleWithModuleTitle(
			ProductModule productModule, Campaign campaign) {
		assertTrue(getProductModuleTitle(campaign.getCampaignName()).equals(
				productModule.getTitle()));
		StepInfo.addMessage("Verify the product module with product module title successfully on container page");
	}

	/**
	 * Verify the imageURL in Image module
	 */
	public void verifyAfterAddImageModule(Campaign campaign,
			ImageModule imageModule) {
		if (imageModule.getImageType() == ImageUploadTypeEnum.IMAGELOCAL) {
			// TODO add
			verifyImageModuleWithLocalImage(campaign, imageModule);
			logger.error("33333333333333333333333333333333333333333333");
		} else if (imageModule.getImageType() == ImageUploadTypeEnum.IMAGEURL) {
			verifyImageModuleWithImageURL(campaign, imageModule);
			logger.error("4444444444444444444444444444444444444444444");

		} else {
			logger.error("Wrong upload Type : only IMAGELOCAL & IMAGEURL");
		}

	}

	/**
	 * Verify the productURL in product module after add product module
	 */
	public void verifyAfterAddProductModuleWithProductURL(
			ProductModule productModule, Campaign campaign) {

		List<Product> products = productModule.getProducts();
		System.out.println(products);
		List<String> proURLs = new ArrayList<>();
		proURLs = getProductModuleID(campaign);
		for (int i = 0; i < proURLs.size(); i++) {
			String proURL = proURLs.get(i);

			String newProURL = proURL.substring(proURL.indexOf("/"),
					proURL.indexOf("?"));
			System.out.println(newProURL);

			boolean status = false;

			for (Product product : products) {
				String url = product.getUrl();
				if (StringUtils.isNotBlank(url) && url.contains(newProURL)) {
					status = true;
				}

			}
			assertTrue(status);
		}
		StepInfo.addMessage("Verify the product module with product URLs successfully on container page");
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return null;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return false;
	}

}
