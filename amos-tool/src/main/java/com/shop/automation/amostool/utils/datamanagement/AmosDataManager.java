package com.shop.automation.amostool.utils.datamanagement;

import com.marketamerica.automation.utilities.enums.Environment;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Locale;

/**
 * Created by Bruce.zhang on 8/26/2015.
 */
public abstract class AmosDataManager {
    protected final Logger logger = LogManager
            .getLogger(getClass());

    private Environment environment;
    private Locale locale;

    public AmosDataManager(final Environment environment, final Locale locale) {
        this.environment = environment;
        this.locale = locale;
    }

    private String formatString(String string) {
        return string.toLowerCase().replaceAll(" ", "")
                .replaceAll("_", "");
    }

    /**
     * This method determines where our data is located based on the environment
     * . The returned path is in the following format:
     * "/"${environment}/"
     *
     * @return
     */
    protected String getPathToDataByEnvironment() {
        String formattedEnvironment = formatString(environment.toString());

        if (formattedEnvironment.equals("proof_of_concept")
                || formattedEnvironment.equals("poc")) {
            formattedEnvironment = "staging";
        }

        return String.format("/testdata/%s/",
                formattedEnvironment);
    }

    /**
     * @return returns the current locale being used by the Data Manager
     */
    public Locale getLocale() {
        return locale;
    }


}
