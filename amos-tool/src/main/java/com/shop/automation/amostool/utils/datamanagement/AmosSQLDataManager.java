package com.shop.automation.amostool.utils.datamanagement;

import com.marketamerica.automation.utilities.database.TDCRSDatabase;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automationframework.tools.datamanagement.DataManager;

import java.util.Locale;

/**
 * Created by bruce.zhang on 07/03/2015.
 */
public class AmosSQLDataManager extends DataManager {

	private TDCRSDatabase tdcrsDatabase;

    public AmosSQLDataManager(Environment environment, Locale locale) {
        super(environment, locale);
    }
    
    public TDCRSDatabase getTDCRSDatabase() {
    	return tdcrsDatabase;
    }
     
}
