package com.shop.automation.amostool.seleninum.testdata;

import com.marketamerica.automation.testdatalists.AbstractDataList;

import java.util.Iterator;
import java.util.List;

/**
 * Store ImageList data from Excel
 *
 * Created by bruce.zhang on 8/27/15.
 */
public class ImageList extends AbstractDataList implements Iterable<Image>{
    @SuppressWarnings("unchecked")
    public List<Image> getImages() {
        return (List<Image>) this.getTestDataList();
    }

    public Image getImage(String id) {
        return (Image) super.getTestData(id);
    }

    @Override
    public Iterator<Image> iterator() {
        @SuppressWarnings("unchecked")
        Iterator<Image> imageIterator = (Iterator<Image>) this.testDataList.iterator();
        return imageIterator;
    }
}
