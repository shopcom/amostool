package com.shop.automation.amostool.seleninum.testdata;

import com.marketamerica.automation.testdatalists.AbstractDataList;

import java.util.Iterator;
import java.util.List;

/**
 * Store ProductModuleList data from Excel
 *
 * Created by bruce.zhang on 9/01/15.
 */
public class ProductModuleList extends AbstractDataList implements Iterable<ProductModule> {

    @SuppressWarnings("unchecked")
    public List<ProductModule> getProductModules() {
        return (List<ProductModule>) this.getTestDataList();
    }

    public ProductModule getProductModule(String id) {
        return (ProductModule) super.getTestData(id);
    }

    @Override
    public Iterator<ProductModule> iterator() {
        @SuppressWarnings("unchecked")
        Iterator<ProductModule> productModuleIterator = (Iterator<ProductModule>) this.testDataList.iterator();
        return productModuleIterator;
    }
}