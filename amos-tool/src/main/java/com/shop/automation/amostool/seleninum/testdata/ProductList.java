package com.shop.automation.amostool.seleninum.testdata;

import com.marketamerica.automation.testdatalists.AbstractDataList;

import java.util.Iterator;
import java.util.List;

/**
 * Store ProductList data from XML
 *
 * Created by bruce.zhang on 8/27/15.
 */
public class ProductList extends AbstractDataList implements Iterable<Product>{
    @SuppressWarnings("unchecked")
    public List<Product> getProducts() {
        return (List<Product>) this.getTestDataList();
    }

    public Product getProduct(String id) {
        return (Product) super.getTestData(id);
    }

    @Override
    public Iterator<Product> iterator() {
        @SuppressWarnings("unchecked")
        Iterator<Product> productIterator = (Iterator<Product>) this.testDataList.iterator();
        return productIterator;
    }
}
