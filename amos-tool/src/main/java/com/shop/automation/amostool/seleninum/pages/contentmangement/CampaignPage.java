package com.shop.automation.amostool.seleninum.pages.contentmangement;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.shop.automation.amostool.seleninum.pages.base.AmosPage;
import com.shop.automation.amostool.seleninum.testdata.Campaign;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CampaignPage extends AmosPage {

    public CampaignPage(WebDriver driver, Translator translator, ElementDefinitionManager elementDefinitionManager) {
        super(driver, translator, elementDefinitionManager);
    }

    public static final By pageTitle = By.id("header");
    public static final By compaginName = By.xpath(".//input[@id='campaignName']");
    public static final By startDate = By.id("startDate");
    public static final By doneButtonINDatePicker = By.xpath(".//*[@id='ui-datepicker-div']//button[2]");
    public static final By endDate = By.id("endDate");
    public static final By showOnDesktop = By.id("showOnDesktop");
    public static final By showOnMobile = By.id("showOnMobile");
    public static final By deleteCompagin = By.id("containerCancel");
    public static final By save = By.id("containerSave");
    public static final By close = By.xpath(".//*[@id='containerDiv']/a']");


    /**
     * Get each element
     *
     * @return
     */
    private WebElement getPageTitle() {
        return findElement(pageTitle);
    }

    private WebElement getCompaginName() {
        return findElement(compaginName);
    }

    private WebElement getStartDate() {
        return findElement(startDate);
    }

    private WebElement getEndDate() {
        return findElement(endDate);
    }

    private WebElement getShowOnDesktop() {
        return findElement(showOnDesktop);
    }

    private WebElement getShowOnMobile() {
        return findElement(showOnMobile);
    }

    private WebElement getDeleteCompagin() {
        return findElement(deleteCompagin);
    }

    private WebElement getSave() {
        return findElement(save);
    }

    private WebElement getClose() {
        return findElement(close);
    }

    private WebElement getChooseDateDone() {
        return findElement(doneButtonINDatePicker);
    }

    public void deleteCampaign() {
        getDeleteCompagin().click();
    }

    /**
     * Fill in the compaign name
     *
     * @param campaign containing CompaginName
     */
    public void setCampaignName(Campaign campaign) {
        StepInfo.addMessage("Enter Campaign name: " + campaign.getCampaignName());
        getCompaginName().sendKeys(campaign.getCampaignName());
    }

    /**
     * fill up all form with prepared data & click save button
     *
     * @param campaign imageMoudle data
     */
    public void createNewCampaign(Campaign campaign) {
        StepInfo.addMessage("Create a new Campaign");
        waitForElementToBeClickable(startDate);
        setCampaignName(campaign);
        clickStartDate();
        //if Date picker not clicked then click again
        if (!isElementExistingAndDisplayed(doneButtonINDatePicker, TEN_SECONDS)) {
            logger.warn("Date Picker is not showing up, click the start Date input again.");
            clickStartDate();
        }
        waitForElementToBeClickable(doneButtonINDatePicker, true, THIRTY_SECONDS);
        WebElement dateChooseButton = getChooseDateDone();
        dateChooseButton.click();
        getSave().click();
        //confirm Save action after click Save Button
        alertAccept();
        //Save Successfully
        alertAccept();
        //wait for loading disappear
        waitForLoadingFinish();
        StepInfo.addMessage("Create a new Campaign Successfully");

    }

    /**
     * Click the "StartDate" box on
     * the Campagin page
     */
    public void clickStartDate() {
        getStartDate().click();
    }

    @Override
    protected By getBody() throws UnableToVerifyPagesExistenceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean atPage() throws UnableToVerifyPagesExistenceException {
        // TODO Auto-generated method stub
        return false;
    }
}