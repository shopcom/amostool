package com.shop.automation.amostool.seleninum.pages.contentmangement;




import java.util.concurrent.TimeUnit;

import com.marketamerica.automationframework.tools.reporting.StepInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.shop.automation.amostool.seleninum.pages.base.AmosPage;
import com.shop.automation.amostool.seleninum.testdata.ImageModule;

/**
 * Image Module Page
 * 
 * @author Dawn.Fu
 */
public class ImageModulePage extends AmosPage{ 
	
	private static final By TAB1 = By.id("tab1");
	private static final By TAB2 = By.id("tab2");
	
	private static final By RADIO_ENTER_IMAGE = By.id("enterImage");
	private static final By RADIO_UPLOAD_IMAGE = By.id("uploadImage");
	
	private static final By CHECKBOX_OPEN_LINK = By.id("openLink");
	
	private static final By INPUT_URL = By.id("url");
	private static final By INPUT_ALT = By.id("alt");
	private static final By INPUT_PADDING_TOP = By.id("paddingTop");
	private static final By INPUT_PADDING_BOTTOM = By.id("paddingBottom");
	private static final By INPUT_PATH = By.id("uploadImageUrl");
	private static final By INPUT_LINK = By.id("link");
	
	private static final By HIDDEN_IMAGE_SPEC_ID = By.id("imageSpecId");
	private static final By UPLOAD_IMAGE_BUTTON = By.id("uploadImageButton");
	private static final By SAVE_INSERTIMAGE = By.id("insertImage");
	private static final String  TYPE_IMG_URL = "IMAGEURL";
	private static final String  TYPE_IMG_UPLOAD_PATH = "IMAGELOCAL";

	/**
	 * Super constructor
	 * @param driver
	 * @param translator
	 * @param elementDefinitionManager
	 */
	public ImageModulePage(WebDriver driver, Translator translator, ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}
	
	/**
	 * Fill up form with data
	 * @param data
	 */
	public void createImageModule(WebElement element, ImageModule data) {
		StepInfo.addMessage("Create Image Module");
		if(TYPE_IMG_URL.equalsIgnoreCase(data.getImageType().getValue())){
			StepInfo.addMessage("Enter Image URL: " + data.getImageUrl() );

			radioEnterImage(element).click();
			inputUrl(element).sendKeys(data.getImageUrl());
			
			
		} else if(TYPE_IMG_UPLOAD_PATH.equalsIgnoreCase(data.getImageType().getValue())){
			StepInfo.addMessage("Upload Image from Local");

			radioUploadImage(element).click();
			inputPath(element).sendKeys(data.getUploadImagePath());
			
			uploadImageButton(element).click();
			alertAccept();
			waitForjQueryAjax("Upload a local image");
			waitForLoadingFinish();
		}
		StepInfo.addMessage("Enter Alt Tag: " + data.getAlt());
		inputAlt(element).sendKeys(data.getAlt());
 		inputPaddingTop(element).sendKeys(data.getMarginTop());
 		inputPaddingBottom(element).sendKeys(data.getMarginBottom());
 		tab2(element).click();
 		StepInfo.addMessage("Enter Link URL: " + data.getLinkUrl());
 		inputLink(element).sendKeys(data.getLinkUrl());
 		if (data.isOpenLinkInNewWindow()) {
 			checkboxOpenLink(element).click();
 		}
 		waitForPageLoaingFinish();
		saveInsertimage(element).click();
		//confirm alert
		alertAccept();

		//confirm alert
		alertAccept();

		//waiting for Loading Finish
		waitForLoadingFinish();
		StepInfo.addMessage("Create Image Module Successfully");

	}

	public WebElement tab1(WebElement element) {
		return element.findElement(TAB1);
	}
	
	public WebElement tab2(WebElement element) {
		return element.findElement(TAB2);
	}
	
	public WebElement radioEnterImage(WebElement element) {
		return element.findElement(RADIO_ENTER_IMAGE);
	}
	
	public WebElement radioUploadImage(WebElement element) {
		return element.findElement(RADIO_UPLOAD_IMAGE);
	}
	
	public WebElement checkboxOpenLink(WebElement element) {
		return element.findElement(CHECKBOX_OPEN_LINK);
	}
	
	public WebElement inputUrl(WebElement element) {
		return element.findElement(INPUT_URL);
	}
	
	public WebElement inputAlt(WebElement element) {
		return element.findElement(INPUT_ALT);
	}
	
	public WebElement inputPaddingTop(WebElement element) {
		return element.findElement(INPUT_PADDING_TOP);
	}
	
	public WebElement inputPaddingBottom(WebElement element) {
		return element.findElement(INPUT_PADDING_BOTTOM);
	}
	
	public WebElement inputPath(WebElement element) {
		return element.findElement(INPUT_PATH);
	}
	
	public WebElement inputLink(WebElement element) {
		return element.findElement(INPUT_LINK);
	}
	
	public WebElement hiddenImageSpecId(WebElement element) {
		return element.findElement(HIDDEN_IMAGE_SPEC_ID);
	}
	
	public WebElement uploadImageButton(WebElement element) {
		return element.findElement(UPLOAD_IMAGE_BUTTON);
	}
	
	public WebElement saveInsertimage(WebElement element) {
		return element.findElement(SAVE_INSERTIMAGE);
	}
	
	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return null;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return false;
	}
}
