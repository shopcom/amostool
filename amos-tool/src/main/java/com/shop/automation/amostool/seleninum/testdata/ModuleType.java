package com.shop.automation.amostool.seleninum.testdata;

/**
 * Choose ModuleType when Added Campaign
 *
 * Created by bruce.zhang on 9/2/15.
 */
public enum ModuleType {
    //Image Module
    IMAGE(-2),

    //Product Module
    PRODUCT(1),

    //Hot Selling Module
    HOTSELLING(282),

    //Band Module
    BRAND(260),

    //BandLogo Module
    BRANDLOGO(222),

    //Trend Module
    TREND(261),

    //HERO Module
    HERO(257);

    private int code;

    ModuleType(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }
}
