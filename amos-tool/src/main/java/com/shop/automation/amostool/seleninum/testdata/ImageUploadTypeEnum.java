package com.shop.automation.amostool.seleninum.testdata;

import java.util.Enumeration;

/**
 * Image Source 3 types
 * imageUrl | local image path | image from paypal script
 * <p/>
 * Created by bruce.zhang on 9/1/15.
 */
public enum ImageUploadTypeEnum {
    IMAGEURL("IMAGEURL"),
    IMAGELOCAL("IMAGELOCAL"),
    IMAGEPAYPAL("IMAGEPAYPAL");

    private final String value;

    private ImageUploadTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public static void main(String[] args) {
        Enumeration<String> str = new Enumeration<String>() {
            @Override
            public boolean hasMoreElements() {
                return false;
            }

            @Override
            public String nextElement() {
                return null;
            }
        };

    }

}
