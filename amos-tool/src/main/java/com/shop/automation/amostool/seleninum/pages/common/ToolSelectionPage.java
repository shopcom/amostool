package com.shop.automation.amostool.seleninum.pages.common;

import static org.testng.AssertJUnit.assertTrue;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.shop.automation.amostool.seleninum.pages.base.AmosPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

/**
 * Created by bruce.zhang on 8/24/15.
 */
public class ToolSelectionPage extends AmosPage {
    private static final By merchantTool = By.name("field_merchant_tool");
    private static final By customerTool = By.name("field_customer_tool");
    private static final By adminTool = By.name("field_admin_tool");
    private static final By toolInDevelopment = By.name("field_internal_tool");
    private static final By merchantGo = By.name("merchant_go");
    private static final By customerGo = By.name("customer_go");
    private static final By adminGo = By.name("admin_go");
    private static final By toolInDevelopmentGo = By.name("internal_go");


    private static final String CONTENT_MANAGEMENT_TOOL = "Content Management Tool";
    private static  Select dropDown ;
    
    /* Body of the ToolSelectionPage*/
    private static final By BodylField = By.id("mn_cnt_tb");

    public ToolSelectionPage(WebDriver driver, Translator translator, ElementDefinitionManager elementDefinitionManager) {
        super(driver, translator, elementDefinitionManager);
    }

    @Override
    protected By getBody() throws UnableToVerifyPagesExistenceException {
        return null;
    }

    @Override
    public boolean atPage() throws UnableToVerifyPagesExistenceException {
        return false;
    }

    public WebElement getMerchantTool() {
        return findElement(merchantTool);
    }

    public WebElement getCustomerTool() {
        return findElement(customerTool);
    }

    public WebElement getAdminTool() {
        return findElement(adminTool);
    }

    public Select getToolInDevelopment() {
        return new Select(findElement(toolInDevelopment));
    }

    /**
     * Go to Content Management Tool page.
     */
    public void goToContentManagementTool(){
        StepInfo.addMessage("Go to CMT Tool Page");

        Select cmt = getToolInDevelopment();
        //select CMT tool
        cmt.selectByVisibleText(CONTENT_MANAGEMENT_TOOL);
        //Click Go Button
        findElement(toolInDevelopmentGo).click();
    }

    public WebElement getBodyElement(){
    	return findElement(BodylField);
    }
    
    /**
     * Verify the Body should display after login successfully
     */
    public void verifyBodyExisted(){
        assertTrue(isElementDisplayed(getBodyElement()));
    }
}
