package com.shop.automation.amostool.seleninum.pages.common;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.shop.automation.amostool.seleninum.pages.base.AmosPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by bruce.zhang on 8/24/15.
 */
public class LoginPage extends AmosPage {

	private static final By emailField = By.name("field_email");
	private static final By passwordField = By.name("field_password");
	private static final By siginButton = By.xpath("//div [@class='clink']/a");
	// email account: automationtest2016@gmail.com/testing2016
	private static final String USERNAME = "automationtest2016@gmail.com";
	private static final String PASSWORD = "Sh*p2016";

	public LoginPage(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}

	/**
	 * Fill up username & password click signIn
	 */
	public void login() {
		StepInfo.addMessage("Navigate to Amos Tool login page");
		StepInfo.addMessage("Sign in the Amos tool");

		this.waitForjQueryAjax("Waiting for page to load");
		setEmailAddress(USERNAME);
		setPasswordField(PASSWORD);

		// click signIn button to Login
		getSignInButton().click();
		StepInfo.addMessage("Sign in Amos tool successfully ");
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return null;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return false;

	}

	private void setEmailAddress(final String email) {
		getEmailField().sendKeys(email);
	}

	private void setPasswordField(final String password) {
		getPasswordField().sendKeys(password);
	}

	public WebElement getEmailField() {
		return findElement(emailField);
	}

	public WebElement getPasswordField() {
		return findElement(passwordField);
	}

	public WebElement getSignInButton() {
		return findElement(siginButton);
	}

}
