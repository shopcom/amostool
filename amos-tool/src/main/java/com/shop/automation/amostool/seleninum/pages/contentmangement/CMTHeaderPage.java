package com.shop.automation.amostool.seleninum.pages.contentmangement;

import com.marketamerica.automationframework.tools.reporting.StepInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.shop.automation.amostool.seleninum.pages.base.AmosPage;

public class CMTHeaderPage extends AmosPage {
	public CMTHeaderPage(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}

	public static final By selectPageField = By.id("pageGroup");
	public static final By selectSiteField = By.id("siteGroup");
	public static final By selectSiteField2 = By.id("siteGroup2");
	public static final By selectCountryField = By.id("country");
	public static final By previewButton = By.id("save_preview");
	public static final By saveButton = By.id("save_head");
	public static final By publishToStagingButton = By.id("staging");
	public static final By releaseButton = By.id("release");
	public static final By cancelButton = By.id("cancel");
	public static final By showLiveHomePageOption = By.id("liveDateTime");
	public static final By showOtherDateOption = By.id("otherDateTime");
	public static final By dateTimeViewPicker = By.id("dateTimeViewPicker");
	public static final By nowButtonINDatePicker = By
			.xpath(".//*[@id='ui-datepicker-div']//button[1]");
	public static final By doneButtonINDatePicker = By
			.xpath(".//*[@id='ui-datepicker-div']//button[2]");
	public static final By updateDateButton = By.id("updateDateTime");
	public static final String selectedHomePage = "Home Page";
	public static final String selectedFullSite = "Full Site";
	public static final String selectedShopCom = "SHOP.COM";
	public static final String selectTestCountry = "Test";
	public static final String selectEmptyCountry = "Select Country";

	/**
	 * Get each element
	 * 
	 * @return
	 */
	public WebElement getSelectePageField() {
		return findElement(selectPageField);
	}

	public WebElement getSelecteSiteField() {
		return findElement(selectSiteField);
	}

	public WebElement getSelecteSiteField2() {
		return findElement(selectSiteField2);
	}

	public WebElement getSelecteCountryField() {
		return findElement(selectCountryField);
	}

	/**
	 * Select each value for each drop-down field
	 */
	public void selectHomePage() {
		selectDropDownText(getSelectePageField(), selectedHomePage);
	}

	public void selectFullSite() {
		selectDropDownText(getSelecteSiteField(), selectedFullSite);
	}

	public void selectShopCom() {
		selectDropDownText(getSelecteSiteField2(), selectedShopCom);
	}

	public void selectTestCountry() {
		selectDropDownText(getSelecteCountryField(), selectTestCountry);
	}

	public void selectCountryRest() {
		selectDropDownText(getSelecteCountryField(), selectEmptyCountry);
	}

	/**
	 * Get Preview, Save, Publish to Staging, Release, Cancel button
	 */
	public WebElement getPreviewButton() {
		return findElement(previewButton);
	}

	public WebElement getSaveButton() {
		return driver.findElement(saveButton);
	}

	public WebElement getPublishToStagingButton() {
		return findElement(publishToStagingButton);
	}

	public WebElement getReleaseButton() {
		return findElement(releaseButton);
	}

	public WebElement getCancelButton() {
		return findElement(cancelButton);
	}

	/**
	 * Get Show Live HomePage, Show Other Date/Time option, DatePicker and
	 * Update Date Element
	 */

	public WebElement getShowLiveHomePageOption() {
		return findElement(showLiveHomePageOption);
	}

	public WebElement getShowOtherDateOption() {
		return findElement(showOtherDateOption);
	}

	public WebElement getDateTimeViewPicker() {
		return findElement(dateTimeViewPicker);
	}

	public WebElement getNowButtonINDatePicker() {
		return findElement(nowButtonINDatePicker);
	}

	public WebElement getDoneButtonINDatePicker() {
		return findElement(doneButtonINDatePicker);
	}

	public WebElement getUpdateDateButton() {
		return findElement(updateDateButton);
	}

	/**
	 * Select Homepage, Full Site, ShopCome, Test
	 */
	public void searchHomePageToolTestSite() {
		StepInfo.addMessage("Choose HomePage Tool --> Full Site --> Shop.Com -->Test Country");

		selectHomePage();
		selectFullSite();
		this.waitForElementToBeVisible(selectSiteField2, 10);
		selectShopCom();
		this.waitForElementToBeVisible(selectCountryField, 10);
		selectTestCountry();
		alertAccept();
		waitForLoadingFinish();

	}

	/**
	 * refresh HomePage SHOPCOM Test Country
	 */
	public void refreshHomePageTestCountry() {
		StepInfo.addMessage("Page encounters the shown problem, which needs to refresh page and rechoose HomePage Tool --> Full Site --> Shop.Com -->Test Country");

		selectCountryRest();
		selectTestCountry();
		alertAccept();
		waitForLoadingFinish();

		StepInfo.addMessage("Rechoose HomePage Tool --> Full Site --> Shop.Com -->Test Country Successfully");
	}

	public void preview() {
		StepInfo.addMessage("Preview the Home Page");
		this.waitForjQueryAjax("Waiting for All Campaigns loaded");
		getPreviewButton().click();
		waitForLoadingFinish();
	}

	/**
	 * select dropdown according to text
	 * 
	 * @param element
	 * @param dropDownText
	 */
	public void selectDropDownText(WebElement element, String dropDownText) {
		new Select(element).selectByVisibleText(dropDownText);
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return false;
	}

}
