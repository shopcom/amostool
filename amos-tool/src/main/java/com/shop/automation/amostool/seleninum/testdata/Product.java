package com.shop.automation.amostool.seleninum.testdata;

import com.marketamerica.automation.testdata.AbstractData;

/**
 * Store Product data from XML
 *
 * Created by bruce.zhang on 9/1/15.
 */
public class Product extends AbstractData {
    //product id
    private String proID;
    //product Name
    private String proName;
    //product url
    private String url;

    public String getProID() {
        return proID;
    }

    public void setProID(String proID) {
        this.proID = proID;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Product{");
        sb.append("proID='").append(proID).append('\'');
        sb.append(", proName='").append(proName).append('\'');
        sb.append(", url='").append(url).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
