package com.shop.automation.amostool.seleninum.testdata;

/**
 * Product Module 300 x 250 Banner Ad configuration
 * Upload as Video or Upload as Image
 * Created by bruce.zhang on 9/4/15.
 */
public enum UploadTypeEnum {

    IMAGE("image"),
    VIDOE("video");

    private String value;

    UploadTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}
