package com.shop.automation.amostool.seleninum.testdata;

import com.marketamerica.automation.testdata.AbstractData;

/**
 * Store Image data from XML
 *
 * Created by bruce.zhang on 8/27/15.
 */
public class Image extends AbstractData {
    private String imageUrl;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Override
    public String toString() {
        return "Image{" +
                "imageUrl='" + imageUrl + '\'' +
                "} " + super.toString();
    }
}
