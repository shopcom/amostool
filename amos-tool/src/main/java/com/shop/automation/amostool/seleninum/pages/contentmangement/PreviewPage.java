package com.shop.automation.amostool.seleninum.pages.contentmangement;

import static org.testng.AssertJUnit.assertTrue;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.shop.automation.amostool.seleninum.pages.base.AmosPage;
import com.shop.automation.amostool.seleninum.testdata.Campaign;
import com.shop.automation.amostool.seleninum.testdata.ImageModule;
import com.shop.automation.amostool.seleninum.testdata.ImageUploadTypeEnum;
import com.shop.automation.amostool.seleninum.testdata.Product;
import com.shop.automation.amostool.seleninum.testdata.ProductModule;
import com.shop.automation.amostool.seleninum.testdata.ProductModuleTypeEnum;
import com.shop.automation.amostool.seleninum.testdata.UploadTypeEnum;

public class PreviewPage extends AmosPage {

	public PreviewPage(WebDriver driver, Translator translator, ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
		// TODO Auto-generated constructor stub
	}

	public static final String PRODUCT_NAME = ".//*[@id=\"homepage\"]//h3/a[text()=\"%s\"]";
	public static final String PRODUCT_MODULE_NAME = ".//*[@id=\"homepage\"]//h2/div[text()=\"%s\"]";
	public static final String PRODUCT_PARTTERN = ".//a[@href=\"%s\"]";
	public static final String PRODUCT_MODULE_IMAGE = ".//*[@id=\"homepage\"]//aside/a/img[@src=\"%s\"]";
	public static final String PRODUCT_MODULE_LINKURL = ".//*[@id=\"homepage\"]//article/aside/a[@href=\"%s\"]";
	public static final String PRODUCT_MODULE_ALTTag = ".//*[@id=\"homepage\"]//img[@alt=\"%s\"]";

	public static final String imageMoudlePrePathWithImageURL = ".//*[@id=\"homepage\"]//img[@src=\"";
	public static final String imageMoudleAftPathWithImageURL = "\"]";

	public static final String imageMoudlePrePathWithAlt = ".//*[@id=\"homepage\"]//img[@alt=\"";
	public static final String imageMoudleAftPathWithAlt = "\"]";

	public static final String imagePrePathWitLinkURL = ".//*[@id=\"homepage\"]//a[@href=\"";
	public static final String imageAftPathWithLinkURL = "\"]";

	public static final String allImageModule = ".//div[@class='image-module']/a/img";

	public static final String localImageOfProductModule = ".//aside/a/img";


	/**
	 * Get All Image Module on Preview page
	 * 
	 * @return
	 */
	public By getAllImageModule() {
		return By.xpath(allImageModule);

	}

	/**
	 * Get All the Image Src for Image Module -> Local Image
	 */
	public List<String> getLocalImageSrcOfImageModule() {
		List<String> srcList = new ArrayList<String>();
		List<WebElement> elements = findElements(getAllImageModule());

		for (WebElement element : elements) {
			srcList.add(element.getAttribute("src"));
		}
		return srcList;
	}

	/**
	 * Verify with Image Src for Image Module -> Local Image
	 */
	public void verifyImageModuleWithLocalImage(ImageModule imageModule) {
		List<String> imagSrcList = getLocalImageSrcOfImageModule();

		String localimagepath = imageModule.getUploadImagePath();
		int extIndex = localimagepath.lastIndexOf(".");
		String localimage = localimagepath.substring(localimagepath.lastIndexOf(File.separator) + 1, extIndex);

		boolean status = false;
		for (String src : imagSrcList) {
			if (StringUtils.isNotBlank(src) && src.contains(localimage)) {
				status = true;
			}
		}
		assertTrue(status);

		StepInfo.addMessage("Verify the Image Module with the uploaded Local Image successfully on Preview Page");

	}

	/**
	 * Get All left local Image for product Module -> Local Image on Preview
	 * page
	 * 
	 * @return
	 */
	public By getLocalImageOfProductModule() {
		return By.xpath(localImageOfProductModule);

	}


	/**
	 * Get All the Image Src for Product Module -> Local Image
	 */
	public List<String> getLocalImageSrcOfProductModule(ProductModule productModule) {
		List<String> srcList = new ArrayList<String>();
		List<WebElement> elements = new ArrayList<WebElement>();

			elements = findElements(getLocalImageOfProductModule());
		for (WebElement element : elements) {
			srcList.add(element.getAttribute("src"));
		}
		return srcList;
	}

	/**
	 * Verify with Image Src for Product Module -> Local Image
	 */
	public void verifyProductModuleWithLocalImage(ProductModule productModule) {
		List<String> imagSrcList = getLocalImageSrcOfProductModule(productModule);

		String localimagepath = productModule.getUploadImagePath();
		int extIndex = localimagepath.lastIndexOf(".");
		String localimage = localimagepath.substring(localimagepath.lastIndexOf(File.separator) + 1, extIndex);

		System.out.println("localimage:" + localimage);

		boolean status = false;
		for (String src : imagSrcList) {
			if (StringUtils.isNotBlank(src) && src.contains(localimage)) {
				System.out.println("src:" + src);

				status = true;
			}
		}
		assertTrue(status);

		StepInfo.addMessage("Verify image module with the uploaded Image from local successfully on Preview Page");

	}

	/**
	 * Find Image Module with the ImageURL
	 *
	 * @return
	 */
	public By getImageModuleWithImageURL(String imageURL) {
		String url = imageMoudlePrePathWithImageURL + imageURL + imageMoudleAftPathWithImageURL;
		return By.xpath(url);

	}

	/**
	 * Find Image Module with the ImageURL
	 *
	 * @return
	 */
	public By getImageModuleWithALT(String imageAlt) {
		String url = imageMoudlePrePathWithAlt + imageAlt + imageMoudleAftPathWithAlt;
		System.out.println(url);
		return By.xpath(imageMoudlePrePathWithAlt + imageAlt + imageMoudleAftPathWithAlt);

	}

	/**
	 * Find Image Module with the LinkURL
	 *
	 * @return
	 */
	public By getImageWithLinkURL(String linkURL) {
		String url = imagePrePathWitLinkURL + linkURL + imageAftPathWithLinkURL;
		System.out.println(url);
		return By.xpath(imagePrePathWitLinkURL + linkURL + imageAftPathWithLinkURL);

	}

	/**
	 * Switch driver to the PreView window
	 */
	public void switchWindow() {
		// Get the current window handle
		String currentWindow = driver.getWindowHandle();

		Set<String> handles = driver.getWindowHandles();

		Iterator<String> it = handles.iterator();
		while (it.hasNext()) {

			if (currentWindow == it.next())
				continue;
			// Go the the new window handle
			driver.switchTo().window(it.next());
		}
	}

	/**
	 * Check whether can find Image Module with the ImageURL
	 *
	 * @return
	 */

	private void verifyImageModuleWithImageURL(String imageURL) {
		assertTrue(isElementExisting(getImageModuleWithImageURL(imageURL)));
		StepInfo.addMessage("Verify image module with the Image URL successfully on Preview Page, the Image URL: " + imageURL);
	}

	/**
	 * Check whether can find Image Module with the ImageAlt
	 *
	 * @return
	 */

	private void verifyWithImageAlt(String imageAlt) {
		assertTrue(isElementExisting(getImageModuleWithALT(imageAlt)));
		StepInfo.addMessage("Verify Image Module with Image ALT successfully on Preview Page, the Image ALT: " + imageAlt);
	}

	/**
	 * Check whether can find Image Module with the LinkURL
	 *
	 * @return
	 */

	private void verifyWithImageLinkURL(String linkURL) {
		assertTrue(isElementExisting(getImageWithLinkURL(linkURL)));
		StepInfo.addMessage("Verify Image Module with Link URL successfully on Preview Page, the Link URL: " + linkURL);
	}

	/**
	 * Check whether can find Product Module with the ImageURL
	 *
	 * @return
	 */

	private void verifyProductModuleWithImageURL(ProductModule product) {
		assertTrue(isElementExisting(messageFormat(PRODUCT_MODULE_IMAGE, product.getImageUrl())));
		StepInfo.addMessage("Verify Product module with the Image URL successfully on Preview Page, the Image URL: " + product.getImageUrl());
	}

	/**
	 * Check whether can find Product Module with the LinkURL
	 *
	 * @return
	 */

	private void verifyProductModuleWithLinkURL(ProductModule product) {

		assertTrue(isElementExisting(getImageWithLinkURL(product.getLinkUrl())));
		StepInfo.addMessage("Verify Product Module with Link URL successfully on Preview Page, the Link URL: " + product.getLinkUrl() );

	}

	/**
	 * Check whether can find Product Module with the AltTag
	 *
	 * @return
	 */

	private void verifyProductModuleWithAltTag(ProductModule product) {
		By by = messageFormat(PRODUCT_MODULE_ALTTag, StringUtils.isNotBlank(product.getAlt())?product.getAlt().trim():"");
		logger.warn("@@@@Product ALT:"+by);
		assertTrue(isElementExisting(by));
		StepInfo.addMessage("Verify Product module with the Alt Tag successfully on Preview Page, the Alt Tag: " + product.getAlt());

	}

	/**
	 * Check whether can find product Module with the product module name
	 *
	 * @return
	 */
	private void verifyProductModuleName(ProductModule productModule) {
		assertTrue(isElementExisting(messageFormat(PRODUCT_MODULE_NAME, productModule.getTitle())));
		StepInfo.addMessage("Verify the product module with product module name successfully on Preview page, the product module name: " + productModule.getTitle());
	}

	/**
	 * Check whether can find product module with the product name
	 *
	 * @param
	 */
	private void verifyProductName(ProductModule product) {
		List<Product> productNames = product.getProducts();
		List<String> results = new ArrayList<String>();
		for (Product prod : productNames) {
			String proname = prod.getProName();
			assertTrue(isElementExisting(messageFormat(PRODUCT_NAME, proname)));
			StepInfo.addMessage("Verify the product module with product name successfully on Preview page, product name: " + proname);
		}

	}

	/**
	 * Check whether can find product module with productURL
	 */
	private void verifyProductModuleWithProductURL(List<String> proURLs, Campaign campaign) {
		for (String proURL : proURLs) {
			String newProURL = proURL.substring(proURL.indexOf("/"));
			logger.debug("Product URL on Preview used to find element" + messageFormat(PRODUCT_PARTTERN, newProURL));
			assertTrue(isElementExisting(messageFormat(PRODUCT_PARTTERN, newProURL)));
			StepInfo.addMessage("Verify the product module with product URL successfully on Preview page, product URL: " + proURL);
			System.out.println("Verify the product module with product URL successfully on Preview page, product URL: " + proURL);

		}

	}

	/**
	 * Verify image module with image url,alt and linkurl
	 */
	public void verifyImageModule(ImageModule imageModule) {
		switchWindow();
		verifyWithImageAlt(imageModule.getAlt());
		verifyWithImageLinkURL(imageModule.getLinkUrl());
		if (imageModule.getImageType() == ImageUploadTypeEnum.IMAGELOCAL) {
			// TODO add
			verifyImageModuleWithLocalImage(imageModule);
		} else if (imageModule.getImageType() == ImageUploadTypeEnum.IMAGEURL) {
			verifyImageModuleWithImageURL(imageModule.getImageUrl());

		} else {
			logger.error("Wrong upload Type : only IMAGELOCAL & IMAGEURL");
		}

	}

	/**
	 * Verify product module with product module name, product name, product url
	 * and image url,alt and linkurl..
	 */
	public void verifyProductModule(ProductModule product, List<String> proURLs, Campaign campaign) {
		switchWindow();
		// waitForjQueryAjax("Waiting for preview page loading");
		getJavaScriptActions().waitForPageToRefresh();
		StepInfo.addMessage("Finish the preview page loading ");
		// TODO
		verifyProductModuleName(product);
		verifyProductName(product);
		verifyProductModuleWithProductURL(proURLs, campaign);

		if (product.getModuleType() == ProductModuleTypeEnum.MULT_80X80_300X250 && product.getUploadAsImageVideo() == UploadTypeEnum.IMAGE) {
			if (product.getUploadType() == ImageUploadTypeEnum.IMAGELOCAL) {
				verifyProductModuleWithLinkURL(product);
				verifyProductModuleWithAltTag(product);
				verifyProductModuleWithLocalImage(product);

			} else if (product.getUploadType() == ImageUploadTypeEnum.IMAGEPAYPAL) {
				// TODO
			} else if (product.getUploadType() == ImageUploadTypeEnum.IMAGEURL) {
				verifyProductModuleWithImageURL(product);
				verifyProductModuleWithLinkURL(product);
				verifyProductModuleWithAltTag(product);

			} else {
				logger.error("Wrong upload Type : only IMAGELOCAL & IMAGEPAYPA & IMAGEURL");
			}

		}

	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return false;
	}

	public static void main(String[] agrs) {
		String localimagepath = "c:\\zcm\\zc\\234234\\image.jpg";
		int extIndex = localimagepath.lastIndexOf(".");
		String localimage = localimagepath.substring(localimagepath.lastIndexOf(File.separator) + 1, extIndex);
		System.out.print(localimage);
	}

}
