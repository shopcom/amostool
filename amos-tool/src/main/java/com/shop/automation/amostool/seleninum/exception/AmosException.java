package com.shop.automation.amostool.seleninum.exception;

/**
 * Created by bruce.zhang on 9/4/15.
 */
public class AmosException extends Exception {
    public AmosException(String message) {
        super(message);
    }
}
