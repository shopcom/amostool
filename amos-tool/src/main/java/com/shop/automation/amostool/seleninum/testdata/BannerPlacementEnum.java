package com.shop.automation.amostool.seleninum.testdata;

/**
 * banner placement
 * left | right
 * Created by bruce.zhang on 9/4/15.
 */
public enum BannerPlacementEnum {

    LEFT("left"),
    RIGHT("right");

    private String value;

    BannerPlacementEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }


}
