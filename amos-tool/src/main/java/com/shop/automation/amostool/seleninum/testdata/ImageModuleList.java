package com.shop.automation.amostool.seleninum.testdata;

import com.marketamerica.automation.testdatalists.AbstractDataList;

import java.util.Iterator;
import java.util.List;

/**
 * Store ImageModuleList data from Excel
 *
 * Created by bruce.zhang on 8/27/15.
 */
public class ImageModuleList extends AbstractDataList implements Iterable<ImageModule> {

    @SuppressWarnings("unchecked")
    public List<ImageModule> getImageModules() {
        return (List<ImageModule>) this.getTestDataList();
    }

    public ImageModule getImageModule(String id) {
        return (ImageModule) super.getTestData(id);
    }

    @Override
    public Iterator<ImageModule> iterator() {
        @SuppressWarnings("unchecked")
        Iterator<ImageModule> imageModuleIterator = (Iterator<ImageModule>) this.testDataList.iterator();
        return imageModuleIterator;
    }
}