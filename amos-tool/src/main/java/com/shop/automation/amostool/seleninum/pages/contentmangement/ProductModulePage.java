package com.shop.automation.amostool.seleninum.pages.contentmangement;

import static org.testng.AssertJUnit.assertTrue;

import java.io.File;
import java.util.List;

import com.marketamerica.automationframework.tools.reporting.StepInfo;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.shop.automation.amostool.seleninum.pages.base.AmosPage;
import com.shop.automation.amostool.seleninum.testdata.ImageUploadTypeEnum;
import com.shop.automation.amostool.seleninum.testdata.Product;
import com.shop.automation.amostool.seleninum.testdata.ProductModule;
import com.shop.automation.amostool.seleninum.testdata.ProductModuleTypeEnum;
import com.shop.automation.amostool.seleninum.testdata.UploadTypeEnum;

/**
 * Product Module Page
 * 
 * @author Dawn.Fu
 */
public class ProductModulePage extends AmosPage {

	private static final String ADD_MORE_PROD = "addMoreProd";
	private static final String PROD = "prod";

	private static final By OMNI_CODE = By.id("omniCode");
	private static final By IFRAME_TEXT = By.id("textModuleContent_ifr");
	private static final By MODULE_TITLE = By
			.xpath(".//body[@id='tinymce']/div[1]");

	private static final By CHK_SAVE_PID = By.id("chkSavePid");
	private static final By PROMO_SET_SECONDARY_ID = By
			.id("promoSetSecondaryId");
	private static final By LOAD = By.id("load");
	private static final By SAVE = By.id("save");
	private static final By SAVE_PROD_IMAGE = By.id("saveProdImage");

	private static final By TAB1 = By.id("tab1");
	private static final By TAB2 = By.id("tab2");

	private static final By UPLOAD_AS_IMAGE = By.id("imageType");
	private static final By UPLOAD_AS_VIDEO = By.id("videoType");

	private static final By BANNER_PLACE = By.id("bannerPlace");
	private static final By NEW_BANNER = By.id("newBanner");
	private static final By NEW_PAYPAL = By.id("newPaypal");
	private static final By ENTER_IMG = By.id("enterImg");
	private static final By UPLOAD_IMG = By.id("uploadImg");
	private static final By URL = By.id("url");
	private static final By LINK = By.id("link");
	private static final By OPEN_LINK = By.id("openLink");
	private static final By ALT = By.id("alt");
	private static final By UPLOAD_IMAGE_URL = By.id("uploadImageUrl");
	private static final By SUBMIT_IMAGE = By.id("submitImage");
	private static final By PAYPAL_SCRIPT = By.id("paypalScript");
	private static final By VIDEO_URL = By.id("videoUrl");
	private static final By BANNER_PLACE_VIDEO = By.id("bannerPlaceVideo");
	private static final By PRODUCT_IDS = By.name("productIds");

	/**
	 * Super construct
	 * 
	 * @param driver
	 * @param translator
	 * @param elementDefinitionManager
	 */
	public ProductModulePage(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}

	/**
	 * Fill module form
	 * 
	 * @param modulWin
	 *            -- open module window
	 */
	public void saveModule(WebElement modulWin, ProductModule data) {
		StepInfo.addMessage("Fill up all feilds");

		JavascriptExecutor js = (JavascriptExecutor) driver;
		int scrollSlide = 0;

		setOmnicodeAndModuletitle(modulWin, js, data);
		if (data.getModuleType() == ProductModuleTypeEnum.MULT_80X80_300X250) {
			getTab1(modulWin).click();
			setBannerAdConfiguration(modulWin, js, data);
			getTab2(modulWin).click();
			scrollSlide = 1;
		}
		setProductConfig(modulWin, js, scrollSlide, data);
		if (data.getModuleType() == ProductModuleTypeEnum.MULT_80X80_300X250) {
			getSaveProdImage(modulWin).click();
		} else {
			getSave(modulWin).click();
		}
		alertAccept();
		waitForLoadingFinish();

		StepInfo.addMessage("Save the Product Module Successfully");
	}

	/**
	 * Enter Module Omni Code and Enter Module Title
	 * 
	 * @param modulWin
	 * @param js
	 * @param data
	 */
	private void setOmnicodeAndModuletitle(WebElement modulWin,
			JavascriptExecutor js, ProductModule data) {
		StepInfo.addMessage("Enter Module Omni Code: " + data.getOmniCode());

		getOmniCode(modulWin).sendKeys(data.getOmniCode());
		waitForElementToBeVisible(IFRAME_TEXT, TEN_SECONDS);
		isElementExisting(getIframeText(modulWin));
		WebElement iframElement = getIframeText(modulWin);

		driver.switchTo().frame(iframElement);
		StepInfo.addMessage("Enter Module Title: " + data.getTitle());
		js.executeScript("document.getElementById('tinymce').innerHTML='"
				+ data.getTitle() + "';");
		driver.switchTo().defaultContent();
	}

	/**
	 * Product Configuration, Enter Product IDs
	 * 
	 * @param modulWin
	 * @param js
	 * @param data
	 */
	private void setProductConfig(WebElement modulWin, JavascriptExecutor js,
			int scrollSlide, ProductModule data) {
		StepInfo.addMessage("Enter the products");

		if (data.isUseExistProductData()) {
			getChkSavePid(modulWin).click();
			getPromoSetSecondaryId(modulWin).sendKeys(
					data.getExistProductData());
			getLoad(modulWin).click();
			waitForjQueryAjax("Use Exist Product Data");
		} else {
			List<Product> products = data.getProducts();
			int pidSize = products.size(), productIds = getProductIDs(modulWin)
					.size();
			int i = (pidSize / productIds) + (pidSize % productIds > 0 ? 1 : 0);
			for (int j = 1; j < i; j++) {
				getAddMoreProd(modulWin, data).click();
			}
			String proid = "";
			for (int k = 0; k < pidSize; k++) {
				proid = PROD + (k + 1);
				if (!isElementDisplayed(By.id(proid))) {
					js.executeScript("$('.mCustomScrollbar').eq(" + scrollSlide
							+ ").mCustomScrollbar('scrollTo', '#" + proid
							+ "')");
				}
				modulWin.findElement(By.id(proid)).sendKeys(
						products.get(k).getUrl());
				StepInfo.addMessage("Enter the product: "
						+ products.get(k).getUrl());
				System.out.println("Enter the product: "
						+ products.get(k).getUrl());

			}
		}
	}

	/**
	 * Banner Ad Configuration
	 * 
	 * @param modulWin
	 * @param data
	 */
	private void setBannerAdConfiguration(WebElement modulWin,
			JavascriptExecutor js, ProductModule data) {

		if (UploadTypeEnum.IMAGE == data.getUploadAsImageVideo()) {

			if (isElementExisting(UPLOAD_AS_IMAGE)) {
				getUploadAsImage(modulWin).click();
			}
			uploadAsImage(modulWin, js, data);
		} else if (UploadTypeEnum.VIDOE == data.getUploadAsImageVideo()) {

			uploadAsVideo(modulWin, data);
		}
	}

	/**
	 * Upload as Image
	 * 
	 * @param modulWin
	 * @param data
	 */
	private void uploadAsImage(WebElement modulWin, JavascriptExecutor js,
			ProductModule data) {
		selectDropDownValueByPartialText(new Select(getBannerPlace(modulWin)),
				data.getBannerPlacement().getValue());
		if (ImageUploadTypeEnum.IMAGEPAYPAL.equals(data.getUploadType())) {
			uploadImageFromPaypalScript(modulWin, data);
		} else {

			getNewBanner(modulWin).click();
			if (ImageUploadTypeEnum.IMAGEURL.equals(data.getUploadType())) {
				uploadUsingUrl(modulWin, js, data);

			} else if (ImageUploadTypeEnum.IMAGELOCAL.equals(data
					.getUploadType())) {
				uploadALocalImage(modulWin, js, data);
			}
			js.executeScript("$('#scrollSlide').mCustomScrollbar('scrollTo', 'bottom')");
			getLink(modulWin).sendKeys(data.getLinkUrl());
			if (data.isOpenInNewWindow()) {
				getOpenLink(modulWin).click();
			}
			getAlt(modulWin).sendKeys(data.getAlt());
		}
	}

	/**
	 * Upload a local image
	 * 
	 * @param modulWin
	 * @param data
	 */
	private void uploadALocalImage(WebElement modulWin, JavascriptExecutor js,
			ProductModule data) {

		getUploadImg(modulWin).click();
		js.executeScript("$('#scrollSlide').mCustomScrollbar('scrollTo', 'bottom')");
		String uploadImagePath = data.getUploadImagePath();
		getUploadImagePath(modulWin).sendKeys(uploadImagePath);
		StepInfo.addMessage("Upload a local Image");
		js.executeScript("$('#scrollSlide').mCustomScrollbar('scrollTo', 'bottom')");

		getSubmitImage(modulWin).click();

		alertAccept();
		waitForjQueryAjax("Upload a local image with 300x250px");
		waitForLoadingFinish();
	}

	/**
	 * Upload using URL
	 * 
	 * @param modulWin
	 * @param data
	 */
	private void uploadUsingUrl(WebElement modulWin, JavascriptExecutor js,
			ProductModule data) {

		getEnterImg(modulWin).click();
		// if not displayed scroll down to that element
		if (!isElementDisplayed(URL)) {
			js.executeScript("$('#scrollSlide').mCustomScrollbar('scrollTo', '#url')");
		}
		StepInfo.addMessage("Upload using Image URL, the Image URL: "
				+ data.getImageUrl());
		getUrl(modulWin).sendKeys(data.getImageUrl());

	}

	/**
	 * Upload image from paypal script
	 * 
	 * @param modulWin
	 * @param data
	 */
	private void uploadImageFromPaypalScript(WebElement modulWin,
			ProductModule data) {
		getNewPaypal(modulWin).click();
		StepInfo.addMessage("Upload image from paypal script, the Paypal Script: "
				+ data.getPaypalScript());
		System.out.println("..........................."
				+ data.getPaypalScript());
		getPaypalScript(modulWin).sendKeys(data.getPaypalScript());
	}

	/**
	 * Upload as Video
	 * 
	 * @param modulWin
	 * @param data
	 */
	private void uploadAsVideo(WebElement modulWin, ProductModule data) {

		if (isElementExisting(UPLOAD_AS_VIDEO)) {
			getUploadAsVideo(modulWin).click();
			StepInfo.addMessage("Enter Video URL, the Video URL: "
					+ data.getVideoUrl());
			getVideoUrl(modulWin).sendKeys(data.getVideoUrl());
			selectDropDownValueByPartialText(new Select(
					getBannerPlaceVideo(modulWin)), data.getBannerPlacement()
					.getValue());
		} else {
			assertTrue(false);
			StepInfo.addMessage("Select Video no existed ");
		}
	}

	/**
	 * Enter Module Omni Code
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getOmniCode(WebElement element) {
		return element.findElement(OMNI_CODE);
	}

	/**
	 * Find Iframe to Enter Module Title
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getIframeText(WebElement element) {
		return element.findElement(IFRAME_TEXT);
	}

	/**
	 * Enter Module Title
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getModuleTitle(WebElement element) {
		return element.findElement(MODULE_TITLE);
	}

	/**
	 * Add more Products (button)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getAddMoreProd(WebElement element, ProductModule data) {
		return element.findElement(By.id(ADD_MORE_PROD
				+ data.getModuleType().getCode()));
	}

	/**
	 * Use existing product data (checkbox)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getChkSavePid(WebElement element) {
		return element.findElement(CHK_SAVE_PID);
	}

	/**
	 * Use existing product data (input)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getPromoSetSecondaryId(WebElement element) {
		return element.findElement(PROMO_SET_SECONDARY_ID);
	}

	/**
	 * Use existing product data (load button)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getLoad(WebElement element) {
		return element.findElement(LOAD);
	}

	/**
	 * Save module data button
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getSave(WebElement element) {
		return element.findElement(SAVE);
	}

	/**
	 * Save module data button from 80x80 Multi-Row with 300x250
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getSaveProdImage(WebElement element) {
		return element.findElement(SAVE_PROD_IMAGE);
	}

	/**
	 * Banner Ad Configuration (tab)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getTab1(WebElement element) {
		return element.findElement(TAB1);
	}

	/**
	 * Product Configuration (tab)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getTab2(WebElement element) {
		return element.findElement(TAB2);
	}

	/**
	 * Upload as Image (radio)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getUploadAsImage(WebElement element) {
		return element.findElement(UPLOAD_AS_IMAGE);
	}

	/**
	 * Upload as Video (radio)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getUploadAsVideo(WebElement element) {
		return element.findElement(UPLOAD_AS_VIDEO);
	}

	/**
	 * Upload as Image (radio) >> Banner Placement (select)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getBannerPlace(WebElement element) {
		return element.findElement(BANNER_PLACE);
	}

	/**
	 * Upload as Image (radio) >> Upload an image (radio)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getNewBanner(WebElement element) {
		return element.findElement(NEW_BANNER);
	}

	/**
	 * Upload as Image (radio) >> Upload Image from Paypal (radio)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getNewPaypal(WebElement element) {
		return element.findElement(NEW_PAYPAL);
	}

	/**
	 * Upload as Image (radio) >> Upload an image (radio) >> Enter Image URL
	 * (radio)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getEnterImg(WebElement element) {
		return element.findElement(ENTER_IMG);
	}

	/**
	 * Upload as Image (radio) >> Upload an image (radio) >> Upload an image
	 * (radio)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getUploadImg(WebElement element) {
		return element.findElement(UPLOAD_IMG);
	}

	/**
	 * Upload as Image (radio) >> Upload an image (radio) >> Enter Image URL
	 * (input)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getUrl(WebElement element) {
		return element.findElement(URL);
	}

	/**
	 * Upload as Image (radio) >> Upload an image (radio) >> Link URL (input)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getLink(WebElement element) {
		return element.findElement(LINK);
	}

	/**
	 * Upload as Image (radio) >> Upload an image (radio) >> Open link in a new
	 * Window/tab (checkbox)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getOpenLink(WebElement element) {
		return element.findElement(OPEN_LINK);
	}

	/**
	 * Upload as Image (radio) >> Upload an image (radio) >> Alt Tag (input)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getAlt(WebElement element) {
		return element.findElement(ALT);
	}

	/**
	 * Upload as Image (radio) >> Upload an image (radio) >> Upload an image
	 * (input file)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getUploadImagePath(WebElement element) {
		return element.findElement(UPLOAD_IMAGE_URL);
	}

	/**
	 * Upload as Image (radio) >> Upload an image (radio) >> Upload an image
	 * (radio) >> Upload Image (button)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getSubmitImage(WebElement element) {
		return element.findElement(SUBMIT_IMAGE);
	}

	/**
	 * Upload as Image (radio) >> Upload Image from Paypal (radio) >> Enter
	 * Paypal Script (textarea)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getPaypalScript(WebElement element) {
		return element.findElement(PAYPAL_SCRIPT);
	}

	/**
	 * Upload as Video (radio) >> Enter Video URL (input)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getVideoUrl(WebElement element) {
		return element.findElement(VIDEO_URL);
	}

	/**
	 * Upload as Video (radio) >> Banner Placement (select)
	 * 
	 * @param element
	 * @return
	 */
	public WebElement getBannerPlaceVideo(WebElement element) {
		return element.findElement(BANNER_PLACE_VIDEO);
	}

	/**
	 * Find product ids
	 * 
	 * @param element
	 * @return
	 */
	public List<WebElement> getProductIDs(WebElement element) {
		return element.findElements(PRODUCT_IDS);
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return null;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return false;
	}

}
