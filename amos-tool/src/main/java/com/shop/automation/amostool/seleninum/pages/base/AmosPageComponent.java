package com.shop.automation.amostool.seleninum.pages.base;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.pages.PageComponent;
import org.openqa.selenium.WebDriver;

/**
 *
 */
public class AmosPageComponent extends PageComponent {

    public AmosPageComponent(WebDriver driver, Translator translator, ElementDefinitionManager elementDefinitionManager) {
        super(driver, translator, elementDefinitionManager);
    }
}
