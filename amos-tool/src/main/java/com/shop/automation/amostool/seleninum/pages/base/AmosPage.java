package com.shop.automation.amostool.seleninum.pages.base;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.pages.Page;
import com.marketamerica.automationframework.tools.reporting.ScreenShooter;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.text.MessageFormat;
import java.util.List;

/**
 *
 */
public abstract class AmosPage extends Page {
    public static final int ZERO_SECOND = 0;
    public static final int TWO_SECONDS = 2;
    public static final int TWO_MILLI_SECONDS = 2000;
    public static final int FIVE_SECONDS = 5;
    public static final int TEN_SECONDS = 5;
    public static final int FIFTEEN_SECONDS = 15;
    public static final int THIRTY_SECONDS = 30;
    public static final int SIXTY_SECONDS = 60;
    public static final int TWO_MINS = 120;
    public static final int FIVE_MINS = 300;

    //40 seconds
    private static final int LOADING_WAIT_TIME = 40;

    //Loading Image Element
    public static final By LOADING_IMG = By.cssSelector(".loading>img");

    public AmosPage(final WebDriver driver, final Translator translator, ElementDefinitionManager elementDefinitionManager) {
        super(driver, translator, elementDefinitionManager);
    }

    /**
     * waiting for loading page finish
     */
    public void waitForLoadingFinish() {
        logger.info("Waiting for Loading Finish");
        this.waitForElementToBeInvisible(LOADING_IMG, SIXTY_SECONDS);
        try {
            logger.info("Thread sleep 2 seconds");
            Thread.sleep(TWO_MILLI_SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
            logger.error(e.getLocalizedMessage());
        }
        logger.info("Loading Finished");
    }

    public void waitForPageLoaingFinish(){
        getJavaScriptActions().waitForPageToRefresh();
    }

    /**
     * Accept Alert max wait time is  5 seconds
     */
    public void alertAccept() {
        final WebDriverWait wait = new WebDriverWait(driver, FIFTEEN_SECONDS);
        try {
            wait.until(ExpectedConditions.alertIsPresent());
        } catch (TimeoutException e) {
            final String message = String
                    .format("After %s seconds, alert could not be found",
                            FIVE_SECONDS);
            ScreenShooter.capture(driver);
            StepInfo.failTestWithoutScreenshot(message);
        }
        driver.switchTo().alert().accept();
    }

    /**
     * replace the original String with replacements
     *
     * @param originalStr original String
     * @param replacements one replacement string
     * @return By Element combined by Xpath
     */
    protected static By messageFormat(String originalStr, String... replacements) {
        return By.xpath(String.format(originalStr, replacements));
    }

}
