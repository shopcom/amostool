package com.shop.automation.amostool.seleninum.pages.contentmangement;

import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.shop.automation.amostool.seleninum.testdata.ProductModule;
import com.shop.automation.amostool.seleninum.testdata.ProductModuleTypeEnum;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.shop.automation.amostool.seleninum.pages.base.AmosPage;

/**
 * Product module select page.
 * 
 * @author Evan
 */
public class ProductModuleSelectPage extends AmosPage {
	
	private static final By CLOSE_BUTTON = By.cssSelector("#modulePopupDiv .close");
	private static final By CANCEL_BUTTON = By.id("cancelProduct");
	private static final By NEXT_BUTTON = By.id("nextStep");
	private static final By RADIO_80_80_MULTI_ROW = By.id("chooseProduct1");
	private static final By RADIO_80_80_MULTI_ROW_WITH_300_250 = By.id("chooseProduct2");
	private static final By RADIO_175_175_MULTI_ROW = By.id("chooseProduct3");
	private static final By RADIO_175_175_SINGLE_ROW_BY = By.id("chooseProduct4");	

	public ProductModuleSelectPage(WebDriver driver, Translator translator, ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);			
	}
	
	/**
	 * Close page.
	 */
	public void closePage(){		
		findElement(CLOSE_BUTTON).click();
	}	
	
	/**
	 * Cancel page.
	 */
	public void cancelPage(){		
		findElement(CANCEL_BUTTON).click();
	}	
	
	/**
	 * Check product module type with (80*80 multi row) then go to next step.
	 */
	private void selectProductModuleOne(){
		this.checkRadioOne();
		findElement(NEXT_BUTTON).click();
	}
	
	/**
	 * Check product module type with (80*80 multi row with 300*250) then go to next step.
	 */
	private void selectProductModuleTwo(){
		this.checkRadioTwo();
		findElement(NEXT_BUTTON).click();
	}
	
	/**
	 * Check product module type with (175*175 multi row) then go to next step.
	 */
	private void selectProductModuleThree(){
		this.checkRadioThree();
		findElement(NEXT_BUTTON).click();
	}
	
	/**
	 * Check product module type with (175*175 single row) then go to next step.
	 */
	private void selectProductModuleFour(){
		this.checkRadioFour();
		findElement(NEXT_BUTTON).click();
	}
	
	/**
	 * Check product module type with (80*80 multi row).
	 */
	private void checkRadioOne(){
		findElement(RADIO_80_80_MULTI_ROW).click();
	}
	
	/**
	 * Check product module type with (80*80 multi row with 300*250).
	 */
	private void checkRadioTwo(){
		findElement(RADIO_80_80_MULTI_ROW_WITH_300_250).click();
	}
	
	/**
	 * Check product module type with (175*175 multi row).
	 */
	private void checkRadioThree(){
		findElement(RADIO_175_175_MULTI_ROW).click();
	}
	
	/**
	 * Check product module type with (175*175 single row).
	 */
	private void checkRadioFour(){
		findElement(RADIO_175_175_SINGLE_ROW_BY).click();
	}

	/**
	 * Choose Product ModuleType
	 * @param productModule product Module
	 */
	public void chooseProductModuleByType(ProductModule productModule){
		if (ProductModuleTypeEnum.MULT_80X80 == productModule.getModuleType()) {
			StepInfo.addMessage("Choose Mulit 80x80 Product Module");

			selectProductModuleOne();
		} else if (ProductModuleTypeEnum.MULT_80X80_300X250 == productModule.getModuleType()) {
			StepInfo.addMessage("Choose MULT_80X80_300X250 Product Module");

			selectProductModuleTwo();
		} else if (ProductModuleTypeEnum.MULT_175X175 == productModule.getModuleType()) {
			StepInfo.addMessage("Choose MULT_175X175 Product Module");

			selectProductModuleThree();
		} else if (ProductModuleTypeEnum.SINGLE_175X175 == productModule.getModuleType()) {
			StepInfo.addMessage("Choose SINGLE_175X175 Product Module");

			selectProductModuleFour();
		} else {
			StepInfo.failTest("Wrong Product Module Type, only accept 1, 2, 3, 4");
			logger.error("Wrong Product Module Type, only accept 1, 2, 3, 4");
		}
	}
	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {		
		return null;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {		
		return false;
	}		
}
