package com.shop.automation.amostool.utils.datamanagement.dataprovider;

import com.shop.automation.amostool.utils.datamanagement.AmosXMLDataManager;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import java.lang.reflect.Method;
import java.util.List;

import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.getAddressKeys;

/**
 * Created by bruce.zhang on 8/27/15.
 */
public class AmosXMLDataDrivenProvider extends AmosAbstractDataProvider {
    private static final String IMAGE_MODULE_XML = "imageModule.xml";

    @DataProvider(name = "addresses")
    public static Object[][] getAddresses(final ITestContext context,
                                          final Method method) {

        // Create a data manager based on our current context
        final AmosXMLDataManager dataManager = createXMLDataManager(context);
        // Get the requested addresses by the DataDrivenTest annotation
        final List<String> requestedAddressKeys = getAddressKeys(method);
        // Return a list of addresses
//        final AddressesList requestedAddressList = dataManager.getAddressList();
//        // Get our requested Data
//        final Object[][] requestedAddresses = getRequestedData(
//                requestedAddressList, requestedAddressKeys);
//        if (requestedAddresses[0] == null) {
//            Assert.fail(String.format("Could not find any addresses for %s",
//                    method.getName()));
//        }
        return null;
    }
}
