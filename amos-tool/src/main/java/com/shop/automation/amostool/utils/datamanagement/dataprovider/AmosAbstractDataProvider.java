package com.shop.automation.amostool.utils.datamanagement.dataprovider;

import com.marketamerica.automationframework.tools.testmanagement.SuiteBuilder;
import com.marketamerica.automationframework.tools.testmanagement.TestBuilder;
import com.shop.automation.amostool.utils.datamanagement.AmosExcelDataManager;
import com.shop.automation.amostool.utils.datamanagement.AmosSQLDataManager;
import com.shop.automation.amostool.utils.datamanagement.AmosXMLDataManager;
import org.testng.ITestContext;

/**
 * This class represents an abstract data provider whose purpose is to return test data based
 * on the text context.
 * <p>
 * Currently it provides functionality to return data via Excel or XML
 */
public abstract class AmosAbstractDataProvider {
    /**
     * Create a AMOS XML Data Manager to return xml serialized test data
     *
     * @param context a TestNG context used to determine the test environment and test locale (country)
     * @return a AmosXMLDataManager for the given context
     */
    protected static AmosXMLDataManager createXMLDataManager(final ITestContext context) {
        final SuiteBuilder configuration = new SuiteBuilder(context);
        TestBuilder testBuilder = new TestBuilder(context);
        final AmosXMLDataManager dataManager = new AmosXMLDataManager(
                configuration.getEnvironment(),
                testBuilder.getLocale()
        );
        return dataManager;
    }

    /**
     * Create an AMOS Excel DataManager used to create ExcelWorkBookReader Objects on the fly based on the current test context
     *
     * @param context a TestNG context used to determine the test environment and test locale (country)
     * @return
     */
    protected static AmosExcelDataManager createExcelManager(final ITestContext context) {
        final SuiteBuilder configuration = new SuiteBuilder(context);
        TestBuilder testBuilder = new TestBuilder(context);
        final AmosExcelDataManager dataManager = new AmosExcelDataManager(
                configuration.getEnvironment(),
                testBuilder.getLocale()
        );
        return dataManager;
    }
    
    /**
     * Create a Amos SQL Database DataManager used to retrieve test data
     *
     * @param context a TestNG context used to determine the test environment and test locale (country)
     * @return a SQLDataManager for the given context
     */
    protected static AmosSQLDataManager createSQLDataManager(final ITestContext context) {
        final SuiteBuilder configuration = new SuiteBuilder(context);
        TestBuilder testBuilder = new TestBuilder(context);
        final AmosSQLDataManager dataManager = new AmosSQLDataManager(
                configuration.getEnvironment(),
                testBuilder.getLocale()
        );
        return dataManager;
    }

}
