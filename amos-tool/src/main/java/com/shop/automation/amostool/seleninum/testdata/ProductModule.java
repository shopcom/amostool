package com.shop.automation.amostool.seleninum.testdata;

import com.marketamerica.automation.testdata.AbstractData;

import java.util.ArrayList;
import java.util.List;

/**
 * Store ProductModule data from Excel
 *
 * Created by bruce.zhang on 9/1/15.
 */
public class ProductModule extends AbstractData {
    /**
     * campaign Name
     */
    private String campaignName;
    /**
     * module type : 4 types
     */
    private ProductModuleTypeEnum moduleType;
    /**
     * omniCode
     */
    private String omniCode;

    /**
     * Product Module Title
     */
    private String title;
    /**
     * product ids more than 6 items
     */
    private List<Product> products;
    /**
     * use existed product Data or not
     */
    private boolean useExistProductData;
    /**
     * input exist product data when useExistProductData is true
     */
    private String existProductData;
    /**
     * "IMAGE or VIDEO
     */
    private UploadTypeEnum uploadAsImageVideo;
    /**
     * left or right
     */
    private BannerPlacementEnum bannerPlacement;
    /**
     * upload using URL 'IMAGEURL', Upload a local image , upload image from paypal script
     */
    private ImageUploadTypeEnum uploadType;
    /**
     * exist image url
     */
    private String imageUrl;
    /**
     * local image path
     */
    private String uploadImagePath;
    /**
     * upload image using paypal script
     */
    private String paypalScript;
    /**
     * image link url
     */
    private String linkUrl;
    /**
     * open the link in a new window or not
     */
    private boolean openInNewWindow;
    /**
     * alt
     */
    private String alt;
    /**
     * font Family
     */
    private String fontFamily;
    /**
     * font Size
     */
    private String fontSize;
    /**
     * custom_Format
     */
    private String custom_Format;
    /**
     * upload youtube video URL
     */
    private String videoUrl;

    public String getCampaignName() {
        return campaignName;
    }

    public void setCampaignName(String campaignName) {
        this.campaignName = campaignName;
    }

    public String getPaypalScript() {
        return paypalScript;
    }

    public void setPaypalScript(String paypalScript) {
        this.paypalScript = paypalScript;
    }

    public ProductModuleTypeEnum getModuleType() {
        return moduleType;
    }

    public void setModuleType(ProductModuleTypeEnum moduleType) {
        this.moduleType = moduleType;
    }

    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    public String getFontSize() {
        return fontSize;
    }

    public void setFontSize(String fontSize) {
        this.fontSize = fontSize;
    }

    public String getCustom_Format() {
        return custom_Format;
    }

    public void setCustom_Format(String custom_Format) {
        this.custom_Format = custom_Format;
    }

    public List<Product> getProducts() {
        if (products == null) {
            products = new ArrayList<Product>();
        }
        return products;
    }

    public String getOmniCode() {
        return omniCode;
    }

    public void setOmniCode(String omniCode) {
        this.omniCode = omniCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isUseExistProductData() {
        return useExistProductData;
    }

    public void setUseExistProductData(boolean useExistProductData) {
        this.useExistProductData = useExistProductData;
    }

    public String getExistProductData() {
        return existProductData;
    }

    public void setExistProductData(String existProductData) {
        this.existProductData = existProductData;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public UploadTypeEnum getUploadAsImageVideo() {
        return uploadAsImageVideo;
    }

    public void setUploadAsImageVideo(UploadTypeEnum uploadAsImageVideo) {
        this.uploadAsImageVideo = uploadAsImageVideo;
    }

    public BannerPlacementEnum getBannerPlacement() {
        return bannerPlacement;
    }

    public void setBannerPlacement(BannerPlacementEnum bannerPlacement) {
        this.bannerPlacement = bannerPlacement;
    }

    public ImageUploadTypeEnum getUploadType() {
        return uploadType;
    }

    public void setUploadType(ImageUploadTypeEnum uploadType) {
        this.uploadType = uploadType;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUploadImagePath() {
        return uploadImagePath;
    }

    public void setUploadImagePath(String uploadImagePath) {
        this.uploadImagePath = uploadImagePath;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public boolean isOpenInNewWindow() {
        return openInNewWindow;
    }

    public void setOpenInNewWindow(boolean openInNewWindow) {
        this.openInNewWindow = openInNewWindow;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ProductModule{");
        sb.append("campaignName='").append(campaignName).append('\'');
        sb.append(", moduleType='").append(moduleType).append('\'');
        sb.append(", omniCode='").append(omniCode).append('\'');
        sb.append(", title='").append(title).append('\'');
        sb.append(", products=").append(products);
        sb.append(", useExistProductData=").append(useExistProductData);
        sb.append(", existProductData='").append(existProductData).append('\'');
        sb.append(", uploadAsImageVideo=").append(uploadAsImageVideo);
        sb.append(", bannerPlacement=").append(bannerPlacement);
        sb.append(", uploadType=").append(uploadType);
        sb.append(", imageUrl='").append(imageUrl).append('\'');
        sb.append(", uploadImagePath='").append(uploadImagePath).append('\'');
        sb.append(", paypalScript='").append(paypalScript).append('\'');
        sb.append(", linkUrl='").append(linkUrl).append('\'');
        sb.append(", openInNewWindow=").append(openInNewWindow);
        sb.append(", alt='").append(alt).append('\'');
        sb.append(", fontFamily='").append(fontFamily).append('\'');
        sb.append(", fontSize='").append(fontSize).append('\'');
        sb.append(", custom_Format='").append(custom_Format).append('\'');
        sb.append(", videoUrl='").append(videoUrl).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
