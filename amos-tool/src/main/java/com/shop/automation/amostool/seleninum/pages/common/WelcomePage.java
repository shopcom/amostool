package com.shop.automation.amostool.seleninum.pages.common;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.shop.automation.amostool.seleninum.pages.base.AmosPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by bruce.zhang on 8/24/15.
 */
public class WelcomePage extends AmosPage {
	private static final By selfPageUrl = By
			.xpath(".//*[@id='amos_id_tools_signin']//img[@title='SHOP.COM Tools']");

	public WelcomePage(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return null;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return false;
	}

	public WebElement getSelfPageUrl() {
		return findElement(selfPageUrl);
	}

	public void goToToolSelectionPage() {

		waitForPageLoaingFinish();
		getSelfPageUrl().click();
	}

}
