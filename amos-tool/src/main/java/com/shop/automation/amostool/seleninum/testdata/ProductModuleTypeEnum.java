package com.shop.automation.amostool.seleninum.testdata;

import java.util.HashMap;
import java.util.Map;

/**
 * Choose Product Module Types
 * there 4 different types
 *
 * Created by bruce.zhang on 9/4/15.
 */
public enum ProductModuleTypeEnum {
    MULT_80X80(1),
    MULT_80X80_300X250(2),
    MULT_175X175(3),
    SINGLE_175X175(4);

    private int code;
    private static Map<Integer, ProductModuleTypeEnum> map = new HashMap<Integer,ProductModuleTypeEnum>();
    static{
        for(ProductModuleTypeEnum productModuleTypeEnum: ProductModuleTypeEnum.values()){
            map.put(productModuleTypeEnum.getCode(), productModuleTypeEnum);
        }
    }

    ProductModuleTypeEnum(int code) {
        this.code = code;
    }
    public int getCode(){
        return this.code;
    }

    public static ProductModuleTypeEnum valueOf(int code){
        return map.get(code);
    }
}
