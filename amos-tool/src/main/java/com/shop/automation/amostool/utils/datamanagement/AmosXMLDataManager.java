package com.shop.automation.amostool.utils.datamanagement;

import com.marketamerica.automation.testdatalists.*;
import com.marketamerica.automation.utilities.enums.Environment;
import com.shop.automation.amostool.seleninum.testdata.*;
import com.shop.automation.amostool.seleninum.testdata.ProductList;
import com.thoughtworks.xstream.XStream;

import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

/**
 * Created by bruce.zhang on 8/27/15.
 */
public class AmosXMLDataManager extends AmosDataManager {
    private static final String IMAGE_MODULE_XML = "imageModule.xml";
    private static final String PRODUCT_MODULE_XML = "product.xml";

    private XStream xstream;

    public AmosXMLDataManager(final Environment environment, final Locale locale) {
        super(environment, locale);
        this.xstream = new XStream();
        this.setAliases();
    }

    /**
     * Configure XML aliases. This helps us not to hard code package paths
     * representing class paths to test data implementations
     */
    private void setAliases() {
        this.xstream.alias("ImageModule", ImageModule.class);
        this.xstream.alias("ImageModules", ImageModuleList.class);
        this.xstream.alias("Image", Image.class);
        this.xstream.alias("Images", ImageList.class);
        this.xstream.alias("Product", Product.class);
        this.xstream.alias("Products", ProductList.class);
    }


    /**
     * @return returns a list of images
     */
    public ImageList getImageList() {
        return (ImageList) getTestDataList( getPathToDataByEnvironment(), IMAGE_MODULE_XML);
    }

    /**
     *
     * @return returns a list of products
     */
    public ProductList getProductList() {
        return (ProductList) getTestDataList(getPathToDataByEnvironment(), PRODUCT_MODULE_XML);
    }

    /**
     * This method returns a Test Data List based on the file path and xml name
     *
     * @param filePath the file path to the xml
     * @param xmlName  the xml file name
     * @return a Test Data List object representing a collection of TestData.
     */
    private AbstractDataList getTestDataList(final String filePath,
                                             final String xmlName) {

		/*
         * Based on collections of data objects, we store those collections in
		 * the field called testDataList. This field is inherited by all
		 * TestDataLists.
		 */
        this.xstream.addImplicitCollection(AbstractDataList.class, "testDataList");

        InputStream inputStream = getInputStream(filePath, xmlName);
        AbstractDataList testData;
        try {
            testData = (AbstractDataList) xstream.fromXML(inputStream);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }
        return testData;
    }

    /**
     * get the input stream based for the xml file
     *
     * @param filePath
     * @param xmlName
     * @return
     */
    private InputStream getInputStream(String filePath,
                                       String xmlName) {
        final InputStream inputStream = getClass().getResourceAsStream(
                filePath + xmlName);
        if (inputStream == null) {
            if (xmlName == null) {
                xmlName = "\"No XML Defined\"";
            }
            if (filePath == null) {
                filePath = "\"No FilePath Defined\"";

            }
            logger.error(String.format("Could not find %s in %s", xmlName,
                    filePath));
            return null;
        }
        return inputStream;
    }

}
