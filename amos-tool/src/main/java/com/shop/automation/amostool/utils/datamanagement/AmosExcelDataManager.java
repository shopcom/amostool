package com.shop.automation.amostool.utils.datamanagement;

import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.excel.ExcelWorkbookReader;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;

/**
 * Created by bruce.zhang on 8/26/15.
 */
public class AmosExcelDataManager extends AmosDataManager {

    public AmosExcelDataManager(Environment environment, Locale locale) {
        super(environment, locale);
    }


    public ExcelWorkbookReader getWorkBook(final String workbookName) {
        final URL resource = getClass().getResource(getPathToDataByEnvironment() + workbookName);
        Path destination = null;
        try {
            destination = Paths.get(resource.toURI());
        } catch (URISyntaxException e) {
            logger.error(e);
        }
        assert destination != null;
        return new ExcelWorkbookReader(destination.toString());
    }

}
