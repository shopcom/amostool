package com.shop.automation.amostool.seleninum.testdata;

import com.marketamerica.automation.testdata.AbstractData;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.WebDriver;

/**
 * Store ImageModule data from Excel
 *
 * Created by bruce.zhang on 8/26/15.
 */
public class ImageModule extends AbstractData {

    private String imageUrl;
    private String uploadImagePath;
    private String alt;
    private String marginTop;
    private String marginBottom;
    private ImageUploadTypeEnum imageType;
    private String paypalScript;
	private String linkUrl;
    private boolean openLinkInNewWindow;

    public ImageUploadTypeEnum getImageType() {
        return imageType;
    }

    public void setImageType(ImageUploadTypeEnum imageType) {
        this.imageType = imageType;
    }

    public String getPaypalScript() {
        return paypalScript;
    }

    public void setPaypalScript(String paypalScript) {
        this.paypalScript = paypalScript;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUploadImagePath() {
        return uploadImagePath;
    }

    public void setUploadImagePath(String uploadImagePath) {
        this.uploadImagePath = uploadImagePath;
    }

    public String getAlt() {
        return alt;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public String getMarginTop() {
        return marginTop;
    }

    public void setMarginTop(String marginTop) {
        this.marginTop = marginTop;
    }

    public String getMarginBottom() {
        return marginBottom;
    }

    public void setMarginBottom(String marginBottom) {
        this.marginBottom = marginBottom;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public boolean isOpenLinkInNewWindow() {
        return openLinkInNewWindow;
    }

    public void setOpenLinkInNewWindow(boolean openLinkInNewWindow) {
        this.openLinkInNewWindow = openLinkInNewWindow;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ImageModule{");
        sb.append("imageUrl='").append(imageUrl).append('\'');
        sb.append(", uploadImagePath='").append(uploadImagePath).append('\'');
        sb.append(", alt='").append(alt).append('\'');
        sb.append(", marginTop='").append(marginTop).append('\'');
        sb.append(", marginBottom='").append(marginBottom).append('\'');
        sb.append(", imageType='").append(imageType).append('\'');
        sb.append(", paypalScript='").append(paypalScript).append('\'');
        sb.append(", linkUrl='").append(linkUrl).append('\'');
        sb.append(", openLinkInNewWindow=").append(openLinkInNewWindow);
        sb.append('}');
        return sb.toString();
    }
}
