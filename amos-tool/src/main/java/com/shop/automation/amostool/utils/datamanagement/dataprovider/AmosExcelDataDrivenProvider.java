package com.shop.automation.amostool.utils.datamanagement.dataprovider;

import com.marketamerica.automation.utilities.excel.ExcelWorkbookReader;
import com.shop.automation.amostool.seleninum.exception.AmosException;
import com.shop.automation.amostool.seleninum.testdata.*;
import com.shop.automation.amostool.utils.datamanagement.AmosExcelDataManager;
import com.shop.automation.amostool.utils.datamanagement.AmosXMLDataManager;
import org.apache.commons.lang.StringUtils;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Read Excel Data for Amos Tool Created by bruce.zhang on 8/26/15.
 */
public class AmosExcelDataDrivenProvider extends AmosAbstractDataProvider {
    private static final String EXCEL_WORKBOOK_NAME = "amosToolTestData.xlsx";
    private static final String IMG_ALT = "alttag";
    private static final String IMG_MARGIN_TOP = "topmargins";
    private static final String IMG_MARGIN_BOTTOM = "bottommargins";
    private static final String IMG_OPEN_IN_NEW_WINDOW = "openinnewwindow";
    private static final String IMG_ID = "imageurl";
    private static final String IMG_TYPE = "imagetype";
    private static final String IMG_UPLOAD_PATH = "imagelocal";
    private static final String IMG_PAYPAL = "imagepaypal";
    private static final String IMG_LINK_URL = "linkurl";
    private static final String PRODUCT_MODULE_TYPE = "moduletype";
    private static final String PRODUCT_OMNI_CODE = "omnicode";
    private static final String PRODUCT_BANNER_PLACEMENT = "bannerplacement";
    private static final String PRODUCT_TITLE = "title";
    private static final String PRODUCT_BANNER_UPLOAD_TYPE = "uploadtype";
    private static final String PRODUCT_FONT_FAMILY = "fontfamily";
    private static final String PRODUCT_FONT_SIZES = "fontsizes";
    private static final String PRODUCT_CUSTOM_FORMATS = "customeformats";
    private static final String PRODUCT_USE_EXIST_PRODUCT = "useexistproduct";
    private static final String PRODUCT_DATA = "product";
    private static final String PRODUCT_VIDEO_URL = "videourl";

    private static final String CAMPAIGN_NAME = "campaignname";

    /**
     * Find the Image path
     *
     * @return
     */
    public static String getSrcPath() {
        String separator = File.separator;
        StringBuilder path = new StringBuilder();
        path.append(System.getProperty("user.dir"));
        path.append(separator).append("src");
        path.append(separator).append("test");
        path.append(separator).append("resources");
        path.append(separator).append("testdata");
        path.append(separator).append("staging");
        path.append(separator).append("image");
        path.append(separator);
        return path.toString();
    }


    @DataProvider(name = "imageModules")
    public static Object[][] getImageModules(ITestContext context, Method method) {
        // Create a data manager based on our current context
        int parameterCount = 4;
        final AmosXMLDataManager xmlDataManager = createXMLDataManager(context);

        final ImageList imageList = xmlDataManager.getImageList();

        final AmosExcelDataManager excelDataManager = createExcelManager(context);
        final ExcelWorkbookReader excelWorkbookTestDataParser = excelDataManager.getWorkBook(EXCEL_WORKBOOK_NAME);

        // Find our rows based on the method name
        final List<Map<String, List<String>>> excelRows = excelWorkbookTestDataParser.getSheetDataAsMultiMap(method.getName());
        // Create our return object. Dimension one is sized based on the number
        // of rows.
        // This represents each iteration
        int rowCount = excelRows.size();
        // remove the title row
        final Object[][] requestedAddresses = new Object[rowCount][parameterCount];

        // Counter representing each row
        int position = 0;

        // For each row found the relevant address data
        for (Map<String, List<String>> row : excelRows) {
            final Set<String> columns = excelRows.get(0).keySet();

            ImageModule imageModule = new ImageModule();
            Campaign campaign = new Campaign();

            for (String column : columns) {
                String formattedColumn = column.trim();

                final List<String> columnValues = row.get(column);
                for (final String columnValue : columnValues) {
                    switch (formattedColumn.toLowerCase()) {
                        case IMG_ID:
                            if (columnValue == null || columnValue.equals("NULL") || columnValue.isEmpty()) {
                                continue;
                            }
                            Image image = imageList.getImage(columnValue);
                            if (image != null) {
                                imageModule.setImageUrl(image.getImageUrl());
                            }
                            continue;
                        case IMG_ALT:
                            imageModule.setAlt(columnValue);
                            continue;
                        case IMG_MARGIN_TOP:
                            imageModule.setMarginTop(columnValue);
                            continue;
                        case IMG_MARGIN_BOTTOM:
                            imageModule.setMarginBottom(columnValue);
                            continue;
                        case IMG_TYPE:
                            imageModule.setImageType(ImageUploadTypeEnum.valueOf(columnValue.toUpperCase()));
                            continue;
                        case IMG_UPLOAD_PATH:
                            String path = getSrcPath() + columnValue;
                            System.out.println("aaaaaaaaaaaaaaaaaa" + path);
                            imageModule.setUploadImagePath(path);
                            continue;
                        case IMG_PAYPAL:
                            imageModule.setPaypalScript(columnValue);
                            continue;
                        case IMG_LINK_URL:
                            imageModule.setLinkUrl(columnValue);
                            continue;
                        case IMG_OPEN_IN_NEW_WINDOW:
                            imageModule.setOpenLinkInNewWindow(StringUtils.isNotEmpty(columnValue) && !"NULL".equalsIgnoreCase(columnValue));
                            continue;
                        case CAMPAIGN_NAME:
                            campaign.setCampaignName(columnValue);
                            continue;
                        default:
                            // System.out.print("GO TO Default");
                            continue;
                    }
                }
            }
            requestedAddresses[position][0] = row.get("id").get(0);
            requestedAddresses[position][1] = row.get("description").get(0);
            requestedAddresses[position][2] = imageModule;
            requestedAddresses[position][3] = campaign;
            position++;
        }
        return requestedAddresses;
    }

    @DataProvider(name = "productModules")
    public static Object[][] getProductModules(ITestContext context, Method method) throws AmosException {
        // Create a data manager based on our current context
        int parameterCount = 4;
        final AmosXMLDataManager xmlDataManager = createXMLDataManager(context);

        final ProductList productList = xmlDataManager.getProductList();
        final ImageList imageList = xmlDataManager.getImageList();

        final AmosExcelDataManager excelDataManager = createExcelManager(context);
        final ExcelWorkbookReader excelWorkbookTestDataParser = excelDataManager.getWorkBook(EXCEL_WORKBOOK_NAME);

        // Find our rows based on the method name
        final List<Map<String, List<String>>> excelRows = excelWorkbookTestDataParser.getSheetDataAsMultiMap(method.getName());
        // Create our return object. Dimension one is sized based on the number
        // of rows.
        // This represents each iteration
        int rowCount = excelRows.size();
        // remove the title row
        final Object[][] requestedAddresses = new Object[rowCount][parameterCount];

        // Counter representing each row
        int position = 0;

        List<String> testCampaignNames = new ArrayList<String>();
        // For each row found the relevant address data
        for (Map<String, List<String>> row : excelRows) {
            final Set<String> columns = excelRows.get(0).keySet();

            ProductModule productModule = new ProductModule();
            Campaign campaign = new Campaign();

            for (String column : columns) {
                String formattedColumn = column.trim();

                final List<String> columnValues = row.get(column);
                for (final String columnValue : columnValues) {
                    if (columnValue == null || columnValue.equals("NULL") || columnValue.isEmpty()) {
                        continue;
                    }
                    switch (formattedColumn.toLowerCase()) {
                        case IMG_ID:
                            Image image = imageList.getImage(columnValue);
                            if (image != null) {
                                productModule.setImageUrl(image.getImageUrl());
                            }
                            continue;
                        case PRODUCT_DATA:
                            Product product = productList.getProduct(columnValue);
                            if (product != null) {
                                productModule.getProducts().add(product);
                            }
                            continue;
                        case PRODUCT_MODULE_TYPE:
                            productModule.setModuleType(ProductModuleTypeEnum.valueOf(Integer.parseInt(columnValue)));
                            continue;
                        case PRODUCT_OMNI_CODE:
                            productModule.setOmniCode(columnValue);
                            continue;
                        case PRODUCT_BANNER_PLACEMENT:
                            productModule.setBannerPlacement(BannerPlacementEnum.valueOf(columnValue.toUpperCase()));
                            continue;
                        case IMG_TYPE:
                            productModule.setUploadType(ImageUploadTypeEnum.valueOf(columnValue.toUpperCase()));
                            continue;
                        case IMG_UPLOAD_PATH:
                            String path = getSrcPath() + columnValue;
                            System.out.println("bbbbbbbbbbbbbbbbbbb" + path);
                            productModule.setUploadImagePath(path);
                            continue;
                        case IMG_PAYPAL:
                            productModule.setPaypalScript(columnValue);
                            continue;
                        case IMG_LINK_URL:
                            productModule.setLinkUrl(columnValue);
                            continue;
                        case IMG_OPEN_IN_NEW_WINDOW:
                            productModule.setOpenInNewWindow(StringUtils.isNotEmpty(columnValue) && !"NULL".equalsIgnoreCase(columnValue) && "TRUE".equalsIgnoreCase(columnValue));
                            continue;
                        case IMG_ALT:
                            productModule.setAlt(columnValue);
                            continue;
                        case PRODUCT_TITLE:
                            productModule.setTitle(columnValue);
                            continue;
                        case PRODUCT_BANNER_UPLOAD_TYPE:
                            if (UploadTypeEnum.VIDOE.getValue().equalsIgnoreCase(columnValue)) {
                                productModule.setUploadAsImageVideo(UploadTypeEnum.VIDOE);
                            } else if (UploadTypeEnum.IMAGE.getValue().equalsIgnoreCase(columnValue)) {
                                productModule.setUploadAsImageVideo(UploadTypeEnum.IMAGE);
                            } else {
                                throw new AmosException("UploadType Enum only accept vlaue 'IMAGE' or 'VIDEO' ");
                            }
                            continue;
                        case PRODUCT_VIDEO_URL:
                            productModule.setVideoUrl(columnValue);
                            continue;
                        case PRODUCT_FONT_FAMILY:
                            productModule.setFontFamily(columnValue);
                            continue;
                        case PRODUCT_FONT_SIZES:
                            productModule.setFontSize(columnValue);
                            continue;
                        case PRODUCT_CUSTOM_FORMATS:
                            productModule.setCustom_Format(columnValue);
                            continue;
                        case PRODUCT_USE_EXIST_PRODUCT:
                            productModule.setUseExistProductData("TRUE".equalsIgnoreCase(columnValue));
                            continue;
                        case CAMPAIGN_NAME:
                            campaign.setCampaignName(columnValue);
                            continue;
                        default:
                            // System.out.print("GO TO Default");
                            continue;
                    }
                }
            }
            requestedAddresses[position][0] = row.get("id").get(0);
            requestedAddresses[position][1] = row.get("description").get(0);
            requestedAddresses[position][2] = productModule;
            requestedAddresses[position][3] = campaign;
            position++;
        }
        return requestedAddresses;
    }

}
