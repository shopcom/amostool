package com.shop.automation.amostool.seleninum.testdata;

import com.marketamerica.automation.testdata.AbstractData;

/**
 * Store campaign data from Excel
 *
 * Created by bruce.zhang on 8/27/15.
 */
public class Campaign extends AbstractData {
    private String campaignName;
    private String startDate;
    private String endDate;
    private boolean noEndDate;
    private boolean showOnDesktop;
    private boolean showOnMobile;

    public String getCampaignName() {
		return campaignName;
	}

	public void setCampaignName(String campaignName) {
		this.campaignName = campaignName;
	}

	public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public boolean isNoEndDate() {
        return noEndDate;
    }

    public void setNoEndDate(boolean noEndDate) {
        this.noEndDate = noEndDate;
    }

    public boolean isShowOnDesktop() {
        return showOnDesktop;
    }

    public void setShowOnDesktop(boolean showOnDesktop) {
        this.showOnDesktop = showOnDesktop;
    }

    public boolean isShowOnMobile() {
        return showOnMobile;
    }

    public void setShowOnMobile(boolean showOnMobile) {
        this.showOnMobile = showOnMobile;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Campaign{");
        sb.append("campaignName='").append(campaignName).append('\'');
        sb.append(", startDate='").append(startDate).append('\'');
        sb.append(", endDate='").append(endDate).append('\'');
        sb.append(", noEndDate=").append(noEndDate);
        sb.append(", showOnDesktop=").append(showOnDesktop);
        sb.append(", showOnMobile=").append(showOnMobile);
        sb.append('}');
        return sb.toString();
    }
}
