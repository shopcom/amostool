package com.shop.automation.shop.selenium.pages.base.checkout.mobile;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;

import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.selenium.pages.base.ShopPage;


public class ShopReviewOrderPageMobile extends ShopPage {

	private final static By reviewSubtotals = By.cssSelector("form#shop-form.shop-form section div.order-summary ul li span.amount.subtotal");
	private final static By reviewTax = By.cssSelector("form#shop-form.shop-form section div.order-summary ul li span.amount.tax");
	private final static By reviewShipping = By.cssSelector("form#shop-form.shop-form section div.order-summary ul li span.amount.shipping-cost");
	private final static By reviewFinalTotal = By.cssSelector("div.bottom-stripe.cart-subtotal-banner.cart-total span.amount.total");
	private final static By continueButton = By.cssSelector("input.button.primary");
	private final static By shippingDropdown = By.cssSelector("fieldset.select-list.primary .select-shipping");
	private final static By bagField = By.cssSelector("input#bag-qty.bags");
	
	public ShopReviewOrderPageMobile(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
		
		try {
			Verify.verifyTrue(atPage(),
					"Expected to be Mobile Review Order Page");
		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence of Mobile Review Order Page");
		}

		
	}
	
	public BigDecimal getSubtotal()
	{
			List<WebElement> subtotals = findElements(reviewSubtotals);
			BigDecimal total = new BigDecimal(0);
			if (subtotals.size() >1)
			{
				for (int i = 0; i < subtotals.size()-1; ++i) {
					total = total
							.add(CurrencyUtilities
									.convertPriceStringToBigDecimal(subtotals
											.get(i).getText().trim()));
					}
			}
			else
			{
				String element = findElement(reviewSubtotals).getText().toString().trim();
				total = CurrencyUtilities.convertPriceStringToBigDecimal(element);
			}
			return total;
	}
	
	public BigDecimal getTax(final Map<String, String> customParameters)
	{
		BigDecimal tax = new BigDecimal("0");
		if (!customParameters.get(String.format("InvoiceTaxValue")).equals(
				"NULL"))
		{
			
			List<WebElement> taxElement = findElements(reviewTax);
			tax = new BigDecimal(0);
			if (taxElement.size() >1)
				{
					for (int i = 0; i < taxElement.size()-1; ++i) {
					tax = tax
							.add(CurrencyUtilities
									.convertPriceStringToBigDecimal(taxElement
											.get(i).getText().trim()));
				}
			}
			else
			{
				String element = findElement(reviewTax).getText().toString().trim();
				tax = CurrencyUtilities.convertPriceStringToBigDecimal(element);
			}
			
		}	
		
		return tax;
			
	}
	
	public BigDecimal getShipping()
	{
		List<WebElement> ship = findElements(reviewShipping);
		BigDecimal shipping = new BigDecimal(0);
		if (ship.size() >1)
		{
			for (int i = 0; i < ship.size()-1; ++i) {
				shipping = shipping
						.add(CurrencyUtilities
								.convertPriceStringToBigDecimal(ship
										.get(i).getText().trim()));
				}
		}
		else
		{
			String element = findElement(reviewShipping).getText().toString().trim();
			shipping = CurrencyUtilities.convertPriceStringToBigDecimal(element);
		}
				
		return shipping;
	}
	
	public BigDecimal getFinalTotal()
	{
		String element = findElement(reviewFinalTotal).getText().toString().trim();
		BigDecimal finalTotal = CurrencyUtilities
				.convertPriceStringToBigDecimal(element);
		
		return finalTotal;
	}
	
	public BigDecimal addTotals(final Map<String, String> customParameters)
	{
		BigDecimal addedTotals = getSubtotal().add(getTax(customParameters)).add(getShipping());
				
		return addedTotals;
	}
	
	public void changeShipping(final Map<String, String> customParameters)
	{
		if (isElementExisting(By.cssSelector("fieldset.select-list.primary .select-shipping"), 5) != false)
		{
			if (!customParameters.get("ShippingType").equals("NULL")) 
			{
				String shipValue = customParameters.get(String.format("ShippingType"));
				List<WebElement> options = createDropDown(shippingDropdown).getOptions();
				for (int i =0; i < options.size(); i++)
				{
					if (options.get(i).getText().contains(shipValue))
					{
						createDropDown(shippingDropdown).selectByIndex(i);
					}
				}
			}
		}
	}
	
	public void addBags(final Map<String, String> customParameters)
	{
		if (customParameters.containsKey(String.format("Bags")))
		{
			if (!customParameters.get(String.format("Bags")).equals("NULL"))
			{
				enterBags(customParameters);
			}
		}
	}
	
	private void enterBags(final Map<String, String> customParameters)
	{
		setTextField(bagField, customParameters.get(String.format("Bags")));
	}
	
	public void continueToPayment()
	{
		findElement(continueButton).click();
	}

	public void waitForLoad() {
		this.waitForjQueryAjax("Waiting for page to load");
		waitForElementToBeVisible(By.id("shop-form"), 15);
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(this.getBody(), 30);
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		waitForLoad();
		return By.id("shop-form");
	}

}