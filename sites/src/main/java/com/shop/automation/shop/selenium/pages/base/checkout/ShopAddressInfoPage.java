package com.shop.automation.shop.selenium.pages.base.checkout;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.testdata.Address;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.selenium.pages.base.ShopPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;

public class ShopAddressInfoPage extends ShopPage {

	@SuppressWarnings("unused")
	private static final By bodyElement = By
			.cssSelector("div [id='checkout'].guest");
	private static final By emailField = By.cssSelector("input[name='email']");
	private static final By firstNameField = By
			.xpath("//input[@id='firstNameShipping']");
	private static final By lastNameField = By
			.xpath("//input[@id='lastNameShipping']");
	private static final By streetAddressOneField = By
			.xpath("//input[@id='address1Shipping']");
	// private static final By streetAddressTwoField =
	// By.xpath("//input[@id='address2Shipping']");
	private static final By cityField = By.xpath("//input[@id='cityShipping']");
	private static final By stateDropDownField = By
			.xpath("//select[@id='stateShipping']");
	private static final By zipCodeField = By
			.xpath("//input[@id='zipShipping']");
	private static final By phoneCodeField = By
			.xpath("//input[@id='phoneShipping']");
	// private static final By referrelEmailField =
	// By.cssSelector("input[name='referralEmail']");
	// Add billing address selection fields here
	private static final By saveButton = By
			.xpath("//a[@id='saveNewShippingAddress']");
	private static final By cancelLink = By
			.xpath("//a[@class='cancel footerLinks']");
	private static final By editButton = By
			.xpath("//a[@class='editBillingAddressLink footerLinks margL15']");
	private static final By continueButton = By
			.xpath("//a[@class='checkoutButton']");
	private static final By addNewAddressLink = By
			.xpath("//a[@id='addNewShippingAddress']");

	/*
	 * private static final By currentAddress =
	 * By.xpath("//span[@class='addressDetails']");
	 */

	public ShopAddressInfoPage(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager, final Country country) {
		super(driver, translator, elementDefinitionManager);
		
		ShopHomePage homePage = new ShopHomePage(driver, translator,
				elementDefinitionManager);
		homePage.header.checkForTransitionOverlay();
		
			try {
				Verify.verifyTrue(atPage(), "Expected to be on Address Info Page");
			} catch (UnableToVerifyPagesExistenceException e) {
				StepInfo.failTest("Unable to verify existence of Address Info Page");
			}
	}

	private void setEmailAddress(final String email) {
		getEmailAddressElement().sendKeys(email);
	}

	private WebElement getEmailAddressElement() {
		return findElement(By.cssSelector("input[name='email']"));
	}

	private WebElement getFirstNameElement() {
		return findElement(firstNameField);
	}

	private WebElement getLastNameElement() {
		return findElement(lastNameField);
	}

	private WebElement getStreetAddressOneElement() {
		return findElement(streetAddressOneField);
	}

	private WebElement getCityElement() {
		return findElement(cityField);
	}

	private WebElement getStateDropDown() {
		return findElement(stateDropDownField);
	}

	// Most likely doesn't work
	private void setStateDropDown(final String state) {
		getStateDropDown().sendKeys(state);
	}

	private WebElement getZipCodeElement() {
		return findElement(zipCodeField);
	}

	private void setFirstName(final String firstName) {
		getFirstNameElement().sendKeys(firstName);
	}

	private void setLastName(final String lastName) {
		findElement(lastNameField).sendKeys(lastName);
	}

	private void setStreetAddressOne(final String streetOne) {
		getStreetAddressOneElement().sendKeys(streetOne);
	}

	private void setCity(final String city) {
		getCityElement().sendKeys(city);
	}

	private WebElement getPhoneNumberElement() {
		return findElement(phoneCodeField);
	}

	private void setZipCode(final String zip) {
		getZipCodeElement().sendKeys(zip);
	}

	private void setPhoneNumber(final String phoneNumber) {
		getPhoneNumberElement().sendKeys(phoneNumber);
	}

	private WebElement continueButton() {
		return findElement(continueButton);

	}

	protected WebElement saveNewAddress() {

		return findElement(saveButton);
	}

	protected WebElement cancelNewAddress() {

		return findElement(cancelLink);
	}

	protected WebElement editAddress() {

		return findElement(editButton);
	}

	// public String getAddress(){
	//
	// return findElement(currentAddress);
	//
	// }

	protected WebElement addNewAddress() {

		return findElement(addNewAddressLink);
	}

	public void continueCheckout() {
		
			ShopHomePage homePage = new ShopHomePage(driver, translator,
					elementDefinitionManager);
			waitForElementToBeClickable(By.xpath("//a[@class='checkoutButton']"), true, 20);
			continueButton().click();
			homePage.header.checkForTransitionOverlay();

	}

	/*
	 * public String getAddress() { String address; address =
	 * findElement(By.cssSelector("span.addressDetails")) .getText(); return
	 * address; }
	 */

	/**
	 * Ppulate billing and address information
	 *
	 * @param shopper
	 *            user information used to fill in address and shipping
	 *            information
	 * @return A implementation of MotivesCosmeticsGuestAddressPage
	 */
	@SuppressWarnings("unchecked")
	public <T extends ShopAddressInfoPage> T populateAddressAndBillingInfo(
			final Shopper shopper) {
		final Address address = shopper.getAddress();
		setCity(address.getCity());
		setEmailAddress(shopper.getEmail());
		setFirstName(shopper.getFirstName());
		setLastName(shopper.getLastName());
		setStreetAddressOne(address.getStreet());
		setZipCode(address.getZipCode());
		// Most likely won't work for now
		setStateDropDown(address.getState());
		setPhoneNumber(shopper.getPhoneNumber());
		return (T) this;
	}

	/**
	 * TODO Refactor the address page into two separate pages one for new users
	 * and one for existing users This method verifies that the following fields
	 * match our inputted user. Email, Phone, First Name, Last Name, Address
	 * One, City, State, Zip Code
	 *
	 * @param shopper
	 *            A user object to compare the fields to
	 * @return
	 */
	public ShopAddressInfoPage verifyFieldsFilledInForUser(final Shopper shopper) {
		// Verify the first name matches our user
		Verify.verifyContains(getFirstNameElement().getText(),
				shopper.getFirstName(),
				"Verify the first name matches our user");
		// Verify the last name matches our user
		Verify.verifyContains(getLastNameElement().getText(),
				shopper.getLastName(), "Verify the last name matches our user");

		// Verify the street address matches our shoppers
		Verify.verifyContains(getStreetAddressOneElement().getText(), shopper
				.getAddress().getStreet(),
				"Verify the street address matches our shoppers");

		// Verify the city matches our shoppers
		Verify.verifyContains(getCityElement().getText(), shopper.getAddress()
				.getCity(), "Verify the city matches our shoppers");

		// Verify the state drop down selected value matches our state
		Verify.verifyContains(getStateDropDown().getText(), shopper
				.getAddress().getState(),
				"Verify the state drop down selected value matches our state");

		// Verify our zip code matches
		Verify.verifyContains(getZipCodeElement().getText(), shopper
				.getAddress().getZipCode(), "Verify our zip code matches");

		// Verify our phone number matches
		// Verify.verifyContains(
		// getPhoneNumberElement().getText(),"Verify our phone number matches"
		// shopper.getPhoneNumber()
		// );

		return this;
	}

	private String waitForLoad() {
		return waitForElementToBePresent(By.id("returningUserContinueButton"),
				true, 20).getText();
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(getBody());
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		if(WebDriverUserAgent.getCurrentUserAgent().equals(WebDriverUserAgent.DEFAULT))
    	{
			waitForLoad();
			return By.id("returningUserContinueButton");
    	}
		else
		{
			return By.id("checkout");
		}
		
	}

}
