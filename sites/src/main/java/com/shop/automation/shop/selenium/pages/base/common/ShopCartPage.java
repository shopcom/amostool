package com.shop.automation.shop.selenium.pages.base.common;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;

/**
 * Created by javierv on 5/7/2014. Edited by Rick on 5/222014.
 */
public class ShopCartPage extends ShopTemplatePage {
	private static final String productQuantityField = "//input[contains(@name,'quantity15744718')]";
	private static final String updateProductQuantityButton = "//a[contains(.,'update qty')]";
	private static final String removeProductButton = "//a[contains(.,'remove item')]";
	private static final String saveForLaterButton = "//a[contains(.,'save for later')]";
	private static final By checkoutButton = By.xpath("//a[@class='checkoutbutton']");
	private static final By zipCodeField = By
			.cssSelector("div.current table.items tbody tr.calculator td table tbody tr.content_first td input.text");
	private static final By totals = By
			.cssSelector("tbody tr td.left_border div.current table.items tbody tr.total td span.float_right span.tx_nowrap table tbody tr td span.emphasis");
	private static final By orderTotal = By.cssSelector("tbody tr.grand_total td span.price");
	private static final By shippingTotal = By
			.xpath("//tbody/tr[(@class='total')]//tr[(@valign='top') and position()=2]//td/span");
	private static final By shippingSpainTotal = By
			.xpath("//tbody/tr[(@class='total')]//tr[(@valign='top') and position()=3]//td/span");
	private static final By subTotals = By
			.xpath("//tr[(@class='total')]//tr[(@valign='top')and position()=1]//td[(@align='right')]//span[@class='emphasis']");

	public ShopCartPage(final WebDriver driver, final Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}

	private WebElement getProductQuantityField(Product product) {
		return findElementByXpath(productQuantityField, product.getSku());
	}

	private WebElement getUpdateProductQuantityButton(Product product) {
		return findElementByXpath(updateProductQuantityButton, product.getSku());
	}

	private WebElement getRemoveProductButton(Product product) {
		return findElementByXpath(removeProductButton, product.getSku());
	}

	private WebElement getSaveForLaterButton(Product product) {
		return findElementByXpath(saveForLaterButton, product.getSku());
	}

	public WebElement getZipCodeField() {
		return findElement(zipCodeField);
	}

	public Boolean shipCalculated(final Country country) {
		// TWN, SGP, HKG do not have shipping calculators
		if (country.equals(Country.TAIWAN) || country.equals(Country.HONG_KONG) || country.equals(Country.SINGAPORE)) {
			return false;
		}
		if (!getZipCodeField().getAttribute("value").isEmpty()) {
			return true;
		} else {
			return false;

		}
	}

	public By getShippingXPath() {
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		try {
			if (this.isElementExistingAndDisplayed(shippingTotal)) {
				return shippingTotal;
			} else if (this.isElementExistingAndDisplayed(shippingSpainTotal)) {
				return shippingSpainTotal;
			}
		} catch (Exception e) {
			return null;
		} finally {
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		}
		return null;
	}
	
	public BigDecimal getInvoiceVendorItemsTotal() {
		List<WebElement> invoiceVenderItemTotal = driver
				.findElements(By
						.xpath("//div[@class='pyoCartItemTemplate' and contains(@style, 'display:block')]//span[contains(@class, 'vendorItemsTotal')]"));
		BigDecimal venderItemsTotal = new BigDecimal(0);
		for (int i = 0; i < invoiceVenderItemTotal.size(); ++i) {
			venderItemsTotal = venderItemsTotal.add(CurrencyUtilities
					.convertPriceStringToBigDecimal(invoiceVenderItemTotal.get(i).getText().trim()));
		}
		return venderItemsTotal;
	}

	public BigDecimal getSubtotal() {
		List<WebElement> values = findElements(subTotals);
		BigDecimal subtotal = new BigDecimal(0);
		for (int i = 0; i < values.size(); ++i) {
			subtotal = subtotal.add(CurrencyUtilities.convertPriceStringToBigDecimal(values.get(i).getText().trim()));
		}
		return subtotal;
	}

	private BigDecimal getTax(final Map<String, String> customParameters) {
		List<WebElement> values = findElements(totals);
		BigDecimal tax = new BigDecimal("0");
		if (!customParameters.get(String.format("InvoiceTaxValue")).equals("NULL")) {
			tax = CurrencyUtilities.convertPriceStringToBigDecimal(values.get(1).getText().trim());
		}
		return tax;
	}

	private BigDecimal getShipping(final Country country) {
		List<WebElement> values = findElements(getShippingXPath());
		BigDecimal shipping = new BigDecimal(0);
		BigDecimal zeroShipping = new BigDecimal(0);
		if (shipCalculated(country) == true) {
			for (int i = 0; i < values.size(); ++i) {
				if (values.get(i).getText().trim().contains("Free")
						|| values.get(i).getText().trim().contains("Gratis")) {
					zeroShipping = new BigDecimal(0);
				} else {
					shipping = CurrencyUtilities.convertPriceStringToBigDecimal(values.get(i).getText());
				}
				shipping = shipping.add(zeroShipping);
			}
			return shipping;
		}
		return shipping;
	}

	public BigDecimal getGrandTotal() {
		BigDecimal total = new BigDecimal("0");
		total = CurrencyUtilities.convertPriceStringToBigDecimal(findElement(orderTotal).getText().trim());
		return total;
	}

	public BigDecimal addTotals(final Map<String, String> customParameters, final Country country) {
		BigDecimal grandTotal = new BigDecimal(0);
		grandTotal = getSubtotal().add(getTax(customParameters)).add(getShipping(country));

		return grandTotal;
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return null;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return false;
	}

	/**
	 * Adjust the given product's quantity on the cart page
	 *
	 * @param product
	 *            A product whose quantity you want to adjust. The product is
	 *            selected on the page by its sku, so please be sure that the
	 *            product has an entered sku
	 * @param quantity
	 *            The number of items you would like the product to have
	 * @return
	 */
	public ShopCartPage adjustProductQuantity(final Product product, final int quantity) {
		getProductQuantityField(product).sendKeys(String.valueOf(quantity));
		getUpdateProductQuantityButton(product).click();
		return new ShopCartPage(driver, translator, elementDefinitionManager);
	}

	public ShopCartPage removeProduct(final Product product) {
		getRemoveProductButton(product).click();
		return new ShopCartPage(driver, translator, elementDefinitionManager);
	}

	public ShopCartPage SaveForLater(final Product product) {
		getSaveForLaterButton(product).click();
		return new ShopCartPage(driver, translator, elementDefinitionManager);
	}

	private WebElement getCheckoutButton() {
		return findElement(checkoutButton);

	}

	public void emptyCart() {

		while (!isCartEmpty()) {
			this.removeSelectedProductFromCart(1);
		}
	}

	public boolean isCartEmpty() {
		return isElementExisting(By.cssSelector("div#orderingWrapper > p.corner"));
	}

	private void removeSelectedProductFromCart(final int position) {
		getProductsInCart().get(position - 1).findElement(By.className("removeItem")).click();
		this.waitForjQueryAjax("Wait for item removal");
	}

	private List<WebElement> getProductsInCart() {
		return findElements(By.className("prodRow"));
	}

	public void proceedToCheckoutButton() {
		WebElement proccedCheckoutBtn = findElement(By.cssSelector("a.button.primary"));
		waitForElementToBeClickable(proccedCheckoutBtn, 10);
		proccedCheckoutBtn.click();
	}

	public ShopMiniCartPage checkout() {
		getCheckoutButton().click();
		return new ShopMiniCartPage(driver, translator, elementDefinitionManager);
	}

	private void slowdowImplicitWaits() {
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}

	private void turnOnImplicitWaits() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	// Old Clear cart function, keeping temporarily
	/*
	 * public void Looptoclear() { int count = 0; slowdowImplicitWaits(); String
	 * xPath =
	 * ".//*[@id='shopping_cart_table']/tbody/tr/td/form/div/table/tbody/tr/td[1]/table/tbody/tr[5]/td/div[1]/table/tbody/tr[*]/td[4]/table/tbody/tr[2]/td/a"
	 * ;
	 * 
	 * while (count < 30) {
	 * 
	 * if (driver.findElements(By.xpath(xPath)).size() != 0) { WebElement
	 * removecart = driver.findElement(By.xpath(xPath)); removecart.click();
	 * count = count + 1; } if (driver.findElements(By.xpath(xPath)).size() ==
	 * 0) { count = count + 30; } }
	 * 
	 * turnOnImplicitWaits(); }
	 */

	public void clearCart() {
		slowdowImplicitWaits();
		if (WebDriverUserAgent.getCurrentUserAgent().equals(WebDriverUserAgent.DEFAULT)) {
			clearCartFullSite();
			clearSavedForLaterFullSite();
		} else {
			clearCartMobileSite();
		}
		turnOnImplicitWaits();
	}

	public void RWDclearCart() {
		slowdowImplicitWaits();
		clearCartMobileSite();
		turnOnImplicitWaits();
	}

	private void clearCartFullSite() {

		String xPath = ".//*[@id='shopping_cart_table']/tbody/tr/td/form/div/table/tbody/tr/td[1]/table/tbody/tr[5]/td/div[1]/table/tbody/tr[*]/td[4]/table/tbody/tr[2]/td/a";
		List<WebElement> cartItems = driver.findElements(By.cssSelector("div.current table.items tbody tr.item"));
		for (int i = 0; i < cartItems.size(); i++) {
			WebElement removecart = driver.findElement(By.xpath(xPath));
			removecart.click();
		}
	}

	private void clearCartMobileSite() {
		List<WebElement> cartItems = driver.findElements(By.cssSelector("li.cart-item"));
		for (int i = 0; i < cartItems.size(); i++) {
			WebElement removecart = driver.findElement(By.cssSelector("a.button.tertiary.item-delete"));
			removecart.click();
		}
	}

	private void clearSavedForLaterFullSite() {
		String xPath = ".//*[@id='shopping_cart_table']/tbody/tr/td/form/div/table/tbody/tr/td[1]/table/tbody/tr[5]/td/div/table/tbody/tr[*]/td[4]/table/tbody/tr[2]/td/a";
		List<WebElement> cartItems = driver.findElements(By.cssSelector("div.saved table.items tbody tr.item"));

		for (int i = 0; i < cartItems.size(); i++) {
			WebElement removecart = driver.findElement(By.xpath(xPath));
			removecart.click();
		}
	}

}
