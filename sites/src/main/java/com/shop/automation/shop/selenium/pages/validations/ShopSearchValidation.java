package com.shop.automation.shop.selenium.pages.validations;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang.WordUtils;
import org.openqa.selenium.WebDriver;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Validate;
import com.shop.automation.shop.selenium.pages.base.common.ShopDebugModePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopHeaderPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopRestCallParser;
import com.shop.automation.shop.selenium.pages.base.common.ShopSearchResultsPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopSearchXMLParser;

public class ShopSearchValidation {

	public ShopSearchValidation(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager,
			final Map<String, String> customParameters, final Locale locale)
			throws UnsupportedEncodingException {

		final ShopHomePage homePage = new ShopHomePage(driver, translator,
				elementDefinitionManager);

		ShopSearchResultsPage searchResultsPage = new ShopSearchResultsPage(
				driver, translator, elementDefinitionManager);

		ShopDebugModePage debugModePage = new ShopDebugModePage(driver,
				translator, elementDefinitionManager);

		ShopSearchXMLParser searchXMLParser = new ShopSearchXMLParser(driver,
				translator, elementDefinitionManager);

		ShopRestCallParser restCallParser = new ShopRestCallParser(driver,
				translator, elementDefinitionManager);

		ShopHeaderPage headerPage = new ShopHeaderPage(driver, translator,
				elementDefinitionManager);
			
		String currentURL = driver.getCurrentUrl();
				
		StepInfo.addMessage(String.format("Search URL: %s", currentURL));

		System.out.println(currentURL);

		String modifiedURL = driver.getCurrentUrl().replace("+", " ");

		driver.navigate().refresh();

		StepInfo.addMessage(String.format("Rest Call URL: %s",
				debugModePage.getRestCallURL()));

		System.out.println(String.format("Rest Call URL: %s",
				debugModePage.getRestCallURL()));

		StepInfo.addMessage(String.format("Search URL: %s",
				WordUtils.wrap(restCallParser.getSearchURL(), 175, "\n", true)));

		// List<String> oPcontainerID =
		// searchXMLParser.getOPContainerID(restCallParser.getSearchURL());

		List<String> productTitles = searchXMLParser
				.getContainerNames(restCallParser.getSearchURL());

		if (!locale.getDisplayCountry().equals("Taiwan") || locale.getDisplayCountry().equals("Hong Kong")) {
			boolean searchCount = searchResultsPage.almostOrEqualSearchCount();
			Validate.validateTrue(searchCount,
					"Expected Search XML count to almost or equal to search results UI count");
		}

		searchResultsPage.comparingListStrings();

		homePage.navigateToDebugPage(locale);

		debugModePage.turnOffDebugMode();

		driver.navigate().to(currentURL);

		homePage.checkMboxisDisabledOnURL(locale);

		Validate.validateContains(
				String.format(searchResultsPage.searchResultsHeaderString()),
				customParameters.get("Search"),
				"Expected search term to match search results");

		if (customParameters.get("ResultsInsteadMsg").equals("FALSE")) {
			Validate.validateContains(
					String.format(searchResultsPage.searchResultsBreadcrumb()),
					customParameters.get("Search"),
					"Expected search term to be contained in breadcrumb results");
		}

		// Validate not found message
		if (customParameters.get("ResultsInsteadMsg").equals("TRUE")) {
			StepInfo.addMessage(String.format("%s",
					searchResultsPage.getNotFoundMsg()));
			Validate.validateContains(searchResultsPage.getNotFoundMsg(),
					customParameters.get("Search"),
					"Check message contains search term");
		}

		// Validate View Per Page
		if (customParameters.get("ViewPerPage").equals("NULL")) {
			Validate.validateContains(currentURL, "k=60",
					"Validating url contains default k=60");
		}

		if (!customParameters.get("ViewPerPage").equals("NULL")) {
			Validate.validateContains(currentURL,
					customParameters.get(String.format("ViewPerPage")),
					"Validating url contains (set) view per page");
		}

		// Validate Sort
		if (!customParameters.get("Sort").equals("NULL")) {
			if (customParameters.get("Sort").equals("Price Low-High")) {
				Validate.validateContains(currentURL, "sort_price_low",
						"Validating url contains sort low-high");
			}
			if (customParameters.get("Sort").equals("Price High-Low")) {
				Validate.validateContains(currentURL, "sort_price_high",
						"Validating url contains sort high-low");
			}
			if (customParameters.get("Sort").equals("Newest Arrival")) {
				Validate.validateContains(currentURL, "sort_new",
						"Validating url contains sort new");
			}
		}

		// Validate Next Page
		if (customParameters.get("NextPage").equals("TRUE")) {
			Validate.validateContains(currentURL, "i=1",
					"Validating url contains i=1 for 1 page over");
		}

		if (locale.getDisplayCountry().equals("Taiwan") || locale.getDisplayCountry().equals("Hong Kong")) {
			System.out.println(headerPage.URLDecoder(modifiedURL));
			Validate.validateContains(headerPage.URLDecoder(modifiedURL),
					customParameters.get("Search"),
					"Expected search term to be contain in url");
		} else {
			Validate.validateContains(modifiedURL,
					customParameters.get("Search"),
					"Expected search term to be contain in url");
		}

		/*
		 * Validate.validateListContainsAll(searchResultsPage.getOpcontainerID(),
		 * oPcontainerID,
		 * "Expected Search URL OPContainerID to match search UI OPContainerID"
		 * );
		 */

		List<String> unescapeXml = headerPage.sortList(headerPage
				.unescapeXmlListOfStrings(productTitles));

		Validate.validateListContainsAll(headerPage.sortList(headerPage
				.escapeHtmlListOfStrings(searchResultsPage.getTitleName())),
				headerPage.escapeHtmlListOfStrings(unescapeXml),
				"Expected Search URL Product Names to match search UI Product Names");

		System.out
				.println(String
						.format("Expected Title List Search Results UI: %s -  Acutal Title List Search URL_XML: %s",
								headerPage.sortList(headerPage
										.escapeHtmlListOfStrings(searchResultsPage
												.getTitleName())), headerPage
										.escapeHtmlListOfStrings(unescapeXml)));

	}
}
