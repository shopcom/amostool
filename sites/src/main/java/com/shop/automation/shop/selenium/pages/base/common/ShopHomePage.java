package com.shop.automation.shop.selenium.pages.base.common;

import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.testdata.User;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automationframework.tools.MarketAmericaSiteURLs;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.shop.automation.shop.configuration.ShopSiteConfiguration;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;
import com.shop.automation.shop.selenium.pages.base.common.shopadvisor.ShopAdvisorWelcomePage;

public class ShopHomePage extends ShopTemplatePage {

	// @FindBy(xpath="//a[@href='/nbts/login-myaccount.xhtml?returnurl=http%3A%2F%2Fwww.shop.com%2F']")
	@FindBy(xpath = "//a[contains(@href,'/nbts/login-myaccount.xhtml')]")
	@CacheLookup
	private WebElement signInLink;

	public ShopHomePage(final WebDriver driver, final Translator translator,
			final ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}

	public void checkMboxisDisabledOnURL(final Locale locale) {
		String url = MarketAmericaSiteURLs.determineURL(
				new ShopSiteConfiguration(locale),
				Environment.getCurrentEnvironment(), translator.getLocale());
		if (driver.getCurrentUrl().equals(url)) {
			if (url.endsWith("/")) {
				url = url.substring(0, url.length() - 1);
			}
			driver.navigate().to(driver.getCurrentUrl() + "?mboxDisable=1");
		}

		else if (((!driver.getCurrentUrl().contains("mboxDisable=1")) || (!driver
				.getCurrentUrl().contains("mbox")))) {
			if (driver.getCurrentUrl().equals(url)) {
				if (url.endsWith("/")) {
					url = url.substring(0, url.length() - 1);

					driver.navigate().to(
							driver.getCurrentUrl() + "&mboxDisable=1");
				}
			}
		}
	}

	public void navigateToDebugPage(final Locale locale) {
		String url = MarketAmericaSiteURLs.determineURL(
				new ShopSiteConfiguration(locale),
				Environment.getCurrentEnvironment(), translator.getLocale());
		if (url.endsWith("/")) {
			url = url.substring(0, url.length() - 1);
		}
		StepInfo.addMessage(String.format("Entered debug Mode"));
		driver.navigate().to(String.format("%s/debug.xhtml", url));
		StepInfo.addMessage(String.format("Navigated to URL: %s",
				driver.getCurrentUrl()));
	}

	public void navigateToPortal(final Map<String, String> customParameters,
			final Locale locale) {
		String url = MarketAmericaSiteURLs.determineURL(
				new ShopSiteConfiguration(locale),
				Environment.getCurrentEnvironment(), translator.getLocale());
		if (url.endsWith("/")) {
			url = url.substring(0, url.length() - 1);
		}
		driver.navigate().to(
				String.format("%s/%s", url, customParameters.get("Portal")));
		StepInfo.addMessage(String.format("Navigated to URL: %s/%s", url,
				customParameters.get("Portal")));
		System.out.println("Navigated to Portal");
	}

	public void navigateToSourceID(final Map<String, String> customParameters,
			final Locale locale) {
		String url = MarketAmericaSiteURLs.determineURL(
				new ShopSiteConfiguration(locale),
				Environment.getCurrentEnvironment(), translator.getLocale());
		if (url.endsWith("/")) {
			url = url.substring(0, url.length() - 1);
		}
		driver.navigate().to(
				String.format("%s/?sourceid=%s", url,
						customParameters.get("SourceID")));
		StepInfo.addMessage(String.format("Navigated to URL: %s?%s", url,
				customParameters.get("SourceID")));
		System.out.println("Navigated to SourceId");
	}

	public boolean getStagingBanner() {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try {
			if (this.isElementExistingAndDisplayed(By
					.xpath("//div[(@class='super_header')]"))) { // isElementExistingAndDisplayed(findElement(By
				// .xpath("//div[(@class='super_header')]")))){
				return true;
			} else if (this.isElementExistingAndDisplayed(By
					.xpath("//div[(@id='page')]//div[(@id='stagingBanner')]"))) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		} finally {
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		}

	}

	public void checkEnvironment(Locale locale) {
		if (WebDriverUserAgent.getCurrentUserAgent().equals(
				WebDriverUserAgent.DEFAULT)) {

			if (Environment.getCurrentEnvironment().equals(Environment.STAGING)) {
				assert (getStagingBanner() == true);
				System.out.println("You are on Staging Environment");
			}
			if (Environment.getCurrentEnvironment().equals(Environment.LIVE)) {
				assert (getStagingBanner() == false);
				System.out.println("You are on Live Environment");
			}
		}

		else {
			final ShopFooter footer = new ShopFooter(driver, translator,
					elementDefinitionManager);

			String appServer = footer.getNewAppServer(locale);
			if (Environment.getCurrentEnvironment().equals(Environment.STAGING)) {
				assert (appServer.contains("stg"));
				System.out.println("You are on Staging Environment");
			}

			if (Environment.getCurrentEnvironment().equals(Environment.LIVE)) {
				assert (!appServer.contains("stg"));
				System.out.println("You are on Live Environment");
			}

		}

	}

	public Boolean isFirstTimePromo(final Map<String, String> customParameters) {
		if (customParameters.containsKey("SourceId")) {
			if (!customParameters.get(String.format("SourceId")).equals("NULL")) {
				String sourceId = customParameters.get(String
						.format("SourceId"));
				if (sourceId.equals("2822") || sourceId.equals("2834")
						|| sourceId.equals("1618") || sourceId.equals("1571")
						|| sourceId.equals("1537")) {
					return true;
				} else {
					return false;
				}
			}
		}

		return false;

	}

	public ShopFamosSignInPage shopAdvisorWelcomeNotLoggedIn(WebDriver driver,
			Translator translator,
			ElementDefinitionManager elementDefinitionManager, Locale locale) {

		String url = MarketAmericaSiteURLs.determineURL(
				new ShopSiteConfiguration(locale),
				Environment.getCurrentEnvironment(), translator.getLocale());
		if (url.endsWith("/")) {
			url = url.substring(0, url.length() - 1);
		}
		driver.navigate().to(String.format("%s/shoppingadvisor/welcome", url));
		StepInfo.addMessage(String.format("Navigated to URL: %s",
				driver.getCurrentUrl()));

		return new ShopFamosSignInPage(driver, translator,
				elementDefinitionManager);
	}

	public ShopAdvisorWelcomePage shopAdvisorWelcomeLoggedIn() {

		String url = MarketAmericaSiteURLs.determineURL(
				new ShopSiteConfiguration(translator.getLocale()),
				Environment.getCurrentEnvironment(), translator.getLocale());
		if (url.endsWith("/")) {
			url = url.substring(0, url.length() - 1);
		}
		driver.navigate().to(String.format("%s/shoppingadvisor/welcome", url));
		StepInfo.addMessage(String.format("Navigated to URL: %s",
				driver.getCurrentUrl()));

		return new ShopAdvisorWelcomePage(driver, translator,
				elementDefinitionManager);
	}

	public ShopFamosSignInPage signInFromHomePage() {

		signInLink.click();
		return new ShopFamosSignInPage(driver, translator,
				elementDefinitionManager);
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(getBody());
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		header.closeCountrySelectionPopup();
		throw new UnableToVerifyPagesExistenceException(
				"Unable to determine if we are on the home page");
	}

	/**
	 * Checks to see if the correct user is logged in
	 *
	 * @param user
	 *            to check to see if we are signed in
	 * @return boolean indicating if the user is signed in
	 */
	public <T extends User> boolean isSignedIn(final T user) {
		return header.isSignedIn(user);
	}
}
