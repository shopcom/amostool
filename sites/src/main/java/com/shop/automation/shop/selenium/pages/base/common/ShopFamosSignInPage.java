package com.shop.automation.shop.selenium.pages.base.common;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.MarketAmericaSiteURLs;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.shop.automation.shop.configuration.ShopSiteConfiguration;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;
import com.shop.automation.shop.selenium.pages.base.common.shopadvisor.ShopAdvisorWelcomePage;

public class ShopFamosSignInPage extends ShopTemplatePage {

	@FindBy(xpath = "//input[@id='email']")
	@CacheLookup
	private WebElement email;

	@FindBy(xpath = "//input[@id='j_password']")
	@CacheLookup
	private WebElement password;

	@FindBy(xpath = "//button[@class='button primary right']")
	@CacheLookup
	private WebElement signInButton;

	@FindBy(xpath = ("//meta[@name='description']"))
	@CacheLookup
	private WebElement descriptionTag;

	public ShopFamosSignInPage(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager) {

		super(driver, translator, elementDefinitionManager);

	}

	private void signIn(String userEmail, String userPassword) {

		email.sendKeys(userEmail);
		password.sendKeys(userPassword);
		signInButton.click();
	}

	public ShopAdvisorWelcomePage signInFromAdvisorWelcomePage(
			String userEmail, String userPassword) {

		signIn(userEmail, userPassword);
		return new ShopAdvisorWelcomePage(driver, translator,
				elementDefinitionManager);
	}

	public ShopHomePage signInFromHomePage(String userEmail, String userPassword) {

		signIn(userEmail, userPassword);
		return new ShopHomePage(driver, translator, elementDefinitionManager);
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return true;

	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return null;
	}

}
