package com.shop.automation.shop.selenium.pages.validations;

import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Validate;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopOrderSummarySection;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopPlaceOrderPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopCustomParameters;
import com.shop.automation.shop.selenium.pages.base.common.ShopTestVariables;

public class ShopPlaceOrderPageValidation {

	public ShopPlaceOrderPageValidation(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager,
			final Map<String, String> customParameters) {

		ShopPlaceOrderPage PlaceOrderPage = new ShopPlaceOrderPage(driver,
				translator, elementDefinitionManager);

		ShopOrderSummarySection OrderSummarySection = new ShopOrderSummarySection(
				driver, translator, elementDefinitionManager, customParameters);

		ShopCustomParameters shopCustomParameters = new ShopCustomParameters(
				driver, translator, elementDefinitionManager, customParameters);
		
		ShopTestVariables testVariables = new ShopTestVariables(driver, translator, elementDefinitionManager);
		
		String invoiceTaxValue = customParameters.get("InvoiceTaxValue");

		if ((customParameters.get("GiftCardApplied").equals(
				"FALSE")) && (customParameters.get("CashBackApplied").equals("FALSE")) || (customParameters.get("CashBackApplied").equals("FALSE"))) {

			waitForElementToBeVisible(By.id("pyoPaymentInfoContainer"), true, 10);
			StepInfo.addMessage(String.format("Card Type: %s %s", PlaceOrderPage.CreditCardType(), PlaceOrderPage.CreditCardNumber()));

			Validate.validateEquals(PlaceOrderPage
					.getCreditCardInfoOrderTotal(),
					OrderSummarySection.getOrderTotalOS(),
					"Place Order Page: Validating Payment Info 'Credit Card Total'");

		}

		if ((customParameters.get("GiftCardApplied").equals(
				"TRUE")) && (customParameters.get("CashBackApplied").equals("TRUE")) || (customParameters.get("CashBackApplied").equals("TRUE"))) {
			waitForElementToBeVisible(By.xpath("//div[@class='cashbackPaymentContainer fl margB5 fullWidth']"), true, 10);
			StepInfo.addMessage(String.format("%s", PlaceOrderPage.getCashbackAppliedAmount()));

		}
	
		Validate.validateEquals(PlaceOrderPage.getInvoiceVendorItemsTotal(),
				PlaceOrderPage.getFinalOrderSubtotal(),
				"Place Order Page: Validating Vendor(s) Invoice Items totals matches Order Subtotal");

		Validate.validateEquals(
				PlaceOrderPage.getInvoiceVendorShippingTotal(),
				PlaceOrderPage.getFinalShippingTotal(),
				"Place Order Page: Validating Vendor(s) Shipping totals matches Order Shipping Total");

		Validate.validateEquals(PlaceOrderPage.getInvoiceVendorSalesTaxTotal(customParameters),
				PlaceOrderPage.getFinalSalesTaxTotal(customParameters),
				"Place Order Page: Validating Vendor(s) Sales Tax totals matches Order Sales Tax Total");
		
		Validate.validateEquals(
				PlaceOrderPage.getInvoiceVendorOrderTotal(customParameters),
				PlaceOrderPage.getFinalOrderTotal(), "Place Order Page: Validating"
						+ "Vendor(s) Order Total matches Order Total");
		
		if(!customParameters.get("OrderCashbackEarned").equals("NULL"))
		{
			Validate.validateEquals(
			PlaceOrderPage.getInvoiceVendorCashbackTotal(),
			PlaceOrderPage.getFinalCashBackEarned(),"Place Order Page: Validating"
							+ "Vendor(s) Cashback total matches Order Cashback total");
		}
		
		if (testVariables.isFirstTimePromo(customParameters)==true){
			Validate.validateTrue(PlaceOrderPage.checkDiscountIsAppliedInvoice(customParameters), "Place Order Page: Validating Vendor(s) first time discount is applied");
		}

		StepInfo.addMessage(String
				.format("Order Subtotal: %s Sales Tax Total: %s Shipping Total: %s = Order Total: %s",
						PlaceOrderPage.getFinalOrderSubtotal(),
						PlaceOrderPage.getFinalSalesTaxTotal(customParameters),
						PlaceOrderPage.getFinalShippingTotal(),
						PlaceOrderPage.getFinalOrderTotal()));


		Validate.validateEquals(PlaceOrderPage.getFinalOrderTotal(),
				OrderSummarySection.getOrderTotalOS(),
				"Place Order Page: Summary Section and Final Order total's match");

		Validate.validateEquals(
				PlaceOrderPage.getInvoiceVendorItemsTotal(),
				shopCustomParameters
						.getCPOrderSubTotal(customParameters),
				"Place Order Page: Validating Vendor(s) Invoice Items subtotal matches Custom Parameter OrderSubTotal");

		Validate.validateEquals(
				PlaceOrderPage.getInvoiceVendorSalesTaxTotal(customParameters),
				shopCustomParameters.getCPOrderTaxValue(customParameters),
				"Place Order Page: Validating Vendor(s) Invoice Sales Tax total matches Custom Parameter OrderTaxValue");

		Validate.validateEquals(
				PlaceOrderPage.getInvoiceVendorShippingTotal(),
				shopCustomParameters
						.getCPOrderShipValue(customParameters),
				"Place Order Page: Validating Vendor(s) Invoice Shipping total matches Custom Parameter OrderShipValue");

		Validate.validateEquals(
				PlaceOrderPage.getInvoiceVendorOrderTotal(customParameters),
				shopCustomParameters.getCPOrderTotal(customParameters),
				"Place Order Page: Validating Vendor(s) Order Total matches Custom Parameter OrderTotal");

		if(!customParameters.get("OrderCashbackEarned").equals("NULL"))
		{
			Validate.validateEquals(
			PlaceOrderPage.getInvoiceVendorCashbackTotal(),
			shopCustomParameters.getCPOrderCashbackEarned(customParameters),
					"Place Order Page: Validating Vendor(s) Cashback total matches Custom Parameter OrderCashBackEarned");
		}
		
		Validate.validateEquals(PlaceOrderPage.getFinalOrderSubtotal(),
				shopCustomParameters.getCPOrderSubTotal(customParameters),
				"Place Order Page: Validating Order Subtotal matches Custom Parameter OrderSubTotal");

		Validate.validateEquals(
				PlaceOrderPage.getFinalSalesTaxTotal(customParameters),
				shopCustomParameters.getCPOrderTaxValue(customParameters),
				"Place Order Page: Validating Order Sales Tax Total matches Custom Parameter OrderTaxValue");

		Validate.validateEquals(PlaceOrderPage.getFinalShippingTotal(),
				shopCustomParameters.getCPOrderShipValue(customParameters),
				"Place Order Page: Validating Order Shipping Total matches Custom Parameter OrderShipValue");

		Validate.validateEquals(PlaceOrderPage.getFinalOrderTotal(),
				shopCustomParameters.getCPOrderTotal(customParameters),
				"Place Order Page: Validating"
						+ "Order Total matches Custom Parameter OrderTotal");
		
		if(!customParameters.get("OrderCashbackEarned").equals("NULL"))
		{
			Validate.validateEquals(
			PlaceOrderPage.getFinalCashBackEarned(),
			shopCustomParameters.getCPOrderCashbackEarned(customParameters),
			"Place Order Page: Validating Order Cashback Total matches Custom Parameter OrderCashbackEarned");
		}
		
		//If Tax is NULL, verify that Tax is not displayed
		if(invoiceTaxValue.equals("NULL"))
		{
			Validate.validateFalse(OrderSummarySection.getTaxLabelOS().isDisplayed(), "Place Order Page: Validating Tax is not Displayed in the Order Summary");
			//System.out.println("Tax order total: " + PlaceOrderPage.getTaxOrderTotal().isEnabled());
			//Validate.validateFalse(PlaceOrderPage.getTaxOrderTotal().isDisplayed(), "Place Order Page: Validating Tax is not Displayed in the Order Total Summary");
		}
		}

	private void waitForElementToBeVisible(By id, boolean b, int i) {
		// TODO Auto-generated method stub

	}
	
}
