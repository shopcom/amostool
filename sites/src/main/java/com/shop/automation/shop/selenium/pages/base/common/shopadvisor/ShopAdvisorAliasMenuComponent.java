package com.shop.automation.shop.selenium.pages.base.common.shopadvisor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.shop.automation.shop.selenium.pages.base.ShopPageComponent;

public class ShopAdvisorAliasMenuComponent extends ShopPageComponent {

	public ShopAdvisorAliasMenuComponent(WebDriver driver,
			Translator translator,
			final ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

		PageFactory.initElements(this.driver, this);

	}

	// Following procedures pulled directly from Rick's search. Need to be
	// adjusted
	public List<String> getAliases() {
		List<WebElement> leftNav = driver.findElements(By.id("refine-search"));
		List<String> lnav = new ArrayList<String>();
		for (int i = 0; i < leftNav.size(); i++) {
			lnav.add(leftNav.get(i).getText());
		}
		return lnav;
	}

	public WebElement getRefineSearchLink(
			final Map<String, String> customParameters) {
		WebElement refineSearchLinkCheckBox = driver
				.findElement(By
						.xpath("//div[(@id='main-content')]//section[(@id='refine-search')]//li[contains(., '"
								+ customParameters.get("SearchRefineLeftNav")
								+ "')]/a/input[@type='checkbox']"));
		return refineSearchLinkCheckBox;
	}

	public void clickRefineLeftNav(final Map<String, String> customParameters) {
		if (!customParameters.get("SearchRefineLeftNav").equals("NULL")) {
			this.waitForjQueryAjax("Wait for page to load");
			getRefineSearchLink(customParameters).click();
			StepInfo.addMessage(String.format("Clicked on %s left nav",
					customParameters.get("SearchRefineLeftNav")));
		}
	}

}
