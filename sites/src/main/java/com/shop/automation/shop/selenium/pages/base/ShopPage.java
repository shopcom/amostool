package com.shop.automation.shop.selenium.pages.base;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import org.openqa.selenium.WebDriver;

import com.marketamerica.automationframework.pages.Page;
import com.marketamerica.automation.utilities.helpers.Translator;

public abstract class ShopPage extends Page {

    public ShopPage(final WebDriver driver, final Translator translator, ElementDefinitionManager elementDefinitionManager) {
        super(driver, translator, elementDefinitionManager);
    }

}
