package com.shop.automation.shop.selenium.pages.base.checkout;


import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.selenium.pages.base.ShopPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;



public class ShopEnetsPage extends ShopPage {
	public ShopEnetsPage(final WebDriver driver, final Translator translator, ElementDefinitionManager elementDefinitionManager, final CreditCard creditCard){
		super(driver, translator,elementDefinitionManager);
			try {
				Verify.verifyTrue(atPage(), "Expected to be on eNets page");
				final String orderInformation = String.format("Order Number: %s", getShopOrderID());
				StepInfo.addMessage(orderInformation);
			} catch (UnableToVerifyPagesExistenceException e) {
				StepInfo.failTest("Unable to verify existence of eNets page");
			}
	
		}
	
	public String getShopOrderID() {
		return findElement(By.xpath(".//*[@id='workingArea']/table/tbody/tr/td/form/p/table/tbody/tr[3]/td/table/tbody/tr[4]/td[2]")).getText();
	}
	
	public void completeEnetsOrder(final CreditCard creditCard)
	{
			//Complete the eNets Test simulator
			this.waitForElementToBePresent(
					By.xpath("html/body/table/tbody/tr/td/table/tbody/tr[1]/td/table/tbody/tr[3]/td[2]/img"),
					true, 15);
			WebElement submit = driver.findElement(By.xpath(".//*[@id='submit']"));
			submit.click();
			//Bank Selction Page 
			WebElement submit2 = driver.findElement(By.xpath(".//*[@id='test']"));
			Select bankdropDown = new Select(driver.findElement(By.xpath("//select[@name='selBankID']")));
			bankdropDown.selectByVisibleText("UAT BankSim (E3 UAT only)");
			submit2.click();
			//Test Process Simulator
			WebElement process = driver.findElement(By.xpath("//input[@value='Process']"));
			process.click();
			//close Country selection Popup window if exists
			ShopHomePage homePage = new ShopHomePage(driver, translator,
					elementDefinitionManager);
			homePage.header.closeCountrySelectionPopup();
			
	}
	
	private String waitForLoad() {
		return waitForElementToBePresent(By.xpath(".//*[@id='workingArea']/table/tbody/tr/td/form/p/table/tbody/tr[3]/td/table/tbody/tr[4]/td[2]"), true, 15)
				.getText();
	}


	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		waitForLoad();
		return By.xpath(".//*[@id='workingArea']/table/tbody/tr/td/form/p/table/tbody/tr[3]/td/table/tbody/tr[4]/td[2]");
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(getBody(), 30);
	}

}
