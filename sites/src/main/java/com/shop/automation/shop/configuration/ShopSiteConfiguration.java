package com.shop.automation.shop.configuration;

import java.util.Locale;

import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automationframework.tools.MarketAmericaSiteURLs;
import com.marketamerica.automationframework.tools.datamanagement.DataManager;

/**
 * This class holds all the urls  for Shop.com Web Sites
 * Created by Javier L. Velasquez on 5/9/2014.
 * Modified by Rick Vera on 5/14/2014.
 */
public final class ShopSiteConfiguration extends MarketAmericaSiteURLs{

    /**
     * This class holds all the Shop.com "Live and LTV" Sites
     */

	public ShopSiteConfiguration(final Locale locale) {
		populateDevelopmentSites();
		populateStagingSites(locale);
		populateLiveSites(locale);
	}

	private void populateDevelopmentSites() {
		this.developmentSites.put(Country.AUSTRALIA, "http://dev-au.shop.com/");
		this.developmentSites.put(Country.CANADA, "http://dev-ca.shop.com/");
		this.developmentSites.put(Country.TAIWAN, "http://dev-tw.shop.com/");
		this.developmentSites.put(Country.UNITED_STATES, "http://dev-shop.com/");
		this.developmentSites.put(Country.MEXICO, "http://dev-mexico.shop.com/");
		this.developmentSites.put(Country.SPAIN, "http://dev-spain.shop.com/");
		this.developmentSites.put(Country.UNITED_KINGDOM, "http://dev-uk.shop.com/");
		this.developmentSites.put(Country.SINGAPORE, "http://dev-sg.shop.com/");
		this.developmentSites.put(Country.HONG_KONG,"http://dev-hk.shop.com/");

	}

	private void populateStagingSites(final Locale locale) {
		
		switch(locale.getCountry())
		{
			
		case "US":
			this.stagingSites.put(Country.UNITED_STATES, "http://www.shop.com");
			break;
		case "AU":
			this.stagingSites.put(Country.AUSTRALIA, "http://au.shop.com");
			break;
		case "CA":
			if (locale.toString().contains("fr"))
			{
				this.stagingSites.put(Country.CANADA, "http://cafr.shop.com/");
			}
			else
			{
				this.stagingSites.put(Country.CANADA, "http://ca.shop.com/");
			}
			break;
		case "TW":
			this.stagingSites.put(Country.TAIWAN, "http://tw.shop.com/");
			break;
		case "MX":
			this.stagingSites.put(Country.MEXICO, "http://mexico.shop.com");
			break;
		case "ES":
			this.stagingSites.put(Country.SPAIN, "http://es.shop.com/");
			break;
		case "UK":
		case "GB":
		case "UNITED KINGDOM":
			this.stagingSites.put(Country.UNITED_KINGDOM, "http://uk.shop.com/");
			break;
		case "SG":
			this.stagingSites.put(Country.SINGAPORE,"http://sg.shop.com/");
			break;
		case "HK":
			if (locale.toString().contains("en"))
			{
				this.stagingSites.put(Country.HONG_KONG,"http://hken.shop.com/");
			}
			else
			{
				this.stagingSites.put(Country.HONG_KONG,"http://hk.shop.com/");
			}
			break;
		default:
			this.stagingSites.put(Country.UNITED_STATES, "http://www.shop.com");
		
		}
	}

	private void populateLiveSites(final Locale locale) {
		switch(locale.getCountry())
		{
			
		case "US":
			if (locale.toString().contains("ma"))
			{
				this.liveSites.put(Country.UNITED_STATES, "http://marketing.shop.com");
			}
			else
			{
				this.liveSites.put(Country.UNITED_STATES, "http://www.shop.com");
			}

			break;
		case "AU":
			this.liveSites.put(Country.AUSTRALIA, "http://au.shop.com");
			break;
		case "CA":
			if (locale.toString().contains("fr"))
			{
				this.liveSites.put(Country.CANADA, "http://cafr.shop.com/");
			}
			else
			{
				this.liveSites.put(Country.CANADA, "http://ca.shop.com/");
			}
			break;
		case "TW":
			this.liveSites.put(Country.TAIWAN, "http://tw.shop.com/");
			break;
		case "MX":
			this.liveSites.put(Country.MEXICO, "http://mexico.shop.com");
			break;
		case "ES":
			this.liveSites.put(Country.SPAIN, "http://es.shop.com/");
			break;
		case "UK":
		case "GB":
		case "UNITED KINGDOM":
			this.liveSites.put(Country.UNITED_KINGDOM, "http://uk.shop.com/");
			break;
		case "SG":
			this.liveSites.put(Country.SINGAPORE,"http://sg.shop.com/");
			break;
		case "HK":
			if (locale.toString().contains("en"))
			{
				this.liveSites.put(Country.HONG_KONG,"http://hken.shop.com/");
			}
			else
			{
				this.liveSites.put(Country.HONG_KONG,"http://hk.shop.com/");
			}
			break;
		default:
			this.liveSites.put(Country.UNITED_STATES, "http://www.shop.com");
		
		}
	}
}


