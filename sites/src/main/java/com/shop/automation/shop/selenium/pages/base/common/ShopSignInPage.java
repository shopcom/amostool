package com.shop.automation.shop.selenium.pages.base.common;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;

class ShopSignInPage extends ShopTemplatePage {

	ShopSignInPage(final WebDriver driver, final Translator translator, ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
				
		try {
			Verify.verifyTrue(atPage(), "Expected to be on sign in page");
			header.closeCountrySelectionPopup();
		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence of sign in page");
		}
	}

	public WebElement getEmailAddressTextBox(){
		WebElement userNameField = driver.findElement(By.id("sign_in_email_textfield"));
		return userNameField;
		
	}
	
	public WebElement getPasswordTextField(){
		WebElement passwordField = driver.findElement(By.id("sign_in_password_textfield"));
		return 	passwordField;	
	}
	
	public WebElement getSignInButton(){
		WebElement submitLoginFormButton = driver.findElement(By.id("signin_button"));
		return submitLoginFormButton;
	}
	
	public String getSignInErrorMessage(){
		String signInErrorMessageElement = driver.findElement(By.id("sign_in_error")).getText();
		return signInErrorMessageElement;
	}
	
	public WebElement getNoAcctRadioButton(){
		WebElement noAcctRadioButton = driver.findElement(By.id("no_account_radio_button"));
		return noAcctRadioButton;
	}
	
	void SignIn(Shopper shopper){
		getEmailAddressTextBox().sendKeys(shopper.getEmail());
		getPasswordTextField().sendKeys(shopper.getPassword());
		this.waitForElementToBeClickable(By.id("signin_button"),true, 10);
		Actions action = new Actions(driver);
		action.doubleClick(getSignInButton()).perform();
	}
	
	public void CreateNewAcct(Shopper shopper){
		getNoAcctRadioButton().click();
		getEmailAddressTextBox().sendKeys(shopper.getEmail());
		this.waitForElementToBeClickable(By.id("signin_button"),true, 10);
		Actions action = new Actions(driver);
		action.doubleClick(getSignInButton()).perform();
	}
	
	private void waitForLoad() {
		this.waitForjQueryAjax("Waiting for page to load");
		waitForElementToBeVisible(By.id("signin_button"), 15);
	}

		

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(getBody(), 15);
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		waitForLoad();
		return By.xpath("//form[(@id='form_sign_in')]");
	}
}