package com.shop.automation.shop.selenium.pages.base.checkout;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.selenium.pages.base.ShopPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopCustomParameters;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;

public class ShopPlaceOrderPage extends ShopPage {
	public ShopPlaceOrderPage(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

		try {
			Verify.verifyTrue(atPage(), "Expected to be on Place Order Page");
		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence of place order page");
		}
	}
	
		public WebElement getTaxOrderTotal() {
		WebElement taxOrderTotal = findElement(By
				.id("finalOS-orderSalesTaxTotal"));
		return taxOrderTotal;
	}

	public String getBillingInfo() {
		return findElement(By.xpath("//span[@id='billingInfo']")).getText();

	}

	public String BillingInfo() {
		final String billingInfo = getBillingInfo();
		return billingInfo;

	}

	public String getCashbackAppliedAmount() {
		String cashbackAppliedAmount = findElement(
				By.xpath("//div[@class='cashbackPaymentContainer fl margB5 fullWidth']"))
				.getText();
		return cashbackAppliedAmount;

	}

	public String getPaymentInfo() {
		String paymentInfo = findElement(
				By.xpath("//div[@class='ccInfoContainer2 fl margB5 fullWidth']"))
				.getText();
		return paymentInfo;

	}

	public String getCCtype() {
		this.waitForjQueryAjax("Waiting for page to load");
		return findElement(By.id("ccType")).getText().toString();

	}

	public String CreditCardType() {
		return getCCtype();

	}

	public String getCCnumber() {
		this.waitForjQueryAjax("Waiting for page to load");
		return findElement(By.id("ccNumber")).getText().toString();
	}

	public String CreditCardNumber() {
		return getCCnumber();

	}

	public BigDecimal getCreditCardInfoOrderTotal() {
		/*
		 * if ((customParameters.get(String.format("CashBackApplied")).equals(
		 * "FALSE")) && (customParameters.get("GiftCard")).equals("FALSE")) {
		 */
		waitForElementToBeVisible(By.id("creditCardInfoOrderTotal"), 10);
		BigDecimal creditCardInfoOrderTotal = CurrencyUtilities
				.convertPriceStringToBigDecimal(findElement(
						By.id("creditCardInfoOrderTotal")).getText().trim());

		return creditCardInfoOrderTotal;
		/*
		 * } else // Subtract Cashback applied in final order summary from cback
		 * // applied in cc section { BigDecimal cbackApplied =
		 * CurrencyUtilities .convertPriceStringToBigDecimal(findElement(
		 * By.id("finalOS-orderCashbackCreditTotal")) .getText().trim());
		 * BigDecimal creditCardInfoOrderTotal = CurrencyUtilities
		 * .convertPriceStringToBigDecimal(findElement(
		 * By.id("cashbackInfoOrderTotal")).getText().trim());
		 * creditCardInfoOrderTotal = cbackApplied
		 * .subtract(creditCardInfoOrderTotal); return creditCardInfoOrderTotal;
		 * }
		 */
	}

	public BigDecimal getGCAmountTotal() {
		BigDecimal gcAmountTotal = CurrencyUtilities
				.convertPriceStringToBigDecimal(findElement(
						By.id("giftCardInfoOrderTotal")).getText().trim());
		return gcAmountTotal;

	}

	public BigDecimal getInvoiceVendorItemsTotal() {
		List<WebElement> invoiceVenderItemTotal = driver
				.findElements(By
						.xpath("//div[@class='pyoCartItemTemplate' and contains(@style, 'display:block')]//span[contains(@class, 'vendorItemsTotal')]"));
		BigDecimal venderItemsTotal = new BigDecimal(0);
		for (int i = 0; i < invoiceVenderItemTotal.size(); ++i) {
			venderItemsTotal = venderItemsTotal.add(CurrencyUtilities
					.convertPriceStringToBigDecimal(invoiceVenderItemTotal
							.get(i).getText().trim()));
		}
		return venderItemsTotal;
	}

	public BigDecimal getInvoiceVendorSalesTaxTotal(
			final Map<String, String> customParameters) {
		BigDecimal vendorItemsSalesTaxTotal = new BigDecimal(0);
		if (!customParameters.get("InvoiceTaxValue").equals(
				"NULL")) {
			List<WebElement> invoiceVendorSalesTaxTotal = driver
					.findElements(By
							.xpath("//div[@class='pyoCartItemTemplate' and contains(@style, 'display:block')]//span[contains(@class, 'vendorSalesTaxTotal')]"));
			for (int i = 0; i < invoiceVendorSalesTaxTotal.size(); ++i) {
				vendorItemsSalesTaxTotal = vendorItemsSalesTaxTotal
						.add(CurrencyUtilities
								.convertPriceStringToBigDecimal(invoiceVendorSalesTaxTotal
										.get(i).getText().trim()));
			}
		}
		return vendorItemsSalesTaxTotal;
	}

	public BigDecimal getInvoiceVendorShippingTotal() {
		List<WebElement> invoiceVendorShippingTotal = driver
				.findElements(By
						.xpath("//div[@class='pyoCartItemTemplate' and contains(@style, 'display:block')]//span[contains(@class, 'vendorShippingTotal')]"));
		BigDecimal vendorShippingTotal = new BigDecimal(0);
		for (int i = 0; i < invoiceVendorShippingTotal.size(); ++i) {
			vendorShippingTotal = vendorShippingTotal.add(CurrencyUtilities
					.convertPriceStringToBigDecimal(invoiceVendorShippingTotal
							.get(i).getText().trim()));
		}
		return vendorShippingTotal;
	}

	public BigDecimal getInvoiceVendorOrderTotal(
			final Map<String, String> customParameters) {
		List<WebElement> invoiceVendorOrderTotal = driver
				.findElements(By
						.xpath("//div[@class='pyoCartItemTemplate' and contains(@style, 'display:block')]//span[contains(@class, 'vendorOrderTotalPrice')]"));
		BigDecimal vendorOrderTotal = new BigDecimal(0);
		for (int i = 0; i < invoiceVendorOrderTotal.size(); ++i) {
			vendorOrderTotal = vendorOrderTotal.add(CurrencyUtilities
					.convertPriceStringToBigDecimal(invoiceVendorOrderTotal
							.get(i).getText().trim()));
		}
		// Subtract cashback if cashback is applied
		if (customParameters.get(String.format("CashBackApplied")).equals(
				"TRUE")) {
			// System.out.println("subtracting cashback");
			BigDecimal CashbackCredit = getCashbackCredit();
			vendorOrderTotal = vendorOrderTotal.subtract(CashbackCredit);

		}
		return vendorOrderTotal;
	}

	public BigDecimal getInvoiceVendorCashbackTotal() {
		List<WebElement> invoiceVendorCashbackTotal = driver
				.findElements(By
						.xpath("//div[@class='pyoCartItemTemplate' and contains(@style, 'display:block')]//span[contains(@class, 'pyoVendorRewardEarned')]"));
		BigDecimal vendorCashbackTotal = new BigDecimal(0);
		for (int i = 0; i < invoiceVendorCashbackTotal.size(); ++i) {
			vendorCashbackTotal = vendorCashbackTotal.add(CurrencyUtilities
					.convertPriceStringToBigDecimal(invoiceVendorCashbackTotal
							.get(i).getText().trim()));
		}
		return vendorCashbackTotal;
	}
	
	public BigDecimal getInvoiceCouponAppliedTotal() {
		List<WebElement> couponDiscount = driver.findElements(By.xpath("//div[(@style='display:block')]//div[(@class='vendorTotalsContainer')]//div[(@class='vendorDiscountsContainer')]//span[@class='vendorDiscountsPrice normal amount']"));
		BigDecimal couponDiscounts = new BigDecimal(0);
		for (int i = 0; i <couponDiscount.size(); ++i){
			couponDiscounts = couponDiscounts.add(CurrencyUtilities
					.convertPriceStringToBigDecimal(couponDiscount.get(i).getText().trim()));
		}
		return couponDiscounts;
	}
	
	public boolean checkDiscountIsAppliedInvoice(final Map<String, String> customParameters){
		BigDecimal vendorTotal = getInvoiceVendorItemsTotal().add(getInvoiceVendorShippingTotal().add(getInvoiceVendorSalesTaxTotal(customParameters)));
		BigDecimal discount = getInvoiceCouponAppliedTotal();
		BigDecimal newDiscountTotal = vendorTotal.subtract(discount);
		
		if (newDiscountTotal.equals(getInvoiceVendorOrderTotal(customParameters))){
			return true;
		}
		else{
			StepInfo.addMessage("Coupon Discount not applied");
			return false;
		}
	}

	/*public BigDecimal getVendorOrderTotal() {
		BigDecimal vendorOrderTotal = CurrencyUtilities
				.convertPriceStringToBigDecimal(findElement(
						By.xpath("//div[(@style='display:block')]//div[(@class='vendorTotalsContainer')]//span[@class='vendorOrderTotalPrice']"))
						.getText().toString().trim());
		return vendorOrderTotal;
	}*/

	public BigDecimal getFinalOrderSubtotal() {
		BigDecimal finalOrderSubtotal = CurrencyUtilities
				.convertPriceStringToBigDecimal(findElement(
						By.xpath("//div[@id='finalOS-orderSubTotal']"))
						.getText().toString().trim());
		return finalOrderSubtotal;
	}

	public BigDecimal getFinalSalesTaxTotal(
			final Map<String, String> customParameters) {
		BigDecimal finalSalesTaxTotal = new BigDecimal("0");
		if (!customParameters.get(String.format("OrderTaxValue"))
				.equals("NULL")) {
			finalSalesTaxTotal = CurrencyUtilities
					.convertPriceStringToBigDecimal(findElement(
							By.xpath("//div[@id='finalOS-orderSalesTaxTotal']"))
							.getText().toString().trim());
		}
		return finalSalesTaxTotal;
	}

	public BigDecimal getFinalShippingTotal() {
		BigDecimal finalShippingTotal = CurrencyUtilities
				.convertPriceStringToBigDecimal(findElement(
						By.xpath("//div[@id='finalOS-orderShippingTotal']"))
						.getText().toString().trim());
		return finalShippingTotal;
	}

	public BigDecimal getFinalOrderTotal() {
		BigDecimal finalOrderTotal = CurrencyUtilities
				.convertPriceStringToBigDecimal(findElement(
						By.xpath("//div[@id='finalOS-orderTotal']")).getText()
						.toString().trim());
		return finalOrderTotal;
	}

	public BigDecimal getFinalCashBackEarned() {
		BigDecimal finalCashBackEarned = CurrencyUtilities
				.convertPriceStringToBigDecimal(findElement(
						By.cssSelector(".finalOSContainers > .finalOS-value.cashbackGreen.bold.plus"))
						.getText().toString().trim());

		return finalCashBackEarned;
	}

	public BigDecimal getFinalCouponDiscount() {
		BigDecimal finalCouponDiscount = CurrencyUtilities
				.convertPriceStringToBigDecimal(findElement(
						By.xpath("//div[@id='finalOS-orderCouponDiscountTotal']"))
						.getText().toString().trim());

		return finalCouponDiscount;
	}

	public BigDecimal getCashbackCredit() {
		BigDecimal finalCashbackDiscount = CurrencyUtilities
				.convertPriceStringToBigDecimal(findElement(
						By.xpath("//div[@id='finalOS-orderCashbackCreditTotal']"))
						.getText().toString().trim());

		return finalCashbackDiscount;
	}

	public BigDecimal verifyFinalTotal(
			final Map<String, String> customParameters) {
		BigDecimal AddFinalTotals = getFinalOrderSubtotal().add(
				getFinalSalesTaxTotal(customParameters)).add(
				getFinalShippingTotal());
		return AddFinalTotals;
	}

	
	public BigDecimal getCPInvoiceVendorSubTotal(
			final Map<String, String> customParameters) {
		BigDecimal cpInvoiceVendorSubTotal = CurrencyUtilities
				.convertPriceStringToBigDecimal(customParameters
						.get("InvoiceVendorSubTotal"));
		return cpInvoiceVendorSubTotal;
	}

	/*public BigDecimal getInvoiceTaxValue(
			final Map<String, String> customParameters) {
		ShopCustomParameters shopCustomParameters = new ShopCustomParameters(
				driver, translator, elementDefinitionManager, customParameters);
	
		BigDecimal invoiceTaxValue = new BigDecimal(0);
		if (!shopCustomParameters.getCPInvoiceTaxValue(customParameters)
				.equals("NULL")) {
			invoiceTaxValue = CurrencyUtilities
					.convertPriceStringToBigDecimal(customParameters
							.get("InvoiceTaxValue"));
		}
		return invoiceTaxValue;
	}*/

	public void placeOrderButton() {
		ShopHomePage homePage = new ShopHomePage(driver, translator,
				elementDefinitionManager);
		waitForElementToBeClickable(By.className("placeOrderButton"), true, 10);
		findElement(By.className("placeOrderButton")).click();
		homePage.header.checkForTransitionOverlay();
	}

	/**
	 * Click the "Place Order" page to place order
	 *
	 * @return ShopOrderConfirmationPage
	 */
	public ShopConfirmationPage placeOrder(final CreditCard creditCard) {
		driver.findElement(By.id("btnSubmitLeft")).click();
		return new ShopConfirmationPage(driver, translator,
				elementDefinitionManager, creditCard);
	}

	private WebElement waitForLoad() {
		return waitForElementToBePresent(
				By.id("pyoReviewPaymentGlobalContainer"), true, 15);
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(this.getBody(), 30);
	}

	/*
	 * @Override protected By getBody() throws
	 * UnableToVerifyPagesExistenceException { return
	 * By.xpath("//span[@id='ccType' and not(@text='')]"); }
	 */

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		waitForLoad();
		return By.id("pyoReviewPaymentGlobalContainer");
	}

}