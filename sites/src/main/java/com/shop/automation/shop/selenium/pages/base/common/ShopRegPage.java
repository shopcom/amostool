package com.shop.automation.shop.selenium.pages.base.common;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.testdata.address.HongKongAddress;
import com.marketamerica.automation.testdata.address.MexicanAddress;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Validate;
import com.marketamerica.automationframework.tools.MarketAmericaSiteURLs;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.shop.automation.shop.configuration.ShopSiteConfiguration;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopAddressInfoPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopCheckoutPage;
import com.shop.automation.shop.selenium.pages.base.common.mobile.ShopHomePageMobile;
import com.shop.automation.shop.selenium.pages.base.common.mobile.ShopRegPageMobile;
import com.shop.automation.shop.selenium.pages.base.common.mobile.ShopRegSuccessPageMobile;
import com.shop.automation.shop.selenium.pages.base.common.mobile.ShopCartPageMobile;
import com.shop.automation.shop.selenium.pages.base.checkout.mobile.ShopAddressInfoPageMobile;

public class ShopRegPage extends ShopTemplatePage {

	public ShopRegPage(final WebDriver driver, final Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

	}

	/**
	 * Select the correct country from the page's dropdown
	 *
	 * @param country
	 *            String of country to be selected
	 * */
	public void selectCountry(final String country) {
		createDropDown(By.id("countrySelect")).selectByVisibleText(country);
	}

	public void selectCountry2(final String country) {
		createDropDown(By.name("register.field_country")).selectByVisibleText(
				country);
	}

	/**
	 * Enter the first name in the first name field of the page's form
	 *
	 * @param firstName
	 *            String of first name to be entered
	 */
	private void enterFirstName(final String firstName) {
		setTextField(By.name("first"), firstName);
		findElement(By.name("last")).click();
	}

	private void enterFirstName2(final String firstName) {
		setTextField(By.name("field_firstname"), firstName);
		findElement(By.name("field_lastname")).click();
	}

	/**
	 * Enter the last name in the last name field of the page's form
	 *
	 * @param lastName
	 *            String of last name to be entered
	 */
	private void enterLastName(final String lastName) {
		setTextField(By.name("last"), lastName);
	}

	private void enterLastName2(final String lastName) {
		setTextField(By.name("field_lastname"), lastName);
	}

	/**
	 * Enter the e-mail in the e-mail field of the page's form
	 *
	 * @param email
	 *            String of e-mail to be enterd
	 */
	private void enterEmail(final String email) {
		waitForjQueryAjax("Waiting for registration Page to load");
		// waitForElementToBeClickable(By.id("email"));
		findElement(By.id("email")).click();
		setTextField(By.id("email"), email);

	}

	private void enterEmail2(final String email) {
		waitForjQueryAjax("Waitingfor Page to load");
		findElement(By.id("sign_in_email_textfield")).click();
		setTextField(By.id("sign_in_email_textfield"), email);
	}

	/**
	 * Enter the password in the password field of the page's form
	 *
	 * @param password
	 *            String of password to be entered
	 */
	private void enterPassword(final String password) {
		setTextField(By.id("password"), password);
	}

	private void enterPassword2(final String password) {
		setTextField(By.name("field_new_password"), password);
	}

	/**
	 * Enter the confirmation password in the confirm password field of the
	 * page's form
	 *
	 * @param passwordConfirm
	 *            String of password confirmation to be entered
	 */
	private void enterPasswordConfirm(final String passwordConfirm) {
		setTextField(By.id("passwordConfirm"), passwordConfirm);
	}

	private void enterPasswordConfirm2(final String passwordConfirm) {
		setTextField(By.name("field_confirm"), passwordConfirm);
	}

	/**
	 * Enter the street address in the street field of the page's form
	 *
	 * @param street
	 *            String of the street to be entered
	 */
	private void enterAddressStreet(final String street) {
		setTextField(By.name("address1"), street);
	}

	private void enterAddressStreet2(final String street) {
		setTextField(By.name("register.field_address"), street);
	}

	// Address 2 field
	private void enterAddr2Field(final String street) {
		setTextField(By.name("address2"), street);
	}

	private void enterAddr2Field2(final String street) {
		setTextField(By.name("register.field_address2"), street);
	}

	// Address 3 field
	private void enterAddr3Field(final String street) {
		setTextField(By.name("address3"), street);
	}

	private void enterAddr3Field2(final String street) {
		// Hack for AI-69874, don't want all my es tests to fail right now
		// setTextField(By.name("register.field_address3"), street);
	}

	/**
	 * Enter the city in the city field of the page's form
	 *
	 * @param city
	 *            String of the city to be entered
	 */
	private void enterAddressCity(final String city, final Country country) {

		if (!country.equals(Country.SINGAPORE)
				&& !country.equals(Country.HONG_KONG)) {
			setTextField(By.name("city"), city);
		}
	}

	private void enterAddressCity2(final String city, final Country country) {
		if (!country.equals(Country.SINGAPORE)
				&& !country.equals(Country.HONG_KONG)) {
			setTextField(By.name("register.field_city"), city);
		}
	}

	/**
	 * Select the correct state from the page's dropdown
	 *
	 * @param state
	 *            String of state to be selected
	 */
	private void selectAddressState(final String state, final Country country) {

		if (!country.equals(Country.SINGAPORE)
				&& !country.equals(Country.HONG_KONG)) {
			createDropDown(By.name("state")).selectByVisibleText(state);
		}

	}

	private void selectAddressState2(final String state, final Country country) {
		if (!country.equals(Country.SINGAPORE)
				&& !country.equals(Country.HONG_KONG)) {
			createDropDown(By.id("register.field_state")).selectByVisibleText(
					state);
		}
	}

	/**
	 * Enter the ZIP/Postal code in the ZIP/Postal code field of the page's form
	 *
	 * @param postalCode
	 *            String of ZIP/Postal code to be entered
	 */
	private void enterPostalCode(final String postalCode, final Country country) {
		if (!country.equals(Country.HONG_KONG)) {
			setTextField(By.name("zip"), postalCode);
		}

	}

	private void enterPostalCode2(final String postalCode, final Country country) {
		if (!country.equals(Country.HONG_KONG)) {
			setTextField(By.name("register.field_zip"), postalCode);
		}
	}

	private void enterFlat(final String flat) {
		setTextField(By.name("address3"), flat);
	}

	private void enterFlat2(final String flat) {
		setTextField(By.name("register.field_address3"), flat);
	}

	private void enterFloor(final String floor) {
		setTextField(By.name("address4"), floor);
	}

	private void enterFloor2(final String floor) {
		setTextField(By.name("register.field_address4"), floor);
	}

	private void enterBuilding(final String building) {
		setTextField(By.name("address5"), building);
	}

	private void enterBuilding2(final String building) {
		setTextField(By.name("register.field_address5"), building);
	}

	private void enterBlock(final String block) {
		setTextField(By.name("address7"), block);
	}

	private void enterBlock2(final String block) {
		setTextField(By.name("register.field_address7"), block);
	}

	private void enterEstate(final String estate) {
		setTextField(By.name("address6"), estate);
	}

	private void enterEstate2(final String estate) {
		setTextField(By.name("register.field_address6"), estate);
	}

	private void selectDistrict(final String district) {
		WebElement districtDropDown = driver.findElement(By.name("zone"));
		this.waitForElementToBeClickable(By.name("zone"));
		Select clickThis = new Select (districtDropDown);
		clickThis.selectByVisibleText(district);
		//createDropDown(By.name("zone")).selectByVisibleText(district);
	}

	private void selectDistrict2(final String district) {
		this.waitForElementToBeClickable(By.name("register.field_zonelevel2"));
		createDropDown(By.name("register.field_zonelevel2"))
				.selectByVisibleText(district);
	}

	private void enterReferralEmail(final Map<String, String> customParameters) {
		setTextField(By.id("referrer"),
				customParameters.get(String.format("ReferralEmail")));
	}

	private void enterReferralEmail2(final Map<String, String> customParameters) {
		setTextField(By.name("field_referral"),
				customParameters.get(String.format("ReferralEmail")));
	}

	/**
	 * Click the "Terms of Service" checkbox to check or uncheck the checkbox
	 */
	public void clickTermsOfServiceBox() {
		findElement(By.id("isAgree")).click();
	}

	/**
	 * Click the Submit button
	 */

	private void clickSubmitButton() {
		findElement(By.id("createAccount")).click();
	}

	private void clickCreateAccountButton() {
		findElement(By.xpath("//span[@class='button primary']")).click();
	}

	/**
	 * Tests to see if the Submit button is enabled
	 *
	 * @return boolean if the submit button is enabled or not
	 */
	public boolean isSubmitButtonEnabled() {
		return findElement(By.id("submitRegForm")).isEnabled();
	}

	/**
	 * Sign up for a new account
	 *
	 * @param shopper
	 *            Shopper data being used to sign up for a new account
	 *
	 * @return Shopper data for the newly registered user
	 */
	private void generateEmail(final Shopper shopper) {
		shopper.setEmail("@test.com");
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM.dd.yyyy");
		Calendar calendar = Calendar.getInstance();
		final String email = String.format("%s.%s.%s%s",
				shopper.getFirstName(), dateFormat.format(calendar.getTime()),
				new Random().nextInt(), shopper.getEmail());
		shopper.setEmail(email);
	}

	public void signUpAccountAtRegPage(final Shopper shopper,
			final Map<String, String> customParameters, final Country country,
			final Locale locale) {

		ShopTestVariables var = new ShopTestVariables(driver, translator,
				elementDefinitionManager);

		if (var.RWDDesktopCheckoutType(locale,
				WebDriverUserAgent.getCurrentUserAgent())) {
			final ShopHomePage homePage = new ShopHomePage(driver, translator,
					elementDefinitionManager);
			homePage.header.closeCountrySelectionPopup();
			homePage.header.signInLink(country);
			homePage.header.closeCountrySelectionPopup();
			StepInfo.addMessage(String.format(
					"New User Registered with the following %s password: %s",
					shopper.getEmail(), shopper.getPassword()));

			ShopRegPageMobile mobileReg = new ShopRegPageMobile(driver,
					translator, elementDefinitionManager);
			mobileReg.signUpRegRWDCheckoutSite(shopper, customParameters,
					country);
			// Not confirming the consultant as the Mobile path does. Will need
			// to add that in

		} else if (var.AmosDesktopCheckoutType(locale,
				WebDriverUserAgent.getCurrentUserAgent())) {
			signUpRegFullSite(shopper, customParameters, country);
		} else {
			ShopHomePageMobile hp = new ShopHomePageMobile(driver, translator,
					elementDefinitionManager);
			StepInfo.addMessage("HomePage Shop Consultant: "
					+ hp.getShopConsultant(country, locale));
			driver.navigate().refresh();
			ShopRegPageMobile mobileReg = new ShopRegPageMobile(driver,
					translator, elementDefinitionManager);
			mobileReg.signUpRegMobileSite(shopper, customParameters, country);
			new ShopRegSuccessPageMobile(driver, translator,
					elementDefinitionManager, country);
		}

	}

	
	private WebElement getNoAcctRadioButton(){
		WebElement newUserRadioBtn = findElement(By
				.id("no_account_radio_button"));
		return newUserRadioBtn;
	}
	
	private void signUpRegFullSite(final Shopper shopper,
			final Map<String, String> customParameters, final Country country) {
		final ShopHomePage homePage = new ShopHomePage(driver, translator,
				elementDefinitionManager);
		homePage.header.closeCountrySelectionPopup();
		homePage.header.signInLink(country);
		homePage.header.closeCountrySelectionPopup();
		StepInfo.addMessage(String.format(
				"New User Registered with the following %s password: %s",
				shopper.getEmail(), shopper.getPassword()));

		this.waitForjQueryAjax("Waiting for page to load");
		
		this.waitForElementToBeClickable(getNoAcctRadioButton(),10);

		getNoAcctRadioButton().click();

		// Enter e-mail
		enterEmail2(String.format(shopper.getEmail()));

		header.getSubmitLoginFormButtonElement().click();

		// Enter first name
		enterFirstName2(shopper.getFirstName());

		// Enter Last name
		enterLastName2(shopper.getLastName());

		// Enter password
		enterPassword2(shopper.getPassword());

		// Confirm password
		enterPasswordConfirm2(shopper.getPassword());

		// Enter street address
		enterAddressStreet2(shopper.getAddress().getStreet());

		// Enter address2 if it exists
		if (!country.equals(Country.HONG_KONG)) {
			enterAddr2Field2(shopper.getAddress().getStreet2());
		}

		// If country is Spain, enter address 3 if it exists
		if (country.equals(Country.SPAIN)) {
			shopper.getAddress().getStreet3();
		}

		// If country is MEX, enter colony/neighborhood
		if (country.equals(Country.MEXICO)) {
			if (shopper.getAddress() instanceof MexicanAddress) {
				MexicanAddress mexicanAddress = (MexicanAddress) shopper
						.getAddress();
				enterNeighborhood2(mexicanAddress.getNeighborhood());
				enterDistrict2(mexicanAddress.getDistrict());
			}

		}

		// If HKG, enter extra address fields
		if (country.equals(Country.HONG_KONG)) {
			if (shopper.getAddress() instanceof HongKongAddress) {
				HongKongAddress hongkongAddress = (HongKongAddress) shopper
						.getAddress();
				enterFlat2(hongkongAddress.getFlat());
				enterFloor2(hongkongAddress.getFloor());
				enterBlock2(hongkongAddress.getBlock());
				enterBuilding2(hongkongAddress.getBuilding());
				enterEstate2(hongkongAddress.getEstate());
				selectDistrict2(hongkongAddress.getDistrict());
			}
		}

		// Enter town/city
		enterAddressCity2(shopper.getAddress().getCity(), country);

		// Select state or territory
		if (country.equals(Country.UNITED_STATES)
				|| country.equals(Country.CANADA)
				|| country.equals(Country.AUSTRALIA)
				|| country.equals(Country.MEXICO)
				|| country.equals(Country.SPAIN)) {
			selectAddressState2(shopper.getAddress().getState(), country);
		}

		// Enter Zip/Postal code
		enterPostalCode2(shopper.getAddress().getZipCode(), country);

		// Select country not used through account reg
		// selectCountry2(shopper.getAddress().getCountry());

		// Enter Phone number
		enterPhoneNumber2(shopper.getPhoneNumber());

		// Enter Referral email
		if (!customParameters.get("ReferralEmail").equals("NULL")) {
			enterReferralEmail2(customParameters);
		}

		// Submit
		clickCreateAccountButton();

	}

	private ShopCheckoutPage signUpAccountAtCheckout(final Shopper shopper,
			final Map<String, String> customParameters, final Country country,
			final Locale locale) {

		ShopHomePage homePage = new ShopHomePage(driver, translator,
				elementDefinitionManager);
		homePage.checkEnvironment(locale);

		StepInfo.addMessage(String
				.format("New User Registered with the following email: %s password: %s",
						shopper.getEmail(), shopper.getPassword()));

		if (WebDriverUserAgent.getCurrentUserAgent().equals(
				WebDriverUserAgent.DEFAULT)) {
			signUpCheckoutFullSite(shopper, customParameters, country);
		}

		else {
			ShopRegPageMobile mobileReg = new ShopRegPageMobile(driver,
					translator, elementDefinitionManager);
			mobileReg.signUpCheckoutMobileSite(shopper, customParameters,
					country);
		}

		return new ShopCheckoutPage(driver, translator,
				elementDefinitionManager, country);
	}

	private void signUpCheckoutFullSite(final Shopper shopper,
			final Map<String, String> customParameters, final Country country) {
		// Enter e-mail
		enterEmail(shopper.getEmail());

		// Create password
		enterPassword(shopper.getPassword());

		// Confirm password
		enterPasswordConfirm(shopper.getPassword());

		// Enter referral Email
		if (!customParameters.get(String.format("ReferralEmail"))
				.equals("NULL")) {
			enterReferralEmail(customParameters);
		}
		// Enter first name
		enterFirstName(shopper.getFirstName());

		// Enter last name
		enterLastName(shopper.getLastName());

		// Enter street address
		enterAddressStreet(shopper.getAddress().getStreet());

		// Enter address2 if it exists
		if (!country.equals(Country.HONG_KONG)) {
			enterAddr2Field(shopper.getAddress().getStreet2());
		}

		// If country is Spain, enter address 3 if it exists
		if (country.equals(Country.SPAIN)) {
			shopper.getAddress().getStreet3();
		}

		// If country is MEX, enter colony/neighborhood
		if (country.equals(Country.MEXICO)) {
			if (shopper.getAddress() instanceof MexicanAddress) {
				MexicanAddress mexicanAddress = (MexicanAddress) shopper
						.getAddress();
				enterNeighborhood(mexicanAddress.getNeighborhood());
				enterDistrict(mexicanAddress.getDistrict());
			}
		}

		// If HKG, enter extra address fields
		if (country.equals(Country.HONG_KONG)) {
			if (shopper.getAddress() instanceof HongKongAddress) {
				HongKongAddress hongkongAddress = (HongKongAddress) shopper
						.getAddress();
				enterFlat(hongkongAddress.getFlat());
				enterFloor(hongkongAddress.getFloor());
				enterBlock(hongkongAddress.getBlock());
				enterBuilding(hongkongAddress.getBuilding());
				enterEstate(hongkongAddress.getEstate());
				selectDistrict(hongkongAddress.getDistrict());
			}
		}

		// Enter town/city
		enterAddressCity(shopper.getAddress().getCity(), country);

		// Select state or territory
		if (country.equals(Country.UNITED_STATES)
				|| country.equals(Country.CANADA)
				|| country.equals(Country.AUSTRALIA)
				|| country.equals(Country.MEXICO)
				|| country.equals(Country.SPAIN)) {
			selectAddressState(shopper.getAddress().getState(), country);
		}

		// Enter Zip/Postal code
		enterPostalCode(shopper.getAddress().getZipCode(), country);

		// Enter Phone number
		enterPhoneNumber(shopper.getPhoneNumber());

		// Select same billing and shipping
		selectShippingSameAsBillingRadioButton();
		// Continue
		clickSubmitButton();

	}

	private void addProductsRegisterCheckout(final CreditCard creditCard,
			final Map<String, String> customParameters,
			final List<Shopper> shoppers, final List<Product> products,
			final Country country, final Locale locale) {

		final ShopHomePage homePage = new ShopHomePage(driver, translator,
				elementDefinitionManager);

		new ShopProductPage(driver, translator, elementDefinitionManager,
				products, customParameters, country, locale);
		ShopTestVariables var = new ShopTestVariables(driver, translator,
				elementDefinitionManager);

		ShopCheckoutPage checkoutpage = new ShopCheckoutPage(driver,
				translator, elementDefinitionManager, country);

		if ((var.RWDDesktopCheckoutType(locale,
				WebDriverUserAgent.getCurrentUserAgent()))) {
			checkoutpage.enterRWDCheckoutFullSite(customParameters, products,
					creditCard, shoppers.get(0), country);
		} else {
			checkoutpage.enterCheckout(customParameters, creditCard,
					shoppers.get(0), products, country, locale);
		}

		if (var.visaCheckout(customParameters).booleanValue() == false) {
			homePage.header.closeCountrySelectionPopup();

			if ((var.RWDDesktopCheckoutType(locale,
					WebDriverUserAgent.getCurrentUserAgent()))) {

				signUpAccountAtRegPage(shoppers.get(0), customParameters,
						country, locale);
				ShopHeaderPage headerPage = new ShopHeaderPage(driver,
						translator, elementDefinitionManager);
				headerPage.navigateToCartPage();
				ShopCartPageMobile cartpage = new ShopCartPageMobile(driver,
						translator, elementDefinitionManager);
				cartpage.getCheckoutButton().click();

			} else {
				if (var.isDesktop(WebDriverUserAgent.getCurrentUserAgent())) {
					checkoutpage.continueBtn();
				}
				signUpAccountAtCheckout(shoppers.get(0), customParameters,
						country, locale);
			}
		}

		if (var.visaCheckout(customParameters).booleanValue() == true) {
			// Create password
			enterPassword(shoppers.get(0).getPassword());

			// Confirm password
			enterPasswordConfirm(shoppers.get(0).getPassword());

			// Continue
			clickSubmitButton();
		}

	}

	private void addProductsCheckout(final CreditCard creditCard,
			final Map<String, String> customParameters,
			final List<Shopper> shoppers, final List<Product> products,
			final Country country, final Locale locale) {

		new ShopProductPage(driver, translator, elementDefinitionManager,
				products, customParameters, country, locale);
		ShopTestVariables var = new ShopTestVariables(driver, translator,
				elementDefinitionManager);

		ShopCheckoutPage checkoutpage = new ShopCheckoutPage(driver,
				translator, elementDefinitionManager, country);
		if ((var.RWDDesktopCheckoutType(locale,
				WebDriverUserAgent.getCurrentUserAgent()))) {
			// ShopHeaderPage headerPage = new ShopHeaderPage(driver,
			// translator, elementDefinitionManager);
			// headerPage.navigateToCartPage();
			// ShopCartPageMobile cartpage = new ShopCartPageMobile(driver,
			// translator, elementDefinitionManager);
			// cartpage.getCheckoutButton().click();
			checkoutpage.enterRWDCheckoutFullSite(customParameters, products,
					creditCard, shoppers.get(0), country);
		} else {
			checkoutpage.enterCheckout(customParameters, creditCard,
					shoppers.get(0), products, country, locale);
		}

		if (var.RWDDesktopCheckoutType(locale,
				WebDriverUserAgent.getCurrentUserAgent())) {
			new ShopAddressInfoPageMobile(
					driver, translator, elementDefinitionManager);
		} else {
			if (var.isDesktop(WebDriverUserAgent.getCurrentUserAgent())) {
				ShopAddressInfoPage AddressPage = new ShopAddressInfoPage(
						driver, translator, elementDefinitionManager, country);

				AddressPage.continueCheckout();
			}
		}
	}

	public void registerNewUser(final CreditCard creditCard,
			final Map<String, String> customParameters,
			final List<Shopper> shoppers, final List<Product> products,
			final Country country, final Locale locale) {

		generateEmail(shoppers.get(0));

		// New User through Sign in Page
		if (customParameters.get(String.format("CreateUser")).equals(
				"Registration")
				|| customParameters.get(String.format("CreateUser")).equals(
						"PortalReg")) {

			driver.navigate().to(
					generateRegURL(customParameters, shoppers, locale));

			this.waitForjQueryAjax("Waiting for page to load");
			
			if (country.equals(Country.MEXICO)) {
				if (!driver.getCurrentUrl().contains("mexico")) {
					driver.navigate().to(
							MarketAmericaSiteURLs.determineURL(
									new ShopSiteConfiguration(locale),
									Environment.getCurrentEnvironment(),
									translator.getLocale()));
				}

			}

			ShopHomePage homePage = new ShopHomePage(driver, translator,
					elementDefinitionManager);
			homePage.checkEnvironment(locale);

			signUpAccountAtRegPage(shoppers.get(0), customParameters, country,
					locale);

			// validating if referred
			if (!customParameters.get(String.format("ReferralEmail")).equals(
					"NULL")) {
				Validate.validateContains(shoppers.get(0)
						.getReferredConsultantName(), header
						.getShopConsultantName(),
						"Validating Shopper Portal Name matches Referred Consultant Name");
			}

			String actualShopConsultantName;
			if (WebDriverUserAgent.getCurrentUserAgent().equals(
					WebDriverUserAgent.DEFAULT)) {
				actualShopConsultantName = header.getConsultantName();

			} else {
				ShopRegSuccessPageMobile success = new ShopRegSuccessPageMobile(
						driver, translator, elementDefinitionManager, country);
				actualShopConsultantName = success.getShopConsultantName();
			}

			// Registration through batting lineup, Verify Shop Consultant is
			// not SHOP.COM
			if (customParameters.get(String.format("CreateUser")).equals(
					"Registration")) {
				Validate.validateNotEquals(actualShopConsultantName,
						"SHOP.COM",
						"Validating Shop Consultant is not SHOP.COM");
			}
			// Verify Shop Consultant if registering on a portal
			if (customParameters.get(String.format("CreateUser")).equals(
					"PortalReg")) {
				String expectedShopConsultantName = shoppers.get(0).getPortal()
						.getShopConsultantName();
				if (WebDriverUserAgent.getCurrentUserAgent().equals(
						WebDriverUserAgent.DEFAULT)) {
					actualShopConsultantName = header.getConsultantName();

				} else {
					ShopRegSuccessPageMobile success = new ShopRegSuccessPageMobile(
							driver, translator, elementDefinitionManager,
							country);
					actualShopConsultantName = success.getShopConsultantName();
				}

				Validate.validateTrue(expectedShopConsultantName
						.equals(actualShopConsultantName), String.format(
						"Expected: \"%s\", Actual \"%s\"",
						expectedShopConsultantName, actualShopConsultantName));
			}

			addProductsCheckout(creditCard, customParameters, shoppers,
					products, country, locale);
		}

		// New user through Checkout
		if (customParameters.get(String.format("CreateUser"))
				.equals("Checkout")
				|| customParameters.get(String.format("CreateUser")).equals(
						"PortalCheckout")) {

			driver.navigate().to(
					generateRegURL(customParameters, shoppers, locale));

			this.waitForjQueryAjax("Waiting for page to load");
			
			if (country.equals(Country.MEXICO)) {
				if (!driver.getCurrentUrl().contains("mexico")) {
					driver.navigate().to(
							MarketAmericaSiteURLs.determineURL(
									new ShopSiteConfiguration(locale),
									Environment.getCurrentEnvironment(),
									translator.getLocale()));
				}

			}

			ShopHomePage homePage = new ShopHomePage(driver, translator,
					elementDefinitionManager);
			homePage.checkEnvironment(locale);

			if (customParameters.get(String.format("CreateUser")).equals(
					"PortalCheckout")) {
				String expectedShopConsultantName = shoppers.get(0).getPortal()
						.getShopConsultantName();

				String actualShopConsultantName;

				if (WebDriverUserAgent.getCurrentUserAgent().equals(
						WebDriverUserAgent.DEFAULT)) {
					actualShopConsultantName = header.getConsultantName();

				} else {
					ShopHomePageMobile mobile = new ShopHomePageMobile(driver,
							translator, elementDefinitionManager);
					actualShopConsultantName = mobile.getShopConsultant(
							country, locale);
				}

				Validate.validateContains(actualShopConsultantName,expectedShopConsultantName, String.format(
						"Actual: \"%s\", Expected \"%s\"",
						expectedShopConsultantName, actualShopConsultantName));
			}

			addProductsRegisterCheckout(creditCard, customParameters, shoppers,
					products, country, locale);

		}

		// New User Corporate
		String expectedShopConsultantName = shoppers.get(0).getPortal()
				.getShopConsultantName();
		if (shoppers.get(0).getId().equals("newCorporateShopper")) {

			if (customParameters.get(String.format("CreateUser")).equals(
					"CorporateReg")) {

				driver.navigate().to(
						generateRegURL(customParameters, shoppers, locale));

				this.waitForjQueryAjax("Waiting for page to load");
				
				signUpAccountAtRegPage(shoppers.get(0), customParameters,
						country, locale);

				Validate.validateEquals(header.getShopConsultantName(),
						expectedShopConsultantName,
						"Validating Corporate Portal Name matches Consultant Name");

				addProductsCheckout(creditCard, customParameters, shoppers,
						products, country, locale);

			}

			if (customParameters.get(String.format("CreateUser")).equals(
					"CorporateCheckout")) {

				driver.navigate().to(
						generateRegURL(customParameters, shoppers, locale));
				this.waitForjQueryAjax("Waiting for page to load");
				addProductsRegisterCheckout(creditCard, customParameters,
						shoppers, products, country, locale);
			}

		}
	}

	private String generateRegURL(final Map<String, String> customParameters,
			final List<Shopper> shoppers, final Locale locale) {
		String url = MarketAmericaSiteURLs.determineURL(
				new ShopSiteConfiguration(locale),
				Environment.getCurrentEnvironment(), translator.getLocale());
		if (customParameters.get(String.format("CreateUser"))
				.contains("Portal")
				|| customParameters.get(String.format("CreateUser")).contains(
						"Corporate")) {
			if (url.endsWith("/")) {
				url = url.substring(0, url.length() - 1);
			}
			url = String.format("%s/%s", url, shoppers.get(0).getPortal()
					.getPortalName());

		}
		if (!customParameters.get(String.format("SourceId")).equals("NULL")) {

			if (url.endsWith("/")) {
				url = url.substring(0, url.length() - 1);
			}
			url = String.format("%s/?sourceid=%s", url,
					customParameters.get(String.format("SourceId")));
		}

		StepInfo.addMessage("Navigating to URL: " + url);
		System.out.println(url);
		return url;
	}

	/**
	 * Enter the phone number in the phone number field of the page
	 *
	 * @param phoneNumber
	 *            String of the phone number to be entered
	 */
	private void enterPhoneNumber(final String phoneNumber) {
		setTextField(By.cssSelector("[name='phone']"), phoneNumber);
	}

	private void enterPhoneNumber2(final String phoneNumber) {
		setTextField(By.id("fonefield"), phoneNumber);
	}

	private void enterNeighborhood(final String neighborhood) {
		setTextField(By.name("address3"), neighborhood);
	}

	private void enterNeighborhood2(final String neighborhood) {
		setTextField(By.id("register.field_address3"), neighborhood);
	}

	private void enterDistrict(final String district) {
		setTextField(By.name("address4"), district);
	}

	private void enterDistrict2(final String district) {
		setTextField(By.id("register.field_address4"), district);
	}

	/**
	 * Clicks the radio button to set the shipping and billing address as the
	 * same
	 */
	private void selectShippingSameAsBillingRadioButton() {
		findElement(By.id("billingSameRadio")).click();
	}

	/**
	 * Test if on the AccountRegistrationPage
	 *
	 * @return boolean
	 */
	public boolean isOnAccountRegistrationPage() {
		try {
			return atPage();
		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence of Account Registration Page");
		}
		return false;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(getBody(), 15);

	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return By.id("passwordConfirm");
	}
}
