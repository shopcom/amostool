package com.shop.automation.shop.selenium.pages.validations.mobile;


import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.Validate;
import com.shop.automation.shop.selenium.pages.base.checkout.mobile.ShopReviewOrderPageMobile;
import com.shop.automation.shop.selenium.pages.base.common.ShopCustomParameters;

public class ShopReviewOrderMobileValidation {

	public ShopReviewOrderMobileValidation(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager,
			final Map<String, String> customParameters) {

		ShopCustomParameters shopCustomParameters = new ShopCustomParameters(
				driver, translator, elementDefinitionManager, customParameters);
		
		ShopReviewOrderPageMobile reviewOrder = new ShopReviewOrderPageMobile(
				driver, translator, elementDefinitionManager);
		
		Validate.validateEquals(
				reviewOrder.getSubtotal(),
				shopCustomParameters
						.getCPInvoiceVendorSubTotal(customParameters),
				"Mobile Review Order Page: Validating Order Subtotal matches custom parameter InvoiceVendorSubTotal");
		
		if (!(customParameters.get(String.format("CouponApplied")).equals("TRUE") ||customParameters.get(String.format("CouponApplied")).equals("RE_ENTER")))
		{
			Validate.validateEquals(
				reviewOrder.getTax(customParameters),
				shopCustomParameters.getCPOrderTaxValue(customParameters),
				"Mobile Review Order Page: Validating Order Sales Tax Total matches Custom Parameter InvoiceTaxValue");
		}
		
		Validate.validateEquals(reviewOrder.getShipping(),
				shopCustomParameters.getCPOrderShipValue(customParameters),
				"Mobile Review Order Page: Validating Order Shipping Total matches Custom Parameter InvoiceShipValue");

		Validate.validateEquals(
				reviewOrder.getFinalTotal(),
				reviewOrder.addTotals(customParameters),
				"Mobile Review Order Page: Validating Final Order Total was added properly");
		
		//If Tax is NULL, verify that Tax is not displayed

	}
	

}
