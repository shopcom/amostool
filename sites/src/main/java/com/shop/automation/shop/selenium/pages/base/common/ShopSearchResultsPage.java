package com.shop.automation.shop.selenium.pages.base.common;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.apache.commons.collections.ListUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;

/**
 * Created by Rick
 */
public class ShopSearchResultsPage extends ShopTemplatePage {

	public ShopSearchResultsPage(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

		try {
			Verify.verifyTrue(atPage(), "Expected to be on search results page");

		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence of search results page");
		}
	}

	private ShopDebugModePage debugModePage = new ShopDebugModePage(driver, translator,
			elementDefinitionManager);

	private ShopSearchXMLParser searchXMLParser = new ShopSearchXMLParser(driver,
			translator, elementDefinitionManager);

	private ShopRestCallParser restCallParser = new ShopRestCallParser(driver,
			translator, elementDefinitionManager);

	private ShopHeaderPage headerPage = new ShopHeaderPage(driver, translator,
			elementDefinitionManager);

	public boolean getbroadenLinkExists() {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try {
			if (this.isElementDisplayed(findElement(By
					.xpath("//div[(@class= 'results-found shop-gray')]//a[(@class= 'broaden shop-blue')]")))) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		} finally {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}

	}

	public boolean getStoresFound() {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try {
			if (this.isElementDisplayed(findElement(By
					.xpath("//div[(@id='main-content')]//div[(@id='store-results')]")))) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		} finally {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}

	}

	public boolean getHomePageFound() {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try {
			if (this.isElementDisplayed(findElement(By.id("homepage")))) {
				System.out.println("You are on Home Page");
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		} finally {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}

	}

	public boolean getNoProductsFound() {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try {
			if (this.isElementDisplayed(findElement(By.id("no-results-message")))) {
				return true;
			}

			else {
				return false;
			}
		} catch (Exception e) {
			return false;
		} finally {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}

	}

	public void checkMboxDisable() {
		this.waitForjQueryAjax("Wait for page to load");
		String currentURL = driver.getCurrentUrl();
		if (!currentURL.contains("mbox")) {
			driver.navigate().to(currentURL + "&mboxDisable=1");
		}
	}

	public void checkOneCartTabClicked(
			final Map<String, String> customParameters) {
		this.waitForjQueryAjax("Wait for page to load");
		String currentURL = driver.getCurrentUrl();
		if (customParameters.get("OneCart").equals("TRUE")) {
			if (!currentURL.contains("t=1")) {
				driver.navigate().to(currentURL + "t=1");
			}
		}
	}

	public WebElement getSearchResultsCountHeader() {
		WebElement searchResultsCountHeader = driver.findElement(By
				.xpath("//div[@class='search-header']"));
		return searchResultsCountHeader;
	}

	public WebElement getSearchResultsCount() {
		WebElement resultsCount = driver.findElement(By
				.xpath("//div[@class='search-header']"));
		return resultsCount;
	}

	public String getResults() {
		String results = driver.findElement(By.id("results")).getText();
		return results;
	}

	public WebElement getViewSavedSearch() {
		WebElement viewSavedSearch = driver.findElement(By
				.xpath("//a[@class='button secondary save-search-button']"));
		return viewSavedSearch;
	}

	public WebElement getDeleteSavedSearch() {
		WebElement deleteSavedSearch = driver
				.findElement(By
						.xpath("//div[(@class= 'delete')]//a[(@class= 'button tertiary')]"));
		return deleteSavedSearch;
	}

	public WebElement getCloseSaveSearch() {
		WebElement closeSearch = driver
				.findElement(By
						.xpath("//div[(@id='save-search-outer')]//div[(@id='save-search-content')]//span[(@class='close-sprite right')]"));
		return closeSearch;
	}

	public WebElement getSaveSearch() {
		WebElement saveSearch = driver.findElement(By
				.xpath("//li[(@class='save-search')]"));
		return saveSearch;
	}

	public WebElement getSaveSearchButton() {
		WebElement saveSearchButton = driver.findElement(By
				.xpath("//a[(@id= 'save-search-button')]"));
		return saveSearchButton;
	}

	public WebElement getOneCartTab(Locale locale) {

		if (locale.getDisplayCountry().equals("Taiwan") || locale.getDisplayCountry().equals("Hong Kong")) {
			WebElement oneCart = driver
					.findElement(By
							.xpath("//div[(@id='main-content')]//section[(@id='refine-search')]//li[contains(.,'OneCart')]/a/input[@type='checkbox']"));
			System.out.println("Clicked Next Page");
			return oneCart;
		} else if (locale.getDisplayCountry().equals("Singapore")) {
			WebElement oneCart = driver
					.findElement(By
							.xpath("//div[(@id='main-content')]//section[(@id='refine-search')]//li[contains(.,'OneCart')]/a/input[@type='checkbox']"));
			System.out.println("Clicked Next Page");
			return oneCart;
		} else {
			WebElement oneCart = driver
					.findElement(By
							.xpath("//div[(@class='oneCartTab ')]//div[(@class='tab-box')]//div[(@class='tabs-container')]//div[(@name='one-cart')]/a"));
			return oneCart;
		}
	}

	public WebElement getNextPage(Locale locale) {

		if (locale.getDisplayCountry().equals("Taiwan") || locale.getDisplayCountry().equals("Hong Kong")) {
			WebElement nextPage = driver
					.findElement(By
							.xpath("//div[(@class='page-count')]//a[contains(., '下一個 ')]"));
			System.out.println("Clicked Next Page");
			return nextPage;
		} else if (locale.getDisplayCountry().equals("Spain")
				|| locale.getDisplayCountry().equals("Mexico")) {
			WebElement nextPage = driver
					.findElement(By
							.xpath("//div[(@class='page-count')]//a[contains(., 'Siguiente ')]"));
			System.out.println("Clicked Next Page");
			return nextPage;
		} else {
			WebElement nextPage = driver
					.findElement(By
							.xpath("//div[(@class='page-count')]//a[contains(., 'Next')]"));
			System.out.println("Clicked Next Page");
			return nextPage;
		}
	}

	public List<String> getTitleName() {
		List<WebElement> titleName = driver.findElements(By
				.xpath("//ul[(@id='content')]//h3[contains(@class,'title')]"));
		List<String> title = new ArrayList<String>();
		for (int i = 0; i < titleName.size(); i++) {
			title.add(titleName.get(i).getText().toString()
					.replaceAll("\\s{2}", " "));

		}
		return title;
	}

	public List<String> getOpcontainerID() {
		List<WebElement> opcontainerid = driver
				.findElements(By
						.xpath("//div[(@id='results')]//div[(@class= 'qr grid')]//div[(@itemtype ='http://schema.org/Product')]"));
		List<String> opContainer = new ArrayList<String>();
		for (int i = 0; i < opcontainerid.size(); i++) {
			opContainer.add(opcontainerid.get(i).getAttribute("opcontainerid")
					.toString());
		}

		return opContainer;
	}

	public List<String> getArticleID() {
		List<WebElement> articleid = driver
				.findElements(By
						.xpath("//div[(@id='main-content')]//ul[(@id='content')]//li[(@class='clear')]/a/article"));
		List<String> articleID = new ArrayList<String>();
		for (int i = 0; i < articleid.size(); i++) {
			articleID.add(articleid.get(i).getAttribute("id").toString());
		}

		return articleID;
	}

	public List<String> getPrice() {
		List<WebElement> prices = driver.findElements(By
				.xpath("//div[(@class='price')]"));
		List<String> price = new ArrayList<String>();
		for (int i = 0; i < prices.size(); i++) {
			price.add(prices.get(i).getText());
		}
		return price;
	}

	public List<String> getLeftNav() {
		List<WebElement> leftNav = driver.findElements(By.id("refine-search"));
		List<String> lnav = new ArrayList<String>();
		for (int i = 0; i < leftNav.size(); i++) {
			lnav.add(leftNav.get(i).getText());
		}
		return lnav;
	}

	public WebElement getBreadcrumb() {
		WebElement breadCrumb = driver.findElement(By
				.xpath("//div[(@class='breadcrumb')]"));
		return breadCrumb;
	}

	public WebElement getDepartmentLink(
			final Map<String, String> customParameters) {
		WebElement departmentLink = driver.findElement(By
				.xpath("//section[(@id='refine-search')]//a[contains(.,'"
						+ customParameters.get("SearchDeptLeftNav") + "')]"));
		return departmentLink;

	}

	public WebElement getGlassButton() {
		WebElement glassButton = driver
				.findElement(By
						.xpath("//div/section[(@id='refine-search')]//div[(@class='input-container')]//div[(@class='search-button')]"));
		return glassButton;
	}

	public void getSort(final Map<String, String> customParameters, Locale locale) {
		if (locale.getDisplayCountry().equals("Taiwan") || locale.getDisplayCountry().equals("Hong Kong")) {
			WebElement dropDown = driver
					.findElement(By
							.xpath("//div[(@id='main-content')]//div[(@class='filterMenu show ')]//div[(@id='utility-bar')]//div[(@class='drop-down-list-button sort-selection')]//div[(@class='arrow')]"));
			dropDown.click();
			this.waitForElementToBeClickable(
					By.xpath("//div[(@id='main-content')]//div[(@class='oneCartTab ')]//div[(@id='sort-by-drop-down-list')]/ul/li/a[contains(., '"
							+ customParameters.get(String.format("Sort")) + "')]"),
					true, 10);
			WebElement selection = driver
					.findElement(By
							.xpath("//div[(@id='main-content')]//div[(@class='oneCartTab ')]//div[(@id='sort-by-drop-down-list')]/ul/li/a[contains(., '"
									+ customParameters.get(String.format("Sort"))
									+ "')]"));
			selection.click();
		} else if (locale.getDisplayCountry().equals("Singapore")) {
			WebElement dropDown = driver
					.findElement(By
							.xpath("//div[(@id='main-content')]//section[(@id='refine-search')]//li[contains(.,'OneCart')]/a/input[@type='checkbox']"));
			dropDown.click();
			this.waitForElementToBeClickable(
					By.xpath("//div[(@id='main-content')]//div[(@class='oneCartTab ')]//div[(@id='sort-by-drop-down-list')]/ul/li/a[contains(., '"
							+ customParameters.get(String.format("Sort")) + "')]"),
					true, 10);
			WebElement selection = driver
					.findElement(By
							.xpath("//div[(@id='main-content')]//div[(@class='oneCartTab ')]//div[(@id='sort-by-drop-down-list')]/ul/li/a[contains(., '"
									+ customParameters.get(String.format("Sort"))
									+ "')]"));
			selection.click();
					
		} else {
				
		WebElement dropDown = driver
				.findElement(By
						.xpath("//div[(@id='main-content')]//div[(@class='oneCartTab ')]//div[(@id='utility-bar')]//div[(@class='drop-down-list-button sort-selection')]//div[@class='arrow']"));
		dropDown.click();
		this.waitForElementToBeClickable(
				By.xpath("//div[(@id='main-content')]//div[(@class='oneCartTab ')]//div[(@id='sort-by-drop-down-list')]/ul/li/a[contains(., '"
						+ customParameters.get(String.format("Sort")) + "')]"),
				true, 10);
		WebElement selection = driver
				.findElement(By
						.xpath("//div[(@id='main-content')]//div[(@class='oneCartTab ')]//div[(@id='sort-by-drop-down-list')]/ul/li/a[contains(., '"
								+ customParameters.get(String.format("Sort"))
								+ "')]"));
		selection.click();
		}
	}

	public String getNotFoundMsg() {
		WebElement notFound = driver
				.findElement(By
						.xpath("//div[(@id='main-content')]//div[(@class='search-header')]//p[contains(@class,'message info')]"));
		String message = "";
		message = notFound.getText().toString();
		return message;
	}

	public WebElement getGridView() {
		WebElement gridView = driver
				.findElement(By
						.xpath("//div[(@id='main-content')]//div[(@class='oneCartTab ')]//div[(@id='utility-bar')]//a[@class='display-by']"));
		return gridView;
	}

	public WebElement getDetailView() {
		WebElement detailView = driver
				.findElement(By
						.xpath("//div[(@id='main-content')]//div[(@class='oneCartTab ')]//div[(@id='utility-bar')]//a[(@class='display-by')]"));
		return detailView;
	}

	// Jump To no longer used on new search
	/*
	 * public WebElement getJumpToBox() { WebElement jumpToBox =
	 * driver.findElement(By
	 * .xpath("//input[@class='current-page-input corner-small']")); return
	 * jumpToBox; }
	 */

	public WebElement getGoToButton() {
		WebElement goButton = driver
				.findElement(By
						.xpath("//span[(@class= 'page-button corner-small')]//a[contains(., 'Go')]"));
		return goButton;
	}

	public WebElement getRefineSearchLink(
			final Map<String, String> customParameters) {
		WebElement refineSearchLinkCheckBox = driver
				.findElement(By
						.xpath("//div[(@id='main-content')]//section[(@id='refine-search')]//li[contains(., '"
								+ customParameters.get("SearchRefineLeftNav")
								+ "')]/a/input[@type='checkbox']"));
		return refineSearchLinkCheckBox;
	}

	public WebElement getSearchWithinTextbox() {
		WebElement searchWithinTextbox = driver
				.findElement(By
						.xpath("//div/section[(@id='refine-search')]//div[(@class='input-container')]/input"));
		return searchWithinTextbox;
	}

	public WebElement getStoreSearchLink(
			final Map<String, String> customParameters) {
		WebElement storeSearchLink = driver
				.findElement(By
						.xpath("//section[(@id='refine-search')]//span[@class= 'label' and contains(.,'"
								+ customParameters
										.get("SearchStoreLinkLeftNav") + "')]"));
		return storeSearchLink;
	}

	public WebElement getBrandSearchLink(
			final Map<String, String> customParameters) {
		WebElement brandSearchLink = driver
				.findElement(By
						.xpath("//section[(@id='refine-search')]//span[@class= 'label' and contains(.,'"
								+ customParameters
										.get("SearchBrandLinkLeftNav") + "')]"));
		return brandSearchLink;

	}

	public void getHoverProduct(List<Product> products)
			throws InterruptedException {
		this.waitForElementToBeVisible(
				By.xpath("//ul[(@id='content')]//div[(@class='quickview-btn-box') and contains(@data-href, '"
						+ products.get(0).getProductContainerId() + "')]"), 10);
		WebElement product = driver
				.findElement(By
						.xpath("//ul[(@id='content')]//div[(@class='quickview-btn-box') and contains(@data-href, '"
								+ products.get(0).getProductContainerId()
								+ "')]"));
		Actions builder = new Actions(driver);
		Actions hoverOver = builder.moveToElement(product);
		hoverOver.perform();
		Thread.sleep(3000);
	}

	public void getHoverOverQuickViewBtn(List<Product> products)
			throws InterruptedException {
		this.waitForElementToBeVisible(
				By.xpath("//button[contains(@value, '"
						+ products.get(0).getProductContainerId() + "')]"), 10);
		Actions builder = new Actions(driver);
		WebElement quickViewBtn = driver.findElement(By
				.xpath("//button[contains(@value, '"
						+ products.get(0).getProductContainerId() + "')]"));
		Actions hoverToBtn = builder.moveToElement(quickViewBtn);
		hoverToBtn.click().perform();
		Thread.sleep(3000);
		this.waitForjQueryAjax("waiting for pop-up to load");
	}

	public WebElement getViewFullProdDetailsBtn() {
		this.waitForElementToBeClickable(By
				.xpath("//input[@name='view-prod-details']"));
		WebElement viewFullProdDetialsBtn = driver.findElement(By
				.xpath("//input[@name='view-prod-details']"));
		return viewFullProdDetialsBtn;
	}

	private WebElement getMoreBtn(final Map<String, String> customParameters) {

		WebElement moreBtn = driver
				.findElement(By
						.xpath("//div[(@id='main-content')]//h2[(@class='divider') and contains(.,'"
								+ customParameters.get("More")
								+ "')]/following-sibling::ul[position()=1]/li/a[contains(.,'+ More')]"));
		return moreBtn;
	}

	private WebElement getCheckboxSaleItem(final Locale locale) {

		if (locale.getDisplayCountry().equals("Taiwan") || locale.getDisplayCountry().equals("Hong Kong")) {
			WebElement checkboxSale = findElement(By
					.xpath("//div[(@id='main-content')]//section[(@id='refine-search')]//li[contains(.,'特價產品')]/a/input[@type='checkbox']"));
			return checkboxSale;
		}

		if (locale.getDisplayCountry().equals("Spain") || locale.getDisplayCountry().equals("Mexico")) {
			WebElement checkboxSale = findElement(By
					.xpath("//div[(@id='main-content')]//section[(@id='refine-search')]//li[contains(.,'Artículos en oferta')]/a/input[@type='checkbox']"));
			return checkboxSale;
		} else {
			WebElement checkboxSale = findElement(By
					.xpath("//div[(@id='main-content')]//section[(@id='refine-search')]//li[contains(.,'Sale items')]/a/input[@type='checkbox']"));
			return checkboxSale;
		}
	}

	private WebElement getInStockItems(Locale locale) {
		if (locale.getDisplayCountry().equals("Taiwan") || locale.getDisplayCountry().equals("Hong Kong")) {
			WebElement oos = findElement(By
					.xpath("//div[(@id='main-content')]//section[(@id='refine-search')]//li[contains(.,'只顯示有現貨的產品')]/a/input[@type='checkbox']"));
			return oos;
		}

		else if (locale.getDisplayCountry().equals("Spain") || locale.getDisplayCountry().equals("Mexico")) {
			WebElement oos = findElement(By
					.xpath("//div[(@id='main-content')]//section[(@id='refine-search')]//li[contains(.,'Solo artículos con existencias ')]/a/input[@type='checkbox']"));
			return oos;
		} else {
			WebElement oos = findElement(By
					.xpath("//div[(@id='main-content')]//section[(@id='refine-search')]//li[contains(.,'In stock items only')]/a/input[@type='checkbox']"));
			return oos;
		}
	}

	private WebElement getPerPage(final Map<String, String> customParameters) {
		WebElement perPage = findElement(By
				.xpath("//div[(@class= 'per-page')]//span[(@name= '"
						+ customParameters.get("ViewPerPage") + "')]"));
		return perPage;
	}

	public void clickDeptLeftNav(final Map<String, String> customParameters) {
		if (!customParameters.get("SearchDeptLeftNav").equals("NULL")) {
			getDepartmentLink(customParameters).click();
			StepInfo.addMessage(String.format("Clicked on %s left nav",
					customParameters.get("SearchDeptLeftNav")));
		}
	}

	public void clickExpandSearchLink(final Map<String, String> customParameters) {
		this.waitForElementToBeClickable(By
				.xpath("//div[(@class= 'results-found shop-gray')]//a[(@class= 'broaden shop-blue')]"));
		driver.findElement(
				By.xpath("//div[(@class= 'results-found shop-gray')]//a[(@class= 'broaden shop-blue')]"))
				.click();
		StepInfo.addMessage("Clicked on Expand Search Link");
	}

	public void clickOneCartStoresTab(
			final Map<String, String> customParameters, Locale locale) {
		if (customParameters.get("OneCart").equals("TRUE")) {
			StepInfo.addMessage("Clicked on OneCart Tab");
			getOneCartTab(locale).click();
		}
	}

	public void searchWithin(final Map<String, String> customParameters) {
		if (!customParameters.get("SearchWithin").equals("NULL")) {
			StepInfo.addMessage(String.format("Searched within %s on left nav",
					customParameters.get("SearchWithin")));
			getSearchWithinTextbox().sendKeys(
					customParameters.get(String.format("SearchWithin")));
			getGlassButton().click();
		}
	}

	public void clickViewDetailOrGrid(final Map<String, String> customParameters) {
		if (customParameters.get("ViewType").equals("Grid View")) {
			StepInfo.addMessage(String.format("Clicked on %s view",
					customParameters.get("ViewType")));
			getGridView().click();
		}

		else if (customParameters.get("ViewType").equals("Detial View")) {
			StepInfo.addMessage(String.format("Clicked on %s view",
					customParameters.get("ViewType")));
			getDetailView().click();
		}

	}

	// Jump to page is no longer used on new search
	/*
	 * public void setJumpTo(final Map<String, String> customParameters) { if
	 * ((customParameters.containsKey("JumpTo") && !customParameters.get(
	 * "JumpTo").equals("NULL"))) { getJumpToBox().clear();
	 * getJumpToBox().sendKeys(customParameters.get("JumpTo"));
	 * this.waitForElementToBeClickable(By
	 * .xpath("//span[(@class= 'page-button corner-small')]//a[contains(., 'Go')]"
	 * )); getGoToButton().click();
	 * StepInfo.addMessage(String.format("Jumped to page %s ",
	 * customParameters.get("JumpTo"))); } }
	 */

	public void saveSearch(final Map<String, String> customParameters) {
		if ((customParameters.containsKey("SaveSearch") && customParameters
				.get("SaveSearch").equals("TRUE"))) {

			this.waitForElementToBeClickable(
					By.xpath("//div[(@id= 'main-content')]//div[(@class= 'breadcrumb')]//li[(@class='save-search')]"),
					true, 5);
			getSaveSearch().click();
			System.out.println("Clicking on Save this serach...");
			StepInfo.addMessage("Clicked on Save this search");
			this.waitForElementToBeClickable(
					By.xpath("//a[(@id= 'save-search-button')]"), true, 5);
			getSaveSearchButton().click();
			System.out.println("Saved Search");
			StepInfo.addMessage(String.format("Saved Search: %s ",
					customParameters.get("SaveSearch")));
			getCloseSaveSearch().click();
			System.out.println("Closing Saved Search");
			StepInfo.addMessage("Closing Saved Search");
			this.driver.navigate().refresh();
			this.waitForjQueryAjax("Wait for Page to load");
			getSaveSearch().click();
			System.out.println("Clicking on Save this serach...");
			StepInfo.addMessage("Clicked on Save this search");
			this.waitForElementToBeClickable(
					By.xpath("//a[@class='button secondary save-search-button']"),
					true, 5);
			getViewSavedSearch().click();
			this.waitForElementToBeClickable(
					By.xpath("//div[(@class= 'delete')]//a[(@class= 'button tertiary')]"),
					true, 5);
			getDeleteSavedSearch().click();
			System.out.println("Deleting Saved Search");
			StepInfo.addMessage("Deleted Saved Search");
			this.waitForElementToBeClickable(
					By.xpath("//div[(@id='save-search-outer')]//div[(@id='save-search-content')]//span[(@class='close-sprite right')]"),
					true, 5);
			getCloseSaveSearch().click();
			System.out.println("Closing Saved Search");
			StepInfo.addMessage("Closing Saved Search");

		}
	}

	public void setSort(final Map<String, String> customParameters, Locale locale) {
		if (!customParameters.get("Sort").equals("NULL")) {
			StepInfo.addMessage(String.format(
					"Clicked on sort by %s on left nav",
					customParameters.get("Sort")));
			getSort(customParameters,locale);
		}
	}

	public void clickNextPage(final Map<String, String> customParameters,
			final Locale locale) {
		if (customParameters.get("NextPage").equals("TRUE")) {
			this.waitForjQueryAjax("Waiting for page to load");
			getNextPage(locale).click();
		}

	}

	public void clickViewCountPage(final Map<String, String> customParameters) {
		if (!customParameters.get("ViewPerPage").equals("NULL")
				&& !customParameters.get("viewPerPage").equals("NONE")) {
			StepInfo.addMessage(String.format("Clicked on %s view per page",
					customParameters.get("viewPerPage")));
			getPerPage(customParameters).click();
		}
	}

	public void clickRefineLeftNav(final Map<String, String> customParameters) {
		if (!customParameters.get("SearchRefineLeftNav").equals("NULL")) {
			this.waitForjQueryAjax("Wait for page to load");
			getRefineSearchLink(customParameters).click();
			StepInfo.addMessage(String.format("Clicked on %s left nav",
					customParameters.get("SearchRefineLeftNav")));
		}
	}

	public void clickStoreLeftNav(final Map<String, String> customParameters) {
		if (!customParameters.get("SearchStoreLinkLeftNav").equals("NULL")) {
			StepInfo.addMessage(String.format("Clicked on %s left nav",
					customParameters.get("SearchStoreLinkLeftNav")));
			getStoreSearchLink(customParameters).click();
		}
	}

	public void clickBrandLeftNav(final Map<String, String> customParameters) {
		if (!customParameters.get("SearchBrandLinkLeftNav").equals("NULL")) {
			StepInfo.addMessage(String.format("Clicked on %s left nav",
					customParameters.get("SearchBrandLinkLeftNav")));
			getBrandSearchLink(customParameters).click();
		}
	}

	public void clickCheckboxSaleLeftNav(
			final Map<String, String> customParameters, final Locale locale) {
		if (customParameters.get("CheckboxSaleLeftNav").equals("TRUE")) {
			StepInfo.addMessage(String
					.format("Clicked on Sale checkbox left nav"));
			getCheckboxSaleItem(locale).click();

		}
	}

	public void clickInStockLeftNav(final Map<String, String> customParameters,
			final Locale locale) {
		if (customParameters.get("CheckboxInStockLeftNav").equals("TRUE")) {
			StepInfo.addMessage(String
					.format("Clicked on In Stock checkbox left nav"));
			getInStockItems(locale).click();
		}

	}

	public void clickMoreLeftNav(final Map<String, String> customParameters) {
		if (!customParameters.get("More").equals("NULL")) {

		}

	}

	public void resultsMessageNotFound(
			final Map<String, String> customParameters) {
		if ((customParameters.containsKey("ResultsInsteadMsg") && customParameters
				.get("ResultsInsteadMsg").equals("TRUE"))) {
			this.waitForLoad();
			StepInfo.addMessage(getNotFoundMsg());
		}
	}

	public void moreButton(final Map<String, String> customParameters) {
		if ((customParameters.containsKey("More") && !customParameters.get(
				"More").equals("NULL"))) {
			this.waitForElementToBeClickable(By
					.xpath("//div[(@id='main-content')]//h2[(@class='divider') and contains(.,'"
							+ customParameters.get("More")
							+ "')]/following-sibling::ul[position()=1]/li/a[contains(.,'+ More')]"));

			getMoreBtn(customParameters).click();
			System.out.println(String.format(
					"Clicked on %s + More on left nav",
					customParameters.get("More")));
			StepInfo.addMessage(String.format(
					"Clicked on %s +More on left nav",
					customParameters.get("More")));
		}

	}

	public void addProdQuickView(final Map<String, String> customParameters,
			List<Product> products) throws InterruptedException {
		if ((customParameters.containsKey("AddProdQuickView") && customParameters
				.get("AddProdQuickView").equals("TRUE"))) {
			StepInfo.addMessage(String.format("Hovering on prodcontainer %s",
					products));
			getHoverProduct(products);
			StepInfo.addMessage("Clicked on quickview button");
			getHoverOverQuickViewBtn(products);
			StepInfo.addMessage("Clicked on full product details button");
			getViewFullProdDetailsBtn().click();
		}
	}

	/*
	 * public int countGSL(){
	 * 
	 * int countGSL = 0; List<WebElement> GSL =
	 * driver.findElements(By.cssSelector("div.sfblAd.notLast")); for(countGSL =
	 * 0; countGSL < GSL.size(); countGSL++){ System.out.println(countGSL); }
	 * 
	 * return countGSL;
	 * 
	 * }
	 * 
	 * public void checkGSL(final Map<String, String> customParameters){ if
	 * ((customParameters.containsKey("GSL") && !customParameters
	 * .get("GSL").equals("NULL"))){ System.out.println(countGSL()); } }
	 */

	public boolean enterValidationCheck(
			final Map<String, String> customParameters) {

		if (customParameters.get("SearchDropDown").equals("Stores")) {
			return false;
		}

		else if (customParameters.containsKey("AddProdQuickView")
				&& customParameters.get("AddProdQuickView").equals("TRUE")) {
			return false;
		}

		else if (customParameters.containsKey("ProductPage")
				&& customParameters.get("ProductPage").equals("TRUE")) {
			return false;
		}

		else if (customParameters.get("Search").equals("")) {

			return false;
		}

		else if (customParameters.get("Search").equals("[*]")) {
			return false;
		}

		else if (customParameters.get("ResultsInsteadMsg").equals("TRUE")) {
			return false;
		}

		else if (!customParameters.get("SearchWithin").equals("NULL")) {
			return false;
		}

		else
			return true;
	}

	public String searchResultsBreadcrumb() {
		return getBreadcrumb().getText().trim();
	}

	public String searchResultsHeaderString() {
		return getSearchResultsCountHeader().getText().trim();
	}

	public boolean isNotContainingResults() {
		return searchResultsHeaderString().contains(
				translate("no_results_found"));
	}

	public int searchCount() {
		return Integer.parseInt(getSearchResultsCount().getText().replaceAll(
				"[\\D]", ""));
		// getSearchResultsCount().getText().trim().replace(",", "");
	}

	public boolean almostOrEqualSearchCount() {

		float SearchUICount = searchCount();
		float SearchXMLCount = Integer.parseInt(searchXMLParser.resultsCount());
		float relativeError = ((SearchXMLCount - SearchUICount) / SearchUICount);
		double maxRelativeError = .05;

		System.out.println(String.format("Relative Error margin: %s",
				relativeError));
		System.out.println(String.format(
				"Search_URL_XML Count: %s\nSearch UI Count: %s",
				(int) SearchXMLCount, (int) SearchUICount));
		StepInfo.addMessage(String.format(
				"Search_URL_XML Count: %s\nSearch UI Count: %s",
				(int) SearchXMLCount, (int) SearchUICount));

		if (SearchXMLCount == SearchUICount) {
			StepInfo.addMessage("Search XML Count and Search UI count matched");
			System.out.println("Search XML Count and Search UI count matched");
			return true;
		}

		else if (relativeError <= maxRelativeError) {
			System.out
					.println("Search XML Count and Search UI count are within 5%");
			StepInfo.addMessage("Search XML Count and Search UI count are within 5%");
			return true;

		} else {
			System.out
					.println("Search XML Count and Search UI count are not within scope 6% or greater");
			StepInfo.addMessage("Search XML Count and Search UI count are not within scope within 6% or greater");
			return false;
		}

	}

	@SuppressWarnings("unchecked")
	public void comparingListStrings() throws UnsupportedEncodingException {
		List<String> productTitlesUI = headerPage.sortList(headerPage
				.escapeHtmlListOfStrings(getTitleName()));
		List<String> searchXMLTitles = headerPage.sortList(headerPage
				.unescapeXmlListOfStrings(searchXMLParser
						.getContainerNames(restCallParser.getSearchURL())));
		List<String> searchEscapeXMLTitles = headerPage
				.escapeHtmlListOfStrings(searchXMLTitles);
		// double maxRelativeError = .055;

		if (!productTitlesUI.containsAll(searchEscapeXMLTitles)) {
			List<String> SearchDiff = ListUtils.subtract(productTitlesUI,
					searchEscapeXMLTitles);
			List<String> SearchXMLhDiff = ListUtils.subtract(
					searchEscapeXMLTitles, productTitlesUI);
			System.out.println(String.format(
					"UI Product titles: %s\n-Difference-\nSearch XML: %s",
					SearchDiff.toString(), SearchXMLhDiff.toString()));
			StepInfo.addMessage(String.format(
					"UI Product titles %s\n-Defference-\nSearch XML: %s",
					SearchDiff.toString(), SearchXMLhDiff.toString()));

		}
		/*
		 * System.out.println(finalDiff.size()); float relativeError =
		 * (finalDiff.size() / (searchXMLTitles.size() +
		 * productTitlesUI.size()));
		 * System.out.println(String.format("Relative Error margin: %s",
		 * relativeError)); if (finalDiff.size() <= maxRelativeError) {
		 * System.out.println("within 5% maring of error");
		 * 
		 * } else { System.out.println("not within 5% maring of error");
		 * 
		 * } }
		 */

		else if (productTitlesUI.containsAll(searchEscapeXMLTitles)) {
			System.out.println("Search UI and Search XML Product Titles match");
			StepInfo.addMessage("Search UI and Search XML Product Titles match");
		} else {
			System.out.println("Unable to compare Strings");
			StepInfo.addMessage("Unable to compare Strings");
		}

	}

	public WebElement waitForLoad() {
		this.waitForjQueryAjax("Waiting for page to load");
		return waitForElementToBeVisible(By.id("content"), 5);
	}

	public <Store> String resultsSearchURL() {
		String searchResultsURL = driver.getCurrentUrl();
		return searchResultsURL;

	}

	public By resultsExist() {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		this.waitForjQueryAjax("Waiting for Search Results Page to load");
		try {

			if (this.isElementExisting(By.id("homepage"), 5)) {
				return By.id("homepage");
			} else if (this.isElementExisting(By.id("content"), 5)) {
				return By.id("content");
			} else if (this.isElementExisting(By.id("no-results-message"), 5)) {
				return By.id("no-results-message");
			} else if (this.isElementExisting(
					By.xpath("//div[(@class = 'op_list_view')]"), 5))
				return By.xpath("//div[(@class = 'op_list_view')]");
			else {
				return null;
			}

		} catch (Exception e) {
			return null;
		} finally {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}
	}

	public void checkIfStoresFound(final Locale locale) {
		this.waitForjQueryAjax("Waiting for search results to load");
		if (getStoresFound() == true) {
			StepInfo.addMessage("Stores Found");
			System.out.println("Stores found");

		} else {
			StepInfo.failTest("No stores found!");
			System.out.println("No stores found!");
		}
	}

	public void checkIfProductsFound(final Locale locale) {
		this.waitForjQueryAjax("Waiting for search results to load");
		if (getHomePageFound() == true) {
			StepInfo.addMessage("Navigated to home page");
			StepInfo.addMessage("You navigated to shop.com home page");

		} else if (getNoProductsFound() == true) {
			System.out.println("No results found!");
			debugModePage.exitDebugMode(locale);
			Verify.failTest("No results found!");

		} else {
			StepInfo.addMessage("Results found");
			System.out.println("Results found");
		}
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(resultsExist());
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return resultsExist();
	}

}
