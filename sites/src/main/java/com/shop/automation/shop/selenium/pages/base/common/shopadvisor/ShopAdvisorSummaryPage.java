package com.shop.automation.shop.selenium.pages.base.common.shopadvisor;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;

public class ShopAdvisorSummaryPage extends ShopTemplatePage {

	String pageUrl = "/shoppingadvisor/summary";
	
	public ShopAdvisorSummaryPage(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager) {

		super(driver, translator, elementDefinitionManager);

	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return (this.driver.getCurrentUrl().contains(pageUrl) && this.footer
				.atPageComponent());
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return null;
	}
}
