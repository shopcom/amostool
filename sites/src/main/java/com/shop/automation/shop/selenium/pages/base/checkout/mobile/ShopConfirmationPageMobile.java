package com.shop.automation.shop.selenium.pages.base.checkout.mobile;

import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.ScreenShooter;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.marketamerica.automationframework.tools.testmanagement.OrderingReporter;
import com.shop.automation.shop.selenium.pages.base.ShopPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ShopConfirmationPageMobile extends ShopPage {

	private final static By orderConfirmation = By
			.cssSelector("div#order-confirmation h3");
	private final static By cbackEarnedUpper = By
			.cssSelector("div.show-detail-view div.cash-back-stripe span.amount");
	private final static By orderSubtotal = By
			.cssSelector("ul li span.amount.subtotal");
	private final static By orderTax = By.cssSelector("ul li span.amount.tax");
	private final static By orderShipping = By
			.cssSelector("ul li span.amount.shipping-cost");
	private final static By couponAmount = By
			.xpath("//div[(@class='order-summary')]//ul/li [position()=2]//span");
	private final static By couponSummaryAmount = By
			.xpath("//div[(@id='summary-order')]/ul/li[position()=2]/span");
	private final static By cbackAppliedAmount = By
			.cssSelector("div#cashback-credit.cash-back-stripe span.amount");
	private final static By GrandTotal = By
			.cssSelector("div#order-confirmation section div.bottom-stripe.cart-subtotal-banner.cart-total span.amount.total");
	private final static By cbackEarnedLower = By
			.cssSelector("div#order-confirmation div.cash-back-stripe span.amount");

	public ShopConfirmationPageMobile(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager,
			final CreditCard creditCard) {
		super(driver, translator, elementDefinitionManager);

		try {
			Verify.verifyTrue(atPage(),
					"Expected to be on Mobile order confirmation page");
			final String orderInformation = String.format("Order Number: %s",
					getOrderConf());
			StepInfo.addMessage(orderInformation);
			logger.debug(orderInformation);
			ScreenShooter.capture(driver);
		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence of Mobile order confirmation page");
		}
	}

	public String getOrderConf() {
		this.waitForjQueryAjax("Waiting for Confirmation Page to load");
		final String orderNumber = findElement(orderConfirmation).getText();
		OrderingReporter.recordOrder(orderNumber, driver);
		return orderNumber;
	}

	public BigDecimal getCbackUpper() {
		String element = findElement(cbackEarnedUpper).getText().toString()
				.trim();
		BigDecimal cback = CurrencyUtilities
				.convertPriceStringToBigDecimal(element);
		return cback;
	}

	public BigDecimal getOrderSubtotal() {

		List<WebElement> subtotals = findElements(orderSubtotal);
		BigDecimal subtotal = new BigDecimal(0);
		if (subtotals.size() > 1) {
			for (int i = 0; i < subtotals.size() - 1; ++i) {
				subtotal = subtotal.add(CurrencyUtilities
						.convertPriceStringToBigDecimal(subtotals.get(i)
								.getText().trim()));
			}
		} else {
			String element = findElement(orderSubtotal).getText().toString()
					.trim();
			subtotal = CurrencyUtilities
					.convertPriceStringToBigDecimal(element);
		}

		return subtotal;
	}

	public BigDecimal getOrderTax(final Map<String, String> customParameters) {
		BigDecimal tax = new BigDecimal("0");
		if (!customParameters.get(String.format("OrderTaxValue"))
				.equals("NULL")) {
			List<WebElement> taxElement = findElements(orderTax);
			tax = new BigDecimal(0);
			if (taxElement.size() > 1) {
				for (int i = 0; i < taxElement.size() - 1; ++i) {
					tax = tax.add(CurrencyUtilities
							.convertPriceStringToBigDecimal(taxElement.get(i)
									.getText().trim()));
				}
			} else {
				String element = findElement(orderTax).getText().toString()
						.trim();
				tax = CurrencyUtilities.convertPriceStringToBigDecimal(element);
			}
		}
		return tax;
	}

	public BigDecimal getCouponAmount() {
		BigDecimal coupon = new BigDecimal(0);
		List<WebElement> discount = findElements(couponAmount);
		String element = "";
		if (discount.size() > 1) {
			if (this.isElementExistingAndDisplayed(couponSummaryAmount)) {
				element = findElement(couponSummaryAmount).getText().toString()
						.trim();
				coupon = coupon.add(CurrencyUtilities
						.convertPriceStringToBigDecimal(element));
			}
		} else {
			if (this.isElementExistingAndDisplayed(couponAmount)) {
				element = discount.get(0).getText().toString().trim();
				coupon = coupon.add(CurrencyUtilities
						.convertPriceStringToBigDecimal(element));
			}
		}
		/*
		 * else { String element =
		 * findElement(couponAmount).getText().toString().trim(); coupon =
		 * CurrencyUtilities .convertPriceStringToBigDecimal(element); }
		 */
		return coupon;
	}

	public BigDecimal getCbackApplied() {
		String element = findElement(cbackAppliedAmount).getText().toString()
				.trim();
		BigDecimal cback = CurrencyUtilities
				.convertPriceStringToBigDecimal(element);
		return cback;
	}

	public BigDecimal getOrderShipping() {
		List<WebElement> ship = findElements(orderShipping);
		BigDecimal shipping = new BigDecimal(0);
		if (ship.size() > 1) {
			for (int i = 0; i < ship.size() - 1; ++i) {
				shipping = shipping.add(CurrencyUtilities
						.convertPriceStringToBigDecimal(ship.get(i).getText()
								.trim()));
			}
		} else {
			String element = findElement(orderShipping).getText().toString()
					.trim();
			shipping = CurrencyUtilities
					.convertPriceStringToBigDecimal(element);
		}

		return shipping;
	}

	public BigDecimal getGrandTotal() {

		String element = findElement(GrandTotal).getText().toString().trim();
		BigDecimal total = CurrencyUtilities
				.convertPriceStringToBigDecimal(element);
		return total;

	}

	public BigDecimal getCbackLower() {
		String element = findElement(cbackEarnedLower).getText().toString()
				.trim();
		BigDecimal cback = CurrencyUtilities
				.convertPriceStringToBigDecimal(element);
		return cback;
	}

	private BigDecimal addTotals(final Map<String, String> customParameters) {
		BigDecimal total = getOrderSubtotal()
				.add(getOrderTax(customParameters)).add(getOrderShipping());
		if (customParameters.get(String.format("CouponApplied")).equals("TRUE")
				|| customParameters.get(String.format("CouponApplied")).equals(
						"RE_ENTER")) {
			total = total.subtract(getCouponAmount());
		} else if (this.isElementExistingAndDisplayed(couponAmount, 3)) {
			total = total.subtract(getCouponAmount());
		}

		return total;
	}

	public BigDecimal finalBalance(final Map<String, String> customParameters) {
		BigDecimal balance = addTotals(customParameters);

		if (customParameters.get(String.format("CashBackApplied")).equals(
				"TRUE")) {
			balance = balance.subtract(getCbackApplied());
		}

		return balance;
	}

	private void waitForLoad() {
		this.waitForjQueryAjax("Waiting for page to load");
		waitForElementToBeVisible(By.id("order-confirmation"), 15);
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(getBody(), 30);
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		waitForLoad();
		return By.id("order-confirmation");
	}

}