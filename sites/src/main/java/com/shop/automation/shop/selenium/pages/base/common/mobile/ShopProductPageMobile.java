package com.shop.automation.shop.selenium.pages.base.common.mobile;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Validate;
import com.marketamerica.automationframework.tools.MarketAmericaSiteURLs;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.configuration.ShopSiteConfiguration;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;
import com.shop.automation.shop.selenium.pages.base.common.mobile.ShopCartPageMobile;

/**
 * Created by javierv on 5/7/2014.
 */
public class ShopProductPageMobile extends ShopTemplatePage {

	public ShopProductPageMobile(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
		try {
			Verify.verifyTrue(atPage(), "Expected to be on Product Page");
		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence of Product Page");
		}

	}


	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(getBody(), 15);
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return By.className("purchase-options");
		
	}

}
