package com.shop.automation.shop.selenium.pages.base.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;

public class ShopDebugModePage extends ShopTemplatePage {

	private final ShopHomePage homePage = new ShopHomePage(driver, translator,
			elementDefinitionManager);

	public ShopDebugModePage(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

		/*
		 * try { Verify.verifyTrue(atPage(),
		 * "Expected to be on debug mode page"); } catch
		 * (UnableToVerifyPagesExistenceException e) {
		 * StepInfo.failTest("Unable to verify existence of debug mode page"); }
		 */
	}

	public WebElement getPasswordTexbox() {

		WebElement enterPassword = driver
				.findElement(By.name("field_password"));

		return enterPassword;
	}

	public WebElement getSubmitButton() {

		WebElement submitButton = driver.findElement(By.name("button_submit"));
		return submitButton;
	}

	public WebElement getRadioButton() {

		WebElement debugRadio = driver.findElement(By.name("field_debug"));
		return debugRadio;
	}

	public WebElement getRadioButtonOff() {
		WebElement debugRadioOff = driver
				.findElement(By
						.xpath("//input[(@value= 'off')][contains(@name, 'field_debug')]"));
		return debugRadioOff;
	}

	public void enterDebugModeInfo() {

		/*
		 * ShopHeaderPage headerPage = new ShopHeaderPage(driver, translator,
		 * elementDefinitionManager);
		 * 
		 * if (headerPage.promoContainer() == true) { headerPage.hoverAway(); }
		 */
		homePage.header.closeCountrySelectionPopup();
		this.waitForElementToBeVisible(By.xpath("//b[contains(.,'Internal Access Only DEBUGGING Page')]"), 15);
		this.waitForElementToBeClickable(getPasswordTexbox(), 10);
		getPasswordTexbox().sendKeys("mjnhbgvfc");
		getSubmitButton().click();
		getRadioButton().click();
		getSubmitButton().click();
		this.waitForElementToBeClickable(By.name("button_submit"), true, 10);
		getSubmitButton().click();
		this.waitForjQueryAjax("Wait for debug mode page to load");

	}

	public void exitDebugMode(final Locale locale) {

		String currentURL = driver.getCurrentUrl();
		StepInfo.addMessage(String.format("Search URL: %s", currentURL));
		System.out.println(currentURL);
		homePage.navigateToDebugPage(locale);
		turnOffDebugMode();
		driver.navigate().to(currentURL);
		homePage.checkMboxisDisabledOnURL(locale);

	}

	public void turnOffDebugMode() {
		getRadioButtonOff().click();
		getSubmitButton().click();
		this.waitForElementToBeClickable(By.name("button_submit"), true, 10);
		getSubmitButton().click();
		this.waitForjQueryAjax("Wait for debug mode page to load");
	}

	public String getSearchURL() {

		String[] codes = driver.findElement(By.tagName("code")).getText()
				.split("\\n");
		for (int i = 0; i < codes.length; ++i) {
			if (codes[i].contains("Search URL")) {
				logger.debug(codes[i].split(" ")[3]);
				return codes[i].split(" ")[3];
			}
		}
		return "";
	}

	public String getRestCallURL() {
		if (Environment.getCurrentEnvironment().equals(Environment.STAGING)) {
			String restCallStaging = driver
					.findElement(
							By.xpath("//p[contains(.,'http://staging-services.shop.com:8085/Site/Search/Path/')]"))
					.getText().replaceAll(" .*", "");
			return restCallStaging;
		
		} else if (Environment.getCurrentEnvironment().equals(Environment.LIVE)) {
			String restCallLive = driver
					.findElement(
							By.xpath("//p[contains(.,'http://services.shop.com:8085/Site/Search/Path/')]"))
					.getText().replaceAll(" .*", "");
			return restCallLive;
		}
		return null;
	}

	// Get cookie "SessionID"
	public String getSessionID() {
		Set<Cookie> allCookies = driver.manage().getCookies();
		for (Cookie loadedCookie : allCookies) {
			// System.out.println(String.format("%s -> %s",
			// loadedCookie.getName(), loadedCookie.getValue()));
			if (loadedCookie.getName().contains("CATALOGCITY_SSN")) {
				String sessionID = loadedCookie.getValue();
				// System.out.println("SessionID: " + loadedCookie.getValue());
				return sessionID;
			}
		}
		StepInfo.addMessage(String.format("No SessionID cookie found!"));
		return null;
	}

	/*
	 * public String sessionId(final Country country) { String session = driver
	 * .manage() .getCookieNamed( "CATALOGCITY_SSN" + getEnvironment() +
	 * getCcsyn(country)).getValue(); return session; }
	 */

	public String getSourceID() {
		String sourceId = driver.manage().getCookieNamed("CC_SRCID").getValue();
		return sourceId;
	}

	public String getAMOSNSIDCookie() {
		Set<Cookie> allCookies = driver.manage().getCookies();
		for (Cookie loadedCookie : allCookies) {
			if (loadedCookie.getName().contains("AMOS_NS_ID")) {
				String sessionID = loadedCookie.getValue();
				return sessionID;
			}
		}
		return "No AMOS_NS_ID cookie found!";
	}

	/*
	 * public String getEnvironment() { String environment = ""; if
	 * (Environment.getCurrentEnvironment().equals(Environment.LIVE)) {
	 * environment = "LIVE"; }
	 * 
	 * if (Environment.getCurrentEnvironment().equals(Environment.STAGING)) {
	 * environment = "STAGING"; } return environment; }
	 */

	/*
	 * public String getCcsyn(final Country country) { String ccsyn = "260";
	 * switch (country) { case UNITED_STATES: ccsyn = "260"; break; case
	 * AUSTRALIA: ccsyn = "13650"; break; case CANADA: ccsyn = "898"; break;
	 * case HONG_KONG: ccsyn = "13693"; break; case MEXICO: ccsyn = "13643";
	 * break; case SINGAPORE: ccsyn = "23793"; break; case SPAIN: ccsyn =
	 * "13673"; break; case TAIWAN: ccsyn = "13663"; break; case UNITED_KINGDOM:
	 * ccsyn = "515"; break; default: ccsyn = "260"; }
	 * 
	 * return ccsyn; }
	 */

	public void getSessionId() {
		String[] codes = driver
				.findElement(By.cssSelector(".body>table>tbody>tr>td>p>code"))
				.getText().split("\\n");
		for (int i = 0; i < codes.length; ++i) {
			if (codes[i].contains("User Session ID")) {
				StepInfo.addMessage("Session ID: " + codes[i]);
			}
		}
	}
	
	public String getSHOPMFNSIDCookie() {
		Set<Cookie> allCookies = driver.manage().getCookies();
		for (Cookie loadedCookie : allCookies) {
			if (loadedCookie.getName().contains("SHOPMF_NS_ID")) {
				String shopMFNSID = loadedCookie.getValue();
				return shopMFNSID;
			}
		}
		return "No SHOPMF_NS_ID cookie found!";
	}

	public void getDebugCookies() {
		StepInfo.addMessage("AMOS_NS_ID = " + getAMOSNSIDCookie());
		StepInfo.addMessage("Session ID: " + getSessionID());
		StepInfo.addMessage("Source ID: " + getSourceID());
		StepInfo.addMessage("SHOPMF_NS_ID: " + getSHOPMFNSIDCookie());

	}
	
	public void getDateTimeStamp()
	{
		DateFormat dateFormat = new SimpleDateFormat(
				"yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		StepInfo.addMessage("Current TimeStamp: " + dateFormat.format(date));
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return false;
	}

}
