package com.shop.automation.shop.selenium.pages.validations;

import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Validate;
import com.shop.automation.shop.selenium.pages.base.common.ShopCartPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopCustomParameters;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;


public class ShopCartPageValidation {

	public ShopCartPageValidation(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager, final Map<String, String> customParameters, final Country country) {
		
			ShopHomePage homePage = new ShopHomePage(driver, translator,
					elementDefinitionManager);
			homePage.header.checkForTransitionOverlay();
			
			ShopCustomParameters shopCustomParameters = new ShopCustomParameters(
					driver, translator, elementDefinitionManager, customParameters);
			
			ShopCartPage cart = new ShopCartPage(driver, translator, elementDefinitionManager);
			
			StepInfo.addMessage(String.format("Amount of items in mini cart: %s", homePage.header.cartItems()));
			
			StepInfo.addMessage("Validating totals on Cart Page against Custom Parameters");
			
			//Can only verify subtotal against custom parameters because shipping gets changed in checkout (alters tax as well)
			
			Validate.validateEquals(cart.getSubtotal(), shopCustomParameters.getCPInvoiceVendorSubTotal(customParameters), 
					"Cart Page: Validating subtotal matches Custom Parameter InvoiceVendorSubTotal");

			
			if (cart.shipCalculated(country) == true)
			{
				
				Validate.validateEquals(cart.getGrandTotal(), cart.addTotals(customParameters, country),
						"Cart Page: Validating totals add up to grand total");
			}

		}


}