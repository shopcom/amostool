package com.shop.automation.shop.selenium.pages.base.common;


import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.shop.automation.shop.selenium.pages.base.ShopPageComponent;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopCheckoutPage;

/**
 * Created by javierv on 5/7/2014.
 */
class ShopMiniCartPage extends ShopPageComponent {
    private static final By body = By.xpath("//div[@id='addcart' and @aria-hidden='false']");
    private static final By productAddedElement = By.xpath("//div[@id='addcart' and @aria-hidden='false']//h2[contains(@class, 'alert')]");
    private static final By continueShoppingButton = By.xpath("//div[@id='addcart' and @aria-hidden='false']//span[@class='continue-shopping']/../a[@data-dismiss='modal']");
    private static final By checkoutButton = By.cssSelector("a.checkoutbutton");

    ShopMiniCartPage(WebDriver driver, Translator translator, ElementDefinitionManager elementDefinitionManager) {
        super(driver, translator, elementDefinitionManager);
    }

    private WebElement getCheckoutButton() {
        return findElement(checkoutButton);
    }

    private WebElement getContinueShoppingButton() {
        return findElement(continueShoppingButton);
    }

    private WebElement getProductAddedElement() {
        return findElement(productAddedElement);
    }

    public boolean exists() {
        return isElementExisting(body);
    }

    public String getProductTitle() {
        return getProductAddedElement().getText();
    }

    public ShopProductPage continueShopping() {
        getContinueShoppingButton().click();
        return new ShopProductPage(driver, translator, elementDefinitionManager);
    }

    public ShopCheckoutPage checkoutButton(final Country country) {
        getCheckoutButton().click();
        return new ShopCheckoutPage(driver, translator, elementDefinitionManager, country);
    }


}
