package com.shop.automation.shop.selenium.pages.validations;


import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.Validate;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopOrderSummarySection;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopShippingAndBillingInfoPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopCustomParameters;
import com.shop.automation.shop.selenium.pages.base.common.ShopTestVariables;

public class ShopShippingAndBillingValidation {

	public ShopShippingAndBillingValidation(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager,
			final Map<String, String> customParameters) {

		ShopOrderSummarySection OrderSummarySection = new ShopOrderSummarySection(
				driver, translator, elementDefinitionManager, customParameters);

		ShopCustomParameters shopCustomParameters = new ShopCustomParameters(
				driver, translator, elementDefinitionManager, customParameters);

		ShopShippingAndBillingInfoPage ShippingAndBillingInfoPage = new ShopShippingAndBillingInfoPage(
				driver, translator, elementDefinitionManager);
		ShopTestVariables testVariables = new ShopTestVariables (driver, translator, elementDefinitionManager);

		/*
		 * Validate.validateContains(customParameters.get("ShippingType"),
		 * OrderSummarySection.getShippingTotalOS().toString(),
		 * "Validating Shipping changed on Summary Order Section");
		 */

		String cashBackApplied = customParameters.get("CashBackApplied");
		String couponApplied = customParameters.get("CouponApplied");
		String giftCardApplied = customParameters.get("GiftCardApplied");
		String invoiceCashbackEarned = customParameters.get("InvoiceCashBackEarned");
		String customsHandling = customParameters.get("InvoiceCustoms");
		String orderTaxValue = customParameters.get("OrderTaxValue");

		if (cashBackApplied.equals("TRUE")) {

			Validate.validateEquals(
					OrderSummarySection.getCashbackCredit(),
					OrderSummarySection.getCbackAvailable(),
					"Shipping/Billing Page: Validating Order Summary Section applied CashBack matches Applied Amount");

		}

		if (couponApplied.equals("TRUE") || couponApplied.equals("RE_ENTER")) {

			Validate.validateEquals(
					ShippingAndBillingInfoPage
							.getCouponAppliedAmountSuccessContainer(),
					OrderSummarySection.getCouponDiscountOS(),
					"Shipping/Billing Page: Validating Order Summary Section applied Coupon discount matches Discount in Success Container");

		}
		
		if (testVariables.isFirstTimePromo(customParameters)==true){
			
			Validate.validateTrue(OrderSummarySection.getCouponLabelPriceOS(), "Validating discount is applied for first time user");
			
		}
		

		if (giftCardApplied.equals("TRUE")) {

			Validate.validateEquals(
					OrderSummarySection.getGiftCardAppliedAmount(),
					ShippingAndBillingInfoPage
							.getGiftCardAppliedAmountSuccessContainer(),
					"Shipping/Billing Page: Validating Order Summary Section applied Gift Card discount matches promo discount in Success Container");

		}

		if (cashBackApplied.equals("FALSE") && couponApplied.equals("FALSE")
				&& giftCardApplied.equals("FALSE") && testVariables.isFirstTimePromo(customParameters)==false) {
			Validate.validateEquals(
					OrderSummarySection.getOrderTotalOS(),
					OrderSummarySection
							.verifySummaryOrderTotal(customParameters),
					"Shipping/Billing Page: Validating Order Summary Section totals were added properly");
		}

		Validate.validateEquals(
				OrderSummarySection.getOrderSubtotalOS(),
				shopCustomParameters
						.getCPInvoiceVendorSubTotal(customParameters),
				"Shipping/Billing Page: Validating Order Summary Order Subtotal matches custom parameter InvoiceVendorSubTotal");

		Validate.validateEquals(
				OrderSummarySection.getSalesTaxOS(customParameters),
				shopCustomParameters.getCPInvoiceTaxValue(customParameters),
				"Shipping/Billing Page: Validating Order Summary Tax Total matches custom parameter InvoiceTaxValue");

		// If this is a GC test, we need to remove the GC after validation
		if (giftCardApplied.equals(
				"TRUE")){
		ShippingAndBillingInfoPage.removeGiftCardApplied();
		}
		
		
		Validate.validateEquals(
				OrderSummarySection.getShippingOS(customParameters),
				shopCustomParameters
						.getCPInvoiceVendorShippingTotal(customParameters),
				"Shipping/Billing Page: Validating Order Summary Shipping Total matches custom parameter InvoiceShipValue");

		
		Validate.validateEquals(
				OrderSummarySection.getOStotals(customParameters),
				shopCustomParameters.getCPInvoiceVendorTotal(customParameters),
				"Shipping/Billing Page: Validating Order Summary Order Total matches custom parameter InvoiceVendorTotal");

		if(!invoiceCashbackEarned.equals("NULL"))
		{
			Validate.validateEquals(
			OrderSummarySection.getCashbackOS(),
			shopCustomParameters.getCPInvoiceCashBackEarned(customParameters),
			"Shipping/Billing Page: Validating Order Summary Cashback Earned matches custom parameter InvoiceCashBackEarned");
		}
		
		if (customParameters.containsKey(String.format("InvoiceCustoms")))
		{
			if (!customsHandling.equals("NULL"))
			{
				Validate.validateEquals(
						OrderSummarySection.getCustomsHandlingOS(), shopCustomParameters.getCPCustomsHandling(customParameters),
						"Shipping/Billing Page: Validating Order Summary Customs and Handling matches custom parameter InvoiceCustoms");
			}
		}
		
		//If Tax is NULL, verify that Tax is not displayed
		if(orderTaxValue.equals("NULL"))
		{
			Validate.validateFalse(OrderSummarySection.getTaxLabelOS().isDisplayed(), "Shipping/Billing Page: Validating Tax is not Displayed in the Order Summary");
		}

		
	}
}
