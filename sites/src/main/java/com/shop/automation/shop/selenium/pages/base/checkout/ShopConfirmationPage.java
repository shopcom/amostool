package com.shop.automation.shop.selenium.pages.base.checkout;

import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.ScreenShooter;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Validate;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.marketamerica.automationframework.tools.testmanagement.OrderingReporter;
import com.shop.automation.shop.selenium.pages.base.ShopPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopTestVariables;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ShopConfirmationPage extends ShopPage {
	public ShopConfirmationPage(final WebDriver driver, final Translator translator, ElementDefinitionManager elementDefinitionManager, final CreditCard creditCard){
		super(driver, translator,elementDefinitionManager);
		
		if (creditCard.getId().equals("eNets"))
		{
			ShopEnetsPage ShopEnetsPage = new ShopEnetsPage(
					driver, translator, elementDefinitionManager, creditCard);
			ShopEnetsPage.completeEnetsOrder(creditCard);
		}

		try {
			Verify.verifyTrue(atPage(), "Expected to be on order confirmation page");
			
		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence of order confirmation page");
		}
	}

	ShopTestVariables testVariables = new ShopTestVariables (driver, translator, elementDefinitionManager);
	
	public String getOrderConfNumber(){
		this.waitForjQueryAjax("Waiting for Confirmation Page to load");
		final String orderNumber = driver.findElement(By.cssSelector("#orderNumber")).getText();
		StepInfo.addMessage("Order Number: " + orderNumber);
		logger.debug(orderNumber);
		ScreenShooter.capture(driver);
		OrderingReporter.recordOrder(orderNumber, driver);
		return orderNumber;
	}

	/*public String getOrderConfNumber() {
		this.waitForjQueryAjax("Waiting for Confirmation Page to load");
		return findElement(By.cssSelector("#orderNumber")).getText();
	}*/
	
	public BigDecimal getVendorDiscount(){
		List<WebElement> vendorDiscount = driver.findElements(By.xpath("//div[(@class='vendorOrderSummaryContainer')]//div[(@class='orderConfirmSummaryTotalsContainer')]//div[contains(., '-')]"));
		BigDecimal discount = new BigDecimal(0);
		for (int i = 0; i <vendorDiscount.size(); ++i){
			discount = discount.add(CurrencyUtilities.convertPriceStringToBigDecimal(vendorDiscount.get(i).getText().trim()));
		}
		return discount;
	}

	public BigDecimal getVendorSubTotalConf() {
		List<WebElement> invoiceVendorSubTotal = driver.findElements(By.cssSelector(".orderConfirmSummaryTotalsContainer > div:nth-child(1)"));
		BigDecimal vendorSubTotal = new BigDecimal(0);
		for (int i = 0; i < invoiceVendorSubTotal.size(); ++i) {
			vendorSubTotal = vendorSubTotal
					.add(CurrencyUtilities
							.convertPriceStringToBigDecimal(invoiceVendorSubTotal.get(i).getText().trim()));
		}
		return vendorSubTotal;
	}

	public BigDecimal getVendorSalesTaxConf(final Map<String, String> customParameters) {
		List<WebElement> invoiceVendorSalesTaxTotal;
		invoiceVendorSalesTaxTotal = driver.findElements(By.cssSelector(".orderConfirmSummaryTotalsContainer > div:nth-child(2)"));
		BigDecimal vendorSalesTax = new BigDecimal(0);
		if (!customParameters.get(String.format("OrderTaxValue")).equals(
				"NULL"))
		{
			if (customParameters.get(String.format("CouponApplied")).equals("TRUE") || customParameters.get(String.format("CouponApplied")).equals("RE_ENTER"))
			{
				invoiceVendorSalesTaxTotal = driver.findElements(By.cssSelector(".orderConfirmSummaryTotalsContainer > div:nth-child(3)"));
			}
			
			else if (testVariables.isFirstTimePromo(customParameters)==true){
				invoiceVendorSalesTaxTotal = driver.findElements(By.cssSelector(".orderConfirmSummaryTotalsContainer > div:nth-child(3)"));
			}

			for (int i = 0; i < invoiceVendorSalesTaxTotal.size(); ++i)
			{
			vendorSalesTax = vendorSalesTax
					.add(CurrencyUtilities
							.convertPriceStringToBigDecimal(invoiceVendorSalesTaxTotal.get(i).getText().trim()));
			}
		}
		else
		{
			vendorSalesTax = new BigDecimal(0);
		}
		return vendorSalesTax;
	}

	public BigDecimal getVendorShippingTotalConf(final Map<String, String> customParameters) {
		String child = "3";
		if (customParameters.get(String.format("OrderTaxValue")).equals(
				"NULL"))
		{
			child = "2";

			if (customParameters.get(String.format("CouponApplied")).equals("TRUE") || customParameters.get(String.format("CouponApplied")).equals("RE_ENTER"))
			{
				child = "3";
			}
			else if (testVariables.isFirstTimePromo(customParameters)==true){
				child = "3";
			}
		}
		else
		{
			if (customParameters.get(String.format("CouponApplied")).equals("TRUE") || customParameters.get(String.format("CouponApplied")).equals("RE_ENTER"))
			{
				child = "4";
			}
			
			else if (testVariables.isFirstTimePromo(customParameters)==true){
				child = "4";
			}
		}
		int index = Integer.parseInt(child);
		index--;
		String prevChild = Integer.toString(index);
		List<WebElement> invoiceVendorShippingTotal = driver.findElements(By.cssSelector(".orderConfirmSummaryTotalsContainer > div:nth-child(" + child + ")"));
		List<WebElement> label = driver.findElements(By.cssSelector(".orderConfirmSummaryLabelsContainer > div:nth-child(" + child + ")"));
		List<WebElement> prev = driver.findElements(By.cssSelector(".orderConfirmSummaryTotalsContainer > div:nth-child("+ prevChild + ")"));

		BigDecimal vendorShippingTotal = new BigDecimal(0);
		for (int i = 0; i < invoiceVendorShippingTotal.size(); ++i) {
			System.out.println("vendor shipping total: " + label.get(i).getText().trim());
			if (label.get(i).getText().trim().contains("Shipping") || label.get(i).getText().trim().contains("運費總金額"))
			{
				vendorShippingTotal = vendorShippingTotal
						.add(CurrencyUtilities
								.convertPriceStringToBigDecimal(invoiceVendorShippingTotal.get(i).getText().trim()));
			}
			else
			{
				vendorShippingTotal = vendorShippingTotal.add(CurrencyUtilities
						.convertPriceStringToBigDecimal(prev.get(i).getText().trim()));
			}

		}
		return vendorShippingTotal;
	}

	public BigDecimal getVendorOrderTotalConf(final Map<String, String> customParameters) {
		List<WebElement> invoiceVendorOrderTotal = driver.findElements(By.cssSelector(".orderConfirmSummaryTotalsContainer > div:nth-child(4)"));
		if (customParameters.get(String.format("OrderTaxValue")).equals(
				"NULL"))
		{
			invoiceVendorOrderTotal = driver.findElements(By.cssSelector(".orderConfirmSummaryTotalsContainer > div:nth-child(3)"));
		}
		
		if (customParameters.containsKey(String.format("OrderCustoms")))
		{
			if (!customParameters.get("OrderCustoms").equals("NULL"))
			{
				invoiceVendorOrderTotal = driver.findElements(By.cssSelector(".orderConfirmSummaryTotalsContainer > div:nth-child(5)"));

			}
		}	
		
		BigDecimal vendorOrderTotal = new BigDecimal(0);
		for (int i = 0; i < invoiceVendorOrderTotal.size(); ++i) {
			vendorOrderTotal = vendorOrderTotal
					.add(CurrencyUtilities
							.convertPriceStringToBigDecimal(invoiceVendorOrderTotal.get(i).getText().trim()));
		}
		//If Cashback is applied, Subtract Cashback applied from VOT
		if (customParameters.get(String.format("CashBackApplied")).equals(
				"TRUE"))
		{
			BigDecimal CashbackApplied = CurrencyUtilities
					.convertPriceStringToBigDecimal(findElement(
							By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(4)")).getText().trim());
			//If Tax isn't displayed the rows are shifted
			if(customParameters.get(String.format("OrderTaxValue")).equals(
					"NULL"))
			{
				CashbackApplied = CurrencyUtilities
						.convertPriceStringToBigDecimal(findElement(
								By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(3)")).getText().trim());
			}
			vendorOrderTotal = vendorOrderTotal.subtract(CashbackApplied);
		}
		else if (customParameters.get(String.format("CouponApplied")).equals("TRUE") || customParameters.get(String.format("CouponApplied")).equals("RE_ENTER"))
		{

			//If Tax isn't displayed the rows are shifted
			if(customParameters.get(String.format("OrderTaxValue")).equals(
					"NULL"))
			{
				vendorOrderTotal = CurrencyUtilities
						.convertPriceStringToBigDecimal(findElement(
								By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(4)")).getText().trim());
			}
			else
			{
				//Rows are shifted if coupon is applied
				vendorOrderTotal = CurrencyUtilities
						.convertPriceStringToBigDecimal(findElement(
								By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(5)")).getText().trim());
			}
		}
		
		else if (testVariables.isFirstTimePromo(customParameters)==true){
			//If Tax isn't displayed the rows are shifted
			if(customParameters.get(String.format("OrderTaxValue")).equals(
					"NULL"))
			{
				vendorOrderTotal = CurrencyUtilities
						.convertPriceStringToBigDecimal(findElement(
								By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(4)")).getText().trim());
			}
			else
			{
				//Rows are shifted if coupon is applied
				vendorOrderTotal = CurrencyUtilities
						.convertPriceStringToBigDecimal(findElement(
								By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(5)")).getText().trim());
			}
		}
		
		return vendorOrderTotal;
	}

	public String getFinalOrderSubtotalConf() {

		String finalOrderSubtotalConf = findElement(By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(1)")).getText().toString().trim();

		return finalOrderSubtotalConf;
	}

	public String getFinalCouponDiscount() {

		String finalOrderSubtotalConf = findElement(By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(2)")).getText().toString().trim();

		return finalOrderSubtotalConf;
	}
	
	public String getFinalCustomsHandling() {

		String finalCustHandlingConf = findElement(By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(4)")).getText().toString().trim();
		return finalCustHandlingConf;
	}

	public String getFinalTaxTotalConf(final Map<String, String> customParameters) {
		if (customParameters.get(String.format("OrderTaxValue")).equals(
				"NULL"))
		{
			return "0";
		}

		else if(customParameters.get(String.format("CouponApplied")).equals("TRUE") || customParameters.get(String.format("CouponApplied")).equals("RE_ENTER"))
		{
			return findElement(By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(3)"))
					.getText().toString().trim();
		}
		
		else if (testVariables.isFirstTimePromo(customParameters)==true){
			return findElement(By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(3)"))
					.getText().toString().trim();
		}

		return findElement(By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(2)"))
				.getText().toString().trim();
	}

	public String getFinalsShippingConf(final Map<String, String> customParameters) {
		if (customParameters.get(String.format("OrderTaxValue")).equals(
				"NULL"))
		{
			if(customParameters.get(String.format("CouponApplied")).equals("TRUE") || customParameters.get(String.format("CouponApplied")).equals("RE_ENTER"))
			{
				return findElement(By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(3)"))
						.getText().toString().trim();
			}
			return findElement(By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(2)"))
					.getText().toString().trim();
		}

		if(customParameters.get(String.format("CouponApplied")).equals("TRUE") || customParameters.get(String.format("CouponApplied")).equals("RE_ENTER"))
		{

			return findElement(By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(4)"))
					.getText().toString().trim();
		}
		
		else if (testVariables.isFirstTimePromo(customParameters)==true){
			return findElement(By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(4)"))
					.getText().toString().trim();
		}

		return findElement(By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(3)"))
				.getText().toString().trim();
	}

	public BigDecimal getVendorCashbackTotalConf(final Map<String, String> customParameters) {
		List<WebElement> invoiceVendorCashbackTotal = driver.findElements(By.cssSelector(".orderConfirmSummaryTotalsContainer > div:nth-last-of-type(1)"));

		BigDecimal vendorCashbackTotal = new BigDecimal(0);
		for (int i = 0; i < invoiceVendorCashbackTotal.size(); ++i) {
			vendorCashbackTotal = vendorCashbackTotal
					.add(CurrencyUtilities
							.convertPriceStringToBigDecimal(invoiceVendorCashbackTotal.get(i).getText().trim()));
		}
		return vendorCashbackTotal;
		}

	public String getFinalOrderTotalConf(final Map<String, String> customParameters) {
		String child = "4";
		if (customParameters.get(String.format("OrderTaxValue")).equals(
				"NULL"))
		{
			child = "3";
			//If coupon is applied, rows are shifted
			if (customParameters.get(String.format("CouponApplied")).equals("TRUE") || customParameters.get(String.format("CouponApplied")).equals("RE_ENTER"))
			{
				child = "4";
				if (customParameters.get(String.format("CashBackApplied")).equals(
						"TRUE"))
				{
					child = "5";
				}
			}

			//If cashback is applied, rows are shifted
			else if (customParameters.get(String.format("CashBackApplied")).equals(
					"TRUE"))
			{
				child = "4";
			}
		}
		else if (customParameters.containsKey(String.format("OrderCustoms")))
		{
			if (!customParameters.get("OrderCustoms").equals("NULL"))
			{
				child = "5";
			}
		}	
		else if (customParameters.get(String.format("CashBackApplied")).equals(
				"TRUE"))
		{
			child = "5";
			if(customParameters.get(String.format("CouponApplied")).equals("TRUE") || customParameters.get(String.format("CouponApplied")).equals("RE_ENTER"))
			{
				child = "6";
			}
				
		}
		//Coupon ONLY Applied
		else if (customParameters.get(String.format("CouponApplied")).equals("TRUE") || customParameters.get(String.format("CouponApplied")).equals("RE_ENTER"))
			{
				return findElement(By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(5)"))
						.getText().toString().trim();
			}
		
		else if (testVariables.isFirstTimePromo(customParameters)==true){
			return findElement(By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(5)"))
					.getText().toString().trim();
		}
		return findElement(By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(" + child + ")"))
				.getText().toString().trim();
	}

	public String getFinalOrderCashbackTotal(final Map<String, String> customParameters) {
		
		return findElement(By.cssSelector(".finalOrderSummaryTotalsContainer > .finalOrderRewards.cashbackGreen")).getText().toString().trim();
	}
	
	public String getFinalVAT(final Map<String, String> customParameters) {
		
		return findElement(By.cssSelector(".finalOrderSummaryTotalsContainer > .finalOrderVatContainer")).getText().toString().trim();
		}

	public BigDecimal verifyFinalTotal(final Map<String, String> customParameters)
	{
	BigDecimal AddFinalTotals = CurrencyUtilities.convertPriceStringToBigDecimal(getFinalOrderSubtotalConf()).add(CurrencyUtilities.convertPriceStringToBigDecimal(getFinalTaxTotalConf(customParameters))
			.add(CurrencyUtilities.convertPriceStringToBigDecimal(getFinalsShippingConf(customParameters))));
	
	if (customParameters.containsKey(String.format("OrderCustoms")))
	{
		if (!customParameters.get("OrderCustoms").equals("NULL"))
		{
			AddFinalTotals = AddFinalTotals.add(CurrencyUtilities.convertPriceStringToBigDecimal(getFinalCustomsHandling()));
		}
	}
	//If coupon is applied, subtract coupon discount
	if(customParameters.get(String.format("CouponApplied")).equals("TRUE") || customParameters.get(String.format("CouponApplied")).equals("RE_ENTER"))
	{
		BigDecimal CouponApplied = CurrencyUtilities.convertPriceStringToBigDecimal(getFinalCouponDiscount());
		AddFinalTotals = AddFinalTotals.subtract(CouponApplied);
	}
	
	if(testVariables.isFirstTimePromo(customParameters)==true){
		BigDecimal CouponApplied = CurrencyUtilities.convertPriceStringToBigDecimal(getFinalCouponDiscount());
		AddFinalTotals = AddFinalTotals.subtract(CouponApplied);
	}

	//If cashback is applied, subtract the amount from the final total
	if (customParameters.get(String.format("CashBackApplied")).equals(
			"TRUE"))
	{
		String child = "4";
		//If tax is not displayed, the rows are shifted in the final summary
		if (customParameters.get(String.format("OrderTaxValue")).equals("NULL"))
		{
			child = "3";
			
			if(customParameters.get(String.format("CouponApplied")).equals("TRUE") || customParameters.get(String.format("CouponApplied")).equals("RE_ENTER"))
			{
				child = "4";
			}
		}
		else
		{
			if(customParameters.get(String.format("CouponApplied")).equals("TRUE") || customParameters.get(String.format("CouponApplied")).equals("RE_ENTER"))
			{
				child = "5";
			}
		}
		
		BigDecimal CashbackApplied = CurrencyUtilities
				.convertPriceStringToBigDecimal(findElement(
						By.cssSelector(".finalOrderSummaryTotalsContainer > div:nth-child(" + child + ")")).getText().trim());
		
		AddFinalTotals = AddFinalTotals.subtract(CashbackApplied);
	}

	return AddFinalTotals;
	}
	
	public String getShopConsultantName() {

		// Look for element containing text of shop consultant's name
		// when navigating to the portal
		// If element exists, return text
		WebElement consultantNameElement = waitForElementToBeClickable(
				By.id("shopConsultantFullName"), false, 5);
		if (consultantNameElement != null)
			return consultantNameElement.getText().trim();

		// Look for element containing text of shop consultant's name
		// after logging in with an account
		// If element exists, return text
		consultantNameElement = waitForElementToBeClickable(
				By.id("shopConsultantFullName"), false, 5);
		if (consultantNameElement != null)
			return consultantNameElement.getText().trim();

		// If neither element exists, return
		// empty String to indicate that shop
		// consultant's name does not exist
		return "";
	}

	/**
	 * Get the order details on the page
	 *
	 * @return wait for load
	 */

	private void waitForLoad() {
		this.waitForjQueryAjax("Waiting for page to load");
		waitForElementToBeVisible(By.id("finalOrderOuterWrapper"), 15);
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(getBody(), 30);
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		waitForLoad();
		return By.id("finalOrderOuterWrapper");
	}

}