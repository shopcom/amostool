package com.shop.automation.shop.selenium.pages.base.checkout.mobile;

import java.math.BigDecimal;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.testdata.Address;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.selenium.pages.base.ShopPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopDebugModePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;

public class ShopAddressInfoPageMobile extends ShopPage {
	
	private final static By lowerTotal = By.cssSelector("div.bottom-stripe.cart-subtotal-banner.cart-total span.amount.total");
	private final static By upperTotal = By.cssSelector("div#page section div#main-content.page-content div#checkout div.header-section p.header-aside");
	private final static By continuetoReview = By.xpath("//input[@class='button primary full']");
	
	public ShopAddressInfoPageMobile(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

		try {
			Verify.verifyTrue(atPage(), "Expected to be on Mobile Address Info Page");
		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence of Mobile Address Info Page");
		}
		
	}
	
	public BigDecimal getLowerTotal()
	{
		String element = findElement(lowerTotal).getText().toString().trim();
		BigDecimal total = CurrencyUtilities
				.convertPriceStringToBigDecimal(element);

		return total;
	}
	
	public BigDecimal getUpperTotal()
	{
		String element = findElement(upperTotal).getText().toString().trim();
		BigDecimal total = CurrencyUtilities
				.convertPriceStringToBigDecimal(element);
		
		return total;
	}
	
	public WebElement getContinueButton()
	{
		return findElement(continuetoReview);
	}
	

	private void waitForLoad() {
		waitForElementToBePresent(By.id("checkout"),
				true, 20);
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(getBody());
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		waitForLoad();
		return By.id("checkout");
	}

}
