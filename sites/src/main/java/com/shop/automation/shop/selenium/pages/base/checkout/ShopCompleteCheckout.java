package com.shop.automation.shop.selenium.pages.base.checkout;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.shop.automation.shop.selenium.pages.base.ShopPage;
import com.shop.automation.shop.selenium.pages.base.checkout.mobile.ShopAddressInfoPageMobile;
import com.shop.automation.shop.selenium.pages.base.checkout.mobile.ShopPaymentPageMobile;
import com.shop.automation.shop.selenium.pages.base.checkout.mobile.ShopReviewOrderPageMobile;
import com.shop.automation.shop.selenium.pages.base.common.ShopTestVariables;
import com.shop.automation.shop.selenium.pages.validations.ShopConfirmationPageValidation;
import com.shop.automation.shop.selenium.pages.validations.ShopPlaceOrderPageValidation;
import com.shop.automation.shop.selenium.pages.validations.ShopShippingAndBillingValidation;
import com.shop.automation.shop.selenium.pages.validations.mobile.ShopAddressPageMobileValidation;
import com.shop.automation.shop.selenium.pages.validations.mobile.ShopConfirmationPageMobileValidation;
import com.shop.automation.shop.selenium.pages.validations.mobile.ShopPaymentPageMobileValidation;
import com.shop.automation.shop.selenium.pages.validations.mobile.ShopReviewOrderMobileValidation;

public class ShopCompleteCheckout extends ShopPage {

	public ShopCompleteCheckout(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager,
			final List<Shopper> shoppers, final CreditCard creditCard,
			final List<Product> products,
			final Map<String, String> customParameters, final Country country,
			final Locale locale) {
		super(driver, translator, elementDefinitionManager);
		ShopTestVariables var = new ShopTestVariables(driver, translator,
				elementDefinitionManager);

		if ((var.isMobile(WebDriverUserAgent.getCurrentUserAgent()))
				|| (var.RWDDesktopCheckoutType(locale,
						WebDriverUserAgent.getCurrentUserAgent()))) {
			completeMobileSite(shoppers, creditCard, products,
					customParameters, country);
		} else {
			completeFullSite(shoppers, creditCard, customParameters, country);
		}
	}

	public void completeFullSite(final List<Shopper> shoppers,
			final CreditCard creditCard,
			final Map<String, String> customParameters, final Country country) {
		ShopShippingAndBillingInfoPage ShippingAndBillingInfoPage = new ShopShippingAndBillingInfoPage(
				driver, translator, elementDefinitionManager);

		ShippingAndBillingInfoPage.changeShipping(customParameters, country);

		ShippingAndBillingInfoPage.PaymentMethodUsed(shoppers, creditCard,
				country, customParameters);

		ShippingAndBillingInfoPage.addBags(customParameters, country);

		ShippingAndBillingInfoPage.giftCardApplied(customParameters);

		ShippingAndBillingInfoPage.cashBackApplied(customParameters);

		ShippingAndBillingInfoPage.couponApplied(customParameters);

		new ShopShippingAndBillingValidation(driver, translator,
				elementDefinitionManager, customParameters);

		ShippingAndBillingInfoPage.continueCheckout();

		new ShopPlaceOrderPageValidation(driver, translator,
				elementDefinitionManager, customParameters);

		ShopPlaceOrderPage PlaceOrderPage = new ShopPlaceOrderPage(driver,
				translator, elementDefinitionManager);

		PlaceOrderPage.placeOrderButton();

		new ShopConfirmationPageValidation(driver, translator,
				elementDefinitionManager, customParameters, creditCard,
				shoppers);
	}

	public void completeMobileSite(final List<Shopper> shoppers,
			final CreditCard creditCard, final List<Product> products,
			final Map<String, String> customParameters, final Country country) {
		ShopTestVariables var = new ShopTestVariables(driver, translator,
				elementDefinitionManager);

		if (var.visaCheckout(customParameters).booleanValue() == false) {
			new ShopAddressPageMobileValidation(driver, translator,
					elementDefinitionManager, products, customParameters);

			ShopAddressInfoPageMobile address = new ShopAddressInfoPageMobile(
					driver, translator, elementDefinitionManager);
			address.getContinueButton().click();
		}

		ShopReviewOrderPageMobile review = new ShopReviewOrderPageMobile(
				driver, translator, elementDefinitionManager);
		review.changeShipping(customParameters);

		review.addBags(customParameters);

		new ShopReviewOrderMobileValidation(driver, translator,
				elementDefinitionManager, customParameters);

		review.continueToPayment();

		ShopPaymentPageMobile paymentPage = new ShopPaymentPageMobile(driver,
				translator, elementDefinitionManager);

		paymentPage.enterPaymentInfo(customParameters, shoppers, creditCard,
				country);

		paymentPage.addGiftCard(customParameters);

		paymentPage.addCashback(customParameters);

		paymentPage.addCoupon(customParameters);

		new ShopPaymentPageMobileValidation(driver, translator,
				elementDefinitionManager, customParameters);

		paymentPage.placeOrder();

		new ShopConfirmationPageMobileValidation(driver, translator,
				elementDefinitionManager, customParameters, creditCard,
				shoppers);
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(this.getBody(), 30);
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return By.cssSelector("div#checkout div.header-section h1");
	}

}
