package com.shop.automation.shop.selenium.pages.base.common;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;

public class ShopMyAccountPage extends ShopTemplatePage {

	ShopMyAccountPage(WebDriver driver, Translator translator, ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}

	{
		try {
			Verify.verifyTrue(atPage(), "Expected to be on My Account page");
		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence of My Acount page");
		}
	}

	public ShopPaymentMethodsPage clickEditMyPaymentMethodsLink() {
		findElement(By.xpath(interpolateXpath("//ul/li/a[contains(text(), '$')]", translate("edit_my_payment_methods")))).click();;
		return new ShopPaymentMethodsPage(driver, translator, elementDefinitionManager);
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return false;
	}
}
