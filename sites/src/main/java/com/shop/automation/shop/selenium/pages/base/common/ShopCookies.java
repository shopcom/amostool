package com.shop.automation.shop.selenium.pages.base.common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.shop.automation.shop.selenium.pages.base.ShopPageComponent;

public class ShopCookies extends ShopPageComponent {

	public ShopCookies(final WebDriver driver, final Translator translator,
			final ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}

	// Get cookie "SessionID"
	public String getSessionID() {
		Set<Cookie> allCookies = driver.manage().getCookies();
		for (Cookie loadedCookie : allCookies) {
			if (loadedCookie.getName().contains("CATALOGCITY_SSN")) {
				String sessionID = loadedCookie.getValue();
				return sessionID;
			}
		}
		StepInfo.addMessage(String.format("No SessionID cookie found!"));
		return null;
	}

	public String getSourceID() {
		String sourceId = driver.manage().getCookieNamed("CC_SRCID").getValue();
		return sourceId;
	}

	public String getNSIDCookie() {
		Set<Cookie> allCookies = driver.manage().getCookies();
		for (Cookie loadedCookie : allCookies) {
			if (loadedCookie.getName().contains("NS_ID")) {
				String nsidString = loadedCookie.getValue();
				return nsidString;
			}
		}
		StepInfo.addMessage(String.format("No NSID cookie found!"));
		return null;
	}

	public void printDebugCookies() {
		StepInfo.addMessage("AMOS_NS_ID = " + getNSIDCookie());
		StepInfo.addMessage("Session ID: " + getSessionID());
		// StepInfo.addMessage("Source ID: " + getSourceID());

		logger.info("AMOS_NS_ID = " + getNSIDCookie());
		logger.info("Session ID: " + getSessionID());
		// logger.info("Source ID: " + getSourceID());

	}

	public void getDateTimeStamp() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		StepInfo.addMessage("Current TimeStamp: " + dateFormat.format(date));
		logger.info("Current TimeStamp: " + dateFormat.format(date));
	}

}
