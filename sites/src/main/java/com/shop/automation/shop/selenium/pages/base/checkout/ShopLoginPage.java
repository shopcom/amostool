package com.shop.automation.shop.selenium.pages.base.checkout;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopRegPage;




public class ShopLoginPage extends ShopTemplatePage {

	public ShopLoginPage(final WebDriver driver, final Translator translator, ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

		try {
			Verify.verifyTrue(atPage(), "Expected to be on sign in page");
		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence of sign in page");
		}
	}

	/**
	 * Sign in with existing account
	 *
	 * @param shopper containing User data
	 */
	public void signInWithAccount(Shopper shopper) {
		findElement(By.id("usrID")).sendKeys(shopper.getEmail());
		findElement(By.id("usrPwd")).sendKeys(shopper.getPassword());
		findElement(By.cssSelector("#accountSignIn > input.button.primary")).click();
	}

	/**
	 * Click the "Continue" button on
	 * the sign in page after going to cart
	 */
	public ShopCheckoutRegPage clickContinueWithoutSigningIn() {
		findElement(By.cssSelector("div#login > div.signup > a.button")).click();
		return new ShopCheckoutRegPage(driver, translator, elementDefinitionManager);
	}

	/**
	 * Click the "Continue" button on
	 * the sign in page after clicking
	 * "Create Account" in the header
	 */
	public ShopRegPage clickCreateAccount() {
		findElement(By.cssSelector("div#login > div.signup > a.button")).click();
		return new ShopRegPage(driver, translator, elementDefinitionManager);
	}

	/**
	 * Tests if an error message is
	 * present on sign in Page
	 *
	 * @return boolean result of if error message web element exists
	 */
	public boolean isErrorMessagePresent() {
		return isElementExisting(By.cssSelector("p.corner"));
	}

	/**
	 * Sign into an existing account at checkout
	 *
	 * @param shopper Shopper logging in
	 *
	 * @return ShopBillingPage
	 */
	public ShopShippingAndBillingInfoPage signInToAccountAtCheckout(final Shopper shopper) {

		StepInfo.addMessage(String.format("Login information: Login e-mail: %s Login Password: %s",
								shopper.getEmail(), shopper.getPassword()));
		findElement(By.id("usrID")).sendKeys(shopper.getEmail());
		findElement(By.id("usrPwd")).sendKeys(shopper.getPassword());
		findElement(By.cssSelector("form#accountSignIn > input.button")).click();

		return new ShopShippingAndBillingInfoPage(driver, translator, elementDefinitionManager);
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(getBody(), 15);
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return By.id("login");
	}
}