package com.shop.automation.shop.selenium.pages.base.common.mobile;

import java.util.concurrent.TimeUnit;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;

public class ShopMyAccountPageMobile extends ShopTemplatePage {

	public ShopMyAccountPageMobile(WebDriver driver, Translator translator, ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}

	{
		try {
			Verify.verifyTrue(atPage(), "Expected to be on Mobile My Account page");
		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence of Mobile My Acount page");
		}
	}


	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return By.id("my-account");
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(getBody(), 15);
	}
}
