package com.shop.automation.shop.selenium.pages.base.common;

import java.util.List;
import java.util.Map;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopCheckoutPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopPlaceOrderPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopShippingAndBillingInfoPage;

public class ShopPaymentMethodsPage extends ShopTemplatePage {

	public ShopPaymentMethodsPage(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}

	private ShopPaymentMethodsPage changeCCTypeVisa() {
		Select type = new Select(driver.findElement(By.id("ccNewCardType")));
		type.selectByVisibleText("Visa");
		return new ShopPaymentMethodsPage(driver, translator,
				elementDefinitionManager);

	}

	private ShopPaymentMethodsPage changeCCTypeMC() {
		Select type = new Select(driver.findElement(By.id("ccNewCardType")));
		type.selectByVisibleText("MasterCard");
		return new ShopPaymentMethodsPage(driver, translator,
				elementDefinitionManager);

	}

	private ShopPaymentMethodsPage changeCCTypeAmex() {
		Select type = new Select(driver.findElement(By.id("ccNewCardType")));
		type.selectByVisibleText("American Express");
		return new ShopPaymentMethodsPage(driver, translator,
				elementDefinitionManager);

	}



	public void PaymentMethodUsedNewUser(final List<Shopper> shoppers,
			final CreditCard creditCard, final Country country, final Map<String, String> customParameters) {

		ShopShippingAndBillingInfoPage ShippingAndBillingInfoPage = new ShopShippingAndBillingInfoPage(
				driver, translator, elementDefinitionManager);
		
		ShopTestVariables var = new ShopTestVariables(driver, translator, elementDefinitionManager);
		if (var.visaCheckout(customParameters).booleanValue() == false)
		{
			setCreditCardName(shoppers.get(0));
			
			if (creditCard.getId().equals("AmexCC")) 
			{
				addNewCreditCardAmex(creditCard);
				ShippingAndBillingInfoPage.enterCreditCardAmexCVV(creditCard.getCvv(), country);
			}
			
			else
			{
				addNewCreditCard(creditCard);
				ShippingAndBillingInfoPage.enterCreditCardCVV(creditCard.getCvv(), country);
			}
		}
	}

	/** Click the "Continue Shopping" link */
	public void clickContinueShoppingLink() {
		findElement(By.cssSelector("div.continueShopping > a")).click();
	}

	public List<WebElement> getSelectionOfShippingTypes() {
		return findElements(By.cssSelector("select#shippingSelection option"));
	}

	public String getSelectedShippingMethod() {
		return this.createDropDown(By.cssSelector("select#shippingSelection"))
				.getFirstSelectedOption().getText();
	}

	public void addNewCreditCard(final CreditCard creditCard) {

		// Click "Add Card" button
		// findElement(By.id("addCard")).click();
		changeCCTypeVisa();
		setTextField(By.id("ccNewCardNumber"), creditCard.getNumber());
		createDropDown(By.id("ccNewCardExpMonth")).selectByValue(
				String.format("%02d", creditCard.getExpirationMonth()));
		createDropDown(By.id("ccNewCardExpYear")).selectByValue(
				String.valueOf(creditCard.getExpirationYear()));
		waitForElementToBeClickable(By
				.cssSelector("a.doneNewCCPaymentMethod.saveDetailsButton"));
		findElement(
				By.cssSelector("a.doneNewCCPaymentMethod.saveDetailsButton"))
				.click();
	}
	
	public void addNewMasterCard(final CreditCard creditCard) {

		changeCCTypeMC();
		setTextField(By.id("ccNewCardNumber"), creditCard.getNumber());
		createDropDown(By.id("ccNewCardExpMonth")).selectByValue(
				String.format("%02d", creditCard.getExpirationMonth()));
		createDropDown(By.id("ccNewCardExpYear")).selectByValue(
				String.valueOf(creditCard.getExpirationYear()));
		waitForElementToBeClickable(By
				.cssSelector("a.doneNewCCPaymentMethod.saveDetailsButton"));
		findElement(
				By.cssSelector("a.doneNewCCPaymentMethod.saveDetailsButton"))
				.click();
	}

	public void addNewCreditCardAmex(final CreditCard creditCard) {
		// final CreditCard creditCard
		changeCCTypeAmex();
		setTextField(By.id("ccNewCardNumber"), creditCard.getNumber());
		createDropDown(By.id("ccNewCardExpMonth")).selectByValue(
				String.format("%02d", creditCard.getExpirationMonth()));
		createDropDown(By.id("ccNewCardExpYear")).selectByValue(
				String.valueOf(creditCard.getExpirationYear()));
		waitForElementToBeClickable(By
				.cssSelector("a.doneNewCCPaymentMethod.saveDetailsButton"));
		findElement(
				By.cssSelector("a.doneNewCCPaymentMethod.saveDetailsButton"))
				.click();
	}

	public void setCreditCardName(final Shopper shopper) {

		clickAddNewCreditCardRadioButton();
		WebElement ccNameTexbox = findElement(By.id("ccName"));
		ccNameTexbox.click();
		setTextField(By.id("ccName"), shopper.getFullName());

	}

	/**
	 * Enter credit card number
	 *
	 * @param ccNumber
	 *            String of credit card number being entered
	 */
	private void enterCreditCardNumber(final String ccNumber) {
		// Wait for credit card form to appear
		waitForElementToBeClickable(By.id("ccNewCardNumber"), true, 30);

		// Enter Credit card number
		setTextField(By.id("ccNewCardNumber"), ccNumber);
	}

	/**
	 * Select expiration month of credit card
	 *
	 * @param expirationMonth
	 *            month credit card expires
	 */
	private void selectExpirationMonth(final int expirationMonth) {
		// Format month to be 2 digits (example: "1" becomes "01")
		// to fit proper formatting of month for expiration date
		createDropDown(By.id("ccNewCardExpMonth")).selectByValue(
				String.format("%02d", expirationMonth));
	}

	/**
	 * Select expiration year of credit card
	 *
	 * @param expirationYear
	 *            year credit card expires
	 */
	private void selectExpirationYear(final int expirationYear) {
		// int converted to String to
		// be accepted as argument for
		// selectByValue
		createDropDown(By.id("ccNewCardExpYear")).selectByValue(
				String.valueOf(expirationYear));
	}

	/**
	 * Click the "Save Credit Card" Button
	 */
	private void clickSaveCreditCardButton() {
		findElement(By.id("saveCCdata")).click();
	}

	/**
	 * Enter the CVV of the currently selected credit card
	 *
	 * @param cvv
	 *            String of the CVV being entered
	 */
	private void enterCreditCardCVV(final int cvv) {
		waitForElementToBeClickable(By.id("cvv"), true, 30);
		setTextField(By.id("cvv"), String.valueOf(cvv));
	}

	/**
	 * Click the "Continue" button to proceed with checkout
	 *
	 * @return GlobalShopPlaceOrderPage
	 */
	private ShopPlaceOrderPage clickContinueButton() {
		findElement(By.id("continueShippingBilling")).click();
		return new ShopPlaceOrderPage(driver, translator,
				elementDefinitionManager);
	}

	/**
	 * Get the WebElements for saved credit cards
	 *
	 * @return List<WebElement> for all credit cards on page
	 */
	private List<WebElement> getListOfCreditCards() {
		return findElements(By.id("walletItemGlobalContainer"));
	}

	/**
	 * @return number of credit cards
	 */
	public int numberOfCreditCards() {
		return getListOfCreditCards().size();
	}

	/**
	 * Click the "Add New Credit Card" radio button
	 */
	private void clickAddNewCreditCardRadioButton() {

		WebElement NewPaymentRadio = driver.findElement(By
				.id("addNewPaymentRadio"));
		this.waitForElementToBeClickable(NewPaymentRadio, 10);
		// waitForElementToBePresent(NewPaymentRadio, true, 10).isSelected();
		if (!NewPaymentRadio.isSelected()) {
			NewPaymentRadio.click();
		}

	}

	/**
	 * Click the selected credit card's radio button
	 *
	 * (Top credit card is 1, next one down is 2, etc)
	 *
	 * @param selectedCard
	 *            int of selected card
	 */
	public void selectCreditCard(final int selectedCard) {
		getElementsRadioButton(getListOfCreditCards().get(selectedCard - 1))
				.click();
	}

	// TODO: WRITE THIS JAVADOC COMMENT A LOT BETTER
	/**
	 * Test if credit card is selected or not
	 *
	 * @param choice
	 *            Card being inquired about
	 */
	public boolean isCreditCardSelected(final int choice) {
		return getElementsRadioButton(getListOfCreditCards().get(choice - 1))
				.isSelected();
	}

	/**
	 * Edit the credit card information of chosen card
	 *
	 * @param choice
	 *            which card to be edited
	 * @param newMonth
	 *            the month to be changed to
	 * @param newYear
	 *            year to be changed to
	 */
	public void editCreditCard(final int choice, final int newMonth,
			final int newYear) {

		// Click the credit card's radio button to assure it is selected
		getElementsRadioButton(getListOfCreditCards().get(choice - 1)).click();

		// Click "Edit" link
		getListOfCreditCards().get(choice - 1)
				.findElement(By.cssSelector("a.editCard")).click();

		// TODO: DROPDOWNS REQUIRE BY.ID(). THE IDS ARE RANDOMLY GENERATED
		// WITH FORMAT "jqueryui-select-ccExpYY[NUMBER]" FOR YEAR AND
		// ""jqueryui-select-ccExpMM[NUMBER]" FOR MONTH

		// createDropDown(By.name("ccExpMM")).selectByValue(String.format("%02d",
		// newMonth));
		// createDropDown(By.name("ccExpYY")).selectByValue(String.valueOf(newYear));

		findElement(By.id("saveCCEdit")).click();
	}

	/**
	 * Remove the selected credit card
	 *
	 * @param int choice Credit Card to be removed
	 */
	public void removeCreditCard(final int choice) {
		getListOfCreditCards().get(choice - 1)
				.findElement(By.cssSelector("span a")).click();
	}

	/**
	 * Enter credit card information to pay for order
	 *
	 * @param creditCard
	 *            CreditCard used in payment of order
	 */
	public ShopPlaceOrderPage enterCreditCardInfo(final CreditCard creditCard) {

		// Report credit card info being used, showing only
		// last 4 digits of credit card number
		StepInfo.addMessage(String.format("Credit Card ending %s being used",
				creditCard.getLastFourDigitsOfNumber()));
		StepInfo.addMessage(String.format("Expiration month: %02d",
				creditCard.getExpirationMonth()));
		StepInfo.addMessage(String.format("Expiration year: %s",
				creditCard.getExpirationYear()));
		StepInfo.addMessage(String.format("CVV: %s", creditCard.getCvv()));

		enterCreditCardNumber(creditCard.getNumber());
		selectExpirationMonth(creditCard.getExpirationMonth());
		selectExpirationYear(creditCard.getExpirationYear());

		clickSaveCreditCardButton();

		enterCreditCardCVV(creditCard.getCvv());

		clickContinueButton();

		return new ShopPlaceOrderPage(driver, translator,
				elementDefinitionManager);
	}

	/**
	 * Enter new credit card on Payment Information Page
	 *
	 * @param creditCard
	 *            Credit Card being added to account
	 */
	public void enterNewCreditCardInfo(final CreditCard creditCard) {

		// Report credit card info being used, showing only
		// last 4 digits of credit card number
		StepInfo.addMessage(String.format("Credit Card ending %s being used",
				creditCard.getLastFourDigitsOfNumber()));

		StepInfo.addMessage(String.format("Expiration month: %02d",
				creditCard.getExpirationMonth()));

		StepInfo.addMessage(String.format("Expiration year: %s",
				creditCard.getExpirationYear()));

		changeCCTypeVisa();

		enterCreditCardNumber(creditCard.getNumber());

		selectExpirationMonth(creditCard.getExpirationMonth());

		selectExpirationYear(creditCard.getExpirationYear());

		clickSaveCreditCardButton();
	}

	/**
	 * Get the radio button for the provided element
	 *
	 * @param element
	 *            WebElement to be selected via radio button
	 */
	// TODO: IS THIS SOMETHING FOR WEBCONTENT CLASS?
	private WebElement getElementsRadioButton(final WebElement element) {
		return element.findElement(By.cssSelector("input"));
	}

	/**
	 * @return String of field labeled "Order Subtotal"
	 */
	public String getOrderSubtotal() {
		return findElement(By.cssSelector(".summaryTop .summaryVal")).getText();
	}

	// TODO: THE FOLLOWING 4 XPATHS ARE THE ONLY WAY TO FIND THE REQUIRED
	// ELEMENTS
	// TODO: THIS NEEDS TO BE ADDRESSED

	public String getSubtotal() {
		return findElement(By.xpath("//table[2]/tbody/tr[1]/td[2]")).getText();
	}

	public String getTax() {
		return findElement(By.xpath("//table[2]/tbody/tr[2]/td[2]")).getText();
	}

	public String getShippingCost() {
		return findElement(By.xpath("//table[2]/tbody/tr[3]/td[2]")).getText();
	}

	public String getTotalOrderCost() {
		return findElement(By.xpath("//table[2]/tbody/tr[4]/td[2]")).getText();
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return false;
	}

}