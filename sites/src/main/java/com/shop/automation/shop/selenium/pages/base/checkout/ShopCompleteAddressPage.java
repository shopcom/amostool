package com.shop.automation.shop.selenium.pages.base.checkout;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.shop.automation.shop.selenium.pages.base.ShopPage;
import com.shop.automation.shop.selenium.pages.validations.ShopAddressInfoPageValidation;


public class ShopCompleteAddressPage extends ShopPage{
	
	public ShopCompleteAddressPage(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager, final List<Product> products, final Map<String, String> customParameters, 
			final Country country ) 
	{
		super(driver, translator, elementDefinitionManager);
		
		if(WebDriverUserAgent.getCurrentUserAgent().equals(WebDriverUserAgent.DEFAULT))
		{
		completeFullSite(products, customParameters, country);
		}
		//Completion/Validation of address page for mobile gets done in ShopCompleteCheckout
	}
	
	private void completeFullSite(final List<Product> products, final Map<String, String> customParameters, final Country country)
	{
	
		ShopAddressInfoPage AddressInfoPage = new ShopAddressInfoPage(driver, translator, elementDefinitionManager, country);
		
		this.waitForElementToBeVisible(
				By.xpath("//a[@class='checkoutButton']"), 15);
		
		new ShopAddressInfoPageValidation(driver, translator,
						elementDefinitionManager, products, customParameters, country);

		AddressInfoPage.continueCheckout();
	}
		
		
	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(this.getBody(), 30);
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return By.cssSelector("div#checkout div.header-section h1");
	}
	
}
