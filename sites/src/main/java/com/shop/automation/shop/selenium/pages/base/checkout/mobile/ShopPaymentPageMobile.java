package com.shop.automation.shop.selenium.pages.base.checkout.mobile;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.selenium.pages.base.ShopPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopCheckoutPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopEnetsPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopCustomParameters;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopTestVariables;

public class ShopPaymentPageMobile extends ShopPage {

	private final static By cvvField = By.cssSelector("div label.cvv .cc-cvv");
	private final static By ccName = By.cssSelector("label input#bill-to-name");
	private final static By ccType = By
			.cssSelector("fieldset.select-list.primary select#cc-type");
	private final static By ccNum = By.cssSelector("label input#card-number");
	private final static By expMonth = By
			.cssSelector("label.left fieldset.select-list.primary select");
	private final static By expYear = By
			.cssSelector("label.left.margin-left fieldset.select-list.primary select");
	private final static By couponField = By.cssSelector("input#coupon-code");
	private final static By couponApply = By
			.cssSelector("a#apply-coupon.button.secondary");
	private final static By couponSuccessContainer = By
			.cssSelector("section#use-coupon.promotions div.success-story");
	private final static By couponErrorContainer = By
			.cssSelector("section#use-coupon.promotions p.message.error");
	private final static By couponAppliedContainerAmount = By
			.cssSelector("div#coupons-applied span.strong");
	private final static By removeCoupon = By
			.cssSelector("div#coupons-applied a.remove");
	private final static By gcNumField = By
			.cssSelector("input#gift-card-number");
	private final static By gcPinField = By.cssSelector("input#gift-card-pin");
	private final static By gcApply = By
			.cssSelector("a#apply-gift-card.button.secondary");
	private final static By gcSuccessContainer = By
			.cssSelector("section#use-gift-card.promotions div.success-story");
	private final static By gcErrorContainer = By
			.cssSelector("section#use-gift-card.promotions p.message.error");
	private final static By gcAppliedContainerAmount = By
			.cssSelector("div#gift-cards-applied span.strong");
	private final static By removeGC = By
			.cssSelector("div#gift-cards-applied a.remove");
	private final static By cbackApply = By.cssSelector("input#apply-cashback");
	private final static By upperTotal = By.cssSelector("p.header-aside.total");
	private final static By orderSubtotal = By
			.cssSelector("ul li span.amount.subtotal");
	private final static By orderTax = By.cssSelector("ul li span.amount.tax");
	private final static By orderShipping = By
			.cssSelector("ul li span.amount.shipping-cost");
	private final static By couponAmount = By
			.cssSelector("span.amount.discount");
	private final static By gcAmount = By
			.cssSelector("div#gift-card-credit.gift-card-stripe span.amount");
	private final static By cbackAmount = By
			.cssSelector("div#cashback-credit.cash-back-stripe span.amount");
	private final static By orderFinalTotal = By
			.cssSelector("div.bottom-stripe.cart-subtotal-banner.cart-total span.amount.total");
	private final static By placeOrderButton = By
			.cssSelector("input#place-order.button.primary");
	private final static By ccRadioButton = By.xpath("//fieldset[(@class ='control-group')]//input[(@value='0')]");

	public ShopPaymentPageMobile(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

		try {
			Verify.verifyTrue(atPage(), "Expected to be on Mobile Payment Page");
		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence of Mobile Payment page");
		}
	}

	private void enterCVV(final CreditCard creditCard) {
		String cvv = Integer.toString(creditCard.getCvv());
		List<WebElement> cc = findElements(cvvField);
		if (cc.size() > 2) {
			cc.get(0).click();
			cc.get(0).sendKeys(cvv);
		} else {
			findElement(cvvField).click();
			findElement(cvvField).sendKeys(cvv);
		}

	}

	private void enterNewCVV(final CreditCard creditCard) {
		String cvv = Integer.toString(creditCard.getCvv());
		List<WebElement> cc = findElements(cvvField);

		if (cc.size() > 1) {
			cc.get(cc.size() - 1).click();
			cc.get(cc.size() - 1).sendKeys(cvv);
		} else {
			findElement(cvvField).click();
			findElement(cvvField).sendKeys(cvv);
		}

	}

	public void enterPaymentInfo(final Map<String, String> customParameters,
			final List<Shopper> shoppers, final CreditCard creditCard,
			final Country country) {
		if (customParameters.containsKey(String.format("CreateUser"))) {
			enterNewCC(shoppers, creditCard);
		} else {

			// Once VCO is selected on Mobile, there is no option to change
			// payment method (Different than Desktop)
			ShopTestVariables var = new ShopTestVariables(driver, translator,
					elementDefinitionManager);
			if (var.visaCheckout(customParameters).booleanValue() == true) {
				waitForElementToBePresent(
						By.cssSelector("div.pick-list-section"),
						true, 15);
			} else {
				String cc = creditCard.getId();
				if (cc.equals("NewVisaCC5005") || cc.equals("NewVisaCC4111")
						|| cc.equals("AmexCC") || cc.equals("NewMC5892")) {
					enterNewCC(shoppers, creditCard);
				} else if (cc.equals("eNets")) {
					{
						findElement(placeOrderButton).click();
						ShopEnetsPage ShopEnetsPage = new ShopEnetsPage(driver,
								translator, elementDefinitionManager,
								creditCard);
						ShopEnetsPage.completeEnetsOrder(creditCard);
					}

				} else {
					enterCVV(creditCard);
				}
			}

		}
	}

	private void enterNewCC(final List<Shopper> shoppers,
			final CreditCard creditCard) {
		
		
		if (isElementExistingAndDisplayed(ccRadioButton, 3)){
			StepInfo.addMessage("Adding New Credit Card Payment");
			WebElement cc = findElement(ccRadioButton);
			cc.click();
		}
		
		setCCName(shoppers);
		selectCCType(creditCard);
		enterCCNum(creditCard);
		enterExpDate(creditCard);
		enterNewCVV(creditCard);

	}

	public void setCCName(final List<Shopper> shoppers) {
		findElement(ccName).click();
		setTextField(ccName, shoppers.get(0).getFullName());

	}

	private void selectCCType(final CreditCard creditCard) {
		String creditcard = creditCard.getId();
		switch (creditcard) {
		case "AmexCC": {
			creditcard = "American Express";
			break;
		}
		case "VisaCC5005": {
			creditcard = "Visa";
			break;
		}

		case "VisaCC4111": {
			creditcard = "Visa";
			break;
		}
		case "NewVisaCC5005": {
			creditcard = "Visa";
			break;
		}

		case "NewVisaCC4111": {
			creditcard = "Visa";
			break;
		}
		case "NewMC5892":
			creditcard = "MasterCard";
			break;

		default:
			creditcard = "Visa";
		}

		Select type = new Select(driver.findElement(ccType));
		type.selectByVisibleText(creditcard);
	}

	private void enterCCNum(final CreditCard creditCard) {
		findElement(ccNum).click();
		setTextField(ccNum, creditCard.getNumber());
	}

	private void enterExpDate(final CreditCard creditCard) {
		String exMonth = "0"
				+ Integer.toString(creditCard.getExpirationMonth());
		Select month = new Select(driver.findElement(expMonth));
		month.selectByVisibleText(exMonth);

		String exYear = Integer.toString(creditCard.getExpirationYear());
		Select year = new Select(driver.findElement(expYear));
		year.selectByVisibleText(exYear);
	}

	public void addCashback(final Map<String, String> customParameters) {
		if (customParameters.get(String.format("CashBackApplied")).equals(
				"TRUE")) {
			applyCashback();
		}
	}

	private void applyCashback() {
		this.waitForElementToBePresent(cbackApply, true, 10);
		WebElement cashback_checkbox = findElement(cbackApply);

		String cashbackApplied = findElement(cbackAmount).getText().trim();

		if (!cashback_checkbox.isSelected()) {
			cashback_checkbox.click();
		}

		StepInfo.addMessage(String.format("Cashback Applied: %s",
				cashbackApplied));
	}

	public void addGiftCard(final Map<String, String> customParameters) {
		if (customParameters.get(String.format("GiftCardApplied")).equals(
				"TRUE")) {
			enterGC(customParameters);
			gcSuccess();
		}
		if (customParameters.get(String.format("GiftCardApplied")).equals(
				"ERROR")) {
			enterGC(customParameters);
			gcError();
		}
	}

	private void enterGC(final Map<String, String> customParameters) {
		setTextField(gcNumField,
				customParameters.get(String.format("GiftCardNumber")));
		setTextField(gcPinField,
				customParameters.get(String.format("GiftCardPin")));
		findElement(gcApply).click();
	}

	private void gcSuccess() {
		this.waitForElementToBeVisible(gcSuccessContainer, 10);
		this.waitForElementToBeVisible(gcAmount, 10);

		String giftCardAmountApplied = findElement(gcAppliedContainerAmount)
				.getText();

		StepInfo.addMessage(String.format("Gift Card Amount Applied: %s",
				giftCardAmountApplied));
	}

	private void gcError() {
		this.waitForElementToBeVisible(gcErrorContainer, 15);
	}

	public void removeGC() {
		findElement(removeGC).click();
		waitForElementToBeInvisible(gcSuccessContainer, 5);
	}

	public void addCoupon(final Map<String, String> customParameters) {
		if (customParameters.containsKey("CouponApplied")) {
			if (customParameters.get(String.format("CouponApplied")).equals(
					"TRUE")) {
				enterCoupon(customParameters);
				couponSuccess();
			}

			if (customParameters.get(String.format("CouponApplied")).equals(
					"ADD_REMOVE")) {
				enterCoupon(customParameters);
				couponSuccess();
				removeCoupon();
			}

			if (customParameters.get(String.format("CouponApplied")).equals(
					"ERROR")) {
				enterCoupon(customParameters);
				couponError();

			}

			if (customParameters.get(String.format("CouponApplied")).equals(
					"RE_ENTER")) {
				enterCoupon(customParameters);
				couponSuccess();
				enterCoupon(customParameters);
				couponError();
			}
		}

	}

	public void enterCoupon(final Map<String, String> customParameters) {
		setTextField(couponField,
				customParameters.get(String.format("CouponCode")));
		findElement(couponApply).click();

	}

	public void couponSuccess() {
		this.waitForElementToBeVisible(couponSuccessContainer, 10);
		this.waitForElementToBeVisible(couponAmount, 10);
		String appliedAmount = findElement(couponAppliedContainerAmount)
				.getText();

		StepInfo.addMessage(String.format("Coupon Discount Applied: -%s",
				appliedAmount));
	}

	public void couponError() {
		this.waitForElementToBeVisible(couponErrorContainer, 10);
		String errorMessage = findElement(couponErrorContainer).getText();

		StepInfo.addMessage(String.format("%s", errorMessage));
	}

	public void removeCoupon() {
		waitForElementToBeClickable(removeCoupon, true, 5);

		findElement(removeCoupon).click();
		waitForElementToBeInvisible(couponSuccessContainer, 5);
		waitForElementToBeInvisible(couponAmount, 5);
	}

	public BigDecimal getUpperTotal() {
		String element = findElement(upperTotal).getText().toString().trim();
		BigDecimal total = CurrencyUtilities
				.convertPriceStringToBigDecimal(element);
		return total;
	}

	public BigDecimal getOrderSubtotal() {
		List<WebElement> subtotals = findElements(orderSubtotal);
		BigDecimal subtotal = new BigDecimal(0);
		if (subtotals.size() > 1) {
			for (int i = 0; i < subtotals.size() - 1; ++i) {
				subtotal = subtotal.add(CurrencyUtilities
						.convertPriceStringToBigDecimal(subtotals.get(i)
								.getText().trim()));
			}
		} else {
			String element = findElement(orderSubtotal).getText().toString()
					.trim();
			subtotal = CurrencyUtilities
					.convertPriceStringToBigDecimal(element);
		}
		return subtotal;
	}

	public BigDecimal getOrderTax(final Map<String, String> customParameters) {
		BigDecimal tax = new BigDecimal("0");
		if (!customParameters.get(String.format("OrderTaxValue"))
				.equals("NULL")) {
			List<WebElement> taxElement = findElements(orderTax);
			if (taxElement.size() > 1) {
				for (int i = 0; i < taxElement.size() - 1; ++i) {
					tax = tax.add(CurrencyUtilities
							.convertPriceStringToBigDecimal(taxElement.get(i)
									.getText().trim()));
				}
			} else {
				String element = findElement(orderTax).getText().toString()
						.trim();
				tax = CurrencyUtilities.convertPriceStringToBigDecimal(element);
			}
		}

		return tax;
	}

	public BigDecimal getOrderShipping() {
		List<WebElement> ship = findElements(orderShipping);
		BigDecimal shipping = new BigDecimal(0);
		if (ship.size() > 1) {
			for (int i = 0; i < ship.size() - 1; ++i) {
				shipping = shipping.add(CurrencyUtilities
						.convertPriceStringToBigDecimal(ship.get(i).getText()
								.trim()));
			}
		} else {
			String element = findElement(orderShipping).getText().toString()
					.trim();
			shipping = CurrencyUtilities
					.convertPriceStringToBigDecimal(element);
		}

		return shipping;
	}

	public BigDecimal getCbackAmount() {
		String element = findElement(cbackAmount).getText().toString().trim();
		BigDecimal cback = CurrencyUtilities
				.convertPriceStringToBigDecimal(element);
		return cback;
	}

	public BigDecimal getCouponAmount() {
		BigDecimal coupon = new BigDecimal("0");
		List<WebElement> discount = findElements(couponAmount);
		if (discount.size() > 1) {
			for (int i = 0; i < discount.size() - 1; i++) {
				String element = discount.get(i).getText().toString().trim();
				coupon = coupon.add(CurrencyUtilities
						.convertPriceStringToBigDecimal(element));
			}
		} else {
			String element = findElement(couponAmount).getText().toString()
					.trim();
			coupon = CurrencyUtilities.convertPriceStringToBigDecimal(element);
		}
		return coupon;
	}

	public BigDecimal getGCAmount() {
		String element = findElement(gcAmount).getText().toString().trim();
		BigDecimal gc = CurrencyUtilities
				.convertPriceStringToBigDecimal(element);
		return gc;
	}

	public BigDecimal getFinalTotal() {
		String element = findElement(orderFinalTotal).getText().toString()
				.trim();
		BigDecimal total = CurrencyUtilities
				.convertPriceStringToBigDecimal(element);
		return total;
	}

	public BigDecimal addTotals(final Map<String, String> customParameters) {
		BigDecimal finaltotal = getOrderSubtotal().add(
				getOrderTax(customParameters)).add(getOrderShipping());
		if (customParameters.get(String.format("CouponApplied")).equals("TRUE")
				|| customParameters.get(String.format("CouponApplied")).equals(
						"RE_ENTER")) {
			finaltotal = finaltotal.subtract(getCouponAmount());
		}

		if (customParameters.get(String.format("GiftCardApplied")).equals(
				"TRUE")) {
			finaltotal = finaltotal.subtract(getGCAmount());
		}

		if (customParameters.get(String.format("CashBackApplied")).equals(
				"TRUE")) {
			finaltotal = finaltotal.subtract(getCbackAmount());
		}

		return finaltotal;
	}

	public void placeOrder() {
		findElement(placeOrderButton).click();
	}

	public WebElement waitForLoad() {
		return waitForElementToBePresent(By.className("shop-form"), true, 15);
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(this.getBody(), 30);
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		waitForLoad();
		return By.className("shop-form");
	}

}