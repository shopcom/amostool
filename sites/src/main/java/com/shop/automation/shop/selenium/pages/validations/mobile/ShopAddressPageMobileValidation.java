package com.shop.automation.shop.selenium.pages.validations.mobile;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Validate;
import com.shop.automation.shop.selenium.pages.base.checkout.mobile.ShopAddressInfoPageMobile;
import com.shop.automation.shop.selenium.pages.base.common.ShopCustomParameters;
import com.shop.automation.shop.selenium.pages.base.common.ShopProductPage;
import com.shop.automation.shop.selenium.pages.base.common.mobile.ShopHeaderPageMobile;

public class ShopAddressPageMobileValidation {

	public ShopAddressPageMobileValidation(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager,
			List<Product> products, final Map<String, String> customParameters) {
		
		ShopAddressInfoPageMobile addressPage = new ShopAddressInfoPageMobile(driver, translator, elementDefinitionManager);
		
		ShopCustomParameters shopCustomParameters = new ShopCustomParameters(
				driver, translator, elementDefinitionManager, customParameters);
		
		Validate.validateTrue(
				addressPage.getLowerTotal()
						.compareTo(shopCustomParameters.getCPInvoiceVendorSubTotal(customParameters)) == 0,
				String.format("Expected Lower Total: %s",
						shopCustomParameters.getCPInvoiceVendorSubTotal(customParameters)));
		
		Validate.validateTrue(
				addressPage.getUpperTotal()
						.compareTo(shopCustomParameters.getCPInvoiceVendorSubTotal(customParameters)) == 0,
				String.format("Expected Upper Total: %s",
						shopCustomParameters.getCPInvoiceVendorSubTotal(customParameters)));
	}

}