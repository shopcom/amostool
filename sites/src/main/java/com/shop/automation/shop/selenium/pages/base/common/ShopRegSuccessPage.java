package com.shop.automation.shop.selenium.pages.base.common;

import com.marketamerica.automation.testdata.User;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;
import com.shop.automation.shop.selenium.pages.base.common.mobile.ShopRegSuccessPageMobile;

public class ShopRegSuccessPage extends ShopTemplatePage {

	public ShopRegSuccessPage(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager, final Country country) {
		super(driver, translator, elementDefinitionManager);

		if(WebDriverUserAgent.getCurrentUserAgent().equals(WebDriverUserAgent.DEFAULT))
    	{
			try {
				Verify.verifyTrue(atPage(),
						"Expected to be on signed in page");

			} catch (UnableToVerifyPagesExistenceException e) {
				StepInfo.failTest("Unable to verify existence of signed in success page");
			}
		}
		else
		{
			new ShopRegSuccessPageMobile(driver, translator, elementDefinitionManager, country);
		}

		ShopDebugModePage debugpage = new ShopDebugModePage(driver, translator, elementDefinitionManager);
		debugpage.getDebugCookies();
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(getBody());
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return By.xpath("//div[contains(@class,'ccll')]");
	}

	/**
	 * Checks to see if the correct user is logged in
	 *
	 * @param user
	 *            to check to see if we are signed in
	 * @return boolean indicating if the user is signed in
	 */
	public <T extends User> boolean isSignedIn(final T user) {
		return header.isSignedIn(user);
	}
}