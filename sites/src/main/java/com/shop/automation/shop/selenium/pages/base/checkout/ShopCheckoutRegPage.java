package com.shop.automation.shop.selenium.pages.base.checkout;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.Random;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopPaymentMethodsPage;

class ShopCheckoutRegPage extends ShopTemplatePage {

    public ShopCheckoutRegPage(WebDriver driver,
                               Translator translator, ElementDefinitionManager elementDefinitionManager) {
        super(driver, translator, elementDefinitionManager);
    }

    /**
     * Enter the e-mail in the e-mail field of the page's form
     *
     * @param email String of e-mail to be enterd
     */
    private void enterEmail(final String email) {
        setTextField(By.id("email"), email);
    }

    /**
     * Enter the password in the password field of the page's form
     *
     * @param password String of password to be entered
     */
    private void enterPassword(final String password) {
        setTextField(By.id("userPwd"), password);
    }

    /**
     * Enter the confirmation password in the confirm password field of the
     * page's form
     *
     * @param passwordConfirm String of password confirmation to be entered
     */
    private void enterConfirmPassword(final String passwordConfirm) {
        setTextField(By.id("userConPwd"), passwordConfirm);
    }

    /**
     * Select the correct country from the page's dropdown
     *
     * @param country String of country to be selected
     */
    private void selectCountry(final String country) {
        createDropDown(By.id("shpCountrySelect")).selectByVisibleText(country);
    }

    /**
     * Enter the first name in the first name field of the page's form
     *
     * @param firstName String of first name to be entered
     */
    private void enterFirstName(final String firstName) {
        setTextField(By.id("shpFirstName"), firstName);
    }

    /**
     * Enter the last name in the last name field of the page's form
     *
     * @param lastName String of last name to be entered
     */
    private void enterLastName(final String lastName) {
        setTextField(By.id("shpLastName"), lastName);
    }

    /**
     * Enter the street address in the street field of the page's form
     *
     * @param street String of the street to be entered
     */
    private void enterAddressStreet(final String street) {
        setTextField(By.name("shpaddr1"), street);
    }

    /**
     * Enter the city in the city field of the page's form
     *
     * @param city String of the city to be entered
     */
    private void enterAddressCity(final String city) {
        setTextField(By.name("shpcity"), city);
    }

    /**
     * Select the correct state from the page's dropdown
     *
     * @param state String of state to be selected
     */
    private void selectAddressState(final String state) {
        createDropDown(By.id("shpState")).selectByVisibleText(state);
    }

    /**
     * Enter the ZIP/Postal code in the ZIP/Postal code field of the page's form
     *
     * @param postalCode String of ZIP/Postal code to be entered
     */
    private void enterPostalCode(final String postalCode) {
        setTextField(By.name("shppCode"), postalCode);
    }

    /**
     * Enter the phone number in the phone number field of the page
     *
     * @param phoneNumber String of the phone number to be entered
     */
    private void enterPhoneNumber(final String phoneNumber) {
        setTextField(By.id("phone"), phoneNumber);
    }

    /**
     * Clicks the radio button to set the shipping and billing address as the
     * same
     */
    private void selectShippingSameAsBillingRadioButton() {
        findElement(By.id("sameAsShipping")).click();
    }

    /**
     * Click the "Submit" button
     */
    private void clickSubmitButton() {
        findElement(By.id("btnSubmit")).click();
    }

    /**
     * Sign up for a new account at checkout
     *
     * @param shopper Shopper data being used to sign up for a new account
     */
    public ShopPaymentMethodsPage signUpAccountAtCheckout(
            final Shopper shopper) {

        final String email = String.format("%s%s%s", shopper.getFirstName(),
                new Random().nextInt(), shopper.getEmail());
        shopper.setEmail(email);
        StepInfo.addMessage(String.format(
                "New User Registered with the following email: %s", email));
        // Enter e-mail
        enterEmail(String.format(email));
        // Create password
        enterPassword(shopper.getPassword());
        // Confirm password
        enterConfirmPassword(shopper.getPassword());

        // Select country
        selectCountry(shopper.getAddress().getCountry());

        // Enter first name
        enterFirstName(shopper.getFirstName());
        // Enter last name
        enterLastName(shopper.getLastName());

        // Enter street address
        enterAddressStreet(shopper.getAddress().getStreet());
        // Enter town/city
        enterAddressCity(shopper.getAddress().getCity());

        // Select state
        selectAddressState(shopper.getAddress().getState());

        // Enter Zip/Postal code
        enterPostalCode(shopper.getAddress().getZipCode());

        // Enter Phone number
        enterPhoneNumber(shopper.getPhoneNumber());

        // Select same billing and shipping
        selectShippingSameAsBillingRadioButton();
        // Continue
        clickSubmitButton();

        return new ShopPaymentMethodsPage(driver, translator, elementDefinitionManager);
    }

    @Override
    public boolean atPage() throws UnableToVerifyPagesExistenceException {
        throw new UnableToVerifyPagesExistenceException();
    }

    @Override
    protected By getBody() throws UnableToVerifyPagesExistenceException {
        throw new UnableToVerifyPagesExistenceException();
    }

}