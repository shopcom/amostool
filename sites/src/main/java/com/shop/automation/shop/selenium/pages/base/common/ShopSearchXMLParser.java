package com.shop.automation.shop.selenium.pages.base.common;

import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.marketamerica.automation.utilities.helpers.HttpUtilities;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;

//@author Rick Vera

public class ShopSearchXMLParser extends ShopTemplatePage {

	public ShopSearchXMLParser(final WebDriver driver,
			final Translator translator,
			final ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}

	final static Logger logger = LogManager
			.getLogger(ShopSearchXMLParser.class);
	ShopDebugModePage debugModePage = new ShopDebugModePage(driver, translator,
			elementDefinitionManager);
	private ShopRestCallParser restCallParser = new ShopRestCallParser(driver,
			translator, elementDefinitionManager);

	/*
	 * public static void main(String[] args) { String testURL =
	 * 
	 * ; verify(testURL); }
	 */
	public void getXMLData() {
		driver.navigate().refresh();
		/*
		 * ShopDebugModePage debugModePage = new ShopDebugModePage(driver,
		 * translator, elementDefinitionManager);
		 */
		ShopRestCallParser restCallParser = new ShopRestCallParser(driver,
				translator, elementDefinitionManager);
		getContainerNames(restCallParser.getSearchURL());

		// getHistogramNames(debugModePage.getSearchURL(), null);
		getOPContainerID(restCallParser.getSearchURL());
	}

	String resultsCount() {
		String resultsCount = getResultsCount(restCallParser.getSearchURL());
		return resultsCount;
	}

	private static Document getDocument(String uriString) {

		URI uri;
		try {
			uri = new URI(uriString);
		} catch (URISyntaxException e) {
			Verify.failTest(String.format("Provided URI was not valid (%s)",
					uriString));
			return null;
		}

		String downloadedXML = HttpUtilities.get(uri);
		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document document = null;

		try {
			document = jdomBuilder.build(new StringReader(downloadedXML));
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return document;

	}

	private String getResultsCount(String uriString) {
		Element rootNode = getDocument(uriString).getRootElement();
		String resultCount = rootNode.getChild("mainResult").getAttributeValue(
				"resultCount");
		/*
		 * System.out.println("Result Count: " +
		 * rootNode.getChild("mainResult").getAttributeValue( "resultCount"));
		 */

		return resultCount;
	}

	public String getSearchTerm(String uriString) {
		Element rootNode = getDocument(uriString).getRootElement();
		String searchTerm = rootNode.getChild("mainQuery").getChildText(
				"rawUserQuery");
		System.out.println("Raw User Query: "
				+ rootNode.getChild("mainQuery").getChildText("rawUserQuery"));
		return searchTerm;
	}

	@SuppressWarnings({ "unused", "unchecked" })
	public String getHistogramNames(String uriString, String names) {
		Element rootNode = getDocument(uriString).getRootElement();
		Element response = rootNode.getChild("mainResult");
		Element results = response.getChild("results");
		List<Element> documentElements = results.getChildren();
		List<Element> list = response.getChildren();

		for (int i = 0; i < list.size(); i++) {

			Element node = list.get(i);
			names = node.getName();
			System.out.println("*" + node.getName());

		}
		return names;
	}

	@SuppressWarnings({ "unchecked" })
	private List<String> getOPContainerID(String uriString) {
		Element rootNode = getDocument(uriString).getRootElement();
		Element response = rootNode.getChild("mainResult");
		Element results = response.getChild("results");
		List<Element> documentElements = results.getChildren();
		List<String> result = new ArrayList<String>();
		for (int i = 0; i < documentElements.size(); i++) {
			result.add(documentElements.get(i).getAttributeValue("id")
					.toString());
		}
		// System.out.println(result);
		return result;
	}

	public List<String> getProdContainerID(String uriString) {
		Element rootNode = getDocument(uriString).getRootElement();
		Element response = rootNode.getChild("mainResult");
		Element results = response.getChild("results");
		List<Element> documentElements = results.getChildren();
		List<Element> list = response.getChildren();
		List<String> ProdContainerID = new ArrayList<String>();
		for (Element documentElement : documentElements) {
			List<Element> fieldElements = documentElement.getChildren();
			for (Element field : fieldElements) {
				if (field.getAttributeValue("name").equals("ProdTuple")) {
					ProdContainerID.add(field.getChildText("string").toString()
							.substring(13, 23));

				}
			}
		}
		System.out.println(ProdContainerID);
		return ProdContainerID;
	}

	public List<String> getContainerNames(String uriString) {
		Element rootNode = getDocument(uriString).getRootElement();
		Element response = rootNode.getChild("mainResult");
		Element results = response.getChild("results");
		List<Element> documentElements = results.getChildren();
		List<Element> list = response.getChildren();
		List<String> containerName = new ArrayList<String>();
		for (Element documentElement : documentElements) {
			List<Element> fieldElements = documentElement.getChildren();
			for (Element field : fieldElements) {
				if (field.getAttributeValue("name").equals("ContainerText_en")) {
					containerName.add(field.getChildText("string").toString().replaceAll("\\s{2}"," "));
				}
			}
		}
		//System.out.println(containerName);
		return containerName;
	}
	
		
	public List<String> getTitleName() {
		List<WebElement> titleName = driver.findElements(By
				.xpath("//ul[(@id='content')]//h3[contains(@class,'title')]"));
		List<String> title = new ArrayList<String>();
		for (int i = 0; i < titleName.size(); i++) {
			title.add(titleName.get(i).getText().toString().substring(0, 2)
					.replace("\"", ""));

		}
		return title;
	}
	
	/*
	 * public static void getXMLdata(String uriString) { URI uri; try { uri =
	 * new URI(uriString); } catch (URISyntaxException e) {
	 * Verify.failTest(String.format("Provided URI was not valid (%s)",
	 * uriString)); return ; }
	 * 
	 * String downloadedXML = HttpUtilities.get(uri); final SAXBuilder
	 * jdomBuilder = new SAXBuilder(); Document document = null;
	 * 
	 * try {
	 * 
	 * 
	 * document = jdomBuilder.build(new StringReader(downloadedXML)); Element
	 * rootNode = document.getRootElement(); Element response =
	 * rootNode.getChild("mainResult"); Element results =
	 * response.getChild("results"); List<Element> documentElements =
	 * results.getChildren();
	 * 
	 * List<Element> list = response.getChildren();
	 * 
	 * System.out.println("Result Count: " +
	 * rootNode.getChild("mainResult").getAttributeValue( "resultCount"));
	 * System.out.println("Raw User Query: " +
	 * rootNode.getChild("mainQuery").getChildText( "rawUserQuery"));
	 * 
	 * 
	 * for (int i = 0; i < list.size(); i++) {
	 * 
	 * Element node = list.get(i); String names = node.getName();
	 * System.out.println("*" + node.getName());
	 * 
	 * }
	 * 
	 * 
	 * for (Element documentElement : documentElements) {
	 * System.out.println("OPContainerID: " +
	 * documentElement.getAttributeValue("id"));
	 * 
	 * List<Element> fieldElements = documentElement.getChildren();
	 * 
	 * for (Element field : fieldElements) { if
	 * (field.getAttributeValue("name").equals("ContainerText_en")) { String
	 * containerName = field.getChildText("string");
	 * System.out.println("Container Text: " + field.getChildText("string")); }
	 * } }
	 * 
	 * 
	 * 
	 * } catch (IOException io) { logger.error(io); } catch (JDOMException
	 * jdomex) { logger.error(jdomex); } }
	 */

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return false;
	}
}