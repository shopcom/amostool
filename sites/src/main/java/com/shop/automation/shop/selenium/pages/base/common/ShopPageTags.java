package com.shop.automation.shop.selenium.pages.base.common;

import org.openqa.selenium.WebDriver;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.shop.automation.shop.selenium.pages.base.ShopPageComponent;

public class ShopPageTags extends ShopPageComponent {

	public ShopPageTags(final WebDriver driver, final Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}

}
