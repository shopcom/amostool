package com.shop.automation.shop.selenium.pages.base.common.shopadvisor;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;

public class ShopAdvisorUsagePage extends ShopTemplatePage {

	@FindBy(xpath = "//input[@value='1:For+Him']")
	@CacheLookup
	private WebElement forHimCheckbox;

	@FindBy(xpath = "//input[@value='Next']")
	@CacheLookup
	private WebElement nextButton;

	String pageUrl = "/shoppingadvisor/usage";

	public ShopAdvisorUsagePage(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager) {

		super(driver, translator, elementDefinitionManager);

	}

	public void selectUsageCategories() {

		forHimCheckbox.click();
	}

	public ShopAdvisorSearchPage shopAdvisorGoToSearchPage() {

		nextButton.click();
		return new ShopAdvisorSearchPage(driver, translator,
				elementDefinitionManager);
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return (this.driver.getCurrentUrl().contains(pageUrl) && this.footer
				.atPageComponent());
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return null;
	}

}
