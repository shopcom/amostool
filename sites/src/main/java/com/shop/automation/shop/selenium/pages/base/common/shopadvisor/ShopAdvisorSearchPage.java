package com.shop.automation.shop.selenium.pages.base.common.shopadvisor;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopPageTags;

public class ShopAdvisorSearchPage extends ShopTemplatePage {

	public ShopAdvisorHeaderComponent shopAdvisorHeader;
	public ShopAdvisorRoomMenuComponent shopAdvisorRoomMenu;
	public ShopAdvisorSubRoomMenuComponent shopAdvisorSubRoomMenu;
	public ShopAdvisorAliasMenuComponent shopAdvisorAliasMenu;

	// FindAll isn't what we're going to want to use. This needs to be fleshed
	// out for more detailed tests
	@FindAll({ @FindBy(xpath = "//a[@class='button primary saAddToCart']") })
	@CacheLookup
	private List<WebElement> addToCartButtonList;

	@FindAll({ @FindBy(xpath = "//a[@class='button secondary saAddMyList']") })
	@CacheLookup
	private List<WebElement> addToListButtonList;

	@FindBy(xpath = "//div[@id='showMessageOverlay']")
	@CacheLookup
	private WebElement messageOverlay;

	@FindBy(xpath = "//a[@class='close-sprite']")
	@CacheLookup
	private WebElement closeSprite;

	int waitTimeout = 1000;

	String pageUrl = "/shoppingadvisor/results";

	public ShopAdvisorSearchPage(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager) {

		super(driver, translator, elementDefinitionManager);

		shopAdvisorHeader = new ShopAdvisorHeaderComponent(driver, translator,
				elementDefinitionManager);
		shopAdvisorRoomMenu = new ShopAdvisorRoomMenuComponent(driver,
				translator, elementDefinitionManager);
		shopAdvisorSubRoomMenu = new ShopAdvisorSubRoomMenuComponent(driver,
				translator, elementDefinitionManager);
		shopAdvisorAliasMenu = new ShopAdvisorAliasMenuComponent(driver,
				translator, elementDefinitionManager);

	}

	public void addProdtoCart(Product product) {

		addToCartButtonList.get(0).click();
	}

	public void addProdtoList(Product product) {

		addToListButtonList.get(1).click();
		waitForElementToBeClickable(messageOverlay, waitTimeout);
		closeSprite.click();
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return (this.driver.getCurrentUrl().contains(pageUrl) && this.footer
				.atPageComponent());
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return null;
	}

}
