package com.shop.automation.shop.selenium.pages.base.common.mobile;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Validate;
import com.marketamerica.automationframework.tools.MarketAmericaSiteURLs;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.configuration.ShopSiteConfiguration;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;

public class ShopCartPageMobile extends ShopTemplatePage {
	
	private final static By checkoutButtonMobile = By.xpath("//a[@class='button primary checkout margin-right margin-bottom ']");
	private final static By upperSubtotal = By.cssSelector("div#shopping-cart p.header-aside span.amount");
	private final static By orderTotal = By.cssSelector("div.bottom-stripe.cart-subtotal-banner.cart-total span.amount");
	private final static By storeTotals = By.cssSelector("div.cart-items.show-detail-view div.bottom-stripe.cart-subtotal-banner span.amount");
	private final static By visaCheckout = By.cssSelector("div#shopping-cart div.outside-payments div.vco-container img.v-button");
	private final static By prodOptions = By.cssSelector("div.product-info div.option");
	
	public ShopCartPageMobile(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
		try {
			Verify.verifyTrue(atPage(), "Expected to be on Cart Page Mobile");
		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence of Cart Page Mobile");
		}

	}
	
	public BigDecimal getUpperSubtotal()
	{
		String element = findElement(upperSubtotal).getText().toString().trim();
		BigDecimal subtotal = CurrencyUtilities
				.convertPriceStringToBigDecimal(element);
		
		return subtotal;
	}
	
	public BigDecimal getOrderTotal()
	{
		String element = findElement(orderTotal).getText().toString().trim();
		BigDecimal total = CurrencyUtilities
				.convertPriceStringToBigDecimal(element);
		
		return total;
	}
	
	public BigDecimal addStoreTotals()
	{
		List<WebElement> storeTotal = findElements(storeTotals);
		BigDecimal total = new BigDecimal(0);
		for (int i = 0; i < storeTotal.size(); ++i) {
			total = total
					.add(CurrencyUtilities
							.convertPriceStringToBigDecimal(storeTotal
									.get(i).getText().trim()));
			}
		
		return total;
	}


	public WebElement getCheckoutButton()
	{
		return findElement(checkoutButtonMobile);
	}
	
	public WebElement getVCOButton()
	{
		return findElement(visaCheckout);
	}
	
	public List<String> getProdOptions()
	{
		List<WebElement> elements = findElements(prodOptions);
		List<String> options = new ArrayList<String>();
		for (int i=0; i < elements.size(); i++)
		{
			//System.out.println("Get Text of Element on Cart page: " +elements.get(i).getText());
			options.add(i, elements.get(i).getText());
		}
		return options;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(getBody(), 15);
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
			return By.id("shopping-cart");
		
	}

}
