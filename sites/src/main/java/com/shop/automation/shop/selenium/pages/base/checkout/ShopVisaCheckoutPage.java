package com.shop.automation.shop.selenium.pages.base.checkout;

import java.util.List;
import java.util.concurrent.TimeUnit;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.selenium.pages.base.ShopPage;


class ShopVisaCheckoutPage extends ShopPage {

	ShopVisaCheckoutPage(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

		try {
			Verify.verifyTrue(atPage(), "Expected Visa Checkout Overlay");
		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence of Visa Checkout Overlay");
		}
		

	}
	
		
	void signInVisaCheckout(Shopper shopper, CreditCard creditCard)
	{
		switchFrames();
		this.waitForElementToBeClickable(By.id("goToSignIn"), true, 20);
		findElement(By.id("goToSignIn")).click();
		findElement(By.id("userName")).sendKeys(shopper.getEmail());
		findElement(By.id("password")).sendKeys(shopper.getPassword());
		findElement(By.id("v-sign-in-button")).click();
		selectVisaCheckoutCard(creditCard);
		this.waitForElementToBeClickable(By.id("v-submit-intent"), true, 20);
		findElement(By.id("v-submit-intent")).click();
		driver.switchTo().defaultContent();
			
	}
	
	void registerVisaCheckout(Shopper shopper, CreditCard creditCard)
	{
		switchFrames();
		this.waitForElementToBeClickable(By.id("v-sign-up-button"), true, 20);
		findElement(By.id("emailAddress")).sendKeys(shopper.getEmail());
		findElement(By.id("v-sign-up-button")).click();
		addVisaCheckoutAddr(shopper);
		addVisaCheckoutCard(creditCard);
		createAccountVisa(shopper);
	}
	
	private void addVisaCheckoutAddr (Shopper shopper)
	{
		this.waitForElementToBeClickable(By.xpath(".//*[@id='v-shipping-create']/div[2]/button"), true, 20);
		findElement(By.id("v-shipping-create_first_name")).click();
		findElement(By.id("v-shipping-create_first_name")).sendKeys(shopper.getFirstName());
		findElement(By.id("v-shipping-create_last_name")).click();
		findElement(By.id("v-shipping-create_last_name")).sendKeys(shopper.getLastName());
		findElement(By.id("v-shipping-create_line1")).click();
		findElement(By.id("v-shipping-create_line1")).sendKeys(shopper.getAddress().getStreet());
		findElement(By.id("v-shipping-create_city")).click();
		findElement(By.id("v-shipping-create_city")).sendKeys(shopper.getAddress().getCity());
		findElement(By.id("v-shipping-create_stateProvinceCode")).click();
		findElement(By.id("v-shipping-create_stateProvinceCode")).sendKeys(shopper.getAddress().getState());
		findElement(By.id("v-shipping-create_postal_code")).click();
		findElement(By.id("v-shipping-create_postal_code")).sendKeys(shopper.getAddress().getZipCode());
		findElement(By.id("v-shipping-create_phone")).click();
		findElement(By.id("v-shipping-create_phone")).sendKeys(shopper.getPhoneNumber());
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		findElement(By.xpath(".//*[@id='v-shipping-create']/div[2]/button")).click();
	}
	
	private void addVisaCheckoutCard(CreditCard creditCard)
	{
		this.waitForElementToBeClickable(By.xpath(".//*[@id='v-add-payment-method']/div[3]/div/button"), true, 20);
		findElement(By.id("accountNumber")).sendKeys(creditCard.getNumber());
		findElement(By.id("expirationDate")).click();
		findElement(By.id("expirationDate")).sendKeys("0120");
		findElement(By.id("cvv")).click();
		findElement(By.id("cvv")).sendKeys("999");
		findElement(By.xpath(".//*[@id='v-add-payment-method']/div[3]/div/button")).click();
		
	}
	
	private void createAccountVisa(Shopper shopper)
	{
		this.waitForElementToBeVisible(By.cssSelector(".v-s-buttons > button:nth-child(1)"), 10);
		this.waitForElementToBeClickable(By.cssSelector(".v-s-buttons > button:nth-child(1)"), true, 20);
		this.waitForElementToBeVisible(By.xpath(".//*[@id='vuser_password']"), 15);
		findElement(By.xpath(".//*[@id='vuser_password']")).click();
		findElement(By.xpath(".//*[@id='vuser_password']")).sendKeys(shopper.getPassword());
		findElement(By.cssSelector(".v-s-buttons > button:nth-child(1)")).click();
		this.waitForElementToBeVisible(By.id("v-submit-intent"), 10);
		waitForElementToBeClickable(By.id("v-submit-intent"), true, 20);
		findElement(By.id("v-submit-intent")).click();
	}
	
	private void selectVisaCheckoutCard(CreditCard creditCard)
	{
		if (creditCard.getId().contains("VisaCheckout"))
		{
			String ShopperCC = creditCard.getNumber();
			ShopperCC = ShopperCC.substring(ShopperCC.length()-4, ShopperCC.length());
			List <WebElement> cards = driver.findElements(By.cssSelector(".v-s-last-four"));
			List <WebElement> card = findElements(By.cssSelector(".v-card-art"));
			
			for (int i =0; i < cards.size(); i++)
			{
				String ccString = cards.get(i).getText();
				String CC = ccString.substring(ccString.length()-4, ccString.length());
				if (CC.equals(ShopperCC))
				{
					card.get(i).click();
					i = card.size();
				}
				else
				{
					card.get(i+1).click();
				}
				
			}
		}
		
	}
	
	private boolean visaCheckoutOverlayExists() {
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		try {
			if (this.isElementExisting((By.xpath("//div[(@id='VmeCheckout')]/iframe")))) {
				return true;
			}
			else {
				return false;
			}
		} catch (Exception e) {
			return false;
		} finally {
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		}
	}

	private WebElement waitForLoad() {
		this.waitForjQueryAjax("Waiting for page to load");
		return this.waitForElementToBeVisible(By.xpath("//div[(@id='VmeCheckout')]/iframe"), 5);
	}
	
	private void switchFrames(){
		
			driver.switchTo().frame(driver.findElement(By.xpath("//div[(@id='VmeCheckout')]/iframe")));
	
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return visaCheckoutOverlayExists();
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		waitForLoad();
		return By.tagName("iframe");
	}

}
