package com.shop.automation.shop.selenium.pages.validations;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.Validate;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopConfirmationPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopShippingAndBillingInfoPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopCustomParameters;

public class ShopConfirmationPageValidation {

	public ShopConfirmationPageValidation(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager,
			final Map<String, String> customParameters,
			final CreditCard creditCard, final List<Shopper> shoppers) {

		ShopConfirmationPage ConfirmationPage = new ShopConfirmationPage(
				driver, translator, elementDefinitionManager, creditCard);

		ShopCustomParameters shopCustomParameters = new ShopCustomParameters(
				driver, translator, elementDefinitionManager, customParameters);

		Validate.validateEquals(
				ConfirmationPage.getVendorSubTotalConf(),
				shopCustomParameters.getCPOrderSubTotal(customParameters),
				"Confirmation Page: Validating Vendor(s) Invoice Subtotal matches Custom Parameter OrderSubTotal");

		Validate.validateEquals(
				ConfirmationPage.getVendorSalesTaxConf(customParameters),
				shopCustomParameters.getCPOrderTaxValue(customParameters),
				"Confirmation Page: Validating Vendor(s) Invoice Sales Tax total matches Custom Parameter OrderTaxValue");

		Validate.validateEquals(
				ConfirmationPage.getVendorShippingTotalConf(customParameters),
				shopCustomParameters.getCPOrderShipValue(customParameters),
				"Confirmation Page: Validating Vendor(s) Invoice Shipping total matches Custom Parameter OrderShipValue");

		Validate.validateEquals(
				ConfirmationPage.getVendorOrderTotalConf(customParameters),
				shopCustomParameters.getCPOrderTotal(customParameters),
				"Confirmation Page: Validating Vendor(s) Invoice Order total matches Custom Parameter OrderTotal");

		if(!customParameters.get("OrderCashbackEarned").equals("NULL"))
		{
			Validate.validateEquals(
			ConfirmationPage.getVendorCashbackTotalConf(customParameters),
			shopCustomParameters.getCPOrderCashbackEarned(customParameters),
			"Confirmation Page: Validating Vendor(s) Invoice Cashback total matches Custom Parameter OrderCashBackEarned");

		}
		
		Validate.validateEquals(
				CurrencyUtilities
						.convertPriceStringToBigDecimal(ConfirmationPage
								.getFinalOrderSubtotalConf()),
				shopCustomParameters.getCPOrderSubTotal(customParameters),
				"Confirmation Page: Validating Order SubTotal matches Custom Parameter OrderSubTotal");

		Validate.validateEquals(
				CurrencyUtilities
						.convertPriceStringToBigDecimal(ConfirmationPage
								.getFinalTaxTotalConf(customParameters)),
				shopCustomParameters.getCPOrderTaxValue(customParameters),
				"Confirmation Page: Validating Order Sales Tax Total matches Custom Parameter OrderTaxValue");

		Validate.validateEquals(
				CurrencyUtilities
						.convertPriceStringToBigDecimal(ConfirmationPage
								.getFinalsShippingConf(customParameters)),
				shopCustomParameters.getCPOrderShipValue(customParameters),
				"Confirmation Page: Validating Order Shipping Total matches Custom Parameter OrderShipValue");

		Validate.validateEquals(CurrencyUtilities
				.convertPriceStringToBigDecimal(ConfirmationPage
						.getFinalOrderTotalConf(customParameters)),
				shopCustomParameters.getCPOrderTotal(customParameters),
				"Confirmation Page: Validating Order Total matches Custom Parameter OrderTotal");

		if(!customParameters.get("OrderCashbackEarned").equals("NULL"))
		{
			Validate.validateEquals(
			CurrencyUtilities.convertPriceStringToBigDecimal(ConfirmationPage.getFinalOrderCashbackTotal(customParameters)),
			shopCustomParameters.getCPOrderCashbackEarned(customParameters),
			"Confirmation Page: Validating Cashback Order Total matches Custom Parameter OrderCashbackEarned");

		}
		
		if (customParameters.containsKey(String.format("TaxType")))
		{
			if (customParameters.get("TaxType").equals("VAT"))
			{
				Validate.validateEquals(
						CurrencyUtilities.convertPriceStringToBigDecimal(ConfirmationPage.getFinalVAT(customParameters)),
						shopCustomParameters.getCPVAT(customParameters),
						"Confirmation Page: Validating VAT matches Custom Parameter VAT");
			}
			
		}
		
		Validate.validateEquals(CurrencyUtilities
				.convertPriceStringToBigDecimal(ConfirmationPage
						.getFinalOrderTotalConf(customParameters)),
				ConfirmationPage.verifyFinalTotal(customParameters),
				"Confirmation Page: Final Order Total was added properly");
		
		ConfirmationPage.getOrderConfNumber();

		/*Validate.validateEquals(shoppers.get(0).getReferredConsultantName(),
				ConfirmationPage.getShopConsultantName(),
				"Validating Shopper Portal Name matches Referred Consultant Name");*/

	}
}