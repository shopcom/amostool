package com.shop.automation.shop.selenium.pages.base.common;

import java.math.BigDecimal;
import java.util.Map;

import net.sf.cglib.core.Local;

import org.openqa.selenium.WebDriver;

import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.shop.automation.shop.selenium.pages.base.ShopPageComponent;

public class ShopCustomParameters extends ShopPageComponent {

	public ShopCustomParameters(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager,
			final Map<String, String> customParameters) {
		super(driver, translator, elementDefinitionManager);
	}

	public BigDecimal getCPInvoiceVendorSubTotal(
			final Map<String, String> customParameters) {
		BigDecimal cpInvoiceVendorSubTotal = CurrencyUtilities
				.convertPriceStringToBigDecimal(customParameters
						.get("InvoiceVendorSubTotal"));
		return cpInvoiceVendorSubTotal;
	}

	public BigDecimal getCPInvoiceTaxValue(
			final Map<String, String> customParameters) {
		BigDecimal cpInvoiceTaxValue = new BigDecimal(0);
		if (!customParameters.get("InvoiceTaxValue").equals("NULL"))
		{
			cpInvoiceTaxValue = CurrencyUtilities.convertPriceStringToBigDecimal(customParameters
					.get("InvoiceTaxValue"));
		}

		return cpInvoiceTaxValue;
	}

	public BigDecimal getCPInvoiceVendorShippingTotal(
			final Map<String, String> customParameters) {
		BigDecimal cpInvoiceVendorShippingTotal = CurrencyUtilities
				.convertPriceStringToBigDecimal(customParameters
						.get("InvoiceShipValue"));
		return cpInvoiceVendorShippingTotal;
	}

	public BigDecimal getCPInvoiceVendorTotal(
			final Map<String, String> customParameters) {
		BigDecimal cpInvoiceVendorTotal = CurrencyUtilities
				.convertPriceStringToBigDecimal(customParameters
						.get("InvoiceVendorTotal"));
		return cpInvoiceVendorTotal;
	}

	public BigDecimal getCPInvoiceCashBackEarned(
			final Map<String, String> customParameters) {
		BigDecimal cpInvoiceCashBackEarned = CurrencyUtilities
				.convertPriceStringToBigDecimal(customParameters
						.get("InvoiceCashBackEarned"));
		return cpInvoiceCashBackEarned;
	}

	public BigDecimal getCPOrderSubTotal(
			final Map<String, String> customParameters) {
		BigDecimal cpOrderSubTotal = CurrencyUtilities
				.convertPriceStringToBigDecimal(customParameters
						.get("OrderSubTotal"));
		return cpOrderSubTotal;
	}

	public BigDecimal getCPOrderTaxValue(
			final Map<String, String> customParameters) {
		BigDecimal cpOrderTaxValue = new BigDecimal(0);
		if (!customParameters.get(String.format("OrderTaxValue"))
				.equals("NULL")) {
			cpOrderTaxValue = CurrencyUtilities
					.convertPriceStringToBigDecimal(customParameters
							.get("OrderTaxValue"));
		}
		return cpOrderTaxValue;
	}

	public BigDecimal getCPOrderShipValue(
			final Map<String, String> customParameters) {
		BigDecimal getOrderShipValue = CurrencyUtilities
				.convertPriceStringToBigDecimal(customParameters
						.get("OrderShipValue"));
		return getOrderShipValue;
	}

	public BigDecimal getCPOrderTotal(final Map<String, String> customParameters) {
		BigDecimal cpOrderTotal = CurrencyUtilities
				.convertPriceStringToBigDecimal(customParameters
						.get("OrderTotal"));
		return cpOrderTotal;
	}

	public BigDecimal getCPOrderCashbackEarned(
			final Map<String, String> customParameters) {
		BigDecimal cpOrderCashbackEarned = CurrencyUtilities
				.convertPriceStringToBigDecimal(customParameters
						.get("OrderCashbackEarned"));
		return cpOrderCashbackEarned;
	}
	
	public BigDecimal getCPCustomsHandling(final Map<String, String> customParameters)
	{
		BigDecimal cpCustomsHandling = CurrencyUtilities
				.convertPriceStringToBigDecimal(customParameters.get("InvoiceCustoms"));
		return cpCustomsHandling;
	}
	
	public BigDecimal getCPVAT(
			final Map<String, String> customParameters) {
		BigDecimal cpVAT = CurrencyUtilities
				.convertPriceStringToBigDecimal(customParameters
						.get("VAT"));
		return cpVAT;
	}

}
