package com.shop.automation.shop.selenium.pages.base.checkout;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopTestVariables;

public class ShopOrderSummarySection extends ShopTemplatePage {

	public ShopOrderSummarySection(final WebDriver driver, final Translator translator,
			ElementDefinitionManager elementDefinitionManager, final Map<String, String> customParameters) {
		super(driver, translator, elementDefinitionManager);

	}

	public WebElement getTaxLabelOS() {
		WebElement taxOS = findElement(By.cssSelector(".orderSummary-salesTax"));
		return taxOS;
	}

	public String getItemCountOS() {
		this.waitForjQueryAjax("Waiting for Address Info Page to load");
		return findElement(By.cssSelector("div#orderSummary-itemCount")).getText().trim();
	}

	/**
	 * Get the Order Subtotal price of the items as String
	 *
	 * @return String of Subtotal
	 */
	public BigDecimal getOrderSubtotalOS() {
		BigDecimal orderSubtotalOS = CurrencyUtilities.convertPriceStringToBigDecimal(findElement(
				By.cssSelector(".orderSummary-orderSubTotalPrice")).getText().trim());
		return orderSubtotalOS;
	}

	/**
	 * Get the cashback amount as String
	 *
	 * @return String of cashback
	 */
	public BigDecimal getCashbackOS() {
		this.waitForElementToBeVisible(By.cssSelector(".orderSummary-cashbackPrice.bold"), 10);
		BigDecimal cashBackOS = CurrencyUtilities.convertPriceStringToBigDecimal(findElement(
				By.cssSelector(".orderSummary-cashbackPrice.bold")).getText().toString().trim());
		return cashBackOS;
	}

	public boolean getCouponLabelPriceOS() {
		WebElement couponLabelOS = driver.findElement(By.xpath("//div[@class='orderSummary-vendorDiscount']"));
		WebElement couponDiscountOS = driver.findElement(By
				.xpath("//div[@class='orderSummary-orderDiscountCouponPrice']"));
		if (couponLabelOS.isDisplayed() && couponDiscountOS.isDisplayed()) {
			StepInfo.addMessage("Coupon Discount applied for first time user");
			return true;
		} else {
			StepInfo.addMessage("No Coupon Discount applied for first time user");
			return false;
		}

	}

	public BigDecimal getCouponDiscountOS() {
		BigDecimal couponDiscount = CurrencyUtilities.convertPriceStringToBigDecimal(findElement(
				By.xpath("//div[@class='orderSummary-orderDiscountCouponPrice']")).getText().trim());
		return couponDiscount;

	}

	public BigDecimal getProductPriceOS() {
		List<WebElement> getProductPriceOS = driver.findElements(By
				.xpath("//div[@class='orderSummary-prodPrice bold']"));
		BigDecimal productPriceOS = new BigDecimal(0);
		for (int i = 0; i < getProductPriceOS.size(); ++i) {
			productPriceOS = productPriceOS.add(CurrencyUtilities.convertPriceStringToBigDecimal(getProductPriceOS
					.get(i).getText().trim()));
		}
		return productPriceOS;

	}

	public BigDecimal getSalesTaxOS(final Map<String, String> customParameters) {
		BigDecimal salesTaxOS = new BigDecimal("0");

		if (!customParameters.get(String.format("InvoiceTaxValue")).equals("NULL")) {
			if (this.isElementExistingAndDisplayed(
					By.xpath("//div[not(contains(.,'IGIC'))]//div[(@class='orderSummary-salesTaxPrice')]"), 2)) {
				List<WebElement> getSalesTaxOS = driver.findElements(By
						.xpath("//div[not(contains(.,'IGIC'))]//div[(@class='orderSummary-salesTaxPrice')]"));
				for (int i = 0; i < getSalesTaxOS.size(); ++i) {
					salesTaxOS = salesTaxOS.add(CurrencyUtilities.convertPriceStringToBigDecimal(getSalesTaxOS.get(i)
							.getText().trim()));
				}
				return salesTaxOS;
			}
		}
		return salesTaxOS;
	}

	public BigDecimal getShippingOS(final Map<String, String> customParameters) {
		List<WebElement> getShippingOS = driver.findElements(By.xpath("//div[@class='orderSummary-shippingPrice']"));
		BigDecimal shippingOS = new BigDecimal(0);
		for (int i = 0; i < getShippingOS.size(); ++i) {
			shippingOS = shippingOS.add(CurrencyUtilities.convertPriceStringToBigDecimal(getShippingOS.get(i).getText()
					.trim()));
		}

		return shippingOS;
	}

	private boolean orderSummaryGiftCardPrice() {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try {
			driver.findElement(By.xpath("//div[@class='orderSummary-orderGiftCardPrice']"));
			return true;
		} catch (Exception e) {
			return false;
		} finally {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}

	}

		
	public BigDecimal getOStotals(final Map<String, String> customParameters) {
		
		BigDecimal OStotals = getProductPriceOS().add(
				getSalesTaxOS(customParameters).add(getShippingOS(customParameters)));

		OStotals = addCustomHandling(customParameters, OStotals);
		
		OStotals = subtractCouponAmount(customParameters, OStotals);
		
		OStotals = subtractCashbackAmount(customParameters, OStotals);
		
		OStotals= subtractGiftCardAmount(OStotals);
		
		OStotals = subtractFirstTimePromo(customParameters, OStotals);
		
		return OStotals;
	}

	private BigDecimal addCustomHandling(final Map<String, String> customParameters, BigDecimal OStotals){
		// Add Customs and Handling if it exists
		if (customParameters.containsKey(String.format("InvoiceCustoms"))) {
			if (!customParameters.get("InvoiceCustoms").equals("NULL")) {
				OStotals = OStotals.add(getCustomsHandlingOS());
				StepInfo.addMessage(String.format("Adding Custom and Handling: +%s", getCustomsHandlingOS()));
				return OStotals;
			}
		}
		return OStotals;
		
	}
	
	private BigDecimal subtractGiftCardAmount(BigDecimal OStotals){
		//Subtract Gift Card Applied Amount
		if (orderSummaryGiftCardPrice() == true) {
			BigDecimal gcAppliedamount = CurrencyUtilities.convertPriceStringToBigDecimal(findElement(
					By.xpath("//div[@class='orderSummary-orderGiftCardPrice']")).getText().trim());
			OStotals = OStotals.subtract(gcAppliedamount);
			StepInfo.addMessage(String.format("Applying Gift Card Amount: -%s", gcAppliedamount));
		}
				
		return OStotals;
	
	}
	
	private BigDecimal subtractCashbackAmount(final Map<String, String> customParameters, BigDecimal OStotals ){
		// Subtract Cashback Amount if Cashback is applied
		if (customParameters.get(String.format("CashBackApplied")).equals("TRUE")) {
			BigDecimal cback = getCashbackCredit();
			OStotals = OStotals.subtract(cback);
			StepInfo.addMessage(String.format("Applying CashBack Discount: -%s ", cback));
		}
		return OStotals;
	}
	
	private BigDecimal subtractCouponAmount(final Map<String, String> customParameters, BigDecimal OStotals ){
		// Subtract Coupon Discount if Coupon is applied
		if (customParameters.get(String.format("CouponApplied")).equals("TRUE")
				|| customParameters.get(String.format("CouponApplied")).equals("RE_ENTER")) {
			BigDecimal coupon = getCouponDiscountOS();
			OStotals = OStotals.subtract(coupon);
			StepInfo.addMessage(String.format("Applying Coupon Discount: -%s", coupon));
		}
		return OStotals;
	}
	
	private BigDecimal subtractFirstTimePromo(final Map<String, String> customParameters, BigDecimal OStotals){
		// Subtract First Time Promo Discount
		ShopTestVariables testVariables = new ShopTestVariables(driver, translator, elementDefinitionManager);
		if (testVariables.isFirstTimePromo(customParameters) == true) {
			OStotals = OStotals.subtract(getCouponDiscountOS());
			StepInfo.addMessage(String.format("Applying First Time Promo discount Amount: -%s", getCouponDiscountOS()));
		}
		return OStotals;
		
	}
	
	private BigDecimal getTaxTotalOS(final Map<String, String> customParameters) {
		BigDecimal taxTotalOS = new BigDecimal("0");
		if (!customParameters.get(String.format("InvoiceTaxValue")).equals("NULL")) {
			if (this.isElementExistingAndDisplayed(
					By.xpath("//div[not(contains(.,'IGIC'))]//div[(@class='orderSummary-salesTaxPrice')]"), 2)) {
				taxTotalOS = CurrencyUtilities.convertPriceStringToBigDecimal(findElement(
						By.xpath("//div[not(contains(.,'IGIC'))]//div[(@class='orderSummary-salesTaxPrice')]"))
						.getText().trim());
				return taxTotalOS;
			}
		}
		return taxTotalOS;

	}

	public BigDecimal getShippingTotalOS() {
		BigDecimal shippingTotalOS = CurrencyUtilities.convertPriceStringToBigDecimal(findElement(
				By.xpath("//div[@class='orderSummary-orderShippingTotalPrice']")).getText().trim());
		return shippingTotalOS;
	}

	public BigDecimal getOrderTotalOS() {
		BigDecimal orderTotalOS = CurrencyUtilities.convertPriceStringToBigDecimal(findElement(
				By.xpath("//div[@class='orderSummary-orderTotalPrice bold']")).getText().trim());

		return orderTotalOS;
	}

	public BigDecimal getCustomsHandlingOS() {
		BigDecimal customsHandlingOS = CurrencyUtilities.convertPriceStringToBigDecimal(findElement(
				By.xpath("//*[contains(text(),'IGIC:')]//following-sibling::div[1]")).getText().trim());

		return customsHandlingOS;

	}

	public BigDecimal getGiftCardAppliedAmount() {
		BigDecimal giftCardAppliedAmount = CurrencyUtilities.convertPriceStringToBigDecimal(findElement(
				By.xpath("//div[@class='orderSummary-orderGiftCardPrice']")).getText().trim());
		return giftCardAppliedAmount;

	}

	public BigDecimal appliedGiftCardAmount(final Map<String, String> customParameters) {
		BigDecimal finalAppliedGiftCardOrderTotal = getOStotals(customParameters).subtract(getGiftCardAppliedAmount());
		return finalAppliedGiftCardOrderTotal;
	}

	public BigDecimal appliedCouponDiscount(final Map<String, String> customParameters) {
		BigDecimal finalAppliedCouponOrderTotal = getOStotals(customParameters).subtract(getCouponCredit());
		return finalAppliedCouponOrderTotal;
	}

	public BigDecimal getCashbackCredit() {
		BigDecimal finalCashbackCreditOrderTotal = CurrencyUtilities.convertPriceStringToBigDecimal(findElement(
				By.xpath("//div[@class='orderSummary-cashbackCreditPrice']")).getText().trim());

		return finalCashbackCreditOrderTotal;
	}

	public BigDecimal getCouponCredit() {
		// VOT section
		BigDecimal CouponCredit = CurrencyUtilities.convertPriceStringToBigDecimal(findElement(
				By.xpath("//div[@class='vendorDiscountsPrice normal']")).getText().trim());
		return CouponCredit;
	}

	public BigDecimal getCbackAvailable() {
		BigDecimal giftCardAppliedAmount = CurrencyUtilities.convertPriceStringToBigDecimal(findElement(
				By.id("cashbackAvailable")).getText().trim());
		return giftCardAppliedAmount;

	}

	public BigDecimal appliedCashBackOrderTotal(final Map<String, String> customParameters) {
		BigDecimal FinalTotal = getOStotals(customParameters).subtract(getCashbackCredit());
		return FinalTotal;
	}

	public String getYouHaveCashbackOS() {
		return findElement(By.xpath("//div[@id='cashbackAvailable']")).getText().trim();
	}

	public BigDecimal verifySummaryOrderTotal(final Map<String, String> customParameters) {
		BigDecimal AddSummaryOrderTotals = getOrderSubtotalOS().add(getTaxTotalOS(customParameters)).add(
				getShippingTotalOS());

		if (customParameters.containsKey(String.format("InvoiceCustoms"))) {
			if (!customParameters.get("InvoiceCustoms").equals("NULL")) {
				AddSummaryOrderTotals = AddSummaryOrderTotals.add(getCustomsHandlingOS());
				return AddSummaryOrderTotals;
			}
		}
		return AddSummaryOrderTotals;
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return false;
	}

}
