package com.shop.automation.shop.selenium.pages.base.common.mobile;


import java.util.Locale;
import java.util.concurrent.TimeUnit;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.testdata.User;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;

public class ShopHomePageMobile extends ShopTemplatePage {
	
	private final static By shopConsultant = By.cssSelector("section#homepage.quick-search-open p");

    public ShopHomePageMobile(final WebDriver driver, final Translator translator, final ElementDefinitionManager elementDefinitionManager) {
        super(driver, translator, elementDefinitionManager);
    }

    public String getShopConsultant(final Country country, final Locale locale)
    {
    	String ShopConsultant;
    	if (country.equals(Country.HONG_KONG) || country.equals(Country.TAIWAN) )
    		{
    		if (locale.toString().contains("en"))
			{
    			ShopConsultant = findElement(shopConsultant).getText().replace("Shop Consultant: ", "");
			}
    		else
    		{
    			ShopConsultant = findElement(shopConsultant).getText().replace("購物顧問 ", "");
    		}
    		
    		}
    	else
    	{
    		ShopConsultant = findElement(shopConsultant).getText().replace("Shop Consultant: ", "");
    	}
    	return ShopConsultant;
    }
    
    

    @Override
    public boolean atPage() throws UnableToVerifyPagesExistenceException {
        return isElementExistingAndDisplayed(By.id("homepage"),10);
    }

    @Override
    protected By getBody() throws UnableToVerifyPagesExistenceException {
    	header.closeCountrySelectionPopup();
        	throw new UnableToVerifyPagesExistenceException("Unable to determine if we are on the home page");
       }

    /**
     * Checks to see if the correct user is logged in
     *
     * @param user to check to see if we are signed in
     * @return boolean indicating if the user is signed in
     */
    public <T extends User> boolean isSignedIn(final T user) {
        return header.isSignedIn(user);
    }
}
