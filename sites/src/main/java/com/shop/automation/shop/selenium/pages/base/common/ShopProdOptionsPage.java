package com.shop.automation.shop.selenium.pages.base.common;

import java.util.List;
import java.util.Map;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;


/**
 * Created by AlexisB on 5/22/2015.
 */
public class ShopProdOptionsPage extends ShopTemplatePage {


	public ShopProdOptionsPage(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

	}

	public String prodOptionType(final Map<String, String> customParameters,
			int index) {
		return customParameters.get(String.format("OptionType" + index));
	}

	public void selectDropdownOption(
			final Map<String, String> customParameters, int index) {
		
		List<WebElement> dropdowns = driver.findElements(By.cssSelector("div.drop-down-select"));
		
		List<WebElement> li = driver.findElements(By.cssSelector("li.option"));
		
		for (int i=0; i < dropdowns.size(); i++)
		{
			String option = prodOptions(customParameters, index)[i];
			String ul = dropdowns.get(i).getAttribute("data-list-name").toString();
			ul = ul.replaceAll("\\D", "");
			dropdowns.get(i).click();
			
			for (int j =0; j < li.size(); j++)
			{
				if (li.get(j).getAttribute("data-permutation-key").contains(ul))
				{
					//select option if current index of li equals option[i]
					if (li.get(j).getAttribute("data-display-value").equals(option))
					{
						li.get(j).click();
						Verify.verifyTrue(li.get(j).getAttribute("class").contains("selected"), "Verifying radio button is selected");
						StepInfo.addMessage("Selecting Product Option From Dropdown - Product Option: "
								+ li.get(j).getAttribute("data-display-value"));
					}
				}
			}
		}
		
	}

	public void selectColorSwatch(final Map<String, String> customParameters, int index) 
	{
		String swatch = customParameters.get(String.format("Option" + index));
		List<WebElement> options = driver.findElements(By.cssSelector("li.option"));
		
		for (int i = 0; i < options.size(); i++) {
			if (options.get(i).getAttribute("data-display-value")
					.contains(swatch)) {
				StepInfo.addMessage("Selecting Product Option From Color Swatch - Product Option: "
						+ options.get(i).getAttribute("data-display-value"));
				options.get(i).click();
			}
		}
	}
	
	
	public String[] prodOptions(final Map<String, String> customParameters, int index)
    {
    	String[] options;
    	String option = prodOption(customParameters, index);
    	options = option.split(",");
    	return options;
    }
	
	public String prodOption(final Map<String, String> customParameters, int index)
    {
    	return customParameters.get(String.format("Option" + index));
    }
    
    public int prodOptionCount(final Map<String, String> customParameters, int index)
    {
    	String option = prodOption(customParameters, index);
    	String[] options = option.split(",");
    	
    	return options.length;
    }

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return false;
	}
    

}
