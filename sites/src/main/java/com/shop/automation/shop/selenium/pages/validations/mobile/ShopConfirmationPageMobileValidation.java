package com.shop.automation.shop.selenium.pages.validations.mobile;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.Validate;
import com.shop.automation.shop.selenium.pages.base.checkout.mobile.ShopConfirmationPageMobile;
import com.shop.automation.shop.selenium.pages.base.common.ShopCustomParameters;
import com.shop.automation.shop.selenium.pages.base.common.ShopTestVariables;

public class ShopConfirmationPageMobileValidation {

	public ShopConfirmationPageMobileValidation(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager,
			final Map<String, String> customParameters,
			final CreditCard creditCard, final List<Shopper> shoppers) {

		ShopConfirmationPageMobile ConfirmationPage = new ShopConfirmationPageMobile(
				driver, translator, elementDefinitionManager, creditCard);

		ShopCustomParameters shopCustomParameters = new ShopCustomParameters(
				driver, translator, elementDefinitionManager, customParameters);
		ShopTestVariables testVariables = new ShopTestVariables(driver, translator, elementDefinitionManager);

		if(!customParameters.get("OrderCashbackEarned").equals("NULL"))
		{
			Validate.validateEquals(
			ConfirmationPage.getCbackUpper(),
			shopCustomParameters.getCPOrderCashbackEarned(customParameters),
			"Mobile Confirmation Page: Validating Invoice Cashback (Upper) total matches Custom Parameter OrderCashBackEarned");

		}
		
		Validate.validateEquals(ConfirmationPage
								.getOrderSubtotal(),
				shopCustomParameters.getCPOrderSubTotal(customParameters),
				"Mobile Confirmation Page: Validating Order SubTotal matches Custom Parameter OrderSubTotal");
		
		Validate.validateEquals(ConfirmationPage
								.getOrderTax(customParameters),
				shopCustomParameters.getCPOrderTaxValue(customParameters),
				"Mobile Confirmation Page: Validating Order Sales Tax Total matches Custom Parameter OrderTaxValue");
		
		Validate.validateEquals(
				ConfirmationPage
								.getOrderShipping(),
				shopCustomParameters.getCPOrderShipValue(customParameters),
				"Mobile Confirmation Page: Validating Order Shipping Total matches Custom Parameter OrderShipValue");
		
		if(testVariables.isFirstTimePromo(customParameters) == true){
			WebElement discount =  driver.findElement(By.xpath("//span[(@class='amount discount')]"));
			BigDecimal newCPOrderTotal = shopCustomParameters.getCPOrderTotal(customParameters).subtract(CurrencyUtilities
					.convertPriceStringToBigDecimal(discount.getText().toString().trim()));
			Validate.validateEquals(ConfirmationPage
					.getGrandTotal(),newCPOrderTotal,
			"Mobile Confirmation Page: Validating Order Total matches Custom Parameter OrderTotal");
		}

		if(testVariables.isFirstTimePromo(customParameters) == false){
		Validate.validateEquals(ConfirmationPage
						.getGrandTotal(),
				shopCustomParameters.getCPOrderTotal(customParameters),
				"Mobile Confirmation Page: Validating Order Total matches Custom Parameter OrderTotal");
		}
		
		Validate.validateEquals(ConfirmationPage
						.getGrandTotal(),
				ConfirmationPage.finalBalance(customParameters),
				"Mobile Confirmation Page: Final Order Total was added properly");
		
		if(!customParameters.get("OrderCashbackEarned").equals("NULL"))
		{
			Validate.validateEquals(
			ConfirmationPage.getCbackLower(),
			shopCustomParameters.getCPOrderCashbackEarned(customParameters),
			"Mobile Confirmation Page: Validating Invoice Cashback (Lower) total matches Custom Parameter OrderCashBackEarned");

		}
		

	}
}