package com.shop.automation.shop.selenium.pages.base.common.mobile;

import java.util.Map;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.testdata.address.HongKongAddress;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;


public class ShopRegPageMobile extends ShopTemplatePage {
	
	private final static By fName = By.id("bill-first");
	private final static By lName = By.id("bill-last");
	private final static By email = By.id("email");
	private final static By pwd = By.id("password");
	private final static By confirmPassword = By.id("passwordConfirm");
	private final static By addressOne = By.id("bill-address1");
	private final static By addressTwo = By.id("bill-address2");
	private final static By cty = By.id("bill-city");
	private final static By st = By.cssSelector("form.shop-form div.margin-both label fieldset.select-list.primary select");
	private final static By zip = By.id("bill-zip");
	private final static By phoneNum = By.id("bill-phone");
	private final static By referrer = By.id("referrer");
	private final static By flt = By.id("bill-address3");
	private final static By flr = By.id("bill-address4");
	private final static By build = By.id("bill-address5");
	private final static By blk = By.id("bill-address7");
	private final static By est = By.id("bill-address6");
	private final static By dist = By.id("bill-district");
	private final static By createAccount = By.id("submit-button");
	
	private final static By fNameCheckout = By.id("ship-first");
	private final static By lNameCheckout = By.id("ship-last");
	private final static By addressOneCheckout = By.id("ship-address1");
	private final static By addressTwoCheckout = By.id("ship-address2");
	private final static By ctyCheckout = By.id("ship-city");
	private final static By stCheckout = By.cssSelector("fieldset.select-list.primary select");
	private final static By zipCheckout = By.id("ship-zip");
	private final static By phoneCheckout = By.id("ship-phone");
	private final static By fltCheckout = By.id("ship-address3");
	private final static By flrCheckout = By.id("ship-address4");
	private final static By buildCheckout = By.id("ship-address5");
	private final static By blkCheckout = By.id("ship-address7");
	private final static By estCheckout = By.id("ship-address6");
	private final static By distCheckout = By.id("ship-district");

	public ShopRegPageMobile(final WebDriver driver, final Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

	}
	
	public void enterFirstName(final String firstName) {
		findElement(fName).click();
		setTextField(fName, firstName);
	}
	
	public void enterFirstNameChk(final String firstName) {
		findElement(fNameCheckout).click();
		setTextField(fNameCheckout, firstName);
	}
	
	public void enterLastName(final String lastName) {
		findElement(lName).click();
		setTextField(lName, lastName);
	}
	
	public void enterLastNameChk(final String lastName) {
		findElement(lNameCheckout).click();
		setTextField(lNameCheckout, lastName);
	}
	
	public void enterEmail(final String eMail) {
		findElement(email).click();
		setTextField(email, eMail);
	}
	
	public void enterPassword(final String password) {
		findElement(pwd).click();
		setTextField(pwd, password);
	}
	
	public void confirmPassword(final String password) {
		findElement(confirmPassword).click();
		setTextField(confirmPassword, password);
	}
	
	public void enterAddr1(final String street) {
		findElement(addressOne).click();
		setTextField(addressOne, street);
	}
	
	public void enterAddr1Chk(final String street) {
			findElement(addressOneCheckout).click();
			setTextField(addressOneCheckout, street);
	}
	
	public void enterAddr2(final String street2, final Country country) {
		if (!country.equals(Country.HONG_KONG))
		{
			findElement(addressTwo).click();
			setTextField(addressTwo, street2);
		}
		
	}
	
	public void enterAddr2Chk(final String street2, final Country country) {
		if (!country.equals(Country.HONG_KONG))
		{
			findElement(addressTwoCheckout).click();
			setTextField(addressTwoCheckout, street2);
		}
		
	}
	
	public void enterCity(final String city, final Country country) {
		if (!country.equals(Country.SINGAPORE) && !country.equals(Country.HONG_KONG))
		{
			findElement(cty).click();
			setTextField(cty, city);
		}	
	}
	
	public void enterCityChk(final String city, final Country country) {
		if (!country.equals(Country.SINGAPORE) && !country.equals(Country.HONG_KONG))
		{
			findElement(ctyCheckout).click();
			setTextField(ctyCheckout, city);
		}	
	}
	
	public void selectState(final String state, final Country country) {
		if (country.equals(Country.UNITED_STATES) || country.equals(Country.CANADA) || country.equals(Country.AUSTRALIA) || country.equals(Country.MEXICO) || country.equals(Country.SPAIN))
		{
			createDropDown(st).selectByVisibleText(state);
		}
		
	}
	
	public void selectStateChk(final String state, final Country country) {
		if (country.equals(Country.UNITED_STATES) || country.equals(Country.CANADA) || country.equals(Country.AUSTRALIA) || country.equals(Country.MEXICO) || country.equals(Country.SPAIN))
		{
			createDropDown(stCheckout).selectByVisibleText(state);
		}
		
	}
	
	public void enterZip(final String postCode, final Country country) {
		if (!country.equals(Country.HONG_KONG))
		{
			findElement(zip).click();
			setTextField(zip, postCode);
		}
	}
	
	public void enterZipChk(final String postCode, final Country country) {
		if (!country.equals(Country.HONG_KONG))
		{
			findElement(zipCheckout).click();
			setTextField(zipCheckout, postCode);
		}
	}
	
	public void enterPhone(final String phone) {
		findElement(phoneNum).click();
		setTextField(phoneNum, phone);
	}
	
	public void enterPhoneChk(final String phone) {
		findElement(phoneCheckout).click();
		setTextField(phoneCheckout, phone);
	}
	
	public void enterRefEmail(final Map<String, String> customParameters) {
		findElement(referrer).click();
		setTextField(referrer, customParameters.get(String.format("ReferralEmail")));
	}
	
	public WebElement getCreateNewAccount()
	{
		return findElement(createAccount);
	}
	
	public void enterFlat(final String flat) {
		findElement(flt).click();
		setTextField(flt, flat);
	}
	
	public void enterFlatChk(final String flat) {
		findElement(fltCheckout).click();
		setTextField(fltCheckout, flat);
	}
	
	public void enterFloor(final String floor) {
		findElement(flr).click();
		setTextField(flr, floor);
	}
	
	public void enterFloorChk(final String floor) {
		findElement(flrCheckout).click();
		setTextField(flrCheckout, floor);
	}
	
	public void enterBuilding(final String building) {
		findElement(build).click();
		setTextField(build, building);
	}
	
	public void enterBuildingChk(final String building) {
		findElement(buildCheckout).click();
		setTextField(buildCheckout, building);
	}
	
	public void enterBlock(final String block) {
		findElement(blk).click();
		setTextField(blk, block);
	}
	
	public void enterBlockChk(final String block) {
		findElement(blkCheckout).click();
		setTextField(blkCheckout, block);
	}
	
	public void enterEstate(final String estate) {
		findElement(est).click();
		setTextField(est, estate);
	}
	
	public void enterEstateChk(final String estate) {
		findElement(estCheckout).click();
		setTextField(estCheckout, estate);
	}
	
	public void selectDistrict(final String district){
		createDropDown(dist).selectByVisibleText(district);
	}
	
	public void selectDistrictChk(final String district){
		createDropDown(distCheckout).selectByVisibleText(district);
	}
	
	
	public void optionalHKGFieldsReg(final Shopper shopper)
	{
		HongKongAddress hongkongAddress = (HongKongAddress) shopper.getAddress();
		enterFlat(hongkongAddress.getFlat());
		enterFloor(hongkongAddress.getFloor());
		enterBuilding(hongkongAddress.getBuilding());
		enterBlock(hongkongAddress.getBlock());
		enterEstate(hongkongAddress.getEstate());
		selectDistrict(hongkongAddress.getDistrict());
	}
	
	public void optionalHKGFieldsChk(final Shopper shopper)
	{
		HongKongAddress hongkongAddress = (HongKongAddress) shopper.getAddress();
		enterFlatChk(hongkongAddress.getFlat());
		enterFloorChk(hongkongAddress.getFloor());
		enterBuildingChk(hongkongAddress.getBuilding());
		enterBlockChk(hongkongAddress.getBlock());
		enterEstateChk(hongkongAddress.getEstate());
		selectDistrictChk(hongkongAddress.getDistrict());
	}
	
	
	public void signUpRegMobileSite(final Shopper shopper,
			final Map<String, String> customParameters, final Country country)
	{
		
		ShopHeaderPageMobile header = new ShopHeaderPageMobile(driver, translator, elementDefinitionManager);
		waitForElementToBeClickable(header.signInMobile(), 15);
		header.signInMobile().click();
		this.waitForjQueryAjax("Waiting for page to load");
		waitForElementToBeVisible(By.xpath("//a[@class='button primary full-width']"), 15);
		header.getCreateNewAccount().click();
	
		Verify.verifyTrue(isOnAccountRegistrationPage(), "Verifying shopper is on Mobile Account Registration Page");
		enterFirstName(shopper.getFirstName());
		enterLastName(shopper.getLastName());
		enterEmail(String.format(shopper.getEmail()));
		enterPassword(shopper.getPassword());
		confirmPassword(shopper.getPassword());
		enterAddr1(shopper.getAddress().getStreet());
		enterAddr2(shopper.getAddress().getStreet2(), country);
		enterCity(shopper.getAddress().getCity(), country);
		selectState(shopper.getAddress().getState(), country);
		enterZip(shopper.getAddress().getZipCode(), country);
		enterPhone(shopper.getPhoneNumber());
		
		if (country.equals(Country.HONG_KONG))
		{
			if (shopper.getAddress() instanceof HongKongAddress)
			{
				optionalHKGFieldsReg(shopper);
			}
		}
		
		if (!customParameters.get("ReferralEmail").equals("NULL")) {
			enterRefEmail(customParameters);
		}
		
		StepInfo.addMessage(String.format(
				"New User Registered with the following %s password: %s",
				shopper.getEmail(), shopper.getPassword()));
		
		waitForElementToBeClickable(getCreateNewAccount(), 15);
		getCreateNewAccount().click();
		
	}
	
	public void signUpRegFAMOSSite(final Shopper shopper,
			final Map<String, String> customParameters, final Country country)
	{

		//SignUpRegMobileSite module should call this one, but leaving duplicate code for now
		ShopHeaderPageMobile header = new ShopHeaderPageMobile(driver, translator, elementDefinitionManager);
		waitForElementToBeClickable(header.getCreateNewAccount(), 15);
		header.getCreateNewAccount().click();
	
		Verify.verifyTrue(isOnAccountRegistrationPage(), "Verifying shopper is on Mobile Account Registration Page");

		this.waitForElementToBePresent(fName, true, 5);
		enterFirstName(shopper.getFirstName());
		enterLastName(shopper.getLastName());
		enterEmail(String.format(shopper.getEmail()));
		enterPassword(shopper.getPassword());
		confirmPassword(shopper.getPassword());
		enterAddr1(shopper.getAddress().getStreet());
		enterAddr2(shopper.getAddress().getStreet2(), country);
		enterCity(shopper.getAddress().getCity(), country);
		selectState(shopper.getAddress().getState(), country);
		enterZip(shopper.getAddress().getZipCode(), country);
		enterPhone(shopper.getPhoneNumber());
		
		if (country.equals(Country.HONG_KONG))
		{
			if (shopper.getAddress() instanceof HongKongAddress)
			{
				optionalHKGFieldsReg(shopper);
			}
		}
		
		if (!customParameters.get("ReferralEmail").equals("NULL")) {
			enterRefEmail(customParameters);
		}
		
		StepInfo.addMessage(String.format(
				"New User Registered with the following %s password: %s",
				shopper.getEmail(), shopper.getPassword()));
		
		waitForElementToBeClickable(getCreateNewAccount(), 15);
		getCreateNewAccount().click();
		
	}

	public void signUpRegRWDCheckoutSite(final Shopper shopper,
			final Map<String, String> customParameters, final Country country)
	{
		
		signUpRegFAMOSSite(shopper, customParameters, country);
		
	}

	public void signUpCheckoutMobileSite(final Shopper shopper,
			final Map<String, String> customParameters, final Country country)
	{
		ShopHeaderPageMobile header = new ShopHeaderPageMobile(driver, translator, elementDefinitionManager);
		header.getCreateNewAccount().click();
		this.waitForjQueryAjax("Waiting for page to load");
		this.waitForElementToBeVisible(email, 10);
		enterEmail(String.format(shopper.getEmail()));
		enterPassword(shopper.getPassword());
		confirmPassword(shopper.getPassword());
		
		if (!customParameters.get("ReferralEmail").equals("NULL")) {
			enterRefEmail(customParameters);
		}

		enterFirstNameChk(shopper.getFirstName());
		enterLastNameChk(shopper.getLastName());
		enterAddr1Chk(shopper.getAddress().getStreet());
		enterAddr2Chk(shopper.getAddress().getStreet2(), country);
		enterCityChk(shopper.getAddress().getCity(), country);
		selectStateChk(shopper.getAddress().getState(), country);
		enterZipChk(shopper.getAddress().getZipCode(), country);
		enterPhoneChk(shopper.getPhoneNumber());
		
		if (country.equals(Country.HONG_KONG))
		{
			if (shopper.getAddress() instanceof HongKongAddress)
			{
				optionalHKGFieldsChk(shopper);
			}
		}
		
		getCreateNewAccount().click();
	}
	

	public boolean isOnAccountRegistrationPage() {
		try {
			return atPage();
		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence of Mobile Account Registration Page");
		}
		return false;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(getBody(), 15);

	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return By.id("create-account");
	}
}
