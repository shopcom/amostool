package com.shop.automation.shop.selenium.pages.base.common;

import java.util.Locale;
import java.util.Map;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.WebDriver;

import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.shop.automation.shop.selenium.pages.base.ShopPageComponent;

/**
 * Author: Alexis Bryant Class created to set variables associated with checkout
 * tests Booleans: newUser, smoketest, VisaCheckout, productOptions
 */
public class ShopTestVariables extends ShopPageComponent {

	public ShopTestVariables(WebDriver driver, Translator translator, ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}

	public Boolean isFirstTimePromo(final Map<String, String> customParameters) {
		if (customParameters.containsKey("SourceId")) {
			if (!customParameters.get(String.format("SourceId")).equals("NULL")) {
				String sourceId = customParameters.get(String.format("SourceId"));
				if (sourceId.equals("2822") || sourceId.equals("2834") || sourceId.equals("1618")
						|| sourceId.equals("1571") || sourceId.equals("1537") || sourceId.equals("1532")) {
					return true;
				} else {
					return false;
				}
			}
		}

		return false;
	}

	public Boolean isShopperNew(final Map<String, String> customParameters) {
		if (customParameters.containsKey(String.format("CreateUser"))) {
			return true;
		}
		return false;
	}

	public Boolean isTranslatedSite(final Country country, final Locale locale) {
		if (country.equals(Country.CANADA)) {
			if (locale.toString().contains("fr")) {
				return true;
			}
		}

		if (country.equals(Country.HONG_KONG)) {
			if (locale.toString().contains("en")) {
				return true;
			}
		}

		return false;
	}

	// This is set true for scripts that we don't need validations for (ex:
	// cashback reload script, generating OSI orders)
	public Boolean isSmokeTest(final Map<String, String> customParameters) {
		if (customParameters.containsKey(String.format("Smoke"))) {
			if (customParameters.get("Smoke").equals("FALSE")) {
				return false;
			} else {
				return true;
			}

		}
		return false;
	}

	public Boolean visaCheckout(final Map<String, String> customParameters) {
		Boolean VCO = false;

		if (customParameters.containsKey(String.format("VCO"))) {
			if (customParameters.get(String.format("VCO")).equals("TRUE"))

			{
				VCO = true;
			}
		}
		return VCO;
	}

	public Boolean RWDDesktopCheckoutType(Locale locale, WebDriverUserAgent useragent) {

		if (isDesktop(useragent)) {
			switch (locale.getCountry()) {
			case "US":
			case "CA":
			case "AU":
			case "TW":
			case "HK":
			case "SG": {
				return true;
			}

			case "MX":
			case "GB":
			case "UNITED KINGDOM":
			case "ES": {
				return false;
			}

			default: {
				return false;
			}
			}
		} else
			return false;
	}

	public Boolean AmosDesktopCheckoutType(Locale locale, WebDriverUserAgent useragent) {

		if (isDesktop(useragent)) {
			return !(RWDDesktopCheckoutType(locale, useragent));
		}
		return false;

	}

	public Boolean RWDMobileCheckoutType(Locale locale, WebDriverUserAgent useragent) {

		if (isMobile(useragent)) {
			switch (locale.getCountry()) {
			case "MX":
			case "ES":
			case "UK":
			case "GB":
			case "UNITED KINGDOM": {
				return false;
			}
			default: {
				return true;
			}
			}
		} else
			return false;

	}

	public String determineCheckoutType(final Map<String, String> customParameters) {
		String checkoutType = "CARTPAGE";
		return checkoutType;
	}

	public Boolean changeProdOptions(final Map<String, String> customParameters, int index) {
		ShopProdOptionsPage prodOptions = new ShopProdOptionsPage(driver, translator, elementDefinitionManager);
		if (customParameters.containsKey("Option" + index)) {
			if (!prodOptions.prodOption(customParameters, index).equals("NULL")) {
				return true;
			} else {
				return false;
			}

		}

		return false;
	}

	public Boolean isMobile(WebDriverUserAgent useragent) {
		switch (useragent) {
		case IPHONE:
		case ANDROID: {
			return true;
		}
		default: {
			return false;
		}
		}
	}

	public Boolean isDesktop(WebDriverUserAgent useragent) {
		return !isMobile(useragent);
	}

}
