package com.shop.automation.shop.selenium.pages.base.common;

import java.util.Locale;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.shop.automation.shop.selenium.pages.base.ShopPageComponent;
import com.shop.automation.shop.selenium.pages.base.common.ShopTestVariables;

/**
 * Created by rickv on 5/4/2014.
 */
public class ShopFooter extends ShopPageComponent {

	@FindBy(xpath = "//div[@id='footer-full']/div['copyright']/p")
	@CacheLookup
	private WebElement famosCopyright;

	public ShopFooter(final WebDriver driver, final Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

		PageFactory.initElements(this.driver, this);

	}

	public void printAppServer(Locale locale) {
		this.waitForjQueryAjax("Waiting for page to load");
		String server = getNewAppServer(locale);
		StepInfo.addMessage("App Server: " + server);
		logger.info("App Server: " + server);
	}

	public String getNewAppServer(Locale locale) {
		ShopTestVariables var = new ShopTestVariables(driver, translator,
				elementDefinitionManager);
		String footerString = "";
		if (var.isDesktop(WebDriverUserAgent.getCurrentUserAgent())) {
			if (driver.getCurrentUrl().contains("checkout")
					|| driver.getCurrentUrl().contains("cart")) {
				if (var.RWDDesktopCheckoutType(locale,
						WebDriverUserAgent.getCurrentUserAgent())) {
					footerString = driver.findElement(
							By.xpath("//p[@class='copyright']")).getText();
				} else if (var.AmosDesktopCheckoutType(locale,
						WebDriverUserAgent.getCurrentUserAgent())) {
					footerString = driver.findElement(
							By.xpath("//div[@id='copyright']")).getText();
				}
			} else {
				footerString = driver.findElement(
						By.xpath("//div[(@class='copyright')]")).getText();
			}
		} else {
			footerString = driver.findElement(
					By.xpath("//p[@class='copyright']")).getText();
		}
		String AppServer = footerString
				.substring(footerString.lastIndexOf("("));
		return AppServer;
	}

	public boolean atPageComponent()
			throws UnableToVerifyPagesExistenceException {
		return famosCopyright.isDisplayed();
	}

}
