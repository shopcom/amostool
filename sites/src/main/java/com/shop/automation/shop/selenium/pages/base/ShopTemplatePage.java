package com.shop.automation.shop.selenium.pages.base;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.marketamerica.automationframework.pages.Page;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.shop.automation.shop.selenium.pages.base.common.ShopCookies;
import com.shop.automation.shop.selenium.pages.base.common.ShopFooter;
import com.shop.automation.shop.selenium.pages.base.common.ShopHeaderPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopPageTags;

public abstract class ShopTemplatePage extends Page {

	public final ShopHeaderPage header;
	public final ShopFooter footer;
	public final ShopHeaderPage headerOverlay;
	public final ShopCookies cookies;
	public final ShopPageTags pageTags;

	public ShopTemplatePage(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

		PageFactory.initElements(this.driver, this);

		header = new ShopHeaderPage(driver, translator,
				elementDefinitionManager);
		footer = new ShopFooter(driver, translator, elementDefinitionManager);
		headerOverlay = new ShopHeaderPage(driver, translator,
				elementDefinitionManager);
		cookies = new ShopCookies(driver, translator, elementDefinitionManager);
		pageTags = new ShopPageTags(driver, translator,
				elementDefinitionManager);

	}

}
