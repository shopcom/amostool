package com.shop.automation.shop.selenium.pages.base.common;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Validate;
import com.marketamerica.automationframework.tools.MarketAmericaSiteURLs;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.configuration.ShopSiteConfiguration;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;
import com.shop.automation.shop.selenium.pages.base.common.mobile.ShopCartPageMobile;

/**
 * Created by rickv on 5/7/2014.
 */
public class ShopProductPage extends ShopTemplatePage {

	// private static final By body = By.cssSelector("div#product");
	private static final By productTitleElement = By.className("product-title");
	private static final By productTitleElementMobile = By
			.cssSelector("h1.product-title");
	private static final By addToCartButton = By
			.cssSelector("input.button.primary");
	private static final By checkoutButton = By
			.cssSelector("a.primary.button.right");
	private static final By continueShopping = By.id("continueShippingId");
	private static final By addToCartButtonCC = By.id("wp_buyButton");

	public ShopProductPage(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
		try {
			Verify.verifyTrue(atPage(), "Expected to be on Product Page");
		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence of Product Page");
		}
		
		}

	public ShopProductPage(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager,
			List<Product> products, final Map<String, String> customParameters,
			final Country country, final Locale locale) {
		super(driver, translator, elementDefinitionManager);
		
		this.navigateToProductPage(products, customParameters, country, locale);

	}

	public void addToCartCustomCocktail() {
		waitForElementToBeClickable(By.id("wp_buyButton"), true, 10);
		driver.findElement(By.id("wp_buyButton")).click();

	}

	private WebElement getProductTitleElement() {
		if (WebDriverUserAgent.getCurrentUserAgent().equals(
				WebDriverUserAgent.DEFAULT)) {
			return findElement(productTitleElement);
		} else {
			return findElement(productTitleElementMobile);
		}

	}

	private WebElement getAddToCartButton() {
		return findElement(addToCartButton);
	}

	private WebElement getCheckoutButton() {
		return findElement(checkoutButton);

	}

	private WebElement getContinueShoppingLink() {
		return findElement(continueShopping);

	}

	public String waitForLoad() {
		return waitForElementToBePresent(
				By.cssSelector("input.button.primary"), true, 10).getText();
	}

	private void clickContinueShoppingLink() {
		getContinueShoppingLink().click();

	}

	public String getProductTitle() {
		return getProductTitleElement().getText();
	}

	private void addToCart(String qty) {
		StepInfo.addMessage(String.format("Adding product: %s",
				getProductTitle()));
		if (!qty.equals("NULL")) {
			changeQty(qty);
		}

		getAddToCartButton().click();

	}

	private ShopProductPage changeQty(String qty) {
		Select dropDown = this.createDropDown(By.id("quantity-select"));
		dropDown.selectByVisibleText("10+");
		setTextField(By.id("quantity"), qty);
		return this;
	}

	public ShopMiniCartPage checkout() {
		waitForElementToBePresent(By.cssSelector("a.primary.button.right"),
				true, 15);
		getCheckoutButton().click();
		return new ShopMiniCartPage(driver, translator,
				elementDefinitionManager);
	}

	public WebElement getAddCustomCocktailButton() {
		waitForElementToBeClickable(addToCartButtonCC, true, 10);
		return findElement(addToCartButtonCC);
	}

	private ShopProductPage navigateToProductPage(List<Product> products,
			final Map<String, String> customParameters, final Country country, final Locale locale) {
		ShopHeaderPage headerPage = new ShopHeaderPage(driver, translator,
				elementDefinitionManager);
		ShopTestVariables var = new ShopTestVariables(driver, translator, elementDefinitionManager);
		ShopProdOptionsPage prodOptions = new ShopProdOptionsPage(driver, translator, elementDefinitionManager);

		
		String url = MarketAmericaSiteURLs.determineURL(
				new ShopSiteConfiguration(locale),
				Environment.getCurrentEnvironment(), translator.getLocale());

		// Remove the extra backslash if it exists
		if (url.endsWith("/")) {
			url = url.substring(0, url.length() - 1);
		}

		for (int i = 0; i < products.size(); ++i) {
			
			//Change productContainerId to AltproductContainerId if testing translated site
			if (var.isTranslatedSite(country, locale) == true)
			{
				products.get(i).setProductContainerId(products.get(i).getAltproductContainerId());
			}

			if (!products.get(i).getName().equals("Custom Cocktail")) {
				driver.navigate().to(
						String.format("%s/-%s-p+.xhtml?mboxDisable=1", url, products.get(i)
								.getProductContainerId()));

				StepInfo.addMessage("Navigate to product page: "
						+ String.format("%s/-%s-p+.xhtml?mboxDisable=1", url, products.get(i)
								.getProductContainerId()));

				StepInfo.addMessage(String.format(
						"Result Product Page URL: %s", driver.getCurrentUrl()));

				try {
					Verify.verifyTrue(atPage(),
							"Expected to be on Product Page");
				} catch (UnableToVerifyPagesExistenceException e) {
					StepInfo.failTest("Unable to verify existence of Product Page");
				}

				ShopDebugModePage debugpage = new ShopDebugModePage(driver,
						translator, elementDefinitionManager);
				StepInfo.addMessage("Session ID: " + debugpage.getSessionID());

				// If product page throws 404, add message and quit driver
				if (driver.getCurrentUrl().contains(".com/404+")) {
					DateFormat dateFormat = new SimpleDateFormat(
							"yyyy/MM/dd HH:mm:ss");
					Date date = new Date();
					StepInfo.addMessage("Error Page Detected: "
							+ dateFormat.format(date));
					driver.quit();

				}

				this.waitForjQueryAjax("Wait for product page to load");

				if (var.isFirstTimePromo(customParameters) == true) {
					if (WebDriverUserAgent.getCurrentUserAgent().equals(
							WebDriverUserAgent.DEFAULT)) {
						checkPromoBanner();
					}
				}

				if (headerPage.promoContainer() == true) {
					headerPage.hoverAway();
				}

				String productQty = "NULL";
				if (customParameters.containsKey((String.format("Qty") + i))) {
					productQty = customParameters.get(String.format("Qty") + i);
				}
				if (var.changeProdOptions(customParameters, i) == true) {
					if (prodOptions.prodOptionType(customParameters, i).equals("DropDown")) 
					{
						prodOptions.selectDropdownOption(customParameters, i);

					} else if (prodOptions.prodOptionType(customParameters, i).equals(
							"ColorSwatch")) 
					{
						prodOptions.selectColorSwatch(customParameters, i);

					} else if (prodOptions.prodOptionType(customParameters, i).equals(
							"GiftCard")) {

					}
				}

				addToCart(productQty);
				if (WebDriverUserAgent.getCurrentUserAgent().equals(
						WebDriverUserAgent.DEFAULT)) {
					this.waitForElementToBeInvisible(
							By.xpath("//div[(@class= 'addthis overlay-wrapper')]"),
							20);
					waitForElementToBeClickable(getContinueShoppingLink(), 10);
					clickContinueShoppingLink();
					try {
						Verify.verifyTrue(atPage(),
								"Expected to be on Product Page");
					} catch (UnableToVerifyPagesExistenceException e) {
						StepInfo.failTest("Unable to verify existence of Product Page");
					}

				} else {

					ShopCartPageMobile cartPage = new ShopCartPageMobile(
							driver, translator, elementDefinitionManager);
					try {

						Verify.verifyTrue(cartPage.atPage(),
								"Expected to be on Cart Page");
					} catch (UnableToVerifyPagesExistenceException e) {
						StepInfo.failTest("Unable to verify existence of Cart Page");
					}
				}

			}

			if (products.get(i).getName().equals("Custom Cocktail")) {
				driver.navigate().to(
						String.format("%s/Custom+Cocktail-t.xhtml", url));
				StepInfo.addMessage(String.format("Product url: %s",
						driver.getCurrentUrl()));
				getAddCustomCocktailButton().click();
			}
		}
		return this;
	}

	/*
	 * public static void main(String[] args) { Product product = new Product();
	 * product.setProductContainerId("test");
	 * Environment.setCurrent(Environment.STAGING);
	 * Locale.setDefault(Locale.UK); ShopProductPage p = new
	 * ShopProductPage(null, null, null, Arrays.asList(product)); }
	 */

	private void checkPromoBanner() {
		Validate.validateTrue(
				driver.findElement(
						By.cssSelector(".shell-wrapper > .shell-banner > .shell-text"))
						.getText().contains("10% off"),
				"Validating 10% Shell Banner is Displayed");
	}

	public static BigDecimal calculateTotalRetailCostProds(
			List<Product> products, final Map<String, String> customParameters, final Country country) {
		BigDecimal retailTotal = new BigDecimal(0);
		for (int i = 0; i < products.size(); ++i) {

			BigDecimal productQty = new BigDecimal(1);
			if (customParameters.containsKey((String.format("Qty") + i))) {
				if (!customParameters.get("Qty" + i).equals("NULL")) {
					productQty = CurrencyUtilities
							.convertPriceStringToBigDecimal(customParameters
									.get("Qty" + i));
				}
			}

			retailTotal = retailTotal.add(products.get(i).getRetailCost()
					.multiply(productQty));
		}
		
		if (country.equals(Country.SPAIN))
		{
			retailTotal = formatSpainCurrency(retailTotal);
		}
		
		return retailTotal;
	}
	
	private static BigDecimal formatSpainCurrency(BigDecimal price)
	{
		BigDecimal max = new BigDecimal("99999");
		BigDecimal divisor = new BigDecimal("100000");
		if (price.compareTo(max) > 0)
		{
			price = price.divide(divisor);
			
			return price;
		}
		return price;
	}

	public static BigDecimal calculateTotalCashBackProds(List<Product> products) {
		BigDecimal cashBackTotal = new BigDecimal(0);
		for (int i = 0; i < products.size(); ++i) {
			cashBackTotal = cashBackTotal.add(products.get(i).getCashBack());
		}
		return cashBackTotal;
	}
	    

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return this.isElementExistingAndDisplayed(By.id("main-content"), 60);
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return By.id("main-content");

	}

}
