/**
 *
 */
/**
 * @author ricardov
 *
 */
package com.shop.automation.shop.helper;

import java.util.Locale;
import java.util.Map;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.shop.automation.shop.selenium.pages.base.ShopPageComponent;

import org.openqa.selenium.WebDriver;

import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopLoginPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopCartPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopHeaderPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopMyAccountPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopPaymentMethodsPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopRegPage;

public class ShopHelperMethods extends ShopPageComponent {


    public ShopHelperMethods(final WebDriver driver, final Translator translator, ElementDefinitionManager elementDefinitionManager) {
        super(driver, translator, elementDefinitionManager);
    }

	public Shopper registerNewAccount(final Shopper shopper, final CreditCard creditCard, final ElementDefinitionManager elementDefinitionManager, 
			final Map<String, String> customParameters, final Country country, final Locale locale) {

        // Navigate to sign in page to register new account
        ShopHomePage homePage = new ShopHomePage(driver, translator, elementDefinitionManager);
        ShopLoginPage LoginPage = homePage.headerOverlay.clickSignInLink();

        ShopRegPage RegPage = LoginPage.clickCreateAccount();
        RegPage.signUpAccountAtRegPage(shopper, customParameters, country, locale);

        Shopper newShopper = shopper;

        homePage = homePage.headerOverlay.clickHomeLogo();
        // Navigate to "Edit Payment Methods" page to add credit card
        ShopMyAccountPage MyAccountPage = homePage.headerOverlay.clickMyAccountLink();
        ShopPaymentMethodsPage PaymentMethodsPage = MyAccountPage.clickEditMyPaymentMethodsLink();
        PaymentMethodsPage.addNewCreditCard(creditCard);

        // Log out of account
        homePage = PaymentMethodsPage.headerOverlay.logoutOfAccount();

        return newShopper;
    }

    public void emptyCart() {
        ShopHeaderPage header = new ShopHeaderPage(driver, translator, elementDefinitionManager);
        ShopCartPage cartPage = header.editCart();
        cartPage.emptyCart();
    }
}