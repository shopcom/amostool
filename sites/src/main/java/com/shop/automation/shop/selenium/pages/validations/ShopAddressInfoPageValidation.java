package com.shop.automation.shop.selenium.pages.validations;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Validate;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopOrderSummarySection;
import com.shop.automation.shop.selenium.pages.base.checkout.mobile.ShopAddressInfoPageMobile;
import com.shop.automation.shop.selenium.pages.base.common.ShopCustomParameters;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopProductPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopTestVariables;
import com.shop.automation.shop.selenium.pages.base.common.mobile.ShopHeaderPageMobile;

public class ShopAddressInfoPageValidation {

	public ShopAddressInfoPageValidation(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager,
			List<Product> products, final Map<String, String> customParameters, final Country country) {
		
			ShopOrderSummarySection OrderSummarySection = new ShopOrderSummarySection(
					driver, translator, elementDefinitionManager, customParameters);
			ShopTestVariables var = new ShopTestVariables(driver, translator, elementDefinitionManager);
			ShopCustomParameters shopCustomParameters = new ShopCustomParameters(
					driver, translator, elementDefinitionManager, customParameters);
			
			
			StepInfo.addMessage(String.format("Number of items in cart: %s",
					OrderSummarySection.getItemCountOS()));
			
			Boolean changedProdOptions = false; //Check if this is true/false because product prices change depending on permutations (effects validation of calculateTotalRetailCostProds)
			
			for (int i = 0; i < products.size(); ++i) 
			{
				if (var.changeProdOptions(customParameters, i) == true)
				{
					changedProdOptions = true;	
				}
				
			}
			
			if (!customParameters.get(String.format("InvoiceCashBackEarned"))
					.equals("NULL")) 
			{

				if (changedProdOptions == false)
				{
					Validate.validateEquals(OrderSummarySection.getCashbackOS(), ShopProductPage
							.calculateTotalCashBackProds(products),String.format("Expected Order Summary Cashback: %s",
					ShopProductPage.calculateTotalCashBackProds(products)));
				}
				else
				{
					Validate.validateEquals(OrderSummarySection.getCashbackOS(), shopCustomParameters.getCPInvoiceCashBackEarned(customParameters),
							String.format("Expected Order Summary Cashback: %s",
									shopCustomParameters.getCPInvoiceCashBackEarned(customParameters)));
				}
				

			}
			
			String TaxType = "NULL";
			if (customParameters.containsKey(String.format("TaxType")))
			{	
				
				TaxType = customParameters.get("TaxType");
			}	
			
			
			//Totals from products XML will not calculate to expected total since product prices include VAT, validate against totals in excel sheet
			if (TaxType.equals("IGIC"))
			{
				BigDecimal subtotal = CurrencyUtilities
						.convertPriceStringToBigDecimal(customParameters
								.get("InvoiceVendorSubTotal"));
				Validate.validateEquals(OrderSummarySection.getOrderSubtotalOS(), subtotal, 
						"Expected Order Summary Subtotal With VAT Discount: " + subtotal);
			}
			else
			{
//				if (changedProdOptions == false)
//				{
//					Validate.validateTrue(
//							OrderSummarySection
//									.getOrderSubtotalOS()
//									.compareTo(
//											ShopProductPage
//													.calculateTotalRetailCostProds(products, customParameters, country)) == 0,
//							String.format("Expected Order Summary Subtotal: %s",
//									ShopProductPage.calculateTotalRetailCostProds(products, customParameters, country)));
//				}
//				else
//				{
					Validate.validateTrue(
							OrderSummarySection
									.getOrderSubtotalOS()
									.compareTo(
											shopCustomParameters.getCPInvoiceVendorSubTotal(customParameters)) == 0,
							String.format("Expected Order Summary Subtotal: %s",
									shopCustomParameters.getCPInvoiceVendorSubTotal(customParameters)));
//				}
				
			}

		}


}