package com.shop.automation.shop.selenium.pages.base;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import org.openqa.selenium.WebDriver;

import com.marketamerica.automationframework.pages.PageComponent;
import com.marketamerica.automation.utilities.helpers.Translator;

/**
 * Created by javierv on 5/5/2014.
 */
public class ShopPageComponent extends PageComponent {

    public ShopPageComponent(final WebDriver driver, final Translator translator, ElementDefinitionManager elementDefinitionManager) {
        super(driver, translator, elementDefinitionManager);
    }
}
