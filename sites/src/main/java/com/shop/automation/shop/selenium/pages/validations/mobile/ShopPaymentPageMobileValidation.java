package com.shop.automation.shop.selenium.pages.validations.mobile;

import java.util.Map;


import org.openqa.selenium.WebDriver;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.Validate;
import com.shop.automation.shop.selenium.pages.base.checkout.mobile.ShopPaymentPageMobile;
import com.shop.automation.shop.selenium.pages.base.common.ShopCustomParameters;

public class ShopPaymentPageMobileValidation {

	public ShopPaymentPageMobileValidation(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager,
			final Map<String, String> customParameters) {
		
		ShopPaymentPageMobile paymentPage = new ShopPaymentPageMobile(driver, translator, elementDefinitionManager);
		
		ShopCustomParameters shopCustomParameters = new ShopCustomParameters(
				driver, translator, elementDefinitionManager, customParameters);
		
		if (!customParameters.get(String.format("GiftCardApplied")).equals("TRUE"))
		{
			Validate.validateEquals(
					paymentPage.getUpperTotal(),
					shopCustomParameters
							.getCPOrderTotal(customParameters),
					"Mobile Payment Page: Validating Upper Total matches custom parameter OrderTotal");
		}
		else
		{
			Validate.validateEquals(
					paymentPage.getUpperTotal(),
					shopCustomParameters
							.getCPInvoiceVendorTotal(customParameters),
					"Mobile Payment Page: Validating 'Gift Card Applied' Upper Total matches custom parameter InvoiceVendorTotal");
		}
		
		
		Validate.validateEquals(paymentPage.getOrderSubtotal(),
				shopCustomParameters.getCPOrderSubTotal(customParameters),
				"Mobile Payment Page: Validating Order Subtotal matches Custom Parameter OrderSubTotal");
		
		Validate.validateEquals(
				paymentPage.getOrderTax(customParameters),
				shopCustomParameters.getCPOrderTaxValue(customParameters),
				"Mobile Payment Page: Validating Order Sales Tax Total matches Custom Parameter OrderTaxValue");

		Validate.validateEquals(paymentPage.getOrderShipping(),
				shopCustomParameters.getCPOrderShipValue(customParameters),
				"Mobile Payment Page: Validating Order Shipping Total matches Custom Parameter OrderShipValue");

		if (!customParameters.get(String.format("GiftCardApplied")).equals("TRUE"))
		{
			Validate.validateEquals(paymentPage.getFinalTotal(),
					shopCustomParameters.getCPOrderTotal(customParameters),
					"Mobile Payment Page: Validating Order Total matches Custom Parameter OrderTotal");
			
			Validate.validateEquals(paymentPage.addTotals(customParameters),
					shopCustomParameters.getCPOrderTotal(customParameters),
					"Mobile Payment Page: Validating Totals were added Correctly");
		}
		else
		{
			Validate.validateEquals(paymentPage.getFinalTotal(),
					shopCustomParameters.getCPInvoiceVendorTotal(customParameters),
					"Mobile Payment Page: Validating 'Gift Card Applied' Order Total matches Custom Parameter InvoiceVendorTotal");
			
			Validate.validateEquals(paymentPage.addTotals(customParameters),
					shopCustomParameters.getCPInvoiceVendorTotal(customParameters),
					"Mobile Payment Page: Validating 'Gift Card Applied' Totals were added Correctly");
		}
		
		
		//If GiftCard was applied, Remove before placing the order
		if (customParameters.get(String.format("GiftCardApplied")).equals("TRUE"))
		{
			paymentPage.removeGC();
		}
		
	}	
}
