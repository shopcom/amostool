package com.shop.automation.shop.selenium.pages.validations.mobile;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Validate;
import com.shop.automation.shop.selenium.pages.base.common.ShopCustomParameters;
import com.shop.automation.shop.selenium.pages.base.common.ShopHeaderPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopProdOptionsPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopTestVariables;
import com.shop.automation.shop.selenium.pages.base.common.mobile.ShopCartPageMobile;

public class ShopCartPageMobileValidation {

	public ShopCartPageMobileValidation(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager,
			final Map<String, String> customParameters, List<Product> products, final Country country) {
		
		
		ShopCustomParameters shopCustomParameters = new ShopCustomParameters(
				driver, translator, elementDefinitionManager, customParameters);
		
		ShopHeaderPage headerPage = new ShopHeaderPage(driver, translator, elementDefinitionManager); 
		
		ShopCartPageMobile cartpage = new ShopCartPageMobile(driver, translator, elementDefinitionManager);
		ShopTestVariables var = new ShopTestVariables(driver, translator, elementDefinitionManager);
		ShopProdOptionsPage prodOptions = new ShopProdOptionsPage(driver, translator, elementDefinitionManager);
		Boolean changedProdOptions = false; //Check if this is true/false because product prices change depending on permutations (effects validation of calculateTotalRetailCostProds)
		
		
		if (var.isDesktop(WebDriverUserAgent.getCurrentUserAgent())){
			StepInfo.addMessage(String.format("Amount of items in mini cart: %s", headerPage.cartItems()));
			}
		
		
		for (int i = 0; i < products.size(); ++i) 
		{
			if (var.changeProdOptions(customParameters, i) == true)
			{
				//System.out.println("Cart Page Validation, customparameters: " + var.prodOption(customParameters, i));
				//System.out.println("Cart Page Validation, cartPage Element Text: " + cartpage.getProdOptions().get(i));
				for (int j=0; j < prodOptions.prodOptionCount(customParameters, i); j++)
				{
					Validate.validateContains(cartpage.getProdOptions().get(j), prodOptions.prodOptions(customParameters, i)[j], "Mobile Cart Page: Validating Product Options");
				}
				
				changedProdOptions = true;		
			}
		}
		
//		if (changedProdOptions == false)
//		{
//			Validate.validateTrue(
//					cartpage.addStoreTotals()
//							.compareTo(
//									ShopProductPage
//											.calculateTotalRetailCostProds(products, customParameters, country)) == 0,
//					String.format("Expected Subtotal: %s",
//							ShopProductPage.calculateTotalRetailCostProds(products, customParameters, country)));	
//		}
		
		
		if (var.isFirstTimePromo(customParameters) == true)
		{
			Validate.validateEquals(
					cartpage.getUpperSubtotal(),
					cartpage.addStoreTotals(),
					"Mobile Cart Page: Validating Upper Subtotal matches Calculated Subtotal");
		}
		else
		{
			Validate.validateEquals(
					cartpage.getUpperSubtotal(),
					shopCustomParameters
							.getCPInvoiceVendorSubTotal(customParameters),
					"Mobile Cart Page: Validating Upper Subtotal matches custom parameter InvoiceVendorSubTotal");
		}

	}
	
}
