package com.shop.automation.shop.selenium.pages.base.common.mobile;

import java.util.List;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopCartPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;

public class ShopRegSuccessPageMobile extends ShopTemplatePage {
	
	private final static By shopConsultantMessage = By.cssSelector("div.empty-cart-message p.gray");
	
	public ShopRegSuccessPageMobile(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager, final Country country) {
		super(driver, translator, elementDefinitionManager);

		try {
			Verify.verifyTrue(atPage(),
					"Expected to be on Mobile Welcome page");

		} catch (UnableToVerifyPagesExistenceException e) {
			StepInfo.failTest("Unable to verify existence Mobile Welcome Page");
		}
		
		StepInfo.addMessage("Shop Consultant: " + getShopConsultantName());
		
		/*ShopHomePage homePage = new ShopHomePage(driver, translator,
				elementDefinitionManager);
		ShopCartPage cartPage = homePage.header.navigateToCartPage();
		cartPage.clearCart();*/
	}
	
	public String getShopConsultantName()
	{
		List<WebElement> elements = findElements(shopConsultantMessage);
		return elements.get(elements.size()-1).getText();
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(getBody());
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return By.cssSelector("div#create-account.my-account.page-content div.empty-cart-message h3.green");
	}
}