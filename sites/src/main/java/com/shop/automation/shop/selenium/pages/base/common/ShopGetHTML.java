package com.shop.automation.shop.selenium.pages.base.common;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.lang.model.util.Elements;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.gargoylesoftware.htmlunit.html.HTMLParser;
import com.gargoylesoftware.htmlunit.html.HtmlMeta;
import com.gargoylesoftware.htmlunit.javascript.host.dom.Document;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;

//Author Rick Vera

public class ShopGetHTML extends ShopTemplatePage {

	public ShopGetHTML(final WebDriver driver, final Translator translator,
			final ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}

	public String getHtmlData() throws Exception {
		// Set URL
		URL url = new URL(driver.getCurrentUrl());
		URLConnection spoof = url.openConnection();
		StringBuffer htmlString = new StringBuffer();

		// Spoof the connection so we look like a web browser
		spoof.setRequestProperty("User-Agent",
				"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:39.0) Gecko/20100101 Firefox/39.0");
		BufferedReader bufferReader = new BufferedReader(new InputStreamReader(spoof.getInputStream()));
		String strLine = "";

		// Loop through every line in the source
		while ((strLine = bufferReader.readLine()) != null) {
			htmlString.append(strLine);
			// Prints each line to the console
			//System.out.println(strLine);
			return strLine;
		}
		return "";
	}

	public String getVarDataObject() throws Exception {
		
		String data = getHtmlData();
		Pattern p = Pattern.compile("<a href='(.*?)'>");
		Matcher m = p.matcher(data.toString());
		while(m.find()) {
		   System.out.println(m);
		   //System.out.println(m.group(1));
		}
		
		/*for (int i = 0; i < hrefData.length; ++i) {
			if (hrefData[i].contains("http://")) {
				logger.debug(hrefData[i].substring(i));
				String dataString = hrefData[i].substring(i);
				System.out.println(dataString);
				 return dataString;
			}
		}*/
		
		return "";
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return false;
	}

}