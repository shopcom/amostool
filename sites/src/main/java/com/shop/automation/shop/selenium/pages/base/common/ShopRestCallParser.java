package com.shop.automation.shop.selenium.pages.base.common;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;

//@author Rick Vera

public class ShopRestCallParser extends ShopTemplatePage {

	public ShopRestCallParser(final WebDriver driver,
			final Translator translator,
			final ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}

	private final static Logger logger = LogManager.getLogger(ShopRestCallParser.class);
	private ShopDebugModePage debugModePage = new ShopDebugModePage(driver, translator,
			elementDefinitionManager);

	public String getRestCallData() {

		String line;
		StringBuffer jsonString = new StringBuffer();

		try {

			URL url = new URL(debugModePage.getRestCallURL());

			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();

			connection.setDoOutput(true);
			connection.setRequestMethod("GET");
			connection.setRequestProperty("AMOS-debug", "true");
			connection.setRequestProperty("Accept", "application/json");
			connection.setRequestProperty("Content-Type",
					"application/json; charset=UTF-8");

			BufferedReader bufferReader = new BufferedReader(
					new InputStreamReader(connection.getInputStream()));

			while ((line = bufferReader.readLine()) != null) {

				jsonString.append(line);
			}

			bufferReader.close();
			connection.disconnect();
		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());
		}
		return jsonString.toString();
	}

	public String getSearchURL(){

		String origData = getRestCallData();
		String[] restData = origData.split("Search URL:");
		for (int i = 0; i < restData.length; ++i) {
			if (restData[i].contains("http://StagingSearchVIP.corp")) {
				logger.debug(restData[i].substring(i).split("\"")[0].replaceAll("&amp;", "&"));
				 String stagingSearchURL = restData[i].substring(i).split("\"")[0].replaceAll("&amp;", "&");
				 return stagingSearchURL;
			}
			else if (restData[i].contains(" http://twnstagingsearchvip.corp")){
				logger.debug(restData[i].substring(i).split("\"")[0].replaceAll("&amp;", "&"));
				 String twnStagingSearchURL = restData[i].substring(i).split("\"")[0].replaceAll("&amp;", "&");
				 return twnStagingSearchURL;
			}
			
			else if (restData[i].contains("http://twnsearchvip.corp")){
				logger.debug(restData[i].substring(i).split("\"")[0].replaceAll("&amp;", "&"));
				 String twnLiveSearchURL = restData[i].substring(i).split("\"")[0].replaceAll("&amp;", "&");
				 return twnLiveSearchURL;
			}
			
			else if (restData[i].contains("http://LiveSearchVIP.corp") || restData[i].contains("http://UKSearchVIP.corp")) {
				logger.debug(restData[i].substring(i).split("\"")[0].replaceAll("&amp;", "&"));
				 String liveSearchURL = restData[i].substring(i).split("\"")[0].replaceAll("&amp;", "&");
				 return liveSearchURL;
			}

		}
		return "";
	}


	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		// TODO Auto-generated method stub
		return false;
	}

}