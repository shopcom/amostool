package com.shop.automation.shop.selenium.pages.base.common.shopadvisor;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.shop.automation.shop.selenium.pages.base.ShopPageComponent;

public class ShopAdvisorRoomMenuComponent extends ShopPageComponent {

	@FindBy(xpath = ".//*[@id='refine-search']/nav/div[@class='icon-nav']/ul/li[@class='tab finish']/a")
	@CacheLookup
	private WebElement completeAdvisor;

	public ShopAdvisorRoomMenuComponent(WebDriver driver,
			Translator translator,
			final ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

		PageFactory.initElements(this.driver, this);

	}

	public ShopAdvisorSummaryPage completeShopAdvisorList() {

		completeAdvisor.click();
		return new ShopAdvisorSummaryPage(driver, translator,
				elementDefinitionManager);
	}

}
