package com.shop.automation.shop.selenium.pages.base.checkout;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.shop.automation.shop.selenium.pages.base.ShopPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopHeaderPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopTestVariables;
import com.shop.automation.shop.selenium.pages.base.common.mobile.ShopCartPageMobile;
import com.shop.automation.shop.selenium.pages.base.common.mobile.ShopHeaderPageMobile;
import com.shop.automation.shop.selenium.pages.validations.ShopCartPageValidation;
import com.shop.automation.shop.selenium.pages.validations.mobile.ShopCartPageMobileValidation;

/**
 * This class represents the Motives Cosmetics Checkout Page Created by javierv
 * on 5/4/2014.
 */
public class ShopCheckoutPage extends ShopPage {

	/**
	 * Define element definitions below. Element definitions must be flexible
	 * (robust) They must be named appropriately. Additionally they MUST be
	 * marked static and final
	 */

	private static final By bodyElement = By.id("checkout");
	private static final By checkoutAsGuestButton = By
			.cssSelector("fieldset[class='register'] a");
	private static final By emailTextField = By
			.cssSelector("input[name='userName']");
	private static final By passwordTextField = By
			.cssSelector("input[name='password']");
	private static final By signInButton = By
			.cssSelector("input[name='submit']");
	private static final By continueWithoutSignInBtn = By
			.id("continueWithoutSignIn");
	private static final By proceedToCheckout = By
			.xpath("//a[contains(@class,'button primary') and contains(@href,'javascript:void(0);')]");
	private static final By visaCheckout = By.cssSelector(".visa_out.v-button");

	public ShopCheckoutPage(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager,
			final Country country) {
		super(driver, translator, elementDefinitionManager);

		ShopHeaderPage headerPage = new ShopHeaderPage(driver, translator,
				elementDefinitionManager);
		if (headerPage.promoContainer() == true) {
			headerPage.hoverAway();
		}
	}

	public ShopAddressInfoPage continueCheckoutAsGuest(final Country country) {
		return new ShopAddressInfoPage(driver, translator,
				elementDefinitionManager, country);
	}

	private WebElement getEmailTextField() {
		return findElement(emailTextField);
	}

	private WebElement getPasswordTextField() {
		return findElement(passwordTextField);
	}

	private WebElement getSignInButton() {
		return findElement(signInButton);
	}

	private WebElement getContinueWithoutSigningIn() {
		return findElement(continueWithoutSignInBtn);
	}

	public WebElement getCheckoutButton() {
		return findElement(proceedToCheckout);
	}

	public WebElement getVisaCheckoutButton() {
		return findElement(visaCheckout);
	}

	public void enterCheckout(final Map<String, String> customParameters,
			final CreditCard creditCard, final Shopper shopper,
			List<Product> products, final Country country, final Locale locale) {
		ShopTestVariables var = new ShopTestVariables(driver, translator,
				elementDefinitionManager);

		if (var.AmosDesktopCheckoutType(locale,
				WebDriverUserAgent.getCurrentUserAgent())) {
			enterCheckoutFullSite(customParameters, creditCard, shopper,
					country);
		} else {
			enterCheckoutMobileSite(customParameters, products, creditCard,
					shopper, country);

		}

	}

	private void enterCheckoutFullSite(
			final Map<String, String> customParameters,
			final CreditCard creditCard, final Shopper shopper,
			final Country country) {
		ShopTestVariables var = new ShopTestVariables(driver, translator,
				elementDefinitionManager);
		ShopHeaderPage headerPage = new ShopHeaderPage(driver, translator,
				elementDefinitionManager);
		String checkoutType = var.determineCheckoutType(customParameters);

		if (checkoutType.equals("MINICART")) {
			if (var.visaCheckout(customParameters).booleanValue() == true) {
				headerPage.selectVisaCheckout();

				ShopVisaCheckoutPage visaCheckout = new ShopVisaCheckoutPage(
						driver, translator, elementDefinitionManager);
				if (var.isShopperNew(customParameters) == false) {
					visaCheckout.signInVisaCheckout(shopper, creditCard);
				}

				else if (var.isShopperNew(customParameters) == true) {
					visaCheckout.registerVisaCheckout(shopper, creditCard);
				}

				waitForElementToBeVisible(By.id("shopLogoCont"), 20);
			} else {
				headerPage.checkout();
			}
		}

		else if (checkoutType.equals("CARTPAGE")) {
			headerPage.navigateToCartPage();
			new ShopCartPageValidation(driver, translator,
					elementDefinitionManager, customParameters, country);

			if (var.visaCheckout(customParameters) == true) {
				getVisaCheckoutButton().click();
				ShopVisaCheckoutPage visaCheckout = new ShopVisaCheckoutPage(
						driver, translator, elementDefinitionManager);
				if (var.isShopperNew(customParameters) == false) {
					visaCheckout.signInVisaCheckout(shopper, creditCard);
				}

				else if (var.isShopperNew(customParameters) == true) {
					visaCheckout.registerVisaCheckout(shopper, creditCard);
				}
			} else {
				waitForElementToBeClickable(proceedToCheckout,true,5);
				getCheckoutButton().click();
			}
		}
	}

	public void enterRWDCheckoutFullSite(
			final Map<String, String> customParameters, List<Product> products,
			final CreditCard creditCard, final Shopper shopper,
			final Country country) {
		ShopTestVariables var = new ShopTestVariables(driver, translator,
				elementDefinitionManager);
		ShopHeaderPage headerPage = new ShopHeaderPage(driver, translator,
				elementDefinitionManager);

		headerPage.navigateToCartPage();

		if (var.isSmokeTest(customParameters) == false) {
			new ShopCartPageMobileValidation(driver, translator,
					elementDefinitionManager, customParameters, products,
					country);

		}

		ShopCartPageMobile cartpage = new ShopCartPageMobile(driver,
				translator, elementDefinitionManager);

		if (var.visaCheckout(customParameters).booleanValue() == true) {
			cartpage.getVCOButton().click();

			ShopVisaCheckoutPage visaCheckout = new ShopVisaCheckoutPage(
					driver, translator, elementDefinitionManager);
			if (var.isShopperNew(customParameters) == false) {
				visaCheckout.signInVisaCheckout(shopper, creditCard);
			}

			else if (var.isShopperNew(customParameters) == true) {
				visaCheckout.registerVisaCheckout(shopper, creditCard);
			}

			waitForElementToBeInvisible(By.id("page-spinner"), 10);
		} else {
			cartpage.getCheckoutButton().click();
		}

	}

	private void enterCheckoutMobileSite(
			final Map<String, String> customParameters, List<Product> products,
			final CreditCard creditCard, final Shopper shopper,
			final Country country) {
		ShopHeaderPageMobile headerpage = new ShopHeaderPageMobile(driver,
				translator, elementDefinitionManager);
		ShopTestVariables var = new ShopTestVariables(driver, translator,
				elementDefinitionManager);

		headerpage.checkout();

		if (var.isSmokeTest(customParameters) == false) {
			new ShopCartPageMobileValidation(driver, translator,
					elementDefinitionManager, customParameters, products,
					country);

		}

		ShopCartPageMobile cartpage = new ShopCartPageMobile(driver,
				translator, elementDefinitionManager);

		if (var.visaCheckout(customParameters).booleanValue() == true) {
			cartpage.getVCOButton().click();

			ShopVisaCheckoutPage visaCheckout = new ShopVisaCheckoutPage(
					driver, translator, elementDefinitionManager);
			if (var.isShopperNew(customParameters) == false) {
				visaCheckout.signInVisaCheckout(shopper, creditCard);
			}

			else if (var.isShopperNew(customParameters) == true) {
				visaCheckout.registerVisaCheckout(shopper, creditCard);
			}

			waitForElementToBeInvisible(By.id("page-spinner"), 10);
		} else {
			cartpage.getCheckoutButton().click();
		}

	}

	public void continueBtn() {
		getContinueWithoutSigningIn().sendKeys(Keys.ENTER);
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		throw new UnableToVerifyPagesExistenceException();
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(getBody());
	}

	/**
	 * Sign in as a user
	 *
	 * @param shopper
	 *            a user to sign in as.
	 */
	public ShopAddressInfoPage signIn(final Shopper shopper,
			final Country country) {
		getEmailTextField().sendKeys(shopper.getEmail());
		getPasswordTextField().sendKeys(shopper.getPassword());
		getSignInButton().click();
		return new ShopAddressInfoPage(driver, translator,
				elementDefinitionManager, country);

	}
}
