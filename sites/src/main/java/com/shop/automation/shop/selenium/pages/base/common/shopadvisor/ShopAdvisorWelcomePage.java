package com.shop.automation.shop.selenium.pages.base.common.shopadvisor;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.shop.automation.shop.selenium.pages.base.ShopTemplatePage;

public class ShopAdvisorWelcomePage extends ShopTemplatePage {

	@FindBy(xpath = "//input[@class='button primary']")
	@CacheLookup
	private WebElement continueButton;

	@FindBy(xpath = "//*[@id='sas-list-button']")
	@CacheLookup
	private WebElement listDropDown;

	// Eventually will be List<WebElement>
	@FindBy(xpath = "//li[@id='ui-id-2']")
	@CacheLookup
	private WebElement existingList;

	String pageUrl = "/shoppingadvisor/welcome";

	public ShopAdvisorWelcomePage(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

	}

	public ShopAdvisorUsagePage startNewAdvisorList() {

		continueButton.click();
		return new ShopAdvisorUsagePage(driver, translator,
				elementDefinitionManager);
	}

	public ShopAdvisorUsagePage editExistingAdvisorList() {

		listDropDown.click();
		logger.info(existingList.getText());
		existingList.click();
		continueButton.click();
		return new ShopAdvisorUsagePage(driver, translator,
				elementDefinitionManager);
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return (this.driver.getCurrentUrl().contains(pageUrl) && this.footer
				.atPageComponent());
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		return null;
	}

}
