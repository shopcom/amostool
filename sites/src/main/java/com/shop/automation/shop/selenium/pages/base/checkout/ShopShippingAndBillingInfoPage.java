package com.shop.automation.shop.selenium.pages.base.checkout;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.testdata.Store;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.shop.selenium.pages.base.ShopPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopPaymentMethodsPage;

public class ShopShippingAndBillingInfoPage extends ShopPage {

	private final static By shippingDropdown = By.xpath("//div[@class='cartItemTemplate' and contains(@style, 'display:block')]//select[@name='shipping']");
	
	public ShopShippingAndBillingInfoPage(final WebDriver driver,
			final Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

			try {
				Verify.verifyTrue(atPage(),
						"Expected to be on Shipping & Billing Info Page");
			} catch (UnableToVerifyPagesExistenceException e) {
				StepInfo.failTest("Unable to verify existence of Shipping & Billing Info page");
    	}
	}

	/** Click the "Continue Shopping" link */
	public void clickContinueShoppingLink() {
		findElement(By.cssSelector("div.continueShopping > a")).click();
	}

	public void getShippingOptions(Map<String, String> customParameters,
			List<Product> products, final Country country) {

		Map<Store, List<Product>> productsByStore = Product
				.getProductsByStore(products);

		Set<Store> stores = productsByStore.keySet();

		for (Store store : stores) {

			String shippingSelection = customParameters.get(String.format(
					"%s::shippingSelection", store.getName()));

			System.out.println(shippingSelection);

			changeShipping(customParameters, country);

		}
	}

	public ShopShippingAndBillingInfoPage changeShipping(final Map<String, String> customParameters, final Country country) {

		if (!customParameters.get("ShippingType").equals("NULL")) {
			
			//Taiwan does not have a shipping dropdown
			if (!country.equals(Country.TAIWAN))
			{

			this.waitForElementToBePresent(shippingDropdown, true, 10);
			
			String shipValue = customParameters.get(String.format("ShippingType"));
			List<WebElement> options = createDropDown(shippingDropdown).getOptions();
			for (int i =0; i < options.size(); i++)
			{
				if (options.get(i).getText().contains(shipValue))
				{
					createDropDown(shippingDropdown).selectByIndex(i);
				}
			}

			}
		}
		
		return this;
	}

	public void addBags(final Map<String, String> customParameters, final Country country)
	{
		if (customParameters.containsKey(String.format("Bags")))
		{
			if (!customParameters.get(String.format("Bags")).equals("NULL"))
			{
				enterBags(customParameters);
			}
		}

	}

	public void enterBags(Map<String, String> customParameters) {
		setTextField(By.xpath("/html/body/div[1]/center/table/tbody/tr/td/div/div/div[2]/div[3]/div[3]/div[2]/div[5]/div/div[1]/div[2]/label/input[1]"),
				customParameters.get(String.format("Bags")));
	}

	public void continueCheckoutButton() {

		findElement(By.id("continueShippingBilling")).click();

	}

	public void cashBackApplied(final Map<String, String> customParameters) {

		if (customParameters.get(String.format("CashBackApplied")).equals(
				"TRUE")) {
			applyCashback();
		}

	}

	public void couponApplied(final Map<String, String> customParameters) {
		
		if (customParameters.containsKey(String.format("CouponApplied")))
		{
			if (customParameters.get(String.format("CouponApplied")).equals("TRUE")) {
				applyCoupon(customParameters);
			}

			if (customParameters.get(String.format("CouponApplied")).equals(
					"ADD_REMOVE")) {
				applyCoupon(customParameters);
				removeCoupon();
			}

			if (customParameters.get(String.format("CouponApplied"))
					.equals("ERROR")) {
				applyCouponError(customParameters);

			}

			if (customParameters.get(String.format("CouponApplied")).equals(
					"RE_ENTER")) {
				applyCoupon(customParameters);
				applyCoupon(customParameters);
				this.waitForElementToBeVisible(By.id("couponCodeErrorContainer"),
						10);
				String errorMessage = findElement(By.id("couponCodeErrorMessage"))
						.getText();
				StepInfo.addMessage(String.format("%s", errorMessage));
				waitForElementToBeClickable(By.id("couponCode"));
				setTextField(By.id("couponCode"), String.format(""));

			}
		}

		

	}

	public void giftCardApplied(final Map<String, String> customParameters) {

		if (customParameters.get(String.format("GiftCardApplied")).equals(
				"TRUE")) {
			applyGiftCard(customParameters);
		}
		if(customParameters.get(String.format("GiftCardApplied")).equals(
				"ERROR"))
		{
			GiftCardError(customParameters);

		}

	}

	public void removeGiftCardApplied() {
		/* String giftCardApplied = customParameters.get("GiftCardApplied");
		if (giftCardApplied.equals(
				"TRUE"))
		{*/
			WebElement RemoveGC = driver.findElement(By.xpath(".//*[@id='giftCardTemplateWrapper']/div[2]/div[3]/div[2]/a"));
			RemoveGC.click();
			waitForElementToBeInvisible(By.xpath(".//*[@id='giftCardCodeSuccessContainer']"),5);

		//}

	}

	public List<WebElement> getSelectionOfShippingTypes() {
		return driver
				.findElements(By
						.xpath("//div[@class='cartItemTemplate' and contains(@style, 'display:block')]//select[@name='shipping']"));
	}

	public void applyCashback() {
		this.waitForElementToBePresent(
				By.cssSelector("#applyCashbackBalanceToOrder"), true, 10);
		WebElement cashback_checkbox = findElement(By
				.cssSelector("#applyCashbackBalanceToOrder"));

		String cashback_available = findElement(By.id("cashbackAvailable"))
				.getText().trim();

		if(!cashback_checkbox.isSelected()){
		cashback_checkbox.click();
		}

		StepInfo.addMessage(String.format("Cashback Applied: %s",
				cashback_available));

	}

	public void applyCouponError(Map<String, String> customParameters) {
		setTextField(By.id("couponCode"),
				customParameters.get(String.format("CouponCode")));
		findElement(By.id("applyCouponCode")).click();
		this.waitForElementToBeVisible(By.id("couponCodeErrorContainer"), 10);
		String errorMessage = findElement(By.id("couponCodeErrorMessage"))
				.getText();

		StepInfo.addMessage(String.format("%s", errorMessage));

		waitForElementToBeClickable(By.id("couponCode"));
		setTextField(By.id("couponCode"), String.format(""));

	}

	public void applyCoupon(Map<String, String> customParameters) {
		setTextField(By.id("couponCode"),
				customParameters.get(String.format("CouponCode")));
		findElement(By.id("applyCouponCode")).click();
		this.waitForElementToBeVisible(By.id("couponCodeSuccessContainer"), 10);
		this.waitForElementToBeVisible(
				By.xpath("//div[@class='orderSummary-orderDiscountCouponPrice']"),
				10);
		String appliedAmount = findElement(
				By.xpath("//div[@class='orderSummary-orderDiscountCouponPrice']"))
				.getText();

		StepInfo.addMessage(String.format("Coupon Discount Applied: %s",
				appliedAmount));

	}

	public boolean isApplyCouponMessagePresent() {
		return isElementExisting(By
				.xpath("//div[@class='orderSummary-orderDiscountCouponPrice']"));
	}

	public void removeCoupon() {
		waitForElementToBeClickable(
				By.cssSelector(".promoCodeItemContainer > .promoPriceRemoveWrapper > .promoItemRemove > .removePromoItem"),
				true, 5);
		WebElement remove = driver
				.findElement(By
						.cssSelector(".promoCodeItemContainer > .promoPriceRemoveWrapper > .promoItemRemove > .removePromoItem"));
		remove.click();
		waitForElementToBeInvisible(
				By.xpath("//div[@class='orderSummary-orderDiscountCouponPrice']'"),
				5);
		waitForElementToBeInvisible(
				By.xpath(".//*[@id='couponTemplateWrapper']/div[2]/div[3]/div[1]"),
				5);
	}

	public BigDecimal getGiftCardAppliedAmountSuccessContainer(){
		BigDecimal giftCardAppliedAmount = CurrencyUtilities
				.convertPriceStringToBigDecimal(findElement(
						By.xpath(".//*[@id='giftCardTemplateWrapper']/div[2]/div[3]/div[1]"))
						.getText().trim());
		return giftCardAppliedAmount;

	}

	public BigDecimal getCouponAppliedAmountSuccessContainer()
	{
		BigDecimal CouponAppliedAmount = CurrencyUtilities
				.convertPriceStringToBigDecimal(findElement(
						By.xpath(".//*[@id='couponTemplateWrapper']/div[2]/div[3]/div[1]"))
						.getText().trim());

		return CouponAppliedAmount;
	}

	public void applyGiftCard(Map<String, String> customParameters) {
		setTextField(By.id("giftCardCode"),
				customParameters.get(String.format("GiftCardNumber")));
		setTextField(By.id("promoCodePIN"),
				customParameters.get(String.format("GiftCardPin")));
		findElement(By.id("applyGiftCardCode")).click();
		this.waitForElementToBeVisible(By.id("giftCardCodeSuccessContainer"),
				10);

		String giftCardAmountApplied = findElement(
				By.xpath("//div[@class='orderSummary-orderGiftCardPrice']"))
				.getText();

		StepInfo.addMessage(String.format("Gift Card Amount Applied: %s",
				giftCardAmountApplied));
	}

	public void GiftCardError(Map<String, String> customParameters)
	{
		setTextField(By.id("giftCardCode"),
				customParameters.get(String.format("GiftCardNumber")));
		setTextField(By.id("promoCodePIN"),
				customParameters.get(String.format("GiftCardPin")));
		findElement(By.id("applyGiftCardCode")).click();
		this.waitForElementToBeVisible(By.id("giftCardCodeErrorContainer"),
				10);
		this.waitForElementToBeInvisible(By.id("giftCardCodeErrorContainer"),
				15);
	}

	/**
	 * Enter credit card number
	 *
	 * @param ccNumber
	 *            String of credit card number being entered
	 */
	public void enterCreditCardNumber(final String ccNumber) {
		// Wait for credit card form to appear
		waitForElementToBeClickable(By.id("ccNo"), true, 30);

		// Enter Credit card number
		setTextField(By.id("ccNo"), ccNumber);
	}

	/**
	 * Select expiration month of credit card
	 *
	 * @param expirationMonth
	 *            month credit card expires
	 */
	public void selectExpirationMonth(final int expirationMonth) {
		// Format month to be 2 digits (example: "1" becomes "01")
		// to fit proper formatting of month for expiration date
		createDropDown(By.id("ccExpMonth")).selectByValue(
				String.format("%02d", expirationMonth));
	}

	/**
	 * Select expiration year of credit card
	 *
	 * @param expirationYear
	 *            year credit card expires
	 */
	public void selectExpirationYear(final int expirationYear) {
		// int converted to String to
		// be accepted as argument for
		// selectByValue
		createDropDown(By.id("ccExpYr")).selectByValue(
				String.valueOf(expirationYear));
	}

	/**
	 * Click the "Save Credit Card" Button
	 */
	public void clickSaveCreditCardButton() {
		findElement(By.id("saveCard")).click();
	}

	/**
	 * Enter the CVV of the currently selected credit card
	 *
	 * @param cvv
	 *            String of the CVV being entered
	 */

	public ShopShippingAndBillingInfoPage PaymentMethodUsed(final List<Shopper> shoppers,
			final CreditCard creditCard, final Country country, final Map<String, String> customParameters) {

		ShopPaymentMethodsPage PaymentMethodsPage = new ShopPaymentMethodsPage(
				driver, translator, elementDefinitionManager);
		
		if (customParameters.containsKey(String.format("CreateUser")))
		{
			ShopPaymentMethodsPage paymentMethod = new ShopPaymentMethodsPage(driver, translator, elementDefinitionManager);
			paymentMethod.PaymentMethodUsedNewUser(shoppers, creditCard, country, customParameters);
			
		}
		else
		{
			if (creditCard.getId().equals("AmexCC")) {
				PaymentMethodsPage.setCreditCardName(shoppers.get(0));
				PaymentMethodsPage.addNewCreditCardAmex(creditCard);
				enterCreditCardAmexCVV(creditCard
						.getCvv(), country);
			}

			if (creditCard.getId().equals("NewVisaCC5005") || creditCard.getId().equals("NewVisaCC4111")) {
				PaymentMethodsPage.setCreditCardName(shoppers.get(0));
				PaymentMethodsPage.addNewCreditCard(creditCard);
				enterCreditCardCVV(creditCard.getCvv(), country);
			}
			
			if (creditCard.getId().equals("NewMC5892")) {
				PaymentMethodsPage.setCreditCardName(shoppers.get(0));
				PaymentMethodsPage.addNewMasterCard(creditCard);
				enterCreditCardCVV(creditCard.getCvv(), country);
			}


			if (creditCard.getId().equals("VisaCC5005") || creditCard.getId().equals("VisaCC4111")) {
				enterCreditCardCVV(creditCard.getCvv(), country);
			}

			if (creditCard.getId().equals("Oxxo"))
			{
				//Select OXXO radio Button
				waitForElementToBePresent(By.xpath(".//*[@id='walletItemGlobalContainer']/div[2]/div[1]/div[2]/div"),
						true, 15);

				WebElement Radio = driver.findElement(By.xpath(".//*[@id='walletItemGlobalContainer']/div[2]/div[1]/div[1]"));
				Radio.click();
			}

			if (creditCard.getId().equals("Banorte"))
			{
				//Select Banorte Radio Button
				waitForElementToBePresent(By.xpath(".//*[@id='walletItemGlobalContainer']/div[3]/div[1]/div[2]/div"),
						true, 15);

				WebElement Radio = driver.findElement(By.xpath(".//*[@id='walletItemGlobalContainer']/div[3]/div[1]/div[1]"));
				Radio.click();
			}

			if (creditCard.getId().contains("VisaCheckout"))
			{
				waitForElementToBePresent(By.xpath(".//*[@id='walletItemGlobalContainer']/div[2]/div[1]/div[1]"),
						true, 15);
			}
		}

		return new ShopShippingAndBillingInfoPage (driver, translator, elementDefinitionManager);
	}

	public ShopShippingAndBillingInfoPage enterCreditCardCVV(final int cvv, final Country country) {

		String countryVal = "0";
		switch (country)
		{
        case SINGAPORE:
        	countryVal = "3";
        	break;
        case MEXICO:
        	countryVal = "4";
        	break;
            default:
            countryVal = "2";
		}

		this.waitForjQueryAjax("Waiting for page to load");
		waitForElementToBeClickable(
			By.xpath(".//*[@id='walletItemGlobalContainer']/div["+countryVal+"]/div[1]/div[2]/div"),
			true, 20);

		Actions action = new Actions(driver);
		WebElement CC = driver.findElement(By.xpath(".//*[@id='walletItemGlobalContainer']/div["+countryVal+"]/div[1]/div[1]"));
		action.click(CC).perform();

		waitForElementToBePresent(By.xpath("//input[@maxlength='3']"), true, 15);
		setTextField(By.xpath("//input[@maxlength='3']"), String.valueOf(cvv));
		return this;

	}

	public ShopShippingAndBillingInfoPage enterCreditCardAmexCVV(final int cvv, final Country country) {

		if (country.equals(Country.SINGAPORE)) {
			waitForElementToBePresent(
					By.xpath(".//*[@id='walletItemGlobalContainer']/div[3]/div[1]/div[2]/div"),
					true, 15);

			WebElement CC = driver
					.findElement(By
							.xpath(".//*[@id='walletItemGlobalContainer']/div[3]/div[1]/div[1]"));
			CC.click();
		}

		waitForElementToBePresent(By.xpath("//input[@maxlength='4']"), true, 15);
		setTextField(By.xpath("//input[@maxlength='4']"), String.valueOf(cvv));
		return this;

	}


	/**
	 * Click the "Continue" button to proceed with checkout
	 *
	 * @return GlobalShopPlaceOrderPage
	 */

	/**
	 * Get the WebElements for saved credit cards
	 *
	 * @return List<WebElement> for all credit cards on page
	 */
	private List<WebElement> getListOfCreditCards() {
		return driver.findElements(By.className("cardEntryBlock"));
	}

	/**
	 * @return number of credit cards
	 */
	public int numberOfCreditCards() {
		return getListOfCreditCards().size();
	}

	/**
	 * Click the "Add New Credit Card" radio button
	 */
	public void clickAddNewCreditCardRadioButton() {
		getElementsRadioButton(findElement(By.className("newCardControl")))
				.click();
		Verify.verifyTrue(
				getElementsRadioButton(
						findElement(By.className("newCardControl")))
						.isSelected(),
				"Expected new credit card radio button to be selected");
	}

	/**
	 * Click the selected credit card's radio button
	 *
	 * (Top credit card is 1, next one down is 2, etc)
	 *
	 * @param selectedCard
	 *            int of selected card
	 */
	public void selectCreditCard(final int selectedCard) {
		getElementsRadioButton(getListOfCreditCards().get(selectedCard - 1))
				.click();
	}

	// TODO: WRITE THIS JAVADOC COMMENT A LOT BETTER
	/**
	 * Test if credit card is selected or not
	 *
	 * @param choice
	 *            Card being inquired about
	 */
	public boolean isCreditCardSelected(final int choice) {
		return getElementsRadioButton(getListOfCreditCards().get(choice - 1))
				.isSelected();
	}

	/**
	 * Edit the credit card information of chosen card
	 *
	 * @param choice
	 *            which card to be edited
	 * @param newMonth
	 *            the month to be changed to
	 * @param newYear
	 *            year to be changed to
	 */
	public void editCreditCard(final int choice, final int newMonth,
			final int newYear) {

		// Click the credit card's radio button to assure it is selected
		getElementsRadioButton(getListOfCreditCards().get(choice - 1)).click();

		// Click "Edit" link
		getListOfCreditCards().get(choice - 1)
				.findElement(By.cssSelector("a.editCard")).click();

		findElement(By.id("saveCCEdit")).click();
	}

	/**
	 * Remove the selected credit card
	 *
	 * @param choice
	 *            Credit Card to be removed
	 */
	public void removeCreditCard(final int choice) {
		getListOfCreditCards().get(choice - 1)
				.findElement(By.cssSelector("span a")).click();
	}

	/**
	 * Enter credit card information to pay for order
	 *
	 * @param creditCard
	 *            CreditCard used in payment of order
	 */

	/**
	 * Enter new credit card on Payment Information Page
	 *
	 * @param creditCard
	 *            Credit Card being added to account
	 */
	public void enterNewCreditCardInfo(final CreditCard creditCard) {

		// Report credit card info being used, showing only
		// last 4 digits of credit card number
		StepInfo.addMessage(String.format("Credit Card ending %s being used",
				creditCard.getLastFourDigitsOfNumber()));

		StepInfo.addMessage(String.format("Expiration month: %02d",
				creditCard.getExpirationMonth()));

		StepInfo.addMessage(String.format("Expiration year: %s",
				creditCard.getExpirationYear()));

		enterCreditCardNumber(creditCard.getNumber());

		selectExpirationMonth(creditCard.getExpirationMonth());

		selectExpirationYear(creditCard.getExpirationYear());

		clickSaveCreditCardButton();
	}

	public void continueCheckout() {
		ShopHomePage homePage = new ShopHomePage(driver, translator,
				elementDefinitionManager);
		waitForElementToBeClickable(By.xpath("//a[@class='checkoutButton']"), true, 10);
		WebElement continueButton = findElement(By.xpath("//a[@class='checkoutButton']"));
		continueButton.click();
		homePage.header.checkForTransitionOverlay();
	}

	/**
	 * Get the radio button for the provided element
	 *
	 * @param element
	 *            WebElement to be selected via radio button
	 */
	// TODO: IS THIS SOMETHING FOR WEBCONTENT CLASS?
	private WebElement getElementsRadioButton(final WebElement element) {
		return element.findElement(By.cssSelector("input"));
	}

	// TODO: THE FOLLOWING XPATHS ARE THE ONLY WAY TO FIND THE REQUIRED ELEMENTS
	// TODO: THIS NEEDS TO BE ADDRESSED

	public String getShippingCost() {
		return findElement(By.xpath("//table[2]/tbody/tr[3]/td[2]")).getText();
	}

	public String getTotalOrderCost() {
		return findElement(By.xpath("//table[2]/tbody/tr[4]/td[2]")).getText();
	}

	public String getAvailableCashBack() {
		this.waitForjQueryAjax("Waiting for Shipping and Billing Page to load");
		String cashBack = findElement(By.cssSelector("#cashbackAvailable"))
				.getText();
		return cashBack;
	}

	public WebElement waitForLoad() {
		this.waitForjQueryAjax("Waiting for page to load");
		return waitForElementToBeVisible(By.id("walletContainer"), 15);
	}

	@Override
	public boolean atPage() throws UnableToVerifyPagesExistenceException {
		return isElementExisting(this.getBody(), 30);
	}

	@Override
	protected By getBody() throws UnableToVerifyPagesExistenceException {
		waitForLoad();
		return By.id("walletContainer");
	}

}