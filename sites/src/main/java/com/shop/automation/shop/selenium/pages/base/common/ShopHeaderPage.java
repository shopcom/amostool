package com.shop.automation.shop.selenium.pages.base.common;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.apache.commons.lang.StringEscapeUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.testdata.User;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.shop.automation.shop.selenium.pages.base.ShopPageComponent;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopCheckoutPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopLoginPage;
import com.shop.automation.shop.selenium.pages.base.common.mobile.ShopHeaderPageMobile;

/**
 * The Motives Cosmetics Header contains all functionality accessible in the
 * header
 */
public class ShopHeaderPage extends ShopPageComponent {

	private final static By catDropDown = By.name("sy");
	private final static By cartQuantity = By.cssSelector("a[title='Cart'] b");
	private final static By signInLink = By.cssSelector("div.item a");
	private final static By signInLinkTWN = By
			.xpath("//div[(@id='super-header-menu')]//div[(@class='item')]//a[contains(@href,'login')]");
	private final static By userNameField = By.id("sign_in_email_textfield");
	private final static By passwordField = By.id("sign_in_password_textfield");
	private final static By submitLoginFormButton = By.id("signin_button");
	private final static By signedInUserElement = By
			.xpath("//div[contains(@class,'ccll')]");
	private final static By signInErrorMessageElement = By.id("sign_in_error");
	private final static By currentlySelectorCountryIcon = By.id("countries");
	private final static String countryLinkXpath = "//aside[@id='countries']//li/a[contains(text(), '$')]";
	private final static By logoImg = By.id("header-logo");
	private final static By cartButton = By.cssSelector("#mini-cart-icon");
	private final static By checkoutButton = By
			.cssSelector(".tag_bottom>table>tbody>tr>td>input");
	private final static By editCartButton = By
			.cssSelector("a.button.tertiary");

	public ShopHeaderPage(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}

	private WebElement getCheckoutButton() {
		return findElement(checkoutButton);
	}

	protected WebElement getEditCartButton() {
		waitForjQueryAjax("Waiting for Edit Cart button to be visible");
		return findElement(editCartButton);
	}

	private WebElement getSignedInUserElement() {
		return waitForElementToBeClickable(signedInUserElement, false, 5);
	}

	public WebElement changeCategory() {
		return findElement(catDropDown);
	}

	protected WebElement getUserNameElement() {
		waitForjQueryAjax("Waiting for user name field to be visible");
		return waitForElementToBeClickable(userNameField);
	}

	protected WebElement getPasswordElement() {
		if (WebDriverUserAgent.getCurrentUserAgent().equals(
				WebDriverUserAgent.DEFAULT)) {
			return findElement(passwordField);
		} else {
			ShopHeaderPageMobile headerMobile = new ShopHeaderPageMobile(
					driver, translator, elementDefinitionManager);
			return headerMobile.getPasswordElement();
		}

	}

	protected WebElement getSubmitLoginFormButtonElement() {

		return findElement(submitLoginFormButton);

	}

	private WebElement getSignInErrorMessageElement() {
		return waitForElementToBeClickable(signInErrorMessageElement);
	}

	/**
	 * Returns the partialSearch box
	 *
	 * @return returns a Web element representing the partialSearch box on the
	 *         header section on the page
	 */
	private WebElement getSearchBoxElement() {
		WebElement searchBoxElement = driver
				.findElement(By.className("search"));
		return searchBoxElement;
	}

	private WebElement getCurrentlySelectorCountryIcon() {
		return findElement(currentlySelectorCountryIcon);
	}

	/**
	 * Return the cart quantity element
	 *
	 * @return returns a web element representing the cart quantity element on
	 *         the page
	 */
	private WebElement getCartQuantityElement() {
		return findElement(cartQuantity);
	}

	private WebElement getlogoImg() {
		return findElement(logoImg);
	}

	public int getCartQuantity() {
		return Integer.valueOf(getCartQuantityElement().getText());
	}

	public String getCurrentURL(final Locale locale)
			throws UnsupportedEncodingException {
		if (locale.getDisplayCountry().equals("Taiwan")) {
			String currentURL = URLDecoder(driver.getCurrentUrl());
			return currentURL;
		} else if (locale.getDisplayCountry().equals("Hong Kong")) {
			String currentURL = URLDecoder(driver.getCurrentUrl());
			return currentURL;
		} else {
			String currentURL = driver.getCurrentUrl();
			return currentURL;
		}

	}

	public String getModifiedURL() {
		String modifiedURL = driver.getCurrentUrl().replace("+", " ");
		return modifiedURL;
	}

	public List<String> escapeHtmlListOfStrings(List<String> origList)
			throws UnsupportedEncodingException {
		List<String> List = new ArrayList<String>(origList);
		List<String> newList = new ArrayList<String>();
		for (String Str : List) {
			if (Str.contains("?")) {
				String RepStr = Str.replaceAll("\\?", "");
				String encodeListStr = StringEscapeUtils.escapeHtml(RepStr);
				newList.add(encodeListStr);
			} else if (Str.contains("…")) {
				String RepStr = Str.replaceAll("\\…", "");
				String encodeListStr = StringEscapeUtils.escapeHtml(RepStr);
				newList.add(encodeListStr);
			} else if (Str.contains("…")) {
				String RepStr = Str.replaceAll("\\…", "");
				String encodeListStr = StringEscapeUtils.escapeHtml(RepStr);
				newList.add(encodeListStr);
			} else if (Str.contains("™")) {
				String RepStr = Str.replaceAll("\\™", "");
				String encodeListStr = StringEscapeUtils.escapeHtml(RepStr);
				newList.add(encodeListStr);
			} else if (Str.contains("®")) {
				String RepStr = Str.replaceAll("\\®", "");
				String encodeListStr = StringEscapeUtils.escapeHtml(RepStr);
				newList.add(encodeListStr);
			} else {
				String encodeListStr = StringEscapeUtils.escapeHtml(Str);
				newList.add(encodeListStr);
			}
		}
		return newList;
	}

	public List<String> unescapeXmlListOfStrings(List<String> origList)
			throws UnsupportedEncodingException {
		List<String> List = new ArrayList<String>(origList);
		List<String> newList = new ArrayList<String>();
		for (String Str : List) {
			if (Str.contains("?")) {
				String RepStr = Str.replaceAll("\\?", "");
				String encodeListStr = StringEscapeUtils.unescapeXml(RepStr);
				newList.add(encodeListStr);
			} else if (Str.contains("&#153;")) {
				String RepStr = Str.replaceAll("\\&#153;", "");
				String encodeListStr = StringEscapeUtils.unescapeXml(RepStr);
				newList.add(encodeListStr);
			} else if (Str.contains("&reg;")) {
				String RepStr = Str.replaceAll("\\&reg;", "");
				String encodeListStr = StringEscapeUtils.unescapeXml(RepStr);
				newList.add(encodeListStr);
			} else {
				String encodeListStr = StringEscapeUtils.unescapeXml(Str);
				newList.add(encodeListStr);
			}
		}

		return newList;
	}

	public String URLDecoder(String url) throws UnsupportedEncodingException {
		String decodedURL = java.net.URLDecoder.decode(url, "UTF-8");
		return decodedURL;
	}

	public List<String> sortList(List<String> origList) {
		List<String> sortedList;
		sortedList = new ArrayList<String>(origList);
		Collections.sort(sortedList);
		return sortedList;
	}

	void signInLink(Country country) {

		if (WebDriverUserAgent.getCurrentUserAgent().equals(
				WebDriverUserAgent.DEFAULT)) {

			if (country.equals(Country.TAIWAN)) {
				waitForElementToBeClickable(signInLinkTWN);
			}

			waitForElementToBeClickable(By.cssSelector("div.item a"));
			/*
			 * Actions action = new Actions(driver);
			 * action.doubleClick(getSignInLink(country)); action.perform();
			 */
			Actions builder = new Actions(driver);
			Actions hoverOverRegistrar = builder.moveToElement(
					getSignInLink(country)).click();
			hoverOverRegistrar.perform();

			// getSignInLink(country).sendKeys(Keys.RETURN);
		} else {
			ShopHeaderPageMobile headerMobile = new ShopHeaderPageMobile(
					driver, translator, elementDefinitionManager);
			waitForElementToBeClickable(headerMobile.signInMobile(), 10);
			headerMobile.signInMobile().click();
		}

	}

	public WebElement getSwitchLanguageLink() {
		waitForElementToBeClickable(By.xpath("//a[contains(.,'English')]"));
		WebElement switchLanguageLink = driver.findElement(By
				.xpath("//a[contains(.,'English')]"));
		return switchLanguageLink;
	}

	public void SwitchLanguage(final Map<String, String> customParameters) {

		if (customParameters.get("Language").equals("English")) {
			Actions action = new Actions(driver);
			action.doubleClick(getSwitchLanguageLink());
			action.perform();
		}

	}

	private WebElement getSignInLink(Country country) {
		if (country.equals(Country.TAIWAN)) {
			return waitForElementToBeClickable(signInLinkTWN);
		} else {
			return waitForElementToBeClickable(signInLink);
		}
	}

	private WebElement getCartButton() {
		if (WebDriverUserAgent.getCurrentUserAgent().equals(
				WebDriverUserAgent.DEFAULT)) {
			return findElement(cartButton);
		} else {
			ShopHeaderPageMobile headerMobile = new ShopHeaderPageMobile(
					driver, translator, elementDefinitionManager);
			return headerMobile.getCartButton();
		}

	}

	public void checkForTransitionOverlay() {
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		try {
			if (driver.findElement(
					By.xpath("//div[(@id= 'pageTransitionOverlay')]"))
					.isDisplayed()) {
				System.out.println("Waiting on transition overlay......");
				this.waitForElementToBeInvisible(
						By.xpath("//div[(@id= 'pageTransitionOverlay')]"), 10);
			} else {

			}
		} catch (Exception e) {

		} finally {
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		}

	}

	public String cartItems() {
		String items = driver.findElement(By.xpath("//div[(@id='mini-cart-icon')]/span")).getText().toString().trim();
		return items;
	}
	
	
	public boolean promoContainer() {
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try {
			if (driver.findElement(By.id("header-dropdown-content"))
					.isDisplayed()
					|| driver.findElement(By.className("content10711"))
							.isDisplayed()) {
				return true;
			} else {
				System.out.println("No container in the way detected");
				return false;
			}
		} catch (Exception e) {
			System.out.println("No container in the way detected");
			return false;
		} finally {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}

	}

	public void hoverAway() {
		WebElement headerLogo = driver.findElement(By.id("header-logo"));
		Actions builder = new Actions(driver);
		Actions hoverOver = builder.moveToElement(headerLogo);
		hoverOver.perform();
		this.waitForElementToBeInvisible(By.id("header-dropdown-content"), 10);
		System.out.println("Container is in the way, hovering away.....");
	}

	/**
	 * Search for a product on the partialSearch results page
	 *
	 * @return the partialSearch results page holding results that (partially)
	 *         match(ed) our partialSearch term
	 */

	private WebElement getSearchButton() {
		return findElement(By.className("search-submit"));
	}

	private void getSearchDropDownCategory(
			final Map<String, String> customParameters) {
		WebElement dropDown = driver.findElement(By
				.xpath("//button[@class= 'search-in']"));
		dropDown.click();
		WebElement selectOption = driver.findElement(By
				.xpath("//div[@class= 'search-in-container']//a[contains(., '"
						+ customParameters.get(String.format("SearchDropDown"))
						+ "')]"));
		this.waitForElementToBeVisible(By
				.xpath("//div[@class= 'search-in-container']//a[contains(., '"
						+ customParameters.get(String.format("SearchDropDown"))
						+ "')]"), 10);
		selectOption.click();
	}

	private void searchButton() {
		getSearchButton().click();
	}

	private void search(final String searchTerm) {
		getSearchBoxElement().click();
		WebElement searchBoxElement = getSearchBoxElement();
		searchBoxElement.sendKeys(searchTerm);

	}

	public ShopProductPage searchByName(Product product) {
		search(product.getName().trim());
		return new ShopProductPage(driver, translator, elementDefinitionManager);

	}

	public ShopProductPage searchByID(Product product) {
		search(product.getSku().trim());
		return new ShopProductPage(driver, translator, elementDefinitionManager);
	}

	public void searchDropDown_clickSearchBtn(
			final Map<String, String> customParameters) {
		if (!customParameters.get("SearchDropDown").equals("NULL")) {
			getSearchDropDownCategory(customParameters);
		}
		searchButton();
	}

	/**
	 * Search for a product on the partialSearch results page
	 *
	 * @return the partialSearch results page holding results that (partially)
	 *         match(ed) our partialSearch term
	 */
	public void partialSearch(final String searchTerm) {
		this.waitForjQueryAjax("Wait for page to load");
		search(searchTerm);

	}
		
	public void scrollIntoView(String path){
		WebElement element = driver.findElement(By.className(path));
		Actions actions = new Actions(driver);
		actions.moveToElement(element);
		actions.perform();
		System.out.println("Scrolling into Glboal Redirect Overlay");
	}
		
	public boolean getCloseCountrySelectionPopup() {
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		try {
			if (this.isElementDisplayed(findElement(By
					.className("global_redirects_overlay")))) {
				//scrollIntoView("global_redirects_overlay");
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			return false;
		} finally {
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		}

	}

	public void closeCountrySelectionPopup() {
		if (getCloseCountrySelectionPopup() == true) {
			WebElement window = findElement(By.xpath("//div[(@id='global-redirects')]//div[(@class='close')]"));
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].click();", window);
			waitForElementToBeInvisible(By.id("global_redirects_overlay"), 10);
			System.out.println("Closed global redirect window");
		} else {
			System.out.println("No global redirect window detected");
		}
	}

	public ShopHomePage signIn(Shopper shopper, Country country, Locale locale) {

		ShopTestVariables var = new ShopTestVariables(driver, translator,
				elementDefinitionManager);

		StepInfo.addMessage(String.format("Trying to sign in as %s", shopper));
		StepInfo.addMessage(String.format("Shopper Email: "
				+ shopper.getEmail()));

		if (var.RWDDesktopCheckoutType(locale,
				WebDriverUserAgent.getCurrentUserAgent())) {
			driver.navigate().refresh();
			this.waitForjQueryAjax("Waiting for page to load");
			closeCountrySelectionPopup();
			signInLink(country);
			ShopHeaderPageMobile headerMobile = new ShopHeaderPageMobile(
					driver, translator, elementDefinitionManager);
			this.waitForElementToBeVisible(By.cssSelector("input#email"), 5);
			headerMobile.getUserNameElement().sendKeys(shopper.getEmail());
			headerMobile.getPasswordElement().sendKeys(shopper.getPassword());
			headerMobile.getSignInElementMobile().click();
		} else if (var.AmosDesktopCheckoutType(locale,
				WebDriverUserAgent.getCurrentUserAgent())) {
			driver.navigate().refresh();
			this.waitForjQueryAjax("Waiting for page to load");
			closeCountrySelectionPopup();
			signInLink(country);
			ShopSignInPage signInPage = new ShopSignInPage(driver, translator,
					elementDefinitionManager);
			signInPage.SignIn(shopper);
			Verify.verifyTrue(homePage(country),
					"Verifying Shopper is on Home Page After Log In");
		} else {
			ShopHeaderPageMobile headerMobile = new ShopHeaderPageMobile(
					driver, translator, elementDefinitionManager);
			headerMobile.signInMobile().click();
			headerMobile.getUserNameElement().sendKeys(shopper.getEmail());
			headerMobile.getPasswordElement().sendKeys(shopper.getPassword());
			headerMobile.getSignInElementMobile().click();

			/*new ShopMyAccountPageMobile(driver, translator,
					elementDefinitionManager);*/
		}

		return new ShopHomePage(driver, translator, elementDefinitionManager);
	}

	/**
	 * Checks to see if the correct user is logged in
	 *
	 * @param user
	 *            user to check
	 * @return boolean indicating if the user is signed in
	 */
	public <T extends User> boolean isSignedIn(User user) {
		String message;
		if (user.hasLastName()) {
			message = String.format("%s!", user.getFirstName()); // ,
																	// user.getLastName());
		} else {
			message = String.format("%s!", user.getFirstName());
		}
		final WebElement signedInUserElementLocal = getSignedInUserElement();
		// If the element is null then just return false
		if (signedInUserElementLocal == null) {
			return false;
			// else check to see if it has the expected welcome message.
		} else {
			return signedInUserElementLocal.getText().contains(message);
		}
	}

	private boolean homePage(final Country country) {
		this.waitForjQueryAjax("Waiting for page to load");
		if (!findElement(
				By.xpath("//div[(@id='main-content')]//section[(@id='homepage')]"))
				.isDisplayed()) {
			return false;
		} else {
			return true;
		}

	}

	// AMOS Welcome Page is Gone
	/*
	 * public boolean WelcomePage(final Country country) {
	 * 
	 * driver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);
	 * if(!findElement(By.className("ccll")).isDisplayed()) { return false; }
	 * else { return true; }
	 * 
	 * }
	 */

	/**
	 * Check to see if the Invalid, ID, or Password error exists
	 *
	 * @return a boolean indicating if the error was found
	 */
	public boolean invalidIDEmailOrPasswordErrorExists() {
		return getSignInErrorMessageElement().getText().contains(
				translate("invalid_id_email_or_password"));
	}

	/**
	 * Check to see if the wrong country error exists
	 *
	 * @return a boolean indicating if the error was found
	 */
	public boolean wrongCountryErrorExists() {
		return getSignInErrorMessageElement().getText().contains(
				translate("wrong_country"));
	}

	/**
	 * Check to see if the no site available for your country exists
	 *
	 * @return a boolean indicating if the error was found
	 */
	public boolean noSiteAvailableForYourCountryErrorExists() {
		return getSignInErrorMessageElement().getText().contains(
				translate("no_site_available_for_your_country"));
	}

	public ShopHomePage changeCountry(final String countryName) {
		return changeCountry(countryName, translator, elementDefinitionManager);
	}

	private ShopHomePage changeCountry(final String countryName,
			Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		getCurrentlySelectorCountryIcon().click();
		getCountryLink(countryName).click();
		return new ShopHomePage(driver, translator, elementDefinitionManager);
	}

	private WebElement getCountryLink(String countryName) {
		return findElementByXpath(countryLinkXpath, countryName);
	}

	public ShopHomePage clickHomeLogo() {
		findElement(By.cssSelector("h1.logo a")).click();
		return new ShopHomePage(driver, translator, elementDefinitionManager);
	}

	public ShopHomePage clickLogo() {
		getlogoImg().click();
		return new ShopHomePage(driver, translator, elementDefinitionManager);
	}

	public ShopCheckoutPage navigateToCheckoutPage(final Country country) {
		getCartButton().click();
		getCheckoutButton().click();
		return new ShopCheckoutPage(driver, translator,
				elementDefinitionManager, country);
	}

	public ShopCartPage navigateToCartPage() {
		getCartButton().click();

		if (WebDriverUserAgent.getCurrentUserAgent().equals(
				WebDriverUserAgent.DEFAULT)) {
			waitForElementToBeVisible(
					By.xpath("//div[@class='mini-cart-secure']"), 5);
			editCart();
		}
		return new ShopCartPage(driver, translator, elementDefinitionManager);

	}

	public String getConsultantName() {

		waitForElementToBeClickable(By.cssSelector("#consultant a"), true, 5);
		String consultantName = driver
				.findElement(By.cssSelector("#consultant a")).getText().trim();

		return consultantName;

	}

	public String getShopConsultantName() {

		// Look for element containing text of shop consultant's name
		// when navigating to the portal
		// If element exists, return text
		WebElement consultantNameElement = waitForElementToBeClickable(
				By.cssSelector("#consultant a"), false, 5);
		if (consultantNameElement != null)
			return consultantNameElement.getText().trim();

		// Look for element containing text of shop consultant's name
		// after logging in with an account
		// If element exists, return text
		consultantNameElement = waitForElementToBeClickable(
				By.cssSelector("#consultant a"), false, 5);
		if (consultantNameElement != null)
			return consultantNameElement.getText().trim();

		// If neither element exists, return
		// empty String to indicate that shop
		// consultant's name does not exist
		return "";
	}

	public ShopLoginPage clickSignInLink() {
		waitForElementToBeClickable(By.className("login")).click();
		return new ShopLoginPage(driver, translator, elementDefinitionManager);
	}

	public ShopMyAccountPage clickMyAccountLink() {
		findElement(By.cssSelector("a.loggedIn.account")).click();
		return new ShopMyAccountPage(driver, translator,
				elementDefinitionManager);
	}

	public boolean isUserLoggedIn() {
		// return
		// isElementExisting(By.className("loggedIn")).getText().contains(translate("greeting"));
		return isElementExisting(By.className("loggedIn"));
	}

	public ShopHomePage logoutOfAccount() {
		if (this.isUserLoggedIn()) {
			findElement(
					By.xpath(interpolateXpath("//div/a[contains(text(), '$')]",
							translate("logout")))).click();
			return new ShopHomePage(driver, translator,
					elementDefinitionManager);
		}
		// Return null because
		// log out is not happening,
		// so no navigation to home page
		return null;
	}

	public ShopCartPage editCart() {
		findElement(By.xpath("//a[@href='/nbts/ccn%5Fcart.xhtml']")).click();
		return new ShopCartPage(driver, translator, elementDefinitionManager);
	}

	public void openMiniCart() {
		if (!this.isMiniCartVisible()) {
			this.getMiniCartDropDownArrow().click();
			this.waitForjQueryAjax("Waiting for minicart to load");
			waitForElementToBeClickable(By.id("mini-cart"), true, 15);
		}
	}

	public boolean isMiniCartVisible() {
		return findElement(By.id("mini-cart")).isDisplayed();
	}

	private WebElement getMiniCartDropDownArrow() {
		return findElement(By.cssSelector("div.pageWrapper > a.cart"));
	}

	public void checkout() {
		waitForElementToBeClickable(By.id("mini-cart-icon"), true, 20);
		findElement(By.id("mini-cart-icon")).click();
		this.waitForElementToBeVisible(
				By.xpath("//a[@href='/nbts/ccn%5Fcart.xhtml']"), 20);
		this.waitForElementToBeClickable(
				By.xpath("//a[@href='/nbts/ccn%5Fcart.xhtml']"), true, 30);
		findElement(By.xpath("//a[@href='/nbts/ccn%5Fcart.xhtml']")).click();
	}

	public void selectVisaCheckout() {
		waitForElementToBeClickable(By.id("mini-cart-icon"), true, 20);
		findElement(By.id("mini-cart-icon")).click();
		this.waitForElementToBeVisible(
				By.cssSelector(".visa-checkout-con > img.v-button"), 10);
		this.waitForElementToBeClickable(
				By.cssSelector(".visa-checkout-con > img.v-button"), true, 15);
		findElement(By.cssSelector(".visa-checkout-con > img.v-button"))
				.click();
	}

	public ShopHomePage selectNewCountry(final String countrySelection) {

		// Open the top banner
		// to make required
		// element visible
		openTopBanner();

		createDropDown(By.id("country")).selectByVisibleText(countrySelection);

		return new ShopHomePage(driver, translator, elementDefinitionManager);
	}

	/**
	 * @param languageSelection
	 *            new language being selected
	 * @return GlobalShopHomePage
	 */
	public ShopHomePage selectNewLanguage(final String languageSelection) {

		// Open the top banner
		// to make required
		// element visible
		openTopBanner();

		createDropDown(By.id("language"))
				.selectByVisibleText(languageSelection);

		return new ShopHomePage(driver, translator, elementDefinitionManager);
	}

	/**
	 * Open the Shipping Country and Language top banner if it is not already
	 * open
	 */
	private void openTopBanner() {
		// If this element is not null, the top banner is open
		final WebElement element = waitForElementToBeClickable(
				By.cssSelector(".active #localize"), false, 5);

		if (element == null) {
			findElement(By.cssSelector(".editLangs")).click();
			this.waitForjQueryAjax("Waiting for top banner to load");
		}
	}

	/**
	 * Close the Shipping Country and Language top banner if it is open
	 */
	public void closeTopBanner() {

		// If this element is existing, the top banner is open
		if (isElementExisting(By.cssSelector(".active #localize"), 5)) {
			findElement(By.className("editLangs")).click();
			this.waitForjQueryAjax("Waiting for top banner to close");
		}
	}

	/**
	 * Reset the top banner to its default values (country: United States)
	 * (language: English)
	 */
	public void resetTopBanner() {
		// Open the top banner
		// to make required
		// element visible
		openTopBanner();
		createDropDown(By.id("country")).selectByValue("USA");

		// Open the top banner
		// to make required
		// element visible
		openTopBanner();
		createDropDown(By.id("language")).selectByValue("ENG");
	}

	/**
	 * Get the name of the country currently selected in the country dropdown
	 * box in the top banner
	 *
	 * @return String of name of selected country
	 */
	public String getSelectedCountryName() {
		// Open the top banner
		// to make required
		// element visible
		this.openTopBanner();
		return createDropDown(By.id("country")).getFirstSelectedOption()
				.getText();
	}

	/**
	 * Returns the currently selected country's abbreviation.
	 *
	 * @return the abbreviation of the country currently selected
	 * */
	public String getSelectedCountryAbbreviation() {

		// Open the top banner
		// to make required
		// element visible
		openTopBanner();

		List<WebElement> options = findElements(By.tagName("option"));
		for (WebElement webElement : options) {
			if (webElement.isSelected()) {
				return webElement.getAttribute("value");
			}
		}
		// If no country selected,
		// return "N/A"
		return "N/A";
	}
}
