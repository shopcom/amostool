package com.shop.automation.shop.selenium.pages.base.common.shopadvisor;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.shop.automation.shop.selenium.pages.base.ShopPageComponent;

public class ShopAdvisorHeaderComponent extends ShopPageComponent {

	@FindBy(xpath = "//span[@class='cashback-amt']")
	@CacheLookup
	private WebElement cashbackAmt;

	public ShopAdvisorHeaderComponent(WebDriver driver, Translator translator,
			final ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

		PageFactory.initElements(this.driver, this);

	}

}
