package com.shop.automation.shop.selenium.pages.base.common.shopadvisor;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.shop.automation.shop.selenium.pages.base.ShopPageComponent;

public class ShopAdvisorSubRoomMenuComponent extends ShopPageComponent {

	public ShopAdvisorSubRoomMenuComponent(WebDriver driver,
			Translator translator,
			final ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);

		PageFactory.initElements(this.driver, this);

	}

}
