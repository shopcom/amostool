package com.shop.automation.shop.selenium.pages.base.common.mobile;

import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.shop.automation.shop.selenium.pages.base.ShopPageComponent;

public class ShopHeaderPageMobile extends ShopPageComponent {
	
	private final static By userNameFieldMobile = By.cssSelector("input#email");
	private final static By passwordFieldMobile = By.cssSelector("input#j_password");
	private final static By signInIcon = By.xpath("//div[contains(@class,'sprite icon anonymous')]");
	private final static By signInMobile = By.cssSelector("div.margin-top button#submit-button.button.primary.right");
	private final static By cartButtonMobile = By.cssSelector("a.header-icon.right div.sprite.icon.cart");
	private final static By cartCountMobile = By.cssSelector("div.count-circle");
	private final static By createNewAccount = By.xpath("//a[@class='button primary full-width']");
	
	public ShopHeaderPageMobile(WebDriver driver, Translator translator,
			ElementDefinitionManager elementDefinitionManager) {
		super(driver, translator, elementDefinitionManager);
	}
	
	public void checkout()
	{
		if (!driver.findElement(By.id("shopping-cart")).isDisplayed())
		{
			getCartButton().click();
		}
		
	}
	
	public WebElement getCartButton()
	{
		return findElement(cartButtonMobile);
	}
	
	public WebElement getCartCount()
	{
		return findElement(cartCountMobile);
	}
	
	public WebElement getUserNameElement()
	{
		return findElement(userNameFieldMobile);
	}
	
	public WebElement getPasswordElement()
	{
		return findElement(passwordFieldMobile);
	}
	
	public WebElement signInMobile()
	{
		return findElement(signInIcon);
	}

	public WebElement getSignInElementMobile()
	{
		return findElement(signInMobile);
	}
	
	public WebElement getCreateNewAccount()
	{
		return findElement(createNewAccount);
	}
	
	
	
}
