package com.shop.automation.selenium.shop.tests.shopadvisor;

import org.testng.annotations.Test;

import com.shop.automation.selenium.shop.tests.ShopBaseTests;
import com.shop.automation.shop.configuration.ShopSiteConfiguration;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopCheckoutPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopCompleteAddressPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopCompleteCheckout;
import com.shop.automation.shop.selenium.pages.base.common.ShopCartPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopDebugModePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopFamosSignInPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopFooter;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopProductPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopTestVariables;
import com.shop.automation.shop.selenium.pages.base.common.shopadvisor.ShopAdvisorSearchPage;
import com.shop.automation.shop.selenium.pages.base.common.shopadvisor.ShopAdvisorSummaryPage;
import com.shop.automation.shop.selenium.pages.base.common.shopadvisor.ShopAdvisorUsagePage;
import com.shop.automation.shop.selenium.pages.base.common.shopadvisor.ShopAdvisorWelcomePage;
import com.marketamerica.automation.annotations.TestDoc;
import com.marketamerica.automation.annotations.TestSuite;
import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automationframework.tools.MarketAmericaSiteURLs;
import com.marketamerica.automationframework.tools.datamanagement.dataproviders.ExcelDataDrivenProvider;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;

import java.util.List;
import java.util.Locale;
import java.util.Map;

@TestSuite(description = { "ShopAdvisor" })
public class ShopAdvisorTest extends ShopBaseTests {

	public ShopAdvisorTest() {
		super();
		System.out.println("ShopAdvisor has been instantiated");
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will test ShopAdvisor", ids = { "ShopAdvisor" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN,
			Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV })
	public void testShopAdvisor(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard) {

		logger.info(id);
		logger.info(description);
		logger.info(shoppers);
		logger.info(products.get(0));
		logger.info(customParameters);
		logger.info(creditCard);

		ShopFamosSignInPage signInPage;
		ShopAdvisorWelcomePage shopAdvisorWelcomePage;
		ShopAdvisorUsagePage shopAdvisorUsagePage;
		ShopAdvisorSearchPage shopAdvisorSearchPage;
		ShopAdvisorSummaryPage shopAdvisorSummaryPage;

		driver.manage().deleteAllCookies();

		String url = MarketAmericaSiteURLs.determineURL(
				new ShopSiteConfiguration(locale),
				Environment.getCurrentEnvironment(), translator.getLocale());
		driver.navigate().to(String.format("%s", url));

		ShopHomePage homePage = new ShopHomePage(driver, translator,
				elementDefinitionManager);

		homePage.cookies.getDateTimeStamp();
		homePage.checkEnvironment(locale);
		homePage.header.closeCountrySelectionPopup();
		homePage.footer.printAppServer(locale);

		logger.info("homePage Cookies");
		homePage.cookies.printDebugCookies();

		if (customParameters.get(String.format("LoggedIn")).equals("false")) {
			signInPage = homePage.shopAdvisorWelcomeNotLoggedIn(driver,
					translator, elementDefinitionManager, locale);
			// assert you are on correct page
			// assert tags, debug, footer, header (generic assertions that
			// should be in their own class)

			shopAdvisorWelcomePage = signInPage.signInFromAdvisorWelcomePage(
					shoppers.get(0).getEmail(), shoppers.get(0).getPassword());
			assert (shopAdvisorWelcomePage.atPage());
			// assert you are on correct page
			// assert tags, debug, footer, header (generic assertions that
			// should be in their own class)

		} else {

			signInPage = homePage.signInFromHomePage();
			assert (signInPage.atPage());

			homePage = signInPage.signInFromHomePage(
					shoppers.get(0).getEmail(), shoppers.get(0).getPassword());
			// assert you are on correct page
			// assert tags, debug, footer, header (generic assertions that
			// should be in their own class)
			logger.info("homePage Cookies");
			homePage.cookies.printDebugCookies();

			shopAdvisorWelcomePage = homePage.shopAdvisorWelcomeLoggedIn();
			// assert you are on correct page
			// assert tags, debug, footer, header (generic assertions that
			// should be in their own class)
			assert (shopAdvisorWelcomePage.atPage());
		}

		// On Welcome page
		// If starting a new list, else pick existing list
		// Dead code is on purpose to switch back & forth between new & existing
		// lists
		// Eventually, that will be a custom parameter in the input, just not
		// tonight

		if (false) {

			shopAdvisorUsagePage = shopAdvisorWelcomePage.startNewAdvisorList();
			assert (shopAdvisorUsagePage.atPage());
			shopAdvisorUsagePage.selectUsageCategories();
		} else {

			shopAdvisorUsagePage = shopAdvisorWelcomePage
					.editExistingAdvisorList();
			assert (shopAdvisorUsagePage.atPage());

		}

		shopAdvisorSearchPage = shopAdvisorUsagePage
				.shopAdvisorGoToSearchPage();
		assert (shopAdvisorSearchPage.atPage());

		shopAdvisorSearchPage.addProdtoList(products.get(1));

		shopAdvisorSummaryPage = shopAdvisorSearchPage.shopAdvisorRoomMenu
				.completeShopAdvisorList();
		assert (shopAdvisorSummaryPage.atPage());

		driver.manage().deleteAllCookies();

	}
}
