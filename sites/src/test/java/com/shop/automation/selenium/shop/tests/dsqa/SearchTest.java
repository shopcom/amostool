package com.shop.automation.selenium.shop.tests.dsqa;

import com.marketamerica.automation.annotations.TestDoc;
import com.marketamerica.automation.annotations.TestSuite;
import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automationframework.tools.datamanagement.dataproviders.ExcelDataDrivenProvider;
import com.marketamerica.automationframework.tools.reporting.ScreenShooter;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Verify;
import com.shop.automation.selenium.shop.tests.ShopBaseTests;
import com.shop.automation.shop.selenium.pages.base.common.ShopDebugModePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopGetHTML;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopProductPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopSearchResultsPage;
import com.shop.automation.shop.selenium.pages.validations.ShopSearchValidation;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import org.testng.annotations.Test;

//@author Rick Vera

@TestSuite(description = { "Search" })
public class SearchTest extends ShopBaseTests {

	public SearchTest() {
		super();
		System.out.println("Search Tests have been instantiated");
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will check for guided navigation", ids = { "DSQA #5" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN, Country.MEXICO, Country.AUSTRALIA,
			Country.CANADA, Country.TAIWAN, Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE })
	public void ExpandedSearchRegressionTest(final String id, final String description, final List<Product> products,
			final List<Shopper> shoppers, final Map<String, String> customParameters, final CreditCard creditCard)
			throws Exception {
		this.searchTest(id, description, products, shoppers, customParameters, creditCard);
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will check for guided navigation", ids = { "DSQA #5" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN, Country.MEXICO, Country.AUSTRALIA,
			Country.CANADA, Country.TAIWAN, Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE })
	public void DSQA5(final String id, final String description, final List<Product> products,
			final List<Shopper> shoppers, final Map<String, String> customParameters, final CreditCard creditCard)
			throws Exception {
		this.searchTest(id, description, products, shoppers, customParameters, creditCard);
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will check for guided navigation", ids = { "DSQA #5" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN, Country.MEXICO, Country.AUSTRALIA,
			Country.CANADA, Country.TAIWAN, Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE })
	public void DebugTest(final String id, final String description, final List<Product> products,
			final List<Shopper> shoppers, final Map<String, String> customParameters, final CreditCard creditCard)
			throws Exception {
		this.searchTest(id, description, products, shoppers, customParameters, creditCard);
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will check for guided navigation", ids = { "DSQA #5" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN, Country.MEXICO, Country.AUSTRALIA,
			Country.CANADA, Country.TAIWAN, Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE })
	public void searchTest(final String id, final String description, final List<Product> products,
			final List<Shopper> shoppers, final Map<String, String> customParameters, final CreditCard creditCard)
			throws Exception {

		logger.info(id);
		logger.info(description);
		logger.info(products.get(0));
		logger.info(shoppers);
		logger.info(customParameters);
		logger.info(creditCard);

		final ShopHomePage homePage = new ShopHomePage(driver, translator, elementDefinitionManager);

		ShopSearchResultsPage searchResultsPage = new ShopSearchResultsPage(driver, translator,
				elementDefinitionManager);

		ShopDebugModePage debugModePage = new ShopDebugModePage(driver, translator, elementDefinitionManager);

		ShopGetHTML shopGetHTML = new ShopGetHTML(driver, translator, elementDefinitionManager);
		
		driver.manage().deleteAllCookies();

		debugModePage.getDateTimeStamp();

		homePage.header.closeCountrySelectionPopup();
				
		//shopGetHTML.getVarDataObject(driver.getCurrentUrl());

		if ((customParameters.containsKey("Portal") && !customParameters.get("Portal").equals("NULL"))) {
			homePage.navigateToPortal(customParameters, locale);
		}

		else if ((customParameters.containsKey("SourceID") && !customParameters.get("SourceID").equals("NULL"))) {
			homePage.navigateToSourceID(customParameters, locale);
		}

		else if ((customParameters.containsKey("SignedIn") && customParameters.get("SignedIn").equals("TRUE"))) {
			homePage.header.signIn(shoppers.get(0), country, locale);
		}

		homePage.navigateToDebugPage(locale);

		debugModePage.enterDebugModeInfo();

		debugModePage.getDebugCookies();

		homePage.header.partialSearch(customParameters.get("Search"));

		homePage.header.searchDropDown_clickSearchBtn(customParameters);

		if (customParameters.containsKey("ProductPage") && customParameters.get("ProductPage").equals("TRUE")) {
			new ShopProductPage(driver, translator, elementDefinitionManager);
			StepInfo.addMessage("Test Passed navigated to Product Page");

		}

		if (customParameters.get("Search").equals("")) {
			if (searchResultsPage.getHomePageFound() == false) {
				debugModePage.exitDebugMode(locale);
				Verify.failTest("Search for blank space should of returned user to Home Page");
				StepInfo.addMessage("Test Failed did not navigate to Home Page");
			} else {
				Verify.verifyTrue(searchResultsPage.getHomePageFound(), "Navigated to Home Page");

			}
		}

		if (customParameters.get("SearchDropDown").equals("Stores")) {
			searchResultsPage.checkIfStoresFound(locale);
		}

		homePage.checkMboxisDisabledOnURL(locale);

		searchResultsPage.clickDeptLeftNav(customParameters);

		searchResultsPage.clickRefineLeftNav(customParameters);

		searchResultsPage.clickStoreLeftNav(customParameters);

		searchResultsPage.clickBrandLeftNav(customParameters);

		searchResultsPage.clickCheckboxSaleLeftNav(customParameters, locale);

		searchResultsPage.clickInStockLeftNav(customParameters, locale);

		searchResultsPage.searchWithin(customParameters);

		searchResultsPage.clickViewCountPage(customParameters);

		searchResultsPage.clickViewDetailOrGrid(customParameters);

		searchResultsPage.clickOneCartStoresTab(customParameters, locale);

		searchResultsPage.setSort(customParameters, locale);

		searchResultsPage.resultsMessageNotFound(customParameters);

		searchResultsPage.moreButton(customParameters);

		searchResultsPage.addProdQuickView(customParameters, products);

		if (customParameters.get("ExpandSearch").equals("TRUE")) {
			searchResultsPage.clickExpandSearchLink(customParameters);
		}

		// Jump To page no longer used on new search
		// searchResultsPage.setJumpTo(customParameters);

		searchResultsPage.clickNextPage(customParameters, locale);

		searchResultsPage.saveSearch(customParameters);

		homePage.checkMboxisDisabledOnURL(locale);

		if (customParameters.get("SearchDropDown").equals("Stores")) {
			searchResultsPage.checkIfStoresFound(locale);
		}

		if (searchResultsPage.enterValidationCheck(customParameters) == true) {

			System.out.println("Entering Validation process");
			new ShopSearchValidation(driver, translator, elementDefinitionManager, customParameters, locale);

		}

		else {

			debugModePage.exitDebugMode(locale);

		}

		ScreenShooter.capture(driver);

		driver.manage().deleteAllCookies();

		driver.close();

	}
}