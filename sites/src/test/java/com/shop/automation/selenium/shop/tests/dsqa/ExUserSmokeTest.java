package com.shop.automation.selenium.shop.tests.dsqa;

import org.testng.annotations.Test;

import com.shop.automation.shop.selenium.pages.validations.ShopAddressInfoPageValidation;
import com.shop.automation.shop.selenium.pages.validations.ShopPlaceOrderPageValidation;
import com.shop.automation.shop.selenium.pages.validations.ShopShippingAndBillingValidation;
import com.shop.automation.selenium.shop.tests.ShopBaseTests;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopAddressInfoPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopCheckoutPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopConfirmationPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopPlaceOrderPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopShippingAndBillingInfoPage;
import com.shop.automation.shop.selenium.pages.base.checkout.mobile.ShopAddressInfoPageMobile;
import com.shop.automation.shop.selenium.pages.base.checkout.mobile.ShopConfirmationPageMobile;
import com.shop.automation.shop.selenium.pages.base.checkout.mobile.ShopPaymentPageMobile;
import com.shop.automation.shop.selenium.pages.base.checkout.mobile.ShopReviewOrderPageMobile;
import com.shop.automation.shop.selenium.pages.base.common.ShopCartPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopDebugModePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopFooter;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopProductPage;
import com.shop.automation.shop.selenium.pages.validations.ShopConfirmationPageValidation;
import com.marketamerica.automation.annotations.TestDoc;
import com.marketamerica.automation.annotations.TestSuite;
import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automationframework.tools.datamanagement.dataproviders.ExcelDataDrivenProvider;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;

import java.util.List;
import java.util.Locale;
import java.util.Map;

@TestSuite(description = { "Cashback Reload Checkout  - The purpose of this script is to simply complete checkout without validations so that cashback is continually added to accounts in existing user tests" })
public class ExUserSmokeTest extends ShopBaseTests {

	public ExUserSmokeTest() {
		super();
		System.out.println("Cashback Reload Script Initiated");
	}
	
	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This script will run OSI test cases", ids = { "OSI Suite" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN,
			Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV})
	public void OSITests(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard) {
		this.ExUserSmoke(id, description, products, shoppers,
				customParameters, creditCard);
		
	}
	
	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This script place orders to keep cashback in testing accounts", ids = { "DSQA #8" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN,
			Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV})
	public void cBackReload(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard) {
		this.ExUserSmoke(id, description, products, shoppers,
				customParameters, creditCard);
		
	}
	
	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This script will add cashback to test accounts", ids = { "DSQA #8" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN,
			Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV})
	public void ExUserSmoke(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard) {

		logger.info(id);
		logger.info(description);
		logger.info(products.get(0));
		logger.info(shoppers);
		logger.info(customParameters);
		logger.info(creditCard);
		/**
		 * 1.) Navigate to Login Page 2.) Login as Existing user 3.) Navigate to
		 * product 4.) Add it to the cart 5.) Go to Checkout 6.) Continue Add
		 * tab 7.) Continue Ship tab 8.) Place order tab 9.) Confirmation page
		 */

		// Navigate to the home page
		ShopHomePage homePage = new ShopHomePage(driver, translator,
				elementDefinitionManager);
		
		final ShopFooter footer = new ShopFooter(driver, translator,
				elementDefinitionManager);
		
		ShopDebugModePage debugpage = new ShopDebugModePage(driver, translator, elementDefinitionManager);
		
		driver.manage().deleteAllCookies();

		homePage.checkEnvironment(locale);

		homePage.header.closeCountrySelectionPopup();
		
		footer.printAppServer(locale);

		// Sign in with a user
		homePage.header.signIn(shoppers.get(0), country, locale);
		
		debugpage.getDebugCookies();
		
		// Clear Cart
		ShopCartPage cartPage = homePage.header.navigateToCartPage();
		cartPage.clearCart();
				
		// Navigate to Product(s) and add to cart
		new ShopProductPage(driver, translator, elementDefinitionManager,
				products, customParameters, country, locale);

		// Checkout
		ShopCheckoutPage checkout = new ShopCheckoutPage(driver, translator, elementDefinitionManager, country);
		checkout.enterCheckout(customParameters, creditCard, shoppers.get(0), products, country, locale);

		ShopAddressInfoPage AddressInfoPage = new ShopAddressInfoPage(driver,
				translator, elementDefinitionManager, country);
		
		debugpage.getDebugCookies();
		
		footer.printAppServer(locale);
		
		if(WebDriverUserAgent.getCurrentUserAgent().equals(WebDriverUserAgent.DEFAULT))
    	{
			AddressInfoPage.continueCheckout();

			ShopShippingAndBillingInfoPage ShippingAndBillingInfoPage = new ShopShippingAndBillingInfoPage(
					driver, translator, elementDefinitionManager);

			ShippingAndBillingInfoPage.PaymentMethodUsed(shoppers, creditCard, country, customParameters);
			
			ShippingAndBillingInfoPage.couponApplied(customParameters);

			ShippingAndBillingInfoPage.continueCheckout();

			ShopPlaceOrderPage PlaceOrderPage = new ShopPlaceOrderPage(driver,
					translator, elementDefinitionManager);

			PlaceOrderPage.placeOrderButton();
			
			ShopConfirmationPage ConfirmationPage = new ShopConfirmationPage(
					driver, translator, elementDefinitionManager, creditCard);

			ConfirmationPage.getOrderConfNumber();
			
    	}
		else
		{
			ShopAddressInfoPageMobile address = new ShopAddressInfoPageMobile(driver, translator, elementDefinitionManager);
			address.getContinueButton().click();
			
			ShopReviewOrderPageMobile review = new ShopReviewOrderPageMobile(driver, translator, elementDefinitionManager);
			review.addBags(customParameters);
			review.continueToPayment();
			
			ShopPaymentPageMobile paymentPage = new ShopPaymentPageMobile(driver, translator, elementDefinitionManager);
			paymentPage.enterPaymentInfo(customParameters, shoppers, creditCard, country);
			
			paymentPage.addCoupon(customParameters);
			
			paymentPage.placeOrder();
			
			new ShopConfirmationPageMobile(
					driver, translator, elementDefinitionManager, creditCard);
			
		}
		
		driver.manage().deleteAllCookies();

	}

}
