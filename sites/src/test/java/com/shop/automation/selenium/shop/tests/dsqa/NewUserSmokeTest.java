package com.shop.automation.selenium.shop.tests.dsqa;

import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.testng.annotations.Test;

import com.marketamerica.automation.annotations.TestDoc;
import com.marketamerica.automation.annotations.TestSuite;
import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automationframework.tools.datamanagement.dataproviders.ExcelDataDrivenProvider;
import com.shop.automation.selenium.shop.tests.ShopBaseTests;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopPlaceOrderPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopShippingAndBillingInfoPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopDebugModePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopFooter;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopRegPage;

@TestSuite(description = { "Smoke New User Checkout" })
public class NewUserSmokeTest extends ShopBaseTests {

	public NewUserSmokeTest() {
		super();
		System.out.println("Smoke New User Checkout has been instantiated");
	}


	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will create new user via registration or checkout page and then checkout", ids = { "New User Checkout" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV})

	public void newUserSmoke(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard) {

		logger.info(id);
		logger.info(description);
		logger.info(products.get(0));
		logger.info(shoppers);
		logger.info(customParameters);
		logger.info(creditCard);

		driver.manage().deleteAllCookies();

		final ShopFooter footer = new ShopFooter(driver, translator,
				elementDefinitionManager);

		ShopDebugModePage debugpage = new ShopDebugModePage(driver, translator, elementDefinitionManager);

		ShopRegPage regpage = new ShopRegPage(driver, translator,
				elementDefinitionManager);

		ShopHomePage homePage = new ShopHomePage(driver, translator,
				elementDefinitionManager);

		homePage.checkEnvironment(locale);

		footer.printAppServer(locale);

		regpage.registerNewUser(creditCard, customParameters, shoppers, products, country, locale);

		debugpage.getDebugCookies();

		footer.printAppServer(locale);

		// Checkout process Validation

		ShopShippingAndBillingInfoPage ShippingAndBillingInfoPage = new ShopShippingAndBillingInfoPage(
				driver, translator, elementDefinitionManager);
		
		ShippingAndBillingInfoPage.PaymentMethodUsed(shoppers, creditCard, country, customParameters);

		ShippingAndBillingInfoPage.continueCheckout();
		
		ShopPlaceOrderPage PlaceOrderPage = new ShopPlaceOrderPage(driver,
				translator, elementDefinitionManager);

		PlaceOrderPage.placeOrderButton();
		
		driver.manage().deleteAllCookies();

	}
}
