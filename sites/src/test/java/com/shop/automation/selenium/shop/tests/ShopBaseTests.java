package com.shop.automation.selenium.shop.tests;

import java.lang.reflect.Method;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.webdriver.WebDriverBrowser;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.BeforeMethod;

import com.marketamerica.automation.testdata.Address;
import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automationframework.tools.MarketAmericaSiteURLs;
import com.marketamerica.automationframework.tools.testmanagement.TestCases;
import com.shop.automation.shop.configuration.ShopSiteConfiguration;
import com.shop.automation.shop.helper.ShopHelperMethods;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;

public abstract class ShopBaseTests extends TestCases {

	protected CreditCard Visa5005;
	protected CreditCard Visa4111;
	protected CreditCard AmexCC;
	protected CreditCard newCreditCard;

	protected ShopHelperMethods helperMethods;

	// private ShopHomePage homePage;

	protected static void pause() {
		long end = System.currentTimeMillis() + 6000;
		while (System.currentTimeMillis() < end) {
		}
	}

	@BeforeMethod(alwaysRun = true)
	public void setupMethod(final ITestContext context, final Method method,
			final ITestResult result) {
		super.setupMethod(context, method, result);
		try {

			this.creditCards = this.dataManager.getCreditCardList();

			Visa5005 = this.creditCards.getCreditCard("VisaCC5005");
			Visa4111 = this.creditCards.getCreditCard("VisaCC4111");
			AmexCC = this.creditCards.getCreditCard("AmexCC");

			driver.navigate().to(getBaseURL());
			helperMethods = new ShopHelperMethods(driver, translator,
					elementDefinitionManager);

			// If Safari WebDriver, log out if logged in,
			// set country to "United States" and set
			// language to "English"
			if (((RemoteWebDriver) driver).getCapabilities().getBrowserName()
					.equals(WebDriverBrowser.SAFARI.toString())) {
				final ShopHomePage homePage = new ShopHomePage(driver,
						translator, elementDefinitionManager);
				homePage.headerOverlay.logoutOfAccount();
				homePage.headerOverlay.resetTopBanner();
				helperMethods.emptyCart();
				homePage.headerOverlay.clickHomeLogo();
			}

			// If Internet Explorer WebDriver, log out if logged in,
			else if (((RemoteWebDriver) driver).getCapabilities()
					.getBrowserName()
					.equals(WebDriverBrowser.INTERNET_EXPLORER.toString())) {
				final ShopHomePage homePage = new ShopHomePage(driver,
						translator, elementDefinitionManager);
				homePage.headerOverlay.logoutOfAccount();
			}
		} catch (Exception e) {
			logger.fatal(e);
		}

	}

	@Override
	public String getBaseURL() {
		return MarketAmericaSiteURLs.determineURL(new ShopSiteConfiguration(locale),
				environment, locale);
	}

	/*
	 * protected void navigateToTestPortalBaseURL(Shopper shopper){ if(shopper
	 * != null){ navigateToTestPortal(shopper); this.homePage = new
	 * ShopHomePage(driver, translator, elementDefinitionManager); // verify on
	 * test portal
	 * Verify.verifyEquals(homePage.headerOverlay.getShopConsultantName(),
	 * shopper.getPortal().getShopConsultantName(),
	 * String.format("Expected Shop Consultant name to be %s",
	 * shopper.getPortal().getShopConsultantName())); }else{
	 * Assert.fail("You must specify the Test Portal Key in the Shopper xml"); }
	 * }
	 */
}
