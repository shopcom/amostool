package com.shop.automation.selenium.shop.tests.dsqa;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.testng.annotations.Test;

import com.marketamerica.automation.annotations.TestDoc;
import com.marketamerica.automation.annotations.TestSuite;
import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.Portal;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.marketamerica.automationframework.tools.datamanagement.dataproviders.ExcelDataDrivenProvider;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.shop.automation.shop.selenium.pages.validations.ShopPlaceOrderPageValidation;
import com.shop.automation.shop.selenium.pages.validations.ShopShippingAndBillingValidation;
import com.shop.automation.selenium.shop.tests.ShopBaseTests;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopAddressInfoPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopCheckoutPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopCompleteAddressPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopCompleteCheckout;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopPlaceOrderPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopShippingAndBillingInfoPage;
import com.shop.automation.shop.selenium.pages.base.checkout.mobile.ShopAddressInfoPageMobile;
import com.shop.automation.shop.selenium.pages.base.checkout.mobile.ShopReviewOrderPageMobile;
import com.shop.automation.shop.selenium.pages.base.common.ShopDebugModePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopFooter;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopPaymentMethodsPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopProductPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopRegPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopTestVariables;
import com.shop.automation.shop.selenium.pages.validations.ShopConfirmationPageValidation;
import com.shop.automation.shop.selenium.pages.validations.mobile.ShopReviewOrderMobileValidation;

@TestSuite(description = { "New User Checkout" })
public class NewUserCheckoutTest extends ShopBaseTests {

	public NewUserCheckoutTest() {
		super();
		System.out.println("New User Checkout has been instantiated");
	}
	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will test Visa Checkout Test Cases as New User", ids = { "DSQA #7" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV})

	public void NewUserVCO(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard){
			this.testNewUserCheckout(id, description, products, shoppers, customParameters, creditCard);
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will test Open Jira Test Cases", ids = { "DSQA #7" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV})

	public void OpenJiraDSQA7(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard){
			this.testNewUserCheckout(id, description, products, shoppers, customParameters, creditCard);
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will test Coupon Test Cases", ids = { "DSQA #7" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV})

	public void newUserCouponTests(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard){
			this.testNewUserCheckout(id, description, products, shoppers, customParameters, creditCard);
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will create new user via registration or checkout page and then checkout", ids = { "DSQA #7" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV})

	public void DSQA7(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard){
			this.testNewUserCheckout(id, description, products, shoppers, customParameters, creditCard);
	}
	
	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will create new user via registration or checkout page and then checkout", ids = { "DSQA #7" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV})

	public void debugTest(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard){
			this.testNewUserCheckout(id, description, products, shoppers, customParameters, creditCard);
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will create new user via registration or checkout page and then checkout", ids = { "New User Checkout" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV})

	public void testNewUserCheckout(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard) {

		ShopTestVariables var = new ShopTestVariables(driver, translator,
				elementDefinitionManager);

		logger.info(id);
		logger.info(description);
		logger.info(products.get(0));
		logger.info(shoppers);
		logger.info(customParameters);
		logger.info(creditCard);

		driver.manage().deleteAllCookies();
		
		final ShopFooter footer = new ShopFooter(driver, translator,
				elementDefinitionManager);

		ShopDebugModePage debugpage = new ShopDebugModePage(driver, translator, elementDefinitionManager);

		ShopRegPage regpage = new ShopRegPage(driver, translator,
				elementDefinitionManager);

		ShopHomePage homePage = new ShopHomePage(driver, translator,
				elementDefinitionManager);
		
		debugpage.getDateTimeStamp();
		
		footer.printAppServer(locale);

		homePage.checkEnvironment(locale);
		
		regpage.registerNewUser(creditCard, customParameters, shoppers, products, country, locale);

		debugpage.getDebugCookies();

		footer.printAppServer(locale);

		// Checkout process Validation
		new ShopCompleteCheckout(driver, translator, elementDefinitionManager, shoppers, creditCard, products, customParameters, country, locale);

		driver.manage().deleteAllCookies();

	}
}
