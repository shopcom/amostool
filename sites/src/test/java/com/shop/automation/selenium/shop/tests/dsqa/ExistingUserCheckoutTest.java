package com.shop.automation.selenium.shop.tests.dsqa;

import org.testng.annotations.Test;

import com.shop.automation.selenium.shop.tests.ShopBaseTests;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopCheckoutPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopCompleteAddressPage;
import com.shop.automation.shop.selenium.pages.base.checkout.ShopCompleteCheckout;
import com.shop.automation.shop.selenium.pages.base.common.ShopCartPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopDebugModePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopFooter;
import com.shop.automation.shop.selenium.pages.base.common.ShopHomePage;
import com.shop.automation.shop.selenium.pages.base.common.ShopProductPage;
import com.shop.automation.shop.selenium.pages.base.common.ShopTestVariables;
import com.marketamerica.automation.annotations.TestDoc;
import com.marketamerica.automation.annotations.TestSuite;
import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automationframework.tools.datamanagement.dataproviders.ExcelDataDrivenProvider;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/*Author Rick Vera*/

@TestSuite(description = { "Existing User Checkout" })
public class ExistingUserCheckoutTest extends ShopBaseTests {

	public ExistingUserCheckoutTest() {
		super();
		System.out.println("Existing User Checkout has been instantiated");
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will test Visa Checkout Test Cases", ids = { "DSQA #8" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN,
			Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV })
	public void ExUserVCO(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard) {
		this.testExUserCheckout(id, description, products, shoppers,
				customParameters, creditCard);
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will test Open Jira Test Cases", ids = { "DSQA #8" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN,
			Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV })
	public void ExUserProdOptions(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard) {
		this.testExUserCheckout(id, description, products, shoppers,
				customParameters, creditCard);
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will test Open Jira Test Cases", ids = { "DSQA #8" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN,
			Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV })
	public void OpenJiraDSQA8(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard) {
		this.testExUserCheckout(id, description, products, shoppers,
				customParameters, creditCard);
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will test eNets Test Cases", ids = { "DSQA #8" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN,
			Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV })
	public void eNets(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard) {
		this.testExUserCheckout(id, description, products, shoppers,
				customParameters, creditCard);
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will test Coupon Test Cases", ids = { "DSQA #8" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN,
			Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV })
	public void ExUserCouponTests(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard) {
		this.testExUserCheckout(id, description, products, shoppers,
				customParameters, creditCard);
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will test DSQA #8", ids = { "DSQA #8" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN,
			Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV })
	public void DSQA8(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard) {
		this.testExUserCheckout(id, description, products, shoppers,
				customParameters, creditCard);
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will test DSQA #8", ids = { "DSQA #8" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN,
			Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV })
	public void ExUserMPAMAA(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard) {
		this.testExUserCheckout(id, description, products, shoppers,
				customParameters, creditCard);
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will test DSQA #8", ids = { "DSQA #8" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN,
			Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV })
	public void debugTest(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard) {
		this.testExUserCheckout(id, description, products, shoppers,
				customParameters, creditCard);
	}

	@Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "addressShoppersIdsAndDescription")
	@TestDoc(description = "This test script will check for existing user checkout", ids = { "Existing User Checkout" }, supportedCountries = {
			Country.UNITED_STATES, Country.UNITED_KINGDOM, Country.SPAIN,
			Country.MEXICO, Country.AUSTRALIA, Country.CANADA, Country.TAIWAN,
			Country.SINGAPORE, Country.HONG_KONG }, supportedEnvironments = {
			Environment.STAGING, Environment.LIVE, Environment.LTV })
	public void testExUserCheckout(final String id, final String description,
			final List<Product> products, final List<Shopper> shoppers,
			final Map<String, String> customParameters,
			final CreditCard creditCard) {

		ShopTestVariables var = new ShopTestVariables(driver, translator,
				elementDefinitionManager);

		ShopCheckoutPage checkout = new ShopCheckoutPage(driver,
				translator, elementDefinitionManager, country);

		logger.info(id);
		logger.info(description);
		logger.info(products.get(0));
		logger.info(shoppers);
		logger.info(customParameters);
		logger.info(creditCard);

		/**
		 * 1.) Navigate to Login Page 2.) Login as Existing user 3.) Navigate to
		 * product 4.) Add it to the cart 5.) Go to Checkout 6.) Continue Add
		 * tab 7.) Continue Ship tab 8.) Place order tab 9.) Confirmation page
		 */

		// Navigate to the home page
		final ShopFooter footer = new ShopFooter(driver, translator,
				elementDefinitionManager);

		ShopHomePage homePage = new ShopHomePage(driver, translator,
				elementDefinitionManager);

		ShopDebugModePage debugpage = new ShopDebugModePage(driver, translator,
				elementDefinitionManager);

		if (var.isDesktop(WebDriverUserAgent.getCurrentUserAgent())){
		driver.manage().deleteAllCookies();
		}
		
		debugpage.getDateTimeStamp();

		homePage.checkEnvironment(locale);

		homePage.header.closeCountrySelectionPopup();

		footer.printAppServer(locale);

		// Sign in with a user
		homePage.header.signIn(shoppers.get(0), country, locale);

		debugpage.getDebugCookies();

		ShopCartPage cartPage = homePage.header.navigateToCartPage();

		if (var.RWDDesktopCheckoutType(locale,
				WebDriverUserAgent.getCurrentUserAgent())) {

			// Clear Cart
			cartPage.RWDclearCart();

			// Navigate to Product(s) and add to cart
			new ShopProductPage(driver, translator, elementDefinitionManager,
					products, customParameters, country, locale);

			// Checkout
			checkout.enterRWDCheckoutFullSite(customParameters, products,
					creditCard, shoppers.get(0), country);

			homePage.checkEnvironment(locale);

		}

		else {

			// Clear Cart
			cartPage.clearCart();

			// Navigate to Product(s) and add to cart
			new ShopProductPage(driver, translator, elementDefinitionManager,
					products, customParameters, country, locale);

			// Checkout
			checkout.enterCheckout(customParameters, creditCard,
					shoppers.get(0), products, country, locale);

			homePage.checkEnvironment(locale);

			// Validation (Address Info Page)
			new ShopCompleteAddressPage(driver, translator,
					elementDefinitionManager, products, customParameters,
					country);

		}
		footer.printAppServer(locale);

		debugpage.getDebugCookies();

		new ShopCompleteCheckout(driver, translator,
				elementDefinitionManager, shoppers, creditCard, products,
				customParameters, country, locale);

		driver.manage().deleteAllCookies();

	}
}
