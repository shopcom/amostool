# The Safari Hunter looks for Safari Processes that have been
# running longer than 10 minutes. Once a process if found that
# is at least 10 minutes old, the Safari hunter will ruthlessly
# destroy it

# author: Javier L. Velasquez <nycjv321@gmail.com>
# version: v0.04a

$global:safariKills = 0;
$global:werFaultKills = 0;

#
# Sometimes a WerFault process exists since Safari crashed.
# So we kill it  if it exists
#
Function KillWerFault {

    $running = Get-Process WerFault -ErrorAction SilentlyContinue

    if ($running) {
        $process = Get-Process WerFault
        "Killing WerFault"
		Stop-Process $process.Id   
        $werFaultKills++;
    }
}

# Print a Summary
Function PrintSomeStats {

	$now = Get-Date;
    if ($werFaultKills -gt 1 -and $safariKills -gt 0) {
        if ($now.Hour % 2 -eq 0 -and $now.Minute -eq 45) {
            "WerFault has been killed $werFaultKills times";
            "Safari has been killed $safariKills times";
        } 
    } elseif ($now.Hour % 2 -eq 0 -and $now.Minute -eq 45) {
        if ($werFaultKills -eq 0) {
            "WerFault hasn't been killed yet ☹";
        }
        if ($safariKills -eq 0) {
               "Safari hasn't been killed yet ☹";
        }
    }


}


while (1 -eq 1) {

    PrintSomeStats;
    KillWerFault;

	$running = Get-Process Safari -ErrorAction SilentlyContinue;

	if (!$running) {
		"Sleeping Safari Hunter";
		Start-Sleep -s 60
        continue;
	}

	$process = Get-Process Safari;
	$birth = $process.StartTime;
	$now = Get-Date;

	$difference = $now - $birth;

    #
    # If the process has been running for atleast 10 minutes end it
    # else spare its life!
    #
	if ($difference.Minutes -ge 10) {
		"Ending Safari's $process.Id life at $now";
        $safariKills++;
		Stop-Process $process.Id;
	} else {
		"Safari's life has been spared!";
	}

	"Sleeping Safari Hunter";
	Start-Sleep -s 60;
	"The Safari Hunter has awoken!";
}
