#!/usr/bin/perl

use Time::Local;
use constant MAX_PROCESS_AGE => 10 * 60;
use constant HUNTER_SLEEP_TIME => 60;

# The Safari Hunter looks for Safari Processes that have been 
# running longer than 10 minutes. Once a process if found that 
# is at least 10 minutes old, the Safari hunter will ruthlessly
# destroy it
# author: Javier L. Velasquez <nycjv321@gmail.com>

# Begin infinite loop!
while (true) {
  # For each process return a list of its pid,
  # elapse time, arguments
  for (qx{ps -ea -o pid,etime,args }) {
    chomp;
    @days = (split);

    $command = @days[2];

    # We only care about safari. If the current
    # process is not Safari we continue to the 
    # next iteration
    if (index($command, 'Safari') == -1) {
      next;
    }
 
    $days[1] =~ s/-\d\d:\d\d:\d\d//;

    my $count = () = $days[1] =~ /:/g;
  
    $min = 0;
    $hour = 0; 
    $sec = 0; 

    if ($count == 0) {
      ($sec) = split(/:/, $days[1]);
    } elsif ($count == 1) {
      ($min,$sec) = split(/:/, $days[1]);
    } elsif ($count == 2) {
      ($hour,$min,$sec) = split(/:/, $days[1]);
    }

    $age = ($hour * 60 * 60) + ($min * 60) + $sec;
    $pid = $days[0];

    print "$command ($pid) is running $age seconds\n";
    # If the process is older than the max process age
    # then destroy it
    if ($age >= MAX_PROCESS_AGE) {
      print "Killing Safari ($pid) becuase it was $age seconds old!\n";
      kill 'KILL', $pid;
    } else {
      print "Safari's life was spared\n";
    }
  }
  print "Sleeping Safari Hunter\n";
  # Sleep the process so we don't waste unnecassary resources
  sleep HUNTER_SLEEP_TIME;
  print "The Safari Hunter has awoken!\n";
}
