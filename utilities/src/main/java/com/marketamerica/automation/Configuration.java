package com.marketamerica.automation;

import java.io.InputStream;
import java.util.Locale;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.marketamerica.automation.annotations.NotThreadSafe;
import com.marketamerica.automation.constants.GlobalConstants;
import com.marketamerica.automation.utilities.PropertyReader;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.enums.Project;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;

/**
 * Project environmentConfiguration defined in the <span>project.properties</span> and
 * <span>${environment}.properties</span> files should go here.
 */
public class Configuration {
    public static final String PROJECT_NAME = "ProjectName";
    final static Logger logger = LogManager
            .getLogger(Configuration.class);
    private static PropertyReader base;
    private static PropertyReader environmentConfiguration;
    private static Locale locale;
    private static Configuration configuration;
    private static Country country;
    private final Project project;

    public Configuration() {
        project = null;
    }

    public Configuration(Project project) {
        this.project = project;
    }

    public static void initialize(Environment environment, Project project) {
        if (configuration == null) {
            synchronized (Configuration.class) {
                configuration = new Configuration(project);
            }
        }
        final InputStream environmentInputStream = Configuration.class.getResourceAsStream(getConfigurationLocation(environment));
        if (environmentInputStream != null) {
            environmentConfiguration = new PropertyReader(environmentInputStream);
        } else {
            logger.warn(String.format("Could not find environmentConfiguration properties file for given environment, \"%s\"", environment.toString()));
        }
        final InputStream projectInputStream = Configuration.class.getResourceAsStream(getConfigurationLocation());
        if (projectInputStream != null) {
            base = new PropertyReader(projectInputStream);
        } else {
            logger.error(String.format("Could not find base environmentConfiguration properties file for given project. Please add a valid project.properties file to the /config directory."));
        }
    }

    /**
     * Initialize Configuration based on the given environment. This allows us to
     * load environmentConfiguration settings from the specific environment property
     * file as well as the base project.properties file.
     *
     * @param environment a given environment constant.
     */
    public synchronized static void initialize(final Environment environment) {
        initialize(environment, null);
    }

    private static String getConfigurationLocation(Environment environment) {
        return getConfigurationLocation().replace("project", environment.toString());
    }

    private static String getConfigurationLocation() {
        if (configuration.project() != null) {
            return String.format("/%s/config/project.properties", configuration.project.toString());
        } else {
            return "/config/project.properties";
        }
    }

    /**
     * Returns the project's name
     *
     * @return
     */
    public static String getProjectName() {
        return String.valueOf(get(PROJECT_NAME));
    }

    public static Project getProject() {
        return Project.parse(getProjectName());
    }

    /**
     * Return a value based on given key
     *
     * @param key a key identifier
     * @return a value identified by the key that is either stored in the current
     * environment property file or the base project.properties file if it does
     * not exist in the current environment property file.
     */
    public static Object get(final String key) {
        if (environmentConfiguration != null && environmentConfiguration.hasKey(key)) {
            return environmentConfiguration.get(key);
        } else {
            return base.get(key);
        }
    }

    public static Environment getEnvironment() {
        return Environment.getCurrentEnvironment();
    }
    
    public static WebDriverUserAgent getUserAgent() {
        return WebDriverUserAgent.getCurrentUserAgent();
    }

    /**
     * This method should not be used. Instead use the {@code country} field provided by {@code TestCases}
     *
     * @return
     */
    @Deprecated
    public synchronized static Country getCountry() {
        return country;
    }

    /**
     * This method should not be used. Instead use the {@code country} field provided by {@code TestCases}
     *
     * @param country
     */
    @Deprecated
    @NotThreadSafe
    public synchronized static void setCountry(Country country) {
        Configuration.country = country;
    }

    public static String getAppName() {
        return String.valueOf(get(GlobalConstants.Capability.APP));
    }

    public static String getAppActivity() {
        return String.valueOf(get(GlobalConstants.Capability.APP_ACTIVITY));
    }

    public static String getAppPackage() {
        return String.valueOf(get(GlobalConstants.Capability.APP_PACKAGE));
    }

    /**
     * This method should not be used. Instead use the {@code locale} field provided by {@code TestCases}
     *
     */
    @NotThreadSafe
    @Deprecated
    public static Locale getLocale() {
        return locale;
    }

    /**
     * This method should not be used. Instead use the {@code locale} field provided by {@code TestCases}
     *
     * @param locale
     */
    @Deprecated
    @NotThreadSafe
    public static void setLocale(Locale locale) {
        Configuration.locale = locale;
    }

    public Project project() {
        return project;
    }

}
