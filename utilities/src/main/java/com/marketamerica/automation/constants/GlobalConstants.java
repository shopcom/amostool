package com.marketamerica.automation.constants;

/**
 * All Global constants should be stored here
 *
 * @author javierv
 */
public class GlobalConstants {
    public static final String USER_ALL_LOWER_CASE = "user";
    public static final String PASSWORD_ALL_LOWER_CASE = "password";
    public static final String FALSE_ALL_LOWER_CASE = "false";
    public static final String MYSQL = "mysql";
    public static final String JDBC = "jdbc";
    public static final String UTF8 = "UTF8";
    public static final String SYSTEM_TEMP = System.getProperty("java.io.tmpdir");
    public static final String DEFAULT_LANGUAGE_CODE = "en";
    public static final String DEFAULT_COUNTRY_CODE = "US";
    public static final String IE_DRIVER_SERVER_EXE = "IEDriverServer.exe";
    public static final String CHROMEDRIVER_EXE = "chromedriver.exe";
    public static final String SQL_SERVER = "sqlserver";
    public static final String SQL_SERVER_DRIVER_CLASS_NAME = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    public static final long MAX_TEST_EXECUTION_TIME = 900000;    

    //These browser versions need to be updated with any version changes
    public static final String[] VALID_FIREFOX_VERSIONS = {"31", "29", "30", "27", "28"};
    public static final String[] VALID_IE_VERSIONS = {"10", "11"};
    public static final String VERSION_WINDOWS_SAFARI = "5.1";
    public static final String VERSION_MAC_SAFARI = "7.0.5";
    public static final String VERSION_WINDOWS_CHROME = "38.0";

    public static final String DEFAULT_ANDROID_DRIVER_PORT = "4723";
    
 // constants for QA mySQL database
    public static final String MYSQL_SERVER_DRIVER_CLASS_NAME = "com.mysql.jdbc.Driver";
    
    /**
     * This inner class holds all constants related to test link
     *
     * @author javierv
     */
    public static final class TestLink {
        public static final String SSH_USER = "root";
        public static final String SSH_PASSWORD = "p0c4dm!n";
        public static final String HOST_IP = "10.99.110.223";
        public static final int SSH_PORT = 22;
        public static final int DB_PORT = 3306;
        public static final String DB_USER = "tluser";
        public static final String DB_PASSWORD = "testlink";
        public static final String DATABASE = "testlink";
        public static final String XMLRPC_URL = String.format(
                "http://%s/testlink/lib/api/xmlrpc/v1/xmlrpc.php", HOST_IP);
        public static final String API_DEVELOPER_KEY = "ea64076e15fb79c0c7645d7dca92973c";
        public static final String CUSTOM_FIELD_DELIMITER = "\\||, ";
    }

    public static final class TestNGSuiteParameters {
        public static final String REPORT_NG_DISABLED = "reportNGDisabled";
        public static final String TEST_LINK_PROJECT = "testLinkProject";
        public static final String ENVIRONMENT_STRING = "environment";
        public static final String USERAGENT_STRING = "useragent";
        public static final String COUNTRY_CODE = "countryCode";
        public static final String LANGUAGE_CODE = "languageCode";

        public static final String RETRY_COUNT = "retryCount";
        public static final String LOCALE = "locale";
    }

    public static final class Shop {
        public static class DB {
            public static final String USER = "QAAutomatedTesting";
            public static final String PASSWORD = "WorkedOnDev";
            public static class Staging {
                public static final String NAME = "MainDB";
                public static final String URL = "dbms-st043.corp.shop.com\\stg43";

            }

            public static class Live {
                public static final String NAME = "MainDB";
                public static final String URL = "CCSQLREPORTS1.corp.shop.com";

            }
        }
    }

    public static final class HTTP {
        public static final String POST = "POST";
    }

    public static final class Capability {
        public static final String DEVICE = "device";
        public static final String APP = "app";
        public static final String APP_ACTIVITY = "appActivity";
        public static final String APP_PACKAGE = "appPackage";
        public static final String PLATFORM_NAME = "platformName";
        public static final String DEVICE_NAME = "deviceName";
        public static final String APP_WAIT_PACKAGE = "app-wait-activity";
        public static final String AUTOMATION_NAME = "automation-Name";

    }
    
    public static final class QASQL {
        public static class DB {
        	public static final String USER = "misdepttest";
            public static final String PASSWORD = "Testing1212";
            
            public static class Staging {
                public static final String NAME = "TDCRS";
                public static final String URL = "10.99.110.165\\dev";
            }

            public static class Live {
                public static final String NAME = "TDCRS";
                public static final String URL = "10.99.110.165\\dev";
            }
        }
    }
    
    public static final class QAmySQL {
        public static class DB {
            public static final String USER = "misdepttest";
            public static final String PASSWORD = "Testing1212";
            
            public static class Staging {
                public static final String NAME = "TDRCS";
                public static final String URL = "jdbc:mysql://10.99.215.70/TDRCS";
            }

            public static class Live {
                public static final String NAME = "TDRCS";
                public static final String URL = "jdbc:mysql://10.99.215.70/TDRCS";

            }
        }
    }

}
