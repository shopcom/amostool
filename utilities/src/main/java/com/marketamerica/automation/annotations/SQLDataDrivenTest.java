package com.marketamerica.automation.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)

public @interface SQLDataDrivenTest {

    public String[] addressKeys() default {};

    public String[] productKeys() default {};

    public String[] distributerKeys() default {};
    
    public String[] productCategoryKeys() default {};
    
    public String[] iterationKeys() default {};
    
    public String productCategoryKey() default "";
    
    public String iterationKey() default "";
}
