package com.marketamerica.automation.annotations;

import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.METHOD;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation is used to describe constructors and methods whose operations do not guarantee thread safety.
 * Created by Javier L. Velasquez on 11/20/2014.
 */
@Target({METHOD, CONSTRUCTOR})
@Retention(RetentionPolicy.RUNTIME)
public @interface NotThreadSafe {
    public String description() default "";
}
