package com.marketamerica.automation.annotations;

/**
 * This annotation allows us to document TestCase classes
 * by documenting the corresponding Test Suite on TestLink
 */
public @interface TestSuite {
    String[] description() default "";
}
