package com.marketamerica.automation.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)

public @interface XMLDataDrivenTest {

    public String[] addressKeys() default {};

    public String[] productKeys() default {};

    public String[] distributerKeys() default {};

}
