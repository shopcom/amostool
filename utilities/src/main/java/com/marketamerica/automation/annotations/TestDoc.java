package com.marketamerica.automation.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.webdriver.WebDriverDevice;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;

/**
 * Annotation For TestCase - Test Cases id, testObjective, description,
 * environments(Staging , live, development) and countries
 * <p>
 * This annotation should accompany all Test methods.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface TestDoc {

    public String[] ids() default {};

    public String[] requirements() default {};

    @Deprecated
    public String testObjective() default "";

    public String description() default "";

    public Environment[] supportedEnvironments() default {};
    
    public WebDriverUserAgent[] supportedUserAgents() default {};

    public String dateDocumented() default "";

    public Country[] supportedCountries() default {};

    public int retryCount() default 0;

    public int version() default 1;

    public WebDriverDevice[] device() default {};

    public String[] oustandingJiraDefects() default {};

    public boolean usesWebDriver() default true;
}
