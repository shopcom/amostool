package com.marketamerica.automation.testdatalists;

import java.util.Iterator;
import java.util.List;

import com.marketamerica.automation.testdata.User;

/**
 * A user list represents a list of shoppers
 *
 * @author Javier L. Velasquez
 */
public class UserList extends AbstractDataList implements Iterable<User> {

    @SuppressWarnings("unchecked")
    public List<User> getUsers() {
        return (List<User>) this.getTestDataList();
    }

    @Override
    public Iterator<User> iterator() {
        @SuppressWarnings("unchecked")
        Iterator<User> user = (Iterator<User>) this.testDataList.iterator();
        return user;
    }


    public User getUser(String userId) {
        return (User) super.getTestData(userId);
    }

}
