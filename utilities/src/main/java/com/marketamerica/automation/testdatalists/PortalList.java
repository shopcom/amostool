package com.marketamerica.automation.testdatalists;

import java.util.Iterator;
import java.util.List;

import com.marketamerica.automation.testdata.AbstractData;
import com.marketamerica.automation.testdata.Portal;


/**
 * List for Portals
 *
 * @author archanac
 */
public class PortalList extends AbstractDataList implements Iterable<Portal> {

    protected List<Portal> portalDataList;

    public List<Portal> getPortal() {
        return this.getTestDataList();
    }

    @Override
    protected List<Portal> getTestDataList() {
        return portalDataList;
    }

    @Override
    protected AbstractData getTestData(final String id) {
        for (Portal portal : portalDataList) {
            if (portal.getId().equals(id)) {
                return portal;
            }
        }
        throw new IllegalArgumentException(String.format("Could not find data with '%s' key", id));
    }

    public Portal getPortal(String id) {
        return (Portal) getTestData(id);
    }

    @Override
    public Iterator<Portal> iterator() {
        Iterator<Portal> portal = (Iterator<Portal>) this.portalDataList.iterator();
        return portal;
    }

}
