package com.marketamerica.automation.testdatalists;

import java.util.ArrayList;

import com.marketamerica.automation.utilities.enums.CustomMiniProductCategories;

/**
 * List of CustomMini Product Categories
 * @author archanac
 *
 */

public class CustomMiniProductCategoriesList {
	private ArrayList<CustomMiniProductCategories> categories = new ArrayList<>();

	public CustomMiniProductCategoriesList(){
		categories = new ArrayList<CustomMiniProductCategories>();
	}

	public void setElbows(ArrayList<CustomMiniProductCategories> categories){
	    this.categories.clear();
	    this.categories = categories;
	}

	public ArrayList<CustomMiniProductCategories> getElbows() {
	    return categories;
	}

	public void add(CustomMiniProductCategories category){
		categories.add(category);
	}
}
