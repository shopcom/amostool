package com.marketamerica.automation.testdatalists;

import java.util.Iterator;
import java.util.List;

import com.marketamerica.automation.testdata.CustomMiniWebSite;

/**
 * List of CustomMini WebSites
 * @author archanac
 *
 */
public class CustomMiniWebSiteList extends AbstractDataList implements Iterable<CustomMiniWebSite> {

	 protected List<CustomMiniWebSite> CustomMiniWebSiteDataList;

	    public List<CustomMiniWebSite> getCustomMiniWebSites() {
	        return this.getTestDataList();
	    }

	    @Override
	    protected List<CustomMiniWebSite> getTestDataList() {
	        return CustomMiniWebSiteDataList;
	    }

	    @Override
	    protected CustomMiniWebSite getTestData(final String id) {
	        for (CustomMiniWebSite CustomMiniWebSite : CustomMiniWebSiteDataList) {
	            if (CustomMiniWebSite.getId().equals(id)) {
	                return CustomMiniWebSite;
	            }
	        }
	        throw new IllegalArgumentException(String.format(
	                "Could not find data with '%s' key", id));
	    }

	    @Override
	    public Iterator<CustomMiniWebSite> iterator() {
	        Iterator<CustomMiniWebSite> CustomMiniWebSite = (Iterator<CustomMiniWebSite>) this.CustomMiniWebSiteDataList
	                .iterator();
	        return CustomMiniWebSite;
	    }

	    public CustomMiniWebSite getCustomMiniWebSite(String CustomMiniWebSiteId) {
	        return (CustomMiniWebSite) this.getTestData(CustomMiniWebSiteId);
	    }
}
