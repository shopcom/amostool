package com.marketamerica.automation.testdatalists;

import java.util.Iterator;
import java.util.List;

import com.marketamerica.automation.testdata.StaffMember;

/**
 * Staff Member List represents list of staff members
 * @author archanac
 *
 */
public class StaffMemberList extends AbstractDataList implements Iterable<StaffMember> {

	 protected List<StaffMember> staffMemberDataList;

	    public List<StaffMember> getStaffMembers() {
	        return this.getTestDataList();
	    }

	    @Override
	    protected List<StaffMember> getTestDataList() {
	        return staffMemberDataList;
	    }

	    @Override
	    protected StaffMember getTestData(final String id) {
	        for (StaffMember staffMember : staffMemberDataList) {
	            if (staffMember.getId().equals(id)) {
	                return staffMember;
	            }
	        }
	        throw new IllegalArgumentException(String.format(
	                "Could not find data with '%s' key", id));
	    }

	    @Override
	    public Iterator<StaffMember> iterator() {
	        Iterator<StaffMember> staffMember = (Iterator<StaffMember>) this.staffMemberDataList
	                .iterator();
	        return staffMember;
	    }

	    public StaffMember getStaffMember(String staffMemberId) {
	        return (StaffMember) this.getTestData(staffMemberId);
	    }
}
