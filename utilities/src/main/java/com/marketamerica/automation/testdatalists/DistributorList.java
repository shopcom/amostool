package com.marketamerica.automation.testdatalists;

import java.util.Iterator;
import java.util.List;

import com.marketamerica.automation.testdata.Distributor;

/**
 * A Distributor list represents a list of distributors
 *
 * @author Javier L. Velasquez
 */
public class DistributorList extends AbstractDataList implements Iterable<Distributor> {

    protected List<Distributor> distributorDataList;

    public List<Distributor> getDistributors() {
        return this.getTestDataList();
    }

    @Override
    protected List<Distributor> getTestDataList() {
        return distributorDataList;
    }

    @Override
    protected Distributor getTestData(final String id) {
        for (Distributor distributor : distributorDataList) {
            if (distributor.getId().equals(id)) {
                return distributor;
            }
        }
        throw new IllegalArgumentException(String.format(
                "Could not find data with '%s' key", id));
    }

    @Override
    public Iterator<Distributor> iterator() {
        Iterator<Distributor> distributor = (Iterator<Distributor>) this.distributorDataList
                .iterator();
        return distributor;
    }

    public Distributor getDistributor(String distributorId) {
        return this.getTestData(distributorId);
    }

}
