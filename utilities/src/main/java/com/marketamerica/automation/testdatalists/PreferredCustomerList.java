package com.marketamerica.automation.testdatalists;

import java.util.List;

import com.marketamerica.automation.testdata.Shopper;

/**
 * A Shopper list represents a list of shoppers
 *
 * @author Javier L. Velasquez
 */
public class PreferredCustomerList extends AbstractDataList {

    protected List<Shopper> shopperDataList;

    public List<Shopper> getShoppers() {
        return this.getTestDataList();
    }

    @Override
    protected List<Shopper> getTestDataList() {
        return shopperDataList;
    }

    @Override
    protected Shopper getTestData(final String id) {
        for (Shopper shopper : shopperDataList) {
            if (shopper.getId().equals(id)) {
                return shopper;
            }
        }
        throw new IllegalArgumentException(String.format(
                "Could not find data with '%s' key", id));
    }

    public Shopper getShopper(String shopperId) {
        return (Shopper) this.getTestData(shopperId);
    }

}
