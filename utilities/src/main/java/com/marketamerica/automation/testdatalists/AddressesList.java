package com.marketamerica.automation.testdatalists;

import java.util.List;

import com.marketamerica.automation.testdata.Address;

/**
 * An address list represents a list of addresses
 *
 * @author Javier L. Velasquez
 */
public class AddressesList extends AbstractDataList {

    @SuppressWarnings("unchecked")
    public List<Address> getAddresses() {
        return (List<Address>) this.getTestDataList();
    }

    public Address getAddress(String id) {
        return (Address) super.getTestData(id);
    }

}
