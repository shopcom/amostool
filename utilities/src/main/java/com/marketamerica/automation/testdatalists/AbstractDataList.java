package com.marketamerica.automation.testdatalists;

import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.marketamerica.automation.testdata.AbstractData;

/**
 * TestDataList is an abstract representation of a list of test data
 *
 * @author Javier L. Velasquez
 */
public abstract class AbstractDataList {
    final static Logger logger = LogManager
            .getLogger(AbstractDataList.class);
    protected List<? extends AbstractData> testDataList;

    /**
     * Get the test data list
     *
     * @return A list of elements that implement TestData
     */
    protected List<? extends AbstractData> getTestDataList() {
        return testDataList;
    }

    /**
     * Get a test data element by an id
     *
     * @param id an input id
     * @return Test Data element
     */
    protected AbstractData getTestData(final String id) {
        for (AbstractData abstractData : testDataList) {
            if (abstractData.getId().equals(id)) {
                return abstractData;
            }
        }
        logger.error(String.format("Could not find data with '%s' key", id));
        throw new IllegalArgumentException(String.format("Could not find data with '%s' key", id));
    }

    /**
     * Return a test data element by id
     *
     * @param id an id to search for
     * @return
     */
    public AbstractData get(final String id) {
        return getTestData(id);
    }

}
