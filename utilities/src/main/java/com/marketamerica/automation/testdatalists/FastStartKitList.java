package com.marketamerica.automation.testdatalists;


import java.util.Iterator;
import java.util.List;

import com.marketamerica.automation.testdata.FastStartKit;

/**
 * A fastStartKit list represents a list of fastStartKits
 *
 * @author Javier L. Velasquez
 */
public class FastStartKitList extends AbstractDataList implements Iterable<FastStartKit> {


    @Override
    protected List<FastStartKit> getTestDataList() {
        return (List<FastStartKit>) testDataList;
    }

    @Override
    protected FastStartKit getTestData(final String id) {
        for (FastStartKit fastStartKit : getTestDataList()) {
            if (fastStartKit.getId().equals(id)) {
                return fastStartKit;
            }
        }
        throw new IllegalArgumentException(String.format(
                "Could not find data with '%s' key", id));
    }

    @SuppressWarnings("unchecked")
	@Override
    public Iterator<FastStartKit> iterator() {
        Iterator<FastStartKit> fastStartKit = (Iterator<FastStartKit>) this.testDataList
                .iterator();
        return fastStartKit;
    }

    public FastStartKit getfastStartKit(String fastStartKitId) {
        return this.getTestData(fastStartKitId);
    }


}
