package com.marketamerica.automation.testdatalists;

import java.util.Iterator;
import java.util.List;

import com.marketamerica.automation.testdata.Product;

/**
 * An product list represents a list of products
 *
 * @author Javier L. Velasquez
 */
public class ProductList extends AbstractDataList implements Iterable<Product> {

    @SuppressWarnings("unchecked")
    public List<Product> getProducts() {
        return (List<Product>) this.getTestDataList();
    }

    public Product getProduct(String id) {
        return (Product) super.getTestData(id);
    }

    @Override
    public Iterator<Product> iterator() {
        @SuppressWarnings("unchecked")
        Iterator<Product> product = (Iterator<Product>) this.testDataList.iterator();
        return product;
    }
}
