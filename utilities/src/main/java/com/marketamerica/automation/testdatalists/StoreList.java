package com.marketamerica.automation.testdatalists;

import java.util.Iterator;
import java.util.List;

import com.marketamerica.automation.testdata.Store;

/**
 * Created by javierv on 8/28/2014.
 */
public class StoreList extends AbstractDataList implements Iterable<Store> {

    @SuppressWarnings("unchecked")
    public List<Store> getStores() {
        return (List<Store>) this.getTestDataList();
    }

    public Store getStore(String id) {
        return (Store) super.getTestData(id);
    }

    @Override
    public Iterator<Store> iterator() {
        @SuppressWarnings("unchecked")
        Iterator<Store> Store = (Iterator<Store>) this.testDataList.iterator();
        return Store;
    }

}