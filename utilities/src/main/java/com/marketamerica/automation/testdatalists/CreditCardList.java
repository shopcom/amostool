package com.marketamerica.automation.testdatalists;

import java.util.Iterator;
import java.util.List;

import com.marketamerica.automation.testdata.CreditCard;


/**
 * An credit card list represents a list of credit cards
 *
 * @author Javier L. Velasquez
 */
public class CreditCardList extends AbstractDataList implements Iterable<CreditCard> {

    @SuppressWarnings("unchecked")
    public List<CreditCard> getCreditCards() {
        return (List<CreditCard>) this.getTestDataList();
    }

    public CreditCard getCreditCard(String id) {
        return (CreditCard) super.getTestData(id);
    }

    @Override
    public Iterator<CreditCard> iterator() {
        @SuppressWarnings("unchecked")
        Iterator<CreditCard> creditCard = (Iterator<CreditCard>) this.testDataList.iterator();
        return creditCard;
    }

}
