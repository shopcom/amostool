package com.marketamerica.automation.exception;

/**
 * This exception is thrown if we couldn't determine the Order Shipping Total.
 */
public final class OrderShippingNotCalculated extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public OrderShippingNotCalculated() {
        super("Could not calculate shipping for given scenario!");
    }
}
