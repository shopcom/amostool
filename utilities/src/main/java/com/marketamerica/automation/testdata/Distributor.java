package com.marketamerica.automation.testdata;

import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.EIGHT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.SEVEN_POINT_FIVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.SEVEN_POINT_FIVE_PERCENT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TEN;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TEN_PERCENT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TWELVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TWELVE_PERCENT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.formatBigDecimal;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.exception.OrderShippingNotCalculated;
import com.marketamerica.automation.testdata.attributes.DistributorType;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.DistributorPin;
import com.marketamerica.automation.utilities.enums.Gender;

/**
 * Created by javierv on 5/9/2014.
 */
@SuppressWarnings("serial")
@Table
@Entity
public class Distributor extends User implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected int persistenceId;

    private String originalPassword;
    private String distributorId;
    private String bdc;
    private String representativeId;
    private String representativePassword;
    private Gender gender;    
    
    @Transient
    private BigDecimal unfranchiseShippingPercent;
    @Transient
    private BigDecimal unfranchiseShippingPercentTrigger;
    @Transient
    private BigDecimal unfranchiseShippingFlatRate;

    private Date dateOfBirth;
    private String taxID;
    private boolean iTransact;
    private boolean pioneer;
    private boolean autoRenewal;

    @Enumerated(EnumType.ORDINAL)
    private DistributorType type;
    @Transient
    private ShippingCalculator shippingCalculator;
    @Enumerated(EnumType.ORDINAL)
    private DistributorPin pin;
    
    @OneToMany(fetch = FetchType.LAZY)
    private Collection<Portal> portals = new HashSet<>();    
    
    // Add for testing data retrieval system
    private boolean testingAccount;
    private Address mailingAddress;
    private Address shippingAddress;
    private String homePhone;
    @OneToOne(fetch = FetchType.LAZY)
    private Distributor placementDistributor;
    private String placementLeg;
    private boolean unified;   
    private boolean globalExpanded;    
    
    
    /**
     * Distributor Factory Method. Creates new instances of Distributor based on
     * the provided type. Defaults to Distributor
     *
     * @param type a type describing a distributor type.
     * @return a new instance of Distributor based on the provided Distributor
     * type
     */
    public static Distributor createDistributor(DistributorType type) {
        switch (type) {
            case HEALTH_PROFESSIONAL_DISTRIBUTOR:
            case RETAIL_AND_IMPLEMENTATION_ACCOUNT:
                final HealthProfessional healthProfessional = new HealthProfessional();
                healthProfessional.setType(type);
                return healthProfessional;
            case UNFRANCHISE_OWNER:
            case SALES_REPRESENTATIVE:
            default:
                final Distributor distributor = new Distributor();
                distributor.setType(type);
                return distributor;
        }
    }

    /**
     * Get the shipping calculator that provides shipping calculation functions
     *
     * @return a Shipping calculator object that provides calculations against
     * the current shopper
     */
    public ShippingCalculator getShippingCalculator() {
        if (shippingCalculator == null) {
            shippingCalculator = new ShippingCalculator();
        }
        return shippingCalculator;
    }

    /**
     * @return the representativeId
     */
    @Column(name = "repId")
    public String getRepresentativeId() {
        return representativeId;
    }

    /**
     * @param representativeId the representativeId to set
     */
    public void setRepresentativeId(String representativeId) {
        this.representativeId = representativeId;
    }

    /**
     * @return the representativePassword
     */
    public String getRepresentativePassword() {
        return representativePassword;
    }

    /**
     * @param representativePassword the representativePassword to set
     */
    public void setRepresentativePassword(String representativePassword) {
        this.representativePassword = representativePassword;
    }

    /**
     * @return the gender
     */
    public Gender getGender() {
        return gender;
    }

    /**
     * @param gender the gender to set
     */
    public void setGender(Gender gender) {
        this.gender = gender;
    }

    /**
     * @return bdc
     */
    public String getBdc() {
        return bdc;
    }

    /**
     * @param bdc set the distributor's business development center
     */
    public void setBdc(String bdc) {
        this.bdc = bdc;
    }

    /**
     * @return the dateOfBirth
     */
    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * @param dateOfBirth the dateOfBirth to set
     */
    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    /**
     * @return the taxID
     */
    public String getTaxID() {
        return taxID;
    }

    /**
     * @param taxID the taxID to set
     */
    public void setTaxID(String taxID) {
        this.taxID = taxID;
    }

    /**
     * @return the distributorId
     */
    public String getDistributorId() {
        return distributorId;
    }

    /**
     * @param distributorId the distributorId to set
     */
    public void setDistributorId(String distributorId) {
        this.distributorId = distributorId;
    }
    
    

    @Override
    public String toString() {
        if (getDistributorId() != null && getRepresentativeId() != null && getPassword() != null) {
            return String.format("Distributor Id : \'%s\' , Representative ID: \'%s\', Password: \'%s\'",
                    this.getDistributorId(), this.getRepresentativeId(), this.getPassword());
        } else if (getRepresentativeId() != null && getLastName() != null && getFirstName() != null) {
            return String.format("(Rep ID: %s) %s, %s", getRepresentativeId(), getLastName(), getFirstName());
        } else if (getRepresentativeId() != null && getLastName() == null && getFirstName() == null) {
            return String.format("Rep ID: %s", getRepresentativeId());
        } else if (getLastName() != null && getFirstName() != null) {
            return String.format("%s, %s", getLastName(), getFirstName());
        } else {
            return getClass().getName() + '@' + Integer.toHexString(hashCode());
        }
    }

    public boolean isITransact() {
        return iTransact;
    }

    public void setITransact(final boolean iTransact) {
        this.iTransact = iTransact;
    }

    public BigDecimal getUnfranchiseShippingPercent() {
        return unfranchiseShippingPercent;
    }

    public void setUnfranchiseShippingPercent(BigDecimal unfranchiseShippingPercent) {
        this.unfranchiseShippingPercent = unfranchiseShippingPercent;
    }

    public BigDecimal getUnfranchiseShippingPercentTrigger() {
        return unfranchiseShippingPercentTrigger;
    }

    public void setShippingPercentTrigger(BigDecimal unfranchiseShippingPercentTrigger) {
        this.unfranchiseShippingPercentTrigger = unfranchiseShippingPercentTrigger;
    }

    public BigDecimal getUnfranchiseShippingFlatRate() {
        return unfranchiseShippingFlatRate;
    }

    public void setUnfranchiseShippingFlatRate(BigDecimal unfranchiseShippingFlatRate) {
        this.unfranchiseShippingFlatRate = unfranchiseShippingFlatRate;
    }

    public DistributorType getType() {
        return type;
    }

    public void setType(DistributorType type) {
        this.type = type;
    }

    public String getOriginalPassword() {
        return originalPassword;
    }

    public void setOriginalPassword(String originalPassword) {
        this.originalPassword = originalPassword;
    }

    public boolean isPioneer() {
        return pioneer;
    }

    public void setPioneer(boolean pioneer) {
        this.pioneer = pioneer;
    }

    public DistributorPin getPin() {
        return pin;
    }

    public void setPin(DistributorPin pin) {
        this.pin = pin;
    }

    /**
     * @return {@code true} if the distributor is from an EMP country. {@code false} if the distributor is from a
     * non-EMP country.
     */
    public boolean isEMP() {
        return getAddress().getCountryAsConstant().getClassification().equals(Country.CountryClassification.EMP);
    }

    public boolean isAutoRenewal() {
        return autoRenewal;
    }

    public void setAutoRenewal(boolean autoRenewal) {
        this.autoRenewal = autoRenewal;
    }

    public Collection<Portal> getPortals() {
        return portals;
    }

    public void setPortals(Collection<Portal> portals) {
        this.portals = portals;
    }

    @OneToMany(fetch = FetchType.LAZY)
    public void addPortal(Portal portal) {
        this.portals.add(portal);
    }

    /**
     * Distributor Shipping Calculator
     */
    public class ShippingCalculator {

        public BigDecimal calculateShippingCost(Product product) {
            return formatBigDecimal(calculateShippingCost(Arrays.asList(product)));
        }

        public BigDecimal calculateShippingCost(Collection<Product> products) {
            final BigDecimal distributorCost = Product.calculateDistributorCost(products);
            switch (Configuration.getProject()) {
                case GLOBAL_SHOP:
                    final Country distributorCountry = Country.parse(getAddress().getCountry());
                    if (distributorCountry.isPersonalConsumption()) {
                        final BigDecimal calculation = formatBigDecimal(distributorCost.multiply(TWELVE_PERCENT));
                        if (calculation.compareTo(TWELVE) < 0) {
                            return TWELVE;
                        } else {
                            return formatBigDecimal(calculation);
                        }
                    } else {
                        BigDecimal retailCost = Product.calculateRetailCost(products);
                        switch (distributorCountry) {
                            case UNITED_STATES:
                                if (retailCost.compareTo(SEVEN_POINT_FIVE) > 0) {
                                    return formatBigDecimal(retailCost.multiply(SEVEN_POINT_FIVE_PERCENT));
                                } else {
                                    return SEVEN_POINT_FIVE;
                                }
                            case UNITED_KINGDOM:
                                return EIGHT;
                            case SPAIN:
                                return TEN;
                            case TAIWAN:
                                throw new OrderShippingNotCalculated();
                            case HONG_KONG:
                                throw new OrderShippingNotCalculated();
                            default:
                        /*
                         * Pricing for EMP, Non Market/EMP, Other
						 */
                                if (retailCost.compareTo(TWELVE) > 0) {
                                    return formatBigDecimal(retailCost.multiply(TEN_PERCENT));
                                } else {
                                    return TWELVE;
                                }
                        }
                    }
                default:
                    throw new OrderShippingNotCalculated();
            }
        }
    }
    
    /**
     * @return the mailing address
     */
    public Address getMailingAddress() {
        return mailingAddress;
    }

    /**
     * @param mailingAddress the mailing address to set
     */
    public void setMailingAddress(Address mailingAddress) {
        this.mailingAddress = mailingAddress;
    }
    
    /**
     * @return the shipping address
     */
    public Address getShippingAddress() {
        return shippingAddress;
    }

    /**
     * @param shippingAddress the shipping address to set
     */
    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }
    
    public String getHomePhone() {
        return homePhone;
    }
     
    public void setHomePhone(String phoneNumber) {
        this.homePhone = phoneNumber;
    }
    
    public Distributor getPlacement() {
        return placementDistributor;
    }
     
    public void setPlacementID(Distributor placementDistributor) {
        this.placementDistributor = placementDistributor;
    }
    
    public String getPlacementLeg() {
        return placementLeg;
    }
     
    public void setPlacementLeg(String placementLeg) {
        this.placementLeg = placementLeg;
    }
    
    // return true if IRC is being placed and qualified    
    public boolean isUnified() {
        return unified;
    }

    public void setUnified(boolean unified) {
        this.unified = unified;
    }
    
    // return true if all available region are expanded    
    public boolean isGlobalExpanded() {
        return globalExpanded;
    }

    public void setglobalExpanded(boolean globalExpanded) {
        this.globalExpanded = globalExpanded;
    }  
    
 // return true if the distributor is in EXCLUDE.MBRF file    
    public boolean isTestingAccount() {
        return testingAccount;
    }

    public void setTestingAccount(boolean testingAccount) {
        this.testingAccount = testingAccount;
    }         

}
