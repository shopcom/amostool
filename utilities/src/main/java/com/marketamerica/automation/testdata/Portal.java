package com.marketamerica.automation.testdata;

import java.math.BigDecimal;
import java.net.URL;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.marketamerica.automation.testdata.attributes.ApplicationCompletionDates;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.PortalType;

/**
 * Shoppers Portal TestData
 *
 * @author archanac
 */
@Table
@Entity
public class Portal extends AbstractData {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected int persistenceId;

    @Column(name="name")
    private String portalName;
    private String shopConsultantName;
    private URL fqdn;          // domain name
    private boolean freeShipping;
    @Transient
    private BigDecimal shippingCharge;
    @Transient
    private BigDecimal minimumShippingCharge;
    
    @Enumerated(EnumType.ORDINAL)
    private PortalType portalType;
    @Enumerated(EnumType.ORDINAL)
    private Country portalCountry;   
    
    // One distributor has multiple portal; and one portal belongs to a distributor with different BDCs. 
    // So here is many-to-many relationship
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Distributor distributor;

    public String getShopConsultantName() {
        return shopConsultantName;
    }

    public void setShopConsultantName(String shopConsultantName) {
        this.shopConsultantName = shopConsultantName;
    }

    public String getPortalName() {
        return portalName;
    }

    public void setPortalName(String name) {
        this.portalName = name;
    }

    @Override
    public String toString() {
        return String.format("Portal: \"%s\"", portalName);
    }

    public boolean isFreeShipping() {
        return freeShipping;
    }

    public void setFreeShipping(boolean freeShipping) {
        this.freeShipping = freeShipping;
    }

    public BigDecimal getShippingCharge() {
        return shippingCharge;
    }

    public void setShippingCharge(BigDecimal shippingCharge) {
        this.shippingCharge = shippingCharge;
    }

    public BigDecimal getMinimumShippingCharge() {
        return minimumShippingCharge;
    }

    public void setMinimumShippingCharge(BigDecimal minimumShippingCharge) {
        this.minimumShippingCharge = minimumShippingCharge;
    }

    public URL getFqdn() {
        return fqdn;
    }

    public void setFqdn(URL fqdn) {
        this.fqdn = fqdn;
    }
    
    public PortalType getPortalType() {
        return portalType;
    }

    public void setPortalType(PortalType portalType) {
        this.portalType = portalType;
    }     
    
    public Country getPortalCountry() {
        return portalCountry;
    }

    public void setPortalCountry(Country portalCountry) {
        this.portalCountry = portalCountry;
    }
    
}