package com.marketamerica.automation.testdata.address;

import com.marketamerica.automation.testdata.Address;

/**
 * Created by Javier L. Velasquez on 10/8/2014. Updated by kathyg on 07/02/2015
 */
public class MexicanAddress extends Address {

    //private String neighborhood;
    //private String district;

	// based on ma standard address format, mexican neighborhood is address3
    public String getNeighborhood() {
        //return neighborhood;
        return super.getStreet3();
    }

    // based on ma standard address format, mexican neighborhood is address3
    public void setNeighborhood(String neighborhood) {
        //this.neighborhood = neighborhood;
    	super.setStreet3(neighborhood);    	
    }
    
    // based on ma standard address format, mexican district is address4
    public String getDistrict() {
        //return district;
    	return super.getStreet4();
    }
    
    // based on ma standard address format, mexican district is address4
    public void setDistrict(String district) {
        //this.district = district;
    	super.setStreet4(district);
    }
}
