package com.marketamerica.automation.testdata;

import java.text.DateFormatSymbols;

public class CreditCard extends AbstractData {

    private String type;
    private String number;
    private int expirationMonth;
    private int expirationYear;
    private int cvv;

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @param number the number to set
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * @return last 4 digits of number
     */
    public String getLastFourDigitsOfNumber() {
        return number.substring(12);
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the expirationMonth
     */
    public int getExpirationMonth() {
        return expirationMonth;
    }

    /**
     * @param expirationMonth the expirationMonth to set
     */
    public void setExpirationMonth(int expirationMonth) {
        this.expirationMonth = expirationMonth;
    }
    
    public String getExpirationMonthString(){
    	return new DateFormatSymbols().getMonths()[expirationMonth-1];
    }
    
    public String getFirst3CharactersOfExpirationMonthString(){
    	String month =  new DateFormatSymbols().getMonths()[expirationMonth-1];
    	return month.substring(0, 3);
    }

    /**
     * @return the expirationYear
     */
    public int getExpirationYear() {
        return expirationYear;
    }

    /**
     * @param expirationYear the expirationYear to set
     */
    public void setExpirationYear(int expirationYear) {
        this.expirationYear = expirationYear;
    }

    /**
     * @return last 2 digits of expiration year
     */
    public String getLastTwoDigitsOfExpirationYear() {
        String year = String.valueOf(getExpirationYear());
        return year.substring(2);
    }

    /**
     * @return the cvv
     */
    public int getCvv() {
        return cvv;
    }

    /**
     * @param cvv the cvv to set
     */
    public void setCvv(int cvv) {
        this.cvv = cvv;
    }

}
