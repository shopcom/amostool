package com.marketamerica.automation.testdata.attributes;

/**
 * Created by Javier L. Velasquez on 9/3/2014.
 */
public enum DistributorApplicationType {
    INDIVIDUAL("Individual"), CORPORATION("Corporation"), PARTNERSHIP("Partnership");

    private String type;

    private DistributorApplicationType(String type) {
        this.type = type;
    }

    public static DistributorApplicationType parse(String input) {
        DistributorApplicationType[] types = values();
        for (DistributorApplicationType type : types) {
            if (type.toString().equals(input)) {
                return type;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return type;
    }
}
