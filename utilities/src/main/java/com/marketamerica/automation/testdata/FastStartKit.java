package com.marketamerica.automation.testdata;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Table
@Entity
@Inheritance(strategy= InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        name="discriminator",
        discriminatorType=DiscriminatorType.STRING
)
@DiscriminatorValue(value="FSK")
public class FastStartKit extends Merchandise {
    private int businessVolume;
    private boolean includesShipping;

    public int getBusinessVolume() {
        return businessVolume;
    }

    public void setBusinessVolume(int businessVolume) {
        this.businessVolume = businessVolume;
    }

    public boolean isIncludesShipping() {
        return includesShipping;
    }

    public void setIncludesShipping(boolean includesShipping) {
        this.includesShipping = includesShipping;
    }
}
