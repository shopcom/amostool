package com.marketamerica.automation.testdata;

import com.marketamerica.automation.testdata.attributes.StoreClassification;

/**
 * Created by javierv on 8/28/2014.
 */
public class Store extends AbstractData {
    public static final String DEFAULT_MARKET_AMERICA = "marketAmerica";
    private String name;
    private StoreClassification classification;

    public static Store getMarketAmerica() {
        Store store = new Store();
        store.setName("Market America");
        store.setClassification(StoreClassification.MA);
        return store;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StoreClassification getClassification() {
        return classification;
    }

    public void setClassification(StoreClassification classification) {
        this.classification = classification;
    }

}
