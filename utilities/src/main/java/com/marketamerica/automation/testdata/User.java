package com.marketamerica.automation.testdata;

import java.util.Locale;

import com.marketamerica.automation.utilities.enums.Country;
import org.apache.commons.lang.RandomStringUtils;

/**
 * Created by javierv on 5/9/2014.
 */
public class User extends AbstractData {

    protected String pcID;
    private String firstName;
    private String lastName;
    private String password;
    private String email;

    // This value is set via reflection. Setters/Getters are unnecessary.
    // Please ignore the IDE warning as the value is actually by the data
    // manager.
    // The getter/setter is removed because the user should never alter these
    // values
    private String addressKey;
    private Address address;
    private String phoneNumber;
    private Locale locale;

    public User() {
    }

    public void randomizeAttributes() {
        this.firstName = RandomStringUtils.randomAlphanumeric(6);
        this.lastName = "Automation";
        this.password = "testing";
        this.email = String.format("%s.%s@yopmail.com", firstName, lastName);
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the User's full name. The format returned is:
     * "${firstName} ${lastname}"
     */
    public String getFullName() {
        String fullName = "";
        if (firstName != null)
            fullName = fullName.concat(firstName + " ");
        if (lastName != null)
            fullName = fullName.concat(lastName);
        return fullName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(Address address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public boolean hasLastName() {
        if (lastName != null && !lastName.isEmpty()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        if (hasLastName()) {
            return String
                    .format("Last: \"%s\", First: \"%s\", ID: \"%s\", Address Key: \"%s\", Email address: \"%s\"",
                            getLastName(), getFirstName(), getId(), addressKey, email);
        } else {
            return String.format(
                    "First: \"%s\", ID: \"%s\", Address Key: \"%s\"",
                    getFirstName(), getId(), addressKey);
        }
    }

    public Locale getLocale() {
        return locale;
    }

    /**
     * Sets the user locale
     *
     * @param locale
     */
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public String getPcID() {
        return pcID;
    }

    public void setPcID(String pcID) {
        this.pcID = pcID;
    }

    public static String getCountryPhoneNumber(Country country) {

        switch (country) {
            case UNITED_STATES:
            case CANADA:
            case MEXICO:
            case TAIWAN:
                return "3335554848";
            case UNITED_KINGDOM:
                return "05555555";
            case SPAIN:
            case HONG_KONG:
                return "599999999";
            case AUSTRALIA:
            case SINGAPORE:
                return"65557878";
            default:
                break;
        }
        return null;
    }
}
