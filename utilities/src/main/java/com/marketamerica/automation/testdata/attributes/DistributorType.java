package com.marketamerica.automation.testdata.attributes;

/**
 * Enum for Distributor Entry Type
 * Created by Javier L. Velasquez on 10/1/2014.
 */
public enum DistributorType {
    UNFRANCHISE_OWNER("UnFranchise Owner", "UFO", "T"), SALES_REPRESENTATIVE("Sales Representative", "SR", "S"),
    HEALTH_PROFESSIONAL_DISTRIBUTOR("Health Professional Distributor", "HP", "LT"),
    RETAIL_AND_IMPLEMENTATION_ACCOUNT("Retail and Implementation Account", "HP1", "LO");

    private String type;
    private String abbreviation;
    private String code;

    private DistributorType(String type, String abbreviation, String code) {
        this.type = type;
        this.abbreviation = abbreviation;
        this.code = code;
    }

    public static DistributorType parse(String input) {
        DistributorType[] types = values();
        for (DistributorType type : types) {
            if (type.toString().equals(input) || type.getAbbreviation().equals(input)) {
                return type;
            }
        }
        return UNFRANCHISE_OWNER;
    }


    @Override
    public String toString() {
        return type;
    }

    public String getAbbreviation() {
        return abbreviation;
    }

    public String getCode() {
        return code;
    }
}
