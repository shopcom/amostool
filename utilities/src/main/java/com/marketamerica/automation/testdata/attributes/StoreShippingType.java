package com.marketamerica.automation.testdata.attributes;

/**
 * Created by javierv on 8/28/2014.
 */
public enum StoreShippingType {
    TOTAL_BASED, WEIGHT_BASED_ADDITIONAL, WEIGHT_BASED_TOTAL_BEYOND, PER_UNIT_BASED, PER_PRODUCT_SHIPPING;
}
