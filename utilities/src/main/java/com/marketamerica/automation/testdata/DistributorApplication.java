package com.marketamerica.automation.testdata;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.marketamerica.automation.testdata.attributes.ApplicationCompletionDates;
import com.marketamerica.automation.testdata.attributes.DistributorApplicationType;
import com.marketamerica.automation.testdata.attributes.DistributorType;
import com.marketamerica.automation.testdata.attributes.LanguageOptions;
import com.marketamerica.automation.utilities.enums.DistributorApplicationMethod;

/**
 * Holds the necessary data for completing a distributor application
 * Created by javierv on 11/10/2014.
 */
@Table
@Entity
public class DistributorApplication implements Serializable {

	private static final long serialVersionUID = -2697340671656861140L;

	@Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected int persistenceId;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private final Distributor distributor;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private final Distributor sponsoringDistributor;
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private ApplicationCompletionDates applicationCompletionDates = new ApplicationCompletionDates();
    @Enumerated(EnumType.ORDINAL)
    private LanguageOptions preferredLanguage;
    @Enumerated(EnumType.ORDINAL)
    private LanguageOptions subscriptionKitLanguage;

    @OneToMany(fetch = FetchType.LAZY)
    private Collection<Merchandise> merchandise = new HashSet<>();
    @OneToMany(fetch = FetchType.LAZY)
    private Collection<Product> products = new HashSet<>();
    @Enumerated(EnumType.ORDINAL)
    private DistributorApplicationType applicationType;
    @Enumerated(EnumType.ORDINAL)
    private DistributorApplicationMethod method;
    private boolean completeForm9251001 = true;
    private boolean completeTransferBuy = false;
    private boolean completeUFMS = true;
    private boolean completedApplication = false;
    private boolean sponsorPays = false;
    private boolean isPlaced = false;

    private String batchID;
    private boolean optedAutoRenewal = false;
    private int optInAutoRenewal = -1;
    private int optOutAutoRenewal = -1;
    private int gsw = -1;
    private int placeIRC = -1;
    private String ircRegion;
    private String ircPlacement;
    private String ircPlacementLeg;
    private int uamExpansion = -1;
    private String uamExpandedCountry;
    private String uamExpansionBDC;
    private int uapExpansion = -1;
    private String uapExpandedCountry;
    private String uapExpansionBDC;
    private int ueuExpansion = -1;
    private String ueuExpandedCountry;
    private String ueuExpansionBDC;
    private int setUpShopPortal = -1;
    private int setUpHPPortal = -1;
    private int customMiniUSA = -1;
    private int customMiniCAN = -1;
    private int customMiniMEX = -1;
    
    private boolean optedInFreeShipping;
    private boolean secondaryDistributor;
    private boolean IRCPlaced;
    private boolean IRCQualified;
    
    @OneToMany(fetch = FetchType.LAZY)
    private GlobalExpansion globalToOtherRegion;
    

    public DistributorApplication(DistributorType distributorType) {
        this.distributor = Distributor.createDistributor(distributorType);
        sponsoringDistributor = new Distributor();
        setMethod(DistributorApplicationMethod.PARTNER_NOW);
    }

    public DistributorApplication(Distributor sponsoringDistributor, DistributorType distributorType) {
        this.distributor = Distributor.createDistributor(distributorType);
        this.sponsoringDistributor = sponsoringDistributor;
        setMethod(DistributorApplicationMethod.PARTNER_NOW);
    }

    public DistributorApplicationType getApplicationType() {
        return applicationType;
    }

    public void setApplicationType(DistributorApplicationType applicationType) {
        this.applicationType = applicationType;
    }

    @Column(name="appDates")
    public ApplicationCompletionDates getApplicationCompletionDates() {
        return applicationCompletionDates;
    }

    public void setApplicationCompletionDates(ApplicationCompletionDates applicationCompletionDates) {
        this.applicationCompletionDates = applicationCompletionDates;
    }

    /**
     * @return the preferredLanguage
     */
    public LanguageOptions getPreferredLanguage() {
        return preferredLanguage;
    }

    /**
     * @param preferredLanguage the preferredLanguage to set
     */
    public void setPreferredLanguage(LanguageOptions preferredLanguage) {
        this.preferredLanguage = preferredLanguage;
    }

    /**
     * @return the subscriptionKitLanguage
     */
    public LanguageOptions getSubscriptionKitLanguage() {
        return subscriptionKitLanguage;
    }

    /**
     * @param subscriptionKitLanguage the subscriptionKitLanguage to set
     */
    public void setSubscriptionKitLanguage(LanguageOptions subscriptionKitLanguage) {
        this.subscriptionKitLanguage = subscriptionKitLanguage;
    }

    public Collection<Merchandise> getAllMerchandise() {
        return merchandise;
    }

    public void addMerchandise(Merchandise merchandise) {
        this.merchandise.add(merchandise);
    }

    public Merchandise getMerchandise() {
        return merchandise.iterator().next();
    }
    
    public void addProduct(Product product) {
        this.products.add(product);
    }

    public Product getProduct() {
        return products.iterator().next();
    }


    @Column(name="distributor")
    public Distributor getDistributor() {
        return distributor;
    }

    @Column(name="sponsor")
    public Distributor getSponsoringDistributor() {
        return sponsoringDistributor;
    }

    public boolean isHealthProfessional() {
        return distributor instanceof HealthProfessional;
    }

    public DistributorType getEntryType() {
        return distributor.getType();
    }

    public boolean hasCompletedApplication() {
        return completedApplication;
    }

    public void setCompletedApplication(boolean completedApplication) {
        this.completedApplication = completedApplication;
    }

    public boolean completedForm9251001() {
        return completeForm9251001;
    }

    public void setCompleteForm9251001(boolean completeForm9251001) {
        this.completeForm9251001 = completeForm9251001;
    }

    public boolean completedTransferBuy() {
        return completeTransferBuy;
    }

    public void setCompleteTransferBuy(boolean completeTransferBuy) {
        this.completeTransferBuy = completeTransferBuy;
    }

    public boolean completedUFMS() {
        return completeUFMS;
    }

    public void setCompleteUFMS(boolean completeUFMS) {
        this.completeUFMS = completeUFMS;
    }

    public DistributorApplicationMethod getMethod() {
        return method;
    }

    public void setMethod(DistributorApplicationMethod method) {
        this.method = method;
    }

    public boolean isPaidBySponsor() {
        return sponsorPays;
    }

    public void setSponsorPays(boolean sponsorPays) {
        this.sponsorPays = sponsorPays;
    }

    public int gswStepNumber() {
        return gsw;
    }

    public void setCompletedGSW(int gsw) {
        this.gsw = gsw;
    }

    public void setOptedAutoRenewal(boolean optedAutoRenewal) {
        this.optedAutoRenewal = optedAutoRenewal;
    }

    /**
     * Indicates if the distributor has selected an Auto Renewal Opt In/Out option.
     * @return {@code false} if the distributor has not selected an opt-in setting (in most cases this will mean the
     * distributor will see the auto renewal wizard upon login of UF, or {@code true} if the distributor has selected
     * a Opt In/Out option. See {@code DistributorApplication#isOptedInAutoRenewal}
     */
    public boolean isOptedAutoRenewal() {
        return optedAutoRenewal;
    }

    public void setOptInAutoRenewal(int optInAutoRenewal) {
        this.optInAutoRenewal = optInAutoRenewal;
    }

    /**
     * Indicates which Auto Renewal Opt In/Out option the distributor has selected.
     * @return int
     */
    public int getOptOutAutoRenewal() {
        return optOutAutoRenewal;
    }

    public void setOptOutAutoRenewal(int optOutAutoRenewal) {
        this.optOutAutoRenewal = optOutAutoRenewal;
    }

    /**
     * Indicates which Auto Renewal Opt In/Out option the distributor has selected.
     * @return {@code true} if the distributor has selected to opt-in {@code false} otherwise.
     */
    public int getOptInAutoRenewal() {
        return optInAutoRenewal;
    }
    
	public boolean isPlaced() {
		return isPlaced;
	}

	public void setPlaced(boolean isPlaced) {
		this.isPlaced = isPlaced;
	}
	
	/**
     * Indicates if the distributor has selected free shipping for his portals      
     */
    public boolean isOptedInFreeShipping() {
        return optedInFreeShipping;
    }

    public void setOptedInFreeShipping(boolean optedInFreeShipping) {
        this.optedInFreeShipping = optedInFreeShipping;
    }
    
    /**
     * Indicates if the distributor has secondary distributor on the application      
     */
    public boolean hasSecondaryDistributor() {
        return secondaryDistributor;
    }

    public void setSecondaryDistributor(boolean secondaryDistributor) {
        this.secondaryDistributor = secondaryDistributor;
    }
    
    public boolean isIRCPlaced() {
		return IRCPlaced;
	}

	public void setIRCPlaced(boolean IRCPlaced) {
		this.IRCPlaced = IRCPlaced;
	}
	
	public boolean isIRCQualified() {
		return IRCQualified;
	}

	public void setIRCQualified(boolean IRCQualified) {
		this.IRCQualified = IRCQualified;
	}
	
	public GlobalExpansion getglobalToOtherRegion () {
		return globalToOtherRegion;
	}

	public void setglobalToOtherRegion(GlobalExpansion globalToOtherRegion) {
		this.globalToOtherRegion = globalToOtherRegion;
	}

    public int getPlaceIRC() {
        return placeIRC;
    }

    public void setPlaceIRC(int placeIRC) {
        this.placeIRC = placeIRC;
    }

    public String getIrcRegion() {
        return ircRegion;
    }

    public void setIrcRegion(String ircRegion) {
        this.ircRegion = ircRegion;
    }

    public String getIrcPlacement() {
        return ircPlacement;
    }

    public void setIrcPlacement(String ircPlacement) {
        this.ircPlacement = ircPlacement;
    }

    public String getIrcPlacementLeg() {
        return ircPlacementLeg;
    }

    public void setIrcPlacementLeg(String ircPlacementLeg) {
        this.ircPlacementLeg = ircPlacementLeg;
    }

    public int getUamExpansion() {
        return uamExpansion;
    }

    public void setUamExpansion(int uamExpansion) {
        this.uamExpansion = uamExpansion;
    }

    public String getUamExpandedCountry() {
        return uamExpandedCountry;
    }

    public void setUamExpandedCountry(String uamExpandedCountry) {
        this.uamExpandedCountry = uamExpandedCountry;
    }

    public String getUamExpansionBDC() {
        return uamExpansionBDC;
    }

    public void setUamExpansionBDC(String uamExpansionBDC) {
        this.uamExpansionBDC = uamExpansionBDC;
    }

    public int getUapExpansion() {
        return uapExpansion;
    }

    public void setUapExpansion(int uapExpansion) {
        this.uapExpansion = uapExpansion;
    }

    public String getUapExpandedCountry() {
        return uapExpandedCountry;
    }

    public void setUapExpandedCountry(String uapExpandedCountry) {
        this.uapExpandedCountry = uapExpandedCountry;
    }

    public String getUapExpansionBDC() {
        return uapExpansionBDC;
    }

    public void setUapExpansionBDC(String uapExpansionBDC) {
        this.uapExpansionBDC = uapExpansionBDC;
    }

    public int getUeuExpansion() {
        return ueuExpansion;
    }

    public void setUeuExpansion(int ueuExpansion) {
        this.ueuExpansion = ueuExpansion;
    }

    public String getUeuExpandedCountry() {
        return ueuExpandedCountry;
    }

    public void setUeuExpandedCountry(String ueuExpandedCountry) {
        this.ueuExpandedCountry = ueuExpandedCountry;
    }

    public String getUeuExpansionBDC() {
        return ueuExpansionBDC;
    }

    public void setUeuExpansionBDC(String ueuExpansionBDC) {
        this.ueuExpansionBDC = ueuExpansionBDC;
    }

    public int getSetUpShopPortal() {
        return setUpShopPortal;
    }

    public void setSetUpShopPortal(int setUpShopPortal) {
        this.setUpShopPortal = setUpShopPortal;
    }

    public int getSetUpHPPortal() {
        return setUpHPPortal;
    }

    public void setSetUpHPPortal(int setUpHPPortal) {
        this.setUpHPPortal = setUpHPPortal;
    }

    public int getCustomMiniUSA() {
        return customMiniUSA;
    }

    public void setCustomMiniUSA(int customMiniUSA) {
        this.customMiniUSA = customMiniUSA;
    }

    public int getCustomMiniCAN() {
        return customMiniCAN;
    }

    public void setCustomMiniCAN(int customMiniCAN) {
        this.customMiniCAN = customMiniCAN;
    }

    public int getCustomMiniMEX() {
        return customMiniMEX;
    }

    public void setCustomMiniMEX(int customMiniMEX) {
        this.customMiniMEX = customMiniMEX;
    }

    public String getBatchID() {
        return batchID;
    }

    public void setBatchID(String batchID) {
        this.batchID = batchID;
    }
}
