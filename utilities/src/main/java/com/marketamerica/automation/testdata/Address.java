package com.marketamerica.automation.testdata;

import com.marketamerica.automation.testdata.address.HongKongAddress;
import com.marketamerica.automation.testdata.address.MexicanAddress;
import com.marketamerica.automation.utilities.enums.Country;

/**
 * Update by kathyg on 07/02/2015.
 */

public class Address extends AbstractData {

    private String street; //For Address Line 1
    private String street2; //For Address Line 2
    private String street3; // This is also the Flat/Unit field for HKG address, and Neighborhood for MEX address
    private String street4; // This is the Floor field for HKG address, and District for MEX address
    private String street5; // This is the Building field for HKG address
    private String street6; // This is the Estate field for HKG address
    private String street7; // This is the Block field for HKG address
    private String city;
    private String state;
    private String stateAbbreviation;
    private String country;
    private String zipCode;
    private boolean residential;
    
    private String addressId;
    private String specialArea;

    public static Address createAddress(Country country) {
        if (country == null) {
            return new Address();
        }
        switch (country) {
            case HONG_KONG:
                return new HongKongAddress();
            case MEXICO:
                return new MexicanAddress();
            case AUSTRALIA:
            case CANADA:
            case CHINA:
            case TAIWAN:
            case UNITED_KINGDOM:
            case UNITED_STATES:
            case SPAIN:          
            case SINGAPORE:
            case NOT_CLASSIFIED:
            case EMP:
            default:
                return new Address();
        }
    }

    public boolean isLower48States() {
        return !(state.equalsIgnoreCase("hawaii") || state.equalsIgnoreCase("alaska"));
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * @return the street
     */
    public String getStreet2() {
        return street2;
    }

    /**
     * @param street the street to set
     */
    public void setStreet2(String street2) {
        this.street2 = street2;
    }
    
    public String getStreet3() {
        return street3;
    }

    /**
     * @param street the street to set
     */
    public void setStreet3(String street3) {
        this.street3 = street3;
    }
    
    public String getStreet4() {
        return street4;
    }

    /**
     * @param street the street to set
     */
    public void setStreet4(String street4) {
        this.street4 = street4;
    }
    
    public String getStreet5() {
        return street5;
    }

    /**
     * @param street the street to set
     */
    public void setStreet5(String street5) {
        this.street5 = street5;
    }
    
    public String getStreet6() {
        return street6;
    }

    /**
     * @param street the street to set
     */
    public void setStreet6(String street6) {
        this.street6 = street6;
    }
    
    public String getStreet7() {
        return street7;
    }

    /**
     * @param street the street to set
     */
    public void setStreet7(String street7) {
        this.street7 = street7;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the state abbreviation
     */
    public String getStateAbbreviation() {
        return stateAbbreviation;
    }

    /**
     * @param stateAbbreviation the state abbreviation to set
     */
    public void setStateAbbreviation(final String stateAbbreviation) {
        this.stateAbbreviation = stateAbbreviation;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(final String country) {
        this.country = country;
    }

    public Country getCountryAsConstant() {
        return Country.parse(country);
    }

    public void setCountry(final Country country) {
        this.country = country.toString();
    }

    /**
     * @return the zip code
     */
    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    @Override
    public String toString() {
        return String.format("ID: '%s', Street: '%s', State: '%s', Country: '%s', Zip: '%s'", getId(), this.getStreet(), this.getState(), this.getCountry(), this.getZipCode());
    }

    public boolean isResidential() {
        return residential;
    }

    public void setResidential(boolean residential) {
        this.residential = residential;
    }
    
    public String getAddressId() {
        return addressId;
    }

    public void setAddressId(String addressId) {
        this.addressId = addressId;
    }
    
    public String getspecialArea() {
        return specialArea;
    }

    public void setspecialArea(String specialArea) {
        this.specialArea = specialArea;
    }
}
