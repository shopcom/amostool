package com.marketamerica.automation.testdata;

import java.util.List;

import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.CustomMiniDomain;
import com.marketamerica.automation.utilities.enums.CustomMiniLayout;
import com.marketamerica.automation.utilities.enums.CustomMiniProductCategories;

public class CustomMiniWebSite extends Portal {

	private String productFamily;
	private String webAddress;
	private String startDate;
	private String ExpirationDate;
	private String email;
	private String addressKey;
	private String distributorKey;
	private Distributor distributor;
	private Address address;
	private String phoneNumber;
	private CustomMiniLayout layout;
	private List<CustomMiniProductCategories> productCategories;
	private Country country;
	private CustomMiniDomain domain;

	public String getProductFamily() {
		return productFamily;
	}

	public void setProductFamily(String productFamily) {
		this.productFamily = productFamily;
	}

	public String getWebAddress() {
		return webAddress;
	}

	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getExpirationDate() {
		return ExpirationDate;
	}

	public void setExpirationDate(String expirationDate) {
		ExpirationDate = expirationDate;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddressKey() {
		return addressKey;
	}

	public void setAddressKey(String addressKey) {
		this.addressKey = addressKey;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public CustomMiniLayout getLayout() {
		return layout;
	}

	public void setLayout(CustomMiniLayout layout) {
		this.layout = layout;
	}

	public List<CustomMiniProductCategories> getProductCategories() {
		return productCategories;
	}

	public void setProductCategories(List<CustomMiniProductCategories> productCategories) {
		this.productCategories = productCategories;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}

	public CustomMiniDomain getDomain() {
		return domain;
	}

	public void setDomain(CustomMiniDomain domain) {
		this.domain = domain;
	}

	public String getDistributorKey() {
		return distributorKey;
	}

	public void setDistributorKey(String distributorKey) {
		this.distributorKey = distributorKey;
	}

	public Distributor getDistributor() {
		return distributor;
	}

	public void setDistributor(Distributor distributor) {
		this.distributor = distributor;
	}
}
