package com.marketamerica.automation.testdata;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.marketamerica.automation.testdata.attributes.StoreClassification;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.ProductCategory;

@Table
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        name="discriminator",
        discriminatorType=DiscriminatorType.STRING
)
@DiscriminatorValue(value="P")
public class Product extends Merchandise {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected int persistenceId;

    private String productId;
    private String productContainerId;
    private String altProductContainerId;
    private BigDecimal distributorCost;
    private BigDecimal cashBack;
    private String color;
    private double weight;
    private boolean freight;
    @Transient
    private String storeId;
    private BigDecimal productBV;
    private BigDecimal productIBV;
    @Transient
    private Store store;
    @Transient
    private ProductCategory productCategory;
    @Enumerated(EnumType.ORDINAL)
    private Country productCountry;  // Product unit price currency country

    /**
     * Calculates the total distributor cost of a list of products
     *
     * @param products a list of products who have defined distributor costs
     * @return a big decimal representing the list of product's total distributor cost
     */
    public static BigDecimal calculateDistributorCost(Collection<Product> products) {
        return products.stream().filter(
                product -> product.getDistributorCost() != null && product.isFreight()
        ).map(Product::getDistributorCost).reduce(
                BigDecimal.ZERO, BigDecimal::add
        );
    }

    /**
     * Calculate the total weight from a list of products (in ounces)
     *
     * @param products a list of products
     * @return a value presenting the total weight of a list of products in ounces
     */
    public static double calculateTotalWeight(Collection<Product> products) {
        return products.stream().mapToDouble(Product::getWeight).reduce(0, (accumulator, x) -> accumulator + x);
    }

    /**
     * Checks if a list of products contain a non-ma product
     *
     * @param products a list of products
     * @return true/false indicating if a product in the list is a non-ma product
     */
    public static boolean hasVendorProduct(final Collection<Product> products) {
        return products.stream().anyMatch(product -> !product.isMarketAmericaProduct());
    }

    public static BigDecimal calculateRetailCost(Collection<Product> products) {
        return products.stream().filter(product -> product.getRetailCost() != null).
                map(Product::getRetailCost).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    /**
     * Calculates the retail price for the product multiplied by the defined quantity.
     *
     * @param product  a product
     * @param quantity the number of times to multiply the given product
     */
    public static BigDecimal calculateRetailCost(Product product, int quantity) {
        Collection<Product> products = new ArrayList<>();
        for (int i = 0; i < quantity; i++) {
            products.add(product);
        }
        return calculateRetailCost(products);
    }

    /**
     * Takes a list of products and split them by store
     *
     * @param products a collection of products
     * @return
     */
    public static Map<Store, List<Product>> getProductsByStore(List<Product> products) {
        return products.stream().collect(Collectors.groupingBy(Product::getStore));
    }

    /**
     * Get the product base country by ship to country
     *
     * @param ship to country
     * @return product base country
     */
    public static Country getProductCountry(Country shipToCountry) {
    	switch (shipToCountry) {
        //switch (shipToCountry.replaceAll(" ", "").toUpperCase()) {
            case NEW_ZEALAND:
            case MALAYSIA:
            case COLOMBIA:	
            case RUSSIAN_FEDERATION:            	
                return Country.UNITED_STATES;
            case JERSEY:
                return Country.UNITED_KINGDOM;
            case NETHERLANDS:
            case DENMARK:
                return Country.SPAIN;
            case CHINA:                        		 
                return Country.HONG_KONG;
            default:
                break;
        }
        return Country.UNITED_STATES;
    }

    /**
     * @return the product's weight (in ounces)
     */
    public double getWeight() {
        return weight;
    }

    /**
     * Set the product's weight (in ounces)
     *
     * @param weight a double value that represents the product's weight in ounces.
     */
    public void setWeight(double weight) {
        this.weight = weight;
    }

    public BigDecimal getCashBack() {
        return cashBack;
    }

    public void setCashBack(BigDecimal cashBack) {
        this.cashBack = cashBack;
    }

    public void setCashBack(String cashBack) {
        this.cashBack = BigDecimal.valueOf(Double.valueOf(cashBack));
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public BigDecimal getDistributorCost() {
        return distributorCost;
    }

    public void setDistributorCost(String distributionCost) {
        this.distributorCost = BigDecimal.valueOf(Double.valueOf(distributionCost));
    }

    public void setDistributorCost(BigDecimal distributorCost) {
        this.distributorCost = distributorCost;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductContainerId() {
        return productContainerId;
    }

    public void setProductContainerId(String productContainerId) {
        this.productContainerId = productContainerId;
    }
    
    public String getAltproductContainerId() {
        return altProductContainerId;
    }

    public void setAltproductContainerId(String altProductContainerId) {
        this.altProductContainerId = altProductContainerId;
    }

    @Override
    public String toString() {
        return String.format(
                "Name: \"%s\", ID: \"%s\", SKU: \"%s\", Retail Cost: \"%s\"",
                getName(), this.productId, getSku(), getRetailCost()
        );
    }

    public boolean isFreight() {
        return freight;
    }

    public void setFreight(boolean freight) {
        this.freight = freight;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public boolean isMarketAmericaProduct() {
        return this.store == null || this.store.getClassification().equals(StoreClassification.MA);
    }

    public Store getStore() {
        return store;
    }

    public void setStore(Store store) {
        this.store = store;
    }

    public BigDecimal getProductBV() {
        return productBV;
    }

    public void setProductBV(BigDecimal productBV) {
        this.productBV = productBV;
    }

    public BigDecimal getProductBVByQty(int quantity) {
        return productBV.multiply(BigDecimal.valueOf(quantity), new MathContext(4));
    }

    public BigDecimal getProductIBV() {
        return productIBV;
    }

    public void setProductIBV(BigDecimal productIBV) {
        this.productIBV = productIBV;
    }

    public BigDecimal getProductIBVByQty(int quantity) {
        return productIBV.multiply(BigDecimal.valueOf(quantity), new MathContext(4));
    }

    public Country getProductCountry() {
        return productCountry;
    }

    public void setProductCountry(Country productCountry) {
        this.productCountry = productCountry;
    }
    
    public ProductCategory getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(ProductCategory productCategory) {
        this.productCategory = productCategory;
    }

}
