package com.marketamerica.automation.testdata;

import java.util.Arrays;

/**
 * Created by Javier L. Velasquez on 10/24/2014.
 */
public class TestLinkID {
    private String[] externalIds;
    private int version;

    public TestLinkID(String[] externalIds, int version) {
        this.externalIds = externalIds;
        this.version = version;
    }

    public TestLinkID(String[] externalIds) {
        this(externalIds, 1);
    }

    public TestLinkID(String externalId) {
        this(externalId.split(", "), 1);
    }

    public TestLinkID() {

    }

    public String[] getExternalIds() {
        return externalIds;
    }     

    public void setExternalIds(String[] externalIds) {
        this.externalIds = externalIds;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    @Override
    public String toString() {
        if (externalIds == null) {
            return super.toString();
        } else {
            return String.format("TestLink ID: %s, v%s", Arrays.asList(externalIds), version);
        }
    }

}
