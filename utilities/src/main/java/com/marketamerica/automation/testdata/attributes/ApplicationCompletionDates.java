package com.marketamerica.automation.testdata.attributes;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by javierv on 11/5/2014.
 */
@Table
@Entity
public class ApplicationCompletionDates {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected int persistenceId;

    private Date form925SignedDate;
    private Date form1001EntryDate;
    private Date tenDayRequirementDate;


    public Date getForm925SignedDate() {
        return form925SignedDate;
    }

    public void setForm925SignedDate(Date form925SignedDate) {
        this.form925SignedDate = form925SignedDate;
    }

    public Date getForm1001EntryDate() {
        return form1001EntryDate;
    }

    public void setForm1001EntryDate(Date form1001EntryDate) {
        this.form1001EntryDate = form1001EntryDate;
    }

    public Date getTenDayRequirementDate() {
        return tenDayRequirementDate;
    }

    public void setTenDayRequirementDate(Date tenDayRequirementDate) {
        this.tenDayRequirementDate = tenDayRequirementDate;
    }
}
