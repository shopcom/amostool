package com.marketamerica.automation.testdata.address;

import com.marketamerica.automation.testdata.Address;
import com.marketamerica.automation.testdata.attributes.HongKongShippingRegion;

/**
 * Created by javierv on 8/17/2014. Updated by kathyg on 07/02/2015.
 */
public class HongKongAddress extends Address {

    private HongKongShippingRegion region;
    private String district;
    private String area;
    //private String flat;
    //private String floor;
    //private String building;
    //private String block;
    //private String estate;
    

    public HongKongShippingRegion getRegion() {
        return region;
    }

    public void setRegion(HongKongShippingRegion region) {
        this.region = region;
    }

    public String getFlat() {
        //return flat;
    	return super.getStreet3();
    }

    public void setFlat(String flat) {
        //this.flat = flat;
    	super.setStreet3(flat);
    }

    public String getFloor() {
        //return floor;
    	return super.getStreet4();
    }

    public void setFloor(String floor) {
        //this.floor = floor;
    	super.setStreet4(floor);
    }

    public String getBuilding() {
        //return building;
    	return super.getStreet5();
    }

    public void setBuilding(String building) {
        //this.building = building;
    	super.setStreet5(building);
    }

    public String getBlock() {
        //return block;
    	return super.getStreet7();
    }

    public void setBlock(String block) {
        //this.block = block;
    	super.setStreet7(block);
    }

    public String getEstate() {
        //return estate;
    	return super.getStreet6();
    }

    public void setEstate(String estate) {
        //this.estate = estate;
    	super.setStreet6(estate);
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
    
    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }
}
