package com.marketamerica.automation.testdata.attributes;

/**
 * Created by javierv on 8/28/2014.
 */
public enum StoreClassification {
    MOR("Merchant of Record", "MOR"), EZ("One Cart Products", "EZ"), MA("Market America", "MA");
    private String storeClassification;
    private String abbreviation;

    private StoreClassification(String storeClassification, String abbreviation) {
        this.storeClassification = storeClassification;
        this.abbreviation = abbreviation;
    }

    public static StoreClassification parseAbbrevication(String classification) {
        final StoreClassification[] values = values();

        for (StoreClassification storeClassification : values) {
            if (storeClassification.getAbbreviation().equals(classification)) {
                return storeClassification;
            }
        }
        return MA;
    }

    @Override
    public String toString() {
        return storeClassification;
    }

    public String getAbbreviation() {
        return abbreviation;
    }
}
