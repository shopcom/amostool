package com.marketamerica.automation.testdata;

import java.util.Date;

import com.marketamerica.automation.testdata.attributes.DistributorType;

/**
 * Created by Javier L. Velasquez on 10/1/2014.
 */
public class HealthProfessional extends Distributor {
	private static final long serialVersionUID = -4934826584378189036L;
	private String license;
    private String secondaryLicense;
    private String specialty;
    private String secondarySpeciality;
    private Date licenseVerifyDate;

    public String getLicense() {
        return license;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public String getSecondaryLicense() {
        return secondaryLicense;
    }

    public void setSecondaryLicense(String secondaryLicense) {
        this.secondaryLicense = secondaryLicense;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getSecondarySpeciality() {
        return secondarySpeciality;
    }

    public void setSecondarySpeciality(String secondarySpeciality) {
        this.secondarySpeciality = secondarySpeciality;
    }

    @Override
    public DistributorType getType() {
        if (super.getType().equals(DistributorType.UNFRANCHISE_OWNER) || super.getType().equals(DistributorType.SALES_REPRESENTATIVE)) {
            throw new IllegalStateException(String.format("A Health Professional Instance (\"Dist Id: %s\") can't be classified as a UFO or SR", this.getDistributorId()));
        }
        return super.getType();
    }

    public Date getLicenseVerifyDate() {
        return licenseVerifyDate;
    }

    public void setLicenseVerifyDate(Date licenseVerifyDate) {
        this.licenseVerifyDate = licenseVerifyDate;
    }
}
