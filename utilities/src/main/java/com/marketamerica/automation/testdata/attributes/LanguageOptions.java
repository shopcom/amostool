package com.marketamerica.automation.testdata.attributes;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Archana Chalapalle on 04/13/2015
 */
public enum LanguageOptions {
		SPANISH("SPA", "Spanish", Arrays.asList("Espanol")), 
		ENGLISH("ENG","English", Arrays.asList("Ingles")), 
		FRENCH("FRE", "French"), 
		CHINESE("CHI", "Chinese");

	private String langCode;
	private String language;
	// Other translated languages options
	private List<String> otherLanguage;

	private LanguageOptions(final String langCode, final String language,
			List<String> otherLanguage) {
		this.langCode = langCode;
		this.language = language;
		this.otherLanguage = otherLanguage;
	}

	private LanguageOptions(final String langCode, final String language) {
		this.langCode = langCode;
		this.language = language;
	}

	/**
	 * Resolve a lang Option via language
	 *
	 * @param language
	 *            a string that represents a delivery type
	 */
	public static LanguageOptions parse(String language) {
		for (final LanguageOptions currentType : values()) {
			if (currentType.getLanguage().equalsIgnoreCase(language.trim())) {
				return currentType;
			}
			final List<String> inOtherTypes = currentType.otherLanguage;
			if (inOtherTypes != null) {
				for (String variation : inOtherTypes) {
					if (variation.equalsIgnoreCase(language)) {
						return currentType;
					}
				}
			}
		}
		return null;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
	public String getLangCode() {
		return langCode;
	}

	public void setLangCode(String langCode) {
		this.langCode = langCode;
	}

	@Override
	public String toString() {
		return langCode;
	}
}
