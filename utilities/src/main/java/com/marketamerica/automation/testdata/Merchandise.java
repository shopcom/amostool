package com.marketamerica.automation.testdata;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

/**
 * Created by Javier L. Velasquez on 10/6/2014.
 */
@Table
@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
        name="discriminator",
        discriminatorType=DiscriminatorType.STRING
)
@DiscriminatorValue(value="M")

public abstract class Merchandise extends AbstractData {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected int persistenceId;

    private String sku;
    private String name;
    private BigDecimal retailCost;

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getRetailCost(int quantity) {
        //return retailCost.multiply(BigDecimal.valueOf(quantity), new MathContext(4));
        return retailCost.multiply(BigDecimal.valueOf(quantity));
    }

    public BigDecimal getRetailCost() {
        return retailCost;
    }

    public void setRetailCost(String retailCost) {
        this.retailCost = BigDecimal.valueOf(Double.valueOf(retailCost));
    }

    public void setRetailCost(BigDecimal retailCost) {
        this.retailCost = retailCost;
    }


}
