package com.marketamerica.automation.testdata.attributes;

/**
 * This enum represents the potential Reduced Shipping options
 */
public enum ReducedShipping {
    MAIL_INNOVATION("Mail Innovations", "USPMI"), SURE_POST("SurePost", "UPSSP");
    private final String shippingMethod;
    private final String abbreviation;

    ReducedShipping(final String shippingMethod, final String abbreviation) {
        this.shippingMethod = shippingMethod;
        this.abbreviation = abbreviation;
    }


    /**
     * Return the ReducedShipping type's abbreviation;
     *
     * @return a string representing a ReducedShipping type as an abbreviation
     */
    public String getAbbreviation() {
        return abbreviation;
    }

    @Override
    public String toString() {
        return shippingMethod;
    }
}
