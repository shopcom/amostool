package com.marketamerica.automation.testdata.attributes;

/**
 * Created by javierv on 8/27/2014.
 */
public enum ShopperClassification {
    PREFERRED_CUSTOMER("Preferred Customer", "PC"),
    MOTIVES_PRO_ARTIST("Motives Pro Artist", "MPA"),
    MOTIVES_AFFILIATE_ASSOCIATE("Motives Affiliate Associate", "MAA"),
    MOTIVES_TEEN_ASSOCIATE("Motives Teen Associate", "MTA"),
    PERSONAL_PREFERRED_CUSTOMER("Personal Preferred Customer", "PPC"),
    MOTIVES_PRO_ARTIST_CUSTOMER("Motives Pro Artist Customer", "MPAPC"),
    MOTIVES_AFFILIATE_ASSOCIATE_CUSTOMER("Motives Affiliate Associate Customer", "MAAPC"),
    MOTIVES_TEEN_ASSOCIATE_CUSTOMER("Motives Teen Associate Customer", "MTAPC");

    private final String classification;
    private final String abbreviation;

    ShopperClassification(String classification, String abbreviation) {
        this.classification = classification;
        this.abbreviation = abbreviation;
    }

    public static ShopperClassification parse(String value) {
        ShopperClassification[] values = values();
        for (ShopperClassification shopperClassification : values) {
            if (shopperClassification.toString().equalsIgnoreCase(value) ||
                    shopperClassification.getAbbreviation().equalsIgnoreCase(value)) {
                return shopperClassification;
            }
        }
        return PREFERRED_CUSTOMER;
    }

    /**
     * Return the Shopper classification abbreviation;
     *
     * @return a string representing a shopper classification as an abbreviation
     */
    public String getAbbreviation() {
        return abbreviation;
    }

    @Override
    public String toString() {
        return classification;
    }
}
