package com.marketamerica.automation.testdata;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Region;

/**
 * Distributor global expansion TestData
 *
 * @author kathyg
 */
@Table
@Entity
public class GlobalExpansion extends AbstractData {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected int persistenceId;

    @Column(name="name")
    private Region globalToRegion;
    private Country globalToCountry;
    private String globalBDC;   
    private boolean expanded;

    public Region getGlobalToRegion() {
        return globalToRegion;
    }

    public void setGlobalToRegion(Region globalToRegion) {
        this.globalToRegion = globalToRegion;
    }
    
    public Country getGlobalToCountry() {
        return globalToCountry;
    }

    public void setGlobalToCountry(Country globalToCountry) {
        this.globalToCountry = globalToCountry;
    }
    
    public String getGloablBDC() {
        return globalBDC;
    }

    public void setGlobalBDC(String globalBDC) {
        this.globalBDC = globalBDC ;
    }     
    
    public Boolean getExpanded() {
        return expanded;
    }

    public void setExpanded(Boolean expanded) {
        this.expanded = expanded;
    }
     

    @Override
    public String toString() {
        return String.format("Global expansion region: \"%s\"", globalToRegion.toString());
    }
   
}