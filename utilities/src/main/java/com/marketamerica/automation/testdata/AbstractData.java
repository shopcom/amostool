package com.marketamerica.automation.testdata;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public abstract class AbstractData {
    protected final static Logger logger = LogManager
            .getLogger(AbstractData.class);

    private String id;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object object) {
        if (this == object) {
            return true;
        } else if (object instanceof AbstractData) {
            if (this.getId() == null) {
                return false;
            }
            if (this.getId().equals(((AbstractData) object).getId())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

}
