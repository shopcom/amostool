package com.marketamerica.automation.testdata;

import static com.marketamerica.automation.testdata.Product.calculateDistributorCost;
import static com.marketamerica.automation.testdata.Product.hasVendorProduct;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.EIGHT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.ELEVEN_PERCENT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.ELEVEN_POINT_SEVEN_SEVEN;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FIFTEEN;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FIFTY;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FIVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FIVE_POINT_THREE_FIVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FIVE_THOUSAND_SIX_HUNDRED;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FORTEEN;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FORTEEN_PERCENT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FOUR_HUNDRED_FIFTY;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FOUR_THOUSAND;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FREE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.NINETY_NINE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.NINE_PERCENT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.NINE_POINT_FIVE_PERCENT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.ONE_HUNDRED;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.ONE_HUNDRED_FIFTY;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.ONE_HUNDRED_FORTY;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.ONE_HUNDRED_TWENTY_FIVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.ONE_THOUSAND;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.ONE_THOUSAND_FIVE_HUNDRED;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.SEVEN_POINT_FIVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.SEVEN_POINT_FIVE_PERCENT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.SIX;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.SIX_PERCENT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.SIX_POINT_FOUR_TWO;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.SIX_THOUSAND;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TEN;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TEN_PERCENT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TEN_POINT_FIVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.THREE_HUNDRED_FIFTY;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.THREE_HUNDRED_TWENTY;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TWELVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TWELVE_PERCENT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TWENTY_FIVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TWO_HUNDRED;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TWO_HUNDRED_SEVENTY;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.formatBigDecimal;
import static java.math.BigDecimal.ZERO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.exception.OrderShippingNotCalculated;
import com.marketamerica.automation.testdata.address.HongKongAddress;
import com.marketamerica.automation.testdata.attributes.HongKongShippingRegion;
import com.marketamerica.automation.testdata.attributes.ReducedShipping;
import com.marketamerica.automation.testdata.attributes.ShopperClassification;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Region;
import com.marketamerica.automation.utilities.enums.DeliveryType;
import com.marketamerica.automation.utilities.enums.HongKongPickupLocation;
import com.marketamerica.automation.utilities.enums.Project;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;

@SuppressWarnings("incomplete-switch")
//TODO: Remove this supress warnings when warnings with the switch statements are resolved.
public class Shopper extends User {

    private static final int MAIL_INNOVATIONS_MAX_WEIGHT_OUNCES = 15;
    private static final int SURE_POST_MAX_WEIGHT_OUNCES = 144;
    // This keys are set via reflection. Setters/Getters are unnecessary
    // Please ignore the IDE warning as the value is actually by the data manager.
    // The getter/setter is removed because the user should never alter these values
    @SuppressWarnings("unused")
	private String portalKey;
    @SuppressWarnings("unused")
	private String distributorKey;
    @SuppressWarnings("unused")
    private String webSiteKey;
    private String shopperID;
    private Portal portal;
    private Distributor distributor;
    private CustomMiniWebSite webSite;
    private ShopperClassification referralShopperClassification;
    private ShopperClassification classification;
    private ShippingCalculator shippingCalculator;
    private String referredConsultantName;

    private BigDecimal cashbackBalance;
    private Boolean EZCust;

    public boolean isReferredByAnotherShopper() {
        return referralShopperClassification != null;
    }

    public void setPortalKey(String portalKey) {
        this.portalKey = portalKey;
    }

    public Portal getPortal() {
        return portal;
    }

    public void setPortal(Portal portal) {
        this.portal = portal;
    }

    public Distributor getDistributor() {
        return distributor;
    }

    public void setDistributor(Distributor distributor) {
        this.distributor = distributor;
    }

    public CustomMiniWebSite getWebSite() {
		return webSite;
	}

	public void setWebSite(CustomMiniWebSite webSite) {
		this.webSite = webSite;
	}

	public String getReferredConsultantName() {
        return referredConsultantName;
    }

    /**
     * @param Referred Consultant Name
     *                 the Referred Consultant Name to set
     */
    public void setReferredConsultantName(String referredConsultantName) {
        this.referredConsultantName = referredConsultantName;
    }

    public boolean hasReducedShipping(Collection<Product> products) {
        if (this.getShippingCalculator().calculateReducedShipping(products).isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if a product has reduced shipping
     *
     * @param product a MA product
     * @return a boolean indicating if reduced shipping exists
     */
    public boolean hasReducedShipping(Product product) {
        return hasReducedShipping(Arrays.asList(product));
    }

    /**
     * @return boolean value indicates if the portal is defined
     */
    public boolean hasPortal() {
        return portal != null;
    }

    /**
     * Get the shipping calculator that provides shipping calculation functions
     *
     * @return a Shipping calculator object that provides calculations against the current shopper
     */
    public ShippingCalculator getShippingCalculator() {
        if (shippingCalculator == null) {
            shippingCalculator = new ShippingCalculator();
        }
        return shippingCalculator;
    }    

    /**
     * @return boolean value indicating if the shopper has a classification
     */
    public boolean hasClassification() {
        return classification != null;
    }

    public BigDecimal getFreeShippingTriggerValue() {
        if (getAddress().getCountryAsConstant().equals(Country.UNITED_STATES))
            return NINETY_NINE;
        return null;
    }

    public ShopperClassification getReferralShopperClassification() {
        return referralShopperClassification;
    }

    public void setReferralShopperClassification(ShopperClassification referralShopperClassification) {
        this.referralShopperClassification = referralShopperClassification;
    }

    public ShopperClassification getClassification() {
        return classification;
    }

    public void setClassification(ShopperClassification classification) {
        this.classification = classification;
    }

    private boolean hasDistributor() {
        return distributor != null;
    }

    public boolean hasReferralShopper() {
        return referralShopperClassification != null;
    }

    public void setShopperID(String shopperID) {
        this.shopperID = shopperID;
    }

    public String getShopperID() {
        return shopperID;
    }

    /**
     * The Shipping Calculator will return shipping costs associated with the given parameters
     */
    public class ShippingCalculator {
        /**
         * Get the available reduced shipping options.
         *
         * @param product a product. The product is used to determine the shipping cost based on its the total weight
         * @return a map of values representing the available reduced shipping options and their corresponding amounts
         * @see com.marketamerica.automation.testdata.Shopper.ShippingCalculator#calculateReducedShipping(java.util.Collection)
         */
        public Map<ReducedShipping, BigDecimal> calculateReducedShipping(Product product) {
            return calculateReducedShipping(Arrays.asList(product));
        }

        /**
         * Get the available reduced shipping options. Use the table below as a reference:
         * <table>
         * <tr>
         * <th colspan="5">United States Reduced Shipping Table (MA Products Only, Free Shipping must be disabled!)</th>
         * </tr>
         * <tr>
         * <td>Method</td>
         * <td>Weight</td>
         * <td>Shipping Charge</td>
         * <td>Residential/Commerical</td>
         * <td>States</td>
         * </tr>
         * <tr>
         * <td>Mail Innovations</td>
         * <td>&lt; 15 Onces</td>
         * <td>$4.00</td>
         * <td>Residential/Commerical</td>
         * <td>All</td>
         * </tr>
         * <tr>
         * <td>SurePost</td>
         * <td>&lt; 144 Onces</td>
         * <td>Distributor Cost * %6 or $6.00 flat rate</td>
         * <td>Residential</td>
         * <td>Lower 48 States</td>
         * </tr>
         * </table>
         *
         * @param products a list of products. The list of products is used to determine the shipping costs based on their the total weight
         * @return a map of values representing the available reduced shipping options and their corresponding amounts
         */
        public Map<ReducedShipping, BigDecimal> calculateReducedShipping(Collection<Product> products) {

            if (hasVendorProduct(products)) {
                throw new OrderShippingNotCalculated();
            }

            Map<ReducedShipping, BigDecimal> reducedShippingCarriers = new HashMap<>();

            // Reduced shipping is only available in the United States so an empty map is returned
            if (!getAddress().getCountryAsConstant().equals(Country.UNITED_STATES)) {
                return reducedShippingCarriers;
            }

        /*
        If the distributor has enabled free shipping on the portal then reduced shipping will not be present
         */
            if (portal.isFreeShipping()) {
                return reducedShippingCarriers;
            }

            if (Product.calculateTotalWeight(products) < MAIL_INNOVATIONS_MAX_WEIGHT_OUNCES) {
                reducedShippingCarriers.put(ReducedShipping.MAIL_INNOVATION, new BigDecimal("4.00"));
            }
            if (Product.calculateTotalWeight(products) < SURE_POST_MAX_WEIGHT_OUNCES && getAddress().isResidential() && getAddress().isLower48States()) {
                if (calculateDistributorCost(products).compareTo(SIX) <= 0) {
                    reducedShippingCarriers.put(ReducedShipping.SURE_POST, new BigDecimal("6.00"));
                } else {
                    reducedShippingCarriers.put(ReducedShipping.SURE_POST, Product.calculateDistributorCost(products).compareTo(SIX) <= 0 ?
                            SIX :
                            formatBigDecimal(Product.calculateDistributorCost(products).multiply(SIX_PERCENT)));
                }
            }
            return reducedShippingCarriers;
        }

        /**
         * Calculates the estimated shipping cost for the product multiplied by the defined quantity.
         *
         * @param product  a product
         * @param quantity the number of times to multiply the given product
         * @see com.marketamerica.automation.testdata.Shopper.ShippingCalculator#calculateShippingCost(java.util.Collection)
         */
        public BigDecimal calculateShippingCost(Product product, int quantity) {
            Collection<Product> products = new ArrayList<>();
            for (int i = 0; i < quantity; i++) {
                products.add(product);
            }
            return calculateShippingCost(products);
        }


        private BigDecimal calculateShippingCostForMiniSite(final BigDecimal order) {
            if (order.compareTo(NINETY_NINE) >= 0) {
                return FREE;
            } else {
                return FIVE;
            }
        }


        /**
         * Calculate the shipping cost associated with the given product(s). Currently this method
         * only supports MA Products. In the instance that a vendor is included in part of the
         * calculation an exception will be thrown.
         * <p>
         * <table border="2">
         * <tr><th colspan=4>United States Only: Shop, Global Shop (US ShipTo), Custom Minis/Minis, Motives Cosmetics</th></tr>
         * <tr>
         * <td>Free Shipping</td><td>ITransact</td><td>USA Order</td><td>Customer Charge</td>
         * </tr>
         * <tr>
         * <td>TRUE</td><td>TRUE/FALSE</td><td>Order >= $99 </td><td align="center">Free Shipping</td>
         * </tr>
         * <tr>
         * <td>TRUE</td><td>TRUE/FALSE</td><td>Order < $99</td><td align="center">$5</td>
         * </tr>
         * <tr>
         * <td>False</td><td>TRUE</td><td>ALL</td><td align="center">Flat min % & $ (portal admin setting)</td>
         * </tr>
         * <tr>
         * <td>False</td><td>FALSE</td><td>Order >= $99</td><td align="center">7.5% of Distributor Cost</td>
         * </tr>
         * <tr>
         * <td>False</td><td>FALSE</td><td>Order < $99</td><td align="center">$7.5 Flat Shipping Fee</td>
         * </tr>
         * </table>
         * <table>
         * <tr>
         * <th colspan="3">(Global Shop) Non-AU Distributor to AU PC</th>
         * </tr>
         * <tr>
         * <td>Free Shipping</td>
         * <td>Order Amount</td>
         * <td>Shipping Carge</td>
         * </tr>
         * <tr>
         * <td>TRUE</td>
         * <td>All Orders</td>
         * <td>Free Shipping</td>
         * </tr>
         * <tr>
         * <td>FALSE</td>
         * <td>All Orders</td>
         * <td>Apply 10% of Standard Retail Profit or $12.00 Minimum</td>
         * </tr>
         * </table>
         * <table>
         * <tr>
         * <th colspan="3">(Global Shop) AU Distributor to AU PC</th>
         * </tr>
         * <tr>
         * <td>Free Shipping</td>
         * <td>Order Amount</td>
         * <td>Shipping Carge</td>
         * </tr>
         * <tr>
         * <td>TRUE/FALSE</td>
         * <td>All Orders</td>
         * <td>Free Shipping</td>
         * </tr>
         * </table>
         * <table>
         * <tr>
         * <th colspan="3">(Global Shop) Canadada Distributor to Canada PC</th>
         * </tr>
         * <tr>
         * <td>Free Shipping</td>
         * <td>Order Amount</td>
         * <td>Shipping Carge</td>
         * </tr>
         * <tr>
         * <td>TRUE/FALSE</td>
         * <td>All Orders</td>
         * <td>Free Shipping</td>
         * </tr>
         * </table>
         * <table>
         * <tr>
         * <th colspan="3">(Global Shop) Non-Canada Distributor to Canada PC</th>
         * </tr>
         * <tr>
         * <td>Free Shipping</td>
         * <td>Order Amount</td>
         * <td>Shipping Carge</td>
         * </tr>
         * <tr>
         * <td>TRUE/FALSE</td>
         * <td>All Orders</td>
         * <td>Free Shipping</td>
         * </tr>
         * <tr>
         * <td>FALSE</td>
         * <td>All Orders</td>
         * <td>Apply 10% of Order SRP or $12.00 minimum</td>
         * </tr>
         * </table>
         * <table>
         * <tr>
         * <th colspan="3">(Global Shop) United Kingdom</th>
         * </tr>
         * <tr>
         * <td>Free Shipping</td>
         * <td>Order Amount</td>
         * <td>Shipping Carge</td>
         * </tr>
         * <tr>
         * <td>TRUE/FALSE</td>
         * <td>All Orders</td>
         * <td>Free Shipping</td>
         * </tr>
         * </table>
         * <table>
         * <tr>
         * <th colspan="2">Taiwan Shipping Charges</th>
         * </tr>
         * <tr>
         * <td>Order Amount</td>
         * <td>Shipping Charge</td>
         * </tr>
         * <tr>
         * <td>&lt; 5,600 TWD</td>
         * <td>125 TWD</td>
         * </tr>
         * <tr>
         * <td>&gt;= 5,600 TWD</td>
         * <td>FREE</td>
         * </tr>
         * </table>
         */
        public BigDecimal calculateShippingCost(final Collection<Product> products) {

        /*
         * Currently, Shipping Cost cannot be determined for vendor products
         */
            if (hasVendorProduct(products)) {
                throw new OrderShippingNotCalculated();
            }

            if (Configuration.getProject().equals(Project.MINI) || Configuration.getProject().equals(Project.CUSTOM_MINI)) {
                return formatBigDecimal(calculateShippingCostForMiniSite(Product.calculateRetailCost(products)));
            }


            BigDecimal order = Product.calculateRetailCost(products);
            BigDecimal discount = new BigDecimal (0);
            
            switch (Configuration.getCountry()) {          
            
                case MEXICO:
                    break;
                case SPAIN:
                    switch (Configuration.getProject()) {
                        case SHOP: {
                            // skipped for now
                        }
                        break;
                    }
                case UNITED_KINGDOM:
                    switch (Configuration.getProject()) {
                        case MINI:
                            break;
                        case CUSTOM_MINI:
                            break;
                        case SHOP: {
                            return FREE;
                        }
                        case NUTRAMETIX:
                            break;
                        case ISOTONIX:
                            break;
                        case TRANSITIONS:
                            break;
                        case MOTIVES_COSMETICS:
                        case MOTIVES_COSMETICS_MOBILE:
                            break;
                        case AUTOMATION_UTILITIES:
                            break;
                        case GLOBAL_SHOP: {
                            return FREE;
                        }
                    }
                case HONG_KONG:
                    break;
                case TAIWAN:
                    return calculateTaiwanShipping(order);
                case AUSTRALIA:
                    switch (Configuration.getProject()) {
                        case GLOBAL_SHOP:
                            if (getAddress().getCountry().equals(Country.AUSTRALIA.toString()) && distributor.getAddress().getCountry().equals(Country.AUSTRALIA.toString())) {
                                return FREE;
                            } else {
                                if (portal.isFreeShipping()) {
                                    if (order.compareTo(NINETY_NINE) == 1) {
                                        return FREE;
                                    } else if (order.compareTo(NINETY_NINE) == -1) {
                                        return FIVE;
                                    }
                                } else {
                                    if (order.multiply(TEN_PERCENT).compareTo(TWELVE) == -1) {
                                        return TWELVE;
                                    } else {
                                        return formatBigDecimal(order.multiply(TEN_PERCENT));
                                    }
                                }
                            }
                        case SHOP:
                            if (portal.isFreeShipping()) {
                                if (order.compareTo(NINETY_NINE) == 1) {
                                    return FREE;
                                } else if (order.compareTo(NINETY_NINE) == -1) {
                                    return FIVE;
                                }
                            } else {
                                return TEN;
                            }
                            break;
                    }
                    break;

                case CANADA:
                    return formatBigDecimal(calculateShippingCostInCanada(products, discount));
                case UNITED_STATES:
                    return formatBigDecimal(calculateShippingCostInUnitedStates(products, discount));

                case SINGAPORE:
                	break;
            }
            throw new OrderShippingNotCalculated();
        }

        /**
         * Calculates the shipping cost in Hong Kong
         *
         * @param order an order amount
         * @return the total shipping cost associated with the order amount
         */
        public BigDecimal calculateHongKongShipping(BigDecimal order,
                                                    HongKongShippingRegion hkRegion) {
            switch (hkRegion) {
                case DISCOVERY_BAY:
                    if (order.compareTo(ONE_THOUSAND_FIVE_HUNDRED) == -1) {
                        return THREE_HUNDRED_TWENTY;
                    } else {
                        return TWO_HUNDRED_SEVENTY;
                    }
                case URBAN_AREA:
                    if (order.compareTo(ONE_THOUSAND_FIVE_HUNDRED) == -1) {
                        return FIFTY;
                    } else {
                        return FREE;
                    }
                case CHEK_LAP_KOK:
                case MA_WAN:
                case TUNG_CHUNG:
                    if (order.compareTo(ONE_THOUSAND_FIVE_HUNDRED) == -1) {
                        return TWO_HUNDRED;
                    } else {
                        return ONE_HUNDRED_FIFTY;
                    }
            }
            return null;
        }

        /**
         * Calculates the shipping cost for Hong Kong Intranet distributor POS order
         *
         * @param order an order amount
         * @return the total shipping cost associated with the order amount and shipping method
         */
        public BigDecimal calculateHKGDistPOSShipping(DeliveryType shippingMethod,
                                                      BigDecimal order, String hkDistrict, String pickupLocation) {

            if (shippingMethod.equals(DeliveryType.SHIP)) {
                HongKongShippingRegion shipRegion = HongKongShippingRegion
                        .parse(hkDistrict);

                switch (shipRegion) {
                    case DISCOVERY_BAY:
                        if (order.compareTo(FOUR_THOUSAND) == -1) {
                            return THREE_HUNDRED_TWENTY;
                        } else {
                            return TWO_HUNDRED_SEVENTY;
                        }
                    case URBAN_AREA:
                        if (order.compareTo(FOUR_THOUSAND) == -1) {
                            return FIFTY;
                        } else {
                            return FREE;
                        }
                    case CHEK_LAP_KOK:
                    case MA_WAN:
                    case TUNG_CHUNG:
                        if (order.compareTo(FOUR_THOUSAND) == -1) {
                            return TWO_HUNDRED;
                        } else {
                            return ONE_HUNDRED_FIFTY;
                        }
                }
            } else if (shippingMethod.equals(DeliveryType.PICK_UP)) {
                HongKongPickupLocation pickupLoc = HongKongPickupLocation
                        .parse(pickupLocation);

                switch (pickupLoc) {
                    case KWUN_TONG:
                        return TEN;
                    case WANCHAI:
                        return TEN;
                    case TSIM_SHA_TSUI:
                        return FREE;
                }
            }
            return null;
        }

        /**
         * Calculates the shipping cost for Hong Kong Intranet PC POS order
         *
         * @param order an order amount
         * @return the total shipping cost associated with the order amount and shipping method
         */
        public BigDecimal calculateHKGPCPOSShipping(DeliveryType shippingMethod,
                                                    BigDecimal order, String hkDistrict, String pickupLocation) {

            if (shippingMethod.equals(DeliveryType.SHIP)) {
                HongKongShippingRegion shipRegion = HongKongShippingRegion
                        .parse(hkDistrict);

                switch (shipRegion) {
                    case DISCOVERY_BAY:
                        if (order.compareTo(SIX_THOUSAND) == -1) {
                            return THREE_HUNDRED_TWENTY;
                        } else {
                            return TWO_HUNDRED_SEVENTY;
                        }
                    case URBAN_AREA:
                        if (order.compareTo(SIX_THOUSAND) == -1) {
                            return FIFTY;
                        } else {
                            return FREE;
                        }
                    case CHEK_LAP_KOK:
                    case MA_WAN:
                    case TUNG_CHUNG:
                        if (order.compareTo(SIX_THOUSAND) == -1) {
                            return TWO_HUNDRED;
                        } else {
                            return ONE_HUNDRED_FIFTY;
                        }
                }
            } else if (shippingMethod.equals(DeliveryType.PICK_UP)) {
                HongKongPickupLocation pickupLoc = HongKongPickupLocation
                        .parse(pickupLocation);

                switch (pickupLoc) {
                    case KWUN_TONG:
                        return TEN;
                    case WANCHAI:
                        return TEN;
                    case TSIM_SHA_TSUI:
                        return FREE;
                }
            }
            return null;
        }


        /**
         * Calculates the shipping cost in Taiwan
         *
         * @param order an order amount
         * @return the total shipping cost in whole number associated with the order amount
         */
        private BigDecimal calculateTaiwanShipping(BigDecimal order) {
            switch (Configuration.getProject()) {
                case MINI:
                case CUSTOM_MINI:
                case SHOP:
                case MOTIVES_COSMETICS:
                case MOTIVES_COSMETICS_MOBILE:
                case AUTOMATION_UTILITIES:
                case GLOBAL_SHOP:
                default:
            }
            if (order.compareTo(FIVE_THOUSAND_SIX_HUNDRED) == -1) {
                return ONE_HUNDRED_TWENTY_FIVE;
            } else {
                return FREE;
            }

        }
        
        /**
         * Calculates the shipping cost for revamped Intranet POS ordering
         *
         * @param shipping country shipped to country
         * @param order an order amount
         * @param pick up order or not
         * @param ship to special area for an additional charge or not
         * @return the total shipping cost associated with the order amount and ship to area
         */
        
		public BigDecimal calculateShippingForPOSOrder(Country shipToCountry,
				BigDecimal order, boolean pickUp, boolean shipToSpecialArea) {

			switch (shipToCountry) {
				case SINGAPORE:
					return calculateSingaporePOSShipping(order, pickUp,
							shipToSpecialArea);
				case SPAIN:
					return calculateSpainPOSShipping(order, pickUp,
							shipToSpecialArea);
				default:
					return FREE;
			}
		}
        
        /**
         * Calculates the shipping cost in Singapore
         *
         * @param order an order amount
         * @param pick up order or not
         * @param ship to special area for an additional charge or not
         * @return the total shipping cost associated with the order amount and ship to area
         */
        public BigDecimal calculateSingaporePOSShipping(BigDecimal order, boolean pickUp,
                                                    boolean shipToSpecialArea) {
            if(!pickUp) {
            	if(!shipToSpecialArea) {            	 
                    if (order.compareTo(THREE_HUNDRED_FIFTY) == -1) {
                        return FIVE_POINT_THREE_FIVE;
                    } else {
                        return FREE;
                    } 
            	} else {
            		if (order.compareTo(THREE_HUNDRED_FIFTY) == -1) {
                        return ELEVEN_POINT_SEVEN_SEVEN;
                    } else {
                        return SIX_POINT_FOUR_TWO;
                    }  
            	}
            } else return FREE;
        }
        
        /**
         * Calculates the shipping cost in Spain
         *
         * @param order an order amount
         * @param pick up order or not
         * @param ship to special area for an additional charge or not
         * @return the total shipping cost associated with the order amount and ship to area
         */
		public BigDecimal calculateSpainPOSShipping(BigDecimal order,
				boolean pickUp, boolean shipToSpecialArea) {
			if (!pickUp) {
				if (!shipToSpecialArea) {
					if (order.compareTo(ONE_HUNDRED_FORTY) == -1) {
						return FIVE;
					} else
						return FREE;

				} else
					return TEN;

			} else
				return FREE;
		}


        /**
         * Calculates the shipping fee in non marketing, non EMP based on product base country
         *
         * @param order          an order amount
         * @param productCountry product base country
         * @return the total shipping cost associated with the product base country and order amount
         */
        private BigDecimal calculateOtherCountryShipping(Region shipToRegion, Country productCountry, 
        			BigDecimal order) {
            switch (productCountry) {
                case SPAIN:
                    return TEN;
                case UNITED_KINGDOM:
                    return EIGHT;
                case HONG_KONG:
                    if (order.compareTo(FOUR_HUNDRED_FIFTY) > 0) {
                        return ZERO;
                    } else {
                        return ONE_HUNDRED;
                    }        
                case UNITED_STATES:
                	if (shipToRegion.equals(Region.ASIA_PACIFIC)) {
						if ((order.multiply(ELEVEN_PERCENT))
								.compareTo(FORTEEN) > 0) {
							return formatBigDecimal(order
									.multiply(ELEVEN_PERCENT));
						} else {
							return FORTEEN;
						}
					} else {
						if ((order.multiply(NINE_PERCENT))
								.compareTo(TWELVE) > 0) {
							return formatBigDecimal(order
									.multiply(NINE_PERCENT));
						} else {
							return TWELVE;
						}
					}     
                         
                default:
                    return null;
            }
        }

        public BigDecimal calculateShippingForGlobalShop(Collection<Product> products) {
            return formatBigDecimal(calculateShippingForGlobalShop(products, getAddress()));
        }

        public BigDecimal calculateShippingForGlobalShop(Product product) {
            return formatBigDecimal(calculateShippingForGlobalShop(Arrays.asList(product), getAddress()));
        }

        public BigDecimal calculateShippingForGlobalShop(Product product, Address shippingDestination) {
            return formatBigDecimal(calculateShippingForGlobalShop(Arrays.asList(product), shippingDestination));
        }

		public BigDecimal calculateShippingForGlobalShop(
				Collection<Product> products, Address shippingDestination) {

			if ((hasClassification() && (classification
					.equals(ShopperClassification.MOTIVES_AFFILIATE_ASSOCIATE) || classification
					.equals(ShopperClassification.MOTIVES_AFFILIATE_ASSOCIATE_CUSTOMER)))
					|| (hasReferralShopper() && referralShopperClassification
							.equals(ShopperClassification.MOTIVES_AFFILIATE_ASSOCIATE))) {
				return FREE;
			}
			 
			final Country shipToCountry = Country.parse(shippingDestination
					.getCountry());
			final Region shipToRegion = shipToCountry.getRegion();

			final BigDecimal orderRetailCost = Product
					.calculateRetailCost(products);
			final BigDecimal orderDistributorCost = Product
					.calculateDistributorCost(products);

			if (shipToCountry.isPersonalConsumption()) {
				if (hasClassification()
						&& classification
								.equals(ShopperClassification.PERSONAL_PREFERRED_CUSTOMER)) {
					if (shipToRegion.equals(Region.ASIA_PACIFIC)) {
						if ((orderDistributorCost.multiply(FORTEEN_PERCENT))
								.compareTo(FORTEEN) > 0) {
							return formatBigDecimal(orderDistributorCost
									.multiply(FORTEEN_PERCENT));
						} else {
							return FORTEEN;
						}
					} else {
						if ((orderDistributorCost.multiply(TWELVE_PERCENT))
								.compareTo(TWELVE) > 0) {
							return formatBigDecimal(orderDistributorCost
									.multiply(TWELVE_PERCENT));
						} else {
							return TWELVE;
						}
					}
				} else {
					if (orderRetailCost.compareTo(NINETY_NINE) < 0) {
						return FIVE;
					} else {
						return FREE;
					}
				}
			} else {
				if (shipToCountry.getClassification().equals(
						Country.CountryClassification.EMP)
						|| shipToCountry.getClassification().equals(
								Country.CountryClassification.OTHER)) {
					final Country productCountry = Product
							.getProductCountry(shipToCountry);							
					return formatBigDecimal(calculateOtherCountryShipping(shipToRegion,
							productCountry, orderRetailCost));
				} else {

					switch (shipToCountry) {
					case UNITED_STATES:
						if ((orderDistributorCost
								.multiply(SEVEN_POINT_FIVE_PERCENT))
								.compareTo(SEVEN_POINT_FIVE) > 0) {
							return formatBigDecimal(orderDistributorCost
									.multiply(SEVEN_POINT_FIVE_PERCENT));
						} else {
							return SEVEN_POINT_FIVE;
						}
					case MEXICO:
						if ((orderRetailCost.multiply(SEVEN_POINT_FIVE_PERCENT))
								.compareTo(ONE_HUNDRED) > 0) {
							return formatBigDecimal(orderRetailCost
									.multiply(SEVEN_POINT_FIVE_PERCENT));
						} else {
							return ONE_HUNDRED;
						}
					case TAIWAN:
						return formatBigDecimal(
								calculateTaiwanShipping(orderRetailCost))
								.setScale(0, RoundingMode.CEILING);		 
					default:
						return null;
					}
				}
			}
		}

        public BigDecimal getMinimumOrderTotal() {
            if (Configuration.getProject().equals(Project.GLOBAL_SHOP)) {
                if (Configuration.getCountry().equals(Country.UNITED_KINGDOM)) {
                    return TWELVE;
                } else {
                    if (Configuration.getCountry().equals(Country.UNITED_STATES)) {
                        return TWENTY_FIVE;
                    } else {
                        if (Configuration.getCountry().getRegion().equals(Region.EUROPE)) {
                            return FIFTEEN;
                        }
                    }
                }
            }
            return ZERO;
        }

        public BigDecimal getMaximumOrderTotal() {
            if (Configuration.getProject().equals(Project.GLOBAL_SHOP)) {
                if (Configuration.getCountry().equals(Country.UNITED_KINGDOM)) {
                    return ZERO;
                } else if (Configuration.getCountry().equals(Country.UNITED_STATES)) {
                    return ONE_THOUSAND;
                } else if (Configuration.getCountry().getRegion().equals(Region.EUROPE)) {
                    return ZERO;
                } else if (Configuration.getCountry().equals(Country.CHINA)) {
                    return CurrencyUtilities.NINE_HUNDRED;
                }
            }
            return ZERO;
        }

        public boolean hasMinimumOrderTotal() {
            return getMinimumOrderTotal().compareTo(ZERO) > 0;
        }

        public boolean hasMaximumOrderTotal() {
            return getMaximumOrderTotal().compareTo(ZERO) > 0;
        }

        private BigDecimal calculateShippingCostInCanada(Collection<Product> products, BigDecimal discount) {
            if (!hasDistributor()) {
                logger.error("For calculating shipping costs, the distributor is used as a part of the calculation and was not defined. Please revisit your testing requirements");
                throw new OrderShippingNotCalculated();
            }
            
            BigDecimal order = Product.calculateDistributorCost(products);
            BigDecimal shippingCost = null;
            
            switch (Configuration.getProject()) {
                case SHOP:
                case ISOTONIX:     
                case TRANSITIONS:
                case CUSTOM_MINI:
                	if (order.multiply(NINE_POINT_FIVE_PERCENT).compareTo(TEN_POINT_FIVE) < 0) {
                        shippingCost = TEN_POINT_FIVE;
                    } else {
                        shippingCost = order.multiply(NINE_POINT_FIVE_PERCENT);
                    }
                    return CurrencyUtilities.formatBigDecimal(shippingCost);  
                	
                case MOTIVES_COSMETICS: 
                case MOTIVES_COSMETICS_MOBILE:
                	if (hasClassification() && !getClassification().equals(ShopperClassification.PREFERRED_CUSTOMER)
                    		&& !getClassification().equals(ShopperClassification.PERSONAL_PREFERRED_CUSTOMER)) {
                        return formatBigDecimal(calculateShippingForCANMotivesAssociates(products, discount));
                    } else {   
                    	if (order.multiply(NINE_POINT_FIVE_PERCENT).compareTo(TEN_POINT_FIVE) < 0) {
                            shippingCost = TEN_POINT_FIVE;
                        } else {
                            shippingCost = order.multiply(NINE_POINT_FIVE_PERCENT);
                        }
                        return CurrencyUtilities.formatBigDecimal(shippingCost);
                    }                                        
            }
            throw new OrderShippingNotCalculated();
        }

        /**
         * Calculates the shipping for a motives associate preferred customer
         *
         * @param product a product whose retail and distributor cost may affect the total shipping cost
         * @return a big decimal representing the calculated shipping cost for the given product(s)
         */
        public BigDecimal calculateShippingForMotivesAssociatesPreferredCustomer(Product product) {
            return calculateShippingForMotivesAssociatesPreferredCustomer(Arrays.asList(product));
        }

        private BigDecimal calculateShippingForMotivesAssociatesPreferredCustomer(Collection<Product> products) {

            // Only check MPA/MTA/MAA costs if a referral shopper is defined that has a valid classification
            if (isReferredByAnotherShopper()) {
                    /*
                    MAA PCs always have free shipping
                     */
                if (getReferralShopperClassification().equals(ShopperClassification.MOTIVES_AFFILIATE_ASSOCIATE)) {
                    return FREE;
                }

                /*
                Disable iTransact for calculations became iTransact
                doesn't apply to MPA/MTA portals as the settings do not propagate.
                After we calculate the shipping cost, we restore the setting.
                 */
                boolean oldDistributorITransactSetting = distributor.isITransact();
                distributor.setITransact(false);
                BigDecimal shippingCost = calculateShippingCost(products);
                if (!oldDistributorITransactSetting) {
                    distributor.setITransact(oldDistributorITransactSetting);
                }
                return shippingCost;
            }
            throw new OrderShippingNotCalculated();
        }

        private BigDecimal calculateShippingForUSAMotivesAssociates(Collection<Product> products) {
            BigDecimal orderDistCost = Product.calculateDistributorCost(products);
            BigDecimal orderRetailCost = Product.calculateRetailCost(products);
                        
            switch(getClassification()) {
            case MOTIVES_AFFILIATE_ASSOCIATE:
            case MOTIVES_PRO_ARTIST:
            	if (orderDistCost.multiply(SEVEN_POINT_FIVE_PERCENT).compareTo(SEVEN_POINT_FIVE) < 0) {
                	return SEVEN_POINT_FIVE;
                } else {
                	return formatBigDecimal(orderDistCost.multiply(SEVEN_POINT_FIVE_PERCENT));
                }
                
            case MOTIVES_AFFILIATE_ASSOCIATE_CUSTOMER:
            	return FREE;
            case MOTIVES_PRO_ARTIST_CUSTOMER:
            case MOTIVES_TEEN_ASSOCIATE_CUSTOMER:
            	if (portal.isFreeShipping()) {                             
                    if (orderRetailCost.compareTo(NINETY_NINE) < 0) {
                        return FIVE;
                    	} else return FREE;                                 
                }  else {
                	if (orderDistCost.compareTo(ONE_HUNDRED) < 0) {
                        return SEVEN_POINT_FIVE;
                    } else {
                        return formatBigDecimal(orderDistCost.multiply(SEVEN_POINT_FIVE_PERCENT));
                    }
                }            	 
            }              
            throw new OrderShippingNotCalculated();
        }
        
        private BigDecimal calculateShippingForCANMotivesAssociates(Collection<Product> products, BigDecimal discount) {
        	BigDecimal order = Product.calculateDistributorCost(products);
        	
        	switch(getClassification()) {        	
            	case MOTIVES_AFFILIATE_ASSOCIATE:
            	case MOTIVES_PRO_ARTIST:
            	case MOTIVES_PRO_ARTIST_CUSTOMER:
            	case MOTIVES_TEEN_ASSOCIATE_CUSTOMER:
            		if (order.multiply(NINE_POINT_FIVE_PERCENT).compareTo(TEN_POINT_FIVE) < 0) {
            			return TEN_POINT_FIVE;
            		} else {
            			return CurrencyUtilities.formatBigDecimal(order.multiply(NINE_POINT_FIVE_PERCENT));
            		}
                
            	case MOTIVES_AFFILIATE_ASSOCIATE_CUSTOMER:
            		return FREE;            	 	 
            }              
            throw new OrderShippingNotCalculated();
        }

        /**
         * Calculates Shipping Costs in the United States 
         *
         * @param products a list of products
         * @return a big decimal amount representing the estimated shipping of the given products in the united states
         * @param discount the discount percentage based on coupon code
         */
        private BigDecimal calculateShippingCostInUnitedStates(Collection<Product> products, BigDecimal discount) {
             	
        	BigDecimal originalOrderSubTotal = Product.calculateRetailCost(products);
        	BigDecimal subTotalDiscount = formatBigDecimal(originalOrderSubTotal.multiply(discount));
        	BigDecimal orderSubTotal = originalOrderSubTotal.subtract(subTotalDiscount);        	
        	 
        	BigDecimal orderDistCost = Product.calculateDistributorCost(products);        			
             
            BigDecimal shippingCost = null;
            
            switch (Configuration.getProject()) {
                case GLOBAL_SHOP:
                    throw new OrderShippingNotCalculated();                          
                case MOTIVES_COSMETICS: case MOTIVES_COSMETICS_MOBILE:  
                	if (hasClassification()
                			&& !getClassification().equals(
                					ShopperClassification.PREFERRED_CUSTOMER)
                			&& !getClassification()
									.equals(ShopperClassification.PERSONAL_PREFERRED_CUSTOMER)) {
                		return calculateShippingForUSAMotivesAssociates(products);
                	} else {
                		if (portal.isFreeShipping()) {
                			if (orderSubTotal.compareTo(NINETY_NINE) < 0) {
                				shippingCost = FIVE;
                			} else
                				shippingCost = FREE;
                		} else {
                			if (distributor.isITransact()) {
                				final BigDecimal handlingCharge = orderSubTotal
                						.multiply(portal.getShippingCharge());
                				final BigDecimal minimumHandlingCharge = portal
                						.getMinimumShippingCharge();
                				if (handlingCharge.compareTo(minimumHandlingCharge) >= 0) {
                					shippingCost = formatBigDecimal(handlingCharge);
                				} else {
                					shippingCost = formatBigDecimal(minimumHandlingCharge);
                				}
                			} else {
                				if (orderDistCost.compareTo(ONE_HUNDRED) < 0) {
                					shippingCost = SEVEN_POINT_FIVE;
                				} else {
                					shippingCost = formatBigDecimal(orderDistCost
                							.multiply(SEVEN_POINT_FIVE_PERCENT));
                					}
                				}
                			}
                		}
                	return shippingCost;
                case CUSTOM_MINI:
                	if (portal.isFreeShipping()) {
            			if (orderSubTotal.compareTo(NINETY_NINE) < 0) {
            				shippingCost = FIVE;
            			} else
            				shippingCost = FREE;
            		} else {
            			if (distributor.isITransact()) {
            				final BigDecimal handlingCharge = orderSubTotal
            						.multiply(portal.getShippingCharge());
            				final BigDecimal minimumHandlingCharge = portal
            						.getMinimumShippingCharge();
            				if (handlingCharge.compareTo(minimumHandlingCharge) >= 0) {
            					shippingCost = handlingCharge;
            				} else {
            					shippingCost = minimumHandlingCharge;
            				}
            			} else {
            				if (orderDistCost.compareTo(ONE_HUNDRED) < 0) {
            					shippingCost = SEVEN_POINT_FIVE;
            				} else {
            					shippingCost = orderDistCost
            							.multiply(SEVEN_POINT_FIVE_PERCENT);
            					}
            				}
            			}
                	return formatBigDecimal(shippingCost);
                case NUTRAMETIX:            
                case ISOTONIX:            
                case TRANSITIONS: 
                case MINI:
                case SHOP:
                	if (portal.isFreeShipping()) {
            			if (orderSubTotal.compareTo(NINETY_NINE) < 0) {
            				shippingCost = FIVE;
            			} else
            				shippingCost = FREE;
            		} else {
            			if (distributor.isITransact()) {
            				final BigDecimal handlingCharge = orderSubTotal
            						.multiply(portal.getShippingCharge());
            				final BigDecimal minimumHandlingCharge = portal
            						.getMinimumShippingCharge();
            				if (handlingCharge.compareTo(minimumHandlingCharge) >= 0) {
            					shippingCost = handlingCharge;
            				} else {
            					shippingCost = minimumHandlingCharge;
            				}
            			} else {
            				if (orderDistCost.compareTo(ONE_HUNDRED) < 0) {
            					shippingCost = SEVEN_POINT_FIVE;
            				} else {
            					shippingCost = orderDistCost
            							.multiply(SEVEN_POINT_FIVE_PERCENT);
            					}
            				}
            			}
                	return shippingCost;

                case AUTOMATION_UTILITIES:
                    break;
            }
            throw new OrderShippingNotCalculated();
        }
        
        /**
         * Calculates Shipping Costs in Australia 
         *
         * @param products a list of products
         * @return a big decimal amount representing the estimated shipping of the given products in the united states
         */
        private BigDecimal calculateShippingCostInAustralia(Collection<Product> products, BigDecimal discount) {
            BigDecimal originalOrderSubTotal = Product.calculateRetailCost(products);
        	BigDecimal subTotalDiscount = formatBigDecimal(originalOrderSubTotal.multiply(discount));
        	BigDecimal orderSubTotal = originalOrderSubTotal.subtract(subTotalDiscount);
            
            switch (Configuration.getProject()) {
                case GLOBAL_SHOP:
                    throw new OrderShippingNotCalculated();                           
                case MOTIVES_COSMETICS:
                case MOTIVES_COSMETICS_MOBILE:
                case CUSTOM_MINI:
                case MINI:
                case SHOP:                     
                     
                    BigDecimal shippingCost = null;
                    
                    if (portal.isFreeShipping()) {                             
                                if (orderSubTotal.compareTo(NINETY_NINE) < 0) {
                                    shippingCost = FIVE;
                                	} else shippingCost = FREE;                                 
                            }  else {
                        	shippingCost = TEN;
                        }                       
                    return shippingCost;                
            }
            throw new OrderShippingNotCalculated();
        }


        /**
         * Calculates the shipping cost for the given product
         *
         * @param product a product who you would like to estimate the shipping for
         * @see com.marketamerica.automation.testdata.Shopper.ShippingCalculator#calculateShippingCost(java.util.Collection)
         */

        public BigDecimal calculateShippingCost(Product product) {
            return calculateShippingCost(Arrays.asList(product));
        }
        
        /**
         * Calculates the shipping cost on mini web sites
         *
         * @param products the products which you would like to estimate the shipping for
         * @param shippingAddress the ship to address
         * @param discount the discount percentage based on coupon code
         */
        
		public BigDecimal calculateShippingForMiniWebSites(
				Collection<Product> products, Address shippingAddress, BigDecimal discount) {			 

			final Country shipToCountry = Country.parse(shippingAddress
					.getCountry());			
			 
			BigDecimal originalOrderRetailCost = Product.calculateRetailCost(products);
        	BigDecimal subTotalDiscount = formatBigDecimal(originalOrderRetailCost.multiply(discount));
        	BigDecimal orderRetailCost = originalOrderRetailCost.subtract(subTotalDiscount);

			switch (shipToCountry) {
			case UNITED_STATES:
				 return calculateShippingCostInUnitedStates(products, discount);					 
			case CANADA:
				return calculateShippingCostInCanada(products, discount);
			case AUSTRALIA:
				return calculateShippingCostInAustralia(products, discount);
			case UNITED_KINGDOM:
				return FREE;			 
			case TAIWAN:				       	
				return calculateTaiwanShipping(orderRetailCost).setScale(0, RoundingMode.HALF_UP);
			case HONG_KONG:
				try {
					if (shippingAddress instanceof HongKongAddress) {
						HongKongAddress hkAddress = (HongKongAddress) shippingAddress;
						HongKongShippingRegion hkRegion = hkAddress.getRegion();
						return formatBigDecimal(calculateHongKongShipping(
								orderRetailCost, hkRegion));
					}
				} catch (Exception e) {
					logger.warn(String
							.format("Address is not recognized as a Hong Kong Address."));
					return null;
				}
			case MEXICO:
                if ((orderRetailCost
                        .multiply(SEVEN_POINT_FIVE_PERCENT))
                        .compareTo(ONE_HUNDRED) > 0) {
                    return formatBigDecimal(orderRetailCost
                            .multiply(SEVEN_POINT_FIVE_PERCENT));
                } else {
                    return ONE_HUNDRED;
                }
			default:
				return ZERO;
			}

		}
		
		/**
         * Calculates the shipping cost for the given product on mini web site
         *
         * @param product a product who you would like to estimate the shipping for
         * @param shippingAddress the ship to address
         * @param discount the discount percentage
         */
		public BigDecimal calculateShippingForMiniWebSites(
				Product product, Address shippingAddress, BigDecimal discount) { 
            return calculateShippingForMiniWebSites(Arrays.asList(product), shippingAddress, discount);
        }	
		
		public BigDecimal calculateShippingForMiniWebSites(
				Product product, Address shippingAddress) { 
			BigDecimal discount = new BigDecimal(0);			
            return calculateShippingForMiniWebSites(product, shippingAddress, discount);
        }	
		
		public BigDecimal calculateShippingForMiniWebSites(
				Collection<Product> products, Address shippingAddress) {
			BigDecimal discount = new BigDecimal(0);			
            return calculateShippingForMiniWebSites(products, shippingAddress, discount);			
		}
		
    }
    
    public BigDecimal getCashbackBalance() {
    	return cashbackBalance;
    }
    
    public void setCashbackBalance(BigDecimal cashbackBalance) {
    	this.cashbackBalance = cashbackBalance;
    }
    
    public Boolean isEZCust() {
    	return EZCust;
    }
    
    public void setCashbackBalance(Boolean EZCust) {
    	this.EZCust = EZCust;
    }

}
