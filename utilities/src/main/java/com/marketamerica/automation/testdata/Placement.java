package com.marketamerica.automation.testdata;

import com.marketamerica.automation.utilities.enums.PlacementLeg;

/**
 * Created by Javier L. Velasquez on 9/26/2014.
 */
public class Placement {
    private int line;
    private String name;
    private String unFranchiseId;
    private String extension;
    private PlacementLeg leg;
    private String phoneNumber;
    private String atgNumber;

    public boolean isOpen() {
        return extension.equals("002") || extension.equals("003");
    }

    public boolean isOuter() {
        return (extension.equals("002") && leg.equals(PlacementLeg.LEFT)) || (extension.equals("003") && leg.equals(PlacementLeg.RIGHT));
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line = line;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnFranchiseId() {
        return unFranchiseId;
    }

    public void setUnFranchiseId(String unFranchiseId) {
        this.unFranchiseId = unFranchiseId;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public PlacementLeg getLeg() {
        return leg;
    }

    public void setLeg(PlacementLeg leg) {
        this.leg = leg;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getAtgNumber() {
        return atgNumber;
    }

    public void setAtgNumber(String atgNumber) {
        this.atgNumber = atgNumber;
    }

    @Override
    public String toString() {
        if (name != null) {
            return String.format("PlacementDetails : Name - %s , UnfranchiseId - %s, Extension -%s , Leg - %s ", name, unFranchiseId, extension, leg.toString());
        } else {
            return String.format("PlacementDetails : UnfranchiseId - %s, Extension -%s , Leg - %s ", unFranchiseId, extension, leg.toString());
        }
    }

}
