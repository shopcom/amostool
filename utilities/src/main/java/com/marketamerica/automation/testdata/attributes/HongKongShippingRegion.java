package com.marketamerica.automation.testdata.attributes;

/**
 * This enum represents the various shipping regions of Hong Kong.
 * Created by Javier L. Velasquez on 9/30/2014.
 */
public enum HongKongShippingRegion {

    URBAN_AREA("Urban Area"), DISCOVERY_BAY("Discovery Bay"), CHEK_LAP_KOK("Chek Lap Kok"), TUNG_CHUNG("Tung Chung"), MA_WAN("Ma Wan");

    private String region;

    private HongKongShippingRegion(final String region) {
        this.region = region;
    }

    /**
     * Resolve a Hong Kong Shipping Region via a string.
     *
     * @param string a string that represents a Hong Kong Shipping Region
     * @return a enum constant representing a Hong Kong Shipping Region. Returns null if no Shipping Region found.
     */
    public static HongKongShippingRegion parse(String string) {
        final HongKongShippingRegion[] shippingRegions = values();
        for (HongKongShippingRegion hongKongShippingRegion : shippingRegions) {
            if (hongKongShippingRegion.toString().equalsIgnoreCase(string.trim())) {
                return hongKongShippingRegion;
            }
        }
        return HongKongShippingRegion.URBAN_AREA;
    }

    @Override
    public String toString() {
        return region;
    }

}
