package com.marketamerica.automation.testdata.address;

import com.marketamerica.automation.testdata.Address;

public class SpainAddress extends Address {

    private String street3;
 
    public String getStreet3() {
        return street3;
    }

    public void setStreet3(String street3) {
        this.street3 = street3;
    }

}
