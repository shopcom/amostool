package com.marketamerica.automation.utilities.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.*;

/**
 * Class to allow for the opening and editing
 * of Excel WorkBook Files
 * <p>
 * Created by Nicholas Spitzer on 6/11/2015.
 */
public class ExcelWorkBookEditor extends ExcelWorkBook {

    private File file;
    private FileOutputStream out;

    public ExcelWorkBookEditor(final String filePath) {
        this(new File(filePath));
    }

    public ExcelWorkBookEditor(final File workBookFile) {

        file = workBookFile;

        FileInputStream workBookInputStream = null;
        try {
            workBookInputStream = new FileInputStream(workBookFile);
        } catch (FileNotFoundException e) {
            logger.error(e);
        }
        logger.debug(String.format("Opening workbook at: %s", workBookFile.getAbsolutePath()));

        try {
            workbook = new XSSFWorkbook(workBookInputStream);
        } catch (IOException e) {
            logger.error(
                    String.format("Could not create workbook for %s", workBookFile.getName()), e);
        }

        try {
            workBookInputStream.close();
        } catch (IOException e) {
            logger.error(
                    String.format("Could not close workbook for %s", workBookFile.getName()), e);
        }

        switchWorkSheet(workbook.getSheetName(0));
    }

    /**
     * Get column names
     *
     * @return List<String></String>
     */
    public List<String> getColumns() {
        return getRowData(0);
    }

    public void addHeaders(final List<String> headers) {

        final Row columns = this.createRow();
        for (int i = 0; i < headers.size(); i++) {
            final Cell cell = columns.createCell(i);
            cell.setCellValue(headers.get(i));
        }
    }

    public void addHeader(String newHeader) {

        final Row row = this.getRow(0);
        final Cell cell = row.createCell(row.getLastCellNum() + 1);
        cell.setCellValue(newHeader);
    }

    public void setRowValueByColumnName(final Row row, final Object column,
                                        final String content) {

        final int columnIndex = getColumnIndex(String.valueOf(column));
        Cell cell = row.getCell(columnIndex);
        if (cell == null) {
            cell = row.createCell(columnIndex);
        }
        row.getCell(columnIndex).setCellValue(content);
    }

    public Row createRow() {

        if (this.currentSheet.getLastRowNum() == 0) {
            final Row row = this.getRow(0);
            if (row == null) {
                return createRow(0);
            } else {
                return createRow(1);
            }
        } else {
            return createRow(this.currentSheet.getLastRowNum() + 1);
        }
    }

    public Row createRow(final int index) {
        return this.currentSheet.createRow(index);
    }

    public Row getRow(final int index) {
        return currentSheet.getRow(index);
    }

    public <T> void addRow(final List<T> rowList) {

        final Row row = this.createRow();
        for (int i = 0; i < rowList.size(); i++) {
            row.createCell(i).setCellValue(String.valueOf(rowList.get(i)));
        }
    }

    public <T> void addRow(final Map<String, T> rowMap) {

        final Row row = this.createRow();

        final Set<String> keySet = rowMap.keySet();
        final List<String> headers = getHeaders();
        final Collection<String> newColumns = new ArrayList<>();

        // Add any existing keySet
        for (String column : keySet) {
            // Add row data only if the column exists if it doesn't then store it to be processed later
            if (headers.contains(column)) {
                setRowValueByColumnName(row, column, String.valueOf(rowMap.get(column)));
            } else {
                newColumns.add(column);
            }
        }

        // Add any new keySet
        for (String newColumn : newColumns) {
            // Create the header before adding the cell data
            this.addHeader(newColumn);
            setRowValueByColumnName(row, newColumn, String.valueOf(rowMap.get(newColumn)));
        }
    }

    public void resizeAllColumns() {

        final int lastColumnNumber = this.getRow(0).getLastCellNum();
        for (int i = 0; i < lastColumnNumber; ++i)
            currentSheet.autoSizeColumn(i);
    }

    /**
     * Write out the data to the file
     * and close the file
     */
    public void writeAndCloseWorkBook() {

        try {
            out = new FileOutputStream(file);
            logger.info(String.format("Saved workbook to %s", file.getAbsoluteFile().toString()));
        } catch (FileNotFoundException e) {
            logger.error(e);
        }
        try {
            workbook.write(out);
        } catch (IOException e) {
            logger.error(e);
        }
        try {
            out.close();
        } catch (IOException e) {
            logger.error(e);
        }
    }
}
