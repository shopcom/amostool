package com.marketamerica.automation.utilities.helpers;

import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.marketamerica.automation.utilities.enums.Project;
import com.marketamerica.automation.utilities.testmanagement.TestResourceCoordinator;

/**
 * Translator provides the ability to translate phrases by a given locale
 * Created by Javier L. Velasquez on 5/4/2014.
 */
public final class Translator extends TestResourceCoordinator {
    private static final Logger logger = LogManager.getLogger(Translator.class);
    private static final String PATH = "localization";
    private static final String BUNDLE_NAME = "MessagesBundle";
    private Locale locale;
    private ResourceBundle resourceBundle;

    public Translator(final Locale locale, Project project) {
        this.locale = locale;
        String rootPath = getRootPath(project);
        try {
            resourceBundle = ResourceBundle.getBundle(
                    String.format("%s/%s", rootPath, BUNDLE_NAME), this.locale,
                    new UTF8Control());
            logger.info(String.format(
                    "Translator has been hired to provide localization for %s - %s",
                    locale.getCountry(), locale.getLanguage()));
        } catch (MissingResourceException e) {
            logger.warn(String.format(
                    "Unable to find resource bundle for %s - %s, translations will not occur!",
                    locale.getCountry(), locale.getLanguage()), e);
        }
    }

    public Translator(final Locale locale) {
        this(locale, null);
    }

    private String getRootPath(Project project) {
        String rootPath;
        if (project != null) {
            rootPath = String.format("%s/%s", project.toString().toLowerCase(), PATH);
        } else {
            rootPath = PATH;
        }
        return rootPath;
    }

    public Locale getLocale() {
        return this.locale;
    }

    public void setLocale(Locale locale) {
        logger.info(String.format(
                "Translator has been reassigned to provide localization for %s - %s",
                locale.getCountry(), locale.getLanguage()));

        this.locale = locale;
    }

    /**
     * Takes a message and translates it to the current locale
     *
     * @param message a key representing a text
     * @return a translated text value based on the input key.
     */
    public String translate(final String message) {
        return resourceBundle.getString(message);
    }

}
