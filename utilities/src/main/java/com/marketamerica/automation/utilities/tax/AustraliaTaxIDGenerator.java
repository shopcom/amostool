package com.marketamerica.automation.utilities.tax;

import static com.marketamerica.automation.utilities.helpers.NumberUtilities.summateIntArray;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.RandomStringUtils;

/**
 * This class generates valid ABN, ACN, and Personal Tax IDs.
 * Created by Javier L. Velasquez on 10/8/2014.
 */
public class AustraliaTaxIDGenerator {
    private static final Map<Integer, Integer> TAX_ID_WEIGHT;
    private static final Map<Integer, Integer> ABN_TAX_WEIGHT;
    private static final Map<Integer, Integer> ACB_TAX_WEIGHT;

    static {
        TAX_ID_WEIGHT = new HashMap<>();
        TAX_ID_WEIGHT.put(0, 1);
        TAX_ID_WEIGHT.put(1, 4);
        TAX_ID_WEIGHT.put(2, 3);
        TAX_ID_WEIGHT.put(3, 7);
        TAX_ID_WEIGHT.put(4, 5);
        TAX_ID_WEIGHT.put(5, 8);
        TAX_ID_WEIGHT.put(6, 6);
        TAX_ID_WEIGHT.put(7, 9);

        ABN_TAX_WEIGHT = new HashMap<>();
        ABN_TAX_WEIGHT.put(0, 10);
        ABN_TAX_WEIGHT.put(1, 1);
        ABN_TAX_WEIGHT.put(2, 3);
        ABN_TAX_WEIGHT.put(3, 5);
        ABN_TAX_WEIGHT.put(4, 7);
        ABN_TAX_WEIGHT.put(5, 9);
        ABN_TAX_WEIGHT.put(6, 11);
        ABN_TAX_WEIGHT.put(7, 13);
        ABN_TAX_WEIGHT.put(8, 15);
        ABN_TAX_WEIGHT.put(9, 17);
        ABN_TAX_WEIGHT.put(10, 19);

        ACB_TAX_WEIGHT = new HashMap<>();
        ACB_TAX_WEIGHT.put(0, 8);
        ACB_TAX_WEIGHT.put(1, 7);
        ACB_TAX_WEIGHT.put(2, 6);
        ACB_TAX_WEIGHT.put(3, 5);
        ACB_TAX_WEIGHT.put(4, 4);
        ACB_TAX_WEIGHT.put(5, 3);
        ACB_TAX_WEIGHT.put(6, 2);
        ACB_TAX_WEIGHT.put(7, 1);

    }

    /**
     * Generates a ACN.
     * <p>
     * Australian Company Numbers are nine-digit numbers used to identify companies
     * registered under the (Australian) Common Corporations Act 2001.
     *
     * @return a valid ACN.
     */
    public static String generateCompanyNumber() {
        final String id = RandomStringUtils.randomNumeric(8);
        final char[] chars = id.toCharArray();
        final int[] pos = new int[chars.length];


        for (int i = 0; i < chars.length; i++) {
            pos[i] = Integer.valueOf(String.valueOf(chars[i])) * ACB_TAX_WEIGHT.get(i);
        }

        final int remainder = summateIntArray(pos) % 10;
        final int checksum = 10 - remainder;

        if (checksum == 10) {
            return id + (String.valueOf(0));
        } else {
            return id + (String.valueOf(checksum));
        }
    }

    /**
     * Generates a valid ABN.
     * A unique identifier for Australian Businesses.
     *
     * @return a valid ABN
     */
    public static String generateBusinessNumber() {
        final String id = RandomStringUtils.randomNumeric(10);
        final char[] chars = id.toCharArray();
        final int[] pos = new int[chars.length];

        chars[0] = Character.valueOf((char) (Integer.valueOf(chars[0]) - 1));

        for (int i = 0; i < chars.length; i++) {
            pos[i] = Integer.valueOf(String.valueOf(chars[i])) * ABN_TAX_WEIGHT.get(i);
        }

        final String checkDigit = String.valueOf(summateIntArray(pos) % 89);
        return id + checkDigit.substring(0, 1);
    }

    /**
     * A Tax FileNumber (TFN) is a nine digit number used to represent a tax payer (individual
     * or company).
     *
     * @return a valid TFN
     */
    public static String generateTaxID() {
        final String id = RandomStringUtils.randomNumeric(8);
        return id + getTaxIDCheckDigit(id);
    }

    private static String getTaxIDCheckDigit(String id) {
        final char[] chars = id.toCharArray();
        final int[] pos = new int[chars.length];

        for (int i = 0; i < chars.length; i++) {
            pos[i] = Integer.valueOf(String.valueOf(chars[i])) * TAX_ID_WEIGHT.get(i);
        }
        final String checkDigit = String.valueOf(summateIntArray(pos) % 11);
        return checkDigit;
    }

}
