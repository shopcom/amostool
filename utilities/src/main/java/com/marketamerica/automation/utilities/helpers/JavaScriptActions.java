package com.marketamerica.automation.utilities.helpers;

import static org.apache.log4j.LogManager.getLogger;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * This class allows us to perform Java Script actions against a web page
 *
 * @author javierv
 */
public final class JavaScriptActions {
    protected final Logger logger = getLogger(JavaScriptActions.class);
    private final JavascriptExecutor javascriptExecutor;

    /**
     * Create a new JavaScript Actions class with a JavaScript Executor object
     *
     * @param driver
     */
    public JavaScriptActions(final WebDriver driver) {
        javascriptExecutor = (JavascriptExecutor) driver;
    }

    /**
     * This method executes javascript
     *
     * @param command     javascript command
     * @param webElements arguments to javascript command
     */
    private Object executeScript(final String command,
                                 final Object... webElements) {
        return javascriptExecutor.executeScript(command, webElements);
    }

    /**
     * Overloading method executeScript with no webelements Param
     *
     * @param command
     * @return
     */
    private Object executeScript(final String command) {
        return javascriptExecutor.executeScript(command);
    }

    /**
     * Execute a change event on an input element. This is useful if the
     * webdriver does not perform this action for us. As of 6/12/2014 this is an
     * outstanding issue with Safari WebDriver
     *
     * @param element an input web element
     */
    public void executeChangeEventOnElement(final WebElement element) {
        waitForJQueryToLoad();
        executeScript("$(arguments[0]).change(); return true;", element);
    }

    public void scrollToElement(WebElement element) {
        executeScript("arguments[0].scrollIntoView(true);", element);
    }

    /**
     * execute Mouse Over to an Element.
     *
     * @param element
     */
    public void mouseOverToElement(WebElement element) {
        String mouseOverScript = "if(document.createEvent){var evObj = document.createEvent('MouseEvents');evObj.initEvent('mouseover', true, false); "
                + "arguments[0].dispatchEvent(evObj);} else if(document.createEventObject) { arguments[0].fireEvent('onmouseover');}";
        executeScript(mouseOverScript, element);
    }

    /**
     * Select a select's option based on a given value
     *
     * @param dropDown a web element representing a drop down
     * @param value    a value to select that represents an option associated with the drop down
     */
    public void selectDropDownByValue(WebElement dropDown, Object value) {
        executeScript("$(arguments[0]).attr(\"selected\", \"selected\")", dropDown.findElements(By.cssSelector("option[value='" + value + "']")));
    }

    /**
     * Set an element's value
     *
     * @param element a web element representing a field
     * @param value   a value to set the field's value to
     */
    public void setField(WebElement element, Object value) {
        executeScript("$(arguments[0]).val(\"" + value + "\");", element);
    }

    /**
     * Check to see if jQuery is actually loaded
     *
     * @return
     */
    private boolean isJqueryPresent() {
        return Boolean.parseBoolean(String.valueOf(executeScript("return (typeof jQuery != 'undefined')")));
    }

    /**
     * Wait for jQuery to load
     */
    public void waitForJQueryToLoad() {
        int timeWaited = 0;
        while (!isJqueryPresent() && (timeWaited < 5)) {
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                logger.error(e);
            }
            ++timeWaited;
        }
        if (timeWaited > 0) {
            logger.debug(String
                    .format("Waiting %s milliseconds for jQuery to load",
                            (timeWaited * 250)));
        } else {
            logger.warn("Did not have to wait for jQuery to load");
        }

    }

    /**
     * Scroll to End Of Page whose length is known and is not dynamic
     */
    public void scrollToEndOfPage() {
        String scrollOverCommand = "window.scrollTo(0, document.body.scrollHeight);";
        executeScript(scrollOverCommand);
    }

    /**
     * Click an element using native javascript. This is useful in instances where the webdriver can't click on an
     * element using its built in API.
     * @param element a web element to invoke {@code click} on
     */
    public void click(WebElement element) {
        executeScript("arguments[0].click()", element);
    }

    /**
     * @return if page is loaded
     */
    private boolean isPageLoaded() {
        return executeScript("return document.readyState").equals("complete");
    }

    /**
     * Check if the page has refreshed or not
     */
    public void waitForPageToRefresh() {
        int timeWaited = 0;
        while (!isPageLoaded() && timeWaited < 10) {
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                logger.error(e);
            }
            ++timeWaited;
        }
        if (timeWaited > 0) {
            logger.debug(String
                    .format("Waiting %s milliseconds for page to refresh",
                            (timeWaited * 250)));
        } else {
            logger.warn("Did not have to wait for page to refresh");
        }
    }
    
    /**
     * set Value of an Input Field using Javascript. 
     * Takes input of css Selector of the element and the value to be set in the input field
     * @param cssSelector
     * @param value
     */
    public void setValueOfAnInputField(String cssSelector , String value){
    	String command = ("$(\"" + cssSelector + "\").val('" + value +"')");
    	logger.info(String.format("Executing command - %s", command ));
    	executeScript(command);
    	// need to some times executeChangeEventOnElement after updating a field 
    }
}
