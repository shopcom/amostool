package com.marketamerica.automation.utilities.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * Created by javierv on 5/6/2014.
 */
public class ExcelWorkbookReader extends ExcelWorkBook {

    public ExcelWorkbookReader(final String fileName) {
        this(new File(fileName));
    }

    public ExcelWorkbookReader(File workBookFile) {
        FileInputStream workBookInputStream = null;
        try {
            workBookInputStream = new FileInputStream(workBookFile);
        } catch (FileNotFoundException e) {
            logger.error(e);
        }
        logger.debug(String.format("Opening workbook at: %s", workBookFile.getAbsolutePath()));

        try {
            workbook = new XSSFWorkbook(workBookInputStream);
        } catch (IOException e) {
            logger.error(
                    String.format("Could not create workbook for %s", workBookFile.getName()), e);
        }

        try {
            workBookInputStream.close();
        } catch (IOException e) {
            logger.error(
                    String.format("Could not close workbook for %s", workBookFile.getName()), e);
        }
    }


    public List<Map<String, String>> getSheetData(final String sheetName) {
        final List<Map<String, String>> testRowData = new ArrayList<>();
        final Sheet sheet = workbook.getSheet(sheetName);
        final int numberOfRows = sheet.getPhysicalNumberOfRows();
        final Row columnHeaders = sheet.getRow(0);
        for (int i = 1; i < numberOfRows; i++) {
            final Row currentRow = sheet.getRow(i);
            if (isValidRow(currentRow))
                testRowData.add(getRowData(columnHeaders, currentRow));
        }
        return testRowData;
    }


    public List<Map<String, List<String>>> getSheetDataAsMultiMap(final String sheetName) {
        final List<Map<String, List<String>>> testRowData = new ArrayList<>();
        final Sheet sheet = workbook.getSheet(sheetName);
        if (sheet == null) {
            throw new IllegalArgumentException(String.format("Sheet (%s) doesn't exist.  Contained (%s)", sheetName,
                    getSheetNames()));
        }
        final int numberOfRows = sheet.getPhysicalNumberOfRows();
        final Row columnHeaders = sheet.getRow(0);
        for (int i = 1; i < numberOfRows; i++) {
            final Row currentRow = sheet.getRow(i);
            // Only process valid rows (e.g. rows with only null content)
            if (isValidRow(currentRow) && !isCommentRow(currentRow)) {
                testRowData.add(getRowDataAsMultiMap(columnHeaders, currentRow));
            }
        }
        return testRowData;
    }

    public List<String> getColumns() {
        return getRowData(0);
    }

    public boolean hasColumn(String columnName) {
        final List<String> columns = getColumns();
        for (final String column : columns) {
            if (column.toLowerCase().trim().equals(columnName.toLowerCase().trim())) {
                return true;
            }
        }
        return false;
    }

    public List<String> getSheetNames() {
        List<String> sheetNames = new ArrayList<>();
        int numberOfSheets = this.workbook.getNumberOfSheets();
        for (int i = 0; i < numberOfSheets; ++i) {
            sheetNames.add(workbook.getSheetName(i));
        }
        return sheetNames;

    }

}
