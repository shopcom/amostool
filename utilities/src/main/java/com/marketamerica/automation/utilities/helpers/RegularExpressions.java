package com.marketamerica.automation.utilities.helpers;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class will hold regular expressions that may be frequently used.
 * Created by Javier L. Velasquez on 10/3/2014.
 */
public class RegularExpressions {
    public static final String DOLLAR_AMOUNT = "\\$(?=.*\\d)\\d{0,15}(\\.\\d{1,2})?";

    /**
     * Matches a regex pattern against a string and return a list of matched
     * lists of matches
     *
     * @param string a string to search
     * @param regex  a regex pattern (groups supported)
     * @return a list of matched groups represented as a list of strings
     */
    public static List<List<String>> matchPatternAsGroups(final String string,
                                                          final String regex) {
        final Pattern pattern = Pattern.compile(regex);
        final Matcher matcher = pattern.matcher(string);
        final List<List<String>> matches = new ArrayList<>();
        while (matcher.find()) {
            final List<String> match = new ArrayList<>();
            for (int i = 0; i <= matcher.groupCount(); ++i) {
                match.add(matcher.group(i));
            }
            matches.add(match);
        }
        return matches;
    }

    /**
     * Matches a given pattern and returns a list of values that matched the provided pattern
     *
     * @param string a string that may or may not contain one or more instances of the provided pattern
     * @param regex  a regular expression pattern to look for
     * @return a list of values that matched the provided pattern
     */
    public static List<String> matchPattern(final String string,
                                            final String regex) {
        final List<List<String>> lists = matchPatternAsGroups(string, regex);
        final List<String> output = new ArrayList<>();
        lists.forEach(list -> output.add(list.get(0)));
        return output;
    }

    /**
     * Matches a regex pattern against a string and return the first match
     *
     * @param string a string to search
     * @param regex  a regex pattern (groups supported)
     * @return the first matching pattern
     */
    public static String matchFirstPattern(final String string,
                                           final String regex) {
        return matchPatternAsGroups(string, regex).get(0).get(0);
    }
}
