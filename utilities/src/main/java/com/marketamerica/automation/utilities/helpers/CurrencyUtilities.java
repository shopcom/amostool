package com.marketamerica.automation.utilities.helpers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.Locale;
import java.util.StringTokenizer;

/**
 * This class will hold all utility methods concerning currency.
 *
 * @author javierv
 */
public final class CurrencyUtilities {
	public static final BigDecimal FREE = new BigDecimal("0.00");
	public static final BigDecimal ZERO = new BigDecimal("0.00");
    public static final BigDecimal SEVEN_POINT_FIVE = new BigDecimal("7.50");     
    public static final BigDecimal FIVE = new BigDecimal("5.00");    
    public static final BigDecimal THREE_POINT_SEVEN_FIVE = new BigDecimal("3.75");
    public static final BigDecimal ONE_POINT_TWENTY_FIVE = new BigDecimal("1.25");    
    public static final BigDecimal TEN_POINT_FIVE = new BigDecimal("10.50");
    public static final BigDecimal TEN = new BigDecimal("10.00");    
    public static final BigDecimal ONE_HUNDRED_TWENTY_FIVE = new BigDecimal("125");    
    public static final BigDecimal SIX = new BigDecimal("6.00");
    public static final BigDecimal FOUR = new BigDecimal("4.00");
    public static final BigDecimal FIVE_POINT_THREE_FIVE = new BigDecimal("5.35");
    public static final BigDecimal SIX_POINT_FOUR_TWO = new BigDecimal("6.42");
    public static final BigDecimal ELEVEN_POINT_SEVEN_SEVEN = new BigDecimal("11.77");
    public static final BigDecimal TWO = new BigDecimal("2.00");
    public static final BigDecimal TWENTY_FIVE = new BigDecimal("25.00");
    public static final BigDecimal ONE_THOUSAND = new BigDecimal("1000.00");
    public static final BigDecimal TWELVE = new BigDecimal("12.00");
    public static final BigDecimal FORTEEN = new BigDecimal("14.00");
    public static final BigDecimal TEN_PERCENT = new BigDecimal(".10");
    public static final BigDecimal FIFTEEN = new BigDecimal("15.00");
    public static final BigDecimal NINE_HUNDRED = new BigDecimal("900.00");
    public static final BigDecimal EIGHT = new BigDecimal("8.00");
    public static final BigDecimal ONE_HUNDRED = new BigDecimal("100.00");    
    public static final BigDecimal ONE_THOUSAND_FIVE_HUNDRED = new BigDecimal("1500");
    public static final BigDecimal THREE_HUNDRED_TWENTY = new BigDecimal("320.00");
    public static final BigDecimal TWO_HUNDRED_SEVENTY = new BigDecimal("270.00");
    public static final BigDecimal FIFTY = new BigDecimal("50.00");
    public static final BigDecimal TWO_HUNDRED = new BigDecimal("200.00");
    public static final BigDecimal ONE_HUNDRED_FIFTY = new BigDecimal("150.00");
    public static final BigDecimal FOUR_HUNDRED_FIFTY = new BigDecimal("450.00");    
    public static final BigDecimal FOUR_THOUSAND = new BigDecimal("4000");
    public static final BigDecimal FIVE_THOUSAND_SIX_HUNDRED = new BigDecimal("5600");
    public static final BigDecimal SIX_THOUSAND = new BigDecimal("6000");
    public static final BigDecimal THREE_HUNDRED_FIFTY = new BigDecimal("350.00");
    public static final BigDecimal NINETY_NINE = new BigDecimal("99.00");
    public static final BigDecimal ONE_HUNDRED_THIRTY = new BigDecimal("135.00");
    public static final BigDecimal ONE_HUNDRED_FORTY = new BigDecimal("140.00");
    
    public static final BigDecimal THREE_POINT_SEVEN_FIVE_PERCENT = new BigDecimal("0.375");
    public static final BigDecimal SIX_PERCENT = new BigDecimal("0.06");
    public static final BigDecimal SEVEN_POINT_FIVE_PERCENT = new BigDecimal(".075");       
    public static final BigDecimal NINE_PERCENT = new BigDecimal(".09");
    public static final BigDecimal NINE_POINT_FIVE_PERCENT = new BigDecimal(".095");
    public static final BigDecimal ELEVEN_PERCENT = new BigDecimal(".11");
    public static final BigDecimal TWELVE_PERCENT = new BigDecimal(".12");  
    public static final BigDecimal FORTEEN_PERCENT = new BigDecimal(".14");

    /**
     * Verifies that the page is
     * displaying in the correct currency
     *
     * @param locale Locale object where web page is
     * @param price  String price being checked for currency
     * @return boolean if currency is correct or not
     */
    public static boolean verifyCorrectCurrency(final Locale locale, final String price) {
        return price.contains(Currency.getInstance(locale).getSymbol());
    }

    /**
     * Returns the name of the correct currency
     * for the current country the site is in
     *
     * @param locale Locale to find currency name of
     * @return String name of currency in current locale
     */
    public static String getCurrencyName(final Locale locale) {
        return Currency.getInstance(locale).getDisplayName();
    }

    /**
     * Returns the currency symbol of the correct
     * currency for the current country the site is in
     *
     * @param locale Locale to find currency symbol
     * @return String currency symbol in correct locale
     */
    public static String getCurrencySymbol(final Locale locale) {
        return Currency.getInstance(locale).getSymbol();
    }

    /**
     * Converts a String of a price for
     * a numeric value
     *
     * @param price String of price to be converted
     * @return BigDecimal numeric value for price
     */
    public static BigDecimal convertPriceStringToBigDecimal(final String price) {
         if (price == null || price.isEmpty()) {
        	 return new BigDecimal("0");
         }
        // Take input String, remove all non-numerical
        // characters and periods by replacing these
        // characters with nothing
        final String amount = price.replaceAll("[^\\d.]", "");

        // Create new instance of BigDecimal, rounding
        // up with two decimal places for currency
        final BigDecimal bigDecimal = new BigDecimal(amount);
        bigDecimal.setScale(2, BigDecimal.ROUND_UP);
        return bigDecimal;
    }

    /**
     * Parses a String that has multiple numbers in it (like shipping option)
     * for a price and returns it as a BigDecimal for calculations
     * <p>
     * This method works if a currency symbol can divide
     * a String into two substrings, with the price being
     * the second substring. Returns the second substring
     * as a BigDeciaml of the value
     * <p>
     * Example: "USPS Priority Mail (1-6 business days) $5.00"
     * can be divided into "USPS Priority Mail (1-6 business days)"
     * and "5.00". Returns a BigDecimal containing 5.00
     * <p>
     * Use this method if a calculation must be done
     *
     * @param input  String containing price
     * @param locale what Locale we are in to determine currency symbol
     *               and to determine number of decimal places of currency
     * @return BigDecimal of price
     */
    public static BigDecimal getPriceFromString(final String input, final Locale locale) {

        // StringTokenizer that uses currency symbol (ex: $ for U.S. dollars)
        // to locate actual monetary amount in Strings like shipping options
        StringTokenizer tokenizer = new StringTokenizer(input, Currency.getInstance(locale).getSymbol(locale));

        // Grab first non-monetary amount of String
        tokenizer.nextToken();

        // Grab the monetary amount of the String
        final String onlyPriceString = tokenizer.nextToken().trim();

        // Convert to BigDecimal
        final BigDecimal price = new BigDecimal(onlyPriceString);

        // Set decimal places to correct number based upon locale's currency
        price.setScale(Currency.getInstance(locale).getDefaultFractionDigits(), RoundingMode.CEILING);

        return price;
    }

    /**
     * Parses a String that has multiple numbers in it (like shipping option)
     * for a price and returns it as a BigDecimal for calculations
     * <p>
     * This method works if a currency symbol can divide
     * a String into three substrings, with the currency
     * symbol being the second substring and the price being
     * the third substring. The return value is the second and
     * third substring concatenated together.
     * <p>
     * Example: "USPS Priority Mail (1-6 business days) $5.00"
     * can be divided into "USPS Priority Mail (1-6 business days)"
     * "$" and "5.00". Return value would be "$5.00"
     * <p>
     * Use this method if the number is to be printed out for reporting
     *
     * @param input  String containing price
     * @param locale what Locale we are in to determine currency symbol
     * @return String of price
     */
    public static String getPriceFromStringForReporting(final String input, final Locale locale) {
        // StringTokenizer that uses currency symbol (ex: $ for U.S. dollars)
        // to locate actual monetary amount in Strings like shipping options
        StringTokenizer tokenizer = new StringTokenizer(input, Currency.getInstance(locale).getSymbol(locale), true);

        // Grab first non-monetary amount of String
        tokenizer.nextToken();

        final String priceWithSymbol = String.format("%s%s", tokenizer.nextToken(), tokenizer.nextToken());

        // Grab the monetary amount of the String
        return priceWithSymbol;
    }

    public static BigDecimal formatBigDecimal(BigDecimal bigDecimal) {
        return bigDecimal.setScale(2, RoundingMode.HALF_UP);
    }

    public static BigDecimal convertEuroForCalculations(final String input) {
        String newString = input.replaceAll("[^\\d,]", "");
        newString = newString.replace(",", ".");
        return new BigDecimal(newString).setScale(2, RoundingMode.CEILING);
    }
    
    // convert a decimal to a Euro format    
	public static String convertToEuroAmountFormat(final BigDecimal bigDecimal) {
		String amount = bigDecimal.toString();
		String euroAmount = amount.replace(".", ",");
		return euroAmount;
	} 
    
    /**
     * Calculate what quantity of current item
     * go over a certain given value
     *
     * @param targetAmount amount trying to go over
     * @param unitCost     how much a unit a product is
     * @return quantity needed to exceed targetAmount as int
     */
    public static int calculateRequiredQuantity(final BigDecimal targetAmount, final BigDecimal unitCost) {
        final BigDecimal temp = targetAmount.divide(unitCost, 2, RoundingMode.CEILING);
        return temp.intValue() + 1;
    }

}