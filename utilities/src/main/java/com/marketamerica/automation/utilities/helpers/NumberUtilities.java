package com.marketamerica.automation.utilities.helpers;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Random;

import com.marketamerica.automation.testdata.Product;

/**
 * This class will hold all utility methods concerning numbers objects (e.g.
 * int, float, long)
 *
 * @author javierv
 */
public final class NumberUtilities {

    /**
     * Generate a random integer up to a value
     *
     * @param random   a Random object
     * @param maxValue the max value that the integer can be
     * @return a random integer value up to the given max
     */
    public static int generateRandomIntegerUpTo(final Random random,
                                                final int maxValue) {
        final int length = random.nextInt(maxValue + 1);
        return length;
    }

    /**
     * Generate a random integer between the given min and max
     *
     * @param random  a Random object
     * @param minimum a floor
     * @param maximum a ceiling
     * @return an integer that meets the requirements of the given arguments.
     */
    public static int randomInteger(final Random random, final int minimum,
                                    final int maximum) {
        return random.nextInt((maximum - minimum) + 1) + minimum;
    }

    /**
     * Calculate the required quantity of a product
     * to exceed a certain price amount
     *
     * @param targetAmount BigDecimal amount to exceed
     * @param product      Product being used to exceed targetAmount
     * @return int how many of product to use to exceed targetAmount
     */
    public static int calculateQuantityToExceedCost(final BigDecimal targetAmount, final Product product) {
        final BigDecimal quantity = targetAmount.divide(product.getRetailCost(), 2, RoundingMode.CEILING);
        return quantity.intValue() + 1;
    }

    /**
     * Parse a String containing a whole
     * number value and get the number back
     *
     * @param input String of number
     * @return String of whole numeric value desired
     */
    public static String parseStringForWholeNumber(final String input) {

        // Take input String, remove all non-numerical
        // characters by replacing these characters with nothing
        return input.replaceAll("[^\\d]", "");
    }

    /**
     * @param array
     * @param <T>
     * @return
     */
    public static <T> T getRandomValueInArray(T[] array) {
        return array[new Random().nextInt(array.length)];
    }

    public static int[] convertStringToIntArray(String numberAsString) {
        int[] array = new int[numberAsString.length()];
        for (int i = 0; i <= numberAsString.length() - 1; ++i) {
            array[i] = Integer.parseInt(numberAsString.substring(i, i + 1));
        }
        return array;
    }

    public static int summateIntArray(int[] numbers) {
        int total = 0;
        for (int number : numbers) {
            total += number;
        }
        return total;

    }
}
