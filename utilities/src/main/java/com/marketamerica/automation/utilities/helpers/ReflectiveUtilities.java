package com.marketamerica.automation.utilities.helpers;

import static java.util.Arrays.asList;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.testng.ITestResult;
import org.testng.annotations.Test;

import com.marketamerica.automation.annotations.TestDoc;
import com.marketamerica.automation.annotations.XMLDataDrivenTest;
import com.marketamerica.automation.annotations.SQLDataDrivenTest;
import com.marketamerica.automation.testdata.TestLinkID;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;

/**
 * This class should contain "Reflective" functions utilized by the automation
 * framework
 *
 * @author Javier L. Velasquez
 */
public final class ReflectiveUtilities {
    private static final Logger logger = LogManager
            .getLogger(ReflectiveUtilities.class);

    /**
     * Get the supported countries found in a test method
     *
     * @param method a object representing a test method
     * @return a list of countries specified in the method's test documentation
     * annotation
     */
    public static List<Country> getSupportedCountries(Method method) {
        return asList(getTestDoc(method)
                .supportedCountries());
    }

    /**
     * Get the supported environments found in a test method
     *
     * @param method a object representing a test method
     * @return a list of environments specified in the method's test
     * documentation annotation
     */

    public static List<Environment> getSupportedEnvironments(Method method) {
        return asList(getTestDoc(method)
                .supportedEnvironments());
    }

    public static String getMethodExternalId(final ITestResult iTestResult) {
        return getMethodExternalId(getMethod(iTestResult));
    }

    /**
     * Get a method's external ID
     *
     * @param method a test method object
     * @return a string value representing its Id
     */
    public static String getMethodExternalId(final Method method) {
        // Make sure the test has an external Id
        if (getTestDoc(method) == null
                || getTestDoc(method).ids() == null
                || getTestDoc(method).ids().length == 0) {
            return "None";
        } else {
            return getTestDoc(method).ids()[0];
        }
    }

    /**
     * Returns a method version number
     *
     * @param method a test method object
     * @return a integer value representing its version number that should
     * correspond to its version on test link
     */
    public static int getMethodVersion(final Method method) {
        return getTestDoc(method).version();
    }

    /**
     * This method returns the address keys listed in a test method via the
     * XMLDataDrivenTest annotation
     *
     * @param method a testng method object
     * @return a list of address keys associated with the test method
     * @see XMLDataDrivenTest
     */
    public static List<String> getAddressKeys(final Method method) {
        return asList(method.getAnnotation(XMLDataDrivenTest.class)
                .addressKeys());

    }

    /**
     * This method returns the distributor keys listed in a test method via the
     * XMLDataDrivenTest annotation
     *
     * @param method a testng method object
     * @return a list of distributor keys associated with the test method
     * @see XMLDataDrivenTest
     */
    public static List<String> getDistributorKeys(final Method method) {
        return asList(method.getAnnotation(XMLDataDrivenTest.class)
                .distributerKeys());
    }

    /**
     * Get a test method's description
     *
     * @param method a TestNG method object
     * @return the declared method's test description
     */
    public static String getMethodDescription(final Method method) {
        if (getTestDoc(method).description() != null) {
            return getTestDoc(method).description();
        } else {
            return "No Description Provided";
        }
    }

    /**
     * Return a test methods declared requirements
     *
     * @param method a TestNG method
     * @return an array of strings represents the requirements that the given
     * test is associated with
     */
    public static String[] getMethodRequirements(final Method method) {
        return getTestDoc(method).requirements();
    }

    /**
     * This method determines whether or not a method has defined the Test
     * Documentation annotation
     *
     * @param method a method representing a test method object
     * @return a boolean indicating if the test documentation annotation was
     * found associated with the method
     */
    public static boolean hasTestDocumentationAnnotation(final Method method) {
        return method.isAnnotationPresent(TestDoc.class);
    }


    /**
     * This method determines whether or not a method has defined the Test
     * annotation
     *
     * @param method a method representing a test method object
     * @return a boolean indicating if the Test annotation was
     * found associated with the method
     */
    public static boolean hasTestAnnotation(final Method method) {
        return method.isAnnotationPresent(Test.class);
    }


    /**
     * Get a stack trace as a string
     *
     * @param stackTraceElements   an array of stack trace elements
     * @param filterNativeElements boolean indicating whether or not to filter native elements
     *                             (e.g. java.lang or org.testng)
     * @return A string representing an array of stack trace elements delimited
     * by a new line.
     */
    public static String getStackTraceAsString(
            final StackTraceElement[] stackTraceElements,
            boolean filterNativeElements) {

        String throwableString = "";
        for (StackTraceElement stackTraceElement : stackTraceElements) {
            final String stackTraceElementString = stackTraceElement.toString();
            if (filterNativeElements
                    && stackTraceElementString
                    .matches("^((sun\\.reflect)|(java\\.(lang|util))|(org\\.testng)).*")) {
                continue;
            }
            throwableString = throwableString + stackTraceElementString;
        }
        return throwableString.trim();
    }

    /**
     * Get a string that uniquely identifies a test's Throwable object. In most
     * instances it will return the Throwable message but in some rare instances
     * we don't care about the message and only care about the instance type. In
     * these instances we return a canonical name representing the Throwable
     *
     * @param throwable a ITestResult Throwable
     * @return a string value to identify the unique Throwable instance
     */
    public static String getThrowableAsString(final Throwable throwable) {
        if (throwable instanceof NullPointerException) {
            return throwable.getClass().getCanonicalName();
        } else if (throwable instanceof UnreachableBrowserException) {
            return throwable.getClass().getCanonicalName();
        } else if (throwable instanceof TimeoutException) {
            return throwable.getClass().getCanonicalName();
        } else if (throwable == null) {
            return "No Throwable provided!";
        } else {
            return throwable.getMessage();
        }
    }

//    /**
//     * If this method is invoked from a page object it returns the page object
//     * name based on the defined PageDocumentation annotation
//     *
//     * @return a string value determined by the Page object's
//     * 'PageDocumentation' annotation name() value
//     */
//    public static String getPageObjectName() {
//        Class<?> clazz = null;
//        try {
//            clazz = Class.forName(Thread.currentThread().getStackTrace()[3]
//                    .getClassName());
//        } catch (ClassNotFoundException e) {
//            logger.error(e);
//            return "";
//        }
//        return clazz.getAnnotation(PageDocumentation.class).name();
//    }

    /**
     * This method allows us to determine if a test method has a data driven
     * provided based on its Test annotation
     *
     * @param method a test method object
     * @return true if the test has a data driven provider specified. false if
     * no data driven provider is specified
     * @see org.testng.annotations.Test
     */
    public static boolean hasDataDrivenProvider(Method method) {
        Test test = method.getAnnotation(Test.class);
        return test.dataProvider() != null && !test.dataProvider().isEmpty();
    }

    /**
     * Returns an iTestResults native Java Method
     *
     * @param iTestResult a TestNG ITestResult object
     * @return a native Java reflection method
     */
    public static Method getMethod(final ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getMethod();
    }

    /**
     * Get the retry count specified in the iTestResult object
     *
     * @param iTestResult a TestNG ITestResult object
     * @return the retry count specified in the TestDoc annotation
     */
    public static int getRetryCount(final ITestResult iTestResult) {
        return getRetryCount(getMethod(iTestResult));
    }

    /**
     * Get the retry count specified in the iTestResult object
     *
     * @param method a native Java reflection method
     * @return the retry count specified in the TestDoc annotation
     */
    public static int getRetryCount(final Method method) {
        if (hasTestDocumentationAnnotation(method)) {
            return getTestDoc(method).retryCount();
        } else {
            logger.warn(String.format("Method \"%s\" does not have TestDocumentation", method.getName()));
            return 0;
        }
    }

    /**
     * Return the Test Method object in the current thread.
     *
     * @return a Method object representing the current Test. Null if no test method is found
     */
    public static Method getTestMethodInCurrentThread() {
        final StackTraceElement[] stackTraceElements = Thread.currentThread().getStackTrace();

        for (final StackTraceElement stackTraceElement : stackTraceElements) {
            final Class<?> clazz = getClass(stackTraceElement.getClassName());
            final String currentMethodName = stackTraceElement.getMethodName();

            if (clazz == null) {
                continue;
            }

            final Method method = getTestMethod(clazz, currentMethodName);
            if (method != null) {
                return method;
            }

        }
        logger.warn("No test method found in current thread");
        return null;
    }

    /**
     * Resolve a method object in a class by name.
     *
     * @param clazz      a Class in which you expect it to contain the requested method
     * @param methodName a string representing the method
     * @return a Method object that represents the method found in the class
     */
    public static Method getTestMethod(Class<?> clazz, final String methodName) {
        final Method[] methods = clazz.getMethods();
        for (final Method method : methods) {
            if (hasTestAnnotation(method)) {
                if (method.getName().equals(methodName)) {
                    return method;
                }
            }
        }
        return null;
    }

    /**
     * Return the corresponding class based on the given name
     *
     * @param className a string that represents a class. The typical format will be packageName.ClassName
     * @return a Class represented by the package+class.
     */
    public static Class<?> getClass(final String className) {
        Class<?> clazz = null;
        try {
            clazz = Class.forName(className);
        } catch (ClassNotFoundException e) {
            logger.error(e);
        }
        return clazz;
    }

    /**
     * Returns all the fields defined by the object including all private fields from super classes
     *
     * @param object an object to check for given fields
     * @return a collection of fields associated with the object
     */
    private static Collection<Field> getAllFields(final Object object) {
        Collection<Field> fields = new HashSet<>();
        Class<?> clazz = object.getClass();
        do {
            fields.addAll(Arrays.asList(clazz.getDeclaredFields()));
            clazz = clazz.getSuperclass();
        } while (clazz != Object.class);
        return fields;
    }

    /**
     * Returns a field based on a given name
     *
     * @param object    an object that is expected to define or inherit a field
     * @param fieldName a string value that defines the expected field
     * @return the expected field
     */
    public static Field getFieldByName(final Object object, final String fieldName) {
        Collection<Field> fields = getAllFields(object);
        Optional<Field> first = fields.stream().filter(field -> field.getName().equals(fieldName)).findFirst();
        if (first.isPresent()) {
            Field field = first.get();
            setAccessible(field);
            return field;
        } else {
            return null;
        }

    }

    /**
     * Return an object's field reflectively. This method is useful for getting private fields
     *
     * @param object    an object
     * @param fieldName a field that should be found on the object
     * @return an object that represents the field found in the instance of the object.
     */
    public static Object getFieldValue(final Object object, final String fieldName) {
        try {
            return getFieldByName(object, fieldName).get(object);
        } catch (IllegalAccessException e) {
            logger.error(e);
        }
        return new Object();
    }

    /**
     * Set an object's field reflectively. This method is useful for setting private fields
     *
     * @param clazz     a class definition used to describe the object who owns the field.
     * @param fieldName a field that should be found on the object
     * @param value     a value to set the field's value to
     */
    public static void setField(final Class<?> clazz, final String fieldName, final Object value) {
        setField(instantiateObject(clazz), fieldName, value);
    }

    /**
     * Set an object's field reflectively. This method is useful for setting private fields
     *
     * @param object    an object
     * @param fieldName a field that should be found on the object
     * @param value     a value to set the field's value to
     */
    public static void setField(final Object object, final String fieldName, final Object value) {
        Field field = getFieldByName(object, fieldName);
        setAccessible(field); // this is the magic
        try {
            field.set(object, value);
        } catch (IllegalAccessException e) {
            logger.error(e);
        }
    }

    private static Class<?>[] createObjectClassArray(Object... objects) {
        Class<?>[] clazzes = new Class[objects.length];
        for (int i = 0; i < objects.length; ++i) {
            clazzes[i] = objects[i].getClass();
        }
        return clazzes;
    }

    /**
     * Invoke a method on a new instance of the given class with the given parameters
     *
     * @param clazz      a class definition used to describe the object to construct
     * @param methodName a string that corresponds to the method to invoke
     * @param arguments  arguments to pass to the given method
     * @return the return of the method execution or an empty object if nothing is returned
     */
    public static Object invokeMethod(Class<?> clazz, final String methodName, Object... arguments) {
        return invokeMethod(instantiateObject(clazz), methodName, arguments);
    }

    /**
     * Get all inherited methods associated object.
     *
     * @param object an instantiated object
     * @return a collection of methods that are accessible via the param object
     */
    private static Collection<Method> getAllInheritedMethods(final Object object) {
        Collection<Method> methods = new HashSet<>();
        Class<?> clazz = object.getClass();
        do {
            methods.addAll(Arrays.asList(clazz.getDeclaredMethods()));
            clazz = clazz.getSuperclass();
        } while (clazz != Object.class);
        return methods;
    }

    /**
     * Search through a collection of methods and filter for a method that matches based on a name
     *
     * @param methods    a collection of Methods
     * @param methodName a string representing the method
     * @return a Method that is found in the collection to match the provided methodName
     */
    private static Method getMethodByName(Collection<Method> methods, final String methodName) {
        return methods.stream().filter(method -> method.getName().equals(methodName)).findFirst().get();
    }

    /**
     * Invoke a method on an object that may be possibly inherited. This method grabs the first method that matches the given methodName.
     * If there are overloaded methods there is a possibility an InvocationTargetException is throw if the given arguments don't match the found method.
     *
     * @param object     an instantiated object
     * @param methodName a method to invoke on the given object
     * @param arguments  arguments to pass to the given method.
     * @return the data returned based on the Method invocation.
     */
    public static Object invokeInheritedMethod(Object object, final String methodName, Object... arguments) {
        Method method = getMethodByName(getAllInheritedMethods(object),
                methodName);
        if (method == null) {
            logger.error(String.format("No method named \"%s\"was found associated with the object (\"%s\"", methodName, object));
            return new Object();
        }
        setAccessible(method);
        try {
            return method.invoke(object, arguments);
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.error(e);
        }
        return new Object();
    }

    /**
     * Invoke a method on a new instance of the given class with the given parameters
     *
     * @param object     an object to invoke the corresponding method against
     * @param methodName a string that corresponds to the method to invoke
     * @param arguments  arguments to pass to the given method
     * @return the return of the method execution or an empty object if nothing is returned
     */
    public static Object invokeMethod(Object object, final String methodName, Object... arguments) {
        Method method = null;

        try {
            method = object.getClass().getDeclaredMethod(
                    methodName,
                    createObjectClassArray(arguments));
        } catch (NoSuchMethodException e) {
            logger.error(e);
        }
        setAccessible(method);
        try {
            if (method != null) {
                return method.invoke(object, arguments);
            }
        } catch (IllegalAccessException | InvocationTargetException e) {
            logger.error(e);
        }
        return new Object();
    }

    /**
     * Returns a method's name in the following format: package.Class.methodName
     *
     * @param method a method object
     * @return a string value representing the method's name
     */
    public static String getFullMethodName(final Method method) {
        if (method == null) {
            return "";
        }
        final Class<?> clazz = method.getDeclaringClass();
        final String canonicalClassName = clazz.getCanonicalName();
        final String methodName = method.getName();
        return String.format("%s.%s", canonicalClassName, methodName);
    }

    /**
     * Create a new instance of an object. This method is particular useful for unit testing,
     * when a class cannot be otherwise instantiated (those with private constructors)
     *
     * @param clazz a class definition
     * @return an instance of the given class
     */
    public static <T> Object instantiateObject(Class<T> clazz) {
        Constructor<T> constructor;
        try {
            constructor = clazz.getDeclaredConstructor();
        } catch (NoSuchMethodException e) {
            logger.error(e);
            return new Object();
        }
        setAccessible(constructor);
        try {
            return constructor.newInstance();
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
            logger.error(e);
        }

        return new Object();
    }

    /**
     * Returns the oustanding jira defects associated with a test method
     *
     * @param method a TestNG test method that is annotated with the TestDocumentation annotation
     * @return a String array of Jira ticket numbers
     * @see <a href="https://jira.marketamerica.com"/>https://jira.marketamerica.com</a>
     */
    public static List<String> getOutstandingJiraDefects(Method method) {
        if (!hasTestDocumentationAnnotation(method)) {
            return new ArrayList<>();
        } else {
            return asList(getTestDoc(method).oustandingJiraDefects());
        }


    }

    /**
     * This method returns the test documentation of a given test method
     *
     * @param method a TestNG test method that is annotated with the TestDocumentation annotation
     * @return The given TestDoc
     * @see com.marketamerica.automation.annotations.TestDoc
     */
    private static TestDoc getTestDoc(Method method) {
        return method.getAnnotation(TestDoc.class);
    }

    /**
     * Check to see if an object has a defined field
     *
     * @param object an object to check for a given field
     * @param field  a field to search for
     * @return a boolean indicating of the field existed on the object
     */
    public static boolean hasFieldThatContainsValue(Object object, String field) {
        Object output = getFieldValue(object, field);
        return output != null && !output.toString().isEmpty();
    }

    /**
     * Takes an accessible object and make it accessible if it isn't already
     *
     * @param accessible an accessible object
     */
    private static void setAccessible(AccessibleObject accessible) {
        if (!accessible.isAccessible()) {
            accessible.setAccessible(true);
        }
    }

    /**
     * Get a test result test id. It will first check to see if the id is stored in a method's parameter. It will
     * look for a map structure containing the columns "ID" and "Version" ("Version" is optional). If this is not found
     * it will use the test documentation annotation
     *
     * @param result a TestNg test result
     * @return a TestLinkID structure
     */
    @SuppressWarnings("unchecked")
    public static TestLinkID getTestResultID(ITestResult result) {
        TestLinkID testLinkID = new TestLinkID();

        if (hasParameters(result) && result.getParameters()[result.getParameters().length - 1] instanceof Map) {
            final Object parameter = result.getParameters()[result.getParameters().length - 1];
            String version = ((Map<String, String>) parameter).get("Version");
            if (version == null || version.isEmpty()) {
                version = "1";
            }
            String id = ((Map<String, String>) parameter).get("ID");
            if (id == null) {
                id = ((Map<String, String>) parameter).get("id");
                if (id == null) {
                    id = ((Map<String, String>) parameter).get("ids");
                    if (id == null) {
                        id = ((Map<String, String>) parameter).get("IDS");
                        if (id == null) {
                            if (hasTestDocumentationAnnotation(getMethod(result))) {
                                final TestDoc testDoc = getTestDoc(getMethod(result));
                                return new TestLinkID(testDoc.ids(), testDoc.version());
                            } else {
                                return new TestLinkID();
                            }
                        }
                    }
                }
            }
            testLinkID = new TestLinkID(id.split(", "), Integer.valueOf(version));
        } else {
            testLinkID = getTestResultID(getMethod(result));
        }
        return testLinkID;
    }


    public static TestLinkID getTestResultID(Method method) {
        if (hasTestDocumentationAnnotation(method)) {
            final TestDoc testDoc = getTestDoc(method);
            return new TestLinkID(testDoc.ids(), testDoc.version());
        } else {
            return new TestLinkID();
        }
    }

    /**
     * Checks to see if a test has paremeters
     *
     * @param result a TestNG TestResult object
     * @return a boolean indicating that the test had at least one parameter
     */

    private static boolean hasParameters(ITestResult result) {
        return result.getParameters() != null && result.getParameters().length > 0;
    }

    public static boolean usesWebDriver(Method method) {
        if (hasTestDocumentationAnnotation(method)) {
            return method.getAnnotation(TestDoc.class).usesWebDriver();
        } else {
            return true;
        }
    }

    /**
     * Returns the persistence id of an persistence annotated object.
     * @param object an object that has been annotated with the necessary {@code javax.persistence} annotations.
     * @return an integer representing its persistence id.
     * @see javax.persistence
     */
    public static int getPersistenceId(Object object) {
        return Integer.valueOf(getFieldValue(object, "persistenceId").toString());
    }
    
    
    /**
     * This method returns the product category key listed in a test method via the
     * SQLDataDrivenTest annotation
     *
     * @param method a testng method object
     * @return a product category key associated with the test method
     * @see SQLDataDrivenTest
     */
    public static String getCategoryKeyForSQL(final Method method) {
        return method.getAnnotation(SQLDataDrivenTest.class)
                .productCategoryKey();
    }
    
    /**
     * This method returns the iteration number key listed in a test method via the
     * SQLDataDrivenTest annotation
     *
     * @param method a testng method object
     * @return a iteration number key associated with the test method
     * @see SQLDataDrivenTest
     */
    public static String getIterationKeyForSQL(final Method method) {
        return method.getAnnotation(SQLDataDrivenTest.class)
                .iterationKey();
    }

}
