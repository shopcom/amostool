package com.marketamerica.automation.utilities.enums;

import com.marketamerica.automation.utilities.helpers.NumberUtilities;

/**
 * Created by archanac on 3/16/2015.
 */
public enum CustomMiniProductCategories {
   Beauty("beauty"),
   HealthAndNutrition("Health & Nutrition"),
   HomeStore("Home Store"),
   Baby("Baby"),
   Jewelry("Jewelry"),
   Auto("Auto"),
   Clothes("Clothes"),
   PetSupplies("Pet Supplies"),
   Garden("Garden"),
   Music("Music"),
   Electronics("Electronics"),
   Books("Books");
    
    private String category;

    private CustomMiniProductCategories(final String category) {
        this.category = category;
    }

    public static CustomMiniProductCategories getRandom() {
        return NumberUtilities.getRandomValueInArray(values());
    }

    public String getValue() {
        return category;
    }

    @Override
    public String toString() {
        return category;
    }

}
