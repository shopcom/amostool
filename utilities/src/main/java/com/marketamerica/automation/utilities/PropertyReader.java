package com.marketamerica.automation.utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * The purpose of this class is to read property files
 * Created by Javier L. Velasquez on 7/20/2014.
 */
public final class PropertyReader {

    private final Logger logger = LogManager.getLogger(PropertyReader.class);
    private Properties properties;

    public PropertyReader(File file) throws FileNotFoundException {
        this(new FileInputStream(file));
    }

    public PropertyReader(InputStream inputStream) {
        properties = new Properties();

        try {
            properties.load(inputStream);
        } catch (IOException | IllegalArgumentException | NullPointerException e) {
            logger.error("Could not read InputStream for PropertyReader");
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }
    }

    /**
     * Returns a given property value based on a given key. This method returns empty is no key is found
     *
     * @param key a key value presenting an entry in the current property file
     * @return
     */
    public String get(final String key) {
        return properties.getProperty(key) == null ? "" : properties.getProperty(key);
    }

    /**
     * This method checks to see if a given key exists in the current property file
     *
     * @param key a key value presenting an entry in the current property file
     * @return a boolean indicating if the key was found
     */
    public boolean hasKey(final String key) {
        return properties.getProperty(key) != null && !properties.getProperty(key).isEmpty();
    }

}
