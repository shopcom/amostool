package com.marketamerica.automation.utilities.helpers;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Restrictions;

public final class ORMManager {
    final static Logger logger = LogManager.getLogger(ORMManager.class);

    /**
     * Specify where we are storing the hibernate config xml
     */

    private static String CONFIGURATION_FILE = "hibernate.cfg.xml";
    private SessionFactory sessionFactory;
    private StandardServiceRegistry serviceRegistry;

    public ORMManager(Class<?>[] clazzes) {
        initSessionFactory(clazzes);
    }

    public ORMManager() {
        initSessionFactory(new Class<?>[]{});
    }

    /**
     * Set the hibernate configuration xml file name
     *
     * @param configurationFile a string file that should reflect the location of where you
     *                          want to store the hibernate cfg xml
     */
    public static void setConfigurationFile(final String configurationFile) {
        CONFIGURATION_FILE = configurationFile;
    }

    /**
     * Create a session factory to allow us to interact with a table within a db
     * that should reflect a POJO
     */
    private void initSessionFactory(Class<?>[] clazzes) {
        final Configuration configuration = new Configuration()
                .configure(CONFIGURATION_FILE);

        for (int i = 0; i < clazzes.length; i++) {
            configuration.addAnnotatedClass(clazzes[i]);
        }

        serviceRegistry = new StandardServiceRegistryBuilder().applySettings(
                configuration.getProperties()).build();
        sessionFactory = configuration.buildSessionFactory(serviceRegistry);
    }

    /**
     * Save a record to the ui
     *
     * @param object an object that should be serialized to the ui
     */
    public void saveRecord(final Object object) {
        final Session session = createSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(object);
            transaction.commit();
            assert transaction.wasCommitted();
        } catch (HibernateException e) {
            if (transaction != null)
                transaction.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    /**
     * Find a record in the ui
     *
     * @param type the class that represents the record
     * @param id   the records primary key
     * @param <R>  The record return type as a POJO class
     * @return a record that matches the specified ID represented by the
     * specified type
     */
    @SuppressWarnings("unchecked")
    public <R> R findRecord(final Class<?> type, final int id) {
        final Session session = createSession();
        session.beginTransaction();
        final Object output = session.load(type, id);
        session.getTransaction().commit();
        return (R) output;
    }

    public <R> R findRecord(final Class<?> type, final String column,
                            final String value) {
        Map<String, Object> arguments = new HashMap<>();
        arguments.put(column, value);
        return findRecord(type, arguments);
    }

    @SuppressWarnings("unchecked")
    public <R> R findRecord(final Class<?> type, Map<String, Object> arguments) {
        final Session session = createSession();
        session.beginTransaction();
        final Set<String> filters = arguments.keySet();
        Criteria criteria = session.createCriteria(type);
        for (String filter : filters) {
            criteria.add(Restrictions.eq(filter, arguments.get(filter)));
        }
        return (R) criteria.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    public <R> R findRecords(final Class<?> type, Map<String, List<?>> arguments) {
        final Session session = createSession();
        session.beginTransaction();
        final Set<String> filters = arguments.keySet();
        Criteria criteria = session.createCriteria(type);
        for (String filter : filters) {
            List<?> filterArguments = arguments.get(filter);
            if (filterArguments.size() == 1) {
                criteria.add(Restrictions.eq(filter, filterArguments.get(0)));
            } else {
                criteria.add(Restrictions.in(filter, filterArguments));

            }
        }
        return (R) criteria.list();
    }

    /**
     * Create a new session with the ui
     *
     * @return a brand new session
     */
    private Session createSession() {
        return sessionFactory.openSession();
    }

    /**
     * Create a list representing all entries in a table
     *
     * @param tableName the table to search
     * @param <T>       the type of entry
     * @return a list of specified type
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> findAll(final String tableName) {
        final Session session = createSession();
        session.beginTransaction();
        final T output = (T) session.createQuery(
                String.format("from %s", tableName)).list();
        session.getTransaction().commit();
        return (List<T>) output;
    }

    @SuppressWarnings("unchecked")
    public <T> List<T> query(final String query) {
        final Session session = createSession();
        session.beginTransaction();
        final T output = (T) session.createQuery(query).list();
        session.getTransaction().commit();
        return (List<T>) output;
    }

    /**
     * Delete a record from the ui
     *
     * @param object a record to delete
     */
    public void deleteRecord(final Object object) {

        final Session session = createSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.delete(object);
            transaction.commit();
        } catch (HibernateException e) {
            if (transaction != null)
                transaction.rollback();
            throw e;
        } finally {
            session.close();
        }
    }

    @SuppressWarnings("unchecked")
    public Collection<String> customSqlQuery(final String queryString) {
        Query query = this.createSession().createSQLQuery(queryString);
        // Each review is a long description in String format
        return query.list();
    }

    @SuppressWarnings("unchecked")
    public Iterator<String[]> customSQLQueryWithMultipleColumns(final String queryString) {
        Query query = this.createSession().createSQLQuery(queryString);
        return (Iterator<String[]>) query.list().iterator();
    }

    public void terminate() {
        StandardServiceRegistryBuilder.destroy(serviceRegistry);

    }

    @Override
    public void finalize() throws Throwable {
        super.finalize();
        logger.info("Terminating ORM Manager Session");
        terminate();
    }
}