package com.marketamerica.automation.utilities.enums;

import java.util.Arrays;
import java.util.List;

/**
 * This enum represents the various delivery types for order
 * Created by kathy gong on 11/09/2014.
 */
public enum DeliveryType {

    SHIP("Ship"), PICK_UP("Pick Up", Arrays.asList("Pickup")), SHIP_ONLINE("Ship Online"), PICK_UP_LATER("Pick Up Later"), 
    ALREADY_DELIVERED("Already Delivered"), STANDARD_SHIPPING_DAY("Standard Shipping Day"), 
    STANDARD_SHIPPING_NIGHT("Standard Shipping Night"), STANDARD_SHIPPING("Standard Shipping");

    private String deliveryType;
    private List<String> otherSameTypes;

    private DeliveryType(final String deliveryType, List<String> otherSameTypes) {
        this.deliveryType = deliveryType;
        this.otherSameTypes = otherSameTypes;
    }
    
    private DeliveryType(final String deliveryType) {
    	this.deliveryType = deliveryType;
    }

    /**
     * Resolve a delivery type via a string.
     *
     * @param string a string that represents a delivery type
     * @return a enum constant representing a delivery type. Returns null if no type found.
     */
    //public static DeliveryType parse(String string) {
        //final DeliveryType[] deliveryTypes = values();
        //for (DeliveryType DeliveryType : deliveryTypes) {
            //if (DeliveryType.toString().equalsIgnoreCase(string.trim())) {
                //return DeliveryType;
            //}
        //}
        //return null;
    //}
    
    /**
     * Resolve a delivery type via a string.
     *
     * @param string a string that represents a delivery type
     * @return a enum constant representing a delivery type. Returns null if no type found.
     */
    public static DeliveryType parse(String string) {
    	for (final DeliveryType currentType : values()) {
    		if (currentType.toString().equalsIgnoreCase(string.trim())) {
                return currentType;
            }
    		final List<String> inOtherTypes = currentType.otherSameTypes;
    		if (inOtherTypes != null) {
    			for (String variation : inOtherTypes) {
                    if (variation.equalsIgnoreCase(string)) {
                        return currentType;
                    }
                }    			
    		}
    	}
    	return null;
    }     

    @Override
    public String toString() {
        return deliveryType;
    }

}
