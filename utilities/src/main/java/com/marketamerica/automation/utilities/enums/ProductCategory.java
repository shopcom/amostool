package com.marketamerica.automation.utilities.enums;

import java.util.Arrays;
import java.util.List;

/**
 * This enum represents the various product categories for product
 * Created by kathy gong on 07/01/2015.
 */
public enum ProductCategory {

    ISOTONIX("Isotonix"), MOTIVES_COSMETICS("Motives Cosmetics", Arrays.asList("Motives")), SUB("SUB"), 
    SUB_LHP("LHP SUB"), SUB_GLOBLE("Global Sub"), FSK("FSK", Arrays.asList("Fast Start Kit")),
    NUTRAMETRIX("Nutrametrix");     

    private String productCategory;
    private List<String> otherSameCategories;

    private ProductCategory(final String category, List<String> otherSameCategories) {
        this.productCategory = category;
        this.otherSameCategories = otherSameCategories;
    }
    
    private ProductCategory(final String productCategory) {
    	this.productCategory = productCategory;
    }

    /**
     * Resolve a product category via a string.
     *
     * @param string a string that represents a product category
     * @return a enum constant representing a product category. Returns null if no type found.
     */    
    public static ProductCategory parse(String string) {
    	for (final ProductCategory currentCategory : values()) {
    		if (currentCategory.toString().equalsIgnoreCase(string.trim())) {
                return currentCategory;
            }
    		final List<String> inOtherCategories = currentCategory.otherSameCategories;
    		if (inOtherCategories != null) {
    			for (String variation : inOtherCategories) {
                    if (variation.equalsIgnoreCase(string)) {
                        return currentCategory;
                    }
                }    			
    		}
    	}
    	return null;
    }     

    @Override
    public String toString() {
        return productCategory;
    }

}
