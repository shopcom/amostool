package com.marketamerica.automation.utilities.database;

import static com.marketamerica.automation.utilities.helpers.DateAndTimeUtilities.getFormattedDate;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.WordUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.Reporter;

import com.marketamerica.automation.constants.GlobalConstants;
import com.marketamerica.automation.constants.GlobalConstants.Shop;
import com.marketamerica.automation.testdata.User;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.helpers.DateAndTimeUtilities;


/**
 * Provides functionality to interact with the shop database
 *
 * @author javierv
 */
public class ShopDatabase {
    private static final Logger logger = LogManager
            .getLogger(ShopDatabase.class);
    private final int FIXED_WIDTH = 145;
    private DatabaseSession databaseSession;
    private Calendar startingCalendar;
    private Calendar endingCalendar;

    public ShopDatabase() {
        try {
            Class.forName(GlobalConstants.SQL_SERVER_DRIVER_CLASS_NAME);
        } catch (ClassNotFoundException e) {
            logger.error(e);
        }

        if (Environment.getCurrentEnvironment().equals(Environment.STAGING)) {
            databaseSession = new DatabaseSession(GlobalConstants.SQL_SERVER,
                    Shop.DB.Staging.URL, 1433, Shop.DB.Staging.NAME,
                    Shop.DB.PASSWORD, Shop.DB.USER);
        } else if (Environment.getCurrentEnvironment().equals(Environment.LIVE)) {
            databaseSession = new DatabaseSession(GlobalConstants.SQL_SERVER,
                    Shop.DB.Live.URL, 1433, Shop.DB.Live.NAME,
                    Shop.DB.PASSWORD, Shop.DB.USER);
        } else {
            throw new IllegalStateException(String.format("Could not connect to database. Please check for valid " +
                    "environment (%s)", Environment.getCurrentEnvironment()));

        }

        databaseSession.establishConnection();
    }

    /**
     * Gets ETransaction information based on an order. Will return data in the db from the past hour that matches
     * the provided order number
     *
     * @param orderNumber
     * @return
     */
    public Map<String, List<String>> getETransaction(final String orderNumber) {
        final Map<String, List<String>> htmlFetch = new HashMap<>();

        final List<String> postData = new ArrayList<>();
        final List<String> responseData = new ArrayList<>();
        final List<String> dateTimeStamps = new ArrayList<>();

        Calendar startingCalendar;

        if (this.startingCalendar != null) {
            startingCalendar = this.startingCalendar;
        } else {
            startingCalendar = DateAndTimeUtilities
                    .generateRelativeCalendar(0, 0, -1);
        }


        String query;
        if (endingCalendar != null) {
            String formattedStartingDate = getFormattedDate(
                    startingCalendar.getTime(), "MM/dd/yyyy hh:mm:ss.sss");
            String formatedEndingDate = getFormattedDate(
                    endingCalendar.getTime(), "MM/dd/yyyy hh:mm:ss.sss");

            query = "select Top 10 * from l_htmlfetch (nolock) "
                    + "where hostname not like '%www.google.com%' "
                    + "and path like '%eTransOrderService%' and PostData like '%"
                    + orderNumber + "%' and datetimestamp >= '" + formattedStartingDate
                    + "' and datetimestamp <= '" + formatedEndingDate + "' order by datetimestamp desc";

        } else {
            String formattedDate = getFormattedDate(
                    startingCalendar.getTime(), "MM/dd/yyyy hh:mm:ss.sss");

            query = "select Top 10 * from l_htmlfetch (nolock) "
                    + "where hostname not like '%www.google.com%' "
                    + "and path like '%eTransOrderService%' and PostData like '%"
                    + orderNumber + "%' and datetimestamp >= '" + formattedDate
                    + "' order by datetimestamp desc";

        }
        Connection connection;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            connection = databaseSession.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                postData.add(resultSet.getString("PostData")
                        .replaceAll("\\n", "").trim());
                responseData.add(resultSet.getString("ResponseData")
                        .replaceAll("\\n", "").trim());
                dateTimeStamps.add(resultSet.getString("PostData")
                        .replaceAll("\\n", "").trim());
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e1) {
                    logger.error(e1);
                }
            }
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e2) {
                    logger.error(e2);
                }
            }
        }

        htmlFetch.put("PostData", postData);
        htmlFetch.put("ResponseData", responseData);
        htmlFetch.put("DateTimeStamp", dateTimeStamps);

        return htmlFetch;

    }

    /**
     * Get cart order information from a user based on information entered in last 15 minutes
     *
     * @param user
     * @return
     */
    public Map<String, List<String>> getCartOrder(User user) {
        final Map<String, List<String>> htmlFetch = new HashMap<>();

        final List<String> postData = new ArrayList<>();
        final List<String> responseData = new ArrayList<>();
        final List<String> dateTimeStamps = new ArrayList<>();

        final Calendar calendar = DateAndTimeUtilities
                .generateRelativeCalendar(0, 0, -15);
        final String formatedDate = getFormattedDate(
                calendar.getTime(), "MM/dd/yyyy hh:mm:ss.sss");

        final String query = "select Top 10 PostData, ResponseData, DateTimeStamp "
                + "from l_htmlfetch (nolock) "
                + "where hostname not like '%www.google.com%' "
                + "and path like '%/shopping/getCartOrder/%' "
                + "and postdata like '%"
                + user.getEmail()
                + "%' and datetimestamp >= '"
                + formatedDate
                + "' order by datetimestamp desc";
        Connection connection = null;
        Statement statement = null;

        ResultSet resultSet = null;
        try {
            connection = databaseSession.getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                postData.add(resultSet.getString("PostData")
                        .replaceAll("\\n", "").trim());
                responseData.add(resultSet.getString("ResponseData")
                        .replaceAll("\\n", "").trim());
                dateTimeStamps.add(resultSet.getString("PostData")
                        .replaceAll("\\n", "").trim());
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e1) {
                    logger.error(e1);
                }
            }
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e2) {
                    logger.error(e2);
                }
            }
        }

        htmlFetch.put("PostData", postData);
        htmlFetch.put("ResponseData", responseData);
        htmlFetch.put("DateTimeStamp", dateTimeStamps);

        return htmlFetch;

    }

    /**
     * Add the data to ReportNG/TestNg Reports
     *
     * @param htmlFetch
     */
    public void addHtmlDataToReport(final Map<String, List<String>> htmlFetch) {
        final int resultSize = htmlFetch.values().iterator().next().size();
        final Set<String> keys = htmlFetch.keySet();
        for (int i = 0; i < resultSize; i++) {
            for (String key : keys) {
                final String value = htmlFetch.get(key).get(i);
                String wrappedValue = WordUtils.wrap(value, FIXED_WIDTH);
                Reporter.log(String.format("Key: '%s', \nValue: '%s'", key, wrappedValue));
                if (i != (resultSize - 1))
                    Reporter.log(", ");
            }
        }

    }

    public boolean isConnected() {
        try {
            return databaseSession != null && !databaseSession.getConnection().isClosed();
        } catch (SQLException e) {
            return false;
        }
    }

    public void disconnect() {
        if (isConnected())
            databaseSession.disconnect();
    }

    public Calendar getStartingCalendar() {
        return startingCalendar;
    }

    public void setStartingCalendar(Calendar startingCalendar) {
        this.startingCalendar = startingCalendar;
    }

    public Calendar getEndingCalendar() {
        return endingCalendar;
    }

    public void setEndingCalendar(Calendar endingCalendar) {
        this.endingCalendar = endingCalendar;
    }
}
