package com.marketamerica.automation.utilities.enums; 

/**
 * Enum for all regions. 
 */
public enum Region {		 
        ASIA_PACIFIC("Asia Pacific", "UAP"), 
        AMERICAS("Americas", "UAM"), 
        EUROPE("Europe", "UEU"), 
        OTHER("Other", "OTH");
        
        private final String region;
        private final String abbreviation;
        
        private Region(final String region, final String abbreviation) {
    		this.region = region;
    		this.abbreviation = abbreviation;    		 
    	}

    /**
     * Parse a Region based on the given string
     *
     * @param region a string value presenting a Region constant
     * @return the resolved Region constant or null if no matching Region found
     */
    public static Region parse(String region) {
        for (final Region currentRegion : values()) {
            if (currentRegion.toString().equalsIgnoreCase(region) || currentRegion.getAbbreviation()
                    .equalsIgnoreCase(region)) {
                return currentRegion;
            }             
        }
        return OTHER;
    }

    /**
     * Parse a Region based on the given string that represents its abbreviation
     *
     * @param abbreviatedRegion a string value presenting a Region constant
     * @return the resolved Region constant or null if no matching Region found
     */
    public static Region parseAbbreviation(String abbreviatedRegion) {
        for (final Region currentRegion : values()) {
            if (currentRegion.getAbbreviation().equalsIgnoreCase(abbreviatedRegion)) {
                return currentRegion;
            }
        }
        return OTHER;
    }   

    @Override
    public String toString() {
        return region;
    }

    /**
     * Return the Region abbreviation. 
     *
     * @return a string value representing the designated region
     */
    public String getAbbreviation() {
        return this.abbreviation;
    } 

}
