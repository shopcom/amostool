package com.marketamerica.automation.utilities.enums;

import java.util.List;

/**
 * This enum represents the various shipping options for order
 * Created by kathy gong on 03/25/2015.
 */
public enum ShippingOption {

    UPSMI("UPSMI"), SUREPOST("Surepost"), USPS_PRIORITY_MAIL("USPS Priority Mail"), UPS_GROUND("UPS Ground"), 
    UPS_THREE_DAY_SELECT("UPS 3 Day Select"), UPS_SECOND_DAY_AIR("UPS 2nd Day Air"), 
    UPS_NEXT_DAY("UPS Next Day");

    private String shippingOption;        
    // The list of otherSampOptions is for a list of different notations which fall into the same
    // enum constant. Example: Translations
    private List<String> otherSameOptions;
    
    private ShippingOption(final String shippingOption, List<String> otherSameOptions) {
        this.shippingOption = shippingOption;
        this.otherSameOptions = otherSameOptions;
    }
    
    
    private ShippingOption(final String shippingOption) {
    	this.shippingOption = shippingOption;
    }     
    
    /**
     * Resolve a delivery type via a string.
     *
     * @param string a string that represents a delivery type
     * @return a enum constant representing a delivery type. Returns null if no type found.
     */
    public static ShippingOption parse(String string) {
    	for (final ShippingOption currentType : values()) {
    		if (currentType.toString().equalsIgnoreCase(string.trim())) {
                return currentType;
            }
    		final List<String> inOtherTypes = currentType.otherSameOptions;
    		if (inOtherTypes != null) {
    			for (String variation : inOtherTypes) {
                    if (variation.equalsIgnoreCase(string)) {
                        return currentType;
                    }
                }    			
    		}
    	}
    	return null;
    }     

    @Override
    public String toString() {
        return shippingOption;
    }

}
