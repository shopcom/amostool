package com.marketamerica.automation.utilities.enums;

import com.marketamerica.automation.utilities.helpers.NumberUtilities;

/**
 * Created by archanac on 3/16/2015.
 */
public enum CustomMiniDomain {
   OPC3("opc3"),
   OPC3Beauty("opc3"),
   TimelessPrescription("timelessprescription"),
   CellularLabs("cellularlabs"),
   Shopnstop("shopnstop"),
   WhatsYourMotives("whatsyourmotives"),
   TorchSports("torchsports"),
   Pentaxyl("pentaxyl"),
   VitaShield("vitashield"),
   PrimeBlends("primeblends"),
   TLSWellness("tlswellness"),
   MAExclusives("maexclusives"),
   TimeIsCare("timeiscare"),
   MyPetHealthSolutions("mypethealthsolutions"),
   HealthForAFuture("healthforafuture"),
   WellnessDifference("wellnessdifference"),
   PrimeAntiAging("primeantiaging");

   private String domain;

    private CustomMiniDomain(final String domain) {
        this.domain = domain;
    }

    public static CustomMiniDomain getRandom() {
        return NumberUtilities.getRandomValueInArray(values());
    }

    public String getValue() {
        return domain;
    }

    @Override
    public String toString() {
        return domain;
    }

}
