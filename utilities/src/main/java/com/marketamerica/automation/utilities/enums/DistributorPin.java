package com.marketamerica.automation.utilities.enums;

/**
 * This enum represents the possible Pin level a Distributor may be assigned.
 * <table border="0" cellpadding="0">
 * <tr>
 * <th>Pin</th>
 * <th>Description</th>
 * </tr>
 * <tr>
 * <td>R</td>
 * <td>Sales Representative</td>
 * </tr>
 * <tr>
 * <td>D</td>
 * <td>Distributor</td>
 * </tr>
 * <tr>
 * <td>C</td>
 * <td>Coordinator</td>
 * </tr>
 * <tr>
 * <td>E</td>
 * <td>Executive Coordinator</td>
 * </tr>
 * <tr>
 * <td>M</td>
 * <td>Master Coordinator</td>
 * </tr>
 * <p>
 * </table>
 * <p>
 * Created by Javier L. Velasquez on 10/14/2014.
 */
public enum DistributorPin {
    R("r"), D("d"), C("c"), E("e"), M("m");

    private String pin;

    private DistributorPin(String pin) {
        this.pin = pin;
    }

    /**
     * Resolve the Distributor Pin from the string. Returns null if no matching pin is found
     *
     * @param distPinLevel a one character string representing a distributor pin
     * @return
     */
    public static DistributorPin parse(String distPinLevel) {
        DistributorPin[] pins = values();
        for (int i = 0; i < pins.length; ++i) {
            if (pins[i].toString().equalsIgnoreCase(distPinLevel)) {
                return pins[i];
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return pin;
    }
}
