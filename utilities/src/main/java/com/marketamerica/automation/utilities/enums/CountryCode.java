package com.marketamerica.automation.utilities.enums;

/**
 * Created by javierv on 5/5/2014.
 */
public enum CountryCode {
    AU, CA, HK, TW, GB, US, MULTIPLE;

}
