package com.marketamerica.automation.utilities.enums;

/**
 * Enum for different Address Type - Mailing , shipping , Government
 *
 * @author javierv
 */
public enum AddressType {
    MAILING("m"), SHIPPING("s"), GOVERNMENT("g");

    private String type;

    private AddressType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
