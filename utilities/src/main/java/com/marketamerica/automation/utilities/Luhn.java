package com.marketamerica.automation.utilities;

import static com.marketamerica.automation.utilities.helpers.NumberUtilities.convertStringToIntArray;
import static com.marketamerica.automation.utilities.helpers.NumberUtilities.summateIntArray;

/**
 * Created by Javier L. Velasquez on 10/6/2014.
 */
public class Luhn {

    public static boolean isValid(long accountNumber) {
        return getCheckSum(accountNumber, false) == 0;
    }

    private static long getCheckSum(long accountNumber, boolean hasCheckDigit) {
        String numberAsString = String.valueOf(accountNumber);

        if (hasCheckDigit) numberAsString = numberAsString.substring(0, numberAsString.length() - 1);

        int[] values = convertStringToIntArray(numberAsString);
        for (int i = values.length - 2; i >= 0; i -= 2) {
            int value = values[i] * 2;
            if (value <= 9) {
                values[i] = value;
            } else {
                values[i] = 0;
                String operation = String.valueOf(value);
                for (int j = 0; j < operation.length(); ++j) {
                    values[i] += Integer.valueOf(operation.substring(j, j + 1));
                }
            }
        }
        return summateIntArray(values) % 10;
    }


    /**
     * Calculates a checkdigit associated with an account number
     *
     * @param accountNumber an account number that should be validated with a Luhn checksum
     * @param hasCheckDigit boolean indicating if the provided account number already has
     *                      a calculated check digit
     * @return a Luhn check digit
     */
    public static long calculateCheckDigit(long accountNumber, boolean hasCheckDigit) {
        long checkDigit = getCheckSum(accountNumber * 10, hasCheckDigit);
        if (checkDigit == 0) {
            return checkDigit;
        } else {
            return 10 - checkDigit;
        }

    }

    /**
     * Calculates a checkdigit associated with an account number
     *
     * @param accountNumber an account number that should be validated with a Luhn checksum
     * @return a Luhn check digit
     */
    public static long calculateCheckDigit(long accountNumber) {
        return calculateCheckDigit(accountNumber, false);
    }


}