package com.marketamerica.automation.utilities.helpers;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHttpEntityEnclosingRequest;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.remote.HttpCommandExecutor;
import org.openqa.selenium.remote.RemoteWebDriver;

import com.marketamerica.automation.constants.GlobalConstants;

/**
 * Contains utility methods dealing with web/http related activities.
 *
 * @author javierv
 */
public final class HttpUtilities {
    private static final Logger logger = LogManager
            .getLogger(HttpUtilities.class);

    /**
     * Resolves the IP address if a remove webdriver instance<br>
     * <br>
     * Code originally from <a href=
     * "http://selenium.polteq.com/en/get-remote-ip-address-of-node-selenium-grid/"
     * >http://selenium.polteq.com/en/get-remote-ip-address-of-node-selenium-
     * grid/</a>
     *
     * @param remoteDriver a remote webdriver instance
     * @return a string representing the node's IP address
     */
    public static String getIPAddressOfSeleniumNode(
            final RemoteWebDriver remoteDriver) {
        String nodeIp = null;
        final HttpCommandExecutor httpCommandExecutor = (HttpCommandExecutor) remoteDriver
                .getCommandExecutor();
        final String hostName = httpCommandExecutor.getAddressOfRemoteServer()
                .getHost();
        final int port = httpCommandExecutor.getAddressOfRemoteServer()
                .getPort();
        final HttpHost host = new HttpHost(hostName, port);
        final CloseableHttpClient client = HttpClientBuilder.create().build();
        URL sessionURL;

        try {
            sessionURL = new URL(String.format(
                    "http://%s:%s/grid/api/testsession?session=%s", hostName,
                    port, remoteDriver.getSessionId()));
        } catch (MalformedURLException e) {
            logger.error(e);
            return null;
        }
        final BasicHttpEntityEnclosingRequest request = new BasicHttpEntityEnclosingRequest(
                GlobalConstants.HTTP.POST, sessionURL.toExternalForm());
        try {
            final HttpResponse response = client.execute(host, request);
            final JSONObject object = extractJSONFromHttpResponse(response);

            final URL myURL = new URL(object.getString("proxyId"));
            if ((myURL.getHost() != null) && (myURL.getPort() != -1)) {
                nodeIp = myURL.getHost();
            }

        } catch (IOException | JSONException e) {
            logger.error(e);
        }
        return nodeIp;
    }

    /**
     * Extract a Json Object from a HttpResponse
     *
     * @param resp a http response object
     * @return a json object based on the http response
     * @throws IOException
     * @throws JSONException
     */
    public static JSONObject extractJSONFromHttpResponse(final HttpResponse resp)
            throws IOException, JSONException {
        InputStream contents = null;
        try {
            contents = resp.getEntity().getContent();
        } catch (IOException | IllegalStateException e) {
            logger.error(e);
            return new JSONObject();
        } finally {
            if (contents != null)
                contents.close();
        }
        final StringWriter writer = new StringWriter();
        IOUtils.copy(contents, writer, GlobalConstants.UTF8);
        return new JSONObject(writer.toString());
    }

    /**
     * Get a HTTP POST response based on a given URI and http entity object
     *
     * @param uri        a uri for the destination
     * @param httpEntity an http entity object used for the POST request's argument
     * @return a string value representing the results. Will return null if request doesn't go through
     * @see org.apache.http.HttpEntity
     */
    public static String getPostResponse(final URI uri, HttpEntity httpEntity) {
        final CloseableHttpClient httpClient = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost(uri);
        httpPost.setEntity(httpEntity);
        HttpResponse response;
        String result = null;
        try {
            response = httpClient.execute(httpPost);
            result = EntityUtils.toString(response.getEntity());
            return result;
        } catch (IOException e) {
            logger.error(e);
        } finally {
            try {
                httpClient.close();
                logger.info(String.format("Closed connection to %s", uri));
            } catch (IOException e) {
                logger.error(e);
            }
        }
        return result;
    }

    /**
     * Performs a HTTP GET request for the provided URI
     * @param uri a uri to request from
     * @return a string representation of what was requested.
     */
    public static String get(final URI uri) {
        final CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet request = new HttpGet(uri);
        try {
            HttpResponse response = httpClient.execute(request);
            return EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            logger.error(e);
        } finally {
            try {
                httpClient.close();
                logger.info(String.format("Closed connection to %s", uri));
            } catch (IOException e) {
                logger.error(e);
            }
        }
        return "";
    }

}
