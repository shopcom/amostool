package com.marketamerica.automation.utilities.database;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.jcraft.jsch.JSchException;
import com.marketamerica.automation.constants.GlobalConstants;
import com.marketamerica.automation.utilities.helpers.RegularExpressions;
import com.marketamerica.automation.utilities.helpers.SecureShellConnection;

public class TestLinkDatabase {
    protected static final Logger logger = LogManager
            .getLogger(TestLinkDatabase.class);
    private final DatabaseConnectionOverSecureShellConnection databaseConnectionOverSSH;

    public TestLinkDatabase() throws JSchException {
        // SSH login username
        final String userName = GlobalConstants.TestLink.SSH_USER;
        // SSH login password
        final String password = GlobalConstants.TestLink.SSH_PASSWORD;
        // hostname or ip or SSH server
        final String host = GlobalConstants.TestLink.HOST_IP;
        // remote SSH host port number
        final int sshPort = GlobalConstants.TestLink.SSH_PORT;
        // hostname or ip of your database
        final String databaseHostHost = "localhost";
        // local port number use to bind SSH tunnel
        final int localPort = GlobalConstants.TestLink.DB_PORT;
        // remote port number of database
        final int remotePort = localPort;
        // database login username
        final String databaseUser = GlobalConstants.TestLink.DB_USER;
        // database login password
        final String databasePassword = GlobalConstants.TestLink.DB_PASSWORD;
        final String database = GlobalConstants.TestLink.DATABASE;

        final SecureShellConnection ssh = new SecureShellConnection(userName,
                password, host, sshPort);

        final DatabaseSession databaseSessionConnection = new DatabaseSession(
                "mysql", "localhost", remotePort, database, databasePassword,
                databaseUser);

        databaseConnectionOverSSH = new DatabaseConnectionOverSecureShellConnection(
                databaseSessionConnection, ssh);

        databaseConnectionOverSSH.connectToSSHServer();
        databaseConnectionOverSSH.setPortFowardingL(localPort,
                databaseHostHost, remotePort);

        databaseConnectionOverSSH.connectToDatabase();
        databaseSessionConnection.setReadOnly();

    }

    public void disconnect() {
        databaseConnectionOverSSH.disconnect(true);
    }

    public List<String> getAllRequirementSpecificationsByPrefix(
            final String prefix, final String projectName) {
        final List<String> allRequirementSpecificationsByPrefix = new ArrayList<>();

        Statement statement = null;
        final String query = String
                .format("select doc_id from  req_specs where doc_id REGEXP \"%s.*\" and testproject_id = ANY (select id from nodes_hierarchy where name = \"%s\")",
                        prefix, projectName);
        ResultSet resultSet = null;
        try {
            statement = databaseConnectionOverSSH.getConnection()
                    .createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                allRequirementSpecificationsByPrefix.add(resultSet
                        .getString("doc_id"));
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    logger.error(e);
                }
            }
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    logger.error(e);
                }
            }
        }

        return allRequirementSpecificationsByPrefix;

    }

    /**
     * Returns all project titles.
     * This method has been marked as deprecated. Please use {@code TestLinkManager#getProjectNames} instead.
     * @return a list of strings representing all project names associated with the current instance of TestLink.
     */
    @Deprecated
    public List<String> getAllProjectsTitles() {
        final List<String> projects = new ArrayList<>();

        Statement statement = null;
        final String query = "SELECT name from nodes_hierarchy where id in (SELECT DISTINCT id from testprojects) ORDER BY id DESC;";
        ResultSet resultSet = null;
        try {
            statement = databaseConnectionOverSSH.getConnection()
                    .createStatement();
            resultSet = statement.executeQuery(query);

            while (resultSet.next()) {
                projects.add(resultSet.getString("name"));
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    logger.error(e);
                }
            }
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    logger.error(e);
                }
            }
        }

        return projects;

    }

    public String getProjectPrefix(String projectName) {
        String prefix = null;
        Statement statement = null;
        final String query = String
                .format("select prefix from testprojects where id = ANY "
                                + "(select id from nodes_hierarchy where name = \"%s\")",
                        projectName
                );
        ResultSet resultSet = null;
        try {
            statement = databaseConnectionOverSSH.getConnection()
                    .createStatement();
            resultSet = statement.executeQuery(query);
            if (resultSet.next()) {
                prefix = resultSet.getString("prefix");
            }
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    logger.error(e);
                }
            }
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    logger.error(e);
                }
            }
        }

        if (prefix == null) {
            prefix = "";
        } else {
            prefix = prefix.trim();
            if (prefix.endsWith("-")) {
                prefix = prefix.substring(0, prefix.length());
            }

        }
        logger.debug(String.format("In \"%s\" prefix was determined to be \"%s\"", projectName, prefix));
        return prefix;
    }

    public int getMaximumIdentifierForRequirementSpecificationsByProject(
            final String prefix, final String projectName) {
        final List<String> requirementSpecifications = getAllRequirementSpecificationsByPrefix(
                prefix, projectName);

        int max = 0;
        for (String requirementSpecification : requirementSpecifications) {
            if (requirementSpecification.trim().matches(".*\\d+$")) {
                try {
                    int identifier = Integer.parseInt(RegularExpressions
                            .matchFirstPattern(requirementSpecification, "\\d+$"));
                    if (identifier > max) {
                        max = identifier;
                    }
                } catch (NumberFormatException e) {
                    logger.error("Invalid number found in a req spec id", e.getCause());
                }
            }
        }

        return max;

    }
}
