package com.marketamerica.automation.utilities.enums;

/**
 * Enum for different test groups. 
 *  @author archanac
 */
public interface TestGroup {
	String PC_Registration = "PC Registration";  
	String Ordering = "Ordering";  
	String Search_Product = "Search Product";  
	String Order_Tracking = "Order Tracking";
	String Distributor_Creation = "Distributor Creation";
	
	String Site_Navigation = "Site Navigation";
	String Shopping_Cart = "Shopping Cart";
	String PC_Account = "PC Account";
	String Autoship = "Autoship";
	
	String Others = "Others";
	
	// testGroups for distributor creation	
	String SUW_USA_USA_UFO_Corp_Group01 = "SUW USASponsor USAUFO Corporation Group01";
	String SUW_USA_USA_HP_Part_Group02 = "SUW USASponsor USAHP Partnership Group02";
	String SUW_USA_USA_HP1_Ind_Group03 = "SUW USASponsor USAHP1 Individual Group03";
	String SUW_USA_USA_SR_Ind_Group04 = "SUW USASponsor USASR Individual Group04";

	
	String Group01 = "SameBillingShipping UFMS FSK ApplicationCompleted OptedOutAR GSW WebSites CustomMini IRC Global";
	String Group02 = "SameBillingShipping UFMS FirstOrderWithProducts applicationCompleted optedInAR GSW webSites IRC";
	String Group03 = "DifferentBillingShipping UFMS SUBOnly applicationCompleted";
	String Group04 = "sameBillingShipping applicationCompleted";
	
	
}
