package com.marketamerica.automation.utilities.enums;

/**
 * Created by javierv on 11/20/2014.
 */
public enum DistributorApplicationMethod {
    SIGN_UP_WIZARD("suw"), PARTNER_NOW("join"), GET_STARTED_NOW("motives join") ;

    private String method;

    private DistributorApplicationMethod(String method) {
        this.method = method;
    }

}
