package com.marketamerica.automation.utilities.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.marketamerica.automation.constants.GlobalConstants;

/**
 * This class represents a session with a database
 */
public class DatabaseSession {
    private final Logger logger = LogManager
            .getLogger(DatabaseSession.class);

    private final String dbms;
    private final String serverName;
    private final int portNumber;
    private final String databaseName;
    private final String password;
    private final String userName;
    private Connection connection;

    public DatabaseSession(final String dbms, final String serverName,
                           final int portNumber, final String databaseName,
                           final String password, final String userName) {
        this.dbms = dbms;
        this.serverName = serverName;
        this.portNumber = portNumber;
        this.databaseName = databaseName;
        this.password = password;
        this.userName = userName;

    }

    /**
     * Establishes a connection with the database
     */
    public void establishConnection() {
        switch (this.dbms) {
            case GlobalConstants.MYSQL:
                final Properties connectionProperties = new Properties();
                connectionProperties.put(GlobalConstants.USER_ALL_LOWER_CASE, this.userName);
                connectionProperties.put(GlobalConstants.PASSWORD_ALL_LOWER_CASE,
                        this.password);

                try {
                    connection = DriverManager.getConnection(GlobalConstants.JDBC
                                    + ":" + this.dbms + "://" + this.serverName + ":"
                                    + this.portNumber + "/" + this.databaseName,
                            connectionProperties
                    );
                    logger.debug(String.format("Established connection to %s", connection));
                } catch (SQLException e) {
                    logger.error(e);
                }
                break;
            case GlobalConstants.SQL_SERVER:
                try {
                    connection = DriverManager.getConnection(GlobalConstants.JDBC
                            + ":" + this.dbms + "://" + this.serverName + ";databaseName=" + this.databaseName + ";user=" + this.userName + ";password=" + this.password
                            + ";integratedSecurity=true");
                    logger.debug(String.format("Established to %s", connection));
                } catch (SQLException e) {
                    logger.error(e);
                }
                break;
            default:
                throw new IllegalStateException(String.format("Attempting to connect to an unsupported DBMS (%s)", dbms));
        }
    }

    /**
     * Disconnects the active session
     */
    public void disconnect() {
        try {
            if (!this.connection.isClosed()) {
                logger.debug(String.format("Closing connection %s", this.connection));
                this.connection.close();
            }
        } catch (SQLException e) {
            logger.error(e);
        }
    }

    /**
     * Returns the active connection
     *
     * @return
     */
    public Connection getConnection() {
        return this.connection;
    }

    /**
     * Sets the database status to read only
     */
    public void setReadOnly() {
        if (this.connection == null) {
            logger.fatal("No connection has been established!");
        } else {
            try {
                this.connection.setReadOnly(true);
            } catch (SQLException e) {
                logger.error(e);
            }
        }
    }

}
