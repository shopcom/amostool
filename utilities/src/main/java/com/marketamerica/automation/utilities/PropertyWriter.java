package com.marketamerica.automation.utilities;

import java.io.File;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.reloading.FileChangedReloadingStrategy;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * This class provides users with the ability to write to property files. This is useful for unit testing<br><br>
 * Created by Javier L. Velasquez on 8/18/2014.
 */
public class PropertyWriter {
    private final Logger logger = LogManager.getLogger(PropertyWriter.class);
    private PropertiesConfiguration properties;
    private File file;

    public PropertyWriter(File file) {
        try {
            this.file = file;
            properties = new PropertiesConfiguration(file);
            properties.setFile(file);
            properties.setAutoSave(true);
            properties.setReloadingStrategy(new FileChangedReloadingStrategy());

        } catch (ConfigurationException e) {
            logger.error(e);
        }
    }

    /**
     * Set a property key and update the file.
     *
     * @param key   a property's key entry
     * @param value the value to represent the given key
     */
    public void set(final String key, final Object value) {
        properties.setProperty(key, String.valueOf(value));
        try {
            properties.save(file);
        } catch (ConfigurationException e) {
            logger.error(e);
        }
    }


}
