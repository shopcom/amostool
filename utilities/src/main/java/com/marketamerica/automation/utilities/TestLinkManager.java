package com.marketamerica.automation.utilities;

import static com.marketamerica.automation.constants.GlobalConstants.TestLink.API_DEVELOPER_KEY;
import static com.marketamerica.automation.constants.GlobalConstants.TestLink.CUSTOM_FIELD_DELIMITER;
import static com.marketamerica.automation.constants.GlobalConstants.TestLink.XMLRPC_URL;
import static java.util.stream.Collectors.toList;
import static org.testng.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import javax.activation.MimetypesFileTypeMap;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionType;
import br.eti.kinoshita.testlinkjavaapi.constants.ResponseDetails;
import br.eti.kinoshita.testlinkjavaapi.constants.TestCaseDetails;
import br.eti.kinoshita.testlinkjavaapi.constants.TestCaseStepAction;
import br.eti.kinoshita.testlinkjavaapi.model.Attachment;
import br.eti.kinoshita.testlinkjavaapi.model.Build;
import br.eti.kinoshita.testlinkjavaapi.model.CustomField;
import br.eti.kinoshita.testlinkjavaapi.model.Execution;
import br.eti.kinoshita.testlinkjavaapi.model.Platform;
import br.eti.kinoshita.testlinkjavaapi.model.ReportTCResultResponse;
import br.eti.kinoshita.testlinkjavaapi.model.TestCase;
import br.eti.kinoshita.testlinkjavaapi.model.TestCaseStep;
import br.eti.kinoshita.testlinkjavaapi.model.TestPlan;
import br.eti.kinoshita.testlinkjavaapi.model.TestProject;
import br.eti.kinoshita.testlinkjavaapi.model.TestSuite;
import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

import com.marketamerica.automation.testdata.TestLinkID;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.helpers.ReflectiveUtilities;

/**
 * Test Link Manager provides the ability to manage to Test Link. It allows for
 * interactions against the Unofficial Test Link API
 *
 * @author javierv
 */
public class TestLinkManager {

    final static Logger logger = LogManager.getLogger(TestLinkManager.class);
    private static final String SANITY_MESSAGE = "Hello!";
    private TestLinkAPI testLink;
    private TestProject testProject;
    private Set<TestCase> testCases = new HashSet<>();
    private Map<TestPlan, Build> planBuildMap = new HashMap<>();

    public TestLinkManager(String testProject, Environment environment) {
        this();
        connectToTestPlans(testProject, environment);
        loadTestCases();
    }


    public TestLinkManager(String testProject) {
        this(testProject, true);


    }

    public TestLinkManager(String testProject, boolean loadTests) {
        this();
        if (loadTests) {
            connectToTestPlans(testProject);
            loadTestCases();
        }
    }

    public TestLinkManager() {
        URL testlinkURL = null;
        try {
            testlinkURL = new URL(XMLRPC_URL);
        } catch (MalformedURLException mue) {
            logger.error(mue);
        }

        try {
            testLink = new TestLinkAPI(testlinkURL, API_DEVELOPER_KEY);
        } catch (TestLinkAPIException e) {
            logger.error(e);
        }

		/*
         * Sanity Check. Using native java assert since we are not actually
		 * performing a test related to objective
		 */
        assert testLink.ping().equals(SANITY_MESSAGE);
    }
    private void connectToTestPlans(String testProjectName) {
        connectToTestPlans(testProjectName, null);
    }

    private void connectToTestPlans(String testProjectName, Environment environment) {
        setTestProject(getTestProject(testProjectName));
        loadTestPlansAndLatestBuilds(environment);

    }

    private void loadTestPlansAndLatestBuilds(Environment environment) {
        final TestPlan[] projectTestPlans = getProjectTestPlans();

        for (final TestPlan testPlan : projectTestPlans) {
            // The test link has to have at least one platform before being searched for test cases.
            try {
                testLink.getTestPlanPlatforms(testPlan.getId());
            } catch (TestLinkAPIException e) {
                if (e.getMessage().contains("has no platforms linked")) {
                    continue;
                } else {
                    logger.error(e);
                }
            }
            final Build build = getLatestBuildInTestPlan(testPlan);
            if (build != null) {
                final String testPlanName = testPlan.getName();
                Environment testPlanEnvironment = getEnvironment(testPlanName);
                if ((environment == null && testPlanEnvironment == null) ||
                        (testPlanEnvironment != null && testPlanEnvironment == environment)) {
                    planBuildMap.put(testPlan, build);
                } else {
                    if (testPlanEnvironment == null) {
                        logger.debug(
                                String.format(
                                        "The test plan (%s) did not specify an environment. " +
                                                "To prevent recording results incorrectly, the test plan (%s) " +
                                                "must specify the environment that is provided by the test suite (%s)",
                                        testPlanName,
                                        testPlanName,
                                        environment
                                )
                        );
                    } else {
                        logger.debug(
                                String.format(
                                        "The test plan included an environment name (%s). " +
                                                "This name did match the environment specified by the test context (%s). " +
                                                "So it was not added into the list of test plans to report to",
                                        testPlanName,
                                        environment
                                )
                        );

                    }
                }


            }
        }
    }

    /**
     * Load all the automated and manual test cases associated with a test plan
     * to be used for other interactions
     */
    private void loadTestCases() {
        final Set<TestPlan> testPlans = this.planBuildMap.keySet();

        for (TestPlan testPlan : testPlans) {
            Build build = planBuildMap.get(testPlan);
            testCases.addAll(Arrays.asList(testLink.getTestCasesForTestPlan(
                    testPlan.getId(), null, build.getId(), null, null, null, null,
                    null, ExecutionType.AUTOMATED, null, TestCaseDetails.FULL)));
            testCases.addAll(Arrays.asList(testLink.getTestCasesForTestPlan(
                    testPlan.getId(), null, build.getId(), null, null, null, null,
                    null, ExecutionType.MANUAL, null, TestCaseDetails.FULL)));
        }

    }

    /**
     * Get a test project by name
     *
     * @param testProjectName a name associated with a test project
     * @return a test project object representing a test project on test link
     */
    private TestProject getTestProject(final String testProjectName) {
        final TestProject[] testProjects = testLink.getProjects();
        for (final TestProject testProject : testProjects) {
            if (testProject.getName().equalsIgnoreCase(testProjectName)) {
                return testProject;
            }
        }
        return null;
    }

    /**
     * Returns one test case based on a test link id. This method will only return the first matching id.
     * Any external id after the first is ignored
     * @param testLinkID
     * @return
     */
    public TestCase getTestCaseByExternalId(TestLinkID testLinkID) {
        List<TestCase> testCases = getTestCasesByExternalId(testLinkID);
        if (testCases.size() == 0) {
            return null;
        }
        return testCases.get(0);
    }

    /**
     * Get all test cases associated by the provided TestLinkID. This method will check for each test link id
     * provided by {@code TestLinkID#getExternalId} and return an array list of all the TestLink TestCases
     * @param testLinkID
     * @return an array list of TestLink test cases whose each id correspond to an entry provided by the TestLinkID
     * instance.
     */
    public List<TestCase> getTestCasesByExternalId(TestLinkID testLinkID) {
        final String[] externalIDs = testLinkID.getExternalIds();
        final int version = testLinkID.getVersion();
        List<TestCase> testCases = new ArrayList<>();
        if (externalIDs == null || externalIDs.length == 0) {
            return testCases;
        }
        for (int i = 0; i < externalIDs.length; ++i) {
            String externalId = externalIDs[i];
            try {
                testCases.add(testLink.getTestCaseByExternalId(externalId, version));
            } catch (TestLinkAPIException e) {
                logger.warn(String
                        .format("External ID (%s) was not valid on the specified project (%s)",
                                externalId,
                                this.getTestProject().getName()
                        ),
                        e
                );
            }
        }
        return testCases;
    }

    /**
     * Get a test case in Test Case by its external Id
     *
     * @param method
     * @return
     */
    public TestCase getTestCaseByExternalId(Method method) {

        final String externalID = ReflectiveUtilities
                .getMethodExternalId(method);
        final int version = ReflectiveUtilities.getMethodVersion(method);

        try {
            return testLink.getTestCaseByExternalId(externalID, version);
        } catch (TestLinkAPIException e) {
            logger.warn(String
                    .format("Although '%s' provided an external ID (%s), "
                                    + "it was not valid on the specified project (%s)",
                            method, externalID,
                            this.getTestProject().getName()
                    ));
            return null;
        }
    }

    /**
     * Get all the test plans associated with a test project
     *
     * @return
     */
    public TestPlan[] getProjectTestPlans() {
        return testLink.getProjectTestPlans(getTestProject().getId());
    }

    /**
     * Get a test plan in a project by its name
     *
     * @param testPlanName
     * @return
     */
    private TestPlan getTestPlanInProjectByName(final String testPlanName) {
        final TestPlan[] testPlans = getProjectTestPlans();
        for (int i = 0; i < testPlans.length; ++i) {
            if (testPlans[i].getName().equalsIgnoreCase(testPlanName)) {
                return testPlans[i];
            }
        }
        return null;
    }

    /**
     * Get the latest build in the current test plan
     *
     * @return
     */
    private Build getLatestBuildInTestPlan(TestPlan testPlan) {
        try {
            return testLink.getLatestBuildForTestPlan(testPlan.getId());
        } catch (TestLinkAPIException e) {
            logger.error(e);
        }
        return null;
    }

    /**
     * Get the build associated with a test plan
     *
     * @param buildName    a string representing a build associated with the given test
     *                     plan
     * @param testPlanName a plan representing a test plan
     * @return a build object associated with the given test plan
     */
    private Build getBuildInTestPlan(final String buildName,
                                     final String testPlanName) {
        final TestPlan testPlan = getTestPlanInProjectByName(testPlanName);
        final Build[] builds = testLink.getBuildsForTestPlan(testPlan.getId());

        for (int i = 0; i < builds.length; i++) {
            if (builds[i].getName().equalsIgnoreCase(buildName)) {
                return builds[i];
            }
        }
        return null;
    }

    /**
     * Sets a test execution status
     *
     * @param testCase        given test case. Must include a platform
     * @param executionStatus test execution status (e.g. pass, fail, skipped)
     * @param notes           notes describing the execution
     * @return TC results response
     */
    public ReportTCResultResponse setTestStatus(final TestCase testCase, final ExecutionStatus executionStatus,
                                                final String notes) {
        ReportTCResultResponse response = null;
        final Platform platform = testCase.getPlatform();
        final Set<TestPlan> testPlans = this.planBuildMap.keySet();
        Environment testCaseEnvironment = getEnvironment(testCase.getPlatform().getName());
        for (TestPlan testPlan : testPlans) {
            Build build = planBuildMap.get(testPlan);

            if (!testPlanHasPlatform(testPlan, platform)) {
                logger.warn(String.format("Test Plan (%s) doesn't have platform (%s). Test result will not be recorded", testPlan.getName(), platform.getName()));
                continue;
            }


            Environment testPlanEnvironment = getEnvironment(testPlan.getName());
            // If a test case environment and test plan environment are specified and both values are equal
            if (testCaseEnvironment != null && testPlanEnvironment != null && (testPlanEnvironment !=
                    testCaseEnvironment)) {
                logger.debug(String.format("Test Plan platform (%s) didn't match test case platform (%s)", testPlanEnvironment, testCaseEnvironment));
                continue;
            }

            try {
                response = testLink.setTestCaseExecutionResult(testCase.getId(),
                        null, testPlan.getId(), executionStatus, build.getId(),
                        build.getName(), notes, true, null, platform.getId(),
                        platform.getName(), null, true);
            } catch (TestLinkAPIException e) {
                if (e.getMessage().contains(
                        "The status code (n) provided is not valid!")) {
                    logger.error(String
                                    .format("%s (%s - %s) has already been ran at least once. Because "
                                                    + "of this the status can't be set back to \"Not Run\"",
                                            testCase.getName(),
                                            testCase.getFullExternalId(),
                                            platform
                                    )
                    );
                } else {
                    logger.error(String
                            .format("Error recording %s (%s - %s)",
                                    testCase.getName(),
                                    testCase.getFullExternalId(),
                                    platform
                            ), e);
                }
            }
            logger.info(String
                    .format("Recorded result to Test Link For Test '%s - %s' (%s) in platform \"%s\" in test plan \"%s\" "
                                    + "(Currently browser version and OS not considered!)",
                            testCase.getName(), testCase.getFullExternalId(),
                            executionStatus, platform, (testPlan.getId() + " - " + testPlan.getName())
                    ));

        }
        return response;
    }

    /**
     * Checks to see if a test plan contains the given platform
     *
     * @param testPlan test plan to search in
     * @param platform platform to search for
     * @return
     */
    private boolean testPlanHasPlatform(TestPlan testPlan, Platform platform) {
        final Platform[] testPlanPlatforms = testLink.getTestPlanPlatforms(testPlan.getId());
        for (Platform currentPlatform : testPlanPlatforms) {
            if (currentPlatform.getName().equals(platform.getName())) {
                return true;
            }
        }
        return false;
    }

    private Environment getEnvironment(String string) {
        Environment[] environments = Environment.values();
        Environment testCasePlatform = null;
        for (int i = 0; i < environments.length; ++i) {
            final String environmentName = environments[i].toString().toLowerCase().trim();
            final String environmentAbbreviation = environments[i].getAbbreviation().toLowerCase().trim();
            if (string.toLowerCase().trim().contains(environmentName) || string.toLowerCase().trim().contains(environmentAbbreviation)) {
                testCasePlatform = environments[i];
            }
        }
        return testCasePlatform;
    }

    /**
     * Set a test execution to automated
     *
     * @param testCase
     */
    public void setTestExecutionToAutomated(TestCase testCase) {
        testLink.setTestCaseExecutionType(getTestProject().getId(),
                testCase.getId(), getTestExternalId(testCase),
                testCase.getVersion(), ExecutionType.AUTOMATED);
    }

    /**
     * Get a test case external ID
     *
     * @param testCase a test link external id
     * @return an integer value representing a test link test case's external id
     */
    private int getTestExternalId(TestCase testCase) {
        return Integer.parseInt(testCase.getFullExternalId()
                .replace(getTestProject().getPrefix(), "").replace("-", ""));
    }

    /**
     * Get a test cases's custom field
     *
     * @param testCase         a test link test case object
     * @param customFieldTitle a custom field value
     * @return a string array of custom fields
     */
    public String[] getTestCaseCustomField(final TestCase testCase,
                                           final String customFieldTitle) {

        CustomField customField = null;
        try {
            customField = testLink.getTestCaseCustomFieldDesignValue(
                    testCase.getId(), getTestExternalId(testCase), 1,
                    getTestProject().getId(), customFieldTitle,
                    ResponseDetails.SIMPLE);
        } catch (TestLinkAPIException e) {
            fail(e.getMessage());
        }
        return customField.getValue().split(CUSTOM_FIELD_DELIMITER);
    }

    /**
     * Check if a test case has been automated
     *
     * @param testCase
     * @return boolean indicating if the test case execution type is marked as
     * automated
     */
    public boolean isTestCaseAutomated(final TestCase testCase) {
        return testCase.getExecutionType() == ExecutionType.AUTOMATED;
    }

    /**
     * @return the testProject
     */
    public TestProject getTestProject() {
        return testProject;
    }

    /**
     * @param testProject the testProject to set
     */
    private void setTestProject(TestProject testProject) {
        this.testProject = testProject;
    }


    /**
     * Get a test case in a test plan
     *
     * @param inputTestCase get the more detailed test case based on matching external id
     *                      criteria
     * @return
     */
    public Set<TestCase> getAllMatchingTestCases(
            final TestCase inputTestCase) {
        final Set<TestPlan> testPlans = this.planBuildMap.keySet();
        final Set<TestCase> testCasesInTestPlan = new HashSet<>();

        for (TestPlan testPlan : testPlans) {
            for (final TestCase testCase : testCases) {
                if (testCase.getFullExternalId()
                        .equals(inputTestCase.getFullExternalId())) {
                    testCasesInTestPlan.add(testCase);
                }
            }
            if (testCasesInTestPlan.isEmpty()) {
                logger.warn(String.format("%s isn't defined in the current test plan (%s). " +
                        "It may need to be added to the current test plan with a valid platform", inputTestCase.getFullExternalId(), testPlan));
            }
        }

        return testCasesInTestPlan;
    }

    /**
     * Return the specified test case platforms.
     *
     * @param testCase a test case object that has its platform properties loaded
     * @return a string array containing platform values split into a string
     * array. If no platforms are found, then this method will return
     * null
     */
    public String[] getTestCasePlatforms(final TestCase testCase) {
        return testCase.getPlatform() != null ? testCase.getPlatform()
                .getName().split(CUSTOM_FIELD_DELIMITER) : null;
    }

    /**
     * Get the given test suites associated with a test case by its test suite
     * id
     *
     * @param testCase a test case who you would like to return its valid test suites
     * @return an array of test suites associated with a test case
     */
    public TestSuite[] findTestCaseTestSuite(final TestCase testCase) {
        return testLink.getTestSuiteByID(Arrays.asList(testCase
                .getTestSuiteId()));
    }

    /**
     * Return the test suite that contains the corresponding Test Link Test Case Id
     * @param id A valid TestLinkID that matches a test case in the current test project.
     * @return a TestSuite that contains the provided test.
     */
    public TestSuite getTestSuite(TestLinkID id) {
        final TestCase testCase = getTestCaseByExternalId(id);
        final TestSuite[] testSuiteByID = testLink.getTestSuiteByID(Arrays.asList(testCase.getTestSuiteId()));
        return testSuiteByID[0];
    }

    /**
     * Upload an attachment to Test Link Test Suite
     *
     * @param execution   an object representing the execution of test case on Test Link
     * @param title       a value describing the attachment
     * @param description a description
     * @param fileName
     * @return the uploaded attachment file
     */
    public Attachment uploadExecutionAttachment(final Execution execution,
                                                final String title, final String description, final String fileName) {

        final File attachmentFile = new File(fileName);
        String fileContent = null;
        try {
            final byte[] byteArray = FileUtils
                    .readFileToByteArray(attachmentFile);
            // Create a string represent a base64 encoding of the file name
            fileContent = new String(Base64.encodeBase64(byteArray));
        } catch (IOException e) {
            logger.error(e);
            return new Attachment();
        }
        // Read the mime type of the file to be used to identify the file to
        // test link
        final String fileType = new MimetypesFileTypeMap()
                .getContentType(attachmentFile);

        return testLink.uploadExecutionAttachment(
                execution.getId(), title, description,
                attachmentFile.getName(), fileType, fileContent);
    }

    /**
     * Populates the steps of a test case
     * @param testCase a test case that has a populated id.
     * @param steps a list of steps to associate with the provided test case.
     */
    public void setSteps(TestCase testCase, List<TestCaseStep> steps) {
        try {
            testLink.createTestCaseSteps(testCase.getId(), null, 1, TestCaseStepAction.UPDATE, steps);
        } catch (TestLinkAPIException e) {
            logger.error(e);
        }
    }

    /**
     * This method removes all test steps associated with a test case
     * @param testCase a test case that has steps
     */
    public void removeSteps(TestCase testCase) {
        final List<TestCaseStep> steps = testCase.getSteps();
        try {
            testLink.deleteTestCaseSteps(testCase.getFullExternalId(), 1, steps);
        } catch (TestLinkAPIException e) {
            logger.error(e);
        }
    }

    /**
     * @return all the projects associated with TestLink
     */
    public TestProject[] getProjects() {
        return testLink.getProjects();
    }

    /**
     * Return all the current test project names
     * @param sorted a boolean indicating if the project should be sorted by creation date
     * @return An array of objects that represent each test project name.
     */
    public Object[] getProjectNames(boolean sorted) {
        final Stream<TestProject> projects = Stream.of(getProjects());
        if (sorted) {
            Comparator<TestProject> comparator = (projectOne, projectTwo) ->
                    projectOne.getId().compareTo(projectTwo.getId());
            return projects.sorted(comparator.reversed()).
                    map(TestProject::getName).collect(toList()).toArray();
        } else {
            return projects.map(TestProject::getName)
                    .collect(toList()).toArray();
        }

    }


}
