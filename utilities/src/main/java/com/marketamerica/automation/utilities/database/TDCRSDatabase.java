package com.marketamerica.automation.utilities.database;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

import com.marketamerica.automation.testdata.Address;
import com.marketamerica.automation.utilities.TDCRSProcessInfo;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.marketamerica.automation.constants.GlobalConstants;
import com.marketamerica.automation.testdata.Distributor;
import com.marketamerica.automation.testdata.DistributorApplication; 
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.testdata.address.HongKongAddress;
import com.marketamerica.automation.testdata.attributes.DistributorType;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment; 
import com.marketamerica.automation.utilities.enums.ProductCategory;
import com.marketamerica.automation.constants.GlobalConstants.QASQL; 


/**
 * Provides functionality to interact with TDCRS SQL database
 *
 * @author kathyg
 */
public class TDCRSDatabase {
    private static final Logger logger = LogManager
            .getLogger(TDCRSDatabase.class);
    
    private DatabaseSession databaseSession;   

    public TDCRSDatabase() {
        try {
            Class.forName(GlobalConstants.SQL_SERVER_DRIVER_CLASS_NAME);
        } catch (ClassNotFoundException e) {
            logger.error(e);
        }

        if (Environment.getCurrentEnvironment().equals(Environment.STAGING)) {
            databaseSession = new DatabaseSession(GlobalConstants.SQL_SERVER,
                    QASQL.DB.Staging.URL, 1433, QASQL.DB.Staging.NAME,
                    QASQL.DB.PASSWORD, QASQL.DB.USER);
        } else if (Environment.getCurrentEnvironment().equals(Environment.LIVE)) {
            databaseSession = new DatabaseSession(GlobalConstants.SQL_SERVER,
                    QASQL.DB.Live.URL, 1433, QASQL.DB.Live.NAME,
                    QASQL.DB.PASSWORD, QASQL.DB.USER);
        } else {
            throw new IllegalStateException(String.format("Could not connect to database. Please check for valid " +
                    "environment (%s)", Environment.getCurrentEnvironment()));
        }         
    }
    
    public DatabaseSession getDatabaseSession() {
    	return databaseSession;
    }
    
    public void setDatabaseSession (DatabaseSession databaseSession) {
    	this.databaseSession = databaseSession;
    }      
    
    public boolean isConnected() {
        try {
            return databaseSession != null && !databaseSession.getConnection().isClosed();
        } catch (SQLException e) {
            return false;
        }
    }
    
    public void disconnect() {
        if (isConnected())
            databaseSession.disconnect();
    }

    public int insertIncompleteDistributor(DistributorApplication distributorApplication) {

        String insertString;

        insertString = "INSERT INTO Distributor_In_Process (memberID, repID, password, batchID, unoptAutoRenewal, " +
                "optInAutoRenewal, optOutAutoRenewal, completeGSW, placeIRC, IRCRegion, IRCPlacement, IRCPlacementLeg, " +
                "UAMExpansion, UAMExpandedCountry, UAMExpansionBDC, UAPExpansion, UAPExpandedCountry, UAPExpansionBDC, " +
                "UEUExpansion, UEUExpandedCountry, UEUExpandedBDC, setUpShopPortal, setUPHPPortal, " +
                "customMiniUSA, customMiniCAN, customMiniMEX)" +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        Connection connection;
        PreparedStatement statement = null;
        int returnedInt = 0;
        try {
            connection = databaseSession.getConnection();
            statement = connection.prepareStatement(insertString);

            statement.setString(1, distributorApplication.getDistributor().getDistributorId());
            statement.setString(2, distributorApplication.getDistributor().getRepresentativeId());
            statement.setString(3, distributorApplication.getDistributor().getRepresentativePassword());
            statement.setString(4, distributorApplication.getBatchID());
            statement.setString(5, Boolean.toString(distributorApplication.isOptedAutoRenewal()));
            statement.setString(6, Integer.toString(distributorApplication.getOptInAutoRenewal()));
            statement.setString(7, Integer.toString(distributorApplication.getOptOutAutoRenewal()));
            statement.setString(8, Integer.toString(distributorApplication.gswStepNumber()));
            statement.setString(9, Integer.toString(distributorApplication.getPlaceIRC()));
            statement.setString(10, distributorApplication.getIrcRegion());
            statement.setString(11, distributorApplication.getIrcPlacement());
            statement.setString(12, distributorApplication.getIrcPlacementLeg());
            statement.setString(13, Integer.toString(distributorApplication.getUamExpansion()));
            statement.setString(14, distributorApplication.getUamExpandedCountry());
            statement.setString(15, distributorApplication.getUamExpansionBDC());
            statement.setString(16, Integer.toString(distributorApplication.getUapExpansion()));
            statement.setString(17, distributorApplication.getUapExpandedCountry());
            statement.setString(18, distributorApplication.getUapExpansionBDC());
            statement.setString(19, Integer.toString(distributorApplication.getUeuExpansion()));
            statement.setString(20, distributorApplication.getUeuExpandedCountry());
            statement.setString(21, distributorApplication.getUeuExpansionBDC());
            statement.setString(22, Integer.toString(distributorApplication.getSetUpShopPortal()));
            statement.setString(23, Integer.toString(distributorApplication.getSetUpHPPortal()));
            statement.setString(24, Integer.toString(distributorApplication.getCustomMiniUSA()));
            statement.setString(25, Integer.toString(distributorApplication.getCustomMiniCAN()));
            statement.setString(26, Integer.toString(distributorApplication.getCustomMiniMEX()));

            returnedInt = statement.executeUpdate();
            connection.commit();

        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e1) {
                    logger.error(e1);
                }
            }
        }

        return  returnedInt;
    }
     
    /**
     * Insert distributor data to TDCRS Distributor table
     *
     * @param distributorApplication
     * @return number of row inserted to verify if the transaction successful or not
     */
    public int insertDistributor(DistributorApplication distributorApplication) { 
    	String insertString;         
        
        // BDCExt set to 001 by default
        insertString = "INSERT INTO Distributor (memberID, homeCountry, entryType, emailAddress,"
        		+ "repId, password, PPCId, applicationType, pinLevel, firstName, lastName, homePhone)"
        		+ "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
        
        Connection connection;
        PreparedStatement statement = null;
        int returnedInt = 0;
        try {
            connection = databaseSession.getConnection();
            statement = connection.prepareStatement(insertString);
            
            statement.setString(1, distributorApplication.getDistributor().getDistributorId());
            statement.setString(2, distributorApplication.getDistributor().getAddress().getCountry());
            statement.setString(3, distributorApplication.getEntryType().toString());
            statement.setString(4, distributorApplication.getDistributor().getEmail());
            statement.setString(5, distributorApplication.getDistributor().getRepresentativeId());
            statement.setString(6, distributorApplication.getDistributor().getRepresentativePassword());
            statement.setString(7, distributorApplication.getDistributor().getPcID()); 
            statement.setString(8, distributorApplication.getApplicationType().toString());
            statement.setString(9, distributorApplication.getDistributor().getPin().toString());
            statement.setString(10, distributorApplication.getDistributor().getFirstName());
            statement.setString(11, distributorApplication.getDistributor().getLastName());
            statement.setString(12, distributorApplication.getDistributor().getPhoneNumber());
            
            // waiting for DSB call to check to if a distributor is in MBRF exclude file
            //statement.setBoolean(8, distributorApplication.getDistributor().isTestingAccount());
            
            returnedInt = statement.executeUpdate();   
            connection.commit();
             
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e1) {
                    logger.error(e1);
                }
            } 
        } 
        
        return returnedInt;
    }  
    
    /**
     * Find a valid sponsor distributor from TDCRS Distributor table based on distributor type and country
     *
     * @param distributorType
     * @param country
     * @return Distributor
     */
    public Distributor getSponsorDistributor(DistributorType distributorType, Country country) { 
    	Distributor sponsorDistributor = new Distributor(); 
    	String sponsorDistributorID = null;
    	String sponsorRepId = null;
    	String sponsorPassword = null;
    	
    	String query;         
        
        // retrieve 1 row of data
        query = "SELECT Top 1 memberID, repId, password FROM Distributor "
        		+ "WHERE entryType = ? and homeCountry = ? and pinLevel IN ('D', 'C', 'E', 'M') "
        		+ "ORDER BY NEWID()";
                        
        Connection connection;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        
        try {
            connection = databaseSession.getConnection();
            statement = connection.prepareStatement(query);
            
            statement.setString(1, distributorType.toString());
            statement.setString(2, country.toString());             
            
            resultSet = statement.executeQuery();   
            
            while (resultSet.next()) {
            	sponsorDistributorID = resultSet.getString("memberID")
                        .replaceAll("\\n", "").trim();
                sponsorRepId = resultSet.getString("repId")
                        .replaceAll("\\n", "").trim();
                sponsorPassword = resultSet.getString("password")
                        .replaceAll("\\n", "").trim();
            }
            
            sponsorDistributor.setDistributorId(sponsorDistributorID);
            sponsorDistributor.setRepresentativeId(sponsorRepId);
            sponsorDistributor.setRepresentativePassword(sponsorPassword);    
            
            resultSet.close();
                         
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e1) {
                    logger.error(e1);
                }
            }             
        } 
        return sponsorDistributor;
    }

    public Distributor getIncompleteDistributor(String databaseField) {

        Distributor distributor = new Distributor();
        distributor.setAddress(new Address());
        String distributorID = null;
        String repId = null;
        String password = null;

        String query;

        // retrieve 1 row of data
        query = String.format("SELECT Top 1 memberID, repId, password " +
                "FROM Distributor_In_Process "
                + "WHERE %s>0", databaseField);

        Connection connection;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = databaseSession.getConnection();
            statement = connection.prepareStatement(query);

            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                distributorID = resultSet.getString("memberID")
                        .replaceAll("\\n", "").trim();
                repId = resultSet.getString("repId")
                        .replaceAll("\\n", "").trim();
                password = resultSet.getString("password")
                        .replaceAll("\\n", "").trim();
            }

            distributor.setDistributorId(distributorID);
            distributor.setRepresentativeId(repId);
            distributor.setPassword(password);

            resultSet.close();

        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e1) {
                    logger.error(e1);
                }
            }
        }
        return distributor;
    }

    public Distributor getIncompleteDistributor(Country country, String databaseField) {

        Distributor distributor = new Distributor();
        String distributorID = null;
        String repId = null;
        String password = null;
        String firstName = null;
        String lastName = null;
        String emailAddress = null;

        String query;

        // retrieve 1 row of data
        query = String.format("SELECT Top 1 memberID, repId, password, firstName, lastName, emailAddress, homeCountry " +
                "FROM Distributor "
                + "WHERE homeCountry = ? and %s>0", databaseField);

        Connection connection;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = databaseSession.getConnection();
            statement = connection.prepareStatement(query);

            statement.setString(1, country.toString());

            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                distributorID = resultSet.getString("memberID")
                        .replaceAll("\\n", "").trim();
                repId = resultSet.getString("repId")
                        .replaceAll("\\n", "").trim();
                password = resultSet.getString("password")
                        .replaceAll("\\n", "").trim();
                firstName = resultSet.getString("firstName")
                        .replaceAll("\\n", "").trim();
                lastName = resultSet.getString("lastName")
                        .replaceAll("\\n", "").trim();
                emailAddress = resultSet.getString("emailAddress")
                        .replaceAll("\\n", "").trim();
            }

            distributor.setDistributorId(distributorID);
            distributor.setRepresentativeId(repId);
            distributor.setPassword(password);
            distributor.setFirstName(firstName);
            distributor.setLastName(lastName);
            distributor.setEmail(emailAddress);
            distributor.setAddress(getAddress(country.toString()));

            resultSet.close();

        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e1) {
                    logger.error(e1);
                }
            }
        }
        return distributor;
    }

    public Distributor getIncompleteDistributor(TDCRSProcessInfo... processes) {

        Distributor distributor = new Distributor();
        String distributorID = null;
        String repId = null;
        String password = null;
        String stepNum = null;

        String query;

        // retrieve 1 row of data
        query = String.format("SELECT Top 1 * " +
                "FROM Distributor_In_Process "
                + "WHERE %s>0", processes[0].getProcessName(), processes[0].getProcessName());

        for (int i = 1; i < processes.length; i++) {
            query = query.concat(String.format(" and %s>0", processes[i].getProcessName()));
        }

        Connection connection;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = databaseSession.getConnection();
            statement = connection.prepareStatement(query);

            resultSet = statement.executeQuery();

            while (resultSet.next()) {
                distributorID = resultSet.getString("memberID")
                        .replaceAll("\\n", "").trim();
                repId = resultSet.getString("repId")
                        .replaceAll("\\n", "").trim();
                password = resultSet.getString("password")
                        .replaceAll("\\n", "").trim();
                for (int i = 0; i < processes.length; i++) {
                    TDCRSProcessInfo processInfo = processes[i];
                    processInfo.setStepNumber(Integer.parseInt(resultSet.getString(processInfo.getProcessName())
                            .replaceAll("\\n", "").trim()));
                }
            }

            distributor.setDistributorId(distributorID);
            distributor.setRepresentativeId(repId);
            distributor.setPassword(password);
            //processInfo.setStepNumber(Integer.parseInt(stepNum));

            resultSet.close();

        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e1) {
                    logger.error(e1);
                }
            }
        }
        return distributor;
    }

    public void findDistributorByDistID(Distributor distributor) {


        //String distributorID = null;
        //String repId = null;
        //String password = null;
        String firstName = null;
        String lastName = null;
        String emailAddress = null;
        String homeCountry = null;

        String query;

        // retrieve Distributor
        query = String.format("SELECT firstName, lastName, emailAddress, homeCountry " +
                "FROM Distributor " +
                "WHERE memberID=?");

        Connection connection;
        PreparedStatement statement = null;
        ResultSet resultSet = null;

        try {
            connection = databaseSession.getConnection();
            statement = connection.prepareStatement(query);

            statement.setString(1, distributor.getDistributorId());

            resultSet = statement.executeQuery();

            while (resultSet.next()) {
/*                distributorID = resultSet.getString("memberID")
                        .replaceAll("\\n", "").trim();*/
                /*repId = resultSet.getString("repId")
                        .replaceAll("\\n", "").trim();
                password = resultSet.getString("password")
                        .replaceAll("\\n", "").trim();*/
                firstName = resultSet.getString("firstName")
                        .replaceAll("\\n", "").trim();
                lastName = resultSet.getString("lastName")
                        .replaceAll("\\n", "").trim();
                emailAddress = resultSet.getString("emailAddress")
                        .replaceAll("\\n", "").trim();
                homeCountry = resultSet.getString("homeCountry")
                        .replaceAll("\\n", "").trim();

            }

            //distributor.setDistributorId(distributorID);
            /*distributor.setRepresentativeId(repId);
            distributor.setPassword(password);*/
            distributor.setFirstName(firstName);
            distributor.setLastName(lastName);
            distributor.setEmail(emailAddress);

            distributor.setAddress(getAddress(homeCountry));

            resultSet.close();

        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e1) {
                    logger.error(e1);
                }
            }
        }
    }

    public int updateRecord(String distributorID, String columnName, Object fieldValue) {
        String query = String.format("UPDATE Distributor_In_Process " +
                "SET %s=? WHERE memberID=?", columnName);

        Connection connection;
        PreparedStatement statement = null;
        int returnedInt = 0;
        try {
            connection = databaseSession.getConnection();
            statement = connection.prepareStatement(query);

            statement.setString(1, fieldValue.toString());
            statement.setString(2, distributorID);

            returnedInt = statement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e1) {
                    logger.error(e1);
                }
            }
        }

        query = String.format("UPDATE Distributor_In_Process " +
                "SET %s=? WHERE memberID=?", columnName);

        statement = null;
        returnedInt = 0;
        try {
            connection = databaseSession.getConnection();
            statement = connection.prepareStatement(query);

            statement.setString(1, fieldValue.toString());
            statement.setString(2, distributorID);

            returnedInt = statement.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e1) {
                    logger.error(e1);
                }
            }
        }

        return returnedInt;
    }
    
    
    /**
     * Find a valid product from TDCRS Product table based on product country and product category
     *
     * @param country
     * @param category
     * @return Product
     */
    public Product getProductByCategory(Country country, ProductCategory category) { 
    	Product product = new Product(); 
    	String productCode  = null;
    	BigDecimal distributorCost = new BigDecimal(0);
    	BigDecimal retailCost = new BigDecimal(0);
    	BigDecimal BV = new BigDecimal(0);
    	BigDecimal IBV = new BigDecimal(0);
    	Boolean freight = null;
    	    	
    	String query;         
        
        // only retrieve 1 row of data
        query = "SELECT Top 1 productCode, distributorCost, retailCost, productBV, "
        		+ "productIBV, isFreightCharged FROM Product "
        		+ "WHERE productCountry = ? and productCategory = ? and isDiscontinued != 1 and isOutOfStock != 1 "
        		+ "ORDER BY NEWID()";    
        
        Connection connection;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        
        try {
            connection = databaseSession.getConnection();
            statement = connection.prepareStatement(query);
            
            statement.setString(1, country.toString());
            statement.setString(2, category.toString());             
            
            resultSet = statement.executeQuery();   
            
            while (resultSet.next()) {
            	productCode = resultSet.getString("productCode")
                        .replaceAll("\\n", "").trim();
            	distributorCost = resultSet.getBigDecimal("distributorCost");
            	retailCost = resultSet.getBigDecimal("retailCost");
            	BV = resultSet.getBigDecimal("productBV");
            	IBV = resultSet.getBigDecimal("productIBV");
            	freight = resultSet.getBoolean("isFreightCharged");
            }
            
            product.setSku(productCode);
            product.setDistributorCost(distributorCost);
            product.setRetailCost(retailCost);   
            product.setProductBV(BV);
            product.setProductIBV(IBV);
            product.setFreight(freight);
            
            resultSet.close();
                         
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e1) {
                    logger.error(e1);
                }
            }             
        } 
        return product;
    }     
    
    /**
     * Find a valid product from TDCRS Product table based on product country and product category default to Isotonix
     *
     * @param country
     * @return Product
     */
    public Product getProductByCountry(Country country) { 
    	return getProductByCategory(country, ProductCategory.ISOTONIX);
    }
    
     
    /**
     * Find a valid address from TDCRS Address table based on country or special area
     *
     * @param addressSearchKey
     * @return Address
     */
    public Address getAddress(String addressSearchKey) { 
    	Address address = new Address(); 
    	String addressId  = null;
    	String address1 = null;
    	String address2 = null;
    	String address3 = null;
    	String address4 = null;
    	String address5 = null;
    	String address6 = null;
    	String address7 = null;
    	String city = null;
    	String state = null;
    	String postCode = null;
    	String hongKongDistrict = null;
    	String hongKongArea = null;   
    	
    	Country country = Country.parse(addressSearchKey);    	
    	
    	String query = null;       
    	
    	if (!country.equals(Country.NOT_CLASSIFIED)) {    		
    		// retrieve 1 row of data based on country
            query = "SELECT Top 1 * FROM Address "
            		+ "WHERE country = ? "
            		+ "ORDER BY NEWID()";      		
    	} else {
    		// retrieve 1 row of data based on special area, such as Canary Islands, or special HKG regions
            query = "SELECT Top 1 * FROM Address "
            		+ "WHERE specialArea = ? "
            		+ "ORDER BY NEWID()";     		
    	}           
        
        Connection connection;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        
        try {
            connection = databaseSession.getConnection();
            statement = connection.prepareStatement(query);
            
            statement.setString(1, addressSearchKey);                         
            
            resultSet = statement.executeQuery();   
            
            while (resultSet.next()) {
            	addressId = resultSet.getString("addressID").trim();
            	// street1 (address1) is the required field for all countries
            	address1 = resultSet.getString("address1").trim();
            	country = Country.parse(resultSet.getString("country").trim());              	
            	
            	switch (country) {
            	case UNITED_STATES:
            	case CANADA:
            	case AUSTRALIA:
            		address2 = resultSet.getString("address2").trim();
            		city = resultSet.getString("city").trim();
            		state = resultSet.getString("state").trim();
            		postCode = resultSet.getString("postalCode").trim();
                    break;
            	case UNITED_KINGDOM:	
            		address2 = resultSet.getString("address2").trim();
            		city = resultSet.getString("city").trim();            		 
            		postCode = resultSet.getString("postalCode").trim();
                    break;
            	case SPAIN:
            	case COLOMBIA:
            	case BAHAMAS:
            	case DOMINICAN_REPUBLIC:
            	case ECUADOR:
            	case JAMAICA:
            	case PANAMA:
            	case NEW_ZEALAND:
            		address2 = resultSet.getString("address2").trim();
            		address3 = resultSet.getString("address3").trim();
            		city = resultSet.getString("city").trim();
            		state = resultSet.getString("state").trim();
            		postCode = resultSet.getString("postalCode").trim();
                    break;
            	case TAIWAN:	
            		city = resultSet.getString("city").trim();             		 
             		postCode = resultSet.getString("postalCode").trim();
                    break;
            	case SINGAPORE:	
            		address2 = resultSet.getString("address2").trim();
             		postCode = resultSet.getString("postalCode").trim();
                    break;
            	case MEXICO:
            	case CHINA:
            		address2 = resultSet.getString("address2").trim();
            		address3 = resultSet.getString("address3").trim();
            		address4 = resultSet.getString("address4").trim();
            		city = resultSet.getString("city").trim();
            		state = resultSet.getString("state").trim();
            		postCode = resultSet.getString("postalCode").trim();
                    break;
                case HONG_KONG:
                	address3 = resultSet.getString("address3").trim();
            		address4 = resultSet.getString("address4").trim();
            		address5 = resultSet.getString("address5").trim();
            		address6 = resultSet.getString("address6").trim();
            		address7 = resultSet.getString("address7").trim();
            		hongKongDistrict = resultSet.getString("HongKongDistrict").trim();
            		hongKongArea = resultSet.getString("HongKongArea").trim();
                    break;
                default:
                	break;
            	}            	
            	 
            }
            
            address = Address.createAddress(country);

            address.setAddressId(addressId);
            address.setStreet(address1);
            address.setStreet2(address2);
            address.setStreet3(address3);
            address.setStreet4(address4);
            address.setStreet5(address5);
            address.setStreet6(address6);
            address.setStreet7(address7);
            address.setCity(city);
            address.setState(state);
            address.setCountry(country);
            address.setZipCode(postCode);
            
            if(address instanceof HongKongAddress) {
            	((HongKongAddress) address).setDistrict(hongKongDistrict);
            	((HongKongAddress) address).setArea(hongKongArea);
            }
             
            resultSet.close();
                         
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e1) {
                    logger.error(e1);
                }
            }             
        } 
        return address;
    }     
    
    // This method is for testing SQL statements purpose only. 
    public int insertDistributorTest() {       	 

        String insertString;         
        
        insertString = "INSERT INTO Distributor (memberID, homeCountry, entryType, emailAddress,"
        		+ "repId, password, PPCId, inExcludeMBRF)"
        		+ "VALUES ('572602449', 'UNITED_STATES', 'UnFranchise Owner', 'ZMKOZD.AUTOMATION@YOPMAIL.COM', "
        		+ "'0673376', 'RVBHXH', '1000100374', 1)";                               
        
        Connection connection;
        Statement statement = null;
        int returnedInt = 0;
        try {
            connection = databaseSession.getConnection();             
            statement = connection.createStatement();            
            
            returnedInt = statement.executeUpdate(insertString);   
            connection.commit();
             
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e1) {
                    logger.error(e1);
                }
            }             
        } 
        return returnedInt;
    }  
    
}
