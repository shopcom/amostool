package com.marketamerica.automation.utilities.helpers;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * This class will hold all utility methods regarding date and time
 *
 * @author javierv
 */
public final class DateAndTimeUtilities {
    public static final String EXCEL_DATE = "EEE MMM d kk:mm:ss ZZZ yyyy";
    final static Logger logger = LogManager
            .getLogger(DateAndTimeUtilities.class);

    /**
     * Get a formated date string based on a Date object and Simple Date Format
     * string
     *
     * @param date       a Date object whose date you want returned as a string value
     * @param dateFormat a string representing a desired string format
     * @return a formatted string representing the given date object
     */
    public static String getFormattedDate(final Date date,
                                          final String dateFormat) {
        final SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
        return sdf.format(date);
    }

    public static String getFormatedDate(final Date date,
                                         final DateFormat dateFormat) {
        return dateFormat.format(date);
    }

    /**
     * Generate a Calendar object relative to the given parameters. This is
     * useful if wanting a date string in the future or past
     *
     * @param amountToAdjustMonth amount of months to adjust the date by. Use a negative value if you want to go to a past month
     * @param amountToAdjustHour  amount of hours to adjust the date by. Use a negative value if you want to go to a past hour
     * @param amountToAdjustDay   amount of days to adjust the calendar by. Use a negative value if you want to go to a past day
     * @return
     */
    public static Calendar generateRelativeCalendar(
            final int amountToAdjustMonth, final int amountToAdjustHour,
            final int amountToAdjustDay) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH, amountToAdjustMonth);
        calendar.add(Calendar.HOUR_OF_DAY, amountToAdjustHour);
        calendar.add(Calendar.DAY_OF_MONTH, amountToAdjustDay);
        return calendar;
    }

    public static Date getFormattedDate(String value, SimpleDateFormat dateFormat) {
        try {
            return dateFormat.parse(value);
        } catch (ParseException e) {
            logger.error(String.format("Date String (%s) and Format (%s) did not Match", value, dateFormat));
            return null;
        }
    }

    /**
     * Get a short formatted date based on a given locale
     *
     * @param date   a date to format
     * @param locale a locale to format the date by
     * @return a string representing the date in the provided format
     */
    public static String getFormattedDate(Date date, Locale locale) {
        return DateFormat.getDateInstance(DateFormat.SHORT, locale).format(date);
    }
}
