package com.marketamerica.automation.utilities.excel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

/**
 * This class allows us to write ExcelWorkbooks via the Apache POI library. It
 * contains functions to create rows/sheets, add column headers (row 0), set and
 * get values in a row by column name
 *
 * @author Javier L. Velasquez, Market America/SHOP.com
 */
public class ExcelWorkBookWriter extends ExcelWorkBook {
    private FileOutputStream out;
    private String fileName;

    /**
     * Create an Excel Workbook and a default sheet
     *
     * @param fileName string representing the destination file location
     */
    public ExcelWorkBookWriter(final String fileName) {
        this(fileName, false);
    }

    /**
     * Create an ExcelWorkBookWriter
     *
     * @param fileName           string representing the destination file location
     * @param createDefaultSheet a boolean telling us whether or not we should create the
     *                           default sheet
     */
    public ExcelWorkBookWriter(final String fileName,
                               final boolean createDefaultSheet) {
        this.fileName = fileName;
        workbook = new SXSSFWorkbook(-1);
        if (createDefaultSheet)
            currentSheet = workbook.createSheet("Default");
    }


    /**
     * Add headers to the current sheet
     *
     * @param headers a list of values to used to represent the headers in the
     *                current sheet
     */
    public void addHeaders(final List<String> headers) {
        final Row columns = this.createRow();
        for (int i = 0; i < headers.size(); i++) {
            final Cell cell = columns.createCell(i);
            cell.setCellValue(headers.get(i));
        }
    }


    public void addHeader(String newHeader) {
        final Row row = this.getRow(0);
        final Cell cell = row.createCell(row.getLastCellNum() + 1);
        cell.setCellValue(newHeader);
    }


    /**
     * Set a value in a row based on column name
     *
     * @param row     a row to alter
     * @param column  a column used to determine the cell to udpate
     * @param content the content to set the cell value in the row
     */
    public void setRowValueByColumnName(final Row row, final Object column,
                                        final String content) {
        final int columnIndex = getColumnIndex(String.valueOf(column));
        Cell cell = row.getCell(columnIndex);
        if (cell == null) {
            cell = row.createCell(columnIndex);
        }
        row.getCell(columnIndex).setCellValue(content);
    }


    /**
     * Create a row at a specified index in the current sheet
     *
     * @param index an index representing the row in the current sheet
     * @return the newly created row
     */
    public Row createRow(final int index) {
        return this.currentSheet.createRow(index);
    }

    /**
     * <p>
     * Creates a new row. This method is useful for iterating over data sets.
     * </p>
     *
     * @return a newly created row.
     */
    public Row createRow() {
        if (this.currentSheet.getLastRowNum() == 0) {
            final Row row = this.getRow(0);
            if (row == null) {
                return createRow(0);
            } else {
                return createRow(1);
            }
        } else {
            return createRow(this.currentSheet.getLastRowNum() + 1);
        }
    }

    /**
     * Write and close the current workbook
     */
    public void writeAndCloseWork() {
        File file = new File(fileName);

        try {
            out = new FileOutputStream(file);
            logger.info(String.format("Saved workbook to %s", file.getAbsoluteFile().toString()));
        } catch (FileNotFoundException e) {
            logger.error(e);
        }
        try {
            workbook.write(out);
        } catch (IOException e) {
            logger.error(e);
        }
        try {
            out.close();
        } catch (IOException e) {
            logger.error(e);
        }
        ((SXSSFWorkbook) workbook).dispose();
    }

    /**
     * Add a row of data to the current workbook.
     * @param rowList a row of objects
     * @param <T> T represents any object that extends {@code Object}.
     * Its string form will be used for serialization into Excel.
     */
    public <T> void addRow(final List<T> rowList) {
        final Row row = this.createRow();
        for (int i = 0; i < rowList.size(); i++) {
            row.createCell(i).setCellValue(String.valueOf(rowList.get(i)));
        }
    }

    /**
     * Add a row to the current workbook. Each key should represent an existing column. If the column doesn't exist,
     * this method will add it. All previous rows will appear to have an empty cell value
     * @param map a map of values whose keys represent columns and values represent data for a new row
     * @param <T> T represents any object that extends {@code Object}.
     */
    public <T> void addRow(final Map<String,T> map) {
        final Row row = this.createRow();

        final Set<String> keySet = map.keySet();
        final List<String> headers = getHeaders();
        final Collection<String> newColumns = new ArrayList<>();

        // Add any existing keySet
        for (String column : keySet) {
            // Add row data only if the column exists if it doesn't then store it to be processed later
            if (headers.contains(column)) {
                setRowValueByColumnName(row, column, String.valueOf(map.get(column)));
            } else {
                newColumns.add(column);
            }
        }

        // Add any new keySet
        for (String newColumn : newColumns) {
            // Create the header before adding the cell data
            this.addHeader(newColumn);
            setRowValueByColumnName(row, newColumn, String.valueOf(map.get(newColumn)));
        }
    }

    public void resizeAllColumns() {
        final int lastColumnNumber = this.getRow(0).getLastCellNum();
        for (int i = 0; i < lastColumnNumber; ++i)
            currentSheet.autoSizeColumn(i);
    }

}
