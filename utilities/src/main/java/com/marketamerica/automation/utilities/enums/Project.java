package com.marketamerica.automation.utilities.enums;

/**
 * Created by javierv on 8/15/2014.
 */
public enum Project {
    MINI("mini"), CUSTOM_MINI("custom-mini"), SHOP("shop.com"), NUTRAMETIX("nutrametrix"),
    ISOTONIX("isotonix"), TRANSITIONS("transitions"), MOTIVES_COSMETICS("motives-cosmetics"),
    MOTIVES_COSMETICS_MOBILE("motives-cosmetics-mobile"),
    AUTOMATION_UTILITIES("automation-utilities"), GLOBAL_SHOP("global-shop"),
    UNIVERSAL_WORKFLOW("universal-workflow"), UNFRANCHISE("UnFranchise"), DSB_CLIENT("dsb-client");

    private final String automationProject;

    private Project(final String automationProject) {
        this.automationProject = automationProject;
    }

    public static Project parse(final String automationProject) {
        for (final Project currentProject : values()) {
            if (currentProject.toString().equalsIgnoreCase(
                    automationProject.toString())) {
                return currentProject;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return automationProject;
    }

}
