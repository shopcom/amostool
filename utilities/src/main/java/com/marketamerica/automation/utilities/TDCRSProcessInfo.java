package com.marketamerica.automation.utilities;

/**
 * Created by Nicholas Spitzer on 7/20/2015.
 */
public class TDCRSProcessInfo {

    private String processName;
    private int stepNumber;

    public TDCRSProcessInfo() {
        processName = "";
        stepNumber = 1;
    }

    public  TDCRSProcessInfo(String processName) {
        this.processName = processName;
        stepNumber = 1;
    }

    public TDCRSProcessInfo(String processName, int stepNumber) {
        this.processName = processName;
        this.stepNumber = stepNumber;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public int getStepNumber() {
        return stepNumber;
    }

    public void setStepNumber(int stepNumber) {
        this.stepNumber = stepNumber;
    }
}
