package com.marketamerica.automation.utilities.helpers;

import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

/**
 * Secure Shell Connection represents a connection with a Secure Shell Server.
 * This class is beneficial for port forwarding. e.g. Connecting to a DB over
 * SSH :)
 *
 * @author javierv
 */
public final class SecureShellConnection {
    protected static final Logger logger = LogManager
            .getLogger(SecureShellConnection.class);
    private Session session;
    private String userName;
    private String password;
    private String host;
    private int port;
    private int registeredPort;

    public SecureShellConnection(final String userName, final String password,
                                 final String host, final int port) {
        this.userName = userName;
        this.password = password;
        this.host = host;
        this.port = port;
    }

    /**
     * Create a secure shell session
     */
    public void connect() {
        final JSch jsch = new JSch();
        try {
            session = jsch.getSession(userName, host, port);
        } catch (JSchException e) {
            logger.error(e);
        }
        session.setPassword(password);

        final Properties config = new Properties();
        config.put("StrictHostKeyChecking", "no");
        session.setConfig(config);
        logger.debug(String.format("Creating SSH Session on %s::%s", host, port));
        try {
            session.connect();
        } catch (JSchException e) {
            logger.error(e);
        }
    }

    /**
     * Registers the local port forwarding for loop-back interface. If lport is
     * 0, the tcp port will be allocated.
     *
     * @param localPort  local port for local port forwarding
     * @param remoteHost host address for local port forwarding
     * @param remotePort remote port number for local port forwarding
     * @return an allocated local TCP port number
     */
    public void setPortFowardingL(final int localPort, final String remoteHost,
                                  final int remotePort) throws JSchException {
        try {
            registeredPort = session.setPortForwardingL(localPort, remoteHost,
                    remotePort);
        } catch (JSchException e) {
            throw e;
        }
    }

    /**
     * Cancels the local port forwarding assigned at local TCP port lport on loopback interface.
     */
    public void delPortForwardingL() {
        try {
            session.delPortForwardingL(registeredPort);
        } catch (JSchException e) {
            logger.error(e);
        }
    }

    /**
     * Disconnect our SSH session
     */
    public void disconnect() {
        if (session != null) {
            session.disconnect();
            logger.debug(String.format("Terminating SSH Session %s", session));
        }
    }

}
