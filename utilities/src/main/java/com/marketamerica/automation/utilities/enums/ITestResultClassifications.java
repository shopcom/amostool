package com.marketamerica.automation.utilities.enums;

public enum ITestResultClassifications {
    NULL_POINTER_EXCEPTION("Null Pointer Exception"),
    NOT_CLASSIFIED("Not Classified"),
    WEB_DRIVER_CONFIGURATION_ISSUE("WebDriver Configuration Issue"),
    WEB_DRIVER_TIMEOUT("WebDriver Timeout"),
    NAVIGATION_FAILURE("Navigation Failure"),
    ELEMENT_IDENTIFICATION_ISSUE("Element Identifitication Issue");

    private String classification;

    private ITestResultClassifications(String classification) {
        this.classification = classification;
    }

    public static ITestResultClassifications classify(Throwable Throwable) {
        return NOT_CLASSIFIED;
    }

    @Override
    public String toString() {
        return classification;
    }

}