package com.marketamerica.automation.utilities.excel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

/**
 * Created by javierv on 5/6/2014.
 */
public abstract class ExcelWorkBook {
    protected final Logger logger = LogManager.getLogger(getClass());

    protected Workbook workbook;
    protected Sheet currentSheet;

    protected Map<String, List<String>> getRowDataAsMultiMap(final Row columnHeaders,
                                                             final Row row) {
        final Map<String, List<String>> rowData = new HashMap<>();
        final int lastCellNumber = row.getLastCellNum();
        for (int i = 0; i < lastCellNumber; i++) {
            if (columnHeaders.getCell(i) == null) {
                continue;
            }
            final String columnValue = columnHeaders.getCell(i)
                    .getStringCellValue();
            final String cellValue = getCellValueAsText(row.getCell(i));
            final List<String> values = new ArrayList<>();
            values.add(cellValue);
            if (rowData.containsKey(columnValue)) {
                values.addAll(rowData.get(columnValue));
            }
            rowData.put(columnValue, values);
        }
        return rowData;
    }

    protected Map<String, String> getRowData(final Row columnHeaders,
                                             final Row row) {
        final Map<String, String> rowData = new HashMap<>();
        final int lastCellNumber = row.getLastCellNum();
        for (int i = 0; i < lastCellNumber; i++) {
            final String columnValue = columnHeaders.getCell(i)
                    .getStringCellValue();
            final String cellValue = getCellValueAsText(row.getCell(i));
            rowData.put(columnValue, cellValue);
        }
        return rowData;
    }

    /**
     * If the first cell includes a double slash (//) or a pound (#), the row is treated as a comment row
     *
     * @param row a Excel workbook row
     * @return {@code true} if the row starts with a pound (#), {@code false} if the row starts with double slash
     */
    public boolean isCommentRow(final Row row) {
        try {
            final Cell cell = row.getCell(0);
            if (cell == null) {
                return false;
            }
            if (cell.getStringCellValue().startsWith("//") || cell
                    .getStringCellValue().startsWith("#")) {
                return true;
            }
        } catch (IllegalStateException e) {
            return false;
        }
        return false;
    }

    public boolean isValidRow(final Row row) {
        if (row == null) {
            return false;
        }
        final int physicalNumberOfCells = row.getPhysicalNumberOfCells();
        int invalidCellCount = 0;
        for (int i = 0; i < physicalNumberOfCells; ++i) {
            if (row.getCell(i) == null) {
                ++invalidCellCount;
            }
        }
        if (invalidCellCount == physicalNumberOfCells) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Gets the value from a cell as text. It will convert numeric text values
     * to string
     *
     * @param cell A cell we are looking to extract text from
     * @return The text value of the cell
     */
    protected String getCellValueAsText(final Cell cell) {
        String cellValue = null;
        if (cell == null) {
            return null;
        }
        try {
            if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC && DateUtil.isCellDateFormatted(cell)) {
                cellValue = cell.getDateCellValue().toString();
            } else {
                cellValue = cell.getStringCellValue();
            }
        } catch (IllegalStateException e) {
            if (e.getMessage().contains(
                    "Cannot get a text value from a numeric cell")) {
                cellValue = String.valueOf(cell.getNumericCellValue());
            } else if (e.getMessage().contains(
                    "Cannot get a text value from a boolean cell")) {
                cellValue = String.valueOf(cell.getBooleanCellValue());
            } else {
                logger.error(e);
            }
        }
        return cellValue;
    }

    /**
     * Get a value in a row based on a column name
     *
     * @param row    a row to search
     * @param column a column representing a row value
     * @return a string value representing by the column
     */
    public String getRowValueByColumnName(final Row row, final String column) {
        final int columnIndex = getColumnIndex(column);
        final Cell cell = row.getCell(columnIndex);
        if (cell == null) {
            return "";
        } else {
            return cell.getStringCellValue();
        }

    }

    /**
     * Searches the first row for a column and return its index
     *
     * @param column a column name
     * @return an integer value representing the column index (0 based)
     */
    protected int getColumnIndex(final String column) {
        final Row row = this.getRow(0);
        final int cellSize = row.getLastCellNum();
        for (int i = 0; i < cellSize; i++) {
            if (row.getCell(i).getStringCellValue().toLowerCase().trim()
                    .equals(column.toLowerCase().trim())) {
                return i;
            }
        }
        throw new IllegalArgumentException(String.format(
                "Column '%s' not found", column));
    }

    /**
     * Return a row in the current sheet based on a row number (0 based)
     *
     * @param rowNumber an integer representing a row index
     * @return a row found in the current sheet based on the index
     */
    public Row getRow(final int rowNumber) {
        return this.currentSheet.getRow(rowNumber);
    }

    /**
     * Create a sheet in our workbook
     *
     * @param sheetName a name used to identify the sheet in the workbook
     */
    public void createSheet(final String sheetName) {
        this.currentSheet = this.workbook.createSheet(sheetName);
    }

    /**
     * Switch to the current worksheet by name
     *
     * @param sheetName
     */
    public void switchWorkSheet(final String sheetName) {
        this.currentSheet = this.workbook.getSheet(sheetName);
    }

    public List<String> getRowData(int rowIndex) {
        final Row row = this.currentSheet.getRow(rowIndex);
        final List<String> rowData = new ArrayList<>();

        if (!isValidRow(row)) {
            return rowData;
        }

        for (int i = 0; i < row.getLastCellNum(); ++i) {
            rowData.add(getCellValueAsText(row.getCell(i)));
        }
        return rowData;
    }

    public List<String> getHeaders() {
        final Row columns = this.getRow(0);
        final List<String> columnsNames = new ArrayList<>();
        for (int i = 0; i < columns.getLastCellNum(); ++i) {
            final Cell cell = columns.getCell(i);
            columnsNames.set(i, getCellValueAsText(cell));
        }
        return columnsNames;
    }

}
