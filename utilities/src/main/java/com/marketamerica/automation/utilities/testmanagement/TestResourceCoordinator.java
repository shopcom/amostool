package com.marketamerica.automation.utilities.testmanagement;

import com.marketamerica.automation.utilities.enums.Project;

/**
 * Created by Javier L. Velasquez on 10/16/2014.
 */
public abstract class TestResourceCoordinator {

    private Project project;

    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}
