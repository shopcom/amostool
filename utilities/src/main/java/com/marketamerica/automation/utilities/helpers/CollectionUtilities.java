package com.marketamerica.automation.utilities.helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Contains all our utility methods used to interact with collections. Please
 * see <code>java.util.Collections</code> before adding any new methods here. <br>
 * <br>
 * The methods defined here should be generic in nature to interact with any
 * implementation of the Collection interface
 *
 * @author Javier L. Velasquez
 * @see java.util.Collections
 */
public final class CollectionUtilities {
    /**
     * Search a collection and see if any of the entries match a test string
     * partially
     *
     * @param collection a collection of string elements
     * @param string     a string to search for
     * @return a boolean indicating if there was a partial match of text in the
     * collection
     */
    public static boolean containsPartially(final Collection<String> collection,
                                            final String string) {
        if (string == null) {
            return false;
        }
        for (final String entry : collection) {
            if (entry.contains(string)) {
                return true;
            }
        }
        return false;
    }

    /**
     * <p>Returns a collection of elements that exist in collectionTwo but don't exist in collectionOne.</p>
     *
     * @param collectionOne a collection of elements
     * @param collectionTwo a collection of elements to remove from collection one
     * @return a collection of elements that exist in collection two that don't exist in collection one
     */
    public static <T> Collection<T> subtract(Collection<T> collectionOne, Collection<T> collectionTwo) {
        Collection<T> result = new ArrayList<>(collectionTwo);
        result.removeAll(collectionOne);
        return result;
    }

    /**
     * Create a case insensitive map.
     *
     * @param <T> an object representing the map's values
     * @return
     */
    public static <T> Map<String, T> createCaseInSensitiseMap() {
        return new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
    }

    /**
     * Converts an array to an array list.
     *
     * @param objects a list of objects
     * @param <T>     any type
     * @return an ArrayList<T> </T>that is not fixed sized (like when using {@code Arrays#asList})
     */
    @SafeVarargs
    public static <T> List<T> createList(T... objects) {
        return new ArrayList<>(Arrays.asList(objects));
    }

    /**
     * Removes any values from the provided collection when the value's string form matches the provided search
     * value's string form
     *
     * @param collection    a collection of objects
     * @param searchTerm    a value to search for in the collection
     * @param caseSensitive indicates if comparison is case sensitive
     * @param <T>           any descendant of object
     */
    public static <T> void removeMatches(Collection<T> collection, Object searchTerm, boolean caseSensitive) {
        final Iterator<T> iterator = collection.iterator();

        while (iterator.hasNext()) {
            final T next = iterator.next();
            if (caseSensitive) {
                if (String.valueOf(next).contains(String.valueOf(searchTerm))) {
                    iterator.remove();
                }
            } else {
                if (String.valueOf(next).toLowerCase().contains(String.valueOf(searchTerm).toLowerCase())) {
                    iterator.remove();
                }
            }

        }
    }

}
