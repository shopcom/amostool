package com.marketamerica.automation.utilities.enums;

import com.marketamerica.automation.utilities.helpers.NumberUtilities;

/**
 * Created by archanac on 3/16/2015.
 */
public enum CustomMiniLayout {
    AntiAging("Anti-Aging"), 
    CellularLaboratories("Cellular Laboratories"),
    DNAMiracles("DNA Miracles"),
    HealthAndNutrition("Health and Nutrition"),
    HomeCare("Home Care"),
    Lumiere("Lumière de Vie"),
    MAExclusiveBrands("MA Exclusive Brands"),
    PersonalCareAndBeauty("Personal Care and Beauty"),
    PetCare("Pet Care"),
    SalonAndSpa("Salon And Spa"),
    SportsNutrition("Sports Nutrition"),
    WeightManagement("Weight Management");
    
    private String layout;

    private CustomMiniLayout(final String layout) {
        this.layout = layout;
    }

    public static CustomMiniLayout getRandom() {
        return NumberUtilities.getRandomValueInArray(values());
    }

    public String getValue() {
        return layout;
    }

    @Override
    public String toString() {
        return layout;
    }

}
