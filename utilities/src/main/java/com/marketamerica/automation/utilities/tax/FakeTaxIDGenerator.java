package com.marketamerica.automation.utilities.tax;

import static org.apache.commons.lang.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang.RandomStringUtils.randomNumeric;

import java.util.Random;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.utilities.Luhn;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.helpers.NumberUtilities;

/**
 * Created by Javier L. Velasquez on 10/1/2014.
 */
public class FakeTaxIDGenerator {

    public static String generate() {
        return generate(Configuration.getCountry());
    }


    public static String generate(Country country) {
        switch (country) {
            case AUSTRALIA:
                // This is actually a real tax id
                return AustraliaTaxIDGenerator.generateTaxID();
            case CHINA:
                break;
            case HONG_KONG:
                return randomAlphabetic(1).toUpperCase() + randomNumeric(7);
            case TAIWAN:
                return randomAlphabetic(2).toUpperCase() + randomNumeric(8);
            case UNITED_KINGDOM:
                return randomAlphabetic(2).toUpperCase() + randomNumeric(6) + randomAlphabetic(1).toUpperCase();
            case UNITED_STATES:
                return randomNumeric(9);
            case CANADA:
                // this is actually a real tax id
                String digits = String.valueOf(NumberUtilities.randomInteger(new Random(), 10000000, 99999999));
                digits = digits + Luhn.calculateCheckDigit(Long.valueOf(digits));
                return digits;
            case MEXICO:
                return randomAlphabetic(3).toUpperCase() + randomNumeric(9);
            case SPAIN:
                return randomNumeric(8) + randomAlphabetic(1).toUpperCase();
            case SINGAPORE:
                return randomAlphabetic(1).toUpperCase() + randomNumeric(7) + randomAlphabetic(1).toUpperCase();
            case COLOMBIA:
            	return randomNumeric(9);
            case NOT_CLASSIFIED:
                break;
            case EMP:
                break;
        }
        return "";
    }
}
