package com.marketamerica.automation.utilities.enums;


/**
 * Created by kathyg on 10/31/2014.
 */
public enum HongKongPickupLocation {
    KWUN_TONG("Kwun Tong"), WANCHAI("Wanchai"), TSIM_SHA_TSUI("Tsim Sha Tsui");
    private String hongkongPickupLoc;

    private HongKongPickupLocation(final String hongkongPickupLoc) {
        this.hongkongPickupLoc = hongkongPickupLoc;
    }

    /**
     * @param string a string that represents a Hong Kong Pick up location
     * @return a enum constant representing a Hong Kong Pick up location.
     * Returns null if no pickup location found.
     */
    public static HongKongPickupLocation parse(String string) {
        final HongKongPickupLocation[] pickupLocations = values();
        for (HongKongPickupLocation hongKongPickupLocation : pickupLocations) {
            if (hongKongPickupLocation.toString().equalsIgnoreCase(
                    string.trim())) {
                return hongKongPickupLocation;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        return hongkongPickupLoc;
    }

}
