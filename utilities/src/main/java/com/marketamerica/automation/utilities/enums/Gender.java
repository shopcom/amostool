package com.marketamerica.automation.utilities.enums;

import com.marketamerica.automation.utilities.helpers.NumberUtilities;

/**
 * Created by javierv on 5/5/2014.
 */
public enum Gender {
    MALE("Male"), FEMALE("Female");
    private String gender;

    private Gender(final String gender) {
        this.gender = gender;
    }

    public static Gender getRandom() {
        return NumberUtilities.getRandomValueInArray(values());
    }

    public String getValue() {
        return gender;
    }

    @Override
    public String toString() {
        return gender;
    }

}
