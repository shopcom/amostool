package com.marketamerica.automation.utilities.enums;

public enum PlacementLeg {
    LEFT("Left", "L"), RIGHT("Right", "R"), BOTH("Both", "B");

    private String leg;
    private String legAbbreviation;

	private PlacementLeg(String leg, String legAbbreviation) {
		this.leg = leg;
		this.legAbbreviation = legAbbreviation;
	}

	public static PlacementLeg parse(String leg) {
		PlacementLeg[] values = values();
		for (PlacementLeg value : values) {
			if (value.toString().equals(leg) || value.getLegAbbreviation().equalsIgnoreCase(leg.trim())) {
				return value;
			}
		}
		return null;
	}
	
	public static PlacementLeg parseFromAbbreviation(String legAbbreviation) {
		PlacementLeg[] values = values();
		for (PlacementLeg value : values) {
			if (value.getLegAbbreviation().equals(legAbbreviation)) {
				return value;
			}
		}
		return null;
	}


    @Override
    public String toString() {
        return leg;
    }

    public String getLegAbbreviation() {
        return legAbbreviation;
    }

}
