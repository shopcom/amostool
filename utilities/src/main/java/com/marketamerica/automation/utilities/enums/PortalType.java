package com.marketamerica.automation.utilities.enums;

import java.util.Arrays;
import java.util.List;

/**
 * This enum represents the various portal types
 * Created by kathy gong on 05/21/2015.
 */
public enum PortalType {

    SHOP_COM("shop.com", Arrays.asList("Shop Site", "Shop Portal")), 
    MOTIVES_MINI("motivescosmetics.com", Arrays.asList("Motives Mini")), 
    ISOTONIX_MINI("isotonix.com", Arrays.asList("Isotonix Mini")),  
    CUSTOM_MINI("Custom Mini"),
    NUTRAMETRIX_PORTAL("nutrametrix.com",Arrays.asList("Nutrametrix Portal", "Nutrametrix Site"));

    private String portalType;
    private List<String> otherSameTypes;

    private PortalType(final String portalType, List<String> otherSameTypes) {
        this.portalType = portalType;
        this.otherSameTypes = otherSameTypes;
    }
    
    private PortalType(final String portalType) {
    	this.portalType = portalType;
    }

    /**
     * Resolve a portal type via a string.
     *
     * @param string a string that represents a portal type
     * @return a enum constant representing a portal type. Returns null if no type found.
     */     
    public static PortalType parse(String string) {
    	for (final PortalType currentType : values()) {
    		if (currentType.toString().equalsIgnoreCase(string.trim())) {
                return currentType;
            }
    		final List<String> inOtherTypes = currentType.otherSameTypes;
    		if (inOtherTypes != null) {
    			for (String variation : inOtherTypes) {
                    if (variation.equalsIgnoreCase(string)) {
                        return currentType;
                    }
                }    			
    		}
    	}
    	return null;
    }     

    @Override
    public String toString() {
        return portalType;
    }

}
