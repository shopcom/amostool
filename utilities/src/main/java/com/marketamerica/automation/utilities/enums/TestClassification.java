package com.marketamerica.automation.utilities.enums;

/**
 * Represents how a test should be classified. Current constants are Smoke, Regression, ETE.
 * <p>This is useful when wanting to run specific sets of tests
 * Created by javierv on 11/7/2014.
 */
public enum TestClassification {
    SMOKE("smoke"), REGRESSION("regression"), END_TO_END("ete");

    private String classification;

    private TestClassification(String classification) {
        this.classification = classification;
    }

    /**
     * Attempt to resolve a test classification based on the given string
     *
     * @param string a string that should represent a testing classification
     * @return a constant representing the provided string. Defaults to {@code REGRESSION}
     */
    public static TestClassification parse(String string) {
        TestClassification[] classifications = values();
        if (string == null || string.isEmpty()) {
            return REGRESSION;
        }
        for (TestClassification testClassification : classifications) {
            if (testClassification.toString().equalsIgnoreCase(string)) {
                return testClassification;
            }
        }
        return REGRESSION;
    }

    @Override
    public String toString() {
        return classification;
    }

}
