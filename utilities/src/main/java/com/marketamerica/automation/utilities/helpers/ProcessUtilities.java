package com.marketamerica.automation.utilities.helpers;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.ProcessBuilder.Redirect;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * This class contains com.marketamerica.automation.utilities.helpers to interact with processes.
 *
 * @author javierv
 */
public final class ProcessUtilities {
    protected final static Logger logger = LogManager
            .getLogger(ProcessUtilities.class);
    private static final int MAX_PROCESS_WAIT_TIME = 15;
    private static final int PROCESS_WAIT_TIME = 250;

    /**
     * Wait for a process to terminate
     *
     * @param processName a name used to identify a process
     * @return a boolean indicating if the process may still be running.
     */
    public static boolean waitForProcessToTerminateByName(
            final String processName) {

        int iterationsWaited = 0;
        while (isProcessExistingByName(processName)
                && iterationsWaited < MAX_PROCESS_WAIT_TIME) {
            try {
                logger.debug(String.format("Waiting for %s to terminate",
                        processName));
                Thread.sleep(PROCESS_WAIT_TIME);
            } catch (InterruptedException e) {
                logger.error(e);
            }
            ++iterationsWaited;
        }

        if (iterationsWaited >= MAX_PROCESS_WAIT_TIME) {
            return true;
        } else {
            return false;
        }

    }

    /**
     * This method checks to see if a process exists by name. It has only been
     * tested against windows. Other OS's need to be tested to enhance the
     * current functionality.
     *
     * @param processName a name used to identify a process
     * @return a boolean indicating if the process may still be running.
     */
    public static boolean isProcessExistingByName(String processName) {
        if (!System.getProperty("os.name").toLowerCase().contains("windows")) {
            logger.warn(String
                    .format("On non-windows systems, unable to determine if a process is existing. "
                            + "Will need to enhance functionality of this method. "
                            + "Could not identifify if %s exists", processName));
            return true;
        }
        String line;
        BufferedReader input = null;

        try {
            Process process = Runtime.getRuntime().exec("wmic.exe");
            input = new BufferedReader(new InputStreamReader(
                    process.getInputStream()));
            OutputStreamWriter oStream = new OutputStreamWriter(
                    process.getOutputStream());
            processName = processName.replaceAll(".exe", "");
            oStream.write("process where \"name like '%" + processName + "%'\"");
            oStream.flush();
            oStream.close();
            while ((line = input.readLine()) != null) {
                if (line.trim().contains(processName)
                        && !line.trim().contains("wmic:root\\cli>process")) {
                    return true;
                }
            }
        } catch (IOException e) {
            logger.error(e);
        } finally {
            try {
                if (input != null)
                    input.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }
        return false;
    }

    /**
     * Start a process
     *
     * @param environmentVariables
     * @param workingDirectory
     * @param logFile
     * @param arguements
     */
    public static void startProcess(
            final Map<String, String> environmentVariables,
            final String workingDirectory, final String logFile,
            final String... arguements) {

        final ProcessBuilder processBuilder = new ProcessBuilder(arguements);
        final Map<String, String> env = processBuilder.environment();
        if (environmentVariables != null)
            env.putAll(environmentVariables);
        if (workingDirectory != null)
            processBuilder.directory(new File(workingDirectory));

        File log = null;

        // If we are given a logfile, configure our output to redirect to the
        // error stream
        if (logFile != null) {
            log = ProcessUtilities.configureRedirectToLogFile(processBuilder,
                    logFile);
            // else just redirect output to whatever we are currently doing.
            // this should usually be System.out
        } else {
            processBuilder.redirectOutput(Redirect.INHERIT);
        }

        Process process = null;
        // Start our process
        try {
            process = processBuilder.start();
        } catch (IOException e) {
            logger.error(e);
        }

        // Test that our logfile is working
        if (logFile != null) {
            ProcessUtilities.testRedirect(processBuilder, log);
        }

        try {
            assert process.getInputStream().read() == -1;
        } catch (IOException e) {
            logger.error(e);
        }

    }

    /**
     * Start a process
     *
     * @param workingDirectory
     * @param arguements
     */
    public static void startProcess(final String workingDirectory,
                                    final String... arguements) {
        startProcess(null, workingDirectory, null, arguements);
    }

    /**
     * Start a process
     *
     * @param arguements
     */
    public static void startProcess(final String... arguements) {
        startProcess(null, null, null, arguements);
    }

    private static File configureRedirectToLogFile(
            final ProcessBuilder processBuilder, final String logFile) {
        final File log = new File(logFile);
        processBuilder.redirectErrorStream(true);
        processBuilder.redirectOutput(Redirect.appendTo(log));
        return log;
    }

    private static void testRedirect(final ProcessBuilder processBuilder,
                                     final File log) {
        assert processBuilder.redirectInput() == Redirect.PIPE;
        assert processBuilder.redirectOutput().file() == log;

    }

    public static void main(String[] args) {
        String[] arguments = null;

        if (ProcessUtilities.isProcessExistingByName("chromedriver.exe")) {
            arguments = new String[]{"taskkill", "/f", "/im",
                    "chromedriver.exe"};
            ProcessUtilities.startProcess(arguments);
        } else {
            logger.debug("Chromedriver was not running!");
        }

        if (ProcessUtilities.isProcessExistingByName("IEDriverServer.exe")) {
            arguments = new String[]{"taskkill", "/f", "/im",
                    "IEDriverServer.exe"};
            ProcessUtilities.startProcess(arguments);
        } else {
            logger.debug("IEDriverServer was not running!");
        }

        logger.debug(String.format("Taskkill running? %s", ProcessUtilities
                .waitForProcessToTerminateByName("taskkill.exe")));

        logger.debug(System.getProperty("os.name"));

    }

}
