package com.marketamerica.automation.utilities.database;

import java.sql.Connection;

import com.jcraft.jsch.JSchException;
import com.marketamerica.automation.utilities.helpers.SecureShellConnection;

public class DatabaseConnectionOverSecureShellConnection {
    private DatabaseSession databaseSession;
    private SecureShellConnection ssh;

    public DatabaseConnectionOverSecureShellConnection(
            DatabaseSession databaseSession, SecureShellConnection ssh) {
        this.databaseSession = databaseSession;
        this.ssh = ssh;
    }

    public void connectToDatabase() {
        databaseSession.establishConnection();
    }

    public void connectToSSHServer() {
        ssh.connect();
    }

    public void setPortFowardingL(int localPort, String remoteHost,
                                  int remotePort) throws JSchException {
        ssh.setPortFowardingL(localPort, remoteHost, remotePort);
    }

    public void delPortForwardingL() {
        ssh.delPortForwardingL();
    }

    public Connection getConnection() {
        return databaseSession.getConnection();
    }

    public void disconnect(boolean disconnectPortForwarding) {
        databaseSession.disconnect();
        if (disconnectPortForwarding) {
            delPortForwardingL();
        }
        ssh.disconnect();
    }
}
