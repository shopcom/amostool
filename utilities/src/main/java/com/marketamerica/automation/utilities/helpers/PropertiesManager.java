package com.marketamerica.automation.utilities.helpers;

import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class PropertiesManager {
    final static Logger logger = LogManager
            .getLogger(PropertiesManager.class);


    public static Properties globalSettings() {
        final Properties properties = new Properties();
        try {
            properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("settings.properties"));
        } catch (IOException e) {
            logger.error(e);
        }
        return properties;

    }


}
