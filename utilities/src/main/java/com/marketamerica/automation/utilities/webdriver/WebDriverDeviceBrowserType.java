package com.marketamerica.automation.utilities.webdriver;

import com.marketamerica.automation.Configuration;

/**
 * Enum for all device OS, its browser and device Version
 * <p>
 * Appium currently supports Native Android Browser , Chrome in Android Devices
 * for Devices with OS version 4.2 and up and Safari in iOS Devices for Device
 * versions 6.1, 7.0, 7.1
 *
 * @author archanac
 */
public enum WebDriverDeviceBrowserType {
    ANDROID_PHONE_CHROME_V44("phone", "android", "chrome", "4.4"),
    ANDROID_PHONE_ANDROID_V44("phone", "android", "android", "4.4"), // Android Native Browser
    ANDROID_PHONE_CHROME_V43("phone", "android", "chrome", "4.3"),
    ANDROID_PHONE_ANDROID_V43("phone", "android", "android", "4.3"),
    ANDROID_PHONE_APP_V44("phone", "android", "app", "4.4"),
    ANDROID_PHONE_APP_V43("phone", "android", "app", "4.3"),
    IPHONE_SAFARI_V71("phone", "IOS", "safari", "7.1"),
    IPAD_SAFARI_V71("Tablet", "IOS", "safari", "7.1"),
    IPHONE_SAFARI_V70("phone", "IOS", "safari", "7.0"),
    IPAD_SAFARI_V70("Tablet", "IOS", "safari", "7.0"),
    IPHONE_APP_V71("phone", "IOS", "app", "7.1");

    private String deviceType;
    private String browser;
    private String deviceVersion;
    private String deviceOS;

    private WebDriverDeviceBrowserType(final String deviceType, final String deviceOS,
                                       final String browser, final String deviceVersion) {
        this.deviceType = deviceType;
        this.deviceOS = deviceOS;
        this.browser = browser;
        this.deviceVersion = deviceVersion;

    }

    /**
     * return WebDriverDeviceBrowserType based on deviceOS , browser and
     * Version.
     *
     * @param driverType
     * @return
     */
    public static WebDriverDeviceBrowserType getDeviceBrowserType(
            WebDriverType driverType) {
        String deviceBrowserVersionString = driverType.getDriverDevice().getDevice() + " "
                + driverType.getDriverDevice().getDeviceOS() + " "
                + driverType.getBrowser() + " " + driverType.getVersion();
        WebDriverDeviceBrowserType deviceBrowserType = null;
        for (final WebDriverDeviceBrowserType deviceBrowserType1 : values()) {
            if (deviceBrowserType1.toString().equalsIgnoreCase(
                    deviceBrowserVersionString)) {
                deviceBrowserType = deviceBrowserType1;
                break;
            }
        }
        if (deviceBrowserType == null) {
            throw new IllegalArgumentException(String.format(
                    "Not Supported Currently - device %s %s in browser  %s , version - %s",
                    driverType.getDriverDevice().getDevice(), driverType.getDriverDevice().getDeviceOS(),
                    driverType.getDeviceBrowserType().getBrowser(), driverType.getVersion()));
        } else {
            return deviceBrowserType;
        }
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceOS() {
        return deviceOS;
    }

    public void setDeviceOS(String deviceOS) {
        this.deviceOS = deviceOS;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    // /**
    // * returns true if testing on simulator else returns false.
    // * @return
    // */
    // public Boolean isSimulator(){
    // return this.browser.trim().equalsIgnoreCase("simulator");
    // }

    /**
     * returns true if testing is being performed on app else for browsers -this
     * is returned as false
     *
     * @return
     */
    public Boolean isApplication() {
        if (Configuration.getAppActivity() != null && Configuration.getAppPackage() != null) {
            return true;
        } else {
            return false;
        }
    }

    public String getDeviceVersion() {
        return deviceVersion;
    }

    public void setDeviceVersion(String deviceVersion) {
        this.deviceVersion = deviceVersion;
    }

    @Override
    public String toString() {
        return deviceType + " " + deviceOS + " " + browser + " " + deviceVersion;
    }
}