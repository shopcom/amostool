package com.marketamerica.automation.utilities.enums;

import static com.marketamerica.automation.utilities.enums.Country.CountryClassification.MARKET; 
import static java.util.stream.Collectors.toList;
import static com.marketamerica.automation.utilities.enums.Region.AMERICAS;
import static com.marketamerica.automation.utilities.enums.Region.EUROPE;
import static com.marketamerica.automation.utilities.enums.Region.ASIA_PACIFIC;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

/**
 * Enum for all Countries. 
 */
public enum Country {
	UNITED_STATES("United States", "USA", "US", AMERICAS, false, MARKET),
	AUSTRALIA("Australia", "AUS", "AU", ASIA_PACIFIC, true, MARKET),
    CANADA("Canada", "CAN", "CA" , AMERICAS, true, MARKET),
    HONG_KONG("Hong Kong", "HKG", "HK", ASIA_PACIFIC, true, MARKET, Arrays.asList("香港")),
    SINGAPORE("Singapore", "SGP", "SG", ASIA_PACIFIC, true, MARKET),
    UNITED_KINGDOM("United Kingdom", "United Kingdom","UK", EUROPE, true, MARKET),
    SPAIN("Spain", "ESP", "ES", EUROPE, true, MARKET, Arrays.asList("España")),
    TAIWAN("Taiwan", "TWN", "TW", ASIA_PACIFIC, false, MARKET, Arrays.asList("臺灣")),    
    MEXICO("Mexico", "MEX", AMERICAS, false, MARKET),    
    
    NOT_CLASSIFIED("Not Classified", "Other", Region.OTHER, false, CountryClassification.OTHER),
    EMP("Emerging Markets Program", "EMP", Region.OTHER, false, CountryClassification.EMP),
    COLOMBIA("Colombia", "COL", AMERICAS, false, CountryClassification.EMP),
    BAHAMAS("Bahamas", "BHS", AMERICAS, false, CountryClassification.EMP),
    DOMINICAN_REPUBLIC("Dominican Republic", "DOM", AMERICAS, false, CountryClassification.EMP),
    ECUADOR("Ecuador", "ECU", AMERICAS, false, CountryClassification.EMP),
    JAMAICA("Jamaica", "JAM", AMERICAS, false, CountryClassification.EMP),
    PANAMA("Panama", "PAN", AMERICAS, false, CountryClassification.EMP),
    NEW_ZEALAND("New Zealand", "NZL", ASIA_PACIFIC, false, CountryClassification.EMP),
    
	CHINA("China", "CN", ASIA_PACIFIC, false, CountryClassification.OTHER, Arrays.asList("中国", "中國")),
	MALAYSIA("Malaysia", "MYS", ASIA_PACIFIC, false, CountryClassification.OTHER),
	RUSSIAN_FEDERATION("Russian Federation", "RUS", AMERICAS, false, CountryClassification.OTHER),
	NETHERLANDS("Netherlands", "NLD", EUROPE, false, CountryClassification.OTHER),
	DENMARK("Denmark", "DEN", EUROPE, false, CountryClassification.OTHER),
	JERSEY("Jersey", "JSY", EUROPE, false, CountryClassification.OTHER);

    private final String country;
    private final String abbreviation;
    private final String twoCharactersAbbreviation;
    private final Region region;
    private final boolean personalConsumption;
    private final List<String> otherLanguages;
    private final CountryClassification classification;

	private Country(final String country, final String abbreviation, final Region region,
			final boolean personalConsumption, CountryClassification classification, List<String> otherLanguages) {
		this(country, abbreviation, null, region, personalConsumption, classification, otherLanguages);
	}

	private Country(final String country, final String abbreviation, final String twoCharactersAbbreviation,
			final Region region, final boolean personalConsumption, CountryClassification classification,
			List<String> otherLanguages) {
		this.country = country;
		this.abbreviation = abbreviation;
		this.twoCharactersAbbreviation = twoCharactersAbbreviation;
		this.region = region;
		this.personalConsumption = personalConsumption;
		this.classification = classification;
		this.otherLanguages = otherLanguages;
	}

	private Country(final String country, final String abbreviation, final String twoCharactersAbbreviation,
			final Region region, final boolean personalConsumption, CountryClassification classification) {
		this(country, abbreviation, twoCharactersAbbreviation, region, personalConsumption, classification, null);
	}

    private Country(final String country, final String abbreviation, final Region region,
                    final boolean personalConsumption, CountryClassification classification) {
        this(country, abbreviation, null, region, personalConsumption, classification, null);
    }

    public static List<Country> getPersonalConsumptionCountries() {
        return Stream.of(values()).filter(Country::isPersonalConsumption).collect(toList());
    }

    /**
     * Parse a Country based on the given string
     *
     * @param country a string value presenting a Country constant
     * @return the resolved Country constant or null if no matching Country found
     */
    public static Country parse(String country) {
        for (final Country currentCountry : values()) {
            if (currentCountry.toString().equalsIgnoreCase(country) || currentCountry.getAbbreviation()
                    .equalsIgnoreCase(country)) {
                return currentCountry;
            }
            final List<String> inOtherLanguages = currentCountry.otherLanguages;
            if (inOtherLanguages != null) {
                for (String variation : inOtherLanguages) {
                    if (variation.equalsIgnoreCase(country)) {
                        return currentCountry;
                    }
                }
            }
        }
        return NOT_CLASSIFIED;
    }

    /**
     * Parse a Country based on the given string that represents its abbreviation
     *
     * @param abbreviatedCountry a string value presenting a Country constant
     * @return the resolved Country constant or null if no matching Country found
     */
    public static Country parseAbbreviation(String abbreviatedCountry) {
        for (final Country currentCountry : values()) {
            if (currentCountry.getAbbreviation().equalsIgnoreCase(abbreviatedCountry)) {
                return currentCountry;
            }
        }
        return NOT_CLASSIFIED;
    }

    public CountryClassification getClassification() {
        return classification;
    }

    public Region getRegion() {
        return region;
    }

    @Override
    public String toString() {
        return country;
    }

    /**
     * Return the Country abbreviation. This value is based on Test Link Platforms which aren't always abbreviations :)
     *
     * @return a string value representing the designated Country
     */
    public String getAbbreviation() {
        return this.abbreviation;
    }

    /**
     * @return Indicates if the country is a personal enabled consumption country.
     */
    public boolean isPersonalConsumption() {
        return personalConsumption;
    }

    public enum CountryClassification {
        MARKET, EMP, OTHER
    }

    /*public enum Region {
        ASIA_PACIFIC("Asia Pacific"), AMERICAS("Americas"), EUROPE("Europe"), OTHER("Other");
        private String region;

        private Region(String region) {
            this.region = region;
        }

        @Override
        public String toString() {
            return region;
        }
    }*/

	public String getTwoCharactersAbbreviation() {
		return twoCharactersAbbreviation;
	}

}
