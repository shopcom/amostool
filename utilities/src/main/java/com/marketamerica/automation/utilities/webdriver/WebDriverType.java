package com.marketamerica.automation.utilities.webdriver;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.Platform;

/**
 * Access Modifiers for different test params obtained currently from testng xml file.
 *
 * @author archanac
 */
public class WebDriverType {

    private Platform platform;
    private String version;
    private WebDriverBrowser browser;
    private WebDriverDeviceType deviceType;
    private WebDriverDeviceBrowserType deviceBrowserType;
    private Dimension dimension;
    private WebDriverDevice device;
    private WebDriverUserAgent useragent;

//    /**
//     * @param browser
//     * @param deviceType
//     * @param platform
//     * @param version
//     */
//    public WebDriverType(WebDriverBrowser browser,
//                         WebDriverDeviceType deviceType, Platform platform, String version) {
//        this.browser = browser;
//        this.deviceType = deviceType;
//        this.platform = platform;
//        this.version = version;
//
//
//    }
//
//    /**
//     * @param browser
//     * @param deviceType
//     * @param platform
//     */
//    public WebDriverType(WebDriverBrowser browser,
//                         WebDriverDeviceType deviceType, Platform platform) {
//        this.browser = browser;
//        this.deviceType = deviceType;
//        this.platform = platform;
//    }
//
//    public WebDriverType(WebDriverBrowser browser,
//                         WebDriverDeviceType deviceType, Platform platform, String version, Dimension dimension) {
//        this.browser = browser;
//        this.deviceType = deviceType;
//        this.platform = platform;
//        this.version = version;
//        this.dimension = dimension;
//    }


    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public WebDriverBrowser getBrowser() {
        return browser;
    }

    public void setBrowser(WebDriverBrowser browser) {
        this.browser = browser;
    }

    public WebDriverDeviceType getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(WebDriverDeviceType deviceType) {
        this.deviceType = deviceType;
    }

    public Dimension getDimension() {
        return dimension;
    }

    public void setDimension(Dimension dimension) {
        this.dimension = dimension;
    }

    public WebDriverDeviceBrowserType getDeviceBrowserType() {
        return deviceBrowserType;
    }

    public void setDeviceBrowserType(WebDriverDeviceBrowserType deviceBrowserType) {
        this.deviceBrowserType = deviceBrowserType;
    }

    public WebDriverDevice getDriverDevice() {
        return device;
    }

    public void setDriverDevice(WebDriverDevice device) {
        this.device = device;
    }
    public WebDriverUserAgent getUserAgent() {
        return useragent;
    }

    public void setUserAgent(WebDriverUserAgent useragent) {
        this.useragent = useragent;
    }
    
}
