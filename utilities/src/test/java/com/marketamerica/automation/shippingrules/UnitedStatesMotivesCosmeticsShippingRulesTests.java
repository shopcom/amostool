package com.marketamerica.automation.shippingrules;

import static org.testng.Assert.assertTrue;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.testdata.attributes.ShopperClassification;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.enums.Project;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;


/**
 * Created by javierv on 8/18/2014.
 */
public class UnitedStatesMotivesCosmeticsShippingRulesTests extends ShippingRulesTests {

    @BeforeMethod
    public void setupTest() {
        super.setupTest();
        setProject(Project.MOTIVES_COSMETICS);
        Configuration.setCountry(Country.UNITED_STATES);
        Configuration.initialize(Environment.AUTOMATION_UNIT_TESTING);
    }

    @Test
    public void testMAAPCShipping() {
        Shopper pc = new Shopper();
        pc.setReferralShopperClassification(ShopperClassification.MOTIVES_AFFILIATE_ASSOCIATE);

        assertTrue(pc.getShippingCalculator().calculateShippingForMotivesAssociatesPreferredCustomer(product).equals(CurrencyUtilities.FREE), "Expected Free Shipping");

        product.setRetailCost("32.30");
        assertTrue(pc.getShippingCalculator().calculateShippingForMotivesAssociatesPreferredCustomer(product).equals(CurrencyUtilities.FREE), "Expected free shipping for product with a retail cost under $99");

        product.setRetailCost("5000");
        assertTrue(pc.getShippingCalculator().calculateShippingForMotivesAssociatesPreferredCustomer(product).equals(CurrencyUtilities.FREE), "Expected free shipping for product with a retail cost over $99");

        product.setDistributorCost("5000");
        assertTrue(pc.getShippingCalculator().calculateShippingForMotivesAssociatesPreferredCustomer(product).equals(CurrencyUtilities.FREE), "Expected free shipping for product with a distributor cost");

        product.setWeight(40);
        assertTrue(pc.getShippingCalculator().calculateShippingForMotivesAssociatesPreferredCustomer(product).equals(CurrencyUtilities.FREE), "Expected free shipping for product with a given weight");

    }

}
