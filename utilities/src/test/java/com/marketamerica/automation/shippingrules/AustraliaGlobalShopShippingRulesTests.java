package com.marketamerica.automation.shippingrules;

import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FIVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FREE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TEN_PERCENT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TWELVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.formatBigDecimal;
import static org.testng.Assert.assertEquals;

import java.math.BigDecimal;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.enums.Project;

/**
 * Created by javierv on 8/15/2014.
 */
public class AustraliaGlobalShopShippingRulesTests extends ShippingRulesTests {
    @BeforeMethod
    public void setupTest() {
        super.setupTest();
        Configuration.setCountry(Country.AUSTRALIA);
        Configuration.initialize(Environment.AUTOMATION_UNIT_TESTING);
        setProject(Project.GLOBAL_SHOP);
    }

    @Test
    public void testFreeShippingForAUPCAndAUDistributor() {

        distributor.getAddress().setCountry(Country.AUSTRALIA);
        shopper.getAddress().setCountry(Country.AUSTRALIA);

        product.setRetailCost("5800");
        System.out.println("Project is " + Configuration.getProjectName());
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FREE, "Expected free shipping for retail cost > 99");
        product.setDistributorCost("5800");
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FREE, "Expected free shipping if the product has a defined distributor cost");
        product.setRetailCost("21.00");
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FREE, "Expected free shipping for retail cost < 99");
        product.setWeight(40);
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FREE, "Expected free shipping for product with defined weight");
    }

    @Test
    public void testFreeShippingPortalNonAUDistributor() {
        distributor.getAddress().setCountry(Country.UNITED_STATES);

        shopper.getAddress().setCountry(Country.AUSTRALIA);
        portal.setFreeShipping(true);
        product.setRetailCost("5800");
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FREE, "Expected free shipping for retail cost > 99");
        product.setRetailCost("21.00");
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FIVE, "Expected flat rate shipping for retail cost < 99");
    }

    @Test
    public void testNonFreeShippingPortalNonAUDistributor() {
        distributor.getAddress().setCountry(Country.UNITED_STATES);
        shopper.getAddress().setCountry(Country.AUSTRALIA);
        product.setRetailCost("5800");
        BigDecimal actual;
        BigDecimal expected;
        actual = shopper.getShippingCalculator().calculateShippingCost(product);
        expected = formatBigDecimal(product.getRetailCost().multiply(TEN_PERCENT));
        assertEquals(actual, expected, String.format("Expected: %s, Actual: %s", expected, actual));


        product.setRetailCost("21.00");
        actual = shopper.getShippingCalculator().calculateShippingCost(product);
        expected = TWELVE;
        assertEquals(actual, expected, String.format("Expected: %s, Actual: %s", expected, actual));
    }


}
