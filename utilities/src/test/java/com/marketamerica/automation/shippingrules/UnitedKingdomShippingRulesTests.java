package com.marketamerica.automation.shippingrules;

import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FREE;
import static org.testng.Assert.assertEquals;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.enums.Project;

/**
 * Created by javierv on 9/2/2014.
 */
public class UnitedKingdomShippingRulesTests extends ShippingRulesTests {
    @BeforeMethod
    public void setupTest() {
        super.setupTest();
        Configuration.setCountry(Country.UNITED_KINGDOM);
        Configuration.initialize(Environment.AUTOMATION_UNIT_TESTING);
    }

    @Test(description = "Shipping should always be free")
    public void testShippingOnGlobalShop() {
        setProject(Project.GLOBAL_SHOP);
        product.setRetailCost("5800");
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FREE, "Expected free shipping for retail cost > 99");
        product.setDistributorCost("5800");
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FREE, "Expected free shipping if the product has a defined distributor cost");
        product.setRetailCost("21.00");
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FREE, "Expected free shipping for retail cost < 99");
        product.setWeight(40);
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FREE, "Expected free shipping for product with defined weight");

    }

}
