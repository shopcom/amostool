package com.marketamerica.automation.shippingrules;

import static com.marketamerica.automation.testdata.attributes.ReducedShipping.MAIL_INNOVATION;
import static com.marketamerica.automation.testdata.attributes.ReducedShipping.SURE_POST;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FOUR;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.SIX;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.SIX_PERCENT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TWO;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.formatBigDecimal;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.testdata.Address;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.testdata.attributes.ReducedShipping;
import com.marketamerica.automation.utilities.enums.Country;

/**
 * Created by javierv on 8/20/2014.
 */
public class UnitedStatsReducedShippingRulesTests extends ShopShippingRulesTests {

    @BeforeMethod
    public void setupTest() {
        super.setupTest();
        Configuration.setCountry(Country.UNITED_STATES);
        Address address = new Address();
        address.setResidential(true);
        address.setState("north carolina");
        address.setCountry(Country.UNITED_STATES);
        shopper.setAddress(address);
    }


    @Test
    public void testHasReducedShippingNoShippingLessThan64OuncesLessThan99UPSMI() {
        portal.setFreeShipping(false);
        product.setWeight(5);

        product.setRetailCost("50");
        assertTrue(shopper.hasReducedShipping(product));
    }

    @Test
    public void testDoesNotHaveReducedShippingNoShippingLessThan64OuncesLessThan99UPSMI() {
        portal.setFreeShipping(true);
        product.setWeight(5);
        product.setRetailCost("50");
        assertFalse(shopper.hasReducedShipping(product));
    }

    @Test
    public void testReducedShippingFreeShippingLessThan64OuncesLessThan99UPSMI() {
        portal.setFreeShipping(true);
        product.setWeight(5);

        product.setRetailCost("50");
        final Map<ReducedShipping, BigDecimal> reducedShippingMap = shopper.getShippingCalculator().calculateReducedShipping(product);
        final BigDecimal actual = reducedShippingMap.get(MAIL_INNOVATION);
        assertNull(actual);
    }

    @Test(enabled = false)
    public void testReducedShippingFreeShippingLessThan64OuncesIs99UPSMI() {
        portal.setFreeShipping(true);
        product.setWeight(5);
        Address address = new Address();
        address.setResidential(true);

        product.setRetailCost("99");
        final Map<ReducedShipping, BigDecimal> reducedShippingMap = shopper.getShippingCalculator().calculateReducedShipping(product);
        final BigDecimal actual = reducedShippingMap.get(MAIL_INNOVATION);
        assertEquals(actual, TWO);
    }

    @Test(enabled = false)
    public void testReducedShippingFreeShippingLessThan64OuncesGreaterThan99UPSMI() {
        portal.setFreeShipping(true);
        product.setWeight(5);
        Address address = new Address();
        address.setResidential(true);

        product.setRetailCost("120");

        final Map<ReducedShipping, BigDecimal> reducedShippingMap = shopper.getShippingCalculator().calculateReducedShipping(product);
        final BigDecimal actual = reducedShippingMap.get(MAIL_INNOVATION);
        assertEquals(actual, TWO);
    }

    @Test
    public void testReducedShippingFreeShippingGreaterThan64OuncesUPSMI() {
        portal.setFreeShipping(true);
        product.setWeight(65);
        Address address = new Address();
        address.setResidential(true);

        product.setRetailCost("50");
        final Map<ReducedShipping, BigDecimal> reducedShippingMap = shopper.getShippingCalculator().calculateReducedShipping(product);
        final BigDecimal actual = reducedShippingMap.get(MAIL_INNOVATION);
        assertNull(actual);
    }

    @Test
    public void testReducedShippingNoFreeShippingLessThan64OuncesLessThan99UPSMI() {
        portal.setFreeShipping(false);
        product.setWeight(5);
        Address address = new Address();
        address.setResidential(true);

        product.setRetailCost("50");
        final Map<ReducedShipping, BigDecimal> reducedShippingMap = shopper.getShippingCalculator().calculateReducedShipping(product);
        final BigDecimal actual = reducedShippingMap.get(MAIL_INNOVATION);
        assertNotNull(actual);
    }

    @Test
    public void testReducedShippingNoFreeShippingLessThan64OuncesGreaterThan99UPSMI() {
        portal.setFreeShipping(false);
        product.setWeight(5);
        Address address = new Address();
        address.setResidential(true);
        address.setState("north carolina");


        product.setRetailCost("120");
        final Map<ReducedShipping, BigDecimal> reducedShippingMap = shopper.getShippingCalculator().calculateReducedShipping(product);
        final BigDecimal actual = reducedShippingMap.get(MAIL_INNOVATION);
        assertEquals(actual, FOUR);
    }

    @Test
    public void testReducedShippingNoFreeShippingGreaterThan64OuncesUPSMI() {
        portal.setFreeShipping(false);
        product.setWeight(65);
        Address address = new Address();
        address.setResidential(true);
        address.setState("north carolina");


        product.setRetailCost("50");
        final Map<ReducedShipping, BigDecimal> reducedShippingMap = shopper.getShippingCalculator().calculateReducedShipping(product);
        final BigDecimal actual = reducedShippingMap.get(MAIL_INNOVATION);
        assertNull(actual);
    }

    @Test(enabled = false)
    public void testReducedShippingFreeShippingLessThan144OuncesUPSSP() {
        portal.setFreeShipping(true);
        product.setWeight(5);
        product.setRetailCost("50");
        final Map<ReducedShipping, BigDecimal> reducedShippingMap = shopper.getShippingCalculator().calculateReducedShipping(product);
        final BigDecimal actual = reducedShippingMap.get(SURE_POST);
        assertEquals(actual, FOUR);
    }

    @Test
    public void testReducedShippingFreeShippingGreaterThan144OuncesUPSSP() {
        portal.setFreeShipping(true);
        product.setWeight(155);
        Address address = new Address();
        address.setResidential(true);

        product.setRetailCost("50");
        final Map<ReducedShipping, BigDecimal> reducedShippingMap = shopper.getShippingCalculator().calculateReducedShipping(product);
        final BigDecimal actual = reducedShippingMap.get(SURE_POST);
        assertNull(actual);
    }

    @Test
    public void testReducedShippingNoFreeShippingLessThan144OuncesUPSSPSixMinShipping() {
        portal.setFreeShipping(false);
        product.setWeight(5);
        Address address = new Address();
        address.setResidential(true);

        product.setRetailCost("3");
        final Map<ReducedShipping, BigDecimal> reducedShippingMap = shopper.getShippingCalculator().calculateReducedShipping(product);
        final BigDecimal actual = reducedShippingMap.get(SURE_POST);
        assertEquals(actual, SIX);
    }

    @Test
    public void testReducedShippingNoFreeShippingLessThan144OuncesIs99UPSSPMinShipping() {
        portal.setFreeShipping(false);
        product.setDistributorCost("21.6");
        product.setProductId("13007");
        product.setWeight(.3);
        Collection<Product> products = new ArrayList<>();

        for (int i = 0; i < 25; ++i) {
            products.add(product);
        }

        Address address = new Address();
        address.setResidential(true);

        Product.calculateRetailCost(products);
        final Map<ReducedShipping, BigDecimal> reducedShippingMap = shopper.getShippingCalculator().calculateReducedShipping(products);
        final BigDecimal actual = reducedShippingMap.get(SURE_POST);
        assertEquals(actual, Product.calculateDistributorCost(products).multiply(SIX_PERCENT, new MathContext(4)));
    }

    @Test
    public void testReducedShippingNoFreeShippingLessThan144OuncesLessThan99UPSSPMinShipping() {
        portal.setFreeShipping(false);
        product.setDistributorCost("4");

        Collection<Product> products = new ArrayList<>();

        for (int i = 0; i < 9; ++i) {
            products.add(product);
        }

        Address address = new Address();
        address.setResidential(true);

        product.setRetailCost("80");
        final Map<ReducedShipping, BigDecimal> reducedShippingMap = shopper.getShippingCalculator().calculateReducedShipping(products);
        final BigDecimal actual = reducedShippingMap.get(SURE_POST);
        final BigDecimal expected = Product.calculateDistributorCost(products).compareTo(SIX) <= 0 ? SIX : formatBigDecimal(Product.calculateDistributorCost(products).multiply(SIX_PERCENT));
        assertEquals(actual, expected);
    }

    @Test
    public void testReducedShippingNoFreeShippingGreaterThan144OuncesUPSSP() {
        portal.setFreeShipping(false);
        product.setWeight(166);
        Address address = new Address();
        address.setResidential(true);

        product.setRetailCost("50");
        final Map<ReducedShipping, BigDecimal> reducedShippingMap = shopper.getShippingCalculator().calculateReducedShipping(product);
        final BigDecimal actual = reducedShippingMap.get(SURE_POST);
        assertNull(actual);
    }

    @Test
    public void testReducedShippingUPSSPNonResidential() {
        portal.setFreeShipping(false);
        product.setWeight(5);
        Address address = new Address();
        address.setResidential(false);
        shopper.setAddress(address);
        product.setRetailCost("50");
        final Map<ReducedShipping, BigDecimal> reducedShippingMap = shopper.getShippingCalculator().calculateReducedShipping(product);
        final BigDecimal actual = reducedShippingMap.get(SURE_POST);
        assertNull(actual);
    }

    @Test
    public void testReducedShippingUPSSPAlaska() {
        portal.setFreeShipping(false);
        product.setWeight(5);
        Address address = new Address();
        address.setResidential(true);
        shopper.setAddress(address);
        address.setState("Alaska");
        product.setRetailCost("50");
        final Map<ReducedShipping, BigDecimal> reducedShippingMap = shopper.getShippingCalculator().calculateReducedShipping(product);
        final BigDecimal actual = reducedShippingMap.get(SURE_POST);
        assertNull(actual);
    }


}
