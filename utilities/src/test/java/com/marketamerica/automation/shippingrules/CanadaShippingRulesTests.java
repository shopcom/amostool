package com.marketamerica.automation.shippingrules;

import static org.testng.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.enums.Project;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;

/**
 * Created by Javier L. Velasquez on 8/15/2014.
 */
public class CanadaShippingRulesTests extends ShippingRulesTests {

    @BeforeMethod
    public void setupTest() {
        super.setupTest();

        Configuration.setCountry(Country.CANADA);
        Configuration.initialize(Environment.AUTOMATION_UNIT_TESTING);
        setProject(Project.SHOP);
        distributor.setLocale(Locale.CANADA);
    }

    @Test
    public void testNoFreeShipping() {

    }

    @Test
    public void testFreeShippingPortalCanadaPC() {
        portal.setFreeShipping(true);
        address.setCountry("Canada");

        product.setRetailCost("20");
        distributor.getAddress().setCountry("Canada");

        product.setRetailCost("5800");
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), CurrencyUtilities.TEN_POINT_FIVE, "Expected free shipping for retail cost > 99 when free shipping is enabled");
        //product.setDistributorCost("5800");
        //assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), CurrencyUtilities.TEN_POINT_FIVE, "Expected free shipping if the product has a defined distributor cost when free shipping is enabled");
        product.setRetailCost("21.00");
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), CurrencyUtilities.TEN_POINT_FIVE, "Expected free shipping for retail cost < 99 when free shipping is enabled");
        //product.setWeight(40);
        //assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FREE, "Expected free shipping for product with defined weight when free shipping is enabled");
    }

    @Test
    public void testFreeShippingOfCanadaDistributorCanadaPC() {
        address.setCountry("Canada");
        product.setRetailCost("5800");
        distributor.getAddress().setCountry("Canada");

        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), CurrencyUtilities.TEN_POINT_FIVE, "Expected free shipping for retail cost > 99 with Canada PC and Canada Distributor");
        //Aroduct.setDistributorCost("5800");
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), CurrencyUtilities.TEN_POINT_FIVE, "Expected free shipping if the product has a defined distributor costwith Canada PC and Canada Distributor");
        product.setRetailCost("21.00");
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), CurrencyUtilities.TEN_POINT_FIVE, "Expected free shipping for retail cost < 99 with Canada PC and Canada Distributor");
        //product.setWeight(40);
        //assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FREE, "Expected free shipping for product with defined weight with Canada PC and Canada Distributor");
    }

    @Test
    public void testNoFreeShippingUSDistributorCanadaPC() {
        address.setCountry("Canada");
        product.setRetailCost("5800");
        distributor.getAddress().setCountry("United States");
        product.setRetailCost("5900");

        BigDecimal actual;
        BigDecimal expected;
        //actual = shopper.getShippingCalculator().calculateShippingCost(product);
        //expected = formatBigDecimal(product.getRetailCost().multiply(CurrencyUtilities.NINE_POINT_FIVE_PERCENT));
        //assertEquals(actual, expected, String.format("Expected: %s, Actual: %s", expected, actual));

        // Testing minimum
        product.setRetailCost("5");
        actual = shopper.getShippingCalculator().calculateShippingCost(product);
        expected = CurrencyUtilities.TEN_POINT_FIVE;
        assertEquals(actual, expected, String.format("Expected: %s, Actual: %s", expected, actual));

        // Testing minimum
        product.setRetailCost("0");
        actual = shopper.getShippingCalculator().calculateShippingCost(product);
        expected = CurrencyUtilities.TEN_POINT_FIVE;
        assertEquals(actual, expected, String.format("Expected: %s, Actual: %s", expected, actual));

    }
}
