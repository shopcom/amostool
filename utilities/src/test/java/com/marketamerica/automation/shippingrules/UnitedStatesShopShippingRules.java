package com.marketamerica.automation.shippingrules;

import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FIVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FREE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.SEVEN_POINT_FIVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.formatBigDecimal;
import static org.testng.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.helpers.CurrencyUtilities;

/**
 * Created by javierv on 8/15/2014.
 */
public class UnitedStatesShopShippingRules extends ShopShippingRulesTests {

    @BeforeMethod
    public void setupTest() {
        super.setupTest();
        Configuration.setCountry(Country.UNITED_STATES);
        Configuration.initialize(Environment.AUTOMATION_UNIT_TESTING);
    }

    @Test
    public void testOrderTotalITransactFreeShippingOrderLessThan99() {
        distributor.setITransact(true);
        portal.setFreeShipping(true);
        product.setRetailCost("40.00");

        BigDecimal actual = shopper.getShippingCalculator().calculateShippingCost(product);
        logger.info(String.format("For order total of %s, asserting that %s equals to %s", product.getRetailCost(), actual, FIVE));
        assertTrue(actual.compareTo(FIVE) == 0);

    }

    @Test
    public void testOrderTotalITransactOrderFreeShippingGreaterThan99AndLessThat135() {
        distributor.setITransact(true);
        portal.setFreeShipping(true);
        product.setRetailCost("120.00");
        BigDecimal actual = shopper.getShippingCalculator().calculateShippingCost(product);

        logger.info(String.format("For order total of %s, asserting that %s equals to %s", product.getRetailCost(), actual, FREE));

        assertTrue(actual.compareTo(FREE) == 0);

    }

    @Test
    public void testOrderTotalITransactOrderFreeShippingGreaterThan135() {
        distributor.setITransact(true);
        portal.setFreeShipping(true);

        product.setRetailCost("200.00");
        BigDecimal actual = shopper.getShippingCalculator().calculateShippingCost(product);
        logger.info(String.format("For order total of %s, asserting that %s equals to %s", product.getRetailCost(), actual, FREE));

        assertTrue(actual.compareTo(FREE) == 0);

    }

    @Test
    public void testOrderTotalITransactNoFreeShippingOrderGreater135() {
        portal.setFreeShipping(false);
        BigDecimal handlingCharge = new BigDecimal(".03");
        portal.setShippingCharge(handlingCharge);
        portal.setMinimumShippingCharge(FREE);
        distributor.setITransact(true);
        product.setRetailCost("399.50");

        BigDecimal actual = shopper.getShippingCalculator().calculateShippingCost(product);
        BigDecimal expected = formatBigDecimal(product.getRetailCost().multiply(handlingCharge));
        logger.info(String.format("For order total of %s, asserting that %s equals to %s", product.getRetailCost(), expected, actual));
        assertTrue(actual.compareTo(expected) == 0);
    }

    @Test
    public void testOrderTotalITransactNoFreeShippingOrderLessThan99() {
        portal.setFreeShipping(false);
        BigDecimal handlingCharge = new BigDecimal("3");
        portal.setShippingCharge(handlingCharge);
        portal.setMinimumShippingCharge(FREE);
        distributor.setITransact(true);

        product.setRetailCost("80.00");

        BigDecimal actual = shopper.getShippingCalculator().calculateShippingCost(product);
        BigDecimal expected = product.getRetailCost().multiply(handlingCharge);
        logger.info(String.format("For order total of %s, asserting that %s equals to %s", product.getRetailCost(), expected, actual));


        assertTrue(actual.compareTo(expected) == 0);

    }

    @Test
    public void testOrderTotalITransactNoFreeShippingOrderGreater135LessThan99() {
        portal.setFreeShipping(false);
        BigDecimal handlingCharge = new BigDecimal("3");
        portal.setShippingCharge(handlingCharge);
        portal.setMinimumShippingCharge(FREE);
        distributor.setITransact(true);
        product.setRetailCost("98");

        BigDecimal actual = shopper.getShippingCalculator().calculateShippingCost(product);
        BigDecimal expected = product.getRetailCost().multiply(handlingCharge);
        logger.info(String.format("For order total of %s, asserting that %s equals to %s", product.getRetailCost(), expected, actual));
        assertTrue(actual.compareTo(expected) == 0);
    }

    @Test
    public void testOrderTotalNotITransactNoFreeShippingOrderLessThan99() {
        portal.setFreeShipping(false);
        BigDecimal handlingCharge = new BigDecimal("3");
        portal.setShippingCharge(handlingCharge);
        portal.setMinimumShippingCharge(FREE);
        distributor.setITransact(false);

        Configuration.setCountry(Country.UNITED_STATES);
        Configuration.initialize(Environment.AUTOMATION_UNIT_TESTING);

        product.setRetailCost("98.00");

        BigDecimal actual = shopper.getShippingCalculator().calculateShippingCost(product);

        logger.info(String.format("For order total of %s, asserting that %s equals to %s", product.getRetailCost(), actual, SEVEN_POINT_FIVE));

        assertTrue(actual.compareTo(SEVEN_POINT_FIVE) == 0);
    }

    @Test
    public void testOrderTotalNotITransactNoFreeShippingOrderGreaterThan99() {
        portal.setFreeShipping(false);
        BigDecimal handlingCharge = new BigDecimal("3");
        BigDecimal distributorCost = new BigDecimal("48");
        portal.setShippingCharge(handlingCharge);
        portal.setMinimumShippingCharge(FREE);
        product.setDistributorCost(distributorCost);
        distributor.setITransact(false);

        product.setRetailCost("100.00");

        BigDecimal actual = shopper.getShippingCalculator().calculateShippingCost(product);
        //BigDecimal expected = product.getDistributorCost().multiply(SEVEN_POINT_FIVE_PERCENT);
        BigDecimal expected = CurrencyUtilities.SEVEN_POINT_FIVE;
        logger.info(String.format("For order total of %s, asserting that %s equals to %s", product.getRetailCost(), expected, actual));

        assertTrue(expected.compareTo(actual) == 0);
    }

    @Test
    public void testOrderTotalNotITransactFreeShippingOrderGreaterThan135() {
        portal.setFreeShipping(true);
        BigDecimal handlingCharge = new BigDecimal("3");
        BigDecimal distributorCost = new BigDecimal("48");
        portal.setShippingCharge(handlingCharge);
        portal.setMinimumShippingCharge(FREE);
        product.setDistributorCost(distributorCost);
        distributor.setITransact(false);

        product.setRetailCost("500.00");

        BigDecimal actual = shopper.getShippingCalculator().calculateShippingCost(product);
        logger.info(String.format("For order total of %s, asserting that %s equals to %s", product.getRetailCost(), actual, FREE));

        assertTrue(actual.compareTo(FREE) == 0);
    }

    @Test
    public void testOrderTotalNotITransactFreeShippingOrderLessThan99() {
        portal.setFreeShipping(true);
        BigDecimal handlingCharge = new BigDecimal("3");
        BigDecimal distributorCost = new BigDecimal("48");
        portal.setShippingCharge(handlingCharge);
        portal.setMinimumShippingCharge(FREE);
        product.setDistributorCost(distributorCost);
        distributor.setITransact(false);

        product.setRetailCost("98.00");

        BigDecimal actual = shopper.getShippingCalculator().calculateShippingCost(product);
        logger.info(String.format("For order total of %s, asserting that %s equals to %s", product.getRetailCost(), actual, FIVE));

        assertTrue(actual.compareTo(FIVE) == 0);
    }

    @Test
    public void test5MLMEx10NoFreeShippingITransactOrderIs411_49() {
        portal.setFreeShipping(false);
        BigDecimal handlingCharge = new BigDecimal(".03");
        portal.setShippingCharge(handlingCharge);
        portal.setMinimumShippingCharge(FIVE);
        distributor.setITransact(true);
        product.setSku("5MLME");
        product.setDistributorCost("29.50");

        Product product2 = new Product();

        product.setRetailCost("199.75");
        product2.setRetailCost("199.75");

        List<Product> products = Arrays.asList(product, product2);

        BigDecimal actual = shopper.getShippingCalculator().calculateShippingCost(products);
        BigDecimal expected = formatBigDecimal(Product.calculateRetailCost(products).multiply(handlingCharge));
        logger.info(String.format("For order total of %s, asserting that %s equals to %s", Product.calculateRetailCost(products), expected, actual));

        assertTrue(actual.compareTo(expected) == 0, String.format("Expected %s, Actual: %s", expected, actual));
    }


}
