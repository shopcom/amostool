package com.marketamerica.automation.shippingrules;

import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FREE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.ONE_HUNDRED_TWENTY_FIVE;
import static org.testng.Assert.assertEquals;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;

/**
 * Created by javierv on 8/15/2014.
 */
public class TaiwanShopShippingRulesTests extends ShopShippingRulesTests {

    @BeforeMethod
    public void setupTest() {
        super.setupTest();
        Configuration.setCountry(Country.TAIWAN);
        Configuration.initialize(Environment.AUTOMATION_UNIT_TESTING);
    }


    @Test
    public void testOrderSubTotalGreaterThan5600() {
        Product product = new Product();
        product.setRetailCost("5800");
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FREE);
    }

    @Test
    public void testOrderSubTotalLessThan5600() {
        Product product = new Product();
        product.setRetailCost("550");

        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), ONE_HUNDRED_TWENTY_FIVE);
    }
}
