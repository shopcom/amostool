package com.marketamerica.automation.shippingrules;

import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FIFTEEN;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FIVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FREE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.NINE_HUNDRED;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.NINE_PERCENT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.ONE_THOUSAND;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.SEVEN_POINT_FIVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.SEVEN_POINT_FIVE_PERCENT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TEN_PERCENT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TWELVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TWELVE_PERCENT;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TWENTY_FIVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.formatBigDecimal;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNotEquals;
import static org.testng.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.testdata.Address;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.testdata.attributes.ShopperClassification;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.enums.Project;


/**
 * Created by javierv on 8/18/2014.
 */
public class GlobalShopShippingRulesTests extends ShippingRulesTests {

    @BeforeMethod
    public void setupTest() {
        super.setupTest();
        setProject(Project.GLOBAL_SHOP);
        Configuration.setCountry(Country.UNITED_STATES);
        Configuration.initialize(Environment.AUTOMATION_UNIT_TESTING);
    }

    @Test
    public void testMAAShipping() {
        Shopper pc = new Shopper();
        pc.setClassification(ShopperClassification.MOTIVES_AFFILIATE_ASSOCIATE);
        assertTrue(pc.getShippingCalculator().calculateShippingForGlobalShop(product).equals(FREE), "Expected Free Shipping");
    }

    @Test
    public void testImplicitFreeShipping() {
        shopper.getAddress().setCountry(Country.CANADA);
        distributor.getAddress().setCountry(Country.CANADA);
        Address shipToDestination = address;
        shipToDestination.setCountry(Country.CANADA);
        shopper.setClassification(ShopperClassification.PREFERRED_CUSTOMER);
        assertFalse(shopper.getShippingCalculator().calculateShippingForGlobalShop(product).equals(FREE), "Expected Free Shipping");
    }

    @Test
    public void testExplicitFreeShipping() {
        shopper.getAddress().setCountry(Country.CANADA);
        distributor.getAddress().setCountry(Country.CANADA);
        Address shipToDestination = address;
        shipToDestination.setCountry(Country.CANADA);
        shopper.setClassification(ShopperClassification.PREFERRED_CUSTOMER);
        assertFalse(shopper.getShippingCalculator().calculateShippingForGlobalShop(product, shopper.getAddress()).equals(FREE), "Expected Free Shipping");
    }

    @Test
    public void testSpanishShipping() {
        shopper.getAddress().setCountry(Country.SPAIN);
        distributor.getAddress().setCountry(Country.UNITED_STATES);
        BigDecimal actual;
        BigDecimal expected;

        /*
        Expected shipping to equal 8
         */
        actual = shopper.getShippingCalculator().calculateShippingForGlobalShop(product);
        expected = FIVE;
        assertTrue(actual.equals(expected), String.format("Expected: %s, Actual: %s", expected, actual));

        /*
        If the distributor and shoper share the same country then shipping is not free since Spain is not a personal consumption country
         */
        distributor.getAddress().setCountry(Country.SPAIN);
        actual = shopper.getShippingCalculator().calculateShippingForGlobalShop(product);
        expected = FREE;
        assertNotEquals(actual.equals(expected), String.format("Expected: %s, Actual: %s", expected, actual));

        /*
        If the distributor/shopper/shipping destination do not
        share the same shipping destination then expect shipping to US Standard Rate(s)
         */
        distributor.getAddress().setCountry(Country.SPAIN);
        Address shipToDestination = address;
        shipToDestination.setCountry(Country.UNITED_STATES);
        product.setDistributorCost("5000");
        actual = shopper.getShippingCalculator().calculateShippingForGlobalShop(product, shipToDestination);
        expected = formatBigDecimal(product.getDistributorCost().multiply(SEVEN_POINT_FIVE_PERCENT));
        assertTrue(actual.equals(expected), String.format("Expected: %s, Actual: %s", expected, actual));
    }

    @Test
    public void testEMPNonMarketCountry() {
        shopper.getAddress().setCountry(Country.SPAIN);
        distributor.getAddress().setCountry(Country.UNITED_STATES);
        BigDecimal actual;
        BigDecimal expected;

        /*
        If the distributor/shopper/shipping destination do not
        share the same shipping destination then expect shipping to US Standard Rate(s)
         */
        distributor.getAddress().setCountry(Country.SPAIN);
        Address shipToDestination = address;
        shipToDestination.setCountry("Tonga");
        product.setRetailCost("5000");
        actual = shopper.getShippingCalculator().calculateShippingForGlobalShop(product, shipToDestination);
        expected = formatBigDecimal(product.getRetailCost().multiply(NINE_PERCENT));
        assertTrue(actual.equals(expected), String.format("Expected: %s, Actual: %s", expected, actual));
    }

    @Test
    public void testUnitedStatesShippingNoITransact() {
        shopper.getAddress().setCountry(Country.UNITED_STATES);
        distributor.getAddress().setCountry(Country.SINGAPORE);
        product.setDistributorCost("5000");
        BigDecimal actual;
        BigDecimal expected;
        actual = shopper.getShippingCalculator().calculateShippingForGlobalShop(product, shopper.getAddress());
        expected = formatBigDecimal(product.getDistributorCost().multiply(SEVEN_POINT_FIVE_PERCENT));
        assertTrue(actual.equals(expected), String.format("Expected: %s, Actual: %s", expected, actual));

        product.setDistributorCost("2");
        actual = shopper.getShippingCalculator().calculateShippingForGlobalShop(product, shopper.getAddress());
        expected = SEVEN_POINT_FIVE;
        assertTrue(actual.equals(expected), String.format("Expected: %s, Actual: %s", expected, actual));
    }

    @Test
    public void testOrderLimits() {
        Configuration.setCountry(Country.UNITED_STATES);
        assertTrue(shopper.getShippingCalculator().hasMaximumOrderTotal());
        assertTrue(shopper.getShippingCalculator().hasMinimumOrderTotal());
        assertTrue(shopper.getShippingCalculator().getMaximumOrderTotal().compareTo(ONE_THOUSAND) == 0);
        assertTrue(shopper.getShippingCalculator().getMinimumOrderTotal().compareTo(TWENTY_FIVE) == 0);
        Configuration.setCountry(Country.UNITED_KINGDOM);
        assertFalse(shopper.getShippingCalculator().hasMaximumOrderTotal());
        assertTrue(shopper.getShippingCalculator().hasMinimumOrderTotal());
        assertTrue(shopper.getShippingCalculator().getMinimumOrderTotal().compareTo(TWELVE) == 0);

        Configuration.setCountry(Country.SPAIN);
        assertFalse(shopper.getShippingCalculator().hasMaximumOrderTotal());
        assertTrue(shopper.getShippingCalculator().hasMinimumOrderTotal());
        assertTrue(shopper.getShippingCalculator().getMinimumOrderTotal().compareTo(FIFTEEN) == 0);

        Configuration.setCountry(Country.CHINA);
        assertTrue(shopper.getShippingCalculator().hasMaximumOrderTotal());
        assertFalse(shopper.getShippingCalculator().hasMinimumOrderTotal());
        assertTrue(shopper.getShippingCalculator().getMaximumOrderTotal().compareTo(NINE_HUNDRED) == 0);
    }

    @Test
    public void testDistributorShippingCosts() {
        product.setDistributorCost("5743");
        product.setRetailCost("13254");
        BigDecimal actual;
        BigDecimal expected;

        // Expect for shipping cost to be 12% of the distributor cost in Personal Consumption Countries
        final List<Country> personalConsumptionCountries = Country.getPersonalConsumptionCountries();
        for (Country country : personalConsumptionCountries) {
            distributor.getAddress().setCountry(country);
            actual = distributor.getShippingCalculator().calculateShippingCost(product);
            expected = formatBigDecimal(product.getDistributorCost().multiply(TWELVE_PERCENT));
            assertEquals(actual, expected);
        }

        // Expect for 12 dollar flat rate if 10% of retail is < 12 in Personal Consumption Countries
        product.setDistributorCost("3");
        actual = distributor.getShippingCalculator().calculateShippingCost(product);
        expected = TWELVE;
        assertEquals(actual, expected);

        /*
        Expected shipping to equal 8 if the distributor is from Spain
         */
        distributor.getAddress().setCountry(Country.SPAIN);
        actual = distributor.getShippingCalculator().calculateShippingCost(product);
        expected = TWELVE;
        assertEquals(actual, expected, String.format("Expected: %s, Actual: %s", expected, actual));

        /*
        Expected for the shipping rate to be 10% of the retail cost in EMP/Non Market Countries
         */
        distributor.getAddress().setCountry("Tonga");
        product.setRetailCost("5000");
        actual = distributor.getShippingCalculator().calculateShippingCost(product);
        expected = formatBigDecimal(product.getRetailCost().multiply(TEN_PERCENT));
        assertEquals(actual, expected, String.format("Expected: %s, Actual: %s", expected, actual));

        product.setRetailCost("3");
        actual = distributor.getShippingCalculator().calculateShippingCost(product);
        expected = TWELVE;
        assertEquals(actual, expected, String.format("Expected: %s, Actual: %s", expected, actual));

    }

}