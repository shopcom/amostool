package com.marketamerica.automation.shippingrules;

import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FIVE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FREE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.NINETY_NINE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.ONE_HUNDRED_THIRTY;
import static org.testng.Assert.assertEquals;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.enums.Project;

/**
 * Created by javierv on 8/19/2014.
 */
public class MiniSiteShippingRulesTest extends ShippingRulesTests {

    @BeforeMethod
    public void setupTest() {
        super.setupTest();
        setProject(Project.MINI);
        Configuration.setCountry(Country.UNITED_STATES);
        Configuration.initialize(Environment.AUTOMATION_UNIT_TESTING);
    }

    @Test(dataProvider = "countries")
    public void testOrderIs99(Country country) {
        Configuration.setCountry(country);
        product.setRetailCost(NINETY_NINE);
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FREE);
    }

    @Test(dataProvider = "countries")
    public void testOrderGreaterThan99(Country country) {
        Configuration.setCountry(country);
        product.setRetailCost(ONE_HUNDRED_THIRTY);
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FREE);
    }

    @Test(dataProvider = "countries")
    public void testOrderLessThan99(Country country) {
        Configuration.setCountry(country);
        product.setRetailCost(FIVE);
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FIVE);
    }

}
