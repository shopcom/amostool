package com.marketamerica.automation.shippingrules;

import org.testng.annotations.BeforeMethod;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.enums.Project;


/**
 * Created by javierv on 8/14/2014.
 */
public class ShopShippingRulesTests extends ShippingRulesTests {

    @BeforeMethod
    public void setupTest() {
        super.setupTest();
        Configuration.initialize(Environment.AUTOMATION_UNIT_TESTING);
        setProject(Project.SHOP);
    }

}
