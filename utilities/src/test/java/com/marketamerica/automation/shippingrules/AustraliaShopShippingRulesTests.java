package com.marketamerica.automation.shippingrules;

import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.FREE;
import static com.marketamerica.automation.utilities.helpers.CurrencyUtilities.TEN;
import static org.testng.Assert.assertEquals;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.enums.Project;

/**
 * Created by Javier L. Velasquez on 8/15/2014.
 */
public class AustraliaShopShippingRulesTests extends ShippingRulesTests {
    @BeforeMethod
    public void setupTest() {
        super.setupTest();
        Configuration.setCountry(Country.AUSTRALIA);
        Configuration.initialize(Environment.AUTOMATION_UNIT_TESTING);
        setProject(Project.SHOP);

    }

    @Test
    public void testOrderSubTotalLessThan99DollarsFreeShipping() {
        Product product = new Product();
        product.setRetailCost("149.00");
        portal.setFreeShipping(true);
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FREE);
    }

    @Test
    public void testOrderSubTotalLessThan99DollarsNoFreeShipping() {
        portal.setFreeShipping(false);

        Product product = new Product();
        product.setRetailCost("98.00");

        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), TEN);
    }


    @Test
    public void testOrderSubTotalGreaterThan99DollarsNoFreeShipping() {
        portal.setFreeShipping(false);

        Product product = new Product();
        product.setRetailCost("149.00");
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), TEN);
    }


    @Test
    public void testOrderSubTotalGreaterThan99DollarsITransactFreeShipping() {
        Product product = new Product();
        product.setRetailCost("120.00");
        portal.setFreeShipping(true);
        assertEquals(shopper.getShippingCalculator().calculateShippingCost(product), FREE);
    }

}
