package com.marketamerica.automation.shippingrules;

import java.io.File;
import java.io.FileNotFoundException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.testdata.Address;
import com.marketamerica.automation.testdata.Distributor;
import com.marketamerica.automation.testdata.Portal;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.utilities.PropertyReader;
import com.marketamerica.automation.utilities.PropertyWriter;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.enums.Project;
import com.marketamerica.automation.utilities.helpers.ReflectiveUtilities;

/**
 * Created by javierv on 8/19/2014.
 */
public class ShippingRulesTests {
    final static Logger logger = LogManager
            .getLogger(ShopShippingRulesTests.class);
    protected Distributor distributor;
    protected Portal portal;
    protected Product product;
    protected Shopper shopper;
    protected Address address;

    @DataProvider(name = "countries")
    public Country[][] getCountriesDataProvider() {

        final Country[] countries = Country.values();
        final Country[][] dataDrivenCountries = new Country[countries.length][1];

        for (int i = 0; i < countries.length; i++) {
            dataDrivenCountries[i][0] = countries[i];
        }

        return dataDrivenCountries;
    }

    @BeforeMethod(alwaysRun = true)
    public void setupTest() {
        shopper = new Shopper();
        product = new Product();
        portal = new Portal();
        shopper.setDistributor(distributor);
        shopper.setPortal(portal);
        product.setFreight(true);
        address = new Address();
        shopper.setAddress(address);
        distributor = new Distributor();
        distributor.setAddress(new Address());
        shopper.setDistributor(distributor);

    }

    private String getConfigurationPath() {
        return String.valueOf(
                ReflectiveUtilities.invokeMethod(
                        Configuration.class,
                        "getConfigurationLocation",
                        Environment.AUTOMATION_UNIT_TESTING
                )
        );
    }

    protected void setProject(final Project project) {
        File configurationPath = new File(getClass().getResource(getConfigurationPath()).getPath());
        PropertyWriter property = new PropertyWriter(configurationPath);
        property.set(Configuration.PROJECT_NAME, project);
        try {
            PropertyReader propertyReader = new PropertyReader(configurationPath);
            ReflectiveUtilities.setField(Configuration.class, "environmentConfiguration", propertyReader);
        } catch (FileNotFoundException e) {
            logger.error(e);
        }

    }

}
