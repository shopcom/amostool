package com.marketamerica.automation.testdata;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.testng.annotations.Test;

/**
 * Created by javierv on 8/15/2014.
 */
public class ProductTests {

    @Test
    public void testCalculateMultipleDistributorCost() {
        BigDecimal one = new BigDecimal("1");
        BigDecimal two = new BigDecimal("2");
        Product productOne = new Product();
        Product productTwo = new Product();
        productOne.setDistributorCost(one);
        productOne.setFreight(true);
        productTwo.setDistributorCost(two);
        productTwo.setFreight(true);

        assertEquals(Product.calculateDistributorCost(Arrays.asList(productOne, productTwo)), new BigDecimal("3"));

    }

    @Test
    public void testCalculateTotalWeight() {
        Product productOne = new Product();
        Product productTwo = new Product();

        productOne.setWeight(2);
        productTwo.setWeight(3);

        assertEquals(Product.calculateTotalWeight(Arrays.asList(productOne, productTwo)), (double) 5);

    }

    @Test
    public void testGroupProductByStore() {
        Store maStore = new Store();
        Store unbeatableSaleStore = new Store();
        Product maProduct = new Product();
        Product morProduct = new Product();

        maStore.setName("Market America");

        unbeatableSaleStore.setName("UnBeatableSale");

        maProduct.setName("MA Product");
        morProduct.setName("Unbeatable Product");
        morProduct.setStore(unbeatableSaleStore);
        maProduct.setStore(maStore);

        List<Product> products = Arrays.asList(maProduct, morProduct);

        Map<Store, List<Product>> productsByStore = Product.getProductsByStore(products);
        Set<Store> stores = productsByStore.keySet();


        assertTrue(productsByStore.get(maStore).get(0).getName().equals("MA Product"));
        assertTrue(productsByStore.get(unbeatableSaleStore).get(0).getName().equals("Unbeatable Product"));

    }

    @Test
    public void testIsMAProduct() {
        Product product = new Product();
        assertTrue(product.isMarketAmericaProduct());
        product.setStore(Store.getMarketAmerica());
        assertTrue(product.isMarketAmericaProduct());
    }

}
