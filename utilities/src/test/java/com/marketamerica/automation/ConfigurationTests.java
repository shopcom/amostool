package com.marketamerica.automation;

import static org.testng.Assert.assertEquals;

import java.util.Locale;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.enums.Project;

/**
 * Created by Javier L. Velasquez on 9/25/2014.
 */
public class ConfigurationTests {

    @BeforeMethod
    public void setupTest() {
        Configuration.initialize(Environment.STAGING);
    }

    @Test(description = "Expected default locale to be United States - English")
    public void testDefaultLocale() {
        Locale actual = Locale.getDefault();
        Locale expected = Locale.US;
        assertEquals(actual, expected, String.format("Expected: %s, Actual: %s", expected, actual));
    }

    @Test(description = "Test ability to get a project name")
    public void testProjectName() {
        assertEquals(Configuration.getProject(), Project.AUTOMATION_UTILITIES);
    }

}
