package com.marketamerica.automation.utilities.helpers.tools;

import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertTrue;

import java.lang.reflect.Method;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import br.eti.kinoshita.testlinkjavaapi.model.TestCase;
import br.eti.kinoshita.testlinkjavaapi.model.TestSuite;

import com.marketamerica.automation.annotations.TestDoc;
import com.marketamerica.automation.testdata.TestLinkID;
import com.marketamerica.automation.utilities.TestLinkManager;
import com.marketamerica.automation.utilities.helpers.ReflectiveUtilities;

/**
 * Holds Test Cases for TestLinkManager
 * 
 * @author javierv
 * @see com.marketamerica.automation.utilities.TestLinkManager
 */
public class TestLinkManagerTests {
    final static Logger logger = LogManager.getLogger(TestLinkManagerTests.class);
    private static final String project = "ASQA - Automated Site Quality Assurance Checks";
    private TestLinkManager testLinkManager;

    @BeforeMethod
    public void beforeMethod() {
        testLinkManager = new TestLinkManager(project);

    }

    @TestDoc(ids = {"ASQA-992"})
    @Test(description = "If this test passes, we can be assured that the majority of the "
            + "TestLinkManager's functionality is working. This test tests "
            + "the ability to resolve project, build, and test plan info. "
            + "It also allows us to check the ability to resolve test "
            + "methods via relfection of the TestDocumentation annotation")
    public void verifyTestLinkRESTTestRetrieval(Method method) {
        final long startTime = System.nanoTime();
        final TestCase testCase = testLinkManager.getTestCaseByExternalId(method);
        final long endTime = System.nanoTime();
        logger.info(((double) (endTime - startTime) / 1000000000.0));

        assertNotNull(testCase.getName(), String.format(
                "Expected for (%s) test case to contain name",
                testCase.getFullExternalId()));
    }

    @TestDoc(ids = {"ASQA-992"})
    @Test
    public void getTestCaseTestSuite(Method method) {
        final TestLinkID testId = ReflectiveUtilities.getTestResultID(method);
        final TestSuite testSuite = testLinkManager.getTestSuite(testId);
        assertTrue(testSuite.getDetails().contains("test suite details"));
    }

}
