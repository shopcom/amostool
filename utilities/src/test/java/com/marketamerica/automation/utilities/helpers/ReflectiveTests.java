package com.marketamerica.automation.utilities.helpers;

import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.getFullMethodName;
import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.getOutstandingJiraDefects;
import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.getTestMethodInCurrentThread;
import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.hasFieldThatContainsValue;
import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.hasTestAnnotation;
import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.hasTestDocumentationAnnotation;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.Test;

import com.marketamerica.automation.annotations.TestDoc;
import com.marketamerica.automation.testdata.Shopper;


/**
 * Created by javierv on 8/11/2014.
 */
public class ReflectiveTests {
    private static final Logger logger = LogManager
            .getLogger(ReflectiveTests.class);

    @Test(
            description = "Test that we can properly resolve test method in current thread via reflection",
            dependsOnMethods = "testIsTest"
    )
    public void testGetTestMethodInCurrentThread(Method expectedMethod) {
        final Method actualMethod = getTestMethodInCurrentThread();
        assertEquals(actualMethod, expectedMethod);
    }

    @Test(description = "Test that \"getTestMethodInCurrentThread\" " +
            "takes less than 100 milliseconds",
            dependsOnMethods = "testGetTestMethodInCurrentThread")
    public void testGetTestMethodInCurrentThreadSpeed() {
        final long startingTime = System.currentTimeMillis();
        getTestMethodInCurrentThread();
        final long endingTime = System.currentTimeMillis();
        final long totalTime = endingTime - startingTime;
        logger.info(String.format("Took %d milliseconds to get the Test's Method", totalTime));
        assertTrue(totalTime < 100);
    }

    @Test(description = "Test that we can find the Test annotation on a test method")
    public void testIsTest(Method method) {
        assertTrue(hasTestAnnotation(method));
    }

    @Test(description = "Test that we can find the TestDoc annotation in a test method")
    @TestDoc
    public void testHasTestDocumentation(Method method) {
        assertTrue(hasTestDocumentationAnnotation(method));
    }

    @Test(description = "Test that we don't find TestDoc annotation on a method that does not have one")
    public void testDoesNotHaveTestDocumentation(Method method) {
        assertFalse(hasTestDocumentationAnnotation(method));
    }

    @Test(description = "Test we can get a full method name")
    public void testCanGetFullName(Method method) {
        assertEquals("com.marketamerica.automation.utilities.helpers.ReflectiveTests.testCanGetFullName", getFullMethodName(method));
    }

    @Test
    @TestDoc(oustandingJiraDefects = {"MIS-70663"})
    public void testHasJiraDefect(Method method) {
        List<String> jiraDefects = getOutstandingJiraDefects(method);
        List<String> defect = new ArrayList<String>();
        defect.add("MIS-70663");
        assertEquals(jiraDefects, defect);
    }

    @Test
    public void testDoesNotHaveJiraDefect(Method method) {
        List<String> jiraDefects = getOutstandingJiraDefects(method);
        assertEquals(jiraDefects, new ArrayList<String>());
    }

    @Test
    public void testHasField() {
        ReflectiveTests reflective = new ReflectiveTests();
        assertTrue(hasFieldThatContainsValue(reflective, "logger"));
    }

    @Test
    public void testHasInheritedField() {
        Shopper shopper = new Shopper();
        shopper.setId("test");

        ReflectiveUtilities.setField(shopper, "addressKey", "test");
        assertTrue(hasFieldThatContainsValue(shopper, "addressKey"));
        assertTrue(hasFieldThatContainsValue(shopper, "id"));
    }


//    @Test
//    public void testCreateObjectClassArray() {
//        ReflectiveUtilities reflective = (ReflectiveUtilities) instantiateObject(class);
//
//        Class<?>[] expected = new Class<?>[]{class};
//        Object[] objects = new Object[]{reflective};
//
//        Class<?>[] actual = (Class<?>[]) invokeMethod(reflective, "createObjectClassArray", objects);
//        assertEquals(expected, actual);
//
//    }

}
