package com.marketamerica.automation.utilities.database;

import static org.testng.Assert.assertTrue;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.helpers.DateAndTimeUtilities;

/**
 * Created by javierv on 11/26/2014.
 */
public class ShopDataBaseTests {
    private ShopDatabase shopDatabase;

    @Test
    public void testGetETransaction() {
        Environment.setCurrent(Environment.LIVE);
        shopDatabase = new ShopDatabase();
        Date endingDate = DateAndTimeUtilities.getFormattedDate("11-27-2014", new SimpleDateFormat
                ("MM-dd-yyyy"));
        Calendar endingCalendar = new GregorianCalendar();
        endingCalendar.setTime(endingDate);

        Date startingDate = DateAndTimeUtilities.getFormattedDate("11-24-2014", new SimpleDateFormat
                ("MM-dd-yyyy"));
        Calendar startingCalendar = new GregorianCalendar();
        startingCalendar.setTime(startingDate);


        shopDatabase.setEndingCalendar(endingCalendar);
        shopDatabase.setStartingCalendar(startingCalendar);

        Map<String, List<String>> eTransaction = shopDatabase.getETransaction("7321242");

        assertTrue(eTransaction.containsKey("PostData"));
        assertTrue(eTransaction.get("PostData").get(0).contains("7321242"));

        Environment.setCurrent(Environment.STAGING);
        shopDatabase = new ShopDatabase();
        shopDatabase.setEndingCalendar(endingCalendar);
        shopDatabase.setStartingCalendar(startingCalendar);

        eTransaction = shopDatabase.getETransaction("3949913");
        assertTrue(eTransaction.containsKey("PostData"));
        assertTrue(eTransaction.get("PostData").get(0).contains("3949913"));

    }

    @AfterMethod
    public void disconnect() {
        shopDatabase.disconnect();
    }
}
