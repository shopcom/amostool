package com.marketamerica.automation.utilities.helpers;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.testng.annotations.Test;

/**
 * Created by javierv on 8/4/2014.
 */
public class CollectionUtilitiesTests {


    @Test
    public void testSubtractBetweenCollection() {
        final List<String> one = new ArrayList<>();
        final List<String> two = new ArrayList<>();
        final String expected = "test_element";

        two.add(expected);

        final Collection<String> actual = CollectionUtilities.subtract(one, two);
        assertTrue(actual.equals(Arrays.asList(expected)),
                String.format("Expected result to contain \"%s\", Actual: %s", expected, actual));
    }

}
