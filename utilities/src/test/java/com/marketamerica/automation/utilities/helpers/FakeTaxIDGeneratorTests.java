package com.marketamerica.automation.utilities.helpers;

import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.invokeMethod;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

import org.apache.commons.lang3.math.NumberUtils;
import org.testng.annotations.Test;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.utilities.Luhn;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.tax.AustraliaTaxIDGenerator;
import com.marketamerica.automation.utilities.tax.FakeTaxIDGenerator;

/**
 * Created by Javier L. Velasquez on 10/1/2014.
 */
public class FakeTaxIDGeneratorTests {


    @Test
    public void testFakeUnitedStatesTaxID() {
        Configuration.setCountry(Country.UNITED_STATES);
        String actual = FakeTaxIDGenerator.generate();
        assertTrue(actual.length() == 9);
    }

    @Test
    public void testRealCanadaianTaxID() {
        Configuration.setCountry(Country.CANADA);
        String actual = FakeTaxIDGenerator.generate();
        assertTrue(actual.length() == 9);
        assertTrue(Luhn.isValid(Long.valueOf(actual)));
    }

    @Test
    public void testFakeSingaporeTaxId() {
        Configuration.setCountry(Country.SINGAPORE);
        String actual = FakeTaxIDGenerator.generate();
        assertTrue(actual.length() == 9);
        assertFalse(NumberUtils.isNumber(actual.substring(0, 0)));
        assertFalse(NumberUtils.isNumber(actual.substring(actual.length() - 1, actual.length())));
        assertTrue(actual.substring(1, actual.length() - 2).matches("^\\d+$"));
    }

    @Test
    public void testRealAustralianTaxId() {
        Configuration.setCountry(Country.AUSTRALIA);
        String actual = FakeTaxIDGenerator.generate();
        final String id = actual.substring(0, actual.length() - 1);
        assertTrue(id.matches("^\\d+$"));
        final String checkDigit = actual.substring(actual.length() - 1, actual.length());
        assertTrue(NumberUtils.isDigits(checkDigit));
        String expected = String.valueOf(invokeMethod(AustraliaTaxIDGenerator.class, "getTaxIDCheckDigit", id));
        if (expected.contains("java.lang.Object")) {
            expected = "0";
        }
        assertEquals(expected, checkDigit);
    }

    @Test
    public void testFakeMexicanTaxID() {
        Configuration.setCountry(Country.MEXICO);
        String actual = FakeTaxIDGenerator.generate();
        // Expect for the fake tax id to be exactly 3 upper case characters + 9 digits
        actual.matches("^[A-Z]{3}\\d{9}$");
    }

    @Test
    public void testFakeSpanishTaxID() {
        Configuration.setCountry(Country.SPAIN);
        String actual = FakeTaxIDGenerator.generate();
        // expect for the fake tax id to be exactly 8 digits long + 1 upper case character
        actual.matches("^\\d{8}[A-Z]$");
    }

    @Test
    public void testFakeUnitedKingdomTaxID() {
        Configuration.setCountry(Country.UNITED_KINGDOM);
        String actual = FakeTaxIDGenerator.generate();
        // expect for the fake tax id to be exactly 1 upper case character + 6 digits + 1 upper case character
        actual.matches("^[A-Z]\\d{6}[A-Z]$");
    }
}
