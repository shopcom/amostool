package com.marketamerica.automation;

import static org.testng.Assert.assertEquals;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.testng.annotations.Test;

import com.marketamerica.automation.utilities.helpers.DateAndTimeUtilities;

public class DateAndTimeUtilitiesTest {

    @Test
    public void testGetFormattedDate() {
        Date today = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("mm/dd/yyyy");
        System.out.println(sdf.toLocalizedPattern());
        String sd2 = sdf.toLocalizedPattern().replace("mm", "MM").replace("DD", "dd");
        String date = DateAndTimeUtilities.getFormattedDate(today, sd2);
        System.out.println(date);
    }

    @Test(description = "This test our ability to get a relative time")
    public void testGetRelativeTime() {
        Calendar today = Calendar.getInstance();
        Calendar tomorrow = DateAndTimeUtilities.generateRelativeCalendar(0, 0, 1);
        tomorrow.get(Calendar.DAY_OF_MONTH);
        assertEquals(today.get(Calendar.DAY_OF_MONTH) + 1, tomorrow.get(Calendar.DAY_OF_MONTH));
    }

    @Test
    public void test() {
    }

}
