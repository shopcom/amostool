package com.marketamerica.automationframework.elementmanagement;

/**
 * This class holds all common Xpath strings that are used to interact with elements on a DOM.
 * All Xpaths defined in this class must be reusable and not only applicable to a single scenario.
 * Created by Javier L. Velasquez on 7/25/2014.
 */
public final class CommonXPaths {
    private static final String CONTAINS_TEXT = "contains(text(), '$')";

    public static final String H1_CONTAINS_TEXT = createSimpleXPath("h1", CONTAINS_TEXT);
    public static final String A_CONTAINS_TEXT = createSimpleXPath("a", CONTAINS_TEXT);
    public static final String H2_CONTAINS_TEXT = createSimpleXPath("h2", CONTAINS_TEXT);
    public static final String LABEL_CONTAINS_TEXT = createSimpleXPath("label", CONTAINS_TEXT);
    public static final String BUTTON_CONTAINS_TEXT = createSimpleXPath("button", CONTAINS_TEXT);

    /**
     * You should never construct this class!
     */
    private CommonXPaths() {
    }

    /**
     * This method allows you to create simple xpaths based on a tag and an input parameter
     *
     * @param tag      a html tag
     * @param argument an xpathment allowing you to filter an xpath.
     * @return a string representing a simple xpath (e.g. //a[contains(text(), 'test')])
     */
    private final static String createSimpleXPath(final String tag, final String argument) {
        return "//" + tag + "[" + argument + "]";
    }

}
