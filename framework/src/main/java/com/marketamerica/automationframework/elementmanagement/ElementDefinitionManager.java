package com.marketamerica.automationframework.elementmanagement;

import org.openqa.selenium.By;

import java.util.Set;

/**
 * Created by javierv on 7/20/2014.
 */
public interface ElementDefinitionManager {

    public Set<By> findElements(String key);

    public Set<By> findElements(String key, String... additionalValues);

}
