package com.marketamerica.automationframework.elementmanagement;

import com.marketamerica.automation.utilities.PropertyReader;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.enums.Project;
import com.marketamerica.automation.utilities.testmanagement.TestResourceCoordinator;
import com.marketamerica.automation.utilities.webdriver.ByLocator;
import com.marketamerica.automationframework.pages.WebContent;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Javier L. Velasquez on 7/20/2014.
 */
public class PropertyElementDefinitionManager extends TestResourceCoordinator implements ElementDefinitionManager {
    private static final String PROPERTY_DELIMITER = "%";
    private static final String ELEMENT_DEFINITION_DELIMITTER = ":";
    private static final Logger logger = LogManager
            .getLogger(PropertyElementDefinitionManager.class);
    private PropertyReader siteProperties;
    private PropertyReader environmentProperties;
    private PropertyReader environmentCountryProperties;
    private String ELEMENT_DEFINITIONS_ROOT = "/elementdefinitions";

    public PropertyElementDefinitionManager(final Environment environment,
                                            final Country country, Project project) {
        if (project != null)
            ELEMENT_DEFINITIONS_ROOT = "/" + project.toString().toLowerCase() + ELEMENT_DEFINITIONS_ROOT;

        InputStream siteInputStream = getSiteProperties();
        InputStream environmentInputStream = getEnvironmentProperties(environment);
        InputStream environmentCountryInputStream = getEnvironmentCountryProperties(
                environment,
                country);

        if (siteInputStream != null) {
            this.siteProperties = new PropertyReader(siteInputStream);
        } else {
            logger.warn("Could not find properties for the site. Element definitions will not be read from this file");
        }
        if (environmentInputStream != null) {
            this.environmentProperties = new PropertyReader(environmentInputStream);
        } else {
            logger.warn(String.format("Could not find properties for the current environment (%s). Element definitions will not be read from this file", environment));
        }

        if (environmentCountryInputStream != null) {
            this.environmentCountryProperties = new PropertyReader(environmentCountryInputStream);
        } else {
            logger.warn(String.format("Could not find properties for the current environment+country (%s+%s). Element definitions will not be read from this file", environment, country));
        }


    }

    public PropertyElementDefinitionManager(final Environment environment,
                                            final Country country) {
        this(environment, country, null);

    }

    private InputStream getEnvironmentCountryProperties(Environment environment, Country country) {
        return getClass().getResourceAsStream(
                String.format(
                        "%s/%s-%s-elements.properties",
                        ELEMENT_DEFINITIONS_ROOT,
                        environment.toString(),
                        country.toString()
                ).toLowerCase()
        );

    }

    private InputStream getEnvironmentProperties(Environment environment) {
        return getClass().getResourceAsStream(
                String.format(
                        "%s/%s-elements.properties",
                        ELEMENT_DEFINITIONS_ROOT,
                        environment.toString()
                ).toLowerCase()
        );
    }

    private InputStream getSiteProperties() {
        return getClass().getResourceAsStream(
                String.format(
                        "%s/base.properties",
                        ELEMENT_DEFINITIONS_ROOT
                ).toLowerCase()
        );
    }

    private Set<By> getBys(final PropertyReader propertyReader, final String key, String... additionalValues) {
        final Set<By> bys = new HashSet<>();
        if (propertyReader != null && propertyReader.hasKey(key)) {
            String value;
            if (additionalValues != null && additionalValues.length > 0) {
                value = WebContent.interpolateXpath(propertyReader.get(key), additionalValues);
            } else {
                value = propertyReader.get(key);
            }

            final String[] elementDefinitions = value.split(String.valueOf(PROPERTY_DELIMITER));

            for (int i = 0; i < elementDefinitions.length; ++i) {
                final String[] elementDefinition = createElementDefinitionArray(elementDefinitions[i]);
                bys.add(getBy(elementDefinition));
            }
            return bys;
        } else {
            return bys;
        }

    }

    /**
     * Splits a string into an array with 2 entries. 1: element definition, 2: element definition type
     *
     * @param string a string represent an element definition in a property file
     * @return an array representing an element definition
     */
    private String[] createElementDefinitionArray(String string) {
        String[] arguments = new String[2];

        arguments[0] = string.substring(0, string.lastIndexOf(ELEMENT_DEFINITION_DELIMITTER));
        arguments[1] = string.substring(string.lastIndexOf(ELEMENT_DEFINITION_DELIMITTER) + 1, string.length());
        return arguments;
    }

    private By getBy(final String[] elementDefinition) {
        final String definitionValue = elementDefinition[0];
        final String definitionType = elementDefinition[1];
        final ByLocator byLocator = ByLocator.parse(definitionType);
        if (byLocator == null) {
            throw new IllegalArgumentException(String.format("Invalid element definition specified (%s)!", definitionType));
        }
        return byLocator.toBy(definitionValue);
    }


    @Override
    public Set<By> findElements(final String key) {
        return findElements(key, new String[]{});
    }

    @Override
    public Set<By> findElements(final String key, final String... additionalValues) {
        final Set<By> bys = new HashSet<>();
        final Set<By> environmentCountryValues = getBys(environmentCountryProperties, key, additionalValues);
        if (environmentCountryValues.isEmpty()) {
            final Set<By> environmentValues = getBys(environmentProperties, key, additionalValues);
            if (environmentValues.isEmpty()) {
                final Set<By> siteValues = getBys(siteProperties, key, additionalValues);
                if (siteValues.isEmpty()) {
                    if (additionalValues == null || additionalValues.length == 0) {
                        logger.fatal(String.format("No element definition found for %s key", key));
                    } else {
                        logger.fatal(String.format("No element definition found for %s (%s) key", key, Arrays.toString(additionalValues)));
                    }
                } else {
                    return siteValues;
                }
            } else {
                return environmentValues;
            }
        } else {
            return environmentCountryValues;
        }
        return bys;
    }

}