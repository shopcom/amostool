package com.marketamerica.automationframework.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * HMTLTable represents a html table on the DOM.
 * <p>
 * It extracts the headers as a list of Strings and extracts the rows as a List of Maps.
 * Each key in the map represents the table's columns while the corresponding value represents the table data cell value
 * Each Map represents a row within the HTML table. Each Map is stored in a List which preserves the orders of the table's content
 * Created by Javier L. Velasquez on 9/22/2014.
 */
public class HTMLTable extends WebContent {
    private final WebElement table;
    private final List<String> headers;
    private final List<Map<String, String>> rows;

    public HTMLTable(WebDriver driver, WebElement table) {
        super(driver, null, null);
        this.table = table;
        this.headers = getHeaders();
        this.rows = getTable();
    }

    private List<String> getHeaders() {
        return extractTextFromWebElementList(
                table.findElements(By.tagName("th")));

    }

    private List<Map<String, String>> getTable() {
        List<Map<String, String>> rows = new ArrayList<>();
        List<WebElement> rowElements = table.findElements(By.tagName("tr"));

        for (int i = 1; i < rowElements.size(); ++i) {
            List<String> rowWebElement = extractTextFromWebElementList(rowElements.get(i).findElements(By.tagName("td")));
            Map<String, String> rowKeyPairs = new HashMap<>();
            for (int j = 0; j < headers.size(); ++j) {
                rowKeyPairs.put(headers.get(j), rowWebElement.get(j));
            }
            rows.add(rowKeyPairs);
        }
        return rows;
    }

    public WebElement getTableElementByReferenceColumnValue(String referenceColumn, String referenceValue, String columnToSearch) {
        List<WebElement> rowElements = table.findElements(By.tagName("tr"));
        for (int i = 1; i < rowElements.size(); ++i) {
            List<WebElement> rowWebElements = rowElements.get(i).findElements(By.tagName("td"));
            final WebElement webElement = rowWebElements.get(headers.indexOf(referenceColumn));
            if (webElement.getText().equals(referenceValue)) {
                return rowWebElements.get(headers.indexOf(columnToSearch));
            }
        }
        throw new WebDriverException("");
    }

    public List<Map<String, String>> get() {
        return rows;
    }
}
