package com.marketamerica.automationframework.pages;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

/**
 * Page is an abstract representation of a web page that WebDriver will interact
 * with
 */
public abstract class Page extends WebContent {

    public Page(final WebDriver driver, final Translator translator, ElementDefinitionManager elementDefinitionManager) {
        super(driver, translator, elementDefinitionManager);
    }

    /**
     * This method returns a element definition on a page that should indicate whether
     * or not the page exists. It should be used in conjunction with the atPage() method.
     *
     * @return
     * @throws UnableToVerifyPagesExistenceException
     */
    protected abstract By getBody() throws UnableToVerifyPagesExistenceException;

    /**
     * This method allows the user to indicate whether or not they are on the page
     *
     * @return boolean indicating if the user is in fact on the current page
     * @throws com.marketamerica.automationframework.exceptions.UnableToVerifyPagesExistenceException exception indicating that we could not determine if we were on the page. When implementing
     *                                                                                                this method if you cannot accurately determine if you are on the page please throw this
     *                                                                                                exception so it can be handled in subsequent logic
     */
    public abstract boolean atPage() throws UnableToVerifyPagesExistenceException;

}
