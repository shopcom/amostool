package com.marketamerica.automationframework.pages;

import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import org.openqa.selenium.WebDriver;

/**
 * A Page Component represents a component of a a Page
 *
 * @see com.marketamerica.automationframework.pages.Page
 */
public class PageComponent extends WebContent {
    public PageComponent(final WebDriver driver, final Translator translator, ElementDefinitionManager elementDefinitionManager) {
        super(driver, translator, elementDefinitionManager);
    }
}
