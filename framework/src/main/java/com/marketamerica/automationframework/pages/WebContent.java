package com.marketamerica.automationframework.pages;

import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.marketamerica.automation.utilities.helpers.JavaScriptActions;
import com.marketamerica.automation.utilities.helpers.NumberUtilities;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverBrowser;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.tools.reporting.ScreenShooter;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.testmanagement.TestCases;

/**
 * Web content represents any web content that Selenium Web Driver will interact
 * with. Element definitions must be flexible (robust) They must be named
 * appropriately. Additionally they MUST be marked static and final
 * <ol>
 * <li>Elements should also end with distinguishing names.</li>
 * <li>Text fields should end with "Field"</li>
 * <li>Dropdowns should end with "DropDownField"</li>
 * <li>Generic identifier fields used to identify if we are on the page do not
 * have to follow this convention. For example, if you can find an element to
 * uniquely identify the entire body, then you should create a "body" field, and
 * interact with it via getBody()</li>
 * </ol>
 */
public abstract class WebContent {
    /**
     * Maximum time to wait (in ms) when sleeping the current thread
     */
    private static final int THREAD_SLEEP_TIME = 250;
    /**
     * Maximum time to wait when performing an explicit wait
     */
    private static final int MAX_EXPLICIT_WAIT_TIME = 45;
    /**
     * Maximum time to wait (in seconds) when waiting for jQuery AJAX
     */
    private static final int MAX_JQUERY_AJAX_WAIT_TIME = 20;

    private static final int MAX_STALE_ELEMENT_TRIES = 6;

    protected final Logger logger = LogManager.getLogger(getClass());
    protected final WebDriver driver;
    protected final Translator translator;
    protected ElementDefinitionManager elementDefinitionManager;
    private JavaScriptActions javaScriptActions;

	public WebContent(final WebDriver driver, final Translator translator,
			final ElementDefinitionManager elementDefinitionManager) {
		this.driver = driver;
		this.translator = translator;
		this.elementDefinitionManager = elementDefinitionManager;
	}

	/**
	 * This method takes a string that represents an xpath and replaces all
	 * instances of $ with corresponding replacement strings.
	 *
	 * @param xPathString  A string representing an xpath.
	 * @param replacements A list of strings used to replace any references with $
	 * @return An interpolated string
	 */
	public static String interpolateXpath(String xPathString,
			final String... replacements) {
		for (int i = 0; i < replacements.length; i++) {
			xPathString = xPathString.replaceFirst("\\$", replacements[i]);
		}
		return xPathString;
	}

	/**
	 * Checks to see if a WebDriver is a Safari WebDriver. This method checks
	 * the driver capabilities to allow us to check instances of
	 * RemoteWebDriver.
	 *
	 * @param driver a WebDriver object
	 * @return a boolean indicating whether or not the driver is in fact a
	 * safari driver.
	 */
	public static boolean isSafariWebDriver(final WebDriver driver) {
		return getCapabilities(driver).getBrowserName()
				.equals(WebDriverBrowser.SAFARI.toString());
	}

	/**
	 * Returns the capabilities from a given web driver instance
	 *
	 * @param driver a web driver instance
	 * @return the web driver's capabilities
	 */
	public static Capabilities getCapabilities(final WebDriver driver) {
		if (driver instanceof HtmlUnitDriver) {
			return ((HtmlUnitDriver) driver).getCapabilities();
		} else {
			return ((RemoteWebDriver) driver).getCapabilities();
		}
	}

	/**
	 * Checks to see if a WebDriver is an Internet Explorer WebDriver. This
	 * method checks the driver capabilities to allow us to check instances of
	 * RemoteWebDriver.
	 *
	 * @param driver a WebDriver object
	 * @return a boolean indicating whether or not the driver is in fact an
	 * internet explorer driver.
	 */
	public static boolean isInternetExplorerWebDriver(final WebDriver driver) {
		return getCapabilities(driver).getBrowserName()
				.equals(WebDriverBrowser.INTERNET_EXPLORER.toString());
	}

	public static boolean isChromeWebDriver(final WebDriver driver) {
		return getCapabilities(driver).getBrowserName()
				.equals(WebDriverBrowser.CHROME.toString());
	}
	
	public static boolean isFirefoxDriver(final WebDriver driver) {
		return getCapabilities(driver).getBrowserName()
				.equals(WebDriverBrowser.FIREFOX.toString());
	}

	protected JavaScriptActions getJavaScriptActions() {
		if (javaScriptActions == null) {
			javaScriptActions = new JavaScriptActions(driver);
		}
		return javaScriptActions;
	}

	/**
	 * This method waits for an element to be "clickable". This method assists
	 * with synchronization.
	 *
	 * @param by By object describing the element to click
	 * @return The rendered WebElement based on the input by locator
	 */
	protected WebElement waitForElementToBeClickable(final By by) {
		return waitForElementToBeClickable(by, true, MAX_EXPLICIT_WAIT_TIME);
	}

	/**
	 * This method logs a warning if the wait time is greater than 30 seconds.
	 * For some reason "waiting" for a long time causes stability issues with
	 * Internet Explorer Web Driver.
	 *
	 * @param waitTime
	 */
	private void logInternetExplorerDriverTimeoutWarning(final int waitTime) {
		/**
		 * If the wait time is greater than 30, provide a friendly message
		 * telling the user to consider reducing this time in the case of
		 * stability issues.
		 */
		if (driver instanceof InternetExplorerDriver) {
			if (waitTime > 30) {
				logger.warn(String
						.format("Waiting for %s seconds using an "
										+ "Internet Explorer Driver may cause stability "
										+ "issues with the test execution. If you run into issues, "
										+ "please consider reducing this time if possible",
								waitTime));
			}
		}

	}

	/**
	 * This method waits for an element to be "clickable".
	 *
	 * @param by        By object describing the element to click
	 * @param mustExist boolean indicating if we care that the element does not exist.
	 *                  If we do and it doesn't exist, we fail the test.
	 * @param waitTime  max time to wait, overrides the default
	 *                  MAX_EXPLICIT_WAIT_TIME. Setting this value too high seems to
	 *                  crash the IE Driver.
	 * @return The rendered WebElement based on the input By locator.
	 * <p/>
	 * If the element doesn't exist and we don't care, this method will
	 * return null.
	 */
	protected WebElement waitForElementToBeClickable(final By by,
			final boolean mustExist, final int waitTime) {
		logInternetExplorerDriverTimeoutWarning(waitTime);
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		final WebDriverWait wait = new WebDriverWait(driver, waitTime);
		WebElement element = null;
		try {
			element = wait.until(ExpectedConditions.elementToBeClickable(by));
		} catch (TimeoutException e) {
			// If the element must exist fail the test else return a null
			// element
            final String message = String
                    .format("After %s seconds, element identified by '%s' could not be found",
                            waitTime, by.toString());
            if (mustExist) {
				ScreenShooter.capture(driver);
				StepInfo.failTestWithoutScreenshot(message);
			} else {
                logger.warn(message);
            }
		} catch (WebDriverException e) {
			logger.error(e);
		} finally {
			driver.manage().timeouts().implicitlyWait(TestCases.IMPLICIT_WAIT_TIMEOUT, TimeUnit.SECONDS);

		}
		return element;
	}

	/**
	 * @param element
	 * @param waitTime
	 * @return
	 */

	protected boolean waitForElementToBeClickable(final WebElement element,
			final int waitTime) {
		logInternetExplorerDriverTimeoutWarning(waitTime);
		final WebDriverWait wait = new WebDriverWait(driver, waitTime);
		try {
			wait.until(ExpectedConditions.elementToBeClickable(element));
			return true;
		} catch (TimeoutException e) {
			final String message = String
					.format("After %s seconds, element identified by '%s' could not be found",
							waitTime, element);
			ScreenShooter.capture(driver);
			StepInfo.failTestWithoutScreenshot(message);
		}
		return false;
	}

	/**
	 * An expectation for checking that an element is present on the DOM of a
	 * page. This does not necessarily mean that the element is visible.
	 *
	 * @param by
	 * @param mustExist
	 * @param waitTime
	 * @return
	 */
	protected WebElement waitForElementToBePresent(final By by,
			final boolean mustExist, final int waitTime) {
		logInternetExplorerDriverTimeoutWarning(waitTime);
		final WebDriverWait wait = new WebDriverWait(driver, waitTime);
		WebElement element = null;
		try {
			element = wait.until(ExpectedConditions
					.presenceOfElementLocated(by));
		} catch (TimeoutException e) {
			// If the element must exist fail the test else return a null
			// element
			if (mustExist) {
				final String message = String
						.format("After %s seconds, element identified by '%s' could not be found",
								waitTime, by.toString());
				ScreenShooter.capture(driver);
				StepInfo.failTestWithoutScreenshot(message);
			}
		}
		return element;
	}

	/**
	 * This method waits for any AJAX being executed through jQuery to finish.
	 * This method does NOT protect against pure Javascript or pure jQuery or
	 * pure AJAX. It only assists in waiting for AJAX executed through jQuery
	 *
	 * @param reason - string describing why this method is being invoked
	 */
	protected void waitForjQueryAjax(final String reason) {
		int timeWaited = 0;
		while (isjQueryAJAXRunning()
				&& (timeWaited < MAX_JQUERY_AJAX_WAIT_TIME)) {
			try {
				Thread.sleep(THREAD_SLEEP_TIME);
			} catch (InterruptedException e) {
				logger.error(e);
			}
			++timeWaited;
		}
		if (timeWaited > 0) {
			logger.debug(String
					.format("Waiting %s milliseconds for jQuery AJAX operation due to: %s",
							(timeWaited * THREAD_SLEEP_TIME), reason));
		} else {
			logger.warn(String
					.format("Did not have to wait for jQuery AJAX operation due to: '%s'",
							reason));
		}
	}

	/**
	 * This method checks to see if jQuery AJAX is running
	 *
	 * @return a boolean value indicating if jQuery AJAX is currently running
	 */
	private boolean isjQueryAJAXRunning() {
		final String code = "return jQuery.active === 1;";
		try {
			return Boolean.valueOf(String.valueOf(((JavascriptExecutor) driver)
					.executeScript(code)));
		} catch (WebDriverException e) {
			if (e.getMessage().contains("jQuery is not defined")) {
				logger.warn("jQuery was not defined in the DOM, perhaps it hasn't been loaded yet.");
				return false;
			} else {
				throw e;
			}
		}
	}

	protected boolean isJQueryPresent() {
		final String code = "typeof jQuery != 'undefined'";
		return Boolean.valueOf(String.valueOf(((JavascriptExecutor) driver)
				.executeScript(code)));
	}

	/**
	 * This method checks to see if any elements matching our by locator exists
	 * on the dom.
	 * <p/>
	 * This method does not perform an element synchronization. If you wish to
	 * wait for an element to exist, see synchronization method such as
	 * waitForElementToBeClick.
	 *
	 * @param by A by locator representing the element we want to find on the
	 *           dom
	 * @return a boolean telling us if the element was found.
	 */
	public boolean isElementExisting(By by) {
		return driver.findElements(by).size() > 0;
	}

	/**
	 * Overloading method isElementExisting(By by) with a WebElement parameter .
	 * Method checks if element exists
	 *
	 * @param element
	 * @return
	 */
	public boolean isElementExisting(WebElement element) {
		return ExpectedConditions.visibilityOf(element) != null;
	}

	/**
	 * Overloaded version of isElementExisting to wait x number of milliseconds
	 * to check if an element truly exist on the dom
	 *
	 * @param by      a by locator describing an element to search for
	 * @param seconds the number of seconds to wait
	 * @return boolean indicating if the element was found after waiting the
	 * specified number of milliseconds
	 */
	public boolean isElementExisting(final By by, int seconds) {
		return waitForElementToBeClickable(by, false, seconds) != null;
	}

	/**
	 * Checks to see if an element exists by a property key.
	 * @param key
	 * @param seconds
	 * @return
	 */
	public boolean isElementExisting(String key, int seconds) {
		return isElementExisting(getBy(key), seconds);
	}

	/**
	 * This method is to checks if element is existing in the DOM and is also
	 * displayed for a certain wait time.
	 *
	 * @param by
	 * @param seconds
	 * @return boolean indicating element was found in DOM and is displayed.
	 */
	public boolean isElementExistingAndDisplayed(By by, int seconds) {
		if (isElementExisting(by , seconds)) {
			return findElement(by).isDisplayed();
		} else {
			return false;
		}

	}
	
	/**
	 * This method is to checks if element is existing in the DOM and is also
	 * displayed 
	 *
	 * @param by
	 * @return boolean indicating element was found in DOM and is displayed.
	 */
	public boolean isElementExistingAndDisplayed(By by) {
		if (isElementExisting(by)) {
			return findElement(by).isDisplayed();
		} else {
			return false;
		}

	}

	/**
	 * This method checks is to check if element is visible/ displayed
	 *
	 * @param element
	 * @return boolean indicating if element is displayed.
	 */
	public boolean isElementDisplayed(WebElement element) {
		return element.isDisplayed();
	}

	/**
	 * This method checks is to check if element is visible/ displayed
	 * Overloading method isElementDisplayed(WebElement element) with By param
	 *
	 * @param by
	 * @return boolean indicating if element is displayed.
	 */
	public boolean isElementDisplayed(By by) {
		try {
			return findElement(by).isDisplayed();
		} catch (NoSuchElementException|TimeoutException|ElementNotVisibleException e) {
			return false;
		}
	}

	/**
	 * This method checks to see if any elements matching our locator exists on
	 * the dom. The locator uses an interpolated xpath String.
	 * <p/>
	 * * It will replace all "$" with each replacement, in a first come first
	 * replacement basis.
	 * <p/>
	 * Example: //a[contains(text(), "$")] becomes //a[contains(text(),
	 * "My Text")]
	 *
	 * @param xPathString  A string representing an xpath.
	 * @param replacements A list of strings used to replace any references with $
	 * @return a boolean telling us if the element was found.
	 */
	public boolean isElementExistingByXpath(final String xPathString,
			final String... replacements) {
		return driver.findElements(
				By.xpath(interpolateXpath(xPathString, replacements))).size() > 0;
	}

	/**
	 * Overloading method isElementExistingByXpath with waitTime for element to be populated.
	 *
	 * @param xPathString
	 * @param waitTime
	 * @param replacements
	 * @return
	 */
	public boolean isElementExistingByXpath(final String xPathString,
			int waitTime, final String... replacements ) {
		return isElementExisting(
				By.xpath(interpolateXpath(xPathString, replacements)) , 30);
	}

	/**
	 * This method allows us to find a web element on the page by composing the
	 * behavior to the current instance of the webdriver
	 *
	 * @param by An element definition
	 * @return the element found based on the element definition
	 */
	protected WebElement findElement(By by) {
		return driver.findElement(by);
	}

	/**
	 * This method returns List of Web Elements based on element definition
	 *
	 * @param by An element definition
	 * @return the elements found based on the element definition
	 */
	protected List<WebElement> findElements(By by) {
		return driver.findElements(by);
	}

	/**
	 * This method allows a user to find an element using an interpolated xpath
	 * string.
	 * <p/>
	 * It will replace all "$" with each replacement, in a first come first
	 * replacement basis.
	 * <p>
	 * Example: //a[contains(text(), "$")] becomes //a[contains(text(),
	 * "My Text")]
	 * </p>
	 *
	 * @param xPathString  A string representing an xpath.
	 * @param replacements A list of strings used to replace any references with $
	 * @return A web element found using the final interpolated xpath
	 */
	protected WebElement findElementByXpath(final String xPathString,
			final String... replacements) {
		return findElement(By
				.xpath(interpolateXpath(xPathString, replacements)));
	}

	/**
	 * This method translates a message based on current locale
	 *
	 * @param key A key used to get a translated phrase
	 * @return a translated phrase based on the current locale
	 */
	protected String translate(final String key) {
		final String translatedPhrase = translator.translate(key);
		logger.info(String.format("Got translation for %s key: '%s'", key,
				translatedPhrase));
		return translatedPhrase;
	}

	protected void clickStaleElement(final By by) {
		int tries = 0;
		long timeWaited = 0;
		while (tries < MAX_STALE_ELEMENT_TRIES) {
			try {
				final WebElement element = this.waitForElementToBeClickable(by,
						true, THREAD_SLEEP_TIME);
				element.click();
				logger.trace(String
						.format("Was able to click on element identified by '%s' after waiting '%s' ms",
								by.toString(), timeWaited));
				return;
			} catch (StaleElementReferenceException e) {
				try {
					Thread.sleep(THREAD_SLEEP_TIME);
					timeWaited += THREAD_SLEEP_TIME;
				} catch (InterruptedException e1) {
					logger.error(e);
				}
			}
			++tries;
			logger.trace(String.format("element identified by '%s' was stale",
					by.toString()));
		}
		throw new StaleElementReferenceException(
				String.format(
						"Element identified by %s was still stale after %s attempts and waiting '%s' ms!",
						by.toString(), tries, timeWaited));
	}

    protected Select createDropDown(String key) {
        return createDropDown(getBy(key));
    }


    protected Select createDropDown(String key, String... replacementStrings) {
        return createDropDown(getBy(key, replacementStrings));
    }

    protected Select createDropDown(final By by) {
        return new Select(findElement(by));
    }

	protected Select createDropDown(final WebElement element) {
		return new Select(element);
	}

	/**
	 * Method to select an option from drop down list at random
	 * @param by
	 * @return the selected String
	 */
	protected String selectRandomOptionFromDropdown(final By by){
		Select dropdown = createDropDown(by);
		List<String> list = extractTextFromWebElementList(dropdown.getOptions());
		int selection;
		selection = NumberUtilities.generateRandomIntegerUpTo(new Random(), list.size() - 1);
		if (selection == 0) {
			++selection;
		}
		String selectedOption = list.get(selection);
		dropdown.selectByVisibleText(selectedOption);
		return selectedOption;
	}

    /**
     * Attempts to get the current element until it is
     * either not stale anymore or the max time has been spent
     *
     * @param by By locator of element to be searched for
     * @return WebElement that is not stale or null if element is still stale
     */
    protected WebElement getCurrentElement(final By by) {
        long timeWaited = 0;
        final long endTime = System.currentTimeMillis() + MAX_EXPLICIT_WAIT_TIME * 1000;
        while (System.currentTimeMillis() < endTime) {
            try {
                final WebElement element = waitForElementToBePresent(by,
						true, THREAD_SLEEP_TIME);
                logger.trace(String.format("Was able to locate element %s after waiting %s ms",
                        element.toString(),
                        timeWaited));
                return element;
            } catch (StaleElementReferenceException e) {
                try {
                    Thread.sleep(THREAD_SLEEP_TIME);
                    timeWaited += THREAD_SLEEP_TIME;
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
                logger.trace(String.format("Element identified by '%s' was stale",
                        by.toString()));
            }
        }
        logger.fatal(String.format("Element identified by %s after %s ms was still stale",
				by.toString(), timeWaited));
        return null;
    }

    /**
     * Attempt to get the currently selected element of a dropdown
     * until either it is not stale or max time has been spend
     *
     * @param by By locator of element to be searched for
     * @return WebElement of selected option or null if element is stale
     */
    protected WebElement getCurrentSelectedElementOfDropdown(final By by) {
        long timeWaited = 0;
        final long endTime = System.currentTimeMillis() + MAX_EXPLICIT_WAIT_TIME * 1000;
        while (System.currentTimeMillis() < endTime) {
            try {
                final WebElement element = waitForElementToBePresent(by,
						true, THREAD_SLEEP_TIME);
                final Select select = createDropDown(element);
                logger.trace(String
                        .format("Was able to create dropdown for element '%s' after waiting '%s' ms",
								element.toString(), timeWaited));
                return select.getFirstSelectedOption();
            } catch (StaleElementReferenceException e) {
                try {
                    Thread.sleep(THREAD_SLEEP_TIME);
                    timeWaited += THREAD_SLEEP_TIME;
                } catch (InterruptedException e1) {
                    logger.error(e);
                }
            }
            logger.trace(String.format("Selected element of dropdown identified by %s was stale",
                    by.toString()));
        }
        logger.fatal(String.format("After %s ms, first element of '%s' was still stale",
                timeWaited, by.toString()));
        return null;
    }

    /**
     * Handles clearing a text box then typing in the provided input. This
     * method will also perform a native js change() event against the form
     * element to prevent issues encountered with certain web drivers
     *
     * @param by    By locator to find the text field
     * @param input String to be typed into text box
     */
    protected void setTextField(final By by, final Object input) {
        final WebElement textField = waitForElementToBeClickable(by);
        setTextField(textField, String.valueOf(input));
    }

    /**
     * Handles clearing a text box then typing in the provided input. This
     * method will also perform a native js change() event against the form
     * element to prevent issues encountered with certain web drivers
     *
     * @param key   an element definition key
     * @param input String to be typed into text box
     */
    protected void setTextField(final String key, final Object input) {
        final WebElement textField = waitForElementToBeClickable(getBy(key));
        setTextField(textField, String.valueOf(input));
    }

	/**
	 * Handles clearing a text box then typing in the provided input. This
	 * method will also perform a native js change() event against the form
	 * element to prevent issues encountered with certain web drivers
	 *
	 * @param textField the web element representing the text field
	 * @param input     String to be typed into text box
	 */
	protected void setTextField(final WebElement textField, final Object input) {
		textField.click();
		textField.clear();
		textField.sendKeys(String.valueOf(input), Keys.TAB);
		getJavaScriptActions().executeChangeEventOnElement(textField);
	}

	/**
	 * Re-using method setTextField without a click.
	 * Clicking on textbox gives error that element is not clickable at the point sometimes
	 *
	 * @param by the by element representing the text field
	 * @param input     String to be typed into text box
	 */
	protected void setTextFieldWithoutClick(final By by, final Object input) {
		final WebElement textField = waitForElementToBeClickable(by);
		textField.clear();
		textField.sendKeys(String.valueOf(input), Keys.TAB);
		getJavaScriptActions().executeChangeEventOnElement(textField);
	}



    /**
     * Move mouse over a single element. This does not work with Safari. If you
     * need to mouse over an element in Safari, use mouseOverUsingJavaScript()
     *
     * @param element
     */
    public void mouseOverElement(final WebElement element) {
        Actions action = new Actions(driver);
        action = action.moveToElement(element);
        action.build().perform();
    }

    /**
     * Use this Java script when the moveToElement fails to locate the element.
     *
     * @param element
     */
    public void mouseOverUsingJavascript(WebElement element) {
        getJavaScriptActions().mouseOverToElement(element);

    }

    protected void setCheckBoxToTrue(final String key) {
        setCheckBoxToTrue(findElement(key));
    }

    protected void setCheckBoxToTrue(final WebElement element) {
        if (!element.isSelected()) {
            element.click();
        }
    }

    protected void setCheckBoxToFalse(final WebElement element) {
        if (element.isSelected()) {
            element.click();
        }
    }

    /**
     * Select a drop down based on a partial value.
     * @param select
     * @param text
     */
    protected void selectDropDownValueByPartialText(final Select select,
                                                    final Object text) {
        final List<WebElement> options = select.getOptions();
        for (final WebElement option : options) {
            if (option.getText().toLowerCase().contains(String.valueOf(text).toLowerCase())) {
                option.click();
                return;
            }
        }
        throw new WebDriverException(String.format(
                "Unable to find element in dropdown %s with text '%s'", select.toString(), text));
    }

    /**
     * Select an option from the drop down
     *
     * @param by
     * @param optionToBeSelected
     */
    protected void selectDropDownValue(By by, final int optionToBeSelected) {
        Select dropDownSelect = createDropDown(by);
        dropDownSelect.selectByValue(String.format("%02d", optionToBeSelected));
    }

    /**
     * Sometimes Alert is thrown with message " Although this page is encrypted,
     * the information you have entered is to be sent over an unencrypted
     * connection and could easily be read by a third party. Are you sure you
     * want to continue sending this information?" This happens in firefox
     * mostly. At these instances , this method could be called to check if Pop
     * up is thrown and Alert can be accepted.
     *
     * @param acceptAlert
     */
    protected void isAlertPopUp(boolean acceptAlert) {
        boolean presentFlag = false;
        try {
            // Check the presence of alert
            Alert alert = driver.switchTo().alert();
            // Alert present; set the flag
            presentFlag = true;
            if (acceptAlert && presentFlag) {
                // if present consume the alert
                alert.accept();
            }

        } catch (NoAlertPresentException ex) {
            logger.info("Alert not present");
        }
    }

    /**
     * Auto-scroll does not work sometimes - so adding a method to scroll until
     * the element is in view
     *
     * @param element
     * @param waitTime
     */
    protected WebElement scrollToElement(WebElement element,
                                         final int waitTime) {
        // This is not required for Safari since auto scroll to element works in
        // Safari
        if (!((RemoteWebDriver) driver).getCapabilities().getBrowserName()
                .equals(WebDriverBrowser.SAFARI.toString())) {
            getJavaScriptActions().scrollToElement(element);
            final WebDriverWait wait = new WebDriverWait(driver, waitTime);
            try {
                wait.until(ExpectedConditions.visibilityOf(element));
                return element;
            } catch (TimeoutException exception) {
                final String message = String
                        .format("After %s seconds, element identified by '%s' could not be found",
								waitTime, element);
                ScreenShooter.capture(driver);
                StepInfo.failTestWithoutScreenshot(message);
                return null;
            }
        }
        return null;
    }

    /**
     * Overloaded scroll To Element with By reference.
     *
     * @param by
     * @param waitTime
     */
    protected WebElement scrollToElement(By by,
                                         final int waitTime) {
        WebElement element = findElement(by);
        return scrollToElement(element, waitTime);
    }

    /**
     * This method returns a given element definition based on a given key
     *
     * @param key a string identifier representing an element definition
     * @return a by object represented by the given key
     */
    protected By getBy(final String key) {
        return this.elementDefinitionManager.findElements(key).iterator()
                .next();
    }

    /**
     * Scroll to The End Of Page whose length is known and is not dynamic
     *
     */
    protected void scrollToEndOfPage() {
        if (!((RemoteWebDriver) driver).getCapabilities().getBrowserName()
                .equals(WebDriverBrowser.SAFARI.toString())) {
            getJavaScriptActions().scrollToEndOfPage();
        }

    }

    /**
     * This method returns a given element definition based on a given key
     *
     * @param key              a string identifier representing an element definition
     * @param additionalValues optional text to query for. Any $ symbol in the string found
     *                         will be replaced in the string in a FIFO manner. This is
     *                         particular useful for xpath
     * @return a by object represented by the given key
     */
    protected By getBy(final String key, final String... additionalValues) {
        return this.elementDefinitionManager
                .findElements(key, additionalValues).iterator().next();
    }

    /**
     * Finds a web element based on the given key
     *
     * @param key a string identifier representing an element definition
     * @return a web element object represented by the given key
     */
    protected WebElement findElement(final String key) {
        final WebElement element = waitForElementToBeClickable(getBy(key),
				false, WebContent.MAX_EXPLICIT_WAIT_TIME);
        if (element != null) {
            return element;
        }
        return null;
    }

    /**
     * Returns a list of elements based on a property key.
     * @param key a property key used to reference a element definition based on country and environment
     * @return a list of web elements that correspond to given element definition
     */
    protected List<WebElement> findElements(final String key) {
        return findElements(getBy(key));
    }

    /**
     * Finds a web element based on the given key
     *
     * @param key              a string identifier representing an element definition
     * @param additionalValues optional text to query for. Any $ symbol in the string found
     *                         will be replaced in the string in a FIFO manner. This is
     *                         particular useful for xpath queries
     * @return a web element object represented by the given key
     */
    protected WebElement findElement(final String key,
                                     final String... additionalValues) {
        final WebElement element = waitForElementToBeClickable(
				getBy(key, additionalValues), false,
				WebContent.MAX_EXPLICIT_WAIT_TIME);
        if (element != null) {
            return element;
        }
        return null;
    }

    /**
     * This method returns multiple element definition based on a given key.
     * This method is useful in the instance in the instance a single element
     * may take many forms. Ideally this method is used very seldom
     *
     * @param key a string identifier representing an element definition
     * @return a set of by objects represented by the given key
     */
    protected Set<By> getBysByMultipleDefinitions(final String key) {
        return this.elementDefinitionManager.findElements(key);
    }

    /**
     * Finds a web element based on the given key. This method will check for
     * multiple web element definitions checking each one until one is valid. If
     * no element is found the method returns null
     *
     * @param key a string identifier representing an element definition
     * @return a web element object represented by the given key
     */
    protected WebElement findElementByMultipleDefinitions(final String key) {
        final Set<By> elementDefinitions = getBysByMultipleDefinitions(key);
        for (final By elementDefinition : elementDefinitions) {
            final WebElement element = waitForElementToBeClickable(
					elementDefinition, false, WebContent.MAX_EXPLICIT_WAIT_TIME);
            if (element != null) {
                return element;
            }
        }
        return null;
    }

    /**
     * Waits for element to be visible for
     * up to determined amount of time
     *
     * @param by       By locator of element to be searched for
     * @param waitTime how long (in seconds) to wait for element
     * @return WebElement being waited for
     */
    protected WebElement waitForElementToBeVisible(final By by, final int waitTime) {
        // This condition would not work if implicit wait is set , so turning it
        // off first and turning it back on in finally clause.
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        final WebDriverWait wait = new WebDriverWait(driver, waitTime);
        try {
            // Wait for element to return once
            // the element is visible. After element
            // is returned, isVisible is true
            return wait.until(ExpectedConditions.visibilityOfElementLocated(by));
        } catch (TimeoutException e) {
            logger.error("Element is not visible");
        } finally {
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        }
        return null;
    }

	/**
	 * Wait for an element to be present on the DOM
	 * @param by
	 * @param waitTime
	 * @return
	 */
	protected WebElement waitForElementToBePresent(final By by, final int waitTime) {
		return waitForElementToBePresent(by, true, waitTime);
	}

    /**
     * An expectation for checking that an element is either invisible or not
     * present on the DOM.
     */
    public boolean waitForElementToBeInvisible(final By by, final int waitTime) {
        // This condition would not work if implicit wait is set , so turning it
        // off first and turning it back on in finally clause.
        driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        final WebDriverWait wait = new WebDriverWait(driver, waitTime);
        boolean isInvisible = false;
        try {
            // return ExpectedConditions
            // .invisibilityOfElementLocated(by).apply(driver);
            isInvisible = wait.until(ExpectedConditions
                    .invisibilityOfElementLocated(by));

        } catch (TimeoutException e) {
            logger.error("Element still exists in DOM");
        } finally {
            driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
        }
        return isInvisible;
    }

	/**
	 * Extract the text from each web element and return a list representing each text value.
	 * <p>
	 * Each text value will be trimmed.
	 * @param elements A list of Web Elements
	 * @return a list of strings representing each web element
	 */
    public List<String> extractTextFromWebElementList(List<WebElement> elements) {
        return elements.stream().map(
                element -> element.getText().trim()
        ).collect(Collectors.toList());
    }

    public WebElement clickCenterOfWebElement(By by) {
        WebElement element = findElement(by);
        Actions build = new Actions(driver);
        build.moveToElement(element, (element.getSize().getWidth() / 2), (element.getSize().getHeight() / 2)).click().build().perform();
        return element;
    }

    /**
     * This method will allow you to traverse menus with any number of depth.
     * This is particularly useful for applications such as UWF.
     *
     * @param menuOptions
     */
    public void clickSubMenu(final By... menuOptions) {
        Actions moveToHeader = new Actions(driver);
        for (int i = 0; i < menuOptions.length; ++i) {
            if (i < menuOptions.length - 1) {
                moveToHeader = moveToHeader
                        .moveToElement(waitForElementToBeClickable(menuOptions[i]));
                moveToHeader.build().perform();
				waitForElementToBeVisible(menuOptions[i + 1], 5);
            } else {
                Actions moveToSubMenu = moveToHeader
                        .moveToElement(waitForElementToBeClickable(menuOptions[i]));
                moveToSubMenu.click().build().perform();
            }
        }
    }

    /**
     * This method will allow you to switch handler to the newly opened tab or window.
     * This is particularly useful for applications such as UWF.
     */
    public void switchWindowHandler() {
        for (String winHandle : driver.getWindowHandles()) {
            driver.switchTo().window(winHandle);
        }
    }

    /**
     * Check to see if a select tag contains the specified value. This method IS case-sensitive
     *
     * @param select a {@code Select} object
     * @param value  a value to search for. The method will search its String form
     * @return {@code true} if the specified value exists in the select object, else {@code false}
     * @see org.openqa.selenium.support.ui.Select
     */
    protected static boolean containsValue(final Select select, final Object value) {
        final List<WebElement> options = select.getOptions();
        for (final WebElement option : options) {
            if (option.getText().equals(String.valueOf(value))) {
                return true;
            }
        }
        return false;
    }

    /**
	 * This method checks to see if the element is enabled
	 *
	 * @param by A by locator representing the element we want to find on the
	 *           dom
	 * @return a boolean telling us if the element is enabled.
	 */
	public boolean isElementEnabled(By by) {
		WebElement element = findElement(by);
		return element.isEnabled();
	}
	
	/**
     * Check to see if a select tag contains the specified value or a sub-string. 
     * This method IS case-sensitive
     *
     * @param select a {@code Select} object
     * @param value  a value to search for. The method will search its Sub-String form
     * @return {@code true} if the specified value exists in the select object, else {@code false}
     * @see org.openqa.selenium.support.ui.Select
     */
    protected static boolean containsPartialValue(final Select select, final Object value) {
        final List<WebElement> options = select.getOptions();
        for (final WebElement option : options) {
            if (option.getText().toLowerCase().contains(String.valueOf(value).toLowerCase())) {
                return true;
            }
        }
        return false;
    }

}
