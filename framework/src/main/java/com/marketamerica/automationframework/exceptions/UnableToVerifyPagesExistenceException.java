package com.marketamerica.automationframework.exceptions;

/**
 * This exception can be thrown when attempting to verify a page exists.
 * In some instances we don't know and should throw this exception instead.
 */
public class UnableToVerifyPagesExistenceException extends RuntimeException {


    /**
     *
     */
    private static final long serialVersionUID = 1479242649477666473L;

    public UnableToVerifyPagesExistenceException() {
    }

    public UnableToVerifyPagesExistenceException(String message) {
        super(message);
    }

    public UnableToVerifyPagesExistenceException(String message, Throwable cause) {
        super(message, cause);
    }

}
