package com.marketamerica.automationframework.exceptions;

/**
 * In instances the site has not been implemented the application should
 * throw an exception telling the user of the fact
 *
 * @author Javier L. Velasquez
 */
public class SiteURLNotImplemented extends Exception {


    /**
     *
     */
    private static final long serialVersionUID = 626827500889151679L;

    public SiteURLNotImplemented() {
    }

    public SiteURLNotImplemented(String message) {
        super(message);
    }

    public SiteURLNotImplemented(String message, Throwable cause) {
        super(message, cause);
    }
}