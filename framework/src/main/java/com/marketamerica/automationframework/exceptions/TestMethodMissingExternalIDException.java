package com.marketamerica.automationframework.exceptions;

/**
 * Runtime exception that should be thrown if a test method does not include an
 * external id
 *
 * @author javierv
 */
public class TestMethodMissingExternalIDException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 5807201240036796948L;

    public TestMethodMissingExternalIDException() {
    }

    public TestMethodMissingExternalIDException(String message) {
        super(message);
    }

    public TestMethodMissingExternalIDException(String message, Throwable cause) {
        super(message, cause);
    }

}
