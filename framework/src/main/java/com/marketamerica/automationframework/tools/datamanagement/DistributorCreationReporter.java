package com.marketamerica.automationframework.tools.datamanagement;

import com.marketamerica.automation.testdata.Distributor;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.excel.ExcelWorkBookWriter;
import com.marketamerica.automation.utilities.helpers.DateAndTimeUtilities;
import org.apache.log4j.Logger;
import org.apache.log4j.LogManager;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;


/**
 * Created by Nicholas Spitzer on 6/25/2015.
 */
public class DistributorCreationReporter {

    private static final Logger logger = LogManager.getLogger(DistributorCreationReporter.class);
    private static ExcelWorkBookWriter workbook;
    private static boolean configured = false;
    private static int distributors;

    /**
     * Initialize static Distributor Creation Reporter
     *
     * @param environment
     */
    public synchronized static void initialize(final Environment environment) {

        if (!configured) {
            /*workbook = new ExcelWorkBookWriter(String.format(
                    "test/resources/testdata/distributor-creation/%s/dist-creation-%s.xlsx",
                    environment.toString().toLowerCase(),
                    DateAndTimeUtilities.getFormattedDate(Calendar.getInstance().getTime(), "MMddyyyyhhmmss")), false);*/

            workbook = new ExcelWorkBookWriter("C:\\Users\\nspitzer\\Desktop\\output.xlsx");

            workbook.createSheet("createdDistributors");
            workbook.addHeaders(Arrays.asList("Home Country", "Distributor ID", "Representative ID", "Password",
                    "Email", "First Name", "Last Name", "PPC ID"));
            configured = true;
            distributors = 0;
        }
    }

    /**
     * Record new Distributor
     */
    public synchronized static void recordDistributor(final Distributor distributor, final WebDriver driver) {

        final List<String> entries = new ArrayList<>();

        entries.add(distributor.getAddress().getCountry());
        entries.add(distributor.getDistributorId());
        entries.add(distributor.getRepresentativeId());
        entries.add(distributor.getRepresentativePassword());
        entries.add(distributor.getEmail());
        entries.add(distributor.getFirstName());
        entries.add(distributor.getLastName());
        entries.add(distributor.getPcID());

        final String message = String.format("Recording newly signed up distributor \"%s\" to distributor Reporter",
                distributor.getDistributorId());

        workbook.addRow(entries);

        ++distributors;
    }

    /**
     * If workbook was populated with at least one distributor record,
     * write and close the report. All columns get resized as well.
     */
    public synchronized static void generateReport() {
        if (distributors == 0) {
            logger.info("We didn't make any distributors in this run. No report generated");
        } else if (configured && workbook != null) {
            workbook.resizeAllColumns();
            workbook.writeAndCloseWork();
        }
        configured = false;
    }
}