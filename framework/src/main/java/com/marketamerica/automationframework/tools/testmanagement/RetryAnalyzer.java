package com.marketamerica.automationframework.tools.testmanagement;

import com.marketamerica.automation.utilities.helpers.ReflectiveUtilities;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.*;

import java.util.*;

import static com.marketamerica.automation.constants.GlobalConstants.TestNGSuiteParameters.RETRY_COUNT;
import static com.marketamerica.automation.utilities.helpers.CollectionUtilities.containsPartially;
import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.hasDataDrivenProvider;

public class RetryAnalyzer implements IRetryAnalyzer, ITestListener {

    private final static Logger logger = LogManager
            .getLogger(RetryAnalyzer.class);

    private static final String BEGIN_OUTPUT_STRING = "<div style=\"color:red;font-size:25px;\">============ BEGIN OLD OUTPUT ============</div><br>";
    private static final List<String> throwableMessagesNotToReRun = new ArrayList<>();

    static {
        throwableMessagesNotToReRun.add("didn't finish within the time-out");
        throwableMessagesNotToReRun.add("java.lang.NullPointerException");
        throwableMessagesNotToReRun.add("Order");
    }

    private static String END_OUTPUT_STRING = "<div style=\"color:red;font-size:25px;\">============= End OLD OUTPUT =============</div><br>";
    private static int MAX_RETRY_COUNT = 0;
    private static boolean CONFIGURED = false;
    private int tries = 0;

    private static void configure() {
        CONFIGURED = true;
    }

    private static boolean isConfigured() {
        return CONFIGURED;
    }

    /**
     * This method allows us to determine whether or not to rerun a test. The is
     * determined by a global test suite parameter. In some instances a failed
     * test may be indicative of a system issue or an issue that doesn't merit a
     * rerun. In these instances, those tests are not reran. To add new errors,
     * to exclude, add them to the class variable throwableMessagesNotToReRun in
     * the static initializer.
     *
     * @param result a test result object.
     * @return a boolean indicating if a test should be rerun
     * @see org.testng.IRetryAnalyzer
     * @see org.testng.ITestListener
     */
    public boolean retry(final ITestResult result) {

        // If the Throwable messages matches our list then don't bother
        // rerunning the test.
        final String throwableMessage = ReflectiveUtilities.getThrowableAsString(result
                .getThrowable());
        if (containsPartially(throwableMessagesNotToReRun,
                throwableMessage)) {
            logger.info(String
                    .format("Did not rerun %s, because its throwable message (\"%s\") met our exclusion policy",
                            result.getName(), throwableMessage));
            return false;
        }

        final int testRetryCount = ReflectiveUtilities.getRetryCount(result);

        // If we haven't tried more than our max count go ahead and try another
        // test invocation.
        if (tries < MAX_RETRY_COUNT || tries < testRetryCount) {
            // If the max retry count is greater than the test level retry count
            // but the current number of tries exceeds the test try count then don't rerun.
            // This is because the test retry count has greater precedence then the global
            // max retry count
            if (MAX_RETRY_COUNT > testRetryCount) {
                if (testRetryCount != 0 && tries > testRetryCount) {
                    logger.info(String.format("Tries %s exceeded retry count %s for %s.%s", tries, testRetryCount, result
                            .getMethod().getConstructorOrMethod().getMethod()
                            .getDeclaringClass().getName(), result.getMethod()
                            .getMethodName()));
                    return false;
                }
            }
            logger.info(String.format("Rerunning '%s.%s,' Try: %s", result
                    .getMethod().getConstructorOrMethod().getMethod()
                    .getDeclaringClass().getName(), result.getMethod()
                    .getMethodName(), (tries + 1)));
            tries++;
            return true;
        } else {
            logger.warn(String.format(
                    "'%s.%s' has tried '%s' times and is still failing!",
                    result.getMethod().getConstructorOrMethod().getMethod()
                            .getDeclaringClass().getName(), result.getMethod()
                            .getMethodName(), (tries + 1)
            ));
            return false;
        }
    }

    @Override
    public void onTestStart(ITestResult result) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTestSuccess(ITestResult result) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTestFailure(ITestResult result) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTestSkipped(ITestResult result) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        // TODO Auto-generated method stub

    }

    @Override
    public synchronized void onStart(ITestContext context) {
        if (!isConfigured()) {
            final String retryCount = context.getSuite().getXmlSuite()
                    .getAllParameters().get(RETRY_COUNT);
            try {
                if (retryCount != null
                        && Integer.parseInt(retryCount) > MAX_RETRY_COUNT) {
                    MAX_RETRY_COUNT = Integer.parseInt(retryCount);
                }
                logger.info(String.format("Max Retry Count set to \"%s\"",
                        MAX_RETRY_COUNT));
            } catch (NumberFormatException e) {
                logger.error(e);
            }
            configure();
        } else {
            logger.info(String.format("Max Retry Count has already been configured to \"%s\"!", MAX_RETRY_COUNT));
        }
    }

    private boolean hasMatchingTestCase(IResultMap results, ITestNGMethod testResult) {
        return results.getAllResults().stream().anyMatch(result -> {
            Map<String, String> currentIterationParameters = result.getTestContext().getCurrentXmlTest().getAllParameters();
            Map<String, String> testResultParameters = testResult.getXmlTest().getAllParameters();
            return currentIterationParameters.equals(testResultParameters);
        });
    }

    @Override
    public void onFinish(ITestContext context) {
        IResultMap failedTests = context.getFailedTests();
        final Iterator<ITestResult> failedTestCasesIterator = failedTests
                .getAllResults().iterator();
        while (failedTestCasesIterator.hasNext()) {
            final ITestResult failedTestCase = failedTestCasesIterator.next();
            final ITestNGMethod method = failedTestCase.getMethod();
            // Don't remove tests if no passes have occurred
            if (context.getPassedTests().getResults(method).size() == 0) {
                continue;
            }

            if (failedTests.getResults(method).size() > 1 &&
                    !hasDataDrivenProvider(method.getConstructorOrMethod().getMethod())) {
                logger.debug("Failed test case removed as duplicate:"
                        + failedTestCase.getTestClass().toString());
                failedTestCasesIterator.remove();
                ITestResult newResult;
                try {
                    newResult = context.getPassedTests().getResults(method)
                            .iterator().next();
                } catch (NoSuchElementException e) {
                    continue;
                }
                List<String> oldOutput = new ArrayList<>();
                oldOutput.add(BEGIN_OUTPUT_STRING);
                Reporter.setCurrentTestResult(failedTestCase);
                oldOutput.addAll(Reporter.getOutput(failedTestCase));
                oldOutput.add(END_OUTPUT_STRING);
                Reporter.setCurrentTestResult(newResult);

                // log each entry of the old output in the reporter
                oldOutput.forEach(Reporter::log);
            } else {
                if (context.getPassedTests().getResults(method).size() > 0 &&
                        !hasDataDrivenProvider(method.getConstructorOrMethod().getMethod())) {
                    logger.debug("Failed test case removed as duplicate:"
                            + failedTestCase.getTestClass().toString());
                    failedTestCasesIterator.remove();
                    ITestResult newResult;
                    try {
                        newResult = context.getPassedTests().getResults(method)
                                .iterator().next();
                    } catch (NoSuchElementException e) {
                        continue;
                    }
                    List<String> oldOutput = new ArrayList<>();
                    oldOutput.add(BEGIN_OUTPUT_STRING);
                    Reporter.setCurrentTestResult(failedTestCase);

                    oldOutput.addAll(Reporter.getOutput(failedTestCase));
                    oldOutput.add(END_OUTPUT_STRING);
                    Reporter.setCurrentTestResult(newResult);

                    // log each entry of the old output in the reporter
                    oldOutput.forEach(Reporter::log);
                }
            }
        }

    }
}
