package com.marketamerica.automationframework.tools.datamanagement.typeconverters;

import com.marketamerica.automation.testdata.attributes.DistributorApplicationType;
import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

/**
 * Created by Javier L. Velasquez on 9/22/2014.
 */
public class DistributorApplicationTypeConverter extends AbstractSingleValueConverter {

    @SuppressWarnings("rawtypes")
    @Override
    public boolean canConvert(Class clazz) {
        return DistributorApplicationType.class.isAssignableFrom(clazz);
    }

    public Object fromString(String input) {
        return DistributorApplicationType.parse(input);
    }

}
