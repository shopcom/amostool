package com.marketamerica.automationframework.tools.factories;

import com.marketamerica.automation.utilities.helpers.Translator;

import java.util.Locale;

public class TranslatorFactory {

    private static Translator createTranslator(final String language,
                                               final String countryAbreviation) {
        return new Translator(new Locale(language, countryAbreviation));
    }

    public Translator createUnitedStatesEnglishTranslator() {
        return createTranslator("en", "US");
    }

    public Translator createUnitedStatesSpanishTranslator() {
        return createTranslator("es", "US");
    }

    public Translator createTaiwanChineseTranslator() {
        return createTranslator("zh", "TW");
    }

    public Translator createAustraliaEnglishTranslator() {
        return createTranslator("en", "AU");
    }

}
