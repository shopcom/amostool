package com.marketamerica.automationframework.tools.datamanagement.typeconverters;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;
import org.apache.commons.lang3.LocaleUtils;

import java.util.Locale;

/**
 * The locale convert takes a string and creates a corresponding locale object for XStream
 * The format is language code_Country Code
 * Created by javierv on 8/29/2014.
 */
public class LocaleConverter extends AbstractSingleValueConverter {

    @Override
    public boolean canConvert(Class clazz) {
        return Locale.class.isAssignableFrom(clazz);
    }

    public Object fromString(String input) {
        return LocaleUtils.toLocale(input);
    }

}
