package com.marketamerica.automationframework.tools.testmanagement.mobile;

import com.marketamerica.automationframework.tools.testmanagement.SuiteBuilder;
import org.testng.ITestContext;

/**
 * Created by javierv on 11/17/2014.
 */
public class MobileSuiteBuilder extends SuiteBuilder {

    public MobileSuiteBuilder(ITestContext context) {
        super();
        suiteParameters = context.getSuite().getXmlSuite().getAllParameters();
        configureSuite();
    }
}
