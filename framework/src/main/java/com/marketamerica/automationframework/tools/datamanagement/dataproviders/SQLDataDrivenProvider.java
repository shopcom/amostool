package com.marketamerica.automationframework.tools.datamanagement.dataproviders;

import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automationframework.tools.datamanagement.SQLDataManager;

import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.getAddressKeys;
import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.getCategoryKeyForSQL;
import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.getIterationKeyForSQL;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;


/**
 * The SQL Data Provider provides database Data as a part of a TestNG Iteration
 *
 * @author Kathy Gong
 * @see com.marketamerica.automationframework.tools.datamanagement.SQLDataManager
 */
public class SQLDataDrivenProvider extends AbstractDataProvider {
	private static final Logger logger = LogManager
            .getLogger(SQLDataDrivenProvider.class);

    @DataProvider(name = "products")
    public static Object[][] getProducts(final ITestContext context,
                                          final Method method) {
        // Create a data manager based on our current context
        final SQLDataManager dataManager = createSQLDataManager(context);
        
        String country = dataManager.getLocale().getDisplayCountry();        
        // get product category key listed in a test method via the SQLDataDrivenTest annotation        
        //final List <String> requestedCategoryKeys = getAddressKeys(method);
        final String category = getCategoryKeyForSQL(method);
        // get iteration number key listed in a test method via the SQLDataDrivenTest annotation        
        final int requestedNumber = Integer.parseInt(getIterationKeyForSQL(method));        
        
        Object[][] requestedProducts = new Object[requestedNumber][1];
        
    	String productCode  = null;
    	BigDecimal distributorCost = new BigDecimal(0);
    	BigDecimal retailCost = new BigDecimal(0);
    	BigDecimal BV = new BigDecimal(0);
    	BigDecimal IBV = new BigDecimal(0);
    	Boolean freight = null;
    	    	
    	String query;         
        
        // retrieve 2 products based on product country and category
        query = "SELECT Top ? * FROM Product "
        		+ "WHERE productCountry = ? and productCategory = ? and isDiscontinued != 1 and isOutOfStock != 1 "
        		+ "ORDER BY NEWID()";    
        
        Connection connection;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        
        try {
            connection = dataManager.getTDCRSDatabase().getDatabaseSession().getConnection();
            statement = connection.prepareStatement(query);
            
            statement.setInt(1, requestedNumber);
            statement.setString(2, country);
            statement.setString(3, category);             
            
            resultSet = statement.executeQuery();   
            
            for (int i = 0; i < 2; i ++) {
            	Product product = new Product();
            	if (resultSet.next()) {
            		productCode = resultSet.getString("productCode")
                            .replaceAll("\\n", "").trim();                	               	
                	distributorCost = resultSet.getBigDecimal("distributorCost");                	
                	retailCost = resultSet.getBigDecimal("retailCost");
                	BV = resultSet.getBigDecimal("productBV");
                	IBV = resultSet.getBigDecimal("productIBV");
                	freight = resultSet.getBoolean("isFreightCharged");  
                	
                }
            	 product.setSku(productCode);
                 product.setDistributorCost(distributorCost);
                 product.setRetailCost(retailCost);   
                 product.setProductBV(BV);
                 product.setProductIBV(IBV);
                 product.setFreight(freight);
                 
                 requestedProducts[i][0] = product;            	
            }              
            
            resultSet.close();
                         
        } catch (SQLException e) {
            logger.error(e);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e1) {
                    logger.error(e1);
                }
            }             
        } 
        return requestedProducts;
        
        // Get the requested addresses by the DataDrivenTest annotation
        // final List <String> requestedAddressKeys = getAddressKeys(method);
        // Return a list of addresses
        // final AddressesList requestedAddressList = dataManager.getAddressList();
        // Get our requested Data
        //final Object[][] requestedAddresses = new Object[2][2];
        //= getRequestedData(
                //requestedAddressList, requestedAddressKeys);
        //if (requestedAddresses[0] == null) {
            //Assert.fail(String.format("Could not find any addresses for %s",
                   // method.getName()));
        //}
         
    }       

}
