package com.marketamerica.automationframework.tools.testmanagement;

import static com.marketamerica.automation.utilities.helpers.CollectionUtilities.createList;
import static com.marketamerica.automationframework.tools.reporting.StepInfo.addMessage;

import java.util.Arrays;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.testdata.Distributor;
import com.marketamerica.automation.testdata.DistributorApplication;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.excel.ExcelWorkBookWriter;
import com.marketamerica.automation.utilities.helpers.DateAndTimeUtilities;
import com.marketamerica.dsb.DSBClient;

/**
 * This classes purpose is to serialize basic distributor data into Excel. All operations are static as there should be no
 * instances of this class.
 * Created by kathy gong  on 01/20/2015.
 */
public class DistributorReporter {
    private static final Logger logger = LogManager
            .getLogger(DistributorReporter.class);

    private static DSBClient dsbClient;
    private static ExcelWorkBookWriter workbook;
    private static boolean configured = false;
    private static int records;

    public synchronized static void initialize(final Environment environment) {
        final String projectName = Configuration.getProjectName() == null || Configuration.getProjectName().isEmpty()
                ? "project-name-undefined" : Configuration.getProjectName();

        dsbClient = new DSBClient(environment);
        if (!configured) {
        workbook = new ExcelWorkBookWriter(
                String.format("target/distributors-%s-%s-%s.xlsx",
                        projectName,
                        environment.getAbbreviation().toLowerCase(),
                        DateAndTimeUtilities.getFormattedDate(
                                Calendar.getInstance().getTime(),
                                "MMddyyyyhhmmss"
                        )
                )
                , false);

        workbook.createSheet("Distributors");
        workbook.addHeaders(Arrays.asList("Distributor ID", "Rep ID", "Password", 
                "Home Country", "Distributor Type", "Email Address" , "PPC ID" , "Completed UFMS"));
        records = 0;
        configured= true;
        }
    }

    /**
     * Take a distributor and serialize it as an entry to the current workbook. In addition to the attributes present in
     * the class,
     * dsb calls are made to populate additional data. If the DSB returns an error, a message is printed to
     * the current report
     *
     * @param distributor an instance of Distributor
     */
    public synchronized static void record(Distributor distributor) {
         
        final Map<String, String> distributorDetail = dsbClient.getDistInquiryDetail(distributor);
        
        final String message = String.format("Recording newly signed up distributor \"%s\" to distributor Reporter", distributorDetail);
        addMessage(message);
        logger.debug(message);
        final List<Object> values = createList(distributorDetail.values().toArray());
        workbook.addRow(values);
        ++records;
    }
   
    /**
     * For SUW and Partner Now Unfranchise Tests since DSB calls are already made to set distributorApplication 
     *
     * @param distributorApplication an instance of DistributorApplication
     */
    public synchronized static void record(DistributorApplication distributorApplication) {
    	Distributor distributor = distributorApplication.getDistributor();
        final String message = String.format("Recording newly signed up distributor \"%s\" to distributor Reporter", 
        		distributor.getDistributorId());
        addMessage(message);
        logger.debug(message);
        final Map<String, Object> distributorDetail = getDistributorApplicationDetail(distributorApplication);
        final List<Object> values = createList(distributorDetail.values().toArray());
        workbook.addRow(values);
        ++records;
    }

    private static Map<String, Object> getDistributorApplicationDetail(DistributorApplication distributorApplication){
    	 Distributor distributor = distributorApplication.getDistributor();
         Map<String, Object> distributorData = new LinkedHashMap<>();
         
         distributorData.put("Distributor ID", distributor.getDistributorId());
         distributorData.put("Rep ID", distributor.getRepresentativeId());
         distributorData.put("Password", distributor.getPassword());
         distributorData.put("Home Country", distributor.getAddress().getCountry());        
         distributorData.put("Distributor Type", distributor.getType().toString());
         distributorData.put("Email Address", distributor.getEmail()); 
         distributorData.put("PPC ID", distributor.getPcID());
         distributorData.put("Completed UFMS", String.valueOf(distributorApplication.completedUFMS()));
         return distributorData;    
    }

    /**
     * If the workbook was correctly populated and at least one distributor record was saved
     * write and close the report. All columns will be re-sized.
     */
    public synchronized static void generateReport() {
        if (records == 0) {
            logger.info("No distributors recorded. Report will not be generated");
        } else if (configured && workbook != null) {
            workbook.resizeAllColumns();
            workbook.writeAndCloseWork();
        }
        configured = false;
    }

}
