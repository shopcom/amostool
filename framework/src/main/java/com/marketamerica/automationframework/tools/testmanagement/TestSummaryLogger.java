package com.marketamerica.automationframework.tools.testmanagement;

import org.apache.commons.lang3.time.DurationFormatUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.ITestResult;

/**
 * The purpose of this class is to add a basic summary
 * of the current test execution to the logger.
 * Created by Javier L. Velasquez on 8/23/2014.
 */
public final class TestSummaryLogger {
    final static Logger logger = LogManager
            .getLogger(TestSummaryLogger.class);

    private static int total;
    private static int failed;
    private static int passed;
    private static int skipped;

    private static final long startingTime;

    static {
        startingTime = System.currentTimeMillis();
    }

    /**
     * Log the current pass/fail/skip rate
     *
     * @param status the TestNG ITestResult status
     */
    public static synchronized void log(int status) {
        updateTally(status);
        logCurrentTally();
    }

    /**
     * Log the current execution summary tally to the logger
     */
    private static void logCurrentTally() {

        long totalElapsedTime = System.currentTimeMillis();
        long duration = (totalElapsedTime - startingTime);

        logger.info(
                String.format(
                        "Duration: %s, [Totals] Executed: \"%s\", Passed: \"%s (%,.2f%%)\", Failed: \"%s (%,.2f%%)" +
                                "\"," +
                                " " +
                                "Skipped: \"%s (%,.2f%%)\"",
                        DurationFormatUtils.formatDuration(duration, "H:mm:ss", true),
                        total,
                        passed, (100 * (passed / (double) total)),
                        failed, (100 * (failed / (double) total)),
                        skipped, (100 * (skipped / (double) total))
                )
        );
    }

    /**
     * Update the current tally to include the new status
     *
     * @param status
     */
    private static void updateTally(int status) {
        ++total;
        switch (status) {
            case ITestResult.FAILURE:
                ++failed;
                break;

            case ITestResult.SKIP:
                ++skipped;
                break;

            case ITestResult.SUCCESS:
                ++passed;
                break;
        }
    }
}
