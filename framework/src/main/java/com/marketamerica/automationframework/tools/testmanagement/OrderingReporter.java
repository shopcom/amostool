package com.marketamerica.automationframework.tools.testmanagement;

import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.getMethodExternalId;
import static com.marketamerica.automationframework.pages.WebContent.getCapabilities;
import static com.marketamerica.automationframework.tools.reporting.StepInfo.addMessage;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.excel.ExcelWorkBookWriter;
import com.marketamerica.automation.utilities.helpers.DateAndTimeUtilities;
import com.marketamerica.automation.utilities.helpers.ReflectiveUtilities;
import com.marketamerica.automation.utilities.helpers.RegularExpressions;
import com.marketamerica.dsb.DSBClient;

/**
 * Allows for the central capture of Orders.
 * Created by Javier L. Velasquez on 8/11/2014.
 */
public class OrderingReporter {
    private static final Logger logger = LogManager
            .getLogger(OrderingReporter.class);
    private static ExcelWorkBookWriter workbook;
    private static boolean configured = false;
    private static int orders = 0;

    /**
     * Initialize the static Ordering Reporter.
     *
     * @param environment the environment that the ordering will take place (used for the report name)
     * @param country     the country that the ordering will take place (used for the report name)
     */
    public synchronized static void initialize(final Environment environment, final Country country) {
        final String projectName = Configuration.getProjectName() == null || Configuration.getProjectName().isEmpty()
                ? "project-name-undefined" : Configuration.getProjectName();

        if (!configured) {
            workbook = new ExcelWorkBookWriter(
                    String.format("target/order-report-%s-%s-%s.xlsx",
                            projectName,
                            environment.getAbbreviation().toLowerCase(),
                            DateAndTimeUtilities.getFormattedDate(
                                    Calendar.getInstance().getTime(),
                                    "MMddyyyyhhmmss"
                            )
                    )
                    , false);

            workbook.createSheet("Orders");
            workbook.addHeaders(Arrays.asList("Test Name", "ID", "Browser", "Order Number", "Timestamp"));
            configured = true;
            orders = 0;
        }
    }

    /**
     * Record a new order number. In addition to the order number, this method will also grab the current test method name, test id, and timestamp.
     * These four values are added as a row to the final Excel report.
     *
     * @param orderNumber a String representing an order number.
     *                    This method will also remove invalid values from an order number such as unnecessary alpha characters
     */
    public synchronized static void recordOrder(final String orderNumber, final WebDriver driver) {
        final Method method = ReflectiveUtilities.getTestMethodInCurrentThread();
        final List<String> entries = new ArrayList<>();

        final String methodName = ReflectiveUtilities.getFullMethodName(method);
        final String formattedOrderNumber = RegularExpressions.matchFirstPattern(orderNumber, "\\d+");
        final String timeStamp = DateAndTimeUtilities.getFormattedDate(
                Calendar.getInstance().getTime(), "MM/dd/yyyy hh:mm:ss");
        final String id = getMethodExternalId(method);
        final String browser = getCapabilities(driver).getBrowserName();
        entries.add(methodName);
        entries.add(id);
        entries.add(browser);
        entries.add(formattedOrderNumber);
        entries.add(timeStamp);
        final String message = String.format(
                "Adding Order #: \"%s\", Test: \"%s - %s (in %s)\", Timestamp: %s\" to Ordering Report",
                formattedOrderNumber, methodName, id, browser, timeStamp
        );
        logger.debug(
                message
        );
        addMessage(message);
        workbook.addRow(entries);
        ++orders;
    }

    /**
     * Generate the Excel Report
     */
    public synchronized static void generateReport() {
        if (orders == 0) {
            logger.info("No orders recorded. Report will not be generated");
        } else if (configured && workbook != null) {
            workbook.resizeAllColumns();
            workbook.writeAndCloseWork();
        }
        configured = false;
    }
    
    public static boolean cancelOrder(String orderId, String distributorId){
    	DSBClient dsbClient = new DSBClient();
    	return dsbClient.cancelOrder(orderId, distributorId);
    }
}
