package com.marketamerica.automationframework.tools.testmanagement;

import com.marketamerica.automationframework.listeners.TestLinkReporter;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

/**
 * Created by Javier L. Velasquez on 11/4/2014.
 */
public abstract class RecordableTests {
    protected final Logger logger = LogManager.getLogger(getClass());
    private TestLinkReporter testLinkReporter;

    /**
     * Initializes the field {@code testLinkReporter} to the current test context.
     * <p>
     * Initialization is "lazy initialization" and will only occur once per thread.
     *
     * @param context a TestNG test context
     */
    private void createTestLinkReporter(final ITestContext context) {
        if (testLinkReporter == null)
            testLinkReporter = new TestLinkReporter(context.getSuite());

    }

    @AfterMethod(alwaysRun = true)
    public void tearDownTest(final ITestContext context, final ITestResult result) {
        createTestLinkReporter(context);
        if (testLinkReporter.isConnected()) {
            testLinkReporter.recordExecution(result);
        }
    }

}
