package com.marketamerica.automationframework.tools.datamanagement.typeconverters;

import com.marketamerica.automation.testdata.attributes.StoreClassification;
import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

/**
 * This converter is used by XStream to convert String values to StoreClassification enums
 * Created by javierv on 8/28/2014.
 */
public class StoreClassificationConverter extends AbstractSingleValueConverter {

    @Override
    public boolean canConvert(Class clazz) {
        return StoreClassification.class.isAssignableFrom(clazz);
    }

    public Object fromString(String input) {
        return StoreClassification.parseAbbrevication(input);
    }
}
