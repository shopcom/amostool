package com.marketamerica.automationframework.tools.reporting;

import com.marketamerica.automationframework.listeners.ValidationListener;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.getTestMethodInCurrentThread;

/**
 * 'Soft' stepInfo that parallel the Verify class. These will log everything as
 * a Verify would, but will trap exceptions and allow the test to continue. At
 * the end of the test method the framework will fail the test if any
 * Validations have failed.
 *
 * @author archanac
 */
public class Validate extends StepInfo {

	private static final Logger logger = LogManager.getLogger(Validate.class);
	/*
	 * Using a Thread local class for each thread to hold an implicit reference
	 * to its copy of a thread-local variable as long as the
	 * verificationFailuresMaphread is alive
	 */
	private static ThreadLocal<HashMap<ITestResult, List<Throwable>>> validationFailuresMap = new ThreadLocal<HashMap<ITestResult, List<Throwable>>>() {
		@Override
		public HashMap<ITestResult, List<Throwable>> initialValue() {
			return new HashMap<>();
		}
	};

	/**
	 * Validate if actual stringtofind is contained in stringtoEvaluate
	 *
	 * @param actual
	 * @param expected
	 * @param description
	 */
	public static void validateContains(final String stringToEvaluate,
			final String stringToFind, String description) {
		try {
			validateTrue(stringToEvaluate.contains(stringToFind),
					String.format("Expected %s to contain %s",
							stringToEvaluate, stringToFind));
		} catch (Throwable throwable) {
			addValidationFailure(throwable);
		}
	}

	/**
	 * Validate that the expectedListOfStrings contains all of the listOfStringsToFind strings
	 *
	 * @param actual - listOfStringsToFind
	 * @param expected - expectedListOfStrings
	 * @param description
	 */

	public static void validateListContainsAll(final List <String> expectedListOfStrings,
			final List <String> listOfStringsToFind, String description) {
		try {
			validateTrue(expectedListOfStrings.containsAll(listOfStringsToFind), description);
		} catch (Throwable throwable) {
			logger.error(String.format("%s does not contain all strings in %s", listOfStringsToFind,
					expectedListOfStrings));
			addValidationFailure(throwable);
		}
	}

	/**
	 * Validate if the condition is true.
	 *
	 * @param condition
	 * @param description
	 */
	public static void validateTrue(boolean condition, String description) {
		try {
			addStep();
			Assert.assertTrue(condition, description);
			Reporter.log(formattedMessage(true, description));
		} catch (Throwable throwable) {
			addValidationFailure(throwable);
		}
	}

	/**
	 * Validate if the condition is false
	 *
	 * @param condition
	 * @param description
	 */
	public static void validateFalse(boolean condition, String description) {
		try {
			addStep();
			Assert.assertFalse(condition, description);
			Reporter.log(formattedMessage(true, description));
		} catch (Throwable throwable) {
			addValidationFailure(throwable);
		}

	}

	/**
	 * Validate if two Strings are equal
	 *
	 * @param actual
	 * @param expected
	 * @param description
	 */
	public static void validateEquals(final Object actual,
			final Object expected, final String description) {
		try {
			addStep();
			Assert.assertEquals(actual, expected, description);
			Reporter.log(formattedMessage(true, description));
		} catch (Throwable throwable) {
			logger.error(String.format("%s is not equal to %s", actual,
					expected));
			addValidationFailure(throwable);
		}

	}

	/**
	 * @param actual
	 * @param expected
	 * @param description
	 */
	public static void validateNotEquals(final String actual,
			final String expected, final String description) {
		try {
			addStep();
			Assert.assertNotEquals(actual, expected, description);
			Reporter.log(formattedMessage(true, description));

		} catch (Throwable throwable) {
			addValidationFailure(throwable);
		}

	}

	/**
	 * get the Validation Failures Map
	 *
	 * @return
	 */
	public static List<Throwable> getValidationFailuresMap() {
		List<Throwable> verificationFailures = validationFailuresMap.get().get(
				Reporter.getCurrentTestResult());
		return verificationFailures == null ? new ArrayList<>()
				: verificationFailures;
	}

	/**
	 * Add a Validation Failure to the validationFailuresMap
	 *
	 * @param throwable
	 */
	private static void addValidationFailure(Throwable throwable)
			throws RuntimeException {
		ScreenShooter.captureDesktopScreenshot();
		if (throwable instanceof RuntimeException
				&& throwable.getMessage().equals(
						ValidationListener.INITIALIZATION_CHECK_MESSAGE)) {
			throw new RuntimeException(
					String.format(
							"%s is using the validation class but the corresponding listener has not been added to the current test suite. Terminating run! "
									+ "Suppressed stack trace: %s",
							getTestMethodInCurrentThread(),
							throwable.getSuppressed()));
		}
		// TODO : Need to add Screenshot for failures
		// ScreenShooter.capture(driver);
		Reporter.log(formattedMessage(false, throwable.getMessage()));
		List<Throwable> verificationFailures = getValidationFailuresMap();
		validationFailuresMap.get().put(Reporter.getCurrentTestResult(),
				verificationFailures);
		verificationFailures.add(throwable);
		logger.error("Adding Validation Failure", throwable);
	}

	public static void addStep() {
		StepInfo.addStep();
		// if the user is using a validate method but they haven't added it as a
		// suite listener bomb their run.
		if (!ValidationListener.isConfigured()) {
			throw new RuntimeException(
					ValidationListener.INITIALIZATION_CHECK_MESSAGE);
		}
	}

}