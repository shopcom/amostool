package com.marketamerica.automationframework.tools.datamanagement;

import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.getFieldValue;
import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.hasFieldThatContainsValue;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.testdata.Address;
import com.marketamerica.automation.testdata.CreditCard;
import com.marketamerica.automation.testdata.CustomMiniWebSite;
import com.marketamerica.automation.testdata.Distributor;
import com.marketamerica.automation.testdata.FastStartKit;
import com.marketamerica.automation.testdata.Portal;
import com.marketamerica.automation.testdata.Product;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.testdata.StaffMember;
import com.marketamerica.automation.testdata.Store;
import com.marketamerica.automation.testdata.User;
import com.marketamerica.automation.testdata.address.HongKongAddress;
import com.marketamerica.automation.testdata.address.MexicanAddress;
import com.marketamerica.automation.testdata.address.SpainAddress;
import com.marketamerica.automation.testdatalists.AbstractDataList;
import com.marketamerica.automation.testdatalists.AddressesList;
import com.marketamerica.automation.testdatalists.CreditCardList;
import com.marketamerica.automation.testdatalists.CustomMiniProductCategoriesList;
import com.marketamerica.automation.testdatalists.CustomMiniWebSiteList;
import com.marketamerica.automation.testdatalists.DistributorList;
import com.marketamerica.automation.testdatalists.FastStartKitList;
import com.marketamerica.automation.testdatalists.PortalList;
import com.marketamerica.automation.testdatalists.PreferredCustomerList;
import com.marketamerica.automation.testdatalists.ProductList;
import com.marketamerica.automation.testdatalists.StaffMemberList;
import com.marketamerica.automation.testdatalists.StoreList;
import com.marketamerica.automation.testdatalists.UserList;
import com.marketamerica.automation.utilities.enums.CustomMiniProductCategories;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.enums.Project;
import com.marketamerica.automationframework.tools.datamanagement.typeconverters.DistributorApplicationTypeConverter;
import com.marketamerica.automationframework.tools.datamanagement.typeconverters.HongKongRegionTypeConverter;
import com.marketamerica.automationframework.tools.datamanagement.typeconverters.LocaleConverter;
import com.marketamerica.automationframework.tools.datamanagement.typeconverters.StoreClassificationConverter;
import com.thoughtworks.xstream.XStream;

/**
 * <p>
 * Data Manager provides central management of test data such as address
 * information, product information, and credit card information. It should be
 * used to get initialized lists of test data.
 * </p>
 * <p>
 * Scenario specific Data should be located under:
 * ${project.root}/src/test/resources/${country}/${environment} Common Data is
 * located under: ${project.root}/src/test/resources/${country}/common
 * </p>
 *
 * @author Javier L. Velasquez
 */
public class XMLDataManager extends DataManager {

    /*
     * Define the names of our test data files
     */
    private static final String PRODUCT_XML_NAME = "products.xml";
    private static final String ADDRESS_XML_NAME = "addresses.xml";
    private static final String CREDIT_CARD_XML_NAME = "creditcards.xml";
    private static final String SHOPPER_XML_NAME = "shoppers.xml";
    private static final String DISTRIBUTOR_XML_NAME = "distributors.xml";
    private static final String CUSTOM_MINI_WEBSITE_XML_NAME = "websites.xml";
    private static final String STORE_XML_NAME = "stores.xml";
    private static final String PORTAL_XML_NAME = "portals.xml";
    private static final String FAST_START_XML_NAME = "fastStartKits.xml";
    private static final String STAFF_MEMBER_XML = "staffMember.xml";
    private AddressesList addresses;

    private XStream xstream;

    public XMLDataManager(final Environment environment, final Locale locale) {
        super(environment, locale);
        this.xstream = new XStream();

        this.setAliases();
        this.initializeXMLConverters();
    }

    private void initializeXMLConverters() {
        this.xstream.registerConverter(new StoreClassificationConverter());
        this.xstream.registerConverter(new LocaleConverter());
        this.xstream.registerConverter(new DistributorApplicationTypeConverter());
        this.xstream.registerConverter(new HongKongRegionTypeConverter());
    }

    /**
     * Configure XML aliases. This helps us not to hard code package paths
     * representing class paths to test data implementations
     */
    private void setAliases() {

        this.xstream.alias("Address", Address.class);
        this.xstream.alias("Addresses", AddressesList.class);
        this.xstream.alias("HongKongAddress", HongKongAddress.class);
        this.xstream.alias("MexicanAddress", MexicanAddress.class);
        //this.xstream.alias("SpainAddress", SpainAddress.class);
        this.xstream.alias("User", User.class);
        this.xstream.alias("Users", UserList.class);
        this.xstream.alias("CreditCard", CreditCard.class);
        this.xstream.alias("CreditCards", CreditCardList.class);
        this.xstream.alias("Product", Product.class);
        this.xstream.alias("Products", ProductList.class);
        this.xstream.alias("Shopper", Shopper.class);
        this.xstream.alias("Shoppers", PreferredCustomerList.class);
        this.xstream.alias("Distributor", Distributor.class);
        this.xstream.alias("Distributors", DistributorList.class);
        this.xstream.alias("Portal", Portal.class);
        this.xstream.alias("Portals", PortalList.class);
        this.xstream.alias("Stores", StoreList.class);
        this.xstream.alias("Store", Store.class);
        this.xstream.alias("FastStartKits", FastStartKitList.class);
        this.xstream.alias("FastStartKit", FastStartKit.class);
        this.xstream.alias("StaffMember", StaffMember.class);
        this.xstream.alias("StaffMembers", StaffMemberList.class);
        this.xstream.alias("CustomMiniWebSite", CustomMiniWebSite.class);
        this.xstream.alias("CustomMiniWebSites", CustomMiniWebSiteList.class);
        this.xstream.alias("productCategories", CustomMiniProductCategoriesList.class);
        this.xstream.alias("productCategory", CustomMiniProductCategories.class);
        
    }

    /**
     * This method returns a Test Data List based on the file path and xml name
     *
     * @param filePath the file path to the xml
     * @param xmlName  the xml file name
     * @return a Test Data List object representing a collection of TestData.
     */
    private AbstractDataList getTestDataList(final String filePath,
                                         final String xmlName) {

		/*
         * Based on collections of data objects, we store those collections in
		 * the field called testDataList. This field is inherited by all
		 * TestDataLists.
		 */
        this.xstream.addImplicitCollection(AbstractDataList.class, "testDataList");

        InputStream inputStream = getInputStream(filePath, xmlName);
        AbstractDataList testData;
        try {
            testData = (AbstractDataList) xstream.fromXML(inputStream);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }
        return testData;
    }

    /**
     * get the input stream based for the xml file
     *
     * @param filePath
     * @param xmlName
     * @return
     */
    private InputStream getInputStream(String filePath,
                                       String xmlName) {
        final InputStream inputStream = getClass().getResourceAsStream(
                filePath + xmlName);
        if (inputStream == null) {
            if (xmlName == null) {
                xmlName = "\"No XML Defined\"";
            }
            if (filePath == null) {
                filePath = "\"No FilePath Defined\"";

            }
            logger.error(String.format("Could not find %s in %s", xmlName,
                    filePath));
            return null;
        }
        return inputStream;
    }

    /**
     * This method returns a Shoppers Data List based on the file path and xml name
     *
     * @param filePath the file path to the xml
     * @param xmlName  the xml file name
     * @return a Shoppers Data List object representing a collection of shoppers.
     */
    private PreferredCustomerList getShoppersDataList(final String filePath,
                                            final String xmlName) {
        InputStream inputStream = getInputStream(filePath, xmlName);
        this.xstream.addImplicitCollection(AbstractDataList.class, "shopperDataList");
        PreferredCustomerList shopperData;
        try {
            shopperData = (PreferredCustomerList) xstream.fromXML(inputStream);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }
        return shopperData;
    }

    /**
     * This method returns a Shoppers Data List based on the file path and xml name
     *
     * @param filePath the file path to the xml
     * @param xmlName  the xml file name
     * @return a Shoppers Data List object representing a collection of shoppers.
     */
    private DistributorList getDistributorDataList(final String filePath,
                                                   final String xmlName) {
        InputStream inputStream = getInputStream(filePath, xmlName);
        this.xstream.addImplicitCollection(DistributorList.class, "distributorDataList");
        DistributorList distributorData;
        try {
            distributorData = (DistributorList) xstream.fromXML(inputStream);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }
        setUserAddresses(distributorData.getDistributors());
        return distributorData;
    }

    /**
     * This method returns a Portal Data List based on the file path and xml name
     *
     * @param filePath the file path to the xml
     * @param xmlName  the xml file name
     * @return a Portal Data List object representing a collection of portals.
     */
    private PortalList getPortalDataList(final String filePath,
                                         final String xmlName) {
        InputStream inputStream = getInputStream(filePath, xmlName);
        this.xstream.addImplicitCollection(PortalList.class, "portalDataList");
        PortalList portalData;
        try {
            portalData = (PortalList) xstream.fromXML(inputStream);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }
        return portalData;
    }

    public FastStartKitList getFastStartKitList() {
        return (FastStartKitList) getTestDataList(
                getPathToDataByEnvironmentAndCountry(), FAST_START_XML_NAME);
    }
    
    /**
     * This method returns a Staff Member Data List based on the file path and xml name
     *
     * @param filePath the file path to the xml
     * @param xmlName  the xml file name
     * @return a Staff Member Data List object representing a collection of Staff Member.
     */
    private StaffMemberList getStaffMemberDataList(final String filePath,
                                            final String xmlName) {
        InputStream inputStream = getInputStream(filePath, xmlName);
        this.xstream.addImplicitCollection(AbstractDataList.class, "staffMemberDataList");
        StaffMemberList staffMemberData;
        try {
            staffMemberData = (StaffMemberList) xstream.fromXML(inputStream);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }
        return staffMemberData;
    }
    
    /**
     * This method returns a CustomMini WebSites Data List based on the file path and xml name
     *
     * @param filePath the file path to the xml
     * @param xmlName  the xml file name
     * @return a Shoppers Data List object representing a collection of shoppers.
     */
    private CustomMiniWebSiteList getCustomMiniWebSiteDataList(final String filePath,
                                                   final String xmlName) {
        InputStream inputStream = getInputStream(filePath, xmlName);
        this.xstream.addImplicitCollection(CustomMiniWebSiteList.class, "CustomMiniWebSiteDataList");
        CustomMiniWebSiteList webSiteData;
        try {
        	webSiteData = (CustomMiniWebSiteList) xstream.fromXML(inputStream);
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {
                logger.error(e);
            }
        }
        for (final CustomMiniWebSite webSite  : webSiteData.getCustomMiniWebSites()){
            String addressKey = null;
            String distributorKey = null;
            if (hasFieldThatContainsValue(webSite, "addressKey")) {
                addressKey = getFieldValue(webSite, "addressKey").toString();
            }
            
            if (hasFieldThatContainsValue(webSite, "distributorKey")) {
            	distributorKey = getFieldValue(webSite, "distributorKey").toString();
            }

            if (addressKey != null && !addressKey.isEmpty()) {
            	webSite.setAddress(getAddressList().getAddress(
                        addressKey));
            } else {
            	webSite.setAddress(new Address());
            }
            
            if (distributorKey != null && !distributorKey.isEmpty()) {
            	webSite.setDistributor(getDistributorList().getDistributor(
            			distributorKey));
            } else {
            	webSite.setDistributor(new Distributor());
            }
        }
        
        return webSiteData;
    }

    /**
     * For each User set its address based on its address key
     *
     * @param users a list of users
     */
    private <T extends User> void setUserAddresses(final List<T> users) {
        for (final User user : users) {
            String addressKey = null;
            if (hasFieldThatContainsValue(user, "addressKey")) {
                addressKey = getFieldValue(user, "addressKey").toString();
            }

            if (addressKey != null && !addressKey.isEmpty()) {
                user.setAddress(getAddressList().getAddress(
                        addressKey));
            } else {
                user.setAddress(new Address());
            }
        }
    }

    /**
     * For each Shopper set its portal based on its portal key
     *
     * @param Shoppers a list of Shoppers
     */
    private <T extends User> void setPortal(final List<Shopper> Shoppers) {
        for (final Shopper shopper : Shoppers) {
            String portalKey = null;
            if (hasFieldThatContainsValue(shopper, "portalKey")) {
                portalKey = getFieldValue(shopper, "portalKey").toString();
            }

            if (portalKey != null
                    && !portalKey.isEmpty()) {
                shopper.setPortal(getPortalList().getPortal(
                        portalKey));
            } else {
                shopper.setPortal(new Portal());
            }
        }
    }

    /**
     * This method de-serializes the portal xml into a PortalList object
     *
     * @return returns a list of Portals
     */
    public PortalList getPortalList() {
        return getPortalDataList(
                getPathToDataByEnvironmentAndCountry(), PORTAL_XML_NAME);
    }

    /**
     * This method de-serializes the credit card xml into a CreditCardList
     * object
     *
     * @return returns a list of credit cards
     */
    public CreditCardList getCreditCardList() {
        return (CreditCardList) getTestDataList(
                getPathToDataByCountry(), CREDIT_CARD_XML_NAME);
    }

    /**
     * This method de-serializes the distributor xml into a DisrtibutorList
     * object
     *
     * @return returns a list of distributors
     */
    public DistributorList getDistributorList() {
        return getDistributorDataList(
                getPathToDataByEnvironmentAndCountry(), DISTRIBUTOR_XML_NAME);
    }
    
    /**
     * This method de-serializes the staff member xml into a StaffMemberList
     * object
     * 
     * @return list of StaffMemberList
     */
    public StaffMemberList getStaffMemberList() {
        return getStaffMemberDataList(
                getPathToDataByEnvironmentAndCountry(), STAFF_MEMBER_XML);
    }
    
    /**
     * This method de-serializes the staff member xml into a Custom Mini WebSites List
     * object
     * 
     * @return CustomMiniWebSiteList
     */
    public CustomMiniWebSiteList getCustomMiniWebSitesList() {
        return getCustomMiniWebSiteDataList(
                getPathToDataByEnvironmentAndCountry(), CUSTOM_MINI_WEBSITE_XML_NAME);
    }


    /**
     * This method deserializes the addresses xml into a AddressesList object
     *
     * @return returns a list of addresses
     */
    public AddressesList getAddressList() {
        if (this.addresses != null) {
            return this.addresses;
        } else {
            return (AddressesList) getTestDataList(
                    getPathToDataByCountry(), ADDRESS_XML_NAME);
        }

    }

    /**
     * Returns the list of stores. Stores are grouped by environment and country
     *
     * @return
     */
    public StoreList getStoreList() {
        return (StoreList) getTestDataList(
                getPathToDataByEnvironmentAndCountry(), STORE_XML_NAME);
    }

    /**
     * @return returns a list of products
     */
    public ProductList getProductList() {
        final ProductList productList = (ProductList) getTestDataList(
                getPathToDataByEnvironmentAndCountry(), PRODUCT_XML_NAME);
        setStores(productList.getProducts());
        return productList;
    }

    /**
     * Sets the stores for each product in the given list. Defaults to MA product
     *
     * @param products
     */
    private void setStores(List<Product> products) {
        for (final Product product : products) {
            String storeId = null;
            if (hasFieldThatContainsValue(product, "storeId")) {
                storeId = getFieldValue(product, "storeId").toString();
            }
            if (storeId != null
                    && !storeId.isEmpty() && getStoreList() != null) {
                product.setStore(getStoreList().getStore(storeId));
            } else {
                product.setStore(Store.getMarketAmerica());
            }
        }
    }


    /**
     * For each Shopper set its distributor based on its distributor key
     *
     * @param shoppers a list of shoppers
     */
    private void setDistributor(final List<Shopper> shoppers) {
        for (final Shopper shopper : shoppers) {
            String distributorKey = null;
            if (hasFieldThatContainsValue(shopper, "distributorKey")) {
                distributorKey = getFieldValue(shopper, "distributorKey").toString();
            }

            if ((distributorKey != null)
                    && !distributorKey.isEmpty()) {
                shopper.setDistributor(getDistributorList().getDistributor(
                        distributorKey));
            } else {
                shopper.setDistributor(new Distributor());
            }
        }
    }

    
    /**
     * For Custom Mini Project , each Shopper will have website Key
     *
     * @param shoppers a list of shoppers
     */
    private void setWebSiteKey(final List<Shopper> shoppers) {
        for (final Shopper shopper : shoppers) {
            String webSiteKey = null;
            if (hasFieldThatContainsValue(shopper, "webSiteKey")) {
            	webSiteKey = getFieldValue(shopper, "webSiteKey").toString();
            }

            if ((webSiteKey != null)
                    && !webSiteKey.isEmpty()) {
                shopper.setWebSite(getCustomMiniWebSitesList().getCustomMiniWebSite(
                		webSiteKey));
            } else {
                shopper.setWebSite(new CustomMiniWebSite());
            }
        }
    }

    /**
     * Get a list of shoppers
     *
     * @return returns a list of shoppers based on the current locale
     */
    public PreferredCustomerList getShopperList() {
        final PreferredCustomerList shoppers = getShoppersDataList(
                getPathToDataByEnvironmentAndCountry(), SHOPPER_XML_NAME);
        setUserAddresses(shoppers.getShoppers());
        setPortal(shoppers.getShoppers());
        setDistributor(shoppers.getShoppers());
        setWebSiteKey(shoppers.getShoppers());
        setLocale(shoppers.getShoppers());
        return shoppers;
    }

    /**
     * Sets a list of shopper's locale
     *
     * @param shoppers a collection of shoppers
     */
    private void setLocale(List<Shopper> shoppers) {
        shoppers.stream().filter(shopper -> shopper.getLocale() == null).forEach(shopper -> {
            shopper.setLocale(getLocale());
        });
    }

}