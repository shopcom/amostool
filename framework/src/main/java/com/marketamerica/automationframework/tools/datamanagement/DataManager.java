package com.marketamerica.automationframework.tools.datamanagement;

import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Locale;

/**
 * Created by Javier L. Velasquez on 9/26/2014.
 */
public abstract class DataManager {
    protected final Logger logger = LogManager
            .getLogger(getClass());

    private Environment environment;
    private String country;
    private Locale locale;

    public DataManager(final Environment environment, final Locale locale) {
        this.environment = environment;
        this.country = locale.getDisplayCountry();
        this.locale = locale;
    }

    private String formatString(String string) {
        return string.toLowerCase().replaceAll(" ", "")
                .replaceAll("_", "");
    }

    private String formatCountryPath(final String country) {
        if (country.equals(Country.NOT_CLASSIFIED.getAbbreviation().toLowerCase())) {
            return "global";
        } else {
            return country;
        }
    }

    /**
     * Get the path to test data by country. This method does not account for
     * environment.
     *
     * @return
     */
    protected String getPathToDataByCountry() {
        String formattedCountry = formatCountryPath(formatString(country));
        return String.format("/testdata/%s/", formattedCountry);

    }

    /**
     * This method determines where our data is located based on the environment
     * and country. The returned path is in the following format:
     * "/"${country}/${environment}/"
     *
     * @return
     */
    protected String getPathToDataByEnvironmentAndCountry() {
        String formattedEnvironment = formatString(environment.toString());
        String formattedCountry = formatCountryPath(formatString(country));

        if (formattedEnvironment.equals("proof_of_concept")
                || formattedEnvironment.equals("poc")) {
            formattedEnvironment = "staging";
        }

        return String.format("/testdata/%s/%s/", formattedCountry,
                formattedEnvironment);
    }

    /**
     * @return returns the current locale being used by the Data Manager
     */
    public Locale getLocale() {
        return locale;
    }


}
