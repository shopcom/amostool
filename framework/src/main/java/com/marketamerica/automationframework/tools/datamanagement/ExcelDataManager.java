package com.marketamerica.automationframework.tools.datamanagement;

import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.excel.ExcelWorkbookReader;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Locale;

/**
 * Created by Javier L. Velasquez on 9/26/2014.
 */
public class ExcelDataManager extends DataManager {
    private static final String EXCEL_WORKBOOK_NAME = "workbook.xlsx";
    private static final String ADDRESS_EXCEL_NAME = "addresses.xlsx";
    private static final String ADDRESSES_SHOPPERS_IDS_AND_DESCRIPTIONS = "addressesShoppersIdsAndDescription.xlsx";
    private static final String LOGIN_EXCEL_NAME = "logins.xlsx";

    public ExcelDataManager(Environment environment, Locale locale) {
        super(environment, locale);
    }

    /**
     * This method returns the current test data work test data parser based on
     * the environment and country.
     *
     * @return an Excel Workbook to be used to read stored data.
     */
    public ExcelWorkbookReader getExcelWorkbookTestDataParser() {
        final URL resource = getClass().getResource(
                getPathToDataByEnvironmentAndCountry() + EXCEL_WORKBOOK_NAME);
        final File file = new File(resource.getFile());
        return new ExcelWorkbookReader(file.getAbsolutePath());
    }

    public ExcelWorkbookReader getAddressExcelWorkbookParser() {
        final URL resource = getClass().getResource(getPathToDataByEnvironmentAndCountry() + ADDRESS_EXCEL_NAME);
        final File file = new File(resource.getFile());
        return new ExcelWorkbookReader(file.getAbsolutePath());
    }

    public ExcelWorkbookReader getLoginExcelWorkbookParser() {
        final URL resource = getClass().getResource(getPathToDataByEnvironmentAndCountry() + LOGIN_EXCEL_NAME);
        final File file = new File(resource.getFile());
        return new ExcelWorkbookReader(file.getAbsolutePath());
    }

    public ExcelWorkbookReader getAddressesShoppersIdsAndDescription() {
        final URL resource = getClass().getResource(getPathToDataByEnvironmentAndCountry() + ADDRESSES_SHOPPERS_IDS_AND_DESCRIPTIONS);

        Path destination = null;
        try {
            destination = Paths.get(resource.toURI());
        } catch (URISyntaxException e) {
            logger.error(e);
        }
        assert destination != null;
        return new ExcelWorkbookReader(destination.toString());
    }

    public ExcelWorkbookReader getWorkBook(final String workbookName) {
        final URL resource = getClass().getResource(getPathToDataByEnvironmentAndCountry() + workbookName);
        Path destination = null;
        try {
            destination = Paths.get(resource.toURI());
        } catch (URISyntaxException e) {
            logger.error(e);
        }
        assert destination != null;
        return new ExcelWorkbookReader(destination.toString());
    }
}
