package com.marketamerica.automationframework.tools.datamanagement.dataproviders;

import com.marketamerica.automation.testdata.*;
import com.marketamerica.automation.testdata.address.HongKongAddress;
import com.marketamerica.automation.testdata.address.MexicanAddress;
import com.marketamerica.automation.testdata.attributes.DistributorApplicationType;
import com.marketamerica.automation.testdata.attributes.DistributorType;
import com.marketamerica.automation.testdata.attributes.HongKongShippingRegion;
import com.marketamerica.automation.testdata.attributes.LanguageOptions;
import com.marketamerica.automation.testdata.attributes.ShopperClassification;
import com.marketamerica.automation.testdatalists.CreditCardList;
import com.marketamerica.automation.testdatalists.PreferredCustomerList;
import com.marketamerica.automation.testdatalists.ProductList;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Gender;
import com.marketamerica.automation.utilities.enums.TestClassification;
import com.marketamerica.automation.utilities.excel.ExcelWorkbookReader;
import com.marketamerica.automation.utilities.helpers.DateAndTimeUtilities;
import com.marketamerica.automation.utilities.helpers.RegularExpressions;
import com.marketamerica.automation.utilities.tax.FakeTaxIDGenerator;
import com.marketamerica.automationframework.tools.datamanagement.ExcelDataManager;
import com.marketamerica.automationframework.tools.datamanagement.XMLDataManager;

import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.marketamerica.automation.utilities.helpers.CollectionUtilities.createCaseInSensitiseMap;

/**
 * The Excel Data Provider provides Excel Data as a part of a TestNG Iteration
 * <p>
 * It expects the same folder hierarchy as that of the DataManager to organize
 * test data.
 *
 * @author Javier L. Velasquez
 * @see com.marketamerica.automationframework.tools.datamanagement.XMLDataManager
 */
public class ExcelDataDrivenProvider extends AbstractDataProvider {
    private final static String CUSTOM_PARAMETERS = "testData.xlsx";

    @DataProvider(name = "addressShoppersIdsAndDescription")
    public static Object[][] getAddressesShoppersIdsAndDescription(final ITestContext context, final Method method) {

        // Create a data manager based on our current context
        final XMLDataManager xmlDataManager = createXMLDataManager(context);
        final PreferredCustomerList shoppersXMLList = xmlDataManager.getShopperList();
        final ProductList productsXMLList = xmlDataManager.getProductList();

        final CreditCardList creditCardsXMLList = xmlDataManager.getCreditCardList();

        final ExcelDataManager excelDataManager = createExcelManager(context);
        final ExcelWorkbookReader excelWorkbookTestDataParser = excelDataManager
                .getAddressesShoppersIdsAndDescription();

        // Find our rows based on the method name
        final List<Map<String, List<String>>> excelRows = excelWorkbookTestDataParser
                .getSheetDataAsMultiMap(method.getName());
        // Create our return object. Dimension one is sized based on the number
        // of rows.
        // This represents each iteration
        final Object[][] requestedAddresses = new Object[excelRows.size()][6];

        // Counter representing each row
        int position = 0;
        // For each row found the relevant address data
        for (final Map<String, List<String>> row : excelRows) {
            // Get the columns for the current data row
            final Set<String> columns = row.keySet();
            // We create a new address per row to initialize its data by column
            // header

            final List<Product> products = new ArrayList<>();
            final List<Shopper> shoppers = new ArrayList<>();
            CreditCard creditCard = new CreditCard();

            Map<String, String> customArguments = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);

            for (String column : columns) {
                String formattedColumn = column.trim();

                final List<String> columnValues = row.get(column);
                for (final String columnValue : columnValues) {
                    switch (formattedColumn.toLowerCase()) {
                        case "product":
                            if (columnValue == null || columnValue.equals("NULL") || columnValue.isEmpty()) {
                                break;
                            }
                            products.add(productsXMLList.getProduct(columnValue));
                            break;

                        case "shopper":
                            if (columnValue == null || columnValue.equals("NULL") || columnValue.isEmpty()) {
                                break;
                            }
                            shoppers.add(shoppersXMLList.getShopper(columnValue));
                            break;

                        case "creditcard":
                            if (columnValue == null || columnValue.equals("NULL") || columnValue.isEmpty()) {
                                break;
                            }
                            creditCard = creditCardsXMLList.getCreditCard(columnValue);
                            break;

                        default:
                            if (!formattedColumn.equals("id") && !formattedColumn.equals("description")) {
                                customArguments.put(formattedColumn, columnValue);
                            }
                    }
                }
            }
            Collections.reverse(products);
            Collections.reverse(shoppers);

            requestedAddresses[position][0] = row.get("id").get(0);
            requestedAddresses[position][1] = row.get("description").get(0);
            requestedAddresses[position][2] = products;
            requestedAddresses[position][3] = shoppers;
            requestedAddresses[position][4] = customArguments;
            requestedAddresses[position][5] = creditCard;
            ++position;
        }
        return requestedAddresses;
    }

    /**
     * Checks to see if the user has disabled regression at the test suite level. In most instances this should
     * return {@code true}
     *
     * @param context a testng test context.
     * @return {@code false} if the the {@code regression} parameter has been set to false at either the {@code <test>
     * } or {@code <suite> } level, otherwise return {@code true}
     */
    private static boolean isRegressionEnabled(ITestContext context) {
        Map<String, String> testGroupParameters = context.getCurrentXmlTest().getAllParameters();
        boolean regression = false;
        // If the user has set regression to false set regression to false
        if (testGroupParameters.containsKey("regression") && testGroupParameters.get("regression").equalsIgnoreCase("false")) {
            regression = false;
        } else {
            Map<String, String> suiteParameters = context.getSuite().getXmlSuite().getParameters();
            if (!(suiteParameters.containsKey("regression") && suiteParameters.get("regression").equalsIgnoreCase
                    ("false"))) {
                regression = true;
            }
        }
        return regression;
    }

    /**
     * Determine whether or not to skip the row. It will check to see if regression and end to end tests should be
     * disabled.
     *
     * @param context a testng test context.
     * @param cell    a excel data cell that is identified by the "Classification" column.
     * @return {@code true} if regression tests are disabled and the cell value does not equal to {@code
     * TestClassification.SMOKE}, else return {@return false}
     */
    public static boolean skipRow(ITestContext context, String cell) {
        if (!isRegressionEnabled(context)) {
            if (!TestClassification.parse(cell).equals
                    (TestClassification
                            .SMOKE)) {
                return true;
            }
        } else {
            return false;
        }
        return false;
    }

    /**
     * Check to see if the HashSet keys includes the specified value
     *
     * @param keySet a key set
     * @param value  a value to search for
     * @return {@code true} if the keyset included the value, {@code false} if the keyset doesn't include the value
     */
    private static boolean includesValue(Set<String> keySet, String value) {
        return keySet.contains(value);
    }

    @DataProvider(name = "customParameters")
    public static Object[][] getCustomParameters(final ITestContext context, final Method method) {
        // Create a data manager based on our current context
        final ExcelDataManager excelDataManager = createExcelManager(context);
        final ExcelWorkbookReader excelWorkbookTestDataParser = excelDataManager.getWorkBook(CUSTOM_PARAMETERS);
        // Find our rows based on the method name
        final List<Map<String, List<String>>> excelRows = excelWorkbookTestDataParser
                .getSheetDataAsMultiMap(method.getName());
        // Create our return object. Dimension one is sized based on the number
        // of rows.
        // This represents each iteration
        Object[][] customParameters = new Object[excelRows.size()][1];
        // Counter representing each row
        int position = 0;
        // For each row found the relevant address data
        for (final Map<String, List<String>> row : excelRows) {
            // Get the columns for the current data row
            final Set<String> columns = row.keySet();
            boolean skipRow = false;

            // If the iteration doesn't specify the classification column, add the test's classification as regression
            if (!includesValue(columns, "Classification")) {
                row.put("Classification", Arrays.asList(TestClassification.REGRESSION.toString()));
            }
            // We create a new address per row to initialize its data by column
            // header
            Map<String, String> customArguments = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);
            for (String column : columns) {
                String formattedColumn = column.trim();
                final List<String> columnValues = row.get(column);
                for (final String columnValue : columnValues) {
                    if (column.trim().equalsIgnoreCase("classification")) {
                        if (skipRow(context, columnValue)) {
                            skipRow = true;
                            break;
                        }
                    } else if (columnValue == null || columnValue.equals("NULL") || columnValue.isEmpty()) {
                        break;
                    }
                    customArguments.put(formattedColumn, columnValue);
                }
                if (skipRow) {
                    break;
                }
            }
            if (skipRow) {
                Object[][] shrunkArray = new Object[customParameters.length - 1][1];
                System.arraycopy(customParameters, 0, shrunkArray, 0, customParameters.length - 1);
                customParameters = shrunkArray;
            } else {
                customParameters[position][0] = customArguments;
                ++position;
            }
        }
        return customParameters;
    }

    @DataProvider(name = "shoppers")
    public static Object[][] getShoppers(final ITestContext context, final Method method) {

        // Create a data manager based on our current context
        final XMLDataManager xmlDataManager = createXMLDataManager(context);
        final PreferredCustomerList shoppersXMLList = xmlDataManager.getShopperList();
        final ExcelDataManager excelDataManager = createExcelManager(context);

        final ExcelWorkbookReader excelWorkbookTestDataParser = excelDataManager.getWorkBook("shoppers.xlsx");

        // Find our rows based on the method name
        final List<Map<String, List<String>>> excelRows = excelWorkbookTestDataParser
                .getSheetDataAsMultiMap(method.getName());
        // Create our return object. Dimension one is sized based on the number
        // of rows.
        // This represents each iteration
        Object[][] requestedAddresses = new Object[excelRows.size()][2];

        // Counter representing each row
        int position = 0;
        // For each row found the relevant address data
        for (final Map<String, List<String>> row : excelRows) {
            // Get the columns for the current data row
            final Set<String> columns = row.keySet();

            boolean skipRow = false;

            // If the iteration doesn't specify the classification column, add the test's classification as regression
            if (!includesValue(columns, "Classification")) {
                row.put("Classification", Arrays.asList(TestClassification.REGRESSION.toString()));
            }

            Shopper shopper = null;

            Map<String, String> customArguments = createCaseInSensitiseMap();

            for (String column : columns) {
                String formattedColumn = column.trim();
                final List<String> columnValues = row.get(column);
                for (final String columnValue : columnValues) {
                    if (column.trim().equalsIgnoreCase("classification")) {
                        if (skipRow(context, columnValue)) {
                            skipRow = true;
                            break;
                        }
                    } else if (columnValue == null || columnValue.equals("NULL") || columnValue.isEmpty()) {
                        break;
                    }

                    switch (formattedColumn.toLowerCase()) {
                        case "shopper":
                            shopper = shoppersXMLList.getShopper(columnValue);
                            break;
                        default:
                            customArguments.put(formattedColumn, columnValue);
                    }
                }
            }
            if (skipRow) {
                Object[][] shrunkArray = new Object[requestedAddresses.length - 1][2];
                System.arraycopy(requestedAddresses, 0, shrunkArray, 0, shrunkArray.length);
                requestedAddresses = shrunkArray;
            } else {
                requestedAddresses[position][0] = shopper;
                requestedAddresses[position][1] = customArguments;
                ++position;
            }
        }
        return requestedAddresses;
    }

    @DataProvider(name = "addresses")
    public static Object[][] getAddresses(final ITestContext context, final Method method) {

        // Use the Excel Data Manager to load login information via Excel
        final ExcelDataManager excelDataManager = createExcelManager(context);

        final ExcelWorkbookReader excelWorkbookTestDataParser = excelDataManager.getAddressExcelWorkbookParser();

        // Find our rows based on the method name
        final List<Map<String, String>> excelRows = excelWorkbookTestDataParser.getSheetData(method.getName());
        // Create our return object. Dimension one is sized based on the number
        // of rows.
        // This represents each iteration
        final Object[][] requestedAddresses = new Object[excelRows.size()][1];

        // Counter representing each row
        int position = 0;
        // For each row found the relevant address data
        for (final Map<String, String> row : excelRows) {
            // Get the columns for the current data row
            final Set<String> columns = row.keySet();
            // We create a new address per row to initialize its data by column
            // header
            final Address address = new Address();
            for (String column : columns) {
                String formattedColumn = column.toLowerCase().trim();

                final String columnValue = row.get(column);
                setAddressValueByColumn(address, formattedColumn, columnValue);

            }
            requestedAddresses[position][0] = address;
            ++position;
        }
        return requestedAddresses;
    }

    @DataProvider(name = "invalid_login")
    public static Object[][] getInvalidLogins(final ITestContext context, final Method method) {

        // Use the Excel Data Manager to load login information via Excel
        final ExcelDataManager excelDataManager = createExcelManager(context);

        final ExcelWorkbookReader loginWorkbook = excelDataManager.getLoginExcelWorkbookParser();
        List<String> sheetNames = loginWorkbook.getSheetNames();

        int numRows = 0;

        for (String sheetName : sheetNames) {
            // Find our rows based on sheet name
            final List<Map<String, String>> excelRows = loginWorkbook.getSheetData(sheetName);
            numRows += excelRows.size();
        }

        // Create our return object. Dimension one is sized based on the number
        // of total rows. This represents all iterations combined
        // Column 1 is a Distributor log in and
        // column 2 is key for error
        // message expected to appear upon login attempt
        final Object[][] requestedLogins = new Object[numRows][2];

        // Counter representing each row
        int rowPosition = 0;

        for (String sheetName : sheetNames) {
            // Find our rows based on sheet name
            final List<Map<String, String>> excelRows = loginWorkbook.getSheetData(sheetName);

            // For each row found the relevant address data
            for (final Map<String, String> row : excelRows) {
                final Distributor distributor = new Distributor();
                // Get the columns for the current data row
                final Set<String> columns = row.keySet();

                for (String column : columns) {
                    String formattedColumn = column.toLowerCase().trim();

                    String columnValue = row.get(column).trim();

                    switch (formattedColumn) {
                        case "userid": {
                            distributor.setRepresentativeId(columnValue);
                            break;
                        }
                        case "password": {
                            distributor.setPassword(columnValue);
                            break;
                        }
                        case "errormessagekey": {
                            requestedLogins[rowPosition][1] = columnValue;
                            break;
                        }
                        default:
                            break;
                    }
                }
                requestedLogins[rowPosition][0] = distributor;
                ++rowPosition;
            }
        }
        return requestedLogins;
    }

    @DataProvider(name = "ShopperProductAndOther")
    public static Object[][] getShopperProductAndOther(final ITestContext context, final Method method) {

        final ExcelDataManager excelDataManager = createExcelManager(context);
        final ExcelWorkbookReader excelWorkbookTestDataParser = excelDataManager
                .getWorkBook("ShopperProductAndOther.xlsx");

        // Find our rows based on the method name
        final List<Map<String, List<String>>> excelRows = excelWorkbookTestDataParser
                .getSheetDataAsMultiMap(method.getName());
        // Create our return object. Dimension one is sized based on the number
        // of rows.
        // This represents each iteration
        Object[][] objects = new Object[excelRows.size()][3];

        // Counter representing each row
        int position = 0;

        // For each row found the relevant address data
        for (final Map<String, List<String>> row : excelRows) {
            // Get the columns for the current data row
            final Set<String> columns = row.keySet();
            boolean skipRow = false;
            // If the iteration doesn't specify the classification column, add the test's classification as regression
            if (!includesValue(columns, "Classification")) {
                row.put("Classification", Arrays.asList(TestClassification.REGRESSION.toString()));
            }
            Shopper shopper = new Shopper();
            Address primaryAddress = createAddress(row, "Address::country");
            Product product = new Product();
            Portal portal = new Portal();
            Distributor distributor = new Distributor();
            Map<String, String> customArguments = createCaseInSensitiseMap();

            for (String column : columns) {
                String formattedColumn = column.trim().toLowerCase();
                final List<String> columnValues = row.get(column);
                for (final String columnValue : columnValues) {
                    if (column.trim().equalsIgnoreCase("classification")) {
                        if (skipRow(context, columnValue)) {
                            skipRow = true;
                            break;
                        }
                    } else if (columnValue == null || columnValue.equals("NULL") || columnValue.isEmpty()) {
                        break;
                    }
                    if (formattedColumn.startsWith("shopper")) {
                        setShopperValueByColumn(shopper, formattedColumn, columnValue);
                    } else if (formattedColumn.startsWith("product")) {
                        setProductValueByColumn(product, formattedColumn, columnValue);
                    } else if (formattedColumn.startsWith("address")) {
                        setAddressValueByColumn(primaryAddress, formattedColumn, columnValue);
                    } else if (formattedColumn.startsWith("portal")) {
                        setPortalValueByColumn(portal, formattedColumn, columnValue);
                    }else if (formattedColumn.startsWith("distributor")) {
                    	 setDistributorValueByColumn(distributor, formattedColumn, columnValue);
                    }else {
                        customArguments.put(column.trim(), columnValue);
                    }
                }
            }

            if (skipRow) {
                Object[][] shrunkArray = new Object[objects.length - 1][3];
                System.arraycopy(objects, 0, shrunkArray, 0, objects.length - 1);
                objects = shrunkArray;
            } else {
                if (!shopper.hasClassification()) {
                    shopper.setClassification(ShopperClassification.PREFERRED_CUSTOMER);
                }
                shopper.setAddress(primaryAddress);
                shopper.setPortal(portal);
                shopper.setDistributor(distributor);
                // Setting freight to true by default
                product.setFreight(true);

                objects[position][0] = shopper;
                objects[position][1] = product;
                objects[position][2] = customArguments;
                ++position;
            }
        }
        return objects;

    }

    @DataProvider(name = "ShopperCreditProductAndOther")
    public static Object[][] getShopperCreditCardProductAndOther(final ITestContext context, final Method method) {

        final ExcelDataManager excelDataManager = createExcelManager(context);
        final ExcelWorkbookReader excelWorkbookTestDataParser = excelDataManager.getWorkBook("ShopperAndOther.xlsx");

        // Find our rows based on the method name
        final List<Map<String, List<String>>> excelRows = excelWorkbookTestDataParser
                .getSheetDataAsMultiMap(method.getName());
        // Create our return object. Dimension one is sized based on the number
        // of rows.
        // This represents each iteration
        Object[][] objects = new Object[excelRows.size()][4];

        // Counter representing each row
        int position = 0;

        // For each row found the relevant address data
        for (final Map<String, List<String>> row : excelRows) {
            // Get the columns for the current data row
            final Set<String> columns = row.keySet();
            boolean skipRow = false;
            // If the iteration doesn't specify the classification column, add the test's classification as regression
            if (!includesValue(columns, "Classification")) {
                row.put("Classification", Arrays.asList(TestClassification.REGRESSION.toString()));
            }
            Shopper shopper = new Shopper();
            CreditCard card = new CreditCard();
            Distributor distributor = new Distributor();
            Portal portal = new Portal();
            Address primaryAddress = createAddress(row, "Address::country");
            Product product = new Product();

            Map<String, String> customArguments = createCaseInSensitiseMap();

            for (String column : columns) {
                String formattedColumn = column.trim().toLowerCase();
                final List<String> columnValues = row.get(column);
                for (final String columnValue : columnValues) {
                    if (column.trim().equalsIgnoreCase("classification")) {
                        if (skipRow(context, columnValue)) {
                            skipRow = true;
                            break;
                        }
                    } else if (columnValue == null || columnValue.equals("NULL") || columnValue.isEmpty()) {
                        break;
                    }
                    if (formattedColumn.startsWith("shopper")) {
                        setShopperValueByColumn(shopper, formattedColumn, columnValue);
                    } else if (formattedColumn.startsWith("product")) {
                        setProductValueByColumn(product, formattedColumn, columnValue);
                    } else if (formattedColumn.startsWith("portal")) {
                        setPortalValueByColumn(portal, formattedColumn, columnValue);
                    } else if (formattedColumn.startsWith("distributor")) {
                        setDistributorValueByColumn(distributor, formattedColumn, columnValue);
                    } else if (formattedColumn.startsWith("address")) {
                        setAddressValueByColumn(primaryAddress, formattedColumn, columnValue);
                    } else if (formattedColumn.startsWith("creditcard")) {
                        setCreditCardValueByColumn(card, formattedColumn, columnValue);
                    } else {
                        customArguments.put(column.trim(), columnValue);
                    }
                }
            }

            if (skipRow) {
                Object[][] shrunkArray = new Object[objects.length - 1][4];
                System.arraycopy(objects, 0, shrunkArray, 0, objects.length - 1);
                objects = shrunkArray;
            } else {
                if (!shopper.hasClassification()) {
                    shopper.setClassification(ShopperClassification.PREFERRED_CUSTOMER);
                }
                shopper.setAddress(primaryAddress);
                shopper.setPortal(portal);
                shopper.setDistributor(distributor);

                // Setting freight to true by default
                product.setFreight(true);

                objects[position][0] = shopper;
                objects[position][1] = card;
                objects[position][2] = product;
                objects[position][3] = customArguments;
                ++position;
            }
        }
        return objects;

    }

    private static void setCreditCardValueByColumn(CreditCard card, String column, String value) {
        switch (column) {
            case "creditcard::type": {
                card.setType(value);
                break;
            }
            case "creditcard::number": {
                card.setNumber(value);
                break;
            }
            case "creditcard::expirationmonth": {
                card.setExpirationMonth(Integer.valueOf(value));
                break;
            }
            case "creditcard::expirationyear": {
                card.setExpirationYear(Integer.valueOf(value));
                break;
            }
            case "creditcard::cvv": {
                card.setCvv(Integer.valueOf(value));
                break;
            }
            default: {
                break;
            }
        }
    }


	@DataProvider(name = "DistributorCreationProcess")
	public static Object[][] getDistributorCreationData(
			final ITestContext context, final Method method) {

		final ExcelDataManager excelDataManager = createExcelManager(context);
		final ExcelWorkbookReader excelWorkbookTestDataParser = excelDataManager
				.getWorkBook("DistributorCreationProcess.xlsx");

		// Find our rows based on the method name
		final List<Map<String, List<String>>> excelRows = excelWorkbookTestDataParser
				.getSheetDataAsMultiMap(method.getName());
		// Create our return object. Dimension one is sized based on the number
		// of rows.
		// This represents each iteration
		Object[][] objects = new Object[excelRows.size()][2];

		// Counter representing each row
		int position = 0;

		// For each row found the relevant address data
		for (final Map<String, List<String>> row : excelRows) {
			// Get the columns for the current data row
			final Set<String> columns = row.keySet();
			final DistributorType distributorType;
			boolean skipRow = false;
			// If the iteration doesn't specify the classification column, add
			// the test's classification as regression
			if (!includesValue(columns, "Classification")) {
				row.put("Classification",
						Arrays.asList(TestClassification.REGRESSION.toString()));
			}

			if (!row.containsKey("DistributorType")) {
				distributorType = DistributorType.UNFRANCHISE_OWNER;
			} else {
				distributorType = DistributorType.parse(row.get(
						"DistributorType").get(0));
				row.remove(row.get("DistributorType"));
			}
			Country distributorCountry = null;
			if (row.containsKey("Address::country"))
				distributorCountry = Country.parse(row.get("Address::country")
						.get(0));

			Boolean checkGroupName = false;
			if (row.containsKey("TestGroupName")
					&& getTestGroupName(context) != null) {
				checkGroupName = true;
			}

			Merchandise merchandise = new FastStartKit();
			Product product = new Product();
			Address address = Address.createAddress(distributorCountry);
			DistributorApplication distributorApplication = new DistributorApplication(
					distributorType);
			Distributor distributor = distributorApplication.getDistributor();
			Distributor sponsoringDistributor = distributorApplication
					.getSponsoringDistributor();
			Map<String, String> customArguments = createCaseInSensitiseMap();

			for (String column : columns) {
                String formattedColumn = column.trim().toLowerCase();
                final List<String> columnValues = row.get(column);

                for (final String columnValue : columnValues) {
                    if (column.trim().equalsIgnoreCase("classification")) {
                        if (skipRow(context, columnValue)) {
                            skipRow = true;
                            break;
                        }
                    } else if (checkGroupName == true
                            && column.trim().equalsIgnoreCase("testgroupname")) {
                        if (!columnValue.trim().equalsIgnoreCase(
                                getTestGroupName(context))) {
                            skipRow = true;
                            break;
                        }
                    } else if (columnValue == null || columnValue.equals("NULL") || columnValue.isEmpty()) {
                        break;
                    }
                    if (formattedColumn.startsWith("distributor")) {
                        setDistributorValueByColumn(distributor,
                                formattedColumn, columnValue);
                        if (formattedColumn.contains("applicationtype") || formattedColumn.contains("sublanguage"))
                            setDistributorApplicationValueByColumn(
                                    distributorApplication, formattedColumn,
                                    columnValue);
                    } else if (formattedColumn
                            .startsWith("sponsoringdistributor")) {
                        setDistributorValueByColumn(sponsoringDistributor,
                                formattedColumn.replace("sponsoring", ""),
                                columnValue);
                    } else if (formattedColumn.startsWith("address")) {
                        setAddressValueByColumn(address, formattedColumn,
                                columnValue);
                    } else if (formattedColumn.startsWith("merchandise")) {
                        setMerchandiseValueByColumn(merchandise,
                                formattedColumn, columnValue);
                    } else if (formattedColumn.startsWith("product")) {
                        setProductValueByColumn(product, formattedColumn,
                                columnValue);
                    } else if (formattedColumn.startsWith("process")) {
                        setProcessValueByColumn(distributorApplication, formattedColumn,
                                columnValue);
                    } else {
                        customArguments.put(column, columnValue);
                    }
				}
			}

			if (skipRow) {
				Object[][] shrunkArray = new Object[objects.length - 1][2];
				System.arraycopy(objects, 0, shrunkArray, 0, objects.length - 1);
				objects = shrunkArray;

			} else {
				distributor.setAddress(address);
				distributor.randomizeAttributes();
				distributor.setGender(Gender.getRandom());
				distributor.setTaxID(FakeTaxIDGenerator.generate(address
						.getCountryAsConstant()));

				distributorApplication.addMerchandise(merchandise);
				distributorApplication.addProduct(product);
				objects[position][0] = distributorApplication;
				objects[position][1] = customArguments;
				++position;
			}
		}
		return objects;
	}

    private static void setProcessValueByColumn(DistributorApplication distributorApplication, String column, String value) {
        switch (column) {
            case "process::optinautorenewal": {
                distributorApplication.setOptInAutoRenewal(Integer.parseInt(value));
                break;
            }
            case "process::optoutautorenewal": {
                distributorApplication.setOptOutAutoRenewal(Integer.parseInt(value));
                break;
            }
            case "process::completegsw": {
                distributorApplication.setCompletedGSW(Integer.parseInt(value));
                break;
            }
            case "process::placeirc": {
                distributorApplication.setPlaceIRC(Integer.parseInt(value));
                break;
            }
            case "process::ircregion": {
                distributorApplication.setIrcRegion(value);
                break;
            }
            case "process::ircplacement": {
                distributorApplication.setIrcPlacement(value);
                break;
            }
            case "process::ircplacementleg": {
                distributorApplication.setIrcPlacementLeg(value);
                break;
            }
            case "process::uamexpansion": {
                distributorApplication.setUamExpansion(Integer.parseInt(value));
                break;
            }
            case "process::uamexpandedcountry": {
                distributorApplication.setUamExpandedCountry(value);
                break;
            }
            case "process::uamexpansionbdc": {
                distributorApplication.setUamExpansionBDC(value);
                break;
            }
            case "process::uapexpansion": {
                distributorApplication.setUapExpansion(Integer.parseInt(value));
                break;
            }
            case "process::uapexpandedcountry": {
                distributorApplication.setUapExpandedCountry(value);
                break;
            }
            case "process::uapexpansionbdc": {
                distributorApplication.setUapExpansionBDC(value);
                break;
            }
            case "process::ueuexpansion": {
                distributorApplication.setUeuExpansion(Integer.parseInt(value));
                break;
            }
            case "process::ueuexpandedcountry": {
                distributorApplication.setUeuExpandedCountry(value);
                break;
            }
            case "process::ueuexpandedbdc": {
                distributorApplication.setUeuExpansionBDC(value);
                break;
            }
            case "process::setupshopportal": {
                distributorApplication.setSetUpShopPortal(Integer.parseInt(value));
                break;
            }
            case "process::setuphpportal": {
                distributorApplication.setSetUpHPPortal(Integer.parseInt(value));
                break;
            }
            case "process::customminiusa": {
                distributorApplication.setCustomMiniUSA(Integer.parseInt(value));
                break;
            }
            case "process::customminican": {
                distributorApplication.setCustomMiniCAN(Integer.parseInt(value));
                break;
            }
            case "process::customminimex": {
                distributorApplication.setCustomMiniMEX(Integer.parseInt(value));
                break;
            }
            default:
                break;
        }
    }

    private static void setMerchandiseValueByColumn(Merchandise merchandise, String column, String value) {
        switch (column) {
            case "merchandise::name": {
                merchandise.setName(value);
                break;
            }
            case "merchandise::sku": {
                merchandise.setSku(value);
                break;
            }
            case "merchandise::retailCost": {
                merchandise.setRetailCost(new BigDecimal(value));
                break;
            }
            default: {
                break;
            }
        }
    }

    /**
     * Create a new address instanced based on a country column entry provided
     * in a test data row iteration
     *
     * @param row        a test data row object
     * @param columnName a column name describing a Address's country
     * @return a new instance of Address corresponding to the Country
     */
    private static Address createAddress(final Map<String, List<String>> row, final String columnName) {
        if (row.containsKey(columnName)) {
            Country country = Country.parse(row.get(columnName).get(0));
            return Address.createAddress(country);
        } else {
            return Address.createAddress(Country.NOT_CLASSIFIED);
        }
    }

    @DataProvider(name = "ShopperAndOther")
    public static Object[][] getShopperAndOther(final ITestContext context, final Method method) {
        final ExcelDataManager excelDataManager = createExcelManager(context);
        final ExcelWorkbookReader excelWorkbookTestDataParser = excelDataManager
                .getWorkBook("ShopperAndOther.xlsx");

        // Find our rows based on the method name
        final List<Map<String, List<String>>> excelRows = excelWorkbookTestDataParser
                .getSheetDataAsMultiMap(method.getName());
        // Create our return object. Dimension one is sized based on the number
        // of rows.
        // This represents each iteration
        Object[][] objects = new Object[excelRows.size()][2];

        // Counter representing each row
        int position = 0;

        // For each row found the relevant address data
        for (final Map<String, List<String>> row : excelRows) {
            // Get the columns for the current data row
            final Set<String> columns = row.keySet();
            boolean skipRow = false;
            // If the iteration doesn't specify the classification column, add the test's classification as regression
            if (!includesValue(columns, "Classification")) {
                row.put("Classification", Arrays.asList(TestClassification.REGRESSION.toString()));
            }
            Shopper shopper = new Shopper();
            Portal portal = new Portal();
            Distributor distributor = new Distributor();
            Address address = createAddress(row, "Address::country");
            List<Product> products = new ArrayList<>();

            Map<String, String> customArguments = createCaseInSensitiseMap();

            for (String column : columns) {
                String formattedColumn = column.trim().toLowerCase();
                final List<String> columnValues = row.get(column);
                for (final String columnValue : columnValues) {
                    if (column.trim().equalsIgnoreCase("classification")) {
                        if (skipRow(context, columnValue)) {
                            skipRow = true;
                            break;
                        }
                    } else if (columnValue == null || columnValue.equals("NULL") || columnValue.isEmpty()) {
                        break;
                    }
                    if (formattedColumn.startsWith("shopper")) {
                        setShopperValueByColumn(shopper, formattedColumn, columnValue);
                    } else if (formattedColumn.startsWith("distributor")) {
                        setDistributorValueByColumn(distributor, formattedColumn, columnValue);
                    } else if (formattedColumn.startsWith("portal")) {
                        setPortalValueByColumn(portal, formattedColumn, columnValue);
                    } else if (formattedColumn.startsWith("address")) {
                        setAddressValueByColumn(address, formattedColumn, columnValue);
                    } else {
                        customArguments.put(column, columnValue);
                    }

                }
            }

            if (skipRow) {
                Object[][] shrunkArray = new Object[objects.length - 1][2];
                System.arraycopy(objects, 0, shrunkArray, 0, objects.length - 1);
                objects = shrunkArray;
            } else {

                // Setting all products to freight true
                products.forEach(product -> product.setFreight(true));
                if (!shopper.hasClassification()) {
                    shopper.setClassification(ShopperClassification.PREFERRED_CUSTOMER);
                }
                products.stream().filter(product -> product.getStore() == null)
                        .forEach(product -> product.setStore(Store.getMarketAmerica()));
                shopper.setDistributor(distributor);
                shopper.setPortal(portal);
                shopper.setAddress(address);

                objects[position][0] = shopper;
                objects[position][1] = customArguments;
                ++position;
            }
        }
        return objects;

    }

    @DataProvider(name = "ShopperAdditionalAddressProductsAndOther")
    public static Object[][] getShopperAdditionalAddressProductsAndOther(final ITestContext context,
                                                                         final Method method) {

        final ExcelDataManager excelDataManager = createExcelManager(context);
        final ExcelWorkbookReader excelWorkbookTestDataParser = excelDataManager
                .getWorkBook("ShopperAdditionalAddressProductsAndOther.xlsx");

        // Find our rows based on the method name
        final List<Map<String, List<String>>> excelRows = excelWorkbookTestDataParser
                .getSheetDataAsMultiMap(method.getName());
        // Create our return object. Dimension one is sized based on the number
        // of rows.
        // This represents each iteration
        Object[][] objects = new Object[excelRows.size()][4];

        // Counter representing each row
        int position = 0;

        // For each row found the relevant address data
        for (final Map<String, List<String>> row : excelRows) {
            // Get the columns for the current data row
            final Set<String> columns = row.keySet();
            boolean skipRow = false;
            // If the iteration doesn't specify the classification column, add the test's classification as regression
            if (!includesValue(columns, "Classification")) {
                row.put("Classification", Arrays.asList(TestClassification.REGRESSION.toString()));
            }

            Shopper shopper = new Shopper();
            Portal portal = new Portal();
            Distributor distributor = new Distributor();
            Address primaryAddress = createAddress(row, "PrimaryAddress::country");
            Address secondaryAddress = createAddress(row, "SecondaryAddress::country");
            List<Product> products = new ArrayList<>();

            Map<String, String> customArguments = createCaseInSensitiseMap();

            for (String column : columns) {
                String formattedColumn = column.trim().toLowerCase();
                final List<String> columnValues = row.get(column);
                for (final String columnValue : columnValues) {
                    if (column.trim().equalsIgnoreCase("classification")) {
                        if (skipRow(context, columnValue)) {
                            skipRow = true;
                            break;
                        }
                    } else if (columnValue == null || columnValue.equals("NULL") || columnValue.isEmpty()) {
                        break;
                    }
                    if (formattedColumn.startsWith("shopper")) {
                        setShopperValueByColumn(shopper, formattedColumn, columnValue);
                    } else if (formattedColumn.startsWith("distributor")) {
                        setDistributorValueByColumn(distributor, formattedColumn, columnValue);
                    } else if (formattedColumn.startsWith("portal")) {
                        setPortalValueByColumn(portal, formattedColumn, columnValue);
                    } else if (formattedColumn.startsWith("product")) {
                        int productPosition = Integer.parseInt(RegularExpressions.matchFirstPattern(
                                formattedColumn, "\\d+"));
                        Product product;
                        if (products.size() == 0 || (products.size() - 1) < productPosition) {
                            products.add(new Product());
                        }
                        product = products.get(productPosition);
                        setProductValueByColumn(product, formattedColumn.replaceAll("\\d", ""), columnValue);
                    } else if (formattedColumn.startsWith("primaryaddress")) {
                        setAddressValueByColumn(primaryAddress, formattedColumn.replace("primary", ""),
                                columnValue);
                    } else if (formattedColumn.startsWith("secondaryaddress")) {
                        setAddressValueByColumn(secondaryAddress, formattedColumn.replace("secondary", ""),
                                columnValue);
                    } else {
                        customArguments.put(column, columnValue);
                    }

                }
            }
            if (skipRow) {
                Object[][] shrunkArray = new Object[objects.length - 1][4];
                System.arraycopy(objects, 0, shrunkArray, 0, objects.length - 1);
                objects = shrunkArray;
            } else {

                // Setting all products to freight true
                products.forEach(product -> product.setFreight(true));
                if (!shopper.hasClassification()) {
                    shopper.setClassification(ShopperClassification.PREFERRED_CUSTOMER);
                }
                products.stream().filter(product -> product.getStore() == null)
                        .forEach(product -> product.setStore(Store.getMarketAmerica()));
                shopper.setDistributor(distributor);
                shopper.setPortal(portal);
                shopper.setAddress(primaryAddress);

                objects[position][0] = shopper;
                objects[position][1] = secondaryAddress;
                objects[position][2] = products;
                objects[position][3] = customArguments;
                ++position;
            }
        }
        return objects;

    }

    @DataProvider(name = "DistributorProductAndAddress")
    public static Object[][] getDistributorProductAndAddress(final ITestContext context,
                                                                         final Method method) {
        final ExcelDataManager excelDataManager = createExcelManager(context);
        final ExcelWorkbookReader excelWorkbookTestDataParser = excelDataManager
                .getWorkBook("DistributorProductAndAddress.xlsx");

        // Find our rows based on the method name
        final List<Map<String, List<String>>> excelRows = excelWorkbookTestDataParser
                .getSheetDataAsMultiMap(method.getName());
        // Create our return object. Dimension one is sized based on the number
        // of rows.
        // This represents each iteration
        Object[][] objects = new Object[excelRows.size()][3];

        // Counter representing each row
        int position = 0;

        // For each row found the relevant address data
        for (final Map<String, List<String>> row : excelRows) {
            // Get the columns for the current data row
            final Set<String> columns = row.keySet();
            boolean skipRow = false;
            // If the iteration doesn't specify the classification column, add the test's classification as regression
            if (!includesValue(columns, "Classification")) {
                row.put("Classification", Arrays.asList(TestClassification.REGRESSION.toString()));
            }
            Distributor distributor = new Distributor();
            Product product = new Product();
            Address address = new Address();

            Map<String, String> customArguments = createCaseInSensitiseMap();
            for (String column : columns) {
                String formattedColumn = column.trim().toLowerCase();
                final List<String> columnValues = row.get(column);
                for (final String columnValue : columnValues) {
                    if (column.trim().equalsIgnoreCase("classification")) {
                        if (skipRow(context, columnValue)) {
                            skipRow = true;
                            break;
                        }
                    } else if (columnValue == null || columnValue.equals("NULL") || columnValue.isEmpty()) {
                        break;
                    }
                    if (formattedColumn.startsWith("distributor")) {
                        setDistributorValueByColumn(distributor, formattedColumn, columnValue);
                    } else if (formattedColumn.startsWith("product")) {
                        setProductValueByColumn(product, formattedColumn, columnValue);
                    } else if (formattedColumn.startsWith("address")){
                        setAddressValueByColumn(address, formattedColumn, columnValue);
                    }else {
                        customArguments.put(column, columnValue);
                    }
                }
            }
          
            if (skipRow) {
            	 Object[][] shrunkArray = new Object[objects.length - 1][3];
                 System.arraycopy(objects, 0, shrunkArray, 0, objects.length - 1);
                 objects = shrunkArray;
            } else {
            	   distributor.setAddress(address);
            	   objects[position][0] = distributor;
                   objects[position][1] = product;
                   objects[position][2] = customArguments;
                   ++position;
            }
        }

        return objects;
    }

    // get group name from the test parameters for the current test
    private static String getTestGroupName(final ITestContext context) {        
        Map<String, String> parameters = context.getCurrentXmlTest().getAllParameters();
        
        if (parameters.get("groupName") != null) {
        	return parameters.get("groupName");
        } else {
        	return null;       
        }
    }
    
    
    

    /**
     * This method sets the address value based on a column value
     *
     * @param address address object to set the value
     * @param column  the column used to determine the value
     * @param value   the value itself used to describe the address
     */
    private static void setAddressValueByColumn(final Address address, final String column, final String value) {
        switch (column) {
            case "address::id": {
                address.setId(value);
                break;
            }
            case "address::street": {
                address.setStreet(value);
                break;
            }
            case "address::city": {
                address.setCity(value);
                break;
            }
            case "address::state": {
                address.setState(value);
                break;
            }
            case "address::stateabbreviation": {
                address.setStateAbbreviation(value);
                break;
            }
            case "address::zip": {
                address.setZipCode(value);
                break;
            }
            case "address::country": {
                address.setCountry(value);
                break;
            }
            case "address::neighborhood": {
                if (address instanceof MexicanAddress) {
                    ((MexicanAddress) address).setNeighborhood(value);
                }
                break;
            }
            case "address::district": {
                if (address instanceof MexicanAddress) {
                    ((MexicanAddress) address).setDistrict(value);
                } else if (address instanceof HongKongAddress) {
                    ((HongKongAddress) address).setDistrict(value);
                }
                break;
            }
            case "address::region": {
                ((HongKongAddress) address).setRegion(HongKongShippingRegion.parse(value));
                break;
            }
            case "address::flat": {
                ((HongKongAddress) address).setFlat(value);
                break;
            }
            case "address::floor": {
                ((HongKongAddress) address).setFloor(value);
                break;
            }
            case "address::building": {
                ((HongKongAddress) address).setBuilding(value);
                break;
            }
            default: {
                break;
            }
        }
    }

    private static void setPortalValueByColumn(Portal portal, String column, String value) {
        switch (column) {
            case "portal::id": {
                portal.setId(value);
                break;
            }
            case "portal::freeshipping": {
                portal.setFreeShipping(Boolean.valueOf(value));

                break;
            }
            case "portal::minimumshippingcharge": {
                portal.setMinimumShippingCharge(new BigDecimal(value));
                break;
            }
            case "portal::shippingcharge": {
                portal.setShippingCharge(new BigDecimal(value));
                break;
            }
            case "portal::shopconsultantname": {
                portal.setShopConsultantName(value);
                break;
            }
            case "portal::portalname": {
                portal.setPortalName(value);
                break;
            }
            default: {
                break;
            }
        }

    }

    private static void setDistributorValueByColumn(Distributor distributor, String column, String value) {
        switch (column) {
            case "distributor::id": {
                distributor.setId(value);
                break;
            }
            case "distributor::distributorid": {
                distributor.setDistributorId(value);
                break;
            }
            case "distributor::itransact": {
                distributor.setITransact(Boolean.valueOf(value));
                break;
            }
            case "distributor::locale": {
                String[] locales = value.split("_");
                distributor.setLocale(new Locale(locales[0], locales[1]));
                break;
            }
            case "distributor::dateofbirth": {
                SimpleDateFormat dateFormat = new SimpleDateFormat("EEE MMM d kk:mm:ss ZZZ yyyy");
                distributor.setDateOfBirth(DateAndTimeUtilities.getFormattedDate(value, dateFormat));
                break;
            }
            case "distributor::firstname": {
                distributor.setFirstName(value);
                break;
            }
            case "distributor::lastname": {
                distributor.setLastName(value);
                break;
            }
            case "distributor::email": {
                distributor.setEmail(value);
                break;
            }
            case "distributor::phonenumber": {
                distributor.setPhoneNumber(value);
                break;
            }

            case "distributor::password": {
                distributor.setPassword(value);
                break;
            }
            case "distributor::representativeid": {
                distributor.setRepresentativeId(value);
                break;
            }
            case "distributor::representativepassword": {
                distributor.setRepresentativePassword(value);
                break;
            }
            case "distributor:address:country": {
                Address address;
                if (distributor.getAddress() == null) {
                    address = new Address();
                } else {
                    address = distributor.getAddress();
                }
                address.setCountry(value);
                distributor.setAddress(address);

                break;
            }
            default: {
                break;
            }
        }

    }

    private static void setDistributorApplicationValueByColumn(DistributorApplication distributorApplication, String column, String value) {
        switch (column) {
            case "distributor::applicationtype": {
                distributorApplication.setApplicationType(DistributorApplicationType.parse(value));
                break;
            }
            case "distributor::sublanguage": {
                distributorApplication.setSubscriptionKitLanguage(LanguageOptions.parse(value));
                break;
            }
            case "distributor::preferredlanguage": {
                distributorApplication.setPreferredLanguage(LanguageOptions.parse(value));
                break;
            }
            default: {
                break;
            }
        }
    }


    private static void setShopperValueByColumn(Shopper shopper, String column, String value) {
        switch (column) {
            case "shopper::id": {
                shopper.setId(value);
                break;
            }
            case "shopper::pcid": {
                shopper.setPcID(value);
                break;
            }
            case "shopper::classification": {
                ShopperClassification classification = ShopperClassification.parse(value);
                shopper.setClassification(classification);
                break;
            }
            case "shopper::referralshopperclassification": {
                ShopperClassification classification = ShopperClassification.parse(value);
                shopper.setReferralShopperClassification(classification);
                break;
            }
            case "shopper::firstname": {
                shopper.setFirstName(value);
                break;
            }
            case "shopper::lastname": {
                shopper.setLastName(value);
                break;
            }
            case "shopper::password": {
                shopper.setPassword(value);
                break;
            }
            case "shopper::email": {
                shopper.setEmail(value);
                break;
            }
            case "shopper::phonenumber": {
                shopper.setPhoneNumber(value);
                break;
            }
            default: {
                break;
            }
        }
    }

    private static void setProductValueByColumn(Product product, String column, String value) {
        switch (column) {
            case "product::id": {
                product.setId(value);
                break;
            }
            case "product::productcontainerid": {
                product.setProductContainerId(value);
                break;
            }
            case "product::productid": {
                product.setProductId(value);
                break;
            }
            case "product::name": {
                product.setName(value);
                break;
            }
            case "product::sku": {
                product.setSku(value);
                break;
            }
            case "product::productCountry": {
                product.setProductCountry(Country.parse(value));
                break;
            }
            case "product::retailcost": {
                product.setRetailCost(value);
                break;
            }
            case "product::distributorcost": {
                product.setDistributorCost(value);
                break;
            }
            case "product::cashback": {
                product.setCashBack(value);
                break;
            }
            case "product::color": {
                product.setColor(value);
                break;
            }
            case "product::bv": {
                product.setProductBV(BigDecimal.valueOf(Double.valueOf(value)));
                break;
            }
            case "product::ibv": {
                product.setProductIBV(BigDecimal.valueOf(Double.valueOf(value)));
                break;
            }
            default: {
                break;
            }
        }

    }
}
