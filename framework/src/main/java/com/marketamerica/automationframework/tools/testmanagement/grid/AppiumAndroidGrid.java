package com.marketamerica.automationframework.tools.testmanagement.grid;

import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.testng.Reporter;

/**
 * The Appium Configurator for Android Devices to choose from a pool of
 * devices. Assuming that all these devices are connected to the Appium
 * Android Node The Android Devices Node IP is 10.99.215.70 The different
 * ports for Android are currently port- 4723, bootstrap port-4727 with no
 * device ID (works for any default device) and default chromedriver
 * port-9515 port - 4724 with bootstrap port - 4728 - device
 * "10.99.204.231:5555" (9be5b378) connected through Wifi with chrome driver
 * port 9516 port - 4725 with bootstrap port - 4729 - device "TA431090NM"(QA
 * device) , chromedriver port - 9517 port - 4726 with bootstrap port - 4730
 * , device "fa1d9afa", chromedriverport -9518
 */
public class AppiumAndroidGrid {
    //        private static final String HUB_URL = "http://10.99.215.70:4726/wd/hub";
    private static final String HUB_URL = "http://10.99.110.101:4444/wd/hub";
    private static final Logger logger = LogManager.getLogger(AppiumAndroidGrid.class);

    public static WebDriver getRemoteWebBrowser(final DesiredCapabilities capabilities) {
        try {
            logger.info(String.format("Creating Appium WebDriver instance using %s", capabilities));
            // TODO: Uncomment this once you have more devices to test to
            // set up device pooling. commenting until we have more devices.
            // Until then set parallel to false
            // validList = getValidAppiumAndroidPorts();
            // int randomIndex = new Random().nextInt(validList.size());
            // List<String> keys = new
            // ArrayList<String>(validList.keySet());
            // driverKey = keys.get(randomIndex).toString();
            // driverport = validList.get(driverKey);
            // validList.remove(driverKey);
            // logger.info("Android driver key and Port " + driverKey + ":"
            // + driverport);
            // if (driverport != null) {
            // return new AppiumDriver(new URL("http://10.99.215.70:" +
            // driverport.trim() + "/wd/hub"),
            // capabilities);
            // } else {
            return new AndroidDriver(new URL(HUB_URL), capabilities);
            // }
        } catch (MalformedURLException e) { 
            logger.fatal(String.format("%s was an invalid URL", HUB_URL), e);
        } catch (UnreachableBrowserException browserException) {
            logger.fatal(
                    String.format(
                            "Unable to reach the browser, Check if you have started the appium server at 10.99.215.70 and port %s. ",
                            HUB_URL), browserException);
        } catch (WebDriverException e) {
            logger.fatal(String
                    .format("Unable to create Appium WebDriver instance with given capabilities, \"%s\"",
                            capabilities), e);
        }
        Reporter.log(String.format("Invalid capabilities set. Something went wrong!!", HUB_URL));
        return null;
    }
}
