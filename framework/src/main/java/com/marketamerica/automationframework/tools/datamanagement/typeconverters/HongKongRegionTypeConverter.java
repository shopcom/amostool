package com.marketamerica.automationframework.tools.datamanagement.typeconverters;

import com.marketamerica.automation.testdata.attributes.HongKongShippingRegion;
import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

/**
 * Created by Javier L. Velasquez on 9/30/2014.
 */
public class HongKongRegionTypeConverter extends AbstractSingleValueConverter {

    @SuppressWarnings("rawtypes")
    @Override
    public boolean canConvert(Class clazz) {
        return HongKongShippingRegion.class.isAssignableFrom(clazz);
    }

    public Object fromString(String input) {
        return HongKongShippingRegion.parse(input);
    }

}
