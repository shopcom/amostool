package com.marketamerica.automationframework.tools.testmanagement.grid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * The Grid Configurator provides configuration for running on the Selenium
 * Grid for both desktop browsers and devices
 */
public class DesktopGrid {
    private static final String HUB_URL = "http://10.99.110.101:4444/wd/hub";
    private static final Logger logger = LogManager.getLogger(DesktopGrid.class);

    // Returns an instance of Remote WebDriver based on the input
    // DesiredCapabilities
    public static WebDriver getRemoteWebBrowser(final DesiredCapabilities capabilities) {
        try {
            logger.info(String.format("Creating Remote WebDriver instance using %s", capabilities));
            return new RemoteWebDriver(new URL(HUB_URL), capabilities);
        } catch (MalformedURLException e) {
            logger.fatal(String.format("%s was an invalid URL", HUB_URL), e);
        } catch (WebDriverException e) {
            logger.fatal(
                    String.format("Unable to create RemoteWebDriver instance with given capabilities, \"%s\"",
                            capabilities), e);
        }
        // If we got here something went wrong!
        return null;
    }
}
