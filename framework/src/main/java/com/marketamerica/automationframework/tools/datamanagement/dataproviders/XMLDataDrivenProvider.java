package com.marketamerica.automationframework.tools.datamanagement.dataproviders;

import com.marketamerica.automation.testdatalists.AbstractDataList;
import com.marketamerica.automation.testdatalists.AddressesList;
import com.marketamerica.automationframework.tools.datamanagement.XMLDataManager;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import java.lang.reflect.Method;
import java.util.List;

import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.getAddressKeys;


/**
 * The XML Data Provider provides XML Data as a part of a TestNG Iteration
 * It expects the same folder hierarchy as that of the DataManager to organize test data.
 *
 * @author Javier L. Velasquez
 * @see com.marketamerica.automationframework.tools.datamanagement.XMLDataManager
 */
public class XMLDataDrivenProvider extends AbstractDataProvider {

    @DataProvider(name = "addresses")
    public static Object[][] getAddresses(final ITestContext context,
                                          final Method method) {

        // Create a data manager based on our current context
        final XMLDataManager dataManager = createXMLDataManager(context);
        // Get the requested addresses by the DataDrivenTest annotation
        final List<String> requestedAddressKeys = getAddressKeys(method);
        // Return a list of addresses
        final AddressesList requestedAddressList = dataManager.getAddressList();
        // Get our requested Data
        final Object[][] requestedAddresses = getRequestedData(
                requestedAddressList, requestedAddressKeys);
        if (requestedAddresses[0] == null) {
            Assert.fail(String.format("Could not find any addresses for %s",
                    method.getName()));
        }
        return requestedAddresses;
    }

    // @DataProvider(name = "distributors")
    // public static Object[][] getDistributorsViaXML(final ITestContext
    // context, final Method method) {
    //
    // // Create a data manager based on our current context
    // final DataManager dataManager = createXMLDataManager(context);
    // // Get the requested addresses by the DataDrivenTest annotation
    // final List<String> requestedDistributorKeys = getDistributorKeys(method);
    // // Return a list of addresses
    // final AddressesList requestedDistributorList =
    // dataManager.getDistributorList();
    // // Get our requested Data
    // final Object[][] requestedDistributors =
    // getRequestedData(requestedDistributorList, requestedDistributorKeys);
    // if (requestedDistributors[0] == null) {
    // Assert.fail(String.format("Could not find any distributors for %s",
    // method.getName()));
    // }
    // return requestedDistributors;
    // }

    private static <T extends AbstractDataList> Object[][] getRequestedData(
            final T testDataList, final List<String> testDataKeys) {
        final Object[][] requestedTestData = new Object[testDataKeys.size()][1];

        for (int i = 0; i < requestedTestData.length; ++i) {
            final String currentAddressKey = testDataKeys.get(i);
            requestedTestData[i][0] = testDataList.get(currentAddressKey);
        }

        return requestedTestData;
    }


}
