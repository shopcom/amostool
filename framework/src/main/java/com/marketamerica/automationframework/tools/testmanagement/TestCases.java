package com.marketamerica.automationframework.tools.testmanagement;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.testdatalists.*;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.helpers.ReflectiveUtilities;
import com.marketamerica.automation.utilities.helpers.Translator;
import com.marketamerica.automation.utilities.webdriver.WebDriverBrowser;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.marketamerica.automationframework.elementmanagement.ElementDefinitionManager;
import com.marketamerica.automationframework.elementmanagement.PropertyElementDefinitionManager;
import com.marketamerica.automationframework.tools.datamanagement.XMLDataManager;
import com.marketamerica.automationframework.tools.factories.TranslatorFactory;
import com.marketamerica.automationframework.tools.reporting.ScreenShooter;
import com.marketamerica.automationframework.tools.reporting.StepInfo;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.*;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.marketamerica.automation.constants.GlobalConstants.MAX_TEST_EXECUTION_TIME;
import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.hasDataDrivenProvider;
import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.usesWebDriver;
import static com.marketamerica.automationframework.pages.WebContent.*;

/**
 * This class contains logic to perform test/suite configuration by tests
 * context. This class should be inherited by all test case classes. It allows
 * users to configure logging, remote execution, browser determination, web
 * driver initialization
 *
 * @author Javier L. Velasquez
 */
public abstract class TestCases extends RecordableTests {
    public static final int PAGE_LOAD_TIMEOUT = 60;
    public static final int IMPLICIT_WAIT_TIMEOUT = 15;

    /**
     * Perform any global configuration for a test case here
     */
    static {
        configureReporter();
    }

    protected AddressesList addresses;
    protected DistributorList distributors;
    protected PreferredCustomerList shoppers;
    protected CreditCardList creditCards;
    protected CustomMiniWebSiteList customMiniWebSites;
    protected PortalList portals;
    protected Locale locale;
    protected Environment environment;
    protected WebDriverUserAgent useragent;
    protected WebDriver driver;
    protected Translator translator;
    protected ProductList products;
    protected SuiteBuilder suiteBuilder;
    protected TestBuilder testBuilder;
    protected XMLDataManager dataManager;
    protected TranslatorFactory translatorFactory;
    protected ElementDefinitionManager elementDefinitionManager;
    protected Country country;
    private boolean captureScreenShot = true;

    /**
     * Configure the HTML Reporter. Currently it disables escaping html in the
     * TestNGReporter and ReportNG Reporter.
     */
    private static void configureReporter() {
        Reporter.setEscapeHtml(false);
        System.setProperty("org.uncommons.reportng.escape-output", "false");
    }

    /**
     * This method will load the parameters for each test group.
     *
     * @param context
     */
    @BeforeTest(alwaysRun = true)
    public void setupTest(final ITestContext context) {
        // testConfigurator = new TestConfigurator(context);

        // // If no version number is defined, don't add it to the xml test
        // group name.
        // if (testConfigurator.getDesiredCapabilities().getVersion() != null) {
        // String currentXmlTestName =
        // context.getCurrentXmlTest().getProjectName();
        // currentXmlTestName = String.format("%s (v. %s)",
        // currentXmlTestName, testConfigurator
        // .getDesiredCapabilities().getVersion());
        //
        // context.getCurrentXmlTest().setName(currentXmlTestName);
        // }
    }

    /**
     * This method lets us configure our tests based on parameters specified at
     * the suite tag level.It will only initialize the test suite once. It uses
     * the synchronized modifier for thread safety, and exits its operation if
     * the suite has already been configured. This allows us to thread at a test
     * level safely.
     *
     * @param context the current test context. It is used to extract test suite
     *                parameters
     */
    private synchronized void configureSuite(final ITestContext context) {
        if (suiteBuilder == null) {
            suiteBuilder = createSuiteBuilder(context);
            // log the current xml test suite
            logger.info(String.format("Configured the suite %s", context.getCurrentXmlTest().getSuite().getName()));
            environment = suiteBuilder.getEnvironment();
            useragent = suiteBuilder.getUserAgent();
            initialProjectConfiguration();
            Environment.setCurrent(environment);
            WebDriverUserAgent.setCurrent(useragent);
            OrderingReporter.initialize(environment, country);
            PreferredCustomerReporter.initialize(environment);
            DistributorReporter.initialize(environment);
            translatorFactory = new TranslatorFactory();
        }
    }

    protected SuiteBuilder createSuiteBuilder(ITestContext context) {
        return new SuiteBuilder(context);
    }

    /**
     * This method configures each test suite. It will determine the logger
     * level and whether or not to run remotely. Additionally, this method will
     * load test data
     *
     * @param context
     */
    @BeforeSuite(alwaysRun = true)
    public void beforeSuite(final ITestContext context) {

        for (ITestNGMethod method : context.getAllTestMethods()) {
            StepInfo.getAndResetStepInfoCount();
            // We don't add retry analyzer to data driven tests since its not
            // reasonably feasible to one in that context
            if (!hasDataDrivenProvider(method.getConstructorOrMethod().getMethod())) {
                method.setRetryAnalyzer(new RetryAnalyzer());
            } else {
                logger.warn(String.format(
                        "Not adding retry analyzer to '%s' (in '%s') since its a data driven test", method
                                .getConstructorOrMethod().getName(), method.getXmlTest().getName()));
            }

            // If the method doesn't already have a annotation level invocation
            // timeout set a global one
            if (method.getInvocationTimeOut() == 0 && !hasDataDrivenProvider(method.getConstructorOrMethod().getMethod())) {
                method.setTimeOut(MAX_TEST_EXECUTION_TIME);
            }
        }
    }

    @AfterTest(alwaysRun = true)
    public void afterTest() {
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuite() {
        OrderingReporter.generateReport();
        PreferredCustomerReporter.generateReport();
        DistributorReporter.generateReport();
        SuiteBuilder.deleteTempDrivers();
    }

    /**
     * This method initializes the web driver for each test.
     * Configures Page Load and Deletes Cookies
     *
     * @param method
     */
    @BeforeMethod(alwaysRun = true)
    public void setupMethod(final ITestContext context, final Method method, final ITestResult result) {
        try {
            final String methodName = method.getName();
            StepInfo.getAndResetStepInfoCount();
            testBuilder = createTestBuilder(context);
            Reporter.setCurrentTestResult(result);
            addSummaryToReport(method);

            configureSuite(context);
            if (testBuilder.getLocale() != null) {
                locale = testBuilder.getLocale();
            } else {
                locale = suiteBuilder.getLocale();
            }
            translator = getTranslator();

            dataManager = createXMLDataManager();

            Configuration.setCountry(Country.parse(locale.getDisplayCountry()));
            Configuration.setLocale(locale);

            country = Country.parse(locale.getDisplayCountry());

            if (usesWebDriver(method)) {
                this.driver = testBuilder.getWebDriver(suiteBuilder.isRemote(),
                        suiteBuilder.getMachineName());
            }

            elementDefinitionManager = getPropertyElementDefinitionManager();
            if (driver != null) {
                configureDriver();
                logger.info(String.format("Starting test %s in %s test suite", methodName,
                        this.testBuilder.getTestName()));
            } else {
                if (usesWebDriver(method)) {
                    logger.fatal(String.format(
                            "Unable to create webdriver instance for test: %s using capabilities: %s", methodName,
                            testBuilder.getDesiredCapabilities()));
                }
            }
        } catch (RuntimeException e) {
            logger.fatal(e);
        }
    }

    /**
     * Add a summary of a test method to the TestNG Reporter output
     *
     * @param method a test method object
     */
    private void addSummaryToReport(final Method method) {

        if (ReflectiveUtilities.hasTestDocumentationAnnotation(method)) {
            Reporter.log(String.format("<div style=\"color:grey;\"><strong>%s summary</strong></div>",
                    method.getName()));
            if (!ReflectiveUtilities.hasDataDrivenProvider(method)) {
                Reporter.log(String.format("<div style=\"color:grey;padding-left:1em;\">id: \"%s\"</div>",
                        ReflectiveUtilities.getMethodExternalId(method)));
            }
            Reporter.log(String.format("<div style=\"color:grey;padding-left:1em;\">version: \"%s\"</div>",
                    ReflectiveUtilities.getMethodVersion(method)));
            if (ReflectiveUtilities.getMethodRequirements(method).length != 0) {
                Reporter.log(String.format("<div style=\"color:grey;padding-left:1em;\">requirements: %s</div>",
                        Arrays.toString(ReflectiveUtilities.getMethodRequirements(method))));
            }
            if (!ReflectiveUtilities.getMethodDescription(method).isEmpty()) {
                Reporter.log(String.format("<div style=\"color:grey;padding-left:1em;\">description: \"%s\"</div>",
                        ReflectiveUtilities.getMethodDescription(method)));
            }

            final List<String> outstandingJiraDefects = ReflectiveUtilities.getOutstandingJiraDefects(method);
            if (outstandingJiraDefects != null && outstandingJiraDefects.size() > 0) {
                Reporter.log(String.format("<div style=\"color:grey;padding-left:1em;\">outstanding Jira Defects: \"%s\"</div>",
                        outstandingJiraDefects));
            }
            if (ReflectiveUtilities.getSupportedCountries(method) != null) {
                Reporter.log(String.format(
                        "<div style=\"color:grey;padding-left:1em;\">supported countries: \"%s\"</div>",
                        ReflectiveUtilities.getSupportedCountries(method)));
            }

            if (ReflectiveUtilities.getSupportedEnvironments(method) != null) {
                Reporter.log(String.format(
                        "<div style=\"color:grey;padding-left:1em;\">supported environments: \"%s\"</div>",
                        ReflectiveUtilities.getSupportedEnvironments(method)));
            }
        }
    }

    /**
     * Configure WebDriver specific settings such as the implicit wait and max
     * page load. Currently Safari doesn't support page timeouts or deleting
     * cookies (at least reliably). So, Page Load Timeout and Cookies deletion
     * configuration is not performed for Safari browser.
     */
    protected void configureDriver() {
        configurePageLoad(PAGE_LOAD_TIMEOUT);
        deleteAllCookies();
        // Maximize Window if platform is not null or platform is Mac for Safari.
        if ((getCapabilities(driver).getPlatform() != null || !getCapabilities(driver).getPlatform().equals(Platform.MAC))
                && !isSafariWebDriver(driver)) {
            driver.manage().window().maximize();
        }
    }

    /**
     * This method takes a screenshot on fail and exits the driver on finish
     *
     * @param result A test result
     * @param method The test method that was ran
     */
    @AfterMethod(alwaysRun = true)
    public void tearDownTest(final ITestResult result, final Method method) {
        logger.info(String.format("Ending test %s", method.getName()));
        StepInfo.getAndResetStepInfoCount();
        final int testStatus = result.getStatus();
        TestSummaryLogger.log(testStatus);
        if (driver != null) {
            if (this.captureScreenShot) {
                captureScreenshotIfTestFailed(result);
            }
        }
        if (usesWebDriver(method)) {
            try {
                driver.quit();
            } catch (WebDriverException e) {
                logger.error("Unable to close the browser!", e);
            }
        }
    }

    protected void captureScreenshotIfTestFailed(final ITestResult result) {
        final int testStatus = result.getStatus();
        if (testStatus == ITestResult.FAILURE) {
            logger.debug(String.format("%s failed (suite: \"%s\")", result.getMethod().getMethodName(), result
                    .getTestContext().getSuite().getName()));
            Reporter.setCurrentTestResult(result);
            ScreenShooter.capture(driver);
        }
    }

    /**
     * This method represents the base URL that the driver should navigate to.
     *
     * @return A string representing the base URL
     */
    protected abstract String getBaseURL();

    /**
     * Navigate to the Test Portal with shoppers Portal Key . The portal Name is
     * obtained from portals xml based on the shoppers Portal Key
     *
     * @param shopper
     */
    protected void navigateToTestPortal(Shopper shopper) {
        if (shopper.hasPortal()) {
            if (shopper.getPortal() != null) {
                String testPortalBaseURL = getBaseURL();
                if (!getBaseURL().endsWith("/")) {
                    testPortalBaseURL = testPortalBaseURL.concat("/");
                }
                logger.info(shopper.getPortal().getPortalName());
                testPortalBaseURL = testPortalBaseURL.concat(shopper.getPortal().getPortalName());
                logger.info(String.format("Navigating to %s", testPortalBaseURL));
                this.driver.navigate().to(testPortalBaseURL);
            } else {
                Reporter.log(String.format("Portal is not specified in the portals xml for portalKey %s",
                        shopper.getPortal()));
            }
        } else {
            logger.error(String.format(
                    "Cannot navigate to TestPortal . Portal Key for the Shopper %s , %s is not specified",
                    shopper.getFirstName(), shopper.getLastName()));
            Reporter.log("shopper does not have Portal");
        }
    }

    /**
     * returns WebDriverBrowser Name
     *
     * @return
     */
    protected WebDriverBrowser getBrowserName() {
        return WebDriverBrowser.fromString(((RemoteWebDriver) driver).getCapabilities().getBrowserName());
    }

    /**
     * pageLoadTimeout is set and implicitly wait for seconds for elements to be
     * loaded in DOM. For Safari browser , WebDriverException pageLoad Timeout
     * is thrown - in these instances implicitly wait for the specified amount
     * of time
     *
     * @param waitTime
     */
    protected void configurePageLoad(final int waitTime) {
        try {
            // There seems to be a chrome issue with page load. See
            // https://code.google.com/p/chromedriver/issues/detail?id=402
//            if (!isChromeWebDriver(driver) && !isInternetExplorerWebDriver(driver)) {
//                this.driver.manage().timeouts().pageLoadTimeout(waitTime, TimeUnit.SECONDS);
//            }
        	  if (!isFirefoxDriver(driver)){
        		  this.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        	  }// waiting
            // for
            // few
            // seconds for the elements to be loaded in DOM
        } catch (WebDriverException exception) {
            // for Safari - pageLoadTimeout is is not supported,currently.
            // sometimes fails in Chrome driver too.
            logger.warn(String.format("page Load Timeout not supported , Setting implicit wait to  %s time", waitTime));
            this.driver.manage().timeouts().implicitlyWait(IMPLICIT_WAIT_TIMEOUT, TimeUnit.SECONDS);
        }
    }

    /**
     * Delete All Cookies. Delete All Cookies is currently not supported in
     * Safari
     */
    protected void deleteAllCookies() {
        if (!getBrowserName().equals(WebDriverBrowser.SAFARI)) {
            this.driver.manage().deleteAllCookies();
        } else {
            logger.warn("Delete Cookies Not Supported");
        }
    }

    protected XMLDataManager createXMLDataManager() {
        return new XMLDataManager(environment, locale);
    }

    protected PropertyElementDefinitionManager getPropertyElementDefinitionManager() {
        return new PropertyElementDefinitionManager(environment, country);
    }

    protected Translator getTranslator() {
        return new Translator(testBuilder.getLocale());
    }

    protected void initialProjectConfiguration() {
        Configuration.initialize(environment);
    }

    protected void setCaptureScreenShot(boolean captureScreenShot) {
        this.captureScreenShot = captureScreenShot;
    }

    /**
     * Create a new instance of a test builder
     *
     * @param context a test context
     * @return a new test builder
     * @see com.marketamerica.automationframework.tools.testmanagement.TestBuilder
     */
    protected TestBuilder createTestBuilder(ITestContext context) {
        return new TestBuilder(context);
    }

    public String translate(String key) {
        return translator.translate(key);
    }

}
