package com.marketamerica.automationframework.tools.testmanagement.mobile;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.constants.GlobalConstants;
import com.marketamerica.automation.utilities.webdriver.*;
import com.marketamerica.automationframework.tools.testmanagement.TestBuilder;
import com.marketamerica.automationframework.tools.testmanagement.grid.AppiumAndroidGrid;
import com.marketamerica.automationframework.tools.testmanagement.grid.DesktopGrid;
import com.marketamerica.automationframework.tools.testmanagement.grid.LocalAppiumGrid;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.ITestContext;

import java.util.Map;


/**
 * Class for WebDriver on Mobile
 * 
 */
public class MobileTestBuilder extends TestBuilder {
    private static final Logger logger = LogManager.getLogger(MobileTestBuilder.class);

    public MobileTestBuilder(ITestContext context) {
        super(context);
    }

    public WebDriver getWebDriver(final boolean remote, final String applicationName) {
        if (driverType.getDeviceType() == null)
            driverType.setDeviceType(WebDriverDeviceType.DESKTOP);
        logger.info(String.format("Setting capabilities for %s , remote being set to  %s", driverType
                .getDeviceType().toString(), remote));
        // if deviceType is not desktop , use Appium configuration.
        if (!remote && !driverType.getDeviceType().equals(WebDriverDeviceType.DESKTOP)) {
            // start Appium locally since remote is set to false on -
            // http://127.0.0.1:4723/wd/hub
            capabilities = getDesiredCapabilities(driverType);
            return LocalAppiumGrid.getRemoteWebBrowser(capabilities);
        } else if (!driverType.getDeviceType().equals(WebDriverDeviceType.DESKTOP)) {
            capabilities = getDesiredCapabilities(driverType);
            if (capabilities.getPlatform().equals(Platform.ANDROID)) {
                return AppiumAndroidGrid.getRemoteWebBrowser(capabilities);
            } else {
                return DesktopGrid.getRemoteWebBrowser(capabilities);
            }
        } else {
            throw new IllegalStateException("Attempted to create a remote webdriver for the desktop, but using " +
                    "MobileTestBuilder to initialize driver");
        }
    }

    /**
     * Get Desired Capabilities for Devices - phone , tablet Capabilities -
     * platformName, deviceName , platform are mandatory for Appium.
     *
     * @param driverType
     * @return
     */
    private DesiredCapabilities getDesiredCapabilities(WebDriverType driverType) {
        DesiredCapabilities capabilities;
        if (driverType.getBrowser() == null
                && driverType.getDriverDevice().getDeviceOS().trim().equalsIgnoreCase("android")) {
            // considering the latest android version as default for android
            // Phone
            driverType.setDeviceBrowserType(WebDriverDeviceBrowserType.ANDROID_PHONE_CHROME_V44);
        } else if (driverType.getBrowser().toString().equalsIgnoreCase("safari")
                && driverType.getDriverDevice() == null) {
            // considering the latest IOS as default for iphone
            driverType.setDeviceBrowserType(WebDriverDeviceBrowserType.IPHONE_SAFARI_V71);
        } else if (driverType.getBrowser() == null
                && driverType.getDriverDevice().getDeviceOS().trim().equalsIgnoreCase("IOS")) {
            driverType.setDeviceBrowserType(WebDriverDeviceBrowserType.IPHONE_SAFARI_V71);
        } else {
            driverType.setDeviceBrowserType(WebDriverDeviceBrowserType.getDeviceBrowserType(driverType));
        }
        logger.info(String.format("Setting capabilities for driver - %s", driverType.getDeviceBrowserType()
                .toString()));
        switch (driverType.getDeviceBrowserType()) {
            case ANDROID_PHONE_CHROME_V44: {
                capabilities = DesiredCapabilities.android();
                capabilities.setCapability(CapabilityType.BROWSER_NAME, WebDriverBrowser.CHROME);
                capabilities.setVersion("4.4");
                capabilities.setCapability(GlobalConstants.Capability.PLATFORM_NAME, Platform.ANDROID.toString());
                capabilities.setCapability(GlobalConstants.Capability.DEVICE_NAME, "Samsung");
                capabilities.setPlatform(Platform.ANDROID);
                break;
            }
            case ANDROID_PHONE_ANDROID_V44: {
                capabilities = DesiredCapabilities.android();
                capabilities.setCapability(CapabilityType.BROWSER_NAME, WebDriverBrowser.NATIVE_ANDROID_BROWSER);
                capabilities.setVersion("4.4");
                capabilities.setCapability(GlobalConstants.Capability.PLATFORM_NAME, Platform.ANDROID.toString());
                capabilities.setCapability(GlobalConstants.Capability.DEVICE_NAME, "Samsung");
                capabilities.setPlatform(Platform.ANDROID);
                break;
            }
            case ANDROID_PHONE_CHROME_V43: {
                capabilities = DesiredCapabilities.android();
                capabilities.setCapability(CapabilityType.BROWSER_NAME, WebDriverBrowser.CHROME);
                capabilities.setVersion("4.3");
                capabilities.setCapability(GlobalConstants.Capability.PLATFORM_NAME, Platform.ANDROID.toString());
                capabilities.setCapability(GlobalConstants.Capability.DEVICE_NAME, "Samsung");
                capabilities.setPlatform(Platform.ANDROID);
                break;
            }
            case ANDROID_PHONE_ANDROID_V43: {
                capabilities = DesiredCapabilities.android();
                capabilities.setCapability(CapabilityType.BROWSER_NAME, WebDriverBrowser.NATIVE_ANDROID_BROWSER);
                capabilities.setVersion("4.3");
                capabilities.setCapability(GlobalConstants.Capability.PLATFORM_NAME, Platform.ANDROID.toString());
                capabilities.setCapability(GlobalConstants.Capability.DEVICE_NAME, "Samsung");
                capabilities.setPlatform(Platform.ANDROID);
                break;
            }
            case IPHONE_SAFARI_V71: {
                capabilities = DesiredCapabilities.iphone();
                capabilities.setCapability(CapabilityType.BROWSER_NAME, WebDriverBrowser.SAFARI);
                capabilities.setCapability(GlobalConstants.Capability.PLATFORM_NAME, "IOS");
                capabilities.setCapability(GlobalConstants.Capability.DEVICE_NAME, "iphone");
                capabilities.setPlatform(Platform.MAC);
                capabilities.setVersion("7.1");
                break;
            }
            case IPHONE_SAFARI_V70: {
                capabilities = DesiredCapabilities.iphone();
                // Capabilities - platformName, deviceName , platform are mandatory
                // for Appium.
                capabilities.setCapability(CapabilityType.BROWSER_NAME, WebDriverBrowser.SAFARI);
                capabilities.setCapability(GlobalConstants.Capability.PLATFORM_NAME, "iOS");
                capabilities.setCapability(GlobalConstants.Capability.DEVICE_NAME, "iphone");
                capabilities.setPlatform(Platform.MAC);
                capabilities.setVersion("7.0");
                break;
            }
            case ANDROID_PHONE_APP_V44: {
                capabilities = DesiredCapabilities.android();
                capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
                capabilities.setVersion("4.4");
                capabilities.setCapability(GlobalConstants.Capability.PLATFORM_NAME, Platform.ANDROID.toString());
                capabilities.setCapability(GlobalConstants.Capability.DEVICE_NAME, "Samsung");
                capabilities.setCapability(GlobalConstants.Capability.DEVICE, "android");
                if (Configuration.getAppPackage() != null && Configuration.getAppActivity() != null) {
                    capabilities.setCapability(GlobalConstants.Capability.APP_PACKAGE, Configuration.getAppPackage());
                    capabilities
                            .setCapability(GlobalConstants.Capability.APP_ACTIVITY, Configuration.getAppActivity());
                } else {
                    logger.fatal("App Package and App Activity are not specified in project.properties for testing an App");
                    throw new IllegalArgumentException(
                            "App Package and App Activity need to be mentioned in project.properties for testing an App");
                }
                capabilities.setPlatform(Platform.ANDROID);
                break;
            }
            case ANDROID_PHONE_APP_V43: {
                capabilities = DesiredCapabilities.android();
                capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
                capabilities.setVersion("4.3");
                capabilities.setCapability(GlobalConstants.Capability.PLATFORM_NAME, Platform.ANDROID.toString());
                capabilities.setCapability(GlobalConstants.Capability.DEVICE_NAME, "Samsung");
                capabilities.setCapability(GlobalConstants.Capability.DEVICE, "android");
                if (Configuration.getAppPackage() != null && Configuration.getAppActivity() != null) {
                    capabilities.setCapability(GlobalConstants.Capability.APP_PACKAGE, Configuration.getAppPackage());
                    capabilities
                            .setCapability(GlobalConstants.Capability.APP_ACTIVITY, Configuration.getAppActivity());
                } else {
                    logger.fatal("App Package and App Activity are not specified in project.properties for testing an App");
                    throw new IllegalArgumentException(
                            "App Package and App Activity need to be mentioned in project.properties for testing an App");
                }
                capabilities.setCapability(GlobalConstants.Capability.AUTOMATION_NAME, "selendroid");
                // Appium uses Selendroid under the hood for webview support on
                // devices older than 4.4. (In that case, you'll want to specify
                // "automationName": "selendroid" as a desired capability).
                capabilities.setPlatform(Platform.ANDROID);
                break;
            }
            default: {
                throw new IllegalArgumentException(String.format(
                        "Not Supported Currently - device %s %s in browser  %s , version - %s", driverType
                                .getDriverDevice().getDevice(), driverType.getDriverDevice().getDeviceOS(), driverType
                                .getDeviceBrowserType().getBrowser(), driverType.getVersion()));
            }
        }
        return capabilities;
    }

    @Override
    protected WebDriverType getWebDriverConfiguration(Map<String, String> parameters) {
        WebDriverType mobileConfiguration = super.getWebDriverConfiguration(parameters);

        String deviceTypeParam = parameters.get("deviceType");
        final WebDriverDeviceType deviceType = WebDriverDeviceType.fromString(deviceTypeParam);
        mobileConfiguration.setDeviceType(deviceType);
        String deviceOS = parameters.get("deviceOS");
        // deviceOS would be null if not run on devices.
        if (deviceOS != null) {
            final WebDriverDevice device = WebDriverDevice.getDevice(deviceTypeParam, deviceOS);
            mobileConfiguration.setDriverDevice(device);
        }
        return mobileConfiguration;
    }

}
