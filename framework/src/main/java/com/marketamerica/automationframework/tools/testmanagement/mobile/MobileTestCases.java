package com.marketamerica.automationframework.tools.testmanagement.mobile;

import com.marketamerica.automationframework.tools.testmanagement.SuiteBuilder;
import com.marketamerica.automationframework.tools.testmanagement.TestBuilder;
import com.marketamerica.automationframework.tools.testmanagement.TestCases;
import org.testng.ITestContext;

/**
 * Created by javierv on 11/17/2014.
 */
public abstract class MobileTestCases extends TestCases {

    /**
     * Create a new instance of a test builder. This overloaded version
     * will return a new instance of a MobileTestBuilder
     *
     * @param context a test context
     * @return a new MobileTestBuilder
     * @see com.marketamerica.automationframework.tools.testmanagement.mobile.MobileTestBuilder
     */
    @Override
    protected TestBuilder createTestBuilder(ITestContext context) {
        return new MobileTestBuilder(context);
    }

    @Override
    protected SuiteBuilder createSuiteBuilder(ITestContext context) {
        return new MobileSuiteBuilder(context);
    }


    @Override
    protected void configureDriver() {
        configurePageLoad(PAGE_LOAD_TIMEOUT);
    }

}
