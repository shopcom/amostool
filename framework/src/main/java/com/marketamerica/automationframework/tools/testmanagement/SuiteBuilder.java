package com.marketamerica.automationframework.tools.testmanagement;

import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.helpers.ProcessUtilities;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.ITestContext;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Map;

import static com.marketamerica.automation.constants.GlobalConstants.*;
import static com.marketamerica.automation.constants.GlobalConstants.TestNGSuiteParameters.ENVIRONMENT_STRING;
import static com.marketamerica.automation.constants.GlobalConstants.TestNGSuiteParameters.USERAGENT_STRING;

/**
 * Manages the configuration of suite level parameters. Currently it accounts
 * for the following parameters:
 * <ol>
 * <li>remote - determines whether or not to run on Selenium Grid, defaults to
 * false</li>
 * <li>loggerLevel - the level to run the logger, defaults to DEBUG</li>
 * <li>countryCode - used for setting the locale</li>
 * <li>languageCode - used for setting the locale</li>
 * </ol>
 */
public class SuiteBuilder extends LocalizedBuilder {

    public static final String DEFAULT_APPLICATION_NAME = "ma";
    private static final Level DEFAULT_LOGGER_LEVEL = Level.DEBUG;
    private static final Environment DEFAULT_ENVIRONMENT = Environment.STAGING;
    private static final WebDriverUserAgent DEFAULT_USERAGENT = WebDriverUserAgent.DEFAULT;
    private static final Logger logger = LogManager.getLogger(SuiteBuilder.class);
    private static File tempChromeDriver;
    private static File tempInternetExplorerDriver;
    private static boolean chromeAndIEDriversConfigured;
    private static boolean terminatedAnyStaleChromeAndIEDriverSessions;
    protected Map<String, String> suiteParameters;
    private boolean remote;
    private Environment environment;
    private WebDriverUserAgent useragent;
    private Level loggerLevel;
    private String machineName;

    public SuiteBuilder() {
    }

    public SuiteBuilder(final ITestContext context) {
        this.suiteParameters = context.getSuite().getXmlSuite().getAllParameters();
        configureSuite();
        // currently not configuring process for Devices and when running from
        // Mac OS . this needs to be enhanced.
        if (!isRemote()) {
            SuiteBuilder.terminateAnyStaleIEorChromeDriverSessions();
            SuiteBuilder.configureIEAndChromePaths();
        }
    }

    private synchronized static void terminateAnyStaleIEorChromeDriverSessions() {
        if (terminatedAnyStaleChromeAndIEDriverSessions) {
            return;
        }

        String[] arguments;
        if (ProcessUtilities.isProcessExistingByName(CHROMEDRIVER_EXE)) {
            arguments = new String[]{"taskkill", "/f", "/im", CHROMEDRIVER_EXE};
            ProcessUtilities.startProcess(arguments);
        } else {
            logger.trace("No existing session of chromedriver.exe found. No process terminated");
        }

        if (ProcessUtilities.isProcessExistingByName(IE_DRIVER_SERVER_EXE)) {
            arguments = new String[]{"taskkill", "/f", "/im", IE_DRIVER_SERVER_EXE};
            ProcessUtilities.startProcess(arguments);
        } else {
            logger.trace("No existing session of IEDriverServer.exe found. No process terminated");
        }

        assert !ProcessUtilities.waitForProcessToTerminateByName("taskkill.exe");

        terminatedAnyStaleChromeAndIEDriverSessions = true;
    }

    private synchronized static void configureIEAndChromePaths() {
        // if we have already configured the driver server executables,
        // just exit
        if (chromeAndIEDriversConfigured) {
            return;
        }
        final URL chromeDriverURL = TestBuilder.class.getResource("/" + CHROMEDRIVER_EXE);
        tempChromeDriver = new File(String.format("%s\\%s", SYSTEM_TEMP, CHROMEDRIVER_EXE));

        try {
            FileUtils.copyURLToFile(chromeDriverURL, tempChromeDriver);
        } catch (IOException e) {
            logger.error(e);
        }

        System.setProperty("webdriver.chrome.driver", tempChromeDriver.getAbsolutePath());

        final URL internetExplorerDriverURL = TestBuilder.class.getResource("/" + IE_DRIVER_SERVER_EXE);
        tempInternetExplorerDriver = new File(String.format("%s\\%s", SYSTEM_TEMP, IE_DRIVER_SERVER_EXE));

        try {
            FileUtils.copyURLToFile(internetExplorerDriverURL, tempInternetExplorerDriver);
        } catch (IOException e) {
            logger.error(e);
        }

        System.setProperty("webdriver.ie.driver", tempInternetExplorerDriver.getAbsolutePath());
        chromeAndIEDriversConfigured = true;
    }

    /**
     * Delete temp drivers to be used for local execution
     */
    public synchronized static void deleteTempDrivers() {
        if (tempChromeDriver != null && tempChromeDriver.exists())
            tempChromeDriver.delete();
        if (tempInternetExplorerDriver != null && tempInternetExplorerDriver.exists())
            tempInternetExplorerDriver.delete();
    }

    /**
     * Configure all suite level parameters. It will configure the following
     * options: remote, logger, country code, environment, language, locale;
     */
    protected void configureSuite() {
        configureMachineName();
        configureRemote();
        configureLogger();
        configureEnvironment();
        configureUserAgent();
        setLocale(createLocale(suiteParameters));
    }


    /**
     * Configure the environment. If the environment is not set, it will
     * default.
     */
    public void configureEnvironment() {
        if (suiteParameters.get(ENVIRONMENT_STRING) == null) {
            this.environment = DEFAULT_ENVIRONMENT;
        } else {
            this.environment = Environment.parse(suiteParameters.get(ENVIRONMENT_STRING));
        }
    }


    public Environment getEnvironment() {
        return this.environment;
    }
    
    public void configureUserAgent() {
        if (suiteParameters.get(USERAGENT_STRING) == null) {
            this.useragent = DEFAULT_USERAGENT;
        } else {
            this.useragent = WebDriverUserAgent.parse(suiteParameters.get(USERAGENT_STRING));
        }
        
    }

    public WebDriverUserAgent getUserAgent() {
        return this.useragent;
    }
    
    /**
     * Set whether or not testing should occur remotely. This configuration
     * should only occur once
     */
    private void configureRemote() {
        remote = Boolean.valueOf(suiteParameters.get("remote"));
    }

    /**
     * Set the Selenium Grid machine Name Node Parameter. You can use this if
     * you want to run tests on a specific machine. This is useful if you want
     * to run a customized environment
     */
    private void configureMachineName() {
        machineName = suiteParameters.get("machineName") != null ? String.valueOf(suiteParameters
                .get("machineName")) : "ma";
    }

    public String getMachineName() {
        return machineName;
    }

    public boolean isRemote() {
        return remote;
    }

    /**
     * Configure the logger level based on the TestNG Suite Parameter
     * "loggerLevel". It defaults to DEBUG
     */
    public void configureLogger() {
        loggerLevel = Level.toLevel(suiteParameters.get("loggerLevel"), DEFAULT_LOGGER_LEVEL);
        LogManager.getRootLogger().setLevel(loggerLevel);
    }

}
