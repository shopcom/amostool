package com.marketamerica.automationframework.tools.testmanagement;

import com.marketamerica.automation.constants.GlobalConstants;
import com.marketamerica.automation.utilities.webdriver.WebDriverBrowser;
import com.marketamerica.automation.utilities.webdriver.WebDriverDeviceType;
import com.marketamerica.automation.utilities.webdriver.WebDriverType;
import com.marketamerica.automation.utilities.webdriver.WebDriverUserAgent;
import com.marketamerica.automationframework.tools.testmanagement.grid.DesktopGrid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;
import org.testng.ITestContext;
import org.testng.Reporter;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static com.marketamerica.automation.constants.GlobalConstants.VERSION_MAC_SAFARI;
import static com.marketamerica.automation.constants.GlobalConstants.VERSION_WINDOWS_CHROME;

/**
 * Test Configurator allows you to configure configure test methods based on a
 * TestNG test context at the test level. It allows you to get different types
 * of webdrivers based on test suite parameters
 *
 * @author Javier L. Velasquez
 */

public class TestBuilder extends LocalizedBuilder {
    /**
     * The default browser for Selenium WebDriver is set to firefox
     */
    private static final WebDriverBrowser DEFAULT_BROWSER_DESKTOP = WebDriverBrowser.FIREFOX;
    private static final Logger logger = LogManager.getLogger(TestBuilder.class);
    protected String name;
    protected DesiredCapabilities capabilities;
    protected WebDriverType driverType;

    // TODO: Uncomment this once you have more devices to test to set up device
    // pooling. commenting until we have more devices. Until then set parallel
    // to false
    // private static HashMap<String, String> validList = new HashMap<String,
    // String>();
    // private static String AndriodDriverKey = "ANDROID_DRIVER1";
    // private static String AndroidDriverPort = DEFAULT_ANDROID_DRIVER_PORT;
    public TestBuilder(final ITestContext context) {
        setTestName(context.getCurrentXmlTest().getName());
        Map<String, String> parameters = context.getCurrentXmlTest().getAllParameters();
        setLocale(createLocale(parameters));
        driverType = getWebDriverConfiguration(parameters);

    }

    public TestBuilder(WebDriverBrowser browser) {
        Map<String, String> parameters = new HashMap<>();
        parameters.put("browser", browser.toString());
        parameters.put("applicationName", SuiteBuilder.DEFAULT_APPLICATION_NAME);
        driverType = getWebDriverConfiguration(parameters);
    }

    /**
     * Return a configured WebDriver based on an input DesiredCapabilities For
     * Desktop browsers
     *
     * @param capabilities a desired capabilities representing the configuration that we
     *                     want to have executed
     * @return A configured Webdriver
     */
    private static WebDriver getWebDriver(final DesiredCapabilities capabilities, WebDriverType driverType) {
        WebDriver driver;
        
        WebDriverBrowser browserName = WebDriverBrowser.fromString(capabilities.getBrowserName());
        WebDriverUserAgent useragent = driverType.getUserAgent();
  
        switch (browserName) {
            case HTML_UNIT: {
                driver = new HtmlUnitDriver(capabilities);
                break;
            }

            case FIREFOX: 
            {  
            	if (useragent != null)
            	{
            	    FirefoxProfile ffp = new FirefoxProfile(); 
            	    ffp.setPreference("general.useragent.override", useragent.getUserAgentPreference(useragent));
                	driver = new FirefoxDriver(ffp);                
            	}
            	
            	else
            	{
            		driver = new FirefoxDriver(capabilities);
            	}
                
                
                break;
            }
           
            case CHROME: {
                driver = new ChromeDriver(capabilities);
                break;
            }
            case INTERNET_EXPLORER:
            case IE: {
                driver = new InternetExplorerDriver(capabilities);
                break;
            }
            case SAFARI: {
                try {
                    driver = new SafariDriver(capabilities);
                } catch (IllegalStateException illegalStateException) {
                    logger.error("Safari Data Directory path is incorrect" + illegalStateException);
                    Reporter.log("Unable to set Safari Directory");
                    driver = null;
                }
                break;
            }
            // If we don't provide a valid one, throw an exception
            default: {
                throw new IllegalArgumentException(String.format("Invalid browser given: %s",
                        capabilities.getBrowserName()));
            }
        }

        if (driverType.getDimension() != null)
            driver.manage().window().setSize(driverType.getDimension());
        return driver;
    }

    public DesiredCapabilities getDesiredCapabilities() {
        return capabilities;
    }

    public String getTestName() {
        return this.name;
    }

    public void setTestName(final String name) {
        this.name = name;
    }

    /**
     * Return a configured WebDriver
     *
     * @param remote boolean indicating if the web driver should be an instance of
     *               Remote WebDriver
     * @return a webdriver based on the test suite's configuration
     */
    public WebDriver getWebDriver(final boolean remote) {
        return getWebDriver(remote, null);
    }

    public WebDriver getWebDriver(final boolean remote, final String applicationName) {
        // TODO : Many conditional statements , Need to write separate methods
        // if deviceType is not mentioned then the default device Type is
        // considered as Desktop
        if (driverType.getDeviceType() == null)
            driverType.setDeviceType(WebDriverDeviceType.DESKTOP);
        logger.info(String.format("Setting capabilities for %s , remote being set to  %s", driverType
                .getDeviceType().toString(), remote));

        capabilities = getDesiredCapabilities(driverType, applicationName);
        if (remote) {
            return DesktopGrid.getRemoteWebBrowser(capabilities);
        } else {
            return getWebDriver(capabilities, driverType);
        }

    }

    /**
     * This method returns the corresponding WebDriverType based on the given
     * TestNG Test XML Parameters
     *
     * @param parameters a map of string parameters extracted from a TestNG XML Test
     * @return A WebDriverType object based on the given TestNG Test XML
     * Parameters
     */
    protected WebDriverType getWebDriverConfiguration(Map<String, String> parameters) {
        WebDriverType driverType = new WebDriverType();
        final WebDriverBrowser browser = WebDriverBrowser.fromString(parameters.get("browser"));
        driverType.setBrowser(browser);
        Platform platform;
        if (parameters.get("platform") != null) {
            platform = Platform.valueOf(parameters.get("platform").toUpperCase());
            driverType.setPlatform(platform);
        }
        driverType.setVersion(getVersion(browser, parameters.get("version")));
        Dimension dimension = null;
        if (parameters.get("width") != null && parameters.get("height") != null)
            dimension = new Dimension(Integer.valueOf(parameters.get("width")), Integer.valueOf(parameters
                    .get("height")));
        driverType.setDimension(dimension);
        WebDriverUserAgent useragent;
        if (parameters.get("useragent") != null)
        {
        	useragent = WebDriverUserAgent.valueOf(parameters.get("useragent").toUpperCase());
        	driverType.setUserAgent(useragent);
        }
        return driverType;
    }

    /**
     * Get the appropriate web browser version.
     *
     * @param browser a WebDriverBrowser object representing the given browser based
     *                on the test parameter
     * @param version the "version" test parameter
     * @return
     */
    private String getVersion(final WebDriverBrowser browser, String version) {
        if (browser != null && version != null && version.equalsIgnoreCase("random")) {
            String[] validBrowserVersions = null;
            if (browser.equals(WebDriverBrowser.FIREFOX)) {
                validBrowserVersions = GlobalConstants.VALID_FIREFOX_VERSIONS;
            } else if (browser.equals(WebDriverBrowser.INTERNET_EXPLORER)) {
                validBrowserVersions = GlobalConstants.VALID_IE_VERSIONS;
            }
            version = validBrowserVersions[new Random().nextInt(validBrowserVersions.length)];
        }
        return version;

    }

    /**
     * This method creates a desired capabilities object as well as sets the
     * appropriate system properties for running IE or Chrome
     *
     * @param driverType
     * @param machineName the Selenium Grid applicationName node parameter. This
     *                    parameter allows you to run tests on a specific machine. This
     *                    is useful if you want to run tests in a customized environment
     *                    that is only provided by a specific VM. In order to utilize
     *                    this parameter, you will need to add the 'machineName' option
     *                    to the Node's configuration
     * @return a configured DesiredCapabilities instance
     */
    private DesiredCapabilities getDesiredCapabilities(WebDriverType driverType, final String machineName) {
        DesiredCapabilities capability;

        if (driverType.getBrowser() == null) {
            driverType.setBrowser(DEFAULT_BROWSER_DESKTOP);
        }
        
        switch (driverType.getBrowser()) {
            case FIREFOX: {
            	if (driverType.getUserAgent() != null)
            	{        
            	FirefoxProfile profile = new FirefoxProfile();
            	profile.setPreference("general.useragent.override", driverType.getUserAgent().getUserAgentPreference(driverType.getUserAgent()));
            	capability = DesiredCapabilities.firefox(); 
            	capability.setCapability(FirefoxDriver.PROFILE, profile); 
            	}
            	else
            	{
            		capability = DesiredCapabilities.firefox();
            	}
            	
            	break;
            }
            case HTML_UNIT: {
                capability = DesiredCapabilities.htmlUnit();
                break;
            }
            case CHROME: {
                capability = DesiredCapabilities.chrome();
                ChromeOptions options = new ChromeOptions();
                options.addArguments("test-type");// To remove message "You are
                // using an unsupported
                // command-line flag:
                // --ignore-certificate-errors.
                // Stability and security will suffer." Add an argument 'test-type'
                capability.setCapability(ChromeOptions.CAPABILITY, options);
                // Considering the default as Windows and setting latest version of
                // chrome
                if ((driverType.getPlatform() == null || driverType.getPlatform().equals(Platform.ANY))
                        && driverType.getVersion() == null) {
                    capability.setPlatform(Platform.WINDOWS);
                    capability.setVersion(VERSION_WINDOWS_CHROME);
                }
                break;
            }
            case SAFARI: {
                capability = DesiredCapabilities.safari();
                final SafariOptions safariOptions = new SafariOptions();
                safariOptions.setUseCleanSession(true);
                capability.setCapability(SafariOptions.CAPABILITY, safariOptions);
                // We decided not to run tests on Windows - Safari .
                capability.setPlatform(Platform.MAC);
                capability.setVersion(VERSION_MAC_SAFARI);
                break;
            }
            case IE: {
                driverType.setBrowser(WebDriverBrowser.INTERNET_EXPLORER);
            }// fall over - No break;
            case INTERNET_EXPLORER: {
                capability = DesiredCapabilities.internetExplorer();
                capability.setCapability("ignoreZoomSetting", true);
                capability.setCapability("ignoreProtectedModeSettings", true);
                break;
            }

            default: {
                throw new IllegalArgumentException(String.format("Invalid browser given: %s", driverType.getBrowser()));
            }
        }

        // set the updated browser name
        capability.setBrowserName(driverType.getBrowser().toString());
        // Only set the platform if we provided one, otherwise we default to
        // what is available
        if (driverType.getPlatform() != null) {
            capability.setPlatform(driverType.getPlatform());
        }

        // Only set a version if we provided one, otherwise we default to what
        // is available
        if (driverType.getVersion() != null)
            capability.setVersion(driverType.getVersion());

        if (machineName != null && !machineName.isEmpty())
            capability.setCapability("applicationName", machineName);
        return capability;
    }


}
