package com.marketamerica.automationframework.tools.factories;

import java.util.Locale;

/**
 * The built-in Locale class already creates many locale instances, however there are a few
 * common ones that are missing. This class seeks to support the Locale class by providing
 * those common Locale instances on demand.
 * <p>
 * Created by Javier L. Velasquez on 11/5/2014.
 *
 * @see java.util.Locale
 */
public class LocaleFactory {
    public static Locale getHongKongChinese() {
        return new Locale("zh", "HK");
    }
}
