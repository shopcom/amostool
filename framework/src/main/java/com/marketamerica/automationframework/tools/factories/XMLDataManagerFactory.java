package com.marketamerica.automationframework.tools.factories;

import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automationframework.tools.datamanagement.XMLDataManager;

import java.util.Locale;

/**
 * Data Manager Factory provides access to test data from multiple environments
 * and countries. It contains three fields representing environment factories
 *
 * @author javierv
 */
public final class XMLDataManagerFactory {
    public final Live liveEnvironmentFactory = new Live();
    public final Development developmentEnvironemntFactory = new Development();
    public final Staging stagingEnvironmentFactory = new Staging();

    /**
     * Create a data manager object
     *
     * @param language           locale specific language code (e.g. "en" or "sp")
     * @param countryAbreviation locale specific country code (e.g. "TW", or "US")
     * @param environment        Environment such as Development or Staging
     * @return a data manager fresh off the press
     */
    private static XMLDataManager createDataManager(final String language,
                                                    final String countryAbreviation, final Environment environment) {
        final Locale locale = new Locale(language, countryAbreviation);
        final XMLDataManager dataManager = new XMLDataManager(environment,
                locale);
        return dataManager;
    }

    /**
     * This class represents test data specific to Live
     *
     * @author javierv
     */
    public class Live extends Template {
        @Override
        protected Environment getEnvironment() {
            return Environment.LIVE;
        }

    }

    /**
     * This class represents test data specific to staging
     *
     * @author javierv
     */
    public class Staging extends Template {
        @Override
        protected Environment getEnvironment() {
            return Environment.STAGING;
        }

    }

    /**
     * This class represents test data specific to development
     *
     * @author javierv
     */
    public class Development extends Template {
        @Override
        protected Environment getEnvironment() {
            return Environment.DEVELOPMENT;
        }

    }

    /**
     * This class represents test data specific to PoC
     *
     * @author nspitzer
     */
    public class ProofOfConcept extends Template {
        @Override
        protected Environment getEnvironment() {
            return Environment.PROOF_OF_CONCEPT;
        }
    }

    private abstract class Template {
        protected abstract Environment getEnvironment();

        public XMLDataManager createUnitedStatesEnglishDataManager() {
            return createDataManager("en", "US", getEnvironment());
        }

        public XMLDataManager createUnClassifiedEnglishDataManager() {
            return createDataManager("en", Country.NOT_CLASSIFIED.getAbbreviation(), getEnvironment());
        }


        public XMLDataManager createTaiwanChineseDataManager() {
            return createDataManager("ch", "TW", getEnvironment());
        }

        public XMLDataManager createAustraliaEnglishDataManager() {
            return createDataManager("en", "AU", getEnvironment());

        }

        public XMLDataManager createHongKongChineseDataManager() {
            return createDataManager("zh", "HK", getEnvironment());
        }
    }

}
