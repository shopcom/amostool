package com.marketamerica.automationframework.tools.datamanagement;

import com.marketamerica.automation.utilities.database.TDCRSDatabase;
import com.marketamerica.automation.utilities.enums.Environment;
 
import java.util.Locale;

/**
 * Created by kathyg on 07/03/2015.
 */
public class SQLDataManager extends DataManager {
	
	private TDCRSDatabase tdcrsDatabase;    

    public SQLDataManager(Environment environment, Locale locale) {
        super(environment, locale);
    }
    
    public TDCRSDatabase getTDCRSDatabase() {
    	return tdcrsDatabase;
    }
     
}
