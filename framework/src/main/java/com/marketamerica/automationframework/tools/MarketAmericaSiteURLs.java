package com.marketamerica.automationframework.tools;

import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automationframework.exceptions.SiteURLNotImplemented;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * This class represents an abstract container for all urls in a site. It should
 * be document all sites per country and environment
 *
 * @author Javier L. Velasquez
 */
public abstract class MarketAmericaSiteURLs {
    final protected static Logger logger = LogManager
            .getLogger(MarketAmericaSiteURLs.class);
    protected Map<Country, String> liveSites = new HashMap<>();
    protected Map<Country, String> stagingSites = new HashMap<>();
    protected Map<Country, String> developmentSites = new HashMap<>();
    protected Map<Country, String> proofOfConceptSites = new HashMap<>();

    private static String getURLInMap(final Locale locale, final Map<Country, String> urls)
            throws SiteURLNotImplemented {
        switch (Country.parse(locale.getDisplayCountry())) {
            case UNITED_STATES:
                return urls.get(Country.UNITED_STATES);
            case AUSTRALIA:
                return urls.get(Country.AUSTRALIA);
            case UNITED_KINGDOM:
                return urls.get(Country.UNITED_KINGDOM);
            case TAIWAN:
                return urls.get(Country.TAIWAN);
            case CHINA:
                return urls.get(Country.CHINA);
            case HONG_KONG:
                return urls.get(Country.HONG_KONG);
            case CANADA:
                return urls.get(Country.CANADA);
            case MEXICO:
                return urls.get(Country.MEXICO);
            case SPAIN:
                return urls.get(Country.SPAIN);
            case SINGAPORE:
                return urls.get(Country.SINGAPORE);
            case NOT_CLASSIFIED:
                return urls.get(Country.NOT_CLASSIFIED);
            case EMP:
            	 return urls.get(Country.EMP);
            default:
                throw new SiteURLNotImplemented();
        }
    }

    /**
     * Determine the appropriate URL based on the environment and locale
     *
     * @param site        a Market America Site URLs implementation
     * @param environment an environment (e.g. Development, or Live)
     * @param locale      a locale object. This is used to return the locale's displayed
     *                    country.
     * @return a string representing the home page of the specified parameters
     */
    public static String determineURL(final MarketAmericaSiteURLs site,
                                      final Environment environment, final Locale locale) {
        try {
            switch (environment) {
                case PROOF_OF_CONCEPT: {
                    return getURLInMap(locale, site.proofOfConceptSites);
                }
                case DEVELOPMENT: {
                    return getURLInMap(locale, site.developmentSites);
                }
                case STAGING: {
                    return getURLInMap(locale, site.stagingSites);
                }
                case LTV: {
                    return getURLInMap(locale, site.liveSites);
                }
                case LIVE: {
                    return getURLInMap(locale, site.liveSites);
                }

                default: {
                    throw new SiteURLNotImplemented();
                }
            }
        } catch (SiteURLNotImplemented e) {
            logger.error(String
                    .format("Site '%s' has not implemented url based on requested environment (%s) and country (%s)",
                            site, environment, locale.getDisplayCountry()));
        }
        return null;
    }


}
