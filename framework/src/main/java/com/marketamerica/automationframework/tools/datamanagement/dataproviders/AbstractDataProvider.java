package com.marketamerica.automationframework.tools.datamanagement.dataproviders;

import com.marketamerica.automationframework.tools.datamanagement.ExcelDataManager;
import com.marketamerica.automationframework.tools.datamanagement.SQLDataManager;
import com.marketamerica.automationframework.tools.datamanagement.XMLDataManager;
import com.marketamerica.automationframework.tools.testmanagement.SuiteBuilder;
import com.marketamerica.automationframework.tools.testmanagement.TestBuilder;

import org.testng.ITestContext;

/**
 * This class represents an abstract data provider whose purpose is to return test data based
 * on the text context.
 * <p>
 * Currently it provides functionality to return data via Excel or XML
 */
public abstract class AbstractDataProvider {
    /**
     * Create a XML Data Manager to return xml serialized test data
     *
     * @param context a TestNG context used to determine the test environment and test locale (country)
     * @return a XMLDataManager for the given context
     */
    protected static XMLDataManager createXMLDataManager(final ITestContext context) {
        final SuiteBuilder configuration = new SuiteBuilder(context);
        TestBuilder testBuilder = new TestBuilder(context);
        final XMLDataManager dataManager = new XMLDataManager(
                configuration.getEnvironment(),
                testBuilder.getLocale()
        );
        return dataManager;
    }

    /**
     * Create an Excel DataManager used to create ExcelWorkBookReader Objects on the fly based on the current test context
     *
     * @param context a TestNG context used to determine the test environment and test locale (country)
     * @return
     */
    protected static ExcelDataManager createExcelManager(final ITestContext context) {
        final SuiteBuilder configuration = new SuiteBuilder(context);
        TestBuilder testBuilder = new TestBuilder(context);
        final ExcelDataManager dataManager = new ExcelDataManager(
                configuration.getEnvironment(),
                testBuilder.getLocale()
        );
        return dataManager;
    }
    
    /**
     * Create a SQL Database DataManager used to retrieve test data
     *
     * @param context a TestNG context used to determine the test environment and test locale (country)
     * @return a SQLDataManager for the given context
     */
    protected static SQLDataManager createSQLDataManager(final ITestContext context) {
        final SuiteBuilder configuration = new SuiteBuilder(context);
        TestBuilder testBuilder = new TestBuilder(context);
        final SQLDataManager dataManager = new SQLDataManager(
                configuration.getEnvironment(),
                testBuilder.getLocale()
        );
        return dataManager;
    }

}
