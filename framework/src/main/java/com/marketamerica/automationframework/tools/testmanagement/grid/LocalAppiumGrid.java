package com.marketamerica.automationframework.tools.testmanagement.grid;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.UnreachableBrowserException;
import org.testng.Reporter;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * The Appium Configurator for running test on devices locally. This works
 * only for Android devices if you are using Windows machine. You would need
 * start Appium locally with Server Address: 127.0.0.1 and port 4723
 */
public class LocalAppiumGrid {
    private static final String HUB_URL = "http://127.0.0.1:4723/wd/hub";
    private static final Logger logger = LogManager.getLogger(LocalAppiumGrid.class);

    public static WebDriver getRemoteWebBrowser(final DesiredCapabilities capabilities) {
        // TODO : need to add capabilties to take screenshot
        // capabilities.setCapability("takesScreenshot", true);
        try {
            logger.info(String.format("Creating Appium WebDriver instance using %s", capabilities));
            return new RemoteWebDriver(new URL(HUB_URL), capabilities);
        } catch (MalformedURLException e) {
            logger.fatal(String.format("%s was an invalid URL", HUB_URL), e);
        } catch (UnreachableBrowserException browserException) {
            logger.fatal(
                    String.format(
                            "Unable to reach the browser, Check if you have started the appium server at 127.0.0.1 and port 4723. The Hub url is - %s",
                            HUB_URL), browserException);
        } catch (WebDriverException e) {
            logger.fatal(String
                    .format("Unable to create Appium WebDriver instance with given capabilities, \"%s\"",
                            capabilities), e);
        }
        Reporter.log(String.format("Invalid capabilities set. Something went wrong!!", HUB_URL));
        return null;
    }
}
