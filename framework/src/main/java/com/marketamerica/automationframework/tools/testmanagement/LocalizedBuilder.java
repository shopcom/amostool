package com.marketamerica.automationframework.tools.testmanagement;


import java.util.Locale;
import java.util.Map;

import static com.marketamerica.automation.constants.GlobalConstants.DEFAULT_COUNTRY_CODE;
import static com.marketamerica.automation.constants.GlobalConstants.DEFAULT_LANGUAGE_CODE;
import static com.marketamerica.automation.constants.GlobalConstants.TestNGSuiteParameters.*;

/**
 * Created by javierv on 11/17/2014.
 */
public abstract class LocalizedBuilder {
    private Locale locale;

    /**
     * Configure the country code. If it is not set it defaults
     */
    private String getCountryCode(Map<String, String> parameters) {

        String locale = parameters.get(LOCALE);
        if (locale != null && locale.contains("_")) {
            return locale.split("_")[1];
        } else {
            String countryCode = parameters.get(COUNTRY_CODE);
            return countryCode != null ? countryCode
                    : DEFAULT_COUNTRY_CODE;
        }
    }

    /**
     * Configures the language code
     */
    private String getLanguageCode(Map<String, String> parameters) {
        String locale = parameters.get(LOCALE);
        if (locale != null && locale.contains("_")) {
            return locale.split("_")[0];
        } else {
            String languageCode = parameters.get(LANGUAGE_CODE);
            return languageCode != null ? languageCode
                    : DEFAULT_LANGUAGE_CODE;
        }
    }

    /**
     * Configures the suite locale based on the language code and country code
     */
    protected Locale createLocale(Map<String, String> parameters) {
        return new Locale(this.getLanguageCode(parameters), this.getCountryCode(parameters));
    }

    public Locale getLocale() {
        return this.locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

}
