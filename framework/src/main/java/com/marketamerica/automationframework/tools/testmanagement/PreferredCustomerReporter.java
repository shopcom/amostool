package com.marketamerica.automationframework.tools.testmanagement;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.testdata.Distributor;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.excel.ExcelWorkBookWriter;
import com.marketamerica.automation.utilities.helpers.DateAndTimeUtilities;
import com.marketamerica.dsb.DSBClient;
import com.marketamerica.dsb.DSBRecordUpdateException;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import static com.marketamerica.automation.utilities.helpers.CollectionUtilities.createList;
import static com.marketamerica.automationframework.tools.reporting.StepInfo.addMessage;

/**
 * This classes purpose is to serialize customer data into Excel. All operations
 * are static as there should be no instances of this class. Created by Javier
 * L. Velasquez on 12/12/2014.
 */
public class PreferredCustomerReporter {
	private static final Logger logger = LogManager.getLogger(PreferredCustomerReporter.class);

	private static DSBClient dsbClient;
	private static ExcelWorkBookWriter workbook;
	private static boolean configured = false;
	private static int records;

	public synchronized static void initialize(final Environment environment) {
		final String projectName = Configuration.getProjectName() == null
				|| Configuration.getProjectName().isEmpty() ? "project-name-undefined" : Configuration
				.getProjectName();

		dsbClient = new DSBClient(environment);
		if (!configured) {
			workbook = new ExcelWorkBookWriter(String.format("target/newly-registered-pcs-%s-%s-%s.xlsx",
					projectName, environment.getAbbreviation().toLowerCase(),
					DateAndTimeUtilities.getFormattedDate(Calendar.getInstance().getTime(), "MMddyyyyhhmmss")),
					false);

			workbook.createSheet("Preferred Customers");
			workbook.addHeaders(Arrays.asList("First Name", "Last Name", "Email Address", "Registration Source",
					"Password", "Portal ID", "Sponsor Email", "Sponsor Status", "Resident Country",
					"Distributor ID", "PC ID", "Customer Manager", "Cash Back " + "Total",
					"Distributor Home Country"));
			records = 0;
			configured = true;
		}
	}

	/**
	 * Take a shopper and serialize it as an entry to the current workbook. In
	 * addition to the attributes present in the class, dsb calls are made to
	 * populate additional shopper data. If the DSB returns an error, a message
	 * is printed to the current report
	 *
	 * @param shopper
	 *            an instance of Shopper who has a valid email address
	 */
	public synchronized static void record(Shopper shopper) {
		if (shopper.getPcID() == null) {
			try {
				dsbClient.resolveCustomerIDs(shopper);
			} catch (DSBRecordUpdateException e) {
				final String message = String.format("Unable to query DSB for Shopper Info %s. PC Not Recorded!",
						shopper);
				addMessage(message);
				logger.error(message + e);
				return;
			}
		}
		final Map<String, String> pcProfile = dsbClient.getPCProfile(shopper);
		// Password from DSB call seems to be incorrect so we override that
		// value here.
		if (shopper.getPassword() != null && !shopper.getPassword().isEmpty()) {
			pcProfile.put("Password", shopper.getPassword());
		}

		Distributor distributor = new Distributor();
		distributor.setDistributorId(pcProfile.get("Distributor ID"));

		final String distributorHomeCountry = dsbClient.getDistributorHomeCountry(distributor);

		if (distributorHomeCountry == null || distributorHomeCountry.isEmpty()) {
			pcProfile.put("Distributor Home Country", "Not Provided");
		} else {
			pcProfile.put("Distributor Home Country", distributorHomeCountry);
		}

		final String message = String.format("Recording newly registered PC \"%s\" to PC Reporter", pcProfile);
		addMessage(message);
		logger.debug(message);
		final List<Object> values = createList(pcProfile.values().toArray());
		workbook.addRow(values);
		++records;
	}

	/**
	 * If the workbook was correctly populated and at least one customer record
	 * was saved write and close the report. All columns will be re-sized.
	 */
	public synchronized static void generateReport() {
		if (records == 0) {
			logger.info("No Preferred customers recorded. Report will not be generated");
		} else if (configured && workbook != null) {
			workbook.resizeAllColumns();
			workbook.writeAndCloseWork();
		}
		configured = false;
	}

}
