package com.marketamerica.automationframework.listeners;

import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import br.eti.kinoshita.testlinkjavaapi.model.TestCase;
import com.marketamerica.automation.constants.GlobalConstants.TestNGSuiteParameters;
import com.marketamerica.automation.testdata.TestLinkID;
import com.marketamerica.automation.utilities.TestLinkManager;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.helpers.ReflectiveUtilities;
import com.marketamerica.automation.utilities.webdriver.WebDriverBrowser;
import com.marketamerica.automation.utilities.webdriver.WebDriverDevice;
import com.marketamerica.automation.utilities.webdriver.WebDriverDeviceType;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.ISuite;
import org.testng.ITestResult;

import java.util.*;

import static com.marketamerica.automation.constants.GlobalConstants.TestNGSuiteParameters.TEST_LINK_PROJECT;
import static com.marketamerica.automation.utilities.helpers.ReflectiveUtilities.*;

/**
 * This listener records results to Test Link by comparing suite conditions to
 * test link platforms
 *
 * @author Javier L. Velasquez
 * @see org.testng.IReporter
 * @see org.testng.ISuiteListener
 */
public class TestLinkReporter {
    private final Logger logger = LogManager.getLogger(getClass());
    private TestLinkManager testLinkManager;
    private Map<String, String> suiteParameters;


    public TestLinkReporter(ISuite suite) {
        initializeTestLinkManager(suite);
    }

    /**
     * Initialize our test link manager instance
     *
     * @param suite a TestNG ISuite suite represent our test suite.
     */
    protected void initializeTestLinkManager(ISuite suite) {
        suiteParameters = suite.getXmlSuite().getParameters();
        Map<String, String> testParameters = suite.getXmlSuite().getAllParameters();

        String testProject = null;
        if (testParameters.containsKey(TEST_LINK_PROJECT)) {
            testProject = testParameters.get(TEST_LINK_PROJECT);
        } else if (suiteParameters.containsKey(TEST_LINK_PROJECT)) {
            testProject = suiteParameters.get(TEST_LINK_PROJECT);
        }

        Environment environment = Environment.parse(suiteParameters.get("environment"));

        if (testProject != null) {
            testLinkManager = new TestLinkManager(testProject, environment);
        } else {
            logger.error(String.format("The parameter \"%s\" was not defined at the suite level, so "
                    + "no results will be saved in TestLink", TEST_LINK_PROJECT));
        }
    }


    /**
     * Indicates if a connection has been made to the Test Link Server.
     *
     * @return
     */
    public boolean isConnected() {
        return testLinkManager != null;
    }

    public void recordExecution(ITestResult testResult) {
        final TestLinkID testLinkID = getTestResultID(testResult);
        List<TestCase> testCases = this.testLinkManager.getTestCasesByExternalId(testLinkID);
        if (testCases.size() == 0) {
            logger.warn(String.format("No tests found with specified id (%s)", testLinkID));
        } else {
            logger.info(String.format("Attempting to record single test execution to following test link ids (%s)",
                    testLinkID.getExternalIds()));
            for (TestCase testCase : testCases) {
                // If we found a test case check to see if its platforms match. If they
                // do, then get its execution status based on all the execution results
                // and submit the result to test link
                if (testCase != null) {
                    final Set<TestCase> testCasesWithPlatform = this.testLinkManager.getAllMatchingTestCases(testCase);
                    if (testCasesWithPlatform.size() == 0) {
                        logger.warn(String.format("Test Case (%s) was not found in any test plans", testCase));
                    }
                    for (final TestCase testCaseWithPlatform : testCasesWithPlatform) {
                        final String[] platforms = this.testLinkManager.getTestCasePlatforms(testCaseWithPlatform);
                        // If a test doesn't have any platforms then don't record
                        // results.
                        // Platforms are needed to record results on TestLink
                        if (platforms != null && platforms[0] != null && !platforms[0].isEmpty()) {
                            if (!isTestEnvironmentMatchingGivenPlatform(testCaseWithPlatform, testResult, platforms)) {
                                continue;
                            }
                            if (!isTestCountryMatchingGivenPlatform(testCaseWithPlatform, testResult, platforms)) {
                                continue;
                            }
                            if (!isTestBrowserMatchingGivenPlatform(testCaseWithPlatform, testResult, platforms)) {
                                continue;
                            }
                            if (platforms[0].trim()
                                    .equalsIgnoreCase(WebDriverDeviceType.PHONE.toString().trim())
                                    || platforms[0].trim()
                                    .equalsIgnoreCase(WebDriverDeviceType.TABLET.toString().trim())) {
                                if (!isTestDeviceOSMatchingGivenPlatform(testCaseWithPlatform, testResult, platforms)) {
                                    continue;
                                }
                            }
                        } else {
                            logger.warn(String.format(
                                    "No platform specified for test (%s - %s) on TestLink. Please specify one!",
                                    testCaseWithPlatform.getName(), testCaseWithPlatform.getFullExternalId()));
                            continue;
                        }
                        if (!this.testLinkManager.isTestCaseAutomated(testCase)) {
                            this.testLinkManager.setTestExecutionToAutomated(testCase);
                        }

                        final ExecutionStatus executionStatus = getExecutionStatus(testResult,
                                platforms);
                        final String notes = "";
                        this.testLinkManager.setTestStatus(testCaseWithPlatform,
                                executionStatus, notes);
                    }
                } else {
                    logger.warn(
                            String.format("Could not find a manual case on TestLink when searching for the following " +
                                            "test case id: \"%s\" from the test \"%s\"",
                                    testLinkID,
                                    getMethod(testResult).getName()
                            )
                    );
                }
            }
        }
    }

    private boolean isTestBrowserMatchingGivenPlatform(TestCase testCase, ITestResult testResult,
                                                       String[] platforms) {
        final List<WebDriverBrowser> expectedBrowser = getExpectedBrowsersFromPlatforms(platforms);

        final Set<WebDriverBrowser> executedBrowsers = getBrowser(testResult);

        if (executedBrowsers.containsAll(expectedBrowser)) {
            return true;
        } else {
            logger.warn(String.format("Test (%s - %s) did not execute on the given browser(s) (%s) so "
                            + "it was not reported to TestLink. Please ensure it has been added to all the test suites.",
                    testCase.getName(), testCase.getFullExternalId(), Arrays.toString(platforms)));
            return false;
        }

    }

    private boolean isTestDeviceOSMatchingGivenPlatform(TestCase testCase, ITestResult testResult,
                                                        String[] platforms) {
        final List<WebDriverDevice> expectedDevices = getExpectedOSFromPlatforms(platforms);

        final Set<WebDriverDevice> executedDevices = getOS(testResult);

        if (executedDevices.containsAll(expectedDevices)) {
            return true;
        } else {
            logger.warn(String.format("Test (%s - %s) did not execute on the given Device(s) (%s) so "
                            + "it was not reported to TestLink. Please ensure it has been added to all the test suites.",
                    testCase.getName(), testCase.getFullExternalId(), Arrays.toString(platforms)));
            return false;
        }

    }

    private Set<WebDriverBrowser> getBrowser(ITestResult testResult) {
        return getBrowser(Arrays.asList(testResult));
    }

    private Set<WebDriverBrowser> getBrowser(Collection<ITestResult> testResults) {
        final Set<WebDriverBrowser> browsers = new HashSet<>();
        for (final ITestResult testResult : testResults) {
            final WebDriverBrowser browser = WebDriverBrowser.fromString(testResult.getMethod().getXmlTest()
                    .getAllParameters().get("browser"));
            if (browser == null) {
                continue;
            }
            if (browser.equals(WebDriverBrowser.INTERNET_EXPLORER)) {
                browsers.add(WebDriverBrowser.IE);
            } else {
                browsers.add(browser);
            }
        }
        return browsers;
    }

    private Set<WebDriverDevice> getOS(ITestResult testResult) {
        final Set<WebDriverDevice> devices = new HashSet<>();
        final WebDriverDevice device = WebDriverDevice.getDevice(testResult.getMethod().getXmlTest()
                .getAllParameters().get("deviceType"), testResult.getMethod().getXmlTest().getAllParameters()
                .get("deviceOS"));
        devices.add(device);

        return devices;
    }

    private List<WebDriverBrowser> getExpectedBrowsersFromPlatforms(String[] platforms) {
        final List<WebDriverBrowser> browsers = new ArrayList<>();

        // For each platform try and return any valid countries
        for (int i = 0; i < platforms.length; ++i) {
            final WebDriverBrowser browser = WebDriverBrowser.fromString(platforms[i]);
            if (browser != null) {
                if (browsers.equals(WebDriverBrowser.INTERNET_EXPLORER)) {
                    browsers.add(WebDriverBrowser.IE);
                } else {
                    browsers.add(browser);
                }
            }
        }

        return browsers;
    }

    private List<WebDriverDevice> getExpectedOSFromPlatforms(String[] platforms) {
        final List<WebDriverDevice> deviceOSList = new ArrayList<>();

        final WebDriverDevice deviceOS = WebDriverDevice.getDevice(platforms[0], platforms[1]);
        if (deviceOS != null) {
            deviceOSList.add(deviceOS);
        }

        return deviceOSList;
    }

    /**
     * Get all test results based on the external id
     *
     * @param externalID  a string representing a test case id on TestLink
     * @param testResults
     * @return a set of filtered test results
     */
    private Set<ITestResult> filterResultsByExternalId(final String externalID, Set<ITestResult> testResults) {
        final Set<ITestResult> filteredTestResultsByExternalId = new HashSet<>();

        for (final ITestResult testResult : testResults) {
            if (ReflectiveUtilities.getMethodExternalId(
                    testResult.getMethod().getConstructorOrMethod().getMethod()).equals(externalID)) {
                // if (testResult.getTestContext().getCurrentXmlTest()
                // .getAllParameters().get("browser")
                // .equalsIgnoreCase("firefox")) {
                filteredTestResultsByExternalId.add(testResult);
                // }
            }
        }

        return filteredTestResultsByExternalId;
    }

    /**
     * Validate that a test case matches the given platform by environment
     *
     * @param testCase   a test case object containing a platform
     * @param testCase   a method object representing a test method
     * @param testResult
     * @param platforms  a string containing the test platforms
     * @return a boolean indicating if the test matches the given platforms
     */
    private boolean isTestEnvironmentMatchingGivenPlatform(final TestCase testCase,
                                                           ITestResult testResult, final String[] platforms) {

        // Get the environments supported by the test method
        final List<Environment> supportedEnvironments = ReflectiveUtilities.getSupportedEnvironments(ReflectiveUtilities.getMethod(testResult));
        // Get the actual environments ran
        final Set<Environment> executedEnvironments = getExecutedEnvironments(testResult);
        // Get the environments supported by the test case
        final List<Environment> expectedEnvironmets = getExpectedEnvironmentsFromGivePlatform(platforms);

        if (!supportedEnvironments.containsAll(expectedEnvironmets)) {
            logger.warn(String.format("%s did not contain the expected environments: %s, "
                            + "actual: %s. It is reccomended you update the "
                            + "annotations to indicate the updated testing scenarios",
                    testResult.getName(), expectedEnvironmets, supportedEnvironments));
        }

        if (executedEnvironments.containsAll(expectedEnvironmets)) {
            return true;
        } else {
            logger.warn(String.format("Test (%s - %s) did not execute on given environment (%s) so "
                            + "it was not reported to TestLink. Please check its annotations and update accordingly.",
                    testCase.getName(), testCase.getFullExternalId(), Arrays.toString(platforms)));
            return false;
        }
    }

    private Set<Environment> getExecutedEnvironments(final ITestResult testResults) {
        return getExecutedEnvironments(Arrays.asList(testResults));
    }

    /**
     * Get all the execution environments associated with each test result
     *
     * @param testResults a set of TestNG test results
     * @return a set Environment com.marketamerica.automation.utilities.enums
     */
    private Set<Environment> getExecutedEnvironments(final Collection<ITestResult> testResults) {
        final Set<Environment> executedEnvironments = new HashSet<>();
        for (final ITestResult testResult : testResults) {
            final Map<String, String> suiteParameters = testResult.getMethod().getXmlTest().getSuite()
                    .getParameters();
            final String suiteEnvironment = suiteParameters
                    .get(TestNGSuiteParameters.ENVIRONMENT_STRING);
            if (suiteEnvironment == null) {
                return executedEnvironments;
            }
            final Environment environment = Environment.parse(suiteEnvironment);
            executedEnvironments.add(environment);
        }
        return executedEnvironments;
    }

    private Set<Country> getExecutedCountries(final ITestResult testResult) {
        return getExecutedCountries(Arrays.asList(testResult));
    }

    /**
     * Get all the execution countries associated with each test result
     *
     * @param testResults a set of TestNG test results
     * @return a set Country com.marketamerica.automation.utilities.enums
     */
    private Set<Country> getExecutedCountries(final Collection<ITestResult> testResults) {
        final Set<Country> executedCountries = new HashSet<>();
        for (final ITestResult testResult : testResults) {
            final Map<String, String> suiteParameters = testResult.getMethod().getXmlTest().getSuite()
                    .getParameters();
            String countryCode = suiteParameters.get(TestNGSuiteParameters.COUNTRY_CODE);
            String languageCode = suiteParameters.get(TestNGSuiteParameters.LANGUAGE_CODE);
            Country country;
            if (languageCode == null || countryCode == null) {
                Map<String, String> testParameter = testResult.getMethod().getXmlTest().getAllParameters();
                if (testParameter.containsKey("locale")) {
                    String[] split = testParameter.get("locale").split("_");
                    countryCode = split[1];
                    languageCode = split[0];
                } else {
                    continue;
                }
            }
            try {
                country = Country.parse(new Locale(languageCode, countryCode).getDisplayCountry());
            } catch (NullPointerException e) {
                continue;
            }
            executedCountries.add(country);

        }
        return executedCountries;

    }

    /**
     * Get the Environment listed in the test case that we expected to have ran
     *
     * @param platforms a platform's string array
     * @return a list of Environment
     * com.marketamerica.automation.utilities.enums
     */
    private List<Environment> getExpectedEnvironmentsFromGivePlatform(final String[] platforms) {
        final List<Environment> environments = new ArrayList<>();

        // For each platform try returning its abbreviated environment
        for (int i = 0; i < platforms.length; ++i) {
            final Environment environment = Environment.parseAbbreviation(platforms[i]);
            if (environment != null) {
                environments.add(environment);
            }
        }
        return environments;
    }

    /**
     * Validate that a test case matches the given platform by country
     *
     * @param testCase   a test case object containing a platform
     * @param testResult
     * @param platforms  a string containing the test platforms
     * @return a boolean indicating if the test matches the given platforms
     */
    private boolean isTestCountryMatchingGivenPlatform(final TestCase testCase, ITestResult testResult,
                                                       final String[] platforms) {

        // Get the countries supported by the test method
        final List<Country> supportedCountries = getSupportedCountries(getMethod(testResult));
        // Get the actual countries ran
        final Set<Country> executedCountries = getExecutedCountries(testResult);
        // Get the countries expected by test link
        final List<Country> expectedCountries = getExpectedCountriesFromPlatforms(platforms);

        if (!supportedCountries.containsAll(expectedCountries)) {
            logger.warn(String.format("%s did not contain the expected countries: %s, "
                            + "actual: %s. It is reccomended you update the "
                            + "annotations to indicate the updated testing scenarios",
                    testResult.getName(), expectedCountries, supportedCountries));
        }
        if (executedCountries.containsAll(expectedCountries)) {
            return true;
        } else {
            logger.warn(String.format("Test (%s - %s) did not execute on given country (%s) so "
                            + "it was not reported to TestLink. Please check its annotations and update accordingly.",
                    testCase.getName(), testCase.getFullExternalId(), Arrays.toString(platforms)));
            return false;
        }
    }

    /**
     * Get the Country listed in the test case that we expected to have ran
     *
     * @param platforms a platform's string array
     * @return a list of Country com.marketamerica.automation.utilities.enums
     */
    private List<Country> getExpectedCountriesFromPlatforms(final String[] platforms) {
        final List<Country> countries = new ArrayList<>();

        // For each platform try and return any valid countries
        for (int i = 0; i < platforms.length; ++i) {
            final Country country = Country.parseAbbreviation(platforms[i]);
            if (country != Country.NOT_CLASSIFIED) {
                countries.add(country);
            }
        }

        return countries;

    }

    // /**
    // * Create a string representing a test link method note. This value will
    // be
    // * used as a part of a test submission.
    // *
    // * @param testResults
    // * a TestNG ITestResult object
    // * @param executionStatus
    // * a Test Link result status
    // * @return an empty string on test pass, the throwable message on fail or
    // * skip.
    // */
    // private String getNote(final Set<ITestResult> testResults) {
    // String note = "";
    // for (final ITestResult testResult : testResults) {
    // if (testResult.getStatus() != ITestResult.SUCCESS) {
    // if (testResult.getThrowable() != null) {
    // note = String.format("%s, %s", note, testResult
    // .getThrowable().getMessage());
    // } else {
    // continue;
    // }
    // } else {
    // continue;
    // }
    // }
    // return note;
    // }

    /**
     * Get the TestLink corresponding execution status by its TestNG ITestResult
     * test status constant. If the test has an invalid status it is marked as
     * not ran
     *
     * @param testResult
     * @param platforms
     * @return
     */
    private ExecutionStatus getExecutionStatus(final ITestResult testResult,
                                               String[] platforms) {
        ExecutionStatus status;
        if (getExecutedEnvironments(testResult).containsAll(
                this.getExpectedEnvironmentsFromGivePlatform(platforms))) {
            if (this.getExecutedCountries(testResult).containsAll(
                    this.getExpectedCountriesFromPlatforms(platforms))) {
                if (this.getBrowser(testResult).containsAll(
                        getExpectedBrowsersFromPlatforms(platforms))) {
                    switch (testResult.getStatus()) {
                        case ITestResult.SUCCESS:
                            status = ExecutionStatus.PASSED;
                            break;
                        case ITestResult.FAILURE:
                            status = ExecutionStatus.FAILED;
                            break;
                        case ITestResult.SKIP:
                            status = ExecutionStatus.NOT_RUN;
                            break;
                        default:
                            status = ExecutionStatus.NOT_RUN;
                            break;
                    }
                    return status;
                }
            }
        }
        return ExecutionStatus.NOT_RUN;
    }

}
