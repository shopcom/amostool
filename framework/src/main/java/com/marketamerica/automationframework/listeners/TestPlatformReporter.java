package com.marketamerica.automationframework.listeners;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.constants.GlobalConstants.TestNGSuiteParameters;
import com.marketamerica.automation.testdata.TestLinkID;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.excel.ExcelWorkBookWriter;
import com.marketamerica.automation.utilities.helpers.ReflectiveUtilities;
import com.marketamerica.automation.utilities.webdriver.WebDriverBrowser;
import org.testng.IInvokedMethod;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;

import java.lang.reflect.Method;
import java.util.*;
 

public class TestPlatformReporter implements IReporter {        

    @Override
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites,
                               String outputDirectory) {    	
    	
        ExcelWorkBookWriter workbook;  
    	   	  	
        final String projectName = Configuration.getProjectName() == null || Configuration.getProjectName().isEmpty()
                ? "project-name-undefined" : Configuration.getProjectName();
        final Environment environment = Environment.getCurrentEnvironment();
        
        workbook = new ExcelWorkBookWriter(
                String.format("target/test-platform-report-%s-%s.xlsx",
                		projectName,
                		environment                		                               
                )
                ,false);
    	        
        workbook.createSheet("Test_Platforms");
        workbook.addHeaders(Arrays.asList("Test ID", "Test Name", "Country", "Browser", "Result"));    	
    	
        for (final ISuite suite : suites) {
            final List<IInvokedMethod> allInvokedMethods = suite
                    .getAllInvokedMethods();
            for (final IInvokedMethod invokedMethod : allInvokedMethods) {
                if (invokedMethod.getTestMethod().isTest()) {
                    ITestResult testResult = invokedMethod.getTestResult();
                    
                    final TestLinkID testLinkID = ReflectiveUtilities.getTestResultID(testResult);
                    final Method testMethod = ReflectiveUtilities.getMethod(testResult);
                    final String methodName = ReflectiveUtilities.getFullMethodName(testMethod);
                    final Country excutedCountry = getExecutedCountry(testResult);
                    final WebDriverBrowser excutedBrowser =  getBrowser(testResult);                    
                    String testStatus = getTestResult(testResult.getStatus());
                    
                    final List<String> entries = new ArrayList<>();
                    
                    entries.add(testLinkID.toString());
                    entries.add(methodName);
                    entries.add(excutedCountry.toString());
                    entries.add(excutedBrowser.toString());
                    entries.add(testStatus);
                    
                    workbook.addRow(entries);   
                    
                }  
            }            
    	}
    	workbook.resizeAllColumns();
        workbook.writeAndCloseWork();    
    } 
    
    /**
     * Get execution country associated with a test result
     *
     * @param testResult a set of TestNG test result
     * @return Country com.marketamerica.automation.utilities.enums
     */
	private Country getExecutedCountry(final ITestResult testResult) {		
		final Map<String, String> suiteParameters = testResult.getMethod()
				.getXmlTest().getSuite().getParameters();
		String countryCode = suiteParameters
				.get(TestNGSuiteParameters.COUNTRY_CODE);
		String languageCode = suiteParameters
				.get(TestNGSuiteParameters.LANGUAGE_CODE);
		Country country;
		if (languageCode == null || countryCode == null) {
			Map<String, String> testParameter = testResult.getMethod()
					.getXmlTest().getAllParameters();
			if (testParameter.containsKey("locale")) {
				String[] split = testParameter.get("locale").split("_");
				countryCode = split[1];
				languageCode = split[0];
			}
		}
		country = Country.parse(new Locale(languageCode, countryCode)
				.getDisplayCountry());		 
		return country;
	}

	private WebDriverBrowser getBrowser(ITestResult testResult) {		
		return  WebDriverBrowser.fromString(testResult
				.getMethod().getXmlTest().getAllParameters().get("browser"));
	}
	
	private String getTestResult(int status) {
		switch (status) {
        case ITestResult.SUCCESS:
            return "Success";     
        case ITestResult.FAILURE:
            return "Failure";
        case ITestResult.SKIP:
        	return "Skip";
		}
		return "OTHER";
	}

}
