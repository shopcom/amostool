package com.marketamerica.automationframework.listeners;

import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Validate;
import org.testng.*;
import org.testng.internal.Utils;

import java.util.List;

/**
 * Listener for collecting the Validation Failures . The list of validation failures are collectively thrown at the end of the test.
 *
 * @author archanac
 */
public class ValidationListener implements IInvokedMethodListener, ISuiteListener {
    public final static String INITIALIZATION_CHECK_MESSAGE = "Validate is being used but the corresponding listener has not been added to the current test suite";
    private static boolean CONFIGURED = false;

    /**
     * Indicates whether the ValidationListener was actually configured
     *
     * @return
     */
    public static synchronized boolean isConfigured() {
        return CONFIGURED;
    }

    /* 
     * Overriding this method to display the failures at the end of the test.
     */
    @Override
    public void afterInvocation(IInvokedMethod method, ITestResult result) {

        Reporter.setCurrentTestResult(result);
        StepInfo.getAndResetStepInfoCount();
        if (method.isTestMethod()) {

            List<Throwable> validationFailures = Validate.getValidationFailuresMap();

            //if there are verification failures...
            if (validationFailures.size() > 0) {

                //set the test to failed
                result.setStatus(ITestResult.FAILURE);

                //if there is an assertion failure add it to verificationFailures
                if (result.getThrowable() != null) {
                    validationFailures.add(result.getThrowable());
                }

                int size = validationFailures.size();
                //if there's only one failure just set that
                if (size == 1) {
                    result.setThrowable(validationFailures.get(0));
                } else {
                    //create a failure message with all failures and stack traces (except last failure)
                    StringBuffer failureMessage = new StringBuffer("Multiple failures (").append(size).append("):\n\n");
                    for (int i = 0; i < size; i++) {
                        failureMessage.append("Failure ").append(i + 1).append(" of ").append(size).append(":\n");
                        Throwable throwable = validationFailures.get(i);
                        String fullStackTrace = Utils.stackTrace(throwable, false)[1];
                        //Utils.stackTrace - returns the short stack trace and full stack trace - taking the full stack trace here to be displayed here from throwable
                        failureMessage.append(fullStackTrace).append("\n\n");
                    }

                    //set  throwable
                    Throwable throwableMessage = new Throwable(failureMessage.toString());
                    result.setThrowable(throwableMessage);


                }
            }
        }
    }

    @Override
    public void beforeInvocation(IInvokedMethod method, ITestResult testResult) {
        StepInfo.getAndResetStepInfoCount();
    }

    @Override
    public synchronized void onStart(ISuite suite) {
        if (!CONFIGURED)
            CONFIGURED = true;

    }

    @Override
    public void onFinish(ISuite suite) {

    }

}
