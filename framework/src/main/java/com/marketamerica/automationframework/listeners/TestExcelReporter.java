package com.marketamerica.automationframework.listeners;

import com.marketamerica.automation.utilities.enums.ITestResultClassifications;
import com.marketamerica.automation.utilities.excel.ExcelWorkBookWriter;
import com.marketamerica.automation.utilities.helpers.ReflectiveUtilities;
import org.apache.poi.ss.usermodel.Row;
import org.testng.IInvokedMethod;
import org.testng.IReporter;
import org.testng.ISuite;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class TestExcelReporter implements IReporter {
    private ExcelWorkBookWriter workbook;

    public TestExcelReporter() {
        workbook = new ExcelWorkBookWriter(
                "C:\\Users\\javierv\\Desktop\\output.xlsx", false);
        workbook.createSheet(Excel.Sheets.SUMMARY);
        workbook.createSheet(Excel.Sheets.PASSED);
        workbook.switchWorkSheet(Excel.Sheets.PASSED);
        workbook.addHeaders(Excel.Columns.VALUES());
        workbook.createSheet(Excel.Sheets.FAILED);
        workbook.switchWorkSheet(Excel.Sheets.FAILED);
        workbook.addHeaders(Excel.Columns.VALUES());
        workbook.createSheet(Excel.Sheets.SKIPPED);
        workbook.switchWorkSheet(Excel.Sheets.SKIPPED);
        workbook.addHeaders(Excel.Columns.VALUES());

    }

    @Override
    public void generateReport(List<XmlSuite> xmlSuites, List<ISuite> suites,
                               String outputDirectory) {
        final Set<ITestResult> failureMessages = new HashSet<>();
        for (final ISuite suite : suites) {
            final List<IInvokedMethod> allInvokedMethods = suite
                    .getAllInvokedMethods();
            for (final IInvokedMethod invokedMethod : allInvokedMethods) {
                if (invokedMethod.getTestMethod().isTest()) {
                    ITestResult testResult = invokedMethod.getTestResult();
                    int status = testResult.getStatus();
                    Row row = null;
                    switch (status) {
                        case ITestResult.SUCCESS:
                            workbook.switchWorkSheet(Excel.Sheets.PASSED);
                            row = workbook.createRow();
                            workbook.setRowValueByColumnName(row,
                                    Excel.Columns.NAME, testResult.getName());
                            break;
                        case ITestResult.FAILURE:
                            failureMessages.add(testResult);
                            workbook.switchWorkSheet(Excel.Sheets.FAILED);
                            row = workbook.createRow();
                            workbook.setRowValueByColumnName(row,
                                    Excel.Columns.NAME, testResult.getName());

                            final Throwable throwable = testResult.getThrowable();
                            workbook.setRowValueByColumnName(row,
                                    Excel.Columns.ERROR, ReflectiveUtilities
                                            .getThrowableAsString(throwable)
                            );
                            workbook.setRowValueByColumnName(
                                    row,
                                    Excel.Columns.STACK_TRACE,
                                    ReflectiveUtilities.getStackTraceAsString(
                                            throwable.getStackTrace(), true)
                                            .toString()
                            );

                            workbook.setRowValueByColumnName(row,
                                    Excel.Columns.CLASSIFICATION,
                                    ITestResultClassifications.classify(throwable)
                                            .toString()
                            );

                            break;
                        case ITestResult.SKIP:
                            workbook.switchWorkSheet(Excel.Sheets.SKIPPED);
                            row = workbook.createRow();
                            workbook.setRowValueByColumnName(row,
                                    Excel.Columns.NAME, testResult.getName());
                            workbook.setRowValueByColumnName(row,
                                    Excel.Columns.ERROR, testResult.getThrowable()
                                            .getMessage()
                            );

                            break;
                    }

                }
            }
        }
        workbook.writeAndCloseWork();
    }

    private static class Excel {
        private static class Columns {
            public static final String NAME = "Name";
            public static final String ERROR = "Error";
            public static final String STACK_TRACE = "Stack Trace";
            public static final String CLASSIFICATION = "Classification";

            public static List<String> VALUES() {
                return Arrays.asList(new String[]{NAME, ERROR, STACK_TRACE,
                        CLASSIFICATION});
            }
        }

        private class Sheets {
            public static final String SUMMARY = "Summary";
            private static final String SKIPPED = "Skipped";
            private static final String FAILED = "Failed";
            private static final String PASSED = "Passed";
        }
    }

}
