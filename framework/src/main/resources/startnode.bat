SET SELENIUM_SERVER_VERSION=2.46.0
SET HUB_IP=10.99.110.101
SET HUB_PORT=4444
SET HUB_URL=http://%HUB_IP%:%HUB_PORT%/grid/register

SET PATH=%PATH%;%CD%

java -jar "selenium-server-standalone-%SELENIUM_SERVER_VERSION%.jar" -role node -hub %HUB_URL% -nodeConfig "%CD%\nodeconfig.json"
