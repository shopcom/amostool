
SET SELENIUM_SERVER_VERSION=2.46.0
SET HUB_PORT=4444
SET NODE_TIMEOUT=600

java -jar "selenium-server-standalone-%SELENIUM_SERVER_VERSION%.jar" -port 4444 -role hub -nodeTimeout %NODE_TIMEOUT% 