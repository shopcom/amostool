package com.marketamerica.automationframework.mobileexamples;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;


public class AndroidDevicePoolMultiThreadingExampleTestCase {

    private static final Logger logger = LogManager.getLogger(AndroidDevicePoolMultiThreadingExampleTestCase.class);
    WebDriver driver = null;
    HashMap<String, String> validList = new HashMap<String, String>();
    String driverKey = "ANDROID_DRIVER1";
    String driverport = "4723";

    @BeforeMethod
    public void setup() throws MalformedURLException {
        DesiredCapabilities capabilities = DesiredCapabilities.android();
        capabilities.setCapability(CapabilityType.BROWSER_NAME, "chrome");
        capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        capabilities.setVersion("4.4");
        capabilities.setCapability("platformName", "android");
        capabilities.setCapability("deviceName", "Samsung-SM-G900V");
        capabilities.setPlatform(Platform.ANDROID);
        validList = getValidAppiumAndroidPorts();
        int randomIndex = new Random().nextInt(validList.size());
        List<String> keys = new ArrayList<String>(validList.keySet());
        driverKey = keys.get(randomIndex).toString();
        driverport = validList.get(driverKey);
        validList.remove(driverKey);
        logger.info("driver Details" + driverKey + ":" + driverport);
        driver = new RemoteWebDriver(new URL("http://10.99.215.79:" + driverport.trim() + "/wd/hub"), capabilities);

    }

    private HashMap<String, String> getValidAppiumAndroidPorts() {
        HashMap<String, String> validAppiumAndroidPorts = new HashMap<String, String>();
        validAppiumAndroidPorts.put("ANDROID_DRIVER1", "4723");
        validAppiumAndroidPorts.put("ANDROID_DRIVER2", "4724");
        return validAppiumAndroidPorts;
    }

    @Test
    public void googleTest() throws Exception {

        // And now use this to visit Google
        driver.get("http://www.google.com");

        // Find the text input element by its name
        WebElement element = driver.findElement(By.name("q"));

        // Enter something to search for
        element.sendKeys("Cheese!");

        // Now submit the form. WebDriver will find the form for us from the
        // element
        element.submit();

        // Check the title of the page
        System.out.println("Page title is: " + driver.getTitle());

    }

    @Test
    public void googleTest2() throws Exception {

        // And now use this to visit Google
        driver.get("http://www.google.com");

        // Find the text input element by its name
        WebElement element = driver.findElement(By.name("q"));

        // Enter something to search for
        element.sendKeys("Cheese!");

        // Now submit the form. WebDriver will find the form for us from the
        // element
        element.submit();

        // Check the title of the page
        System.out.println("Page title is: " + driver.getTitle());

    }

    @Test
    public void googleTest3() throws Exception {

        // And now use this to visit Google
        driver.get("http://www.google.com");

        // Find the text input element by its name
        WebElement element = driver.findElement(By.name("q"));

        // Enter something to search for
        element.sendKeys("Cheese!");

        // Now submit the form. WebDriver will find the form for us from the
        // element
        element.submit();

        // Check the title of the page
        System.out.println("Page title is: " + driver.getTitle());

    }

    @Test
    public void googleTest4() throws Exception {

        // And now use this to visit Google
        driver.get("http://www.google.com");

        // Find the text input element by its name
        WebElement element = driver.findElement(By.name("q"));

        // Enter something to search for
        element.sendKeys("Cheese!");

        // Now submit the form. WebDriver will find the form for us from the
        // element
        element.submit();

        // Check the title of the page
        System.out.println("Page title is: " + driver.getTitle());

    }

    @AfterMethod
    public void tearDown() {
        logger.info("driver Details" + driverKey + ":" + driverport);
        validList.put(driverKey, driverport);
        driver.quit();
    }

}
