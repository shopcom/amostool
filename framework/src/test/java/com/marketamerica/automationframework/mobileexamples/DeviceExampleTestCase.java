package com.marketamerica.automationframework.mobileexamples;


import com.marketamerica.automationframework.tools.testmanagement.TestCases;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

public class DeviceExampleTestCase extends TestCases {

    @Test
    public void googleTest() throws Exception {

        // And now use this to visit Google
        driver.get("http://www.google.com");

        // Find the text input element by its name
        WebElement element = driver.findElement(By.name("q"));

        // Enter something to search for
        element.sendKeys("Cheese!");

        // Now submit the form. WebDriver will find the form for us from the element
        element.submit();

        // Check the title of the page
        System.out.println("Page title is: " + driver.getTitle());

    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

    @Override
    protected String getBaseURL() {
        return "http://www.google.com";
    }

}
