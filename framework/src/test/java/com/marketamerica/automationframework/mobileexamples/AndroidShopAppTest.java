package com.marketamerica.automationframework.mobileexamples;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.marketamerica.automation.constants.GlobalConstants;

public class AndroidShopAppTest {
    private AppiumDriver driver = null;

    @BeforeMethod
    public void setup() {
//		File appDir = new File("C:\\Users\\archanac\\Desktop");
//		File app = new File(appDir, "Shop-debug.apk");

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.VERSION, "4.4");
        capabilities.setCapability(CapabilityType.BROWSER_NAME, "");
        capabilities.setCapability(GlobalConstants.Capability.PLATFORM_NAME, Platform.ANDROID.toString());
        capabilities.setCapability(GlobalConstants.Capability.DEVICE_NAME, "Samsung");
        capabilities.setCapability(GlobalConstants.Capability.DEVICE, "android");
        capabilities.setCapability(GlobalConstants.Capability.APP_PACKAGE, "com.ma.android");
        capabilities.setCapability(GlobalConstants.Capability.APP_ACTIVITY, "Shop");
//		capabilities.setCapability(GlobalConstants.Capability.APP_WAIT_PACKAGE, "com.ma.android.Shop");
//		capabilities.setCapability("noReset","true");
//		capabilities.setCapability(GlobalConstants.Capability.APP, app.getAbsolutePath());

        try {
            driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);

    }

    @Test
    public void appiumExampleTest() throws Exception {
        Thread.sleep(10000);
        Set<String> contextNames = driver.getContextHandles();
        for (String contextName : contextNames) {
            System.out.println(contextName); //prints out something like NATIVE_APP \n WEBVIEW_1
        }
        driver.context((String) contextNames.toArray()[0]); // set context to WEBVIEW_1
//		driver.context("WEBVIEW_com.ma.android");
        System.out.println((String) contextNames.toArray()[0]);

        WebDriverWait wait = new WebDriverWait(driver, 10);
//		WebElement element = driver.findElement(By.id("ext-element-3"));
//		wait.until(ExpectedConditions.visibilityOf(element));
//		WebElement element1 = driver.findElement(By.id("ext-fixedbutton-1"));
//		System.out.println(element1);
//		driver.findElement(By.id("ext-element-14")).click();
//		WebElement element2 = driver.findElement(By.id("ext-element-32"));
//		System.out.println(element2);
//		element2.click();
//		wait.until(ExpectedConditions.presenceOfElementLocated(By.name("Create Account")));
//		driver.findElement(By.name("Create Account")).click();
//		wait.until(ExpectedConditions.presenceOfElementLocated(By.id("ext-element-14"))).click();
//		WebElement element = driver.findElements(By.className("android.view.View")).get(1);
//		System.out.println(element);
//				element.click();
//		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("Shop")));
        List<WebElement> elementL = driver.findElements(By.className("android.view.View"));
        for (WebElement element1 : elementL) {
            System.out.println(element1 + "Text:  " + element1.getText() + "class:   " + element1.getClass()
                    + "location:  " + element1.getLocation());
        }
        System.out.println(elementL.size());
        elementL.get(16).click(); //Click menu icon
        wait.until(ExpectedConditions.presenceOfElementLocated(By.name("My Account"))).click();
    }

    @AfterMethod
    public void tearDown() {
        if (driver != null) {
            driver.quit();
        }
    }

}
