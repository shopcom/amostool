package com.marketamerica.automationframework.mobileexamples;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class IOSExampleTestCase {
    WebDriver driver = null;

    @BeforeMethod
    public void setup() {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(CapabilityType.BROWSER_NAME, "safari");
        capabilities.setCapability("platformName", "iOS");
        capabilities.setCapability("deviceName", "iPhone");
        capabilities.setCapability("device", "iPhone");
//		DesiredCapabilities capabilities = DesiredCapabilities.android();
//		capabilities.setCapability(CapabilityType.BROWSER_NAME, "browser");
//		capabilities.setCapability("app", "com.android.browser");
//		capabilities.setCapability("platformName", "Android");
//        capabilities.setCapability("deviceName", "Android");
        try {
//			driver = new RemoteWebDriver(new URL("http://10.99.215.120:4445/wd/hub"), capabilities);
            driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void googleTest() throws Exception {
        // And now use this to visit Google
        driver.get("http://www.google.com");

        // Find the text input element by its name
        WebElement element = driver.findElement(By.name("q"));

        // Enter something to search for
        element.sendKeys("Appium Testing!");

        // Now submit the form. WebDriver will find the form for us from the element
        element.submit();

        // Check the title of the page
        System.out.println("Page title is: " + driver.getTitle());
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

    //	@Override
    //	protected String getBaseURL() {
    //		return "http://www.google.com";
    //	}

}
