package com.marketamerica.automationframework.examples;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.utilities.enums.Environment;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;

/**
 * Created by javierv on 8/1/2014.
 */
public class ConfigurationTests {

    @BeforeMethod
    public void configureMethod() {
        Configuration.initialize(Environment.DEVELOPMENT);
    }

    @Test
    public void testGetName() {
        assertFalse(Configuration.getProjectName().isEmpty(), "Expected the name to be configured");
    }

}
