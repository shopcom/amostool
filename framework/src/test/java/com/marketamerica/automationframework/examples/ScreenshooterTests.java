package com.marketamerica.automationframework.examples;

import com.marketamerica.automation.utilities.webdriver.WebDriverBrowser;
import com.marketamerica.automation.utilities.webdriver.WebDriverType;
import com.marketamerica.automationframework.tools.reporting.ScreenShooter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

/**
 * Tests for Screenshooter
 */
public class ScreenshooterTests {

	  @Test
	    public void testDesktopScreenShot() {
		  ScreenShooter.captureDesktopScreenshot();
	    }
	  
	  @Test
	    public void testScreehshotForIE() {
		  WebDriverType driverType = new WebDriverType();
		  driverType.setBrowser(WebDriverBrowser.INTERNET_EXPLORER);
		  DesiredCapabilities capability  = new DesiredCapabilities();
		  capability.setBrowserName(driverType.getBrowser().toString());
		  WebDriver driver = new InternetExplorerDriver(capability);
		  driver.navigate().to("staging.unfranchise.com");
		  ScreenShooter.capture(driver);
	    }
	  
	  @Test
	    public void testSaveDOM() {
		  WebDriverType driverType = new WebDriverType();
		  driverType.setBrowser(WebDriverBrowser.INTERNET_EXPLORER);
		  DesiredCapabilities capability  = new DesiredCapabilities();
		  capability.setBrowserName(driverType.getBrowser().toString());
		  WebDriver driver = new InternetExplorerDriver(capability);
		  driver.navigate().to("staging.unfranchise.com");
		  ScreenShooter.saveDOM(driver);
	    }


}
