package com.marketamerica.automationframework.validationlistener;

import com.marketamerica.automationframework.tools.reporting.ScreenShooter;
import com.marketamerica.automationframework.tools.reporting.StepInfo;
import com.marketamerica.automationframework.tools.reporting.Validate;
import com.marketamerica.automationframework.tools.testmanagement.TestCases;
import org.testng.annotations.Test;

public class ValidationExampleTest extends TestCases {

    @Test
    public void testValidationCount1() {
        Validate.validateEquals(10.2, 10.4, "should fail");
        StepInfo.setDisplayStepInfo(false);
        Validate.validateEquals(10.2, 10.2, "should pass");
        StepInfo.setDisplayStepInfo(true);
        Validate.validateEquals(10, 10, "should pass");
        ScreenShooter.capture(driver);
    }

    @Test
    public void testValidationCount2() {
        StepInfo.setDisplayStepInfo(false);
        Validate.validateEquals(10, 10, "should pass");
    }

    @Test
    public void testValidationCount3() {
        Validate.validateEquals(10.1, 10.1, "should pass");
    }

    @Test
    public void testValidationCount4() {
        StepInfo.setDisplayStepInfo(false);
        Validate.validateTrue(false, "should fail");
        StepInfo.setDisplayStepInfo(false);
        Validate.validateTrue(false, "should fail");
        StepInfo.setDisplayStepInfo(true);
        Validate.validateTrue(false, "should fail");
        Validate.validateTrue(true, "should pass");
    }


    @Override
    protected String getBaseURL() {
        return "www.google.com";
    }

}
