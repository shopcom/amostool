package com.marketamerica.automationframework.datamanagement;

import com.marketamerica.automation.testdata.address.HongKongAddress;
import com.marketamerica.automation.testdata.attributes.HongKongShippingRegion;
import com.marketamerica.automation.testdata.attributes.StoreClassification;
import com.marketamerica.automation.utilities.helpers.ReflectiveUtilities;
import com.marketamerica.automationframework.tools.datamanagement.XMLDataManager;
import com.marketamerica.automationframework.tools.factories.XMLDataManagerFactory;
import org.testng.annotations.Test;

import java.util.Locale;

import static org.testng.Assert.assertEquals;

/**
 * Created by javierv on 8/28/2014.
 */
public class DataManagerTests {

    @Test
    public void getMAStoreByMAProduct() {
        XMLDataManagerFactory dataFactory = new XMLDataManagerFactory();
        XMLDataManager dataManager = dataFactory.liveEnvironmentFactory.createUnitedStatesEnglishDataManager();
        String name = dataManager.getProductList().getProduct("MAProductOne").getStore().getName();
        StoreClassification ma = dataManager.getProductList().getProduct("MAProductOne").getStore().getClassification();
        assertEquals(name, "Market America");
        assertEquals(ma, StoreClassification.MA);
    }

    @Test
    public void getMORStoreByMORProduct() {
        XMLDataManagerFactory dataFactory = new XMLDataManagerFactory();
        XMLDataManager dataManager = dataFactory.liveEnvironmentFactory.createUnitedStatesEnglishDataManager();
        String name = dataManager.getProductList().getProduct("MORProductOne").getStore().getName();
        StoreClassification ma = dataManager.getProductList().getProduct("MORProductOne").getStore().getClassification();
        assertEquals(name, "UnbeatableSale");
        assertEquals(ma, StoreClassification.MOR);
    }

    @Test
    public void getMAProductNoStore() {
        XMLDataManagerFactory dataFactory = new XMLDataManagerFactory();
        XMLDataManager dataManager = dataFactory.liveEnvironmentFactory.createUnitedStatesEnglishDataManager();
        String name = dataManager.getProductList().getProduct("ProductNoStore").getStore().getName();
        StoreClassification ma = dataManager.getProductList().getProduct("ProductNoStore").getStore().getClassification();
        assertEquals(name, "Market America");
        assertEquals(ma, StoreClassification.MA);
    }

    @Test(description = "Test that we can set a custom locale for a shopper")
    public void getTaiwaneseShopperLocale() {
        XMLDataManagerFactory dataFactory = new XMLDataManagerFactory();
        XMLDataManager dataManager = dataFactory.liveEnvironmentFactory.createUnitedStatesEnglishDataManager();
        Locale locale = dataManager.getShopperList().getShopper("testChineseShopper").getLocale();
        assertEquals(locale.getLanguage(), "ch");
        assertEquals(locale.getDisplayCountry(), "Taiwan");
    }


    @Test(description = "Test ability to resolve Hong Kong Shipping Address via XML Deserialization")
    public void getHongKongShippingRegion() {
        final XMLDataManagerFactory dataFactory = new XMLDataManagerFactory();
        final XMLDataManager dataManager = dataFactory.liveEnvironmentFactory.createUnitedStatesEnglishDataManager();
        final HongKongAddress address = (HongKongAddress) dataManager.getAddressList().getAddress("Hong Kong Address in Discovery Bay");

        final String building = address.getBuilding();
        final String floor = address.getFloor();
        final String flat = address.getFlat();
        final HongKongShippingRegion region = address.getRegion();
        final String street = address.getStreet();
        final String city = address.getCity();

        assertEquals(building, "Building");
        assertEquals(floor, "Floor");
        assertEquals(flat, "Flat");
        assertEquals(region, HongKongShippingRegion.DISCOVERY_BAY);
        assertEquals(street, "Street");
        assertEquals(city, "City");
    }

    @Test(description = "Test when the country is unspecified that the path returned in Global")
    public void testUnClassifiedCountryPath() {
        final XMLDataManagerFactory dataFactory = new XMLDataManagerFactory();
        final XMLDataManager dataManager = dataFactory.stagingEnvironmentFactory.createUnClassifiedEnglishDataManager();
        String actual = ReflectiveUtilities.invokeInheritedMethod(dataManager, "getPathToDataByCountry").toString();
        String expected = "/testdata/global/";
        assertEquals(actual, expected);

        actual = ReflectiveUtilities.invokeInheritedMethod(dataManager, "getPathToDataByEnvironmentAndCountry").toString();
        expected = "/testdata/global/staging/";
        assertEquals(actual, expected);
    }


}
