package com.marketamerica.automationframework.datamanagement.dataproviders;

import com.marketamerica.automation.annotations.TestDoc;
import com.marketamerica.automation.testdata.*;
import com.marketamerica.automation.testdata.address.HongKongAddress;
import com.marketamerica.automation.testdata.address.MexicanAddress;
import com.marketamerica.automation.testdata.attributes.HongKongShippingRegion;
import com.marketamerica.automation.utilities.enums.TestClassification;
import com.marketamerica.automationframework.tools.datamanagement.dataproviders.ExcelDataDrivenProvider;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.testng.Assert.*;

/**
 * Created by Javier L. Velasquez on 10/10/2014.
 */
public class ExcelDataProviderTests{

    @Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "ShopperProductAndOther")
    public void testShopperProductOther(Shopper shopper, Product product, Map<String, String> parameters) {
        Address primaryAddress = shopper.getAddress();
        assertNotNull(shopper);
        assertNotNull(product);
        assertNotNull(primaryAddress);
        assertNotNull(parameters);

        assertNotNull(shopper.getPcID());
        assertNotNull(shopper.getEmail());
        assertNotNull(shopper.getPassword());
        assertNotNull(shopper.getClassification());
        assertNotNull(product.getSku());
        assertNotNull(product.getDistributorCost());
        assertNotNull(product.getRetailCost());

        assertNotNull(primaryAddress.getStreet());
        assertNotNull(primaryAddress.getCity());
        assertNotNull(primaryAddress.getStreet());
        assertNotNull(primaryAddress.getZipCode());
        assertNotNull(primaryAddress.getCountry());

    }

    @Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "ShopperAdditionalAddressProductsAndOther")
    public void testDataDrivenMethodTwo(Shopper shopper, Address secondaryAddress, List<Product> products, Map<String, String> parameters) {
        Portal portal = shopper.getPortal();
        Distributor distributor = shopper.getDistributor();
        Address primaryAddress = shopper.getAddress();

        assertNotNull(shopper);
        assertNotNull(portal);
        assertNotNull(distributor);
        assertNotNull(primaryAddress);
        assertNotNull(secondaryAddress);
        assertNotNull(products);
        assertNotNull(parameters);

        assertNotNull(shopper.getEmail());
        assertNotNull(shopper.getPassword());
        assertNotNull(shopper.getClassification());
        assertNotNull(shopper.getReferralShopperClassification());

        assertNotNull(distributor.getEmail());
        assertNotNull(distributor.getRepresentativeId());
        assertNotNull(distributor.getRepresentativePassword());

        assertNotNull(portal.getMinimumShippingCharge());
        assertNotNull(portal.getShopConsultantName());
        assertNotNull(portal.getShippingCharge());

        assertNotNull(products.get(0).getDistributorCost());
        assertNotNull(products.get(0).getRetailCost());
        assertNotNull(products.get(0).getSku());
        assertNotNull(products.get(0).getStore());
        assertNotNull(distributor.getAddress().getCountry());
    }

    @Test(description = "Test that we can set return the Custom Parameters Excel")
    public void getCustomParametersExcel(ITestContext context, Method method) {
        Object[][] customParameters = ExcelDataDrivenProvider.getCustomParameters(context, method);
        assertNotNull(customParameters);
    }

    @Test(description = "Test that we can set return the Custom Parameters Excel in a case insensitive matter")
    public void getCaseInsensitiveParameters(ITestContext context, Method method) {
        Object[][] customParameters = ExcelDataDrivenProvider.getCustomParameters(context, method);
        Map<String, String> parameters = (Map<String, String>) customParameters[0][0];
        assertTrue(parameters.get("column1").equals("Value"));
        assertTrue(parameters.get("Column2").equals("Value"));
        assertTrue(parameters.get("COLUMN1").equals("Value"));
        assertTrue(parameters.get("COLUMN2").equals("Value"));

    }

    @Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "ShopperProductAndOther")
    public void testMexicanAddress(Shopper shopper, Product product, Map<String, String> parameters) {
        Address address = shopper.getAddress();
        assertTrue(address instanceof MexicanAddress, "Expected address to be instance of Mexican Address");
        MexicanAddress mexicanAddress = (MexicanAddress) address;
        assertEquals(mexicanAddress.getDistrict(), "District");
        assertEquals(mexicanAddress.getNeighborhood(), "Neighborhood");
        assertEquals(mexicanAddress.getZipCode(), "zip");
    }

    @Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "ShopperProductAndOther")
    public void testHongKongAddress(Shopper shopper, Product product, Map<String, String> parameters) {
        Address address = shopper.getAddress();
        assertTrue(address instanceof HongKongAddress, "Expected address to be instance of Hong Kong Address");
        HongKongAddress hongKongAddress = (HongKongAddress) address;

        // Assert ability to populate standard Hong Kong Address fields
        assertEquals(hongKongAddress.getFlat(), "Flat");
        assertEquals(hongKongAddress.getRegion(), HongKongShippingRegion.MA_WAN);
        assertEquals(hongKongAddress.getFloor(), "Floor");
        assertEquals(hongKongAddress.getBuilding(), "Building");

        // Assert that when zip is not defined, zip is null
        assertNull(hongKongAddress.getZipCode(), "zip");
    }

    @TestDoc
    @Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "customParameters")
    public void testSkipRegressionTestsC(Map<String, String> parameters) {
        TestClassification actual = TestClassification.parse(parameters.get("Classification"));
        TestClassification expected = TestClassification.SMOKE;
        if (actual.equals(TestClassification.REGRESSION)) {
            System.out.println("");
        }
        assertTrue(actual.equals(expected), String.format(String.format("Expected: %s, Actual: %s", expected, actual)));

    }

    @TestDoc
    @Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "shoppers")
    public void testSkipRegressionTestsS(Shopper shopper, Map<String, String> parameters) {
        TestClassification actual = TestClassification.parse(parameters.get("Classification"));
        TestClassification expected = TestClassification.SMOKE;
        assertTrue(actual.equals(expected), String.format(String.format("Expected: %s, Actual: %s", expected, actual)));
    }


    @TestDoc
    @Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "ShopperProductAndOther")
    public void testSkipRegressionTestsSPAO(Shopper shopper, Product product, Map<String, String> parameters) {
        TestClassification actual = TestClassification.parse(parameters.get("Classification"));
        TestClassification expected = TestClassification.SMOKE;
        assertTrue(actual.equals(expected), String.format(String.format("Expected: %s, Actual: %s", expected, actual)));
    }

    @TestDoc
    @Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "DistributorCreationProcess")
    public void testSkipRegressionTestsDCP(DistributorApplication distributorApplication, Map<String, String>
            parameters) {
        TestClassification actual = TestClassification.parse(parameters.get("Classification"));
        TestClassification expected = TestClassification.SMOKE;
        assertTrue(actual.equals(expected), String.format(String.format("Expected: %s, Actual: %s", expected, actual)));
    }


    @TestDoc
    @Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "ShopperAndOther")
    public void testSkipRegressionTestsSAO(Shopper shopper, Map<String, String> parameters) {
        TestClassification actual = TestClassification.parse(parameters.get("Classification"));
        TestClassification expected = TestClassification.SMOKE;
        assertTrue(actual.equals(expected), String.format(String.format("Expected: %s, Actual: %s", expected, actual)));
    }

    @TestDoc
    @Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "ShopperAdditionalAddressProductsAndOther")
    public void testSkipRegressionTestsSAAPAO(Shopper shopper, Address address, Collection<Product> products,
                                              Map<String, String>
                                                      parameters) {
        TestClassification actual = TestClassification.parse(parameters.get("Classification"));
        TestClassification expected = TestClassification.SMOKE;
        assertTrue(actual.equals(expected), String.format(String.format("Expected: %s, Actual: %s", expected, actual)));
    }

    @TestDoc
    @Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "ShopperCreditProductAndOther")
    public void testCreditCardDP(Shopper shopper, CreditCard card, Product product, Map<String, String>
            parameters) {
        assertNotNull(shopper);
        assertNotNull(card);
        assertNotNull(product);
        assertNotNull(parameters);


    }


}
