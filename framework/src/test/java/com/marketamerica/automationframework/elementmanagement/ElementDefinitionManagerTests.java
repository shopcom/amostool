package com.marketamerica.automationframework.elementmanagement;

import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.util.HashSet;
import java.util.Set;

import static org.testng.Assert.assertEquals;

/**
 * Created by javierv on 8/7/2014.
 */
public class ElementDefinitionManagerTests {
    private ElementDefinitionManager elementDefinitionManager;

    @BeforeSuite
    public void configureMethod() {
        elementDefinitionManager = new PropertyElementDefinitionManager(Environment.STAGING, Country.UNITED_STATES);
    }

    @Test
    public void testXPathElementDefinitionWithOneIterpolation() {
        final Set<By> actual = elementDefinitionManager.findElements("testXPathElementDefinitionWithOneIterpolation", "test");
        final By xpath = By.xpath("//a[contains(text(), 'test')");

        final Set<By> expected = new HashSet<>();
        expected.add(xpath);

        assertEquals(actual, expected);
    }

    @Test
    public void testXPathElementDefinitionWithTwoIterpolations() {
        final Set<By> actual = elementDefinitionManager.findElements("testXPathElementDefinitionWithTwoIterpolations", "testOne", "testTwo");
        final By xpath = By.xpath("//li[contains(text(), 'testOne')//a[contains(text(), 'testTwo')]");
        final Set<By> expected = new HashSet<>();
        expected.add(xpath);

        assertEquals(actual, expected);
    }

    @Test
    public void testCssElementDefinition() {
        final Set<By> actual = elementDefinitionManager.findElements("testCssElementDefinition");
        final By css = By.cssSelector(".test a");
        final Set<By> expected = new HashSet<>();
        expected.add(css);

        assertEquals(actual, expected);
    }

    @Test
    public void testXPathElementDefinitionWithOneIterpolationTwoColonsInternalText() {
        final Set<By> actual = elementDefinitionManager.findElements("testXPathElementDefinitionWithOneIterpolationTwoColonsInternalText");
        final By xpath = By.xpath("//test[contains(text(), 'test: ')]");
        final Set<By> expected = new HashSet<>();
        expected.add(xpath);
        assertEquals(actual, expected);
    }

    @Test
    public void testXPathElementDefinitionWithOneIterpolationTwoColonsExternalText() {
        final Set<By> actual = elementDefinitionManager.findElements("testXPathElementDefinitionWithOneIterpolationTwoColonsExternalText", "test: ");
        final By xpath = By.xpath("//test[contains(text(), 'test: ')]");
        final Set<By> expected = new HashSet<>();
        expected.add(xpath);
        assertEquals(actual, expected);
    }

}
