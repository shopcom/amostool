package com.marketamerica.automationframework.testmanagement;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.excel.ExcelWorkBookWriter;
import com.marketamerica.automation.utilities.helpers.ReflectiveUtilities;
import com.marketamerica.automationframework.pages.WebContent;
import com.marketamerica.automationframework.tools.testmanagement.OrderingReporter;
import com.marketamerica.automationframework.tools.testmanagement.TestBuilder;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

/**
 * Created by javierv on 8/11/2014.
 */
public class OrderingReporterTests {

    private static final Environment ENVIRONMENT = Environment.STAGING;
    private static final Country COUNTRY = Country.UNITED_STATES;
    private WebDriver driver;

    @BeforeMethod(alwaysRun = true)
    public void setupTest() {
        Configuration.initialize(ENVIRONMENT);
        OrderingReporter.initialize(ENVIRONMENT, COUNTRY);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDownTest() {
        ReflectiveUtilities.setField(new OrderingReporter(), "configured", false);
    }


    @Test(description = "Test the ability to store an order number by test name")
    public void testOrderRecording(ITestContext testContext, Method method) {
        TestBuilder testBuilder = new TestBuilder(testContext);
        driver = testBuilder.getWebDriver(true);

        final String orderNumber = "1541234123";
        OrderingReporter.recordOrder(orderNumber, driver);

        final ExcelWorkBookWriter workBook = (ExcelWorkBookWriter) ReflectiveUtilities.getFieldValue(new OrderingReporter(), "workbook");
        final List<String> actual = workBook.getRowData(1);
        final List<String> expected = Arrays.asList(
                ReflectiveUtilities.getFullMethodName(method),
                orderNumber,
                WebContent.getCapabilities(driver).getBrowserName()
        );
        driver.quit();
        assertTrue(actual.containsAll(expected), String.format("Expected %s to contain %s", actual, expected));
    }

    @Test
    public void testOrderCountNoOrders() {
        final int orderCount = Integer.parseInt(ReflectiveUtilities.getFieldValue(new OrderingReporter(), "orders").toString());
        assertEquals(orderCount, 0);
   }

    @Test
    public void testReportName() {
        final ExcelWorkBookWriter workBook = (ExcelWorkBookWriter) ReflectiveUtilities.getFieldValue(new OrderingReporter(), "workbook");
        final String actualReportName = String.valueOf(ReflectiveUtilities.getFieldValue(workBook, "fileName"));

        final String expectedFormat = String.format("order-report-%s-%s", Configuration.getProjectName(), ENVIRONMENT.getAbbreviation().toLowerCase());
        assertTrue(actualReportName.contains(expectedFormat), String.format("Expected %s to contain %s", actualReportName, expectedFormat));
    }

}
