package com.marketamerica.automationframework.testmanagement;

import com.google.common.base.Verify;
import com.marketamerica.automation.annotations.TestDoc;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automationframework.tools.datamanagement.dataproviders.ExcelDataDrivenProvider;
import com.marketamerica.automationframework.tools.testmanagement.RecordableTests;
import org.testng.annotations.Test;

import java.util.Map;


public class TestLinkReporterTests extends RecordableTests {

    @Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "ShopperAndOther")
    @TestDoc
    public void recordMultipleResults(Shopper shopper, Map<String, String> parameters) {
        Verify.verify(true);
    }

    @Test(dataProviderClass = ExcelDataDrivenProvider.class, dataProvider = "ShopperAndOther")
    @TestDoc(ids = "ASQA-992")
    public void recordAnnotatedResults(Shopper shopper, Map<String, String> parameters) {
        Verify.verify(true);
    }


}
