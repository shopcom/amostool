package com.marketamerica.automationframework.testmanagement;

import com.marketamerica.automation.utilities.helpers.NumberUtilities;
import com.marketamerica.automation.utilities.helpers.ReflectiveUtilities;
import com.marketamerica.automationframework.tools.testmanagement.TestSummaryLogger;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import java.util.Random;

import static org.testng.Assert.assertEquals;

/**
 * Created by javierv on 8/23/2014.
 */
public class TestSummaryLoggerTests {

    @AfterMethod
    public void tearDownMethod() {
        ReflectiveUtilities.setField(new TestSummaryLogger(), "total", 0);
        ReflectiveUtilities.setField(new TestSummaryLogger(), "failed", 0);
        ReflectiveUtilities.setField(new TestSummaryLogger(), "passed", 0);
        ReflectiveUtilities.setField(new TestSummaryLogger(), "skipped", 0);
    }

    @Test
    public void testNoResults() {
        int total = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "total");
        int failed = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "failed");
        int passed = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "passed");
        int skipped = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "skipped");
        assertEquals(total, 0);
        assertEquals(failed, 0);
        assertEquals(passed, 0);
        assertEquals(skipped, 0);
    }

    @Test
    public void testOneSuccessResult() {
        TestSummaryLogger.log(ITestResult.SUCCESS);
        int total = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "total");
        int failed = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "failed");
        int passed = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "passed");
        int skipped = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "skipped");
        assertEquals(total, 1);
        assertEquals(failed, 0);
        assertEquals(passed, 1);
        assertEquals(skipped, 0);
    }

    @Test
    public void testOneFailureResult() {
        TestSummaryLogger.log(ITestResult.FAILURE);
        int total = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "total");
        int failed = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "failed");
        int passed = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "passed");
        int skipped = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "skipped");
        assertEquals(total, 1);
        assertEquals(failed, 1);
        assertEquals(passed, 0);
        assertEquals(skipped, 0);
    }


    @Test
    public void testOneSkipResult() {
        TestSummaryLogger.log(ITestResult.SKIP);
        int total = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "total");
        int failed = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "failed");
        int passed = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "passed");
        int skipped = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "skipped");
        assertEquals(total, 1);
        assertEquals(failed, 0);
        assertEquals(passed, 0);
        assertEquals(skipped, 1);
    }

    @Test
    public void testTwoResults() {
        TestSummaryLogger.log(ITestResult.SKIP);
        TestSummaryLogger.log(ITestResult.SKIP);
        int total = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "total");
        int failed = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "failed");
        int passed = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "passed");
        int skipped = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "skipped");
        assertEquals(total, 2);
        assertEquals(failed, 0);
        assertEquals(passed, 0);
        assertEquals(skipped, 2);
    }


    @Test
    public void testOneHundredResults() {
        int expectedTotal = 0;
        int expectedPassed = 0;
        int expectedSkipped = 0;
        int expectedFailure = 0;
        for (int i = 0; i < 100; ++i) {
            ++expectedTotal;
            int result = NumberUtilities.randomInteger(new Random(), 0, 3);
            switch (result) {
                case ITestResult.SUCCESS:
                    ++expectedPassed;
                    break;

                case ITestResult.SKIP:
                    ++expectedSkipped;
                    break;

                case ITestResult.FAILURE:
                    ++expectedFailure;
                    break;
            }
            TestSummaryLogger.log(result);
        }

        int actualTotal = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "total");
        int actualFailure = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "failed");
        int actualPassed = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "passed");
        int actualSkipped = (int) ReflectiveUtilities.getFieldValue(new TestSummaryLogger(), "skipped");
        assertEquals(actualTotal, expectedTotal);
        assertEquals(actualFailure, expectedFailure);
        assertEquals(actualPassed, expectedPassed);
        assertEquals(actualSkipped, expectedSkipped);
    }

}
