package com.marketamerica.automationframework.testmanagement;

import static com.marketamerica.automation.utilities.helpers.CollectionUtilities.createList;
import static org.testng.Assert.assertTrue;

import java.lang.reflect.Method;
import java.util.List;

import org.testng.ITestContext;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.excel.ExcelWorkBookWriter;
import com.marketamerica.automation.utilities.helpers.ReflectiveUtilities;
import com.marketamerica.automationframework.tools.testmanagement.PreferredCustomerReporter;

/**
 * This class holds the unit tests for the PreferredCustomerReporter
 * Created by Javier L. Velasquez on 12/24/2014.
 */
public class PreferredCustomerReporterTests {

    private static final Environment ENVIRONMENT = Environment.STAGING;

    @BeforeMethod(alwaysRun = true)
    public void setupTest() {
        Configuration.initialize(ENVIRONMENT);
        PreferredCustomerReporter.initialize(ENVIRONMENT);
    }

    @Test(description = "Test the ability to record a PC's info to the Preferred Customer Reporter")
    public void testPCRecording(ITestContext testContext, Method method) {

        Shopper shopper = new Shopper();
        shopper.setEmail("543195059newix@testing.com");
        PreferredCustomerReporter.record(shopper);

        final ExcelWorkBookWriter workBook = (ExcelWorkBookWriter) ReflectiveUtilities.getFieldValue(new PreferredCustomerReporter(), "workbook");
        final List<String> actual = workBook.getRowData(1);

        // Don't check for the password as this might change
        final List<String> expected = createList("Bobby", "Testing", "543195059newix@testing.com", "Custom Mini-Websites",
                "6253066",
                "116667168008@yahoo.com", "6253066", "Active", "USA", "266151620", "1000173794", "Market America", "0");

        assertTrue(actual.containsAll(expected), String.format("Expected %s to contain %s", actual, expected));
    }


    @Test(description = "Test the report name format")
    public void testReportName() {
        final ExcelWorkBookWriter workBook = (ExcelWorkBookWriter) ReflectiveUtilities.getFieldValue(new PreferredCustomerReporter(), "workbook");
        final String actualReportName = String.valueOf(ReflectiveUtilities.getFieldValue(workBook, "fileName"));

        final String expectedFormat = String.format("newly-registered-pcs-%s-%s", Configuration.getProjectName(), ENVIRONMENT.getAbbreviation().toLowerCase());
        assertTrue(actualReportName.contains(expectedFormat), String.format("Expected %s to contain %s", actualReportName, expectedFormat));
    }


}
