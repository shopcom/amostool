package com.marketamerica.automationframework.pages;

import static org.testng.Assert.assertEquals;

import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.marketamerica.automationframework.tools.testmanagement.TestBuilder;

/**
 * Tests for WebContent
 * Created by javierv on 8/18/2014.
 */
public class WebContentTests {

    @Test
    public void testGetCapabilitiesFirefoxDriver(ITestContext testContext) {
        
        final TestBuilder testBuilder = new TestBuilder(testContext);
        WebDriver driver = testBuilder.getWebDriver(true);
        final String actual = WebContent.getCapabilities(driver).getBrowserName();
        final String expected = testContext.getCurrentXmlTest().getAllParameters().get("browser");
        assertEquals(actual, expected);
        driver.quit();
    }

    @Test
    public void testGetCapabilitiesHtmlUnitDriver(ITestContext testContext) {
        final String browser = "htmlunit";
        final Map<String, String> parameters = testContext.getCurrentXmlTest().getAllParameters();
        parameters.put("browser", browser);
        testContext.getCurrentXmlTest().setParameters(parameters);

        final TestBuilder testBuilder = new TestBuilder(testContext);
        final WebDriver driver = testBuilder.getWebDriver(false);
        final String actual = WebContent.getCapabilities(driver).getBrowserName();
        assertEquals(actual, browser);
        driver.quit();
    }
}
