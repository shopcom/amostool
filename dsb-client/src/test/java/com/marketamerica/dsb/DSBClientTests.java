package com.marketamerica.dsb;

import com.marketamerica.automation.testdata.Distributor;
import com.marketamerica.automation.testdata.Placement;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.DistributorPin;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;
import java.util.Set;

import static org.testng.Assert.*;

/**
 * DSB Client Unit Tests
 * Created by javierv on 7/31/2014.
 */
public class DSBClientTests {
    final static Logger logger = LogManager
            .getLogger(DSBClientTests.class);

    @BeforeMethod
    public void setupTest() {

    }

    @Test(dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
    public void testGetOrderInfo(DSBTestData dsBTestData) {
        final DSBClient dsbClient = new DSBClient(dsBTestData.getEnvironment(), dsBTestData.getLocale());
        assertNotNull(dsbClient.getOrderInfo(dsBTestData.getDistributor(), dsBTestData.getOrders().get(0)), "Expected to return order info");
    }

    @Test(dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
    public void testSetDistributorPin(DSBTestData dsBTestData) {
        final DSBClient dsbClient = new DSBClient(dsBTestData.getEnvironment(), dsBTestData.getLocale());

        DistributorPin expected;
        DistributorPin actual;

        expected = DistributorPin.R;
        dsbClient.setDistributorPin(dsBTestData.getDistributor(), expected);
        actual = dsbClient
                .getDistributorPin(dsBTestData.getDistributor());
        assertEquals(expected, actual, String.format("Expected: %s, Actual: %s", expected, actual));
        expected = DistributorPin.D;
        dsbClient.setDistributorPin(dsBTestData.getDistributor(), expected);
        actual = dsbClient
                .getDistributorPin(dsBTestData.getDistributor());
        assertEquals(expected, actual, String.format("Expected: %s, Actual: %s", expected, actual));
    }

    @Test(dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
    public void testGetDistributorPin(DSBTestData dsBTestData) {
        final DSBClient dsbClient = new DSBClient(dsBTestData.getEnvironment(), dsBTestData.getLocale());

        DistributorPin expected = DistributorPin.D;
        dsbClient.setDistributorPin(dsBTestData.getDistributor(), expected);
        DistributorPin actual = dsbClient.getDistributorPin(dsBTestData.getDistributor());
        assertEquals(actual, expected, String.format("Expected: %s, Actual: %s", expected, actual));
    }


    @Test(dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
    public void testGetDistributorInfo(DSBTestData dsBTestData) {
        final DSBClient dsbClient = new DSBClient(dsBTestData.getEnvironment(), dsBTestData.getLocale());
        assertNotNull(dsbClient.getDistProfile(dsBTestData.getDistributor()), "Expected to return distributor info");
    }

    @Test(dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
    public void testGetDistOriginalPassword(DSBTestData dsBTestData) {
        final DSBClient dsbClient = new DSBClient(dsBTestData.getEnvironment(), dsBTestData.getLocale());
        Distributor distributor = dsBTestData.getDistributor();

        String actual = dsbClient.getOriginalDistributorPassword(distributor);
        String expected = distributor.getOriginalPassword();
        assertEquals(actual, expected, String.format("Expected: %s, Actual: %s", expected, actual));
    }

    @Test(dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
    public void testGetRepPassword(DSBTestData dsBTestData) {
        final DSBClient dsbClient = new DSBClient(dsBTestData.getEnvironment(), dsBTestData.getLocale());
        Distributor distributor = dsBTestData.getDistributor();
        String actual = dsbClient.getRepPassword(distributor);
        String expected = distributor.getPassword();
        assertEquals(actual, expected, String.format("Expected: %s, Actual: %s", expected, actual));
    }

    @Test(dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
    public void testGetDistributorCOA(DSBTestData dsBTestData) {
        final DSBClient dsbClient = new DSBClient(dsBTestData.getEnvironment(), dsBTestData.getLocale());
        Distributor distributor = dsBTestData.getDistributor();
        BigDecimal actual = dsbClient.getDistributorCOA(distributor);
        assertTrue(actual.compareTo(BigDecimal.ZERO) >= 0);
    }

    @Test(dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "taxEnabledCountries")
    public void testGetValidTaxID(DSBTestData dsBTestData) {
        final DSBClient dsbClient = new DSBClient(dsBTestData.getEnvironment(), dsBTestData.getLocale());
        String actual = dsbClient.getValidTaxID(dsBTestData.getLocale());
        assertTrue(actual != null);
        assertFalse(actual.isEmpty());
        assertTrue(actual.length() > 0);
    }

    @Test(dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
    public void testGetValidTaxIDs(DSBTestData dsBTestData) {
        final DSBClient dsbClient = new DSBClient(dsBTestData.getEnvironment(), dsBTestData.getLocale());

        Map<Country, String> values = dsbClient.getValidTaxIDs();
        Set<Country> taxIdCountries = values.keySet();

        assertTrue(taxIdCountries.size() == DSBClient.supportedLocales.length, String.format("Expected %s, Actual: %s",
                DSBClient.supportedLocales.length, taxIdCountries.size())
        );

        for (Country country : taxIdCountries) {
            String actual = values.get(country);

            logger.debug(String.format("For Country (%s), expected to resolve valid tax id: %s", country, actual));
            assertTrue(actual != null, String.format("For Country (%s), expected to resolve valid tax id: %s", country, actual));
            assertFalse(actual.isEmpty(), String.format("For Country (%s), expected to resolve valid tax id: %s", country, actual));
            assertTrue(actual.length() > 0, String.format("For Country (%s), expected to resolve valid tax id: %s", country, actual));
        }

    }

    @Test(description = "Get a distributor's open placements",
            dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
    public void getOpenPlacements(DSBTestData dsBTestData) {
        final DSBClient dsbClient = new DSBClient(dsBTestData.getEnvironment(), dsBTestData.getLocale());

        Collection<Placement> openPlacements;
        String leg;

        leg = "002";
        openPlacements = dsbClient.getOpenPlacements(dsBTestData.getDistributor(), leg);
        assertTrue(
                openPlacements.size() > 0 && openPlacements.iterator().next().getUnFranchiseId() != null,
                String.format(
                        "Expected %s to have open placements on his %s leg",
                        dsBTestData.getDistributor(), leg
                )
        );
        // Ensure that all placement values are populated
        for (Placement placement : openPlacements) {
            assertNotNull(placement.getUnFranchiseId());
            assertNotNull(placement.getExtension());
            assertNotNull(placement.getLeg());
        }

        leg = "003";
        openPlacements = dsbClient.getOpenPlacements(dsBTestData.getDistributor(), leg);
        assertTrue(
                openPlacements.size() > 0 && openPlacements.iterator().next().getUnFranchiseId() != null,
                String.format(
                        "Expected %s to have open placements on his %s leg",
                        dsBTestData.getDistributor(), leg
                )
        );

        // Ensure that all placement values are populated
        for (Placement placement : openPlacements) {
            assertNotNull(placement.getUnFranchiseId());
            assertNotNull(placement.getExtension());
            assertNotNull(placement.getLeg());
        }

        leg = "001";
        openPlacements = dsbClient.getOpenPlacements(dsBTestData.getDistributor(), leg);
        assertTrue(
                openPlacements.size() > 0 && openPlacements.iterator().next().getUnFranchiseId() != null,
                String.format(
                        "Expected %s to have open placements on his %s leg",
                        dsBTestData.getDistributor(), leg
                )
        );

        // Ensure that all placement values are populated
        for (Placement placement : openPlacements) {
            assertNotNull(placement.getUnFranchiseId());
            assertNotNull(placement.getExtension());
            assertNotNull(placement.getLeg());
        }

    }

    @Test(description = "Populates a distributor's id",
            dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
    public void testResolveDistributorID(DSBTestData dsBTestData) {
        final DSBClient dsbClient = new DSBClient(dsBTestData.getEnvironment(), dsBTestData.getLocale());
        Distributor distributor = dsBTestData.getDistributor();
        String expected = distributor.getDistributorId();
        // Set the distributor's id to null so we can cleanly repopulate it
        distributor.setDistributorId(null);
        // Update the distributor using the DSB
        dsbClient.resolveDistributorID(distributor);
        String actual = distributor.getDistributorId();
        assertEquals(actual, expected, String.format("Expected: %s, Actual: %s", actual, expected));
        // Attempt to update a distributor's id that has already been populated.
        dsbClient.resolveDistributorID(distributor);
        actual = distributor.getDistributorId();
        assertEquals(actual, expected, String.format("Expected: %s, Actual: %s", actual, expected));

    }

    @Test(description = "Populates a shopper's pcid",
            dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
    public void testGetCustomerIDs(DSBTestData dsbTestData) {
        final DSBClient dsbClient = new DSBClient(dsbTestData.getEnvironment(), dsbTestData.getLocale());
        final Shopper shopper = dsbTestData.getShopper();

        String expectedPCID = shopper.getPcID();
        String expectedShopperID = shopper.getShopperID();
        dsbClient.resolveCustomerIDs(shopper);
        shopper.setPcID(null);
        shopper.setShopperID(null);
        // Update the shopper using the DSB
        dsbClient.resolveCustomerIDs(shopper);
        String actualPCID = shopper.getPcID();
        String actualShopperID = shopper.getShopperID();
        assertEquals(actualPCID, expectedPCID, String.format("Expected: %s, Actual: %s", actualPCID, expectedPCID));
        assertEquals(actualShopperID, expectedShopperID, String.format("Expected: %s, Actual: %s", actualShopperID, expectedShopperID));
        // Attempt to update a shopper's id that has already been populated.
        dsbClient.resolveCustomerIDs(shopper);
        actualPCID = shopper.getPcID();
        actualShopperID = shopper.getShopperID();
        assertEquals(actualPCID, expectedPCID, String.format("Expected: %s, Actual: %s", actualPCID, expectedPCID));
        assertEquals(actualShopperID, expectedShopperID, String.format("Expected: %s, Actual: %s", actualShopperID, expectedShopperID));
    }

    @Test(description = "Retrieve Shopper information",
            dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
    public void testGetPCProfile(DSBTestData dsbTestData) {
        final DSBClient dsbClient = new DSBClient(dsbTestData.getEnvironment(), dsbTestData.getLocale());
        final Shopper shopper = dsbTestData.getShopper();
        Map<String, String> profile = dsbClient.getPCProfile(shopper);
        assertFalse(profile.isEmpty(), String.format("Expected profile data to be returned: %s", profile));
    }

    @Test(description = "Retrieve Distributor's home country represented as a 3 letter code",
            dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
    public void testGetDistributorHomeCountry(DSBTestData dsbTestData) {
        final DSBClient dsbClient = new DSBClient(dsbTestData.getEnvironment(), dsbTestData.getLocale());
        final Distributor distributor = dsbTestData.getDistributor();
        String homeCountry = dsbClient.getDistributorHomeCountry(distributor);
        assertTrue(homeCountry.toUpperCase().equals(distributor.getLocale().getISO3Country().toUpperCase()), String.format
                ("Expected: \"%s\", Actual: \"%s\"", homeCountry, distributor.getLocale().getISO3Country()));

    }
    
    @Test(description = "Retrieve Distributor Inquiry Detail",
            dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
    public void testGetDistributorDetail(DSBTestData dsbTestData) {
        final DSBClient dsbClient = new DSBClient(dsbTestData.getEnvironment(), dsbTestData.getLocale());
        final Distributor distributor = dsbTestData.getDistributor();
        Map<String, String> profile = dsbClient.getDistInquiryDetail(distributor);
        assertFalse(profile.isEmpty(), String.format("Expected profile data to be returned: %s", profile));
    }
    
	@Test(description = "Cancel an Order", dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
	public void testCancelOrder(DSBTestData dsbTestData) {
		final DSBClient dsbClient = new DSBClient(dsbTestData.getEnvironment(), dsbTestData.getLocale());
		assertTrue(dsbClient.cancelOrder("19454066", "819672098"), "This order should already be cancelled");
	}

	@Test(description = "Get Order Queue data for an temp Order ID", dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
	public void testGetOrderQueueData(DSBTestData dsbTestData) {
		final DSBClient dsbClient = new DSBClient(dsbTestData.getEnvironment(), dsbTestData.getLocale());
		Map<String, String> orderData = dsbClient.getOrderQueueData("787374");
		assertFalse(orderData.isEmpty(),
				String.format("Expected to get Order Details  for this temp Order ID: %s", "787374"));
	}

	@Test(description = "Get Order Queue data for an temp Order ID", dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
	public void testGetOrderQueueDataAndCancelOrder(DSBTestData dsbTestData) {
		final DSBClient dsbClient = new DSBClient(dsbTestData.getEnvironment(), dsbTestData.getLocale());
		assertTrue(dsbClient.getOrderQueueDataAndCancelOrder("797532"), String.format(
				"Expected to get Order Details  for this temp Order ID: %s and Cancel the Order", "797532"));
	}
	

	@Test(description = "Get Order Queue Health Check", dataProviderClass = DSBDataDrivenProvider.class, dataProvider = "testData")
	public void testGetOrderQueueHealthCheck(DSBTestData dsbTestData) {
		final DSBClient dsbClient = new DSBClient(dsbTestData.getEnvironment(), dsbTestData.getLocale());
		Map<String, String> queueData = dsbClient.getOrderQueueHealthCheck();
		assertFalse(queueData.isEmpty(),
				String.format("Expected to get Queue Health Check Details "));
	}
    

}
