package com.marketamerica.dsb;

import com.marketamerica.automation.testdata.Distributor;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.utilities.enums.Environment;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Test Data used for DataDriven unit testing
* Created by javierv on 7/31/2014.
*/
public class DSBTestData {
    
    private Environment environment;
    private Distributor distributor;
    private List<String> orders;
    private Locale locale;
    private Shopper shopper;

    public DSBTestData() {
        this.orders = new ArrayList<>();
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }


    public void setDistributor(Distributor distributor) {
        this.distributor = distributor;
    }

    public void addOrder(final String order) {
        this.orders.add(order);
    }

    public List<String> getOrders() {
        return orders;
    }

    public Environment getEnvironment() {
        return environment;
    }

    public Distributor getDistributor() {
        return distributor;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Shopper getShopper() {
        return shopper;
    }

    public void setShopper(Shopper shopper) {
        this.shopper = shopper;
    }
}
