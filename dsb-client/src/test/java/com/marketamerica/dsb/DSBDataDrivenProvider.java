package com.marketamerica.dsb;

import com.marketamerica.automation.testdata.Distributor;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.utilities.enums.Environment;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * DSBClient DataDriven Provider class
 * Created by javierv on 7/31/2014.
 */
public class DSBDataDrivenProvider {

    static {
        generateTestData();
    }

    private static List<DSBTestData> dsbTestData;

    /**
     * Generate our test data. Data is not placed into Excel because it would be to much for basic unit tests..
      */
    private static void generateTestData() {
        dsbTestData = new ArrayList<>();
        dsbTestData.add(getUnitedStatesStagingTestData());
//        dsbTestData.add(getUnitedStatesLiveTestData()); // Uncomment this if you want to run Unit tests on Live .
    }

    /**
     * Populate our united states staging test data
     * @return
     */
    private static DSBTestData getUnitedStatesStagingTestData() {
        DSBTestData stagingDSBTestData = new DSBTestData();

        Distributor distributor = new Distributor();
        distributor.setDistributorId("061671566");
        distributor.setRepresentativeId("7328768");
        distributor.setPassword("testing");
        distributor.setOriginalPassword("VAUARM");
        distributor.setLocale(Locale.US);

        stagingDSBTestData.setEnvironment(Environment.STAGING);
        stagingDSBTestData.setDistributor(distributor);

        stagingDSBTestData.addOrder("19378395");
        stagingDSBTestData.setLocale(Locale.US);

        Shopper shopper = new Shopper();
        shopper.setEmail("908975173@testing.com");
        shopper.setPcID("1000132546");
        shopper.setShopperID("44044469");
        stagingDSBTestData.setShopper(shopper);

        return stagingDSBTestData;
    }
    
    /**
     * Populate our united states staging test data
     * @return
     */
    protected static DSBTestData getUnitedStatesLiveTestData() {
        DSBTestData stagingDSBTestData = new DSBTestData();

        Distributor distributor = new Distributor();
        distributor.setDistributorId("193404332");
        distributor.setRepresentativeId("2838399");
        distributor.setPassword("TESTING");
        distributor.setOriginalPassword("UVTCML");
        distributor.setLocale(Locale.US);

        stagingDSBTestData.setEnvironment(Environment.LIVE);
        stagingDSBTestData.setDistributor(distributor);

        stagingDSBTestData.addOrder("51354748");
        stagingDSBTestData.setLocale(Locale.US);

        Shopper shopper = new Shopper();
        shopper.setEmail("jadtest011515@test.com");
        shopper.setPcID("9003792");
        shopper.setShopperID("9003792");
        stagingDSBTestData.setShopper(shopper);

        return stagingDSBTestData;
    }

    @DataProvider(name ="taxEnabledCountries")
    public static Object[][] getTaxEnabledCountries(final ITestContext context,
                                       final Method method) {

        Locale[] supportedLocales = DSBClient.supportedLocales;
        final Object[][] distributors = new Object[supportedLocales.length][1];

        for (int i = 0; i < supportedLocales.length; ++i) {
            DSBTestData testData = new DSBTestData();
            testData.setLocale(supportedLocales[i]);
            testData.setEnvironment(Environment.STAGING);
            distributors[i][0] = testData;
        }
        return distributors;

    }

    @DataProvider(name = "testData")
    public static Object[][] getDistributors(final ITestContext context,
                                          final Method method) {

        final Object[][] distributors = new Object[dsbTestData.size()][1];
        for (int i = 0; i < dsbTestData.size(); ++i)
            distributors[i][0] = dsbTestData.get(i);
        return distributors;
    }



}
