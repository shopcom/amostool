package com.marketamerica.dsb;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;

import com.marketamerica.automation.Configuration;
import com.marketamerica.automation.testdata.Distributor;
import com.marketamerica.automation.testdata.Placement;
import com.marketamerica.automation.testdata.Shopper;
import com.marketamerica.automation.utilities.enums.Country;
import com.marketamerica.automation.utilities.enums.DistributorPin;
import com.marketamerica.automation.utilities.enums.Environment;
import com.marketamerica.automation.utilities.enums.PlacementLeg;
import com.marketamerica.automation.utilities.enums.Project;
import com.marketamerica.automation.utilities.helpers.HttpUtilities;

/**
 * The DSB Client will provide the ability to make programmatic calls against MA
 * DSB. Created by javierv on 7/30/2014.
 */
public class DSBClient {
	public final static String DEVELOPMENT_URL = "http://dsb-d01.maeagle.corp/dataEngine/rest/dataretrieval/redback";
	public static final Locale[] supportedLocales = new Locale[] { Locale.US, Locale.CANADA,
			new Locale("en", "AU"), Locale.TAIWAN, Locale.UK, new Locale("es", "MX") };
	private final static Logger logger = LogManager.getLogger(DSBClient.class);
	private final static String STAGING_URL = "http://dsb-s02.maeagle.corp/dataEngine/rest/dataretrieval/redback";
	private final static String PRODUCTION_URL = "http://dsb-p07.maeagle.corp/dataEngine/rest/dataretrieval/redback";
	private final Locale locale;
	private Environment environment;
	private URI dsbURI;

	public DSBClient() {
		try {
			setURL(Configuration.getEnvironment());
		} catch (URISyntaxException e) {
			logger.error(e);
		}
		this.locale = Configuration.getLocale();
		this.environment = Configuration.getEnvironment();

	}

	public DSBClient(Environment environment) {
		try {
			setURL(environment);
			this.environment = environment;
		} catch (URISyntaxException e) {
			logger.error(e);
		}
		this.locale = null;
	}

	public DSBClient(Environment environment, Locale locale) {
		try {
			setURL(environment);
			this.environment = environment;
		} catch (URISyntaxException e) {
			logger.error(e);
		}
		this.locale = locale;
	}

	/**
	 * Get a distributor's profile based on a distributor id
	 *
	 * @param distributor
	 *            a distributor object.
	 * @return
	 */
	public String getDistProfile(Distributor distributor) {
		String xml = getXMLFile("DSBClient.getDistProfile.xml");

		xml = xml.replaceAll("%Replace::distID%", distributor.getDistributorId());
		xml = xml.replaceAll("%Replace::siteType%", "w");
		xml = replaceStandardFields(xml);

		final HttpEntity httpEntity = createHttpEntity(xml);
		return HttpUtilities.getPostResponse(dsbURI, httpEntity);
	}

	/**
	 * Returns the Original Distributor password via the DSB
	 *
	 * @param distributor
	 *            an instance of distributor that has a rep id
	 * @return the provided distributor's original password
	 */
	public String getOriginalDistributorPassword(Distributor distributor) {
		String xml = getXMLFile("DSBClient.getDistOriginalPwd.xml");

		final String representativeId = distributor.getRepresentativeId();
		if (representativeId == null) {
			logger.error("In order to query for the Rep's original password, the Distributor's rep id must be populated!");
		}
		xml = xml.replaceAll("%Replace::repID%", representativeId);
		xml = xml.replaceAll("%Replace::siteType%", "W");
		xml = replaceStandardFields(xml);

		final HttpEntity httpEntity = createHttpEntity(xml);
		final String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);

		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document jdomDocument = null;
		try {
			jdomDocument = jdomBuilder.build(new StringReader(response));
		} catch (JDOMException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}

		@SuppressWarnings("unchecked")
		final List<Element> entries = jdomDocument.getRootElement().getChild("results").getChild("results")
				.getChildren();
		return getValue(entries, "tempPwd");
	}

	public String getRepPassword(Distributor distributor) {
		String xml = getXMLFile("DSBClient.getRepIdPassword.xml");

		xml = xml.replaceAll("%Replace::repID%", distributor.getRepresentativeId());
		xml = xml.replaceAll("%Replace::siteType%", "W");
		xml = replaceStandardFields(xml);

		final HttpEntity httpEntity = createHttpEntity(xml);
		final String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);

		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document jdomDocument = null;
		try {
			jdomDocument = jdomBuilder.build(new StringReader(response));
		} catch (JDOMException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}

		@SuppressWarnings("unchecked")
		final List<Element> entries = jdomDocument.getRootElement().getChild("results").getChild("results")
				.getChildren();
		return getValue(entries, "password");
	}

	/**
	 * Get order info based on an order number and distributor
	 *
	 * @param distributor
	 *            a distributor object
	 * @param orderNumber
	 *            a string value representing an order number
	 * @return a string value representing the order info
	 */
	public String getOrderInfo(final Distributor distributor, final String orderNumber) {
		String xml = getXMLFile("DSBClient.getOrderDetail.xml");

		xml = xml.replaceAll("%Replace::distID%", distributor.getDistributorId());
		xml = xml.replaceAll("%Replace::ordNumber%", orderNumber);
		xml = replaceStandardFields(xml);

		final HttpEntity httpEntity = createHttpEntity(xml);
		return HttpUtilities.getPostResponse(dsbURI, httpEntity);
	}

	/**
	 * Set a Distributor's pin
	 *
	 * @param distributor
	 *            a distributor object that has a populated distributor id
	 * @param pin
	 *            a Distributor pin level
	 * @return the return provided by the server (mainly useful for debugging)
	 * @see com.marketamerica.automation.utilities.enums.DistributorPin
	 */
	@SuppressWarnings("unchecked")
	public String setDistributorPin(final Distributor distributor, final DistributorPin pin) {
		String xml = getXMLFile("DSBClient.setRepDist.xml");

		xml = xml.replaceAll("%Replace::distID%", distributor.getDistributorId());
		final String newPin = pin.toString().toUpperCase();
		xml = xml.replaceAll("%Replace::newType%", newPin);
		xml = replaceStandardFields(xml);

		final HttpEntity httpEntity = createHttpEntity(xml);
		String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);

		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document jdomDocument = null;
		try {
			jdomDocument = jdomBuilder.build(new StringReader(response));
		} catch (JDOMException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}
		List<Element> entries = null;
		try {
			assert jdomDocument != null;
			entries = jdomDocument.getRootElement().getChild("results").getChild("results").getChildren();
		} catch (NullPointerException e) {
			throw new DSBRecordUpdateException(String.format(
					"When trying to update the distributor's (%s) pin level,"
							+ " The XML Response was invalid! See %s", distributor, jdomDocument));
		}
		checkForError(entries);

		String oldPin = getValue(entries, "oldType");
		if (oldPin.isEmpty()) {
			logger.debug(String.format("Distributor (%s) old pin was empty", distributor.getDistributorId()));
		} else {
			logger.debug(String.format("Changing Distributor (%s) from a %s to a %s",
					distributor.getDistributorId(), oldPin, newPin));
		}
		return response;
	}

	/**
	 * Checks the server message for errors. The DSB Client returns two types of
	 * errors. (1) in the form of an exception and (2) in the form of a server
	 * message. This method checks the latter form.
	 *
	 * @param entries
	 *            a list of XML Elements provided from a DSB request
	 */
	private void checkForError(List<Element> entries) {
		final String svrMessage = getValue(entries, "svrMessage");
		if (svrMessage.contains("record is locked")) {
			throw new DSBRecordUpdateException(
					String.format("Could not update record because it is currently locked!"));
		} else if (svrMessage.contains("Incorrect type was entered")) {
			throw new DSBRecordUpdateException(
					String.format("Could not update distributor pin because the type was invalid!!"));
		} else if (!svrMessage.isEmpty()) {
			throw new DSBRecordUpdateException(svrMessage);
		}
	}

	private String getValue(List<Element> elements, String key) {
		for (Element entry : elements) {
			final List children = entry.getChildren();
			final Element element = (Element) children.get(0);
			if (element.getValue().equals(key)) {
				return ((Element) children.get(1)).getText();
			}
		}
		return "";
	}

	private HttpEntity createHttpEntity(final String xml) {
		HttpEntity entity = null;
		try {
			entity = new ByteArrayEntity(xml.getBytes("UTF-8"));
		} catch (UnsupportedEncodingException e) {
			logger.error(e);
		}
		return entity;
	}

	private String getXMLFile(final String xmlFileName) {
		String xml = null;
		InputStream inputStream = getClass().getResourceAsStream("/" + xmlFileName);
		if (inputStream == null) {
			inputStream = getClass().getResourceAsStream("/" + Project.DSB_CLIENT + "/" + xmlFileName);
		}
		try {
			xml = IOUtils.toString(inputStream);
		} catch (IOException e) {
			logger.error(e);
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				logger.error(e);
			}
		}
		assert xml != null;
		return xml;
	}

	/**
	 * @return the ISO3Country (in upper case)
	 */
	private String getISO3Country() {
		if (locale == null) {
			return "";
		} else {
			return locale.getISO3Country().toUpperCase();
		}
	}

	/**
	 * @return the ISO3Language (in upper case)
	 */
	private String getISO3Language() {
		if (locale == null) {
			return "";
		} else if(locale.getCountry() == "OTHER" || locale.getCountry() == "EMP"){
			//This is a temporary fix. Please revisit if this fails
			return "USA";
		}
		else{
			return locale.getISO3Language().toUpperCase();
		}
	}

	/**
	 * Substitute standard fields in the given xml string. Currently, it
	 * replaces the ISO3 Country and ISO3 language values, as well as the the
	 * transaction ID
	 *
	 * @param xml
	 *            a string representing an xml file
	 * @return an updated xml string
	 */
	private String replaceStandardFields(String xml) {
		xml = xml.replaceAll("%Replace::siteCountry%", getISO3Country());
		xml = xml.replaceAll("%Replace::langCode%", getISO3Language());
		xml = xml.replaceAll("%Replace::UUID%", UUID.randomUUID().toString());
		return xml;
	}

	/**
	 * Set the DSB Client URI based on the given Environment
	 *
	 * @param environment
	 */
	public void setURL(Environment environment) throws URISyntaxException {
		switch (environment) {
		case LIVE:
			this.dsbURI = new URI(DSBClient.PRODUCTION_URL);
			break;
		case STAGING:
			this.dsbURI = new URI(DSBClient.STAGING_URL);
			break;
		case DEVELOPMENT:
			this.dsbURI = new URI(DSBClient.DEVELOPMENT_URL);
			break;
		default:
			throw new IllegalArgumentException(String.format("Invalid Environment Provided: %s", environment));
		}
	}

	/**
	 * Returns a Distributor's COA
	 *
	 * @param distributor
	 *            a distributor object that has a populated distributor id
	 * @return a Big Decimal value representing the distributor's COA
	 */
	public BigDecimal getDistributorCOA(Distributor distributor) {
		String xml = getXMLFile("DSBClient.getDistributorCOA.xml");

		xml = xml.replaceAll("%Replace::distID%", distributor.getDistributorId());
		xml = replaceStandardFields(xml);

		final HttpEntity httpEntity = createHttpEntity(xml);
		String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);

		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document jdomDocument = null;
		try {
			jdomDocument = jdomBuilder.build(new StringReader(response));
		} catch (JDOMException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}

		final List<Element> entries = jdomDocument.getRootElement().getChild("results").getChild("results")
				.getChildren();
		return new BigDecimal(getValue(entries, "creditOnAccountAmount"));
	}

	public String getValidTaxID(Country country) {
		String xml = getXMLFile("DSBClient.getValidTaxID.xml");

		xml = replaceStandardFields(xml);

		final HttpEntity httpEntity = createHttpEntity(xml);
		String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);

		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document jdomDocument = null;
		try {
			jdomDocument = jdomBuilder.build(new StringReader(response));
		} catch (JDOMException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}

		final List<Element> entries = jdomDocument.getRootElement().getChild("results").getChild("results")
				.getChildren();
		return getValue(entries, getISO3Country().toUpperCase() + "NextValidTaxId");
	}

	public String getValidTaxID(Locale locale) {
		String xml = getXMLFile("DSBClient.getValidTaxID.xml");

		xml = replaceStandardFields(xml);

		final HttpEntity httpEntity = createHttpEntity(xml);
		String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);

		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document jdomDocument = null;
		try {
			jdomDocument = jdomBuilder.build(new StringReader(response));
		} catch (JDOMException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}

		final List<Element> entries = jdomDocument.getRootElement().getChild("results").getChild("results")
				.getChildren();
		return getValue(entries, getISO3Country().toUpperCase() + "NextValidTaxId");
	}

	public Map<Country, String> getValidTaxIDs() {

		final Locale[] supportedLocales = new Locale[] { Locale.US, Locale.CANADA, new Locale("en", "AU"),
				Locale.TAIWAN, new Locale("zh", "TW"), Locale.UK, new Locale("es", "MX") };

		String xml = getXMLFile("DSBClient.getValidTaxID.xml");

		xml = replaceStandardFields(xml);
		xml = xml.replaceAll(getISO3Country(), "");

		final HttpEntity httpEntity = createHttpEntity(xml);
		String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);

		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document jdomDocument = null;
		try {
			jdomDocument = jdomBuilder.build(new StringReader(response));
		} catch (JDOMException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}

		checkForError(jdomDocument);
		final List<Element> entries = jdomDocument.getRootElement().getChild("results").getChild("results")
				.getChildren();

		Map<Country, String> output = new HashMap<>();

		for (int i = 0; i < supportedLocales.length; ++i) {
			output.put(Country.parse(supportedLocales[i].getDisplayCountry()),
					getValue(entries, supportedLocales[i].getISO3Country().toUpperCase() + "NextValidTaxId"));
		}

		return output;
	}

	/**
	 * Checks for errors at the root level. The DSB Client returns two types of
	 * errors. (1) in the form of an exception and (2) in the form of a server
	 * message. This method checks for the first form.
	 *
	 * @param jdomDocument
	 */
	private void checkForError(Document jdomDocument) {
		Element exception = jdomDocument.getRootElement().getChild("exception");
		if (exception != null) {
			throw new DSBRecordUpdateException(exception.getChild("detailMessage").getText());
		}
	}

	/**
	 * @param distributor
	 * @return
	 */
	public DistributorPin getDistributorPin(Distributor distributor) {
		String xml = getXMLFile("DSBClient.getDistributorPin.xml");

		xml = xml.replaceAll("%Replace::distID%", distributor.getDistributorId());
		xml = xml.replaceAll("%Replace::siteType%", "w");

		xml = replaceStandardFields(xml);

		final HttpEntity httpEntity = createHttpEntity(xml);
		String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);

		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document jdomDocument = null;
		try {
			jdomDocument = jdomBuilder.build(new StringReader(response));
		} catch (JDOMException | IOException e) {
			logger.error(e);
		}

		final List<Element> entries = jdomDocument.getRootElement().getChild("results").getChild("results")
				.getChildren();
		checkForError(entries);

		return DistributorPin.parse(getValue(entries, "distPinLevel"));
	}

	public Collection<Placement> getAllOpenPlacements(Distributor distributor) {
		Collection<Placement> placements = new HashSet<>();

		try {
			placements.addAll(getOpenPlacements(distributor, "001"));
		} catch (DSBRecordUpdateException e) {
			logger.error("Error retrieving placements on 001 leg", e);
		}		
		 
		try {
			placements.addAll(getOpenPlacements(distributor, "002"));
		} catch (DSBRecordUpdateException e) {
			logger.error("Error retrieving placements on 002 leg", e);
		}

		try {
			placements.addAll(getOpenPlacements(distributor, "003"));
		} catch (DSBRecordUpdateException e) {
			logger.error("Error retrieving placements on 003 leg", e);
		}

		return placements;
	}

	/**
	 * Get a distributor's open placements
	 *
	 * @param distributor
	 *            An instance of a distributor who has a distributor and
	 *            representative id
	 * @param bdc
	 *            a bdc to retrieve open placements on
	 * @return a collection of valid open placements
	 * @see com.marketamerica.automation.testdata.Placement
	 */
	public Collection<Placement> getOpenPlacements(Distributor distributor, String bdc) {
		String xml = getXMLFile("DSBClient.getOpenPlacements.xml");		

		if (distributor.getDistributorId() == null || distributor.getDistributorId().isEmpty()) {
			resolveDistributorID(distributor);
		}

		xml = xml.replaceAll("%Replace::distID%", distributor.getDistributorId());
		xml = xml.replaceAll("%Replace::repID%", distributor.getRepresentativeId());
		xml = xml.replaceAll("%Replace::bdc%", bdc);
		
		// Notice that the country and language parameters passed on will affect the returned 
		// placement result. For Staging, no country/language required, but for LIVE they are
		// required. So I make them a difference here.
		if(environment.equals(Environment.STAGING)){
			xml = xml.replaceAll("%Replace::UUID%", UUID.randomUUID().toString());			
		} else xml = replaceStandardFields(xml);
		
		final HttpEntity httpEntity = createHttpEntity(xml);
		String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);
		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document jdomDocument = null;
		try {
			jdomDocument = jdomBuilder.build(new StringReader(response));
		} catch (JDOMException | IOException e) {
			logger.error(e);
		}

		final List<Element> entries = jdomDocument.getRootElement().getChild("results").getChild("results")
				.getChildren();
		
		checkForError(entries);

		String[] businessDevelopmentCenters = getValue(entries, "DlBdc").split("Ã½");
		String[] distributorIds = getValue(entries, "DlDistId").split("Ã½");
		String[] legs = getValue(entries, "DlLeg").split("Ã½");
		if (businessDevelopmentCenters.length == distributorIds.length && distributorIds.length == legs.length) {
			Collection<Placement> placements = new ArrayList<>();
			for (int i = 0; i < businessDevelopmentCenters.length; ++i) {
				Placement placement = new Placement();
				placement.setUnFranchiseId(distributorIds[i]);
				placement.setExtension(businessDevelopmentCenters[i]);
				placement.setLeg(PlacementLeg.parse(legs[i]));
				placements.add(placement);
			}
			return placements;
		} else {
			return new ArrayList<>();
		}

	}

	/**
	 * Update
	 *
	 * @param distributor
	 */
	public void resolveDistributorID(Distributor distributor) {

		String xml = getXMLFile("DSBClient.findDistributorID.xml");
		xml = replaceStandardFields(xml);
		xml = xml.replaceAll("%Replace::repID%", distributor.getRepresentativeId());
		final HttpEntity httpEntity = createHttpEntity(xml);
		String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);

		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document jdomDocument = null;
		try {
			jdomDocument = jdomBuilder.build(new StringReader(response));
		} catch (JDOMException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}

		checkForError(jdomDocument);

		final List<Element> entries = jdomDocument.getRootElement().getChild("results").getChild("results")
				.getChildren();
		checkForError(entries);

		String distributorId = getValue(entries, "distID");
		if (distributor.getDistributorId() != null && !distributor.getDistributorId().isEmpty()) {
			logger.warn(String.format("Changing distributor [%s]'s id from \"%s\" to \"%s\"", distributor,
					distributor.getDistributorId(), distributorId));
		}
		distributor.setDistributorId(distributorId);

	}

	public void resolveCustomerIDs(Shopper shopper) {
		String xml = getXMLFile("DSBClient.getCustomerID.xml");
		// xml = replaceStandardFields(xml);
		if (shopper.getEmail() == null || shopper.getEmail().isEmpty()) {
			logger.warn(String.format(
					"Could not update shopper PC ID and customer id without an email. Please update"
							+ " %s's info", shopper));
		} else {
			xml = xml.replaceAll("%Replace::userEmail%", shopper.getEmail());
			final HttpEntity httpEntity = createHttpEntity(xml);
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
			String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);

			final SAXBuilder jdomBuilder = new SAXBuilder();
			Document jdomDocument = null;
			try {
				jdomDocument = jdomBuilder.build(new StringReader(response));
			} catch (JDOMException e) {
				logger.error(e);
			} catch (IOException e) {
				logger.error(e);
			}

			checkForError(jdomDocument);

			final List<Element> entries = jdomDocument.getRootElement().getChild("results").getChild("results")
					.getChildren();
			checkForError(entries);

			String pcID = getValue(entries, "pcID");
			if (shopper.getPcID() != null && !shopper.getPcID().isEmpty()) {
				logger.warn(String.format("Changing preferred customer [%s]'s pcid from \"%s\" to \"%s\"",
						shopper, shopper.getPcID(), pcID));
			}
			shopper.setPcID(pcID);

			String shopperID = getValue(entries, "shopperID");
			if (shopper.getShopperID() != null && !shopper.getShopperID().isEmpty()) {
				logger.warn(String.format("Changing preferred customer [%s]'s shopper id from \"%s\" to \"%s\"",
						shopper, shopper.getShopperID(), shopperID));
			}
			shopper.setShopperID(shopperID);
		}
	}

	public Map<String, String> getPCProfile(Shopper shopper) {
		String xml = getXMLFile("DSBClient.getPCProfile.xml");
		// xml = replaceStandardFields(xml);
		xml = xml.replaceAll("%Replace::pcID%", shopper.getPcID());
		final HttpEntity httpEntity = createHttpEntity(xml);
		String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);

		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document jdomDocument = null;
		try {
			jdomDocument = jdomBuilder.build(new StringReader(response));
		} catch (JDOMException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}

		checkForError(jdomDocument);

		@SuppressWarnings("unchecked")
		final List<Element> entries = jdomDocument.getRootElement().getChild("results").getChild("results")
				.getChildren();
		checkForError(entries);

		Map<String, String> profileData = new LinkedHashMap<>();

		profileData.put("First Name", getValue(entries, "firstName"));
		profileData.put("Last Name", getValue(entries, "lastName"));

		profileData.put("Email Address", getValue(entries, "email"));
		profileData.put("Registration Source", getValue(entries, "regSource"));
		profileData.put("Password", getValue(entries, "password"));
		profileData.put("Portal ID", getValue(entries, "portalID"));
		profileData.put("Sponsor Email", getValue(entries, "sponsorEmail"));
		// profileData.put("Representative ID", getValue(entries, "repID"));
		profileData.put("Sponsor Status", getValue(entries, "sponsorStatus"));
		// profileData.put("Order Date List", getValue(entries,
		// "ordDateList").replace("Ã½", ", "));
		profileData.put("Resident Country", getValue(entries, "residentCountry"));
		profileData.put("Distributor ID", getValue(entries, "distID"));
		profileData.put("PC ID", getValue(entries, "pcID"));
		profileData.put("Customer Manager", getValue(entries, "customerMgrName"));
		profileData.put("Cash Back Total", getValue(entries, "cashbackTotal"));

		return profileData;
	}

	/**
	 * Resolve a Distributors home country's 3 letter code abbreviation.
	 *
	 * @param distributor
	 *            a distributor who has a populated distributor id
	 * @return
	 */
	public String getDistributorHomeCountry(Distributor distributor) {
		String xml = getXMLFile("DSBClient.getDistributorHomeCountry.xml");

		xml = replaceStandardFields(xml);
		xml = xml.replaceAll("%Replace::distID%", distributor.getDistributorId());
		xml = xml.replaceAll("%Replace::siteType%", "w");

		final HttpEntity httpEntity = createHttpEntity(xml);
		String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);

		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document jdomDocument = null;
		try {
			jdomDocument = jdomBuilder.build(new StringReader(response));
		} catch (JDOMException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}

		checkForError(jdomDocument);

		final List<Element> entries = jdomDocument.getRootElement().getChild("results").getChild("results")
				.getChildren();
		checkForError(entries);

		return getValue(entries, "homeCountry");
	}

	/**
	 * Get a distributor's inquiry detail based on a distributor id
	 *
	 * @param distributor
	 *            a distributor object.
	 * @return
	 */
	public Map<String, String> getDistInquiryDetail(Distributor distributor) {
		String xml = getXMLFile("DSBClient.getDistInquiryDetail.xml");

		xml = xml.replaceAll("%Replace::distID%", distributor.getDistributorId());
		xml = xml.replaceAll("%Replace::siteType%", "w");
		xml = replaceStandardFields(xml);

		final HttpEntity httpEntity = createHttpEntity(xml);
		// return HttpUtilities.getPostResponse(dsbURI, httpEntity);
		String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);

		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document jdomDocument = null;
		try {
			jdomDocument = jdomBuilder.build(new StringReader(response));
		} catch (JDOMException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}

		checkForError(jdomDocument);

		@SuppressWarnings("unchecked")
		final List<Element> entries = jdomDocument.getRootElement().getChild("results").getChild("results")
				.getChildren();
		checkForError(entries);

		Map<String, String> profileData = new LinkedHashMap<>();

		profileData.put("Distributor ID", getValue(entries, "distID"));
		profileData.put("Rep ID", getValue(entries, "repID"));
		profileData.put("Password", getValue(entries, "password"));
		profileData.put("Home Country", getValue(entries, "homeCountry"));
		profileData.put("Distributor Type", getValue(entries, "distType"));
		profileData.put("Email Address", getValue(entries, "email"));

		return profileData;
	}

	/**
	 * Cancel An Order placed based on orderId and filing Id . For Automation
	 * purposes , the User Id used is always GSO QA
	 * 
	 * @param orderId - order Id of the placed order
	 * @param filingId - is the Distributor Id who placed the orders
	 * @return boolean - is order cancelled successfully? 
	 */
	public boolean cancelOrder(String orderId, String filingId) {
		logger.info(String.format("Cancelling order with orderId %s , fillingId - %s", orderId, filingId));
		String xml = getXMLFile("DSBClient.CancelOrder.xml");

		xml = xml.replaceAll("%Replace::orderID%", orderId.trim());
		xml = xml.replaceAll("%Replace::filingID%", filingId.trim());
		xml = xml.replaceAll("%Replace::userID%", "GSO QA");
		xml = xml.replaceAll("%Replace::UUID%", UUID.randomUUID().toString());

		final HttpEntity httpEntity = createHttpEntity(xml);
		String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);
		if(response.contains("The proxy server could not handle the request") || response.contains("Redback reads has been exceeded")){
			logger.fatal("Proxy / Timeout Error Response..Could not cancel order ");
			return false;
		}
		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document jdomDocument = null;
		try {
			jdomDocument = jdomBuilder.build(new StringReader(response));
		}catch (JDOMException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}

		logger.info(jdomDocument);
		checkForError(jdomDocument);

		@SuppressWarnings("unchecked")
		final List<Element> entries = jdomDocument.getRootElement().getChild("results").getChild("results")
				.getChildren();

		Integer svrStatus = Integer.valueOf(getValue(entries, "svrStatus"));
		String svrMessage = String.valueOf(getValue(entries, "svrMessage")).trim();

		if (svrMessage.contains("ORDERS is locked")) {
			logger.error(String.format("Could not update record because it is currently locked!"));
			logger.error(new DSBRecordUpdateException(
					String.format("Could not update record because it is currently locked!")));
			return false;
		} else if (svrMessage.contains("Incorrect type was entered")) {
			logger.error(String.format("Could not update distributor pin because the type was invalid!!"));
			logger.error(new DSBRecordUpdateException(
					String.format("Could not update distributor pin because the type was invalid!!")));
			return false;
		} else if(svrMessage.contains("EXCLUDE.MBRF")) {
			logger.error(String.format("Member is not EXCLUDE.MBRF FILE, order could not be cancelled"));
			logger.error(new DSBRecordUpdateException(svrMessage));
			return false;
		}

		if (svrStatus == 0) {
			logger.info(String.format("Order was cancelled successfully."));
			return true;
		} else if (svrStatus == -1 && svrMessage.contains("Order is on \"X\" status")) {
			logger.info(String.format("Order id - %s is cancelled already and is in X status , svrMessage : %s", orderId,
					svrMessage));
			return true;
		} else {
			logger.error(String.format("Order id - %s could not be cancelled , Reason : %s", orderId, svrMessage));
			logger.error(new DSBRecordUpdateException(svrMessage));
			return false;
		}
	}

	/**
	 * Toggle Order Queue ON / OFF for specific SiteType to switch ON/ OFF for
	 * Offline Ordering
	 * 
	 * @param transCode
	 * @param siteType
	 * @return
	 */
	public boolean toggleOrderQueue(String transCode, String siteType) {
		String xml = getXMLFile("DSBClient.ToggleOrderQueue.xml");

		xml = xml.replaceAll("%Replace::transCode%", transCode);
		xml = xml.replaceAll("%Replace::siteType%", siteType);
		xml = replaceStandardFields(xml);

		final HttpEntity httpEntity = createHttpEntity(xml);
		String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);

		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document jdomDocument = null;
		try {
			jdomDocument = jdomBuilder.build(new StringReader(response));
		} catch (JDOMException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}

		checkForError(jdomDocument);

		@SuppressWarnings("unchecked")
		final List<Element> entries = jdomDocument.getRootElement().getChild("results").getChild("results")
				.getChildren();
		checkForError(entries);
		return true;
	}

	/**
	 * Get the order ID, distributorID , paying ID, amount , orderStatus from
	 * tempOrderID for Offline Ordering
	 * 
	 * @param tempOrderID
	 * @return
	 */
	public Map<String, String> getOrderQueueData(String tempOrderID) {
		String xml = getXMLFile("DSBClient.getOrderQueueData.xml");

		xml = xml.replaceAll("%Replace::tempOrderID%", tempOrderID.trim());
		xml = replaceStandardFields(xml);

		//sleeping for few seconds because there is a 10 second delay between the phantom processes that select any outstanding queued orders.
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		final HttpEntity httpEntity = createHttpEntity(xml);
		String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);
		if(response.contains("Order not found")){
			logger.info("Order Not Found , Retrying after 20 seconds sleep time");
			//sleeping for few more seconds 
			try {
				Thread.sleep(20000);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
			}
		response = HttpUtilities.getPostResponse(dsbURI, httpEntity);
		logger.info(String.format("response after retrying %s", response ));
		}
		
		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document jdomDocument = null;
		try {
			jdomDocument = jdomBuilder.build(new StringReader(response));
		} catch (JDOMException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}

		checkForError(jdomDocument);

		@SuppressWarnings("unchecked")
		final List<Element> entries = jdomDocument.getRootElement().getChild("results").getChild("results")
				.getChildren();
		checkForError(entries);

		Map<String, String> orderData = new LinkedHashMap<>();

		orderData.put("Order ID", getValue(entries, "orderID"));
		orderData.put("Distributor ID", getValue(entries, "distID"));
		orderData.put("Paying ID", getValue(entries, "payingID"));
		orderData.put("Amount", getValue(entries, "amount"));
		orderData.put("Order Status", getValue(entries, "orderStatus"));

		return orderData;

	}
	
	/**
	 * Single DSB method to get the Order Data from temp Order Id and Cancel the Order
	 * @param tempOrderID
	 * @return
	 */
	public boolean getOrderQueueDataAndCancelOrder(String tempOrderID){
		Map<String, String> orderData = getOrderQueueData(tempOrderID);
		String orderID = orderData.get("Order ID");
		String fillingID = orderData.get("Distributor ID");
		return cancelOrder(orderID, fillingID);
	}
	
	/**
	 * Method to get Order Queue Health Check for different iteType and countries
	 * 
	 * @param transCode
	 * @param siteType
	 * @return
	 */
	public Map<String, String> getOrderQueueHealthCheck() {
		String xml = getXMLFile("DSBClient.GetOrderQueueHealthCheck.xml");

		final HttpEntity httpEntity = createHttpEntity(xml);
		String response = HttpUtilities.getPostResponse(dsbURI, httpEntity);

		final SAXBuilder jdomBuilder = new SAXBuilder();
		Document jdomDocument = null;
		try {
			jdomDocument = jdomBuilder.build(new StringReader(response));
		} catch (JDOMException e) {
			logger.error(e);
		} catch (IOException e) {
			logger.error(e);
		}

		checkForError(jdomDocument);

		@SuppressWarnings("unchecked")
		final List<Element> entries = jdomDocument.getRootElement().getChild("results").getChild("results")
				.getChildren();
		checkForError(entries);
		
		Map<String, String> queueData = new LinkedHashMap<>();

		queueData.put("Queue Monitor Status", getValue(entries, "queueMonitorStatus"));
		queueData.put("SiteType", getValue(entries, "siteType"));
		queueData.put("Country", getValue(entries, "country"));
		queueData.put("Queue Status", getValue(entries, "queueStatus"));

		return queueData;
	}


}
