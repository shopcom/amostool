package com.marketamerica.dsb;

/**
 * Throw this exception when a DSB Record is locked
 * Created by Javier L. Velasquez on 10/14/2014.
 */
public class DSBRecordUpdateException extends RuntimeException {
    public DSBRecordUpdateException(String message) {
        super(message);
    }
}
